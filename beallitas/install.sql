-- sqldump-php SQL Dump
-- https://github.com/clouddueling/mysqldump-php
--
-- Host: localhost
-- Generation Time: Wed, 16 Apr 2014 14:51:58 +0200

--
-- Database: `bstore`
--

-- --------------------------------------------------------

--
-- Table structure for table `beepulo_cikkgaleria`
--

DROP TABLE IF EXISTS `beepulo_cikkgaleria`;

CREATE TABLE `beepulo_cikkgaleria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cikkek_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `kisfile` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `beepulo_cikkgaleria`
--

INSERT INTO `beepulo_cikkgaleria` VALUES ('49','8','9f0ae9ab34bd2ca72dad4deb7ff49ec4.jpg','','404.jpg','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('50','62','ccacbfc01fc6858bbb6abff27eb748b6.JPG','','022.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('53','62','74134f47340e410539a4f6e41c2a52b4.JPG','','005.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('55','62','9d1f7a0e026facb87448df16a1403db9.JPG','','011.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('57','62','f2d0fc334035a3e0fad2cab64e0d76fc.JPG','','010.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('58','62','f4e00b99c80b51af90b27091b45a933e.JPG','','021.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('70','72','83cdc5beb467682ffcb84cbf4a328884.JPG','','004.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('71','72','4084257c389fb9305c476430932a1a4c.JPG','','010.JPG','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('72','72','9033f7c8f9cd85b96a678f496cbeb0ec.JPG','','015.JPG','1');
INSERT INTO `beepulo_cikkgaleria` VALUES ('75','75','fdd21827f9d73ba7e285dda9d7da8f3f.jpg','','1 százalék_plakát_2013-page-001 (1).jpg','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('76','22','9a2dcbb76624533d64805b5f2d68205f.gif','','kezdolap01.gif','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('77','22','94e03638e37604176bf75b164349591d.gif','','kezdolap02.gif','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('78','27','54aa111816432dc7739ca0e4c7d85ad7.jpg','','uzlet1.jpg','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('79','27','3adf0734a492ef59aecab7a79c83cc93.jpg','','uzlet2.jpg','0');
INSERT INTO `beepulo_cikkgaleria` VALUES ('80','27','8e430c867192d8d9ec3d7f6b4bd63035.jpg','','uzlet3.jpg','0');
-- --------------------------------------------------------

--
-- Table structure for table `bos_adatlap`
--

DROP TABLE IF EXISTS `bos_adatlap`;

CREATE TABLE `bos_adatlap` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `csoport_id` int(11) NOT NULL,
  `csoport` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `label` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `szerkesztheto` tinyint(4) NOT NULL,
  `alap` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL,
  `egyseg` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2244 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bos_adatlap`
--

INSERT INTO `bos_adatlap` VALUES ('121','1','Normál hűtőkamra','magasság','1','2,48','3','m');
INSERT INTO `bos_adatlap` VALUES ('81','1','Normál hűtőkamra','hosszúság','1','','1','m');
INSERT INTO `bos_adatlap` VALUES ('101','1','Normál hűtőkamra','szélesség','1','','2','m');
INSERT INTO `bos_adatlap` VALUES ('141','1','Normál hűtőkamra','válaszfal','1','','3','m');
INSERT INTO `bos_adatlap` VALUES ('421','2','Mirelit hűtőkamra','zárható ajtó keretfűtéssel','1','1','12','db');
INSERT INTO `bos_adatlap` VALUES ('381','2','Mirelit hűtőkamra','válaszfal','1','','10','m');
INSERT INTO `bos_adatlap` VALUES ('401','2','Mirelit hűtőkamra','zárható ajtó keretfűtéssel','1','1900x900x100','11','mm');
INSERT INTO `bos_adatlap` VALUES ('361','2','Mirelit hűtőkamra','magasság','1','2.5','9','m');
INSERT INTO `bos_adatlap` VALUES ('341','2','Mirelit hűtőkamra','szélesség','1','','8','m');
INSERT INTO `bos_adatlap` VALUES ('261','1','Normál hűtőkamra','zárható ajtó','1','900x1900x80','100','mm');
INSERT INTO `bos_adatlap` VALUES ('281','1','Normál hűtőkamra','zárható ajtó db','1','1','110','db');
INSERT INTO `bos_adatlap` VALUES ('321','2','Mirelit hűtőkamra','hosszúság','1','','7','m');
INSERT INTO `bos_adatlap` VALUES ('481','3','Ital hűtőkamra','hosszúság','1','','13','m');
INSERT INTO `bos_adatlap` VALUES ('501','3','Ital hűtőkamra','szélesség','1','','14','m');
INSERT INTO `bos_adatlap` VALUES ('521','3','Ital hűtőkamra','magasság','1','2,48','15','m');
INSERT INTO `bos_adatlap` VALUES ('541','3','Ital hűtőkamra','üvegajtó 660x1806','1','2','16','db');
INSERT INTO `bos_adatlap` VALUES ('561','3','Ital hűtőkamra','Keretvilágítás','1','csökkentett hőkibocsátású neon;LED\r\n','17','');
INSERT INTO `bos_adatlap` VALUES ('581','3','Ital hűtőkamra','Feltöltő ajtó 900x1900','1','nem;igen','18','');
INSERT INTO `bos_adatlap` VALUES ('601','4','Áruházi faliállvány','magasság','1','2000/90/30;1754/90/30;1631/60/30','19','mm');
INSERT INTO `bos_adatlap` VALUES ('621','4','Áruházi faliállvány','600-as elem','1','0','20','db');
INSERT INTO `bos_adatlap` VALUES ('641','4','Áruházi faliállvány','800-as elem','1','0','21','db');
INSERT INTO `bos_adatlap` VALUES ('661','4','Áruházi faliállvány','1000-es elem','1','0','22','db');
INSERT INTO `bos_adatlap` VALUES ('681','4','Áruházi faliállvány','1325-ös elem','1','0','23','db');
INSERT INTO `bos_adatlap` VALUES ('701','4','Áruházi faliállvány','90 °-os belső sarokelem','1','0','24','db');
INSERT INTO `bos_adatlap` VALUES ('721','4','Áruházi faliállvány','45°-os külső sarokelem','1','0','25','db');
INSERT INTO `bos_adatlap` VALUES ('741','4','Áruházi faliállvány','Lábazati takarás','1','nyitott;zárt','29','');
INSERT INTO `bos_adatlap` VALUES ('761','4','Áruházi faliállvány','alsó bázispolc','1','500;400;600;nincs','26','mm');
INSERT INTO `bos_adatlap` VALUES ('781','4','Áruházi faliállvány','400 mm-es dönthető polc','1','3','27','sor');
INSERT INTO `bos_adatlap` VALUES ('801','4','Áruházi faliállvány','300 mm-es dönthető polc','1','2','28','sor');
INSERT INTO `bos_adatlap` VALUES ('821','6','Áruházi gondola','magasság','1','1631/60/30;1750/60/30;2000/90/30;1250/60/30;1000/60/30','30','mm');
INSERT INTO `bos_adatlap` VALUES ('841','6','Áruházi gondola','600-as elem','1','0','31','db');
INSERT INTO `bos_adatlap` VALUES ('861','6','Áruházi gondola','800-as elem','1','0','32','db');
INSERT INTO `bos_adatlap` VALUES ('881','6','Áruházi gondola','1000-es elem','1','0','33','db');
INSERT INTO `bos_adatlap` VALUES ('901','6','Áruházi gondola','1325-ös elem','1','0','34','db');
INSERT INTO `bos_adatlap` VALUES ('921','6','Áruházi gondola','Végelem dupla hátfallal','1','1000;800;1325;nincs','39','');
INSERT INTO `bos_adatlap` VALUES ('941','6','Áruházi gondola','alsó bázispolc','1','500;400;600;nincs','35','mm');
INSERT INTO `bos_adatlap` VALUES ('961','6','Áruházi gondola','400 mm-es dönthető polc','1','2','36','sor');
INSERT INTO `bos_adatlap` VALUES ('981','6','Áruházi gondola','300 mm-es dönthető polc','1','2','37','sor');
INSERT INTO `bos_adatlap` VALUES ('1001','6','Áruházi gondola','Lábazati takarás','1','nyitott;zárt','38','');
INSERT INTO `bos_adatlap` VALUES ('1021','7','Normál hűtőaggregát','Típus','1','Copeland Schroll;Bitzer;Dorin;Aspera;Tecumseh;','40','');
INSERT INTO `bos_adatlap` VALUES ('1041','7','Normál hűtőaggregát','Hűtőteljesítmény','1','','41','kW');
INSERT INTO `bos_adatlap` VALUES ('1061','7','Normál hűtőaggregát','Villamos teljesítmény','1','','42','kW');
INSERT INTO `bos_adatlap` VALUES ('1161','8','Mirelit hűtőaggregát','Típus','1','CopelandSchroll;Aspera;Tecumseh;Bitzer;Dorin','44','');
INSERT INTO `bos_adatlap` VALUES ('1101','7','Normál hűtőaggregát','Feszülség igény','1','400/3/50;230/1/50','43','');
INSERT INTO `bos_adatlap` VALUES ('1181','8','Mirelit hűtőaggregát','Hűtőteljesítmény','1','','45','kW');
INSERT INTO `bos_adatlap` VALUES ('1201','8','Mirelit hűtőaggregát','Villamos teljesítmény','1','','46','kW');
INSERT INTO `bos_adatlap` VALUES ('1221','8','Mirelit hűtőaggregát','Feszülség igény','1','400/3/50;230/1/50','47','');
INSERT INTO `bos_adatlap` VALUES ('1261','6','Áruházi gondola','Végelem dupla hátfallal','1','0','42','db');
INSERT INTO `bos_adatlap` VALUES ('1281','9','Normál csoport aggregát','Hűtőteljesítmény','1','','2','kW');
INSERT INTO `bos_adatlap` VALUES ('1301','9','Normál csoport aggregát','Villamos teljesítmény','1','','3','kW');
INSERT INTO `bos_adatlap` VALUES ('1321','9','Normál csoport aggregát','kompresszor típus','1','CopelandSchroll;Bitzer;Dorin','4','');
INSERT INTO `bos_adatlap` VALUES ('1341','9','Normál csoport aggregát','Csoportaggregát típus','1','','1','');
INSERT INTO `bos_adatlap` VALUES ('1361','9','Normál csoport aggregát','Kompresszorok száma','1','2;3;4;5;6;7;8;9;10','6','db');
INSERT INTO `bos_adatlap` VALUES ('1381','10','Mirelit csoport aggregát','Csoportaggregát típus','1','','1','');
INSERT INTO `bos_adatlap` VALUES ('1401','10','Mirelit csoport aggregát','Hűtőteljesítmény','1','','2','kW');
INSERT INTO `bos_adatlap` VALUES ('1421','10','Mirelit csoport aggregát','Villamos teljesítmény','1','','3','kW');
INSERT INTO `bos_adatlap` VALUES ('1441','11','Egyedi háttérpult','teszt','1','10','10','db');
INSERT INTO `bos_adatlap` VALUES ('1461','10','Mirelit csoport aggregát','kompresszor típus','1','CopelandSchroll;Bitzer;Dorin','4','');
INSERT INTO `bos_adatlap` VALUES ('1481','10','Mirelit csoport aggregát','Kompresszorok száma','1','2;3;4;5;6;7;8;9;10','5','db');
INSERT INTO `bos_adatlap` VALUES ('1501','9','Normál csoport aggregát','méretek','1','','7','mm');
INSERT INTO `bos_adatlap` VALUES ('1521','9','Normál csoport aggregát','Digitális kivitel','1','igen;nem','6','');
INSERT INTO `bos_adatlap` VALUES ('2181','1','Normál hűtőkamra','PVC hőfüggöny','1','','1000','db');
INSERT INTO `bos_adatlap` VALUES ('2141','1','Normál hűtőkamra','Tárolt termék','1','','0','');
INSERT INTO `bos_adatlap` VALUES ('2001','1','Normál hűtőkamra','Elpárologtató típus','1','','282','');
INSERT INTO `bos_adatlap` VALUES ('2161','2','Mirelit hűtőkamra','Tárolt termék','1','','0','');
INSERT INTO `bos_adatlap` VALUES ('1641','12','Egyedi péksütemény állvány','Korpusz/fríz, világítás ','1','0','1','db');
INSERT INTO `bos_adatlap` VALUES ('1681','12','Egyedi péksütemény állvány','Tükör ','1','0','3','db');
INSERT INTO `bos_adatlap` VALUES ('1721','12','Egyedi péksütemény állvány','Bükk farácspolc ','1','0','5','db');
INSERT INTO `bos_adatlap` VALUES ('1761','12','Egyedi péksütemény állvány','Laminált polc ','1','0','7','db');
INSERT INTO `bos_adatlap` VALUES ('1801','12','Egyedi péksütemény állvány','Morzsatálca ','1','0','9','db');
INSERT INTO `bos_adatlap` VALUES ('1841','12','Egyedi péksütemény állvány','Plexiajtós fiók','1','0','11','db');
INSERT INTO `bos_adatlap` VALUES ('1881','12','Egyedi péksütemény állvány','Plexiajtós rekesz','1','0','13','db');
INSERT INTO `bos_adatlap` VALUES ('1921','12','Egyedi péksütemény állvány','Zsemle-kifli adagoló','1','0','15','db');
INSERT INTO `bos_adatlap` VALUES ('1961','12','Egyedi péksütemény állvány','LED világítás 1 fm','1','0','17','fm');
INSERT INTO `bos_adatlap` VALUES ('2243','6','Áruházi gondola','Megjegyzés','1','','0','');
INSERT INTO `bos_adatlap` VALUES ('2242','4','Áruházi faliállvány','Megjegyzés','1','','0','');
INSERT INTO `bos_adatlap` VALUES ('2021','2','Mirelit hűtőkamra','Elpárologtató típus','1','','422','');
INSERT INTO `bos_adatlap` VALUES ('2041','3','Ital hűtőkamra','Elpárologtató típus','1','','582','');
INSERT INTO `bos_adatlap` VALUES ('2081','4','Áruházi faliállvány','500 mm-es dönthető polc','1','0','26','sor');
INSERT INTO `bos_adatlap` VALUES ('2121','6','Áruházi gondola','500 mm-es dönthető polc','1','0','35','sor');
INSERT INTO `bos_adatlap` VALUES ('2221','2','Mirelit hűtőkamra','PVC hőfüggöny','1','','1000','db');
-- --------------------------------------------------------

--
-- Table structure for table `bos_adatlap_csoport`
--

DROP TABLE IF EXISTS `bos_adatlap_csoport`;

CREATE TABLE `bos_adatlap_csoport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_adatlap_csoport`
--

INSERT INTO `bos_adatlap_csoport` VALUES ('1','Normál hűtőkamra');
INSERT INTO `bos_adatlap_csoport` VALUES ('2','Mirelit hűtőkamra');
INSERT INTO `bos_adatlap_csoport` VALUES ('3','Ital hűtőkamra');
INSERT INTO `bos_adatlap_csoport` VALUES ('4','Áruházi faliállvány');
INSERT INTO `bos_adatlap_csoport` VALUES ('6','Áruházi gondola');
INSERT INTO `bos_adatlap_csoport` VALUES ('7','Normál hűtőaggregát');
INSERT INTO `bos_adatlap_csoport` VALUES ('8','Mirelit hűtőaggregát');
INSERT INTO `bos_adatlap_csoport` VALUES ('9','Normál csoport aggregát');
INSERT INTO `bos_adatlap_csoport` VALUES ('10','Mirelit csoport aggregát');
INSERT INTO `bos_adatlap_csoport` VALUES ('11','Egyedi háttérpult');
INSERT INTO `bos_adatlap_csoport` VALUES ('12','Egyedi péksütemény állvány');
-- --------------------------------------------------------

--
-- Table structure for table `bos_adoosztalyok`
--

DROP TABLE IF EXISTS `bos_adoosztalyok`;

CREATE TABLE `bos_adoosztalyok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) NOT NULL,
  `szamitas` varchar(50) NOT NULL DEFAULT 'plussz_szazalek',
  `modosito` varchar(50) NOT NULL DEFAULT '27',
  `leiras` varchar(255) NOT NULL,
  `alapertelmezett` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_adoosztalyok`
--

INSERT INTO `bos_adoosztalyok` VALUES ('3','ÁFA','plussz_szazalek','27','','1');
-- --------------------------------------------------------

--
-- Table structure for table `bos_arfolyamok`
--

DROP TABLE IF EXISTS `bos_arfolyamok`;

CREATE TABLE `bos_arfolyamok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `penznem` varchar(3) NOT NULL,
  `bazis` varchar(3) NOT NULL,
  `frissitve` int(11) NOT NULL,
  `rata` double NOT NULL,
  `nev` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_arfolyamok`
--

INSERT INTO `bos_arfolyamok` VALUES ('1','AED','EUR','1397570534','5.06878','United Arab Emirates Dirham');
INSERT INTO `bos_arfolyamok` VALUES ('2','ARS','EUR','1397570534','11.0408','Argentine Peso');
INSERT INTO `bos_arfolyamok` VALUES ('3','AUD','EUR','1397570535','1.4712','Australian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('4','AWG','EUR','1397570535','2.47056','Aruban Florin');
INSERT INTO `bos_arfolyamok` VALUES ('5','BAM','EUR','1397570535','1.95583','Bosnia and Herzegovina convertible mark');
INSERT INTO `bos_arfolyamok` VALUES ('6','BBD','EUR','1397570535','2.7604','Barbadian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('7','BDT','EUR','1397570535','107.2167','Bangladeshi Taka');
INSERT INTO `bos_arfolyamok` VALUES ('8','BGN','EUR','1397570535','1.95583','Bulgarian Lev');
INSERT INTO `bos_arfolyamok` VALUES ('9','BHD','EUR','1397570535','0.51896','Bahraini Dinar');
INSERT INTO `bos_arfolyamok` VALUES ('10','BMD','EUR','1397570535','1.3802','Bermudian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('11','BOB','EUR','1397570535','9.55446','Bolivian Boliviano');
INSERT INTO `bos_arfolyamok` VALUES ('12','BRL','EUR','1397570535','3.0607','Brazilian Real');
INSERT INTO `bos_arfolyamok` VALUES ('13','BSD','EUR','1397570536','1.3802','Bahamian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('14','CAD','EUR','1397570536','1.5171','Canadian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('15','CHF','EUR','1397570536','1.2161','Swiss Franc');
INSERT INTO `bos_arfolyamok` VALUES ('16','CLP','EUR','1397570536','758.5579','Chilean Peso');
INSERT INTO `bos_arfolyamok` VALUES ('17','CNY','EUR','1397570536','8.5868','Chinese Yuan');
INSERT INTO `bos_arfolyamok` VALUES ('18','CYP','EUR','1397570536','0.58527','Cyprus Pound');
INSERT INTO `bos_arfolyamok` VALUES ('19','CZK','EUR','1397570536','27.466','Czech Koruna');
INSERT INTO `bos_arfolyamok` VALUES ('20','DKK','EUR','1397570536','7.46038','Danish Krone');
INSERT INTO `bos_arfolyamok` VALUES ('21','DOP','EUR','1397570536','59.3141','Dominican Peso');
INSERT INTO `bos_arfolyamok` VALUES ('22','EEK','EUR','1397570536','15.64664','Estonia Kroon');
INSERT INTO `bos_arfolyamok` VALUES ('23','EGP','EUR','1397570536','9.6319','Egyptian Pound');
INSERT INTO `bos_arfolyamok` VALUES ('24','EUR','EUR','1397570537','1','Euro');
INSERT INTO `bos_arfolyamok` VALUES ('25','FJD','EUR','1397570537','2.53614','Fijian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('26','GBP','EUR','1397570537','0.8249','British Pound Sterling');
INSERT INTO `bos_arfolyamok` VALUES ('27','GHS','EUR','1397570537','3.794','Ghana Cedi');
INSERT INTO `bos_arfolyamok` VALUES ('28','GMD','EUR','1397570537','54.7263','Gambian Dalasi');
INSERT INTO `bos_arfolyamok` VALUES ('29','GTQ','EUR','1397570537','10.7099','Guatemalan Quetzal');
INSERT INTO `bos_arfolyamok` VALUES ('30','HKD','EUR','1397570537','10.69655','Hong Kong Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('31','HRK','EUR','1397570537','7.61903','Croatian Kuna');
INSERT INTO `bos_arfolyamok` VALUES ('32','HUF','EUR','1397570537','307.47','Hungarian Forint');
INSERT INTO `bos_arfolyamok` VALUES ('33','ILS','EUR','1397570537','4.7859','Israeli Sheqel');
INSERT INTO `bos_arfolyamok` VALUES ('34','INR','EUR','1397570538','83.2213','Indian Rupee');
INSERT INTO `bos_arfolyamok` VALUES ('35','ISK','EUR','1397570538','155.07','Icelandic Krona');
INSERT INTO `bos_arfolyamok` VALUES ('36','JMD','EUR','1397570538','151.1319','Jamaican Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('37','JOD','EUR','1397570538','0.97856','Jordanian Dinar');
INSERT INTO `bos_arfolyamok` VALUES ('38','JPY','EUR','1397570538','140.5998','Japanese Yen');
INSERT INTO `bos_arfolyamok` VALUES ('39','KES','EUR','1397570538','119.7324','Kenyan Shilling');
INSERT INTO `bos_arfolyamok` VALUES ('40','KWD','EUR','1397570538','0.3882','Kuwaiti Dinar');
INSERT INTO `bos_arfolyamok` VALUES ('41','LKR','EUR','1397570538','180.2541','Sri Lankan Rupee');
INSERT INTO `bos_arfolyamok` VALUES ('42','LTL','EUR','1397570538','3.4528','Lithuanian Litas');
INSERT INTO `bos_arfolyamok` VALUES ('43','LVL','EUR','1397570538','0.7028','Latvian Lats');
INSERT INTO `bos_arfolyamok` VALUES ('44','MAD','EUR','1397570538','11.224','Moroccan Dirham');
INSERT INTO `bos_arfolyamok` VALUES ('45','MDL','EUR','1397570538','18.1943','Moldovan Leu');
INSERT INTO `bos_arfolyamok` VALUES ('46','MKD','EUR','1397570538','61.2824','Macedonian Denar');
INSERT INTO `bos_arfolyamok` VALUES ('47','MUR','EUR','1397570538','41.7935','Mauritian Rupee');
INSERT INTO `bos_arfolyamok` VALUES ('48','MVR','EUR','1397570539','21.2556','Maldivian Rufiyaa');
INSERT INTO `bos_arfolyamok` VALUES ('49','MXN','EUR','1397570540','18.013','Mexican Peso');
INSERT INTO `bos_arfolyamok` VALUES ('50','MYR','EUR','1397570541','4.4774','Malaysian Ringgit');
INSERT INTO `bos_arfolyamok` VALUES ('51','NAD','EUR','1397570541','14.5042','Namibian Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('52','NGN','EUR','1397570541','223.3164','Nigerian Naira');
INSERT INTO `bos_arfolyamok` VALUES ('53','NOK','EUR','1397570541','8.2321','Norwegian Krone');
INSERT INTO `bos_arfolyamok` VALUES ('54','NPR','EUR','1397570542','133.15408','Nepalese Rupee');
INSERT INTO `bos_arfolyamok` VALUES ('55','NZD','EUR','1397570542','1.5948','New Zealand Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('56','OMR','EUR','1397570542','0.53068','Omani Rial');
INSERT INTO `bos_arfolyamok` VALUES ('57','PAB','EUR','1397570542','1.3802','Panamanian Balboa');
INSERT INTO `bos_arfolyamok` VALUES ('58','PEN','EUR','1397570542','3.8383','Peruvian Sol');
INSERT INTO `bos_arfolyamok` VALUES ('59','PHP','EUR','1397570542','61.3748','Philippine Peso');
INSERT INTO `bos_arfolyamok` VALUES ('60','PKR','EUR','1397570542','132.7752','Pakistani Rupee');
INSERT INTO `bos_arfolyamok` VALUES ('61','PLN','EUR','1397570542','4.1865','Polish Zloty');
INSERT INTO `bos_arfolyamok` VALUES ('62','QAR','EUR','1397570542','5.02393','Qatari Riyal');
INSERT INTO `bos_arfolyamok` VALUES ('63','RON','EUR','1397570542','4.4678','Romanian Leu');
INSERT INTO `bos_arfolyamok` VALUES ('64','RSD','EUR','1397570542','115.5113','Serbian Dinar');
INSERT INTO `bos_arfolyamok` VALUES ('65','RUB','EUR','1397570542','49.7271','Russian Rouble');
INSERT INTO `bos_arfolyamok` VALUES ('66','SAR','EUR','1397570542','5.17575','Saudi Riyal');
INSERT INTO `bos_arfolyamok` VALUES ('67','SCR','EUR','1397570542','16.9631','Seychellois Rupee');
INSERT INTO `bos_arfolyamok` VALUES ('68','SEK','EUR','1397570542','9.0875','Swedish Krona');
INSERT INTO `bos_arfolyamok` VALUES ('69','SGD','EUR','1397570542','1.7282','Singapore Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('70','SYP','EUR','1397570542','204.0677','Syrian Pound');
INSERT INTO `bos_arfolyamok` VALUES ('71','THB','EUR','1397570543','44.5045','Thai Baht');
INSERT INTO `bos_arfolyamok` VALUES ('72','TND','EUR','1397570543','2.1884','Tunisian Dinar');
INSERT INTO `bos_arfolyamok` VALUES ('73','TRY','EUR','1397570543','2.9374','Turkish Lira');
INSERT INTO `bos_arfolyamok` VALUES ('74','TWD','EUR','1397570543','41.6089','Taiwanese Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('75','UAH','EUR','1397570543','18.00182','Ukraine Hryvnia');
INSERT INTO `bos_arfolyamok` VALUES ('76','USD','EUR','1397570543','1.3802','United States Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('77','UYU','EUR','1397570543','31.3996','Uruguayan Peso');
INSERT INTO `bos_arfolyamok` VALUES ('78','VEF','EUR','1397570543','8.69526','Venezuelan Bolívar');
INSERT INTO `bos_arfolyamok` VALUES ('79','XAF','EUR','1397570543','655.957','Central African Franc');
INSERT INTO `bos_arfolyamok` VALUES ('80','XCD','EUR','1397570543','3.72654','East Caribbean Dollar');
INSERT INTO `bos_arfolyamok` VALUES ('81','XOF','EUR','1397570543','655.957','West African Franc');
INSERT INTO `bos_arfolyamok` VALUES ('82','XPF','EUR','1397570543','119.33174','CFP Franc');
INSERT INTO `bos_arfolyamok` VALUES ('83','ZAR','EUR','1397570543','14.5042','South African Rand');
-- --------------------------------------------------------

--
-- Table structure for table `bos_forgalmazok`
--

DROP TABLE IF EXISTS `bos_forgalmazok`;

CREATE TABLE `bos_forgalmazok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) NOT NULL,
  `kapcsolat_nev` varchar(255) NOT NULL,
  `kapcsolat_cim` text NOT NULL,
  `kapcsolat_elerhetoseg` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_forgalmazok`
--

-- --------------------------------------------------------

--
-- Table structure for table `bos_gyartok_hu`
--

DROP TABLE IF EXISTS `bos_gyartok_hu`;

CREATE TABLE `bos_gyartok_hu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  `logo` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_gyartok_hu`
--

INSERT INTO `bos_gyartok_hu` VALUES ('10','Diktas','','');
INSERT INTO `bos_gyartok_hu` VALUES ('11','JBG2','','');
INSERT INTO `bos_gyartok_hu` VALUES ('12','DGD Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('13','PastorKalt','','');
INSERT INTO `bos_gyartok_hu` VALUES ('14','Costan Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('15','-','','');
INSERT INTO `bos_gyartok_hu` VALUES ('16','Technodom Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('17','Zoyn Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('18','TecFrigo Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('19','Iarp Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('20','DeRigo Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('21','EuroCryor Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('22','B-Store','','');
INSERT INTO `bos_gyartok_hu` VALUES ('23','TechnoBlock Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('24','Isopan Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('25','MIV Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('26','Cisaplast Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('27','Gondella','','');
INSERT INTO `bos_gyartok_hu` VALUES ('28','Tasam','','');
INSERT INTO `bos_gyartok_hu` VALUES ('29','Somcelik','','');
INSERT INTO `bos_gyartok_hu` VALUES ('30','Euroceppi Italy','','');
INSERT INTO `bos_gyartok_hu` VALUES ('31','Minipool','','');
INSERT INTO `bos_gyartok_hu` VALUES ('32','Sirman','','');
INSERT INTO `bos_gyartok_hu` VALUES ('33','u n i s','','');
INSERT INTO `bos_gyartok_hu` VALUES ('34','UNOX','','');
-- --------------------------------------------------------

--
-- Table structure for table `bos_kategoriak_en`
--

DROP TABLE IF EXISTS `bos_kategoriak_en`;

CREATE TABLE `bos_kategoriak_en` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `szulo` int(11) NOT NULL,
  `nev` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  `kep` varchar(255) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_kategoriak_en`
--

INSERT INTO `bos_kategoriak_en` VALUES ('21','0','Hűtött berendezések','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('22','21','Hűtőpultok telepített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('23','21','Hűtőpultok beépített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('24','21','Cukrász-szendvics hűtők','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('25','21','Falihűtők beépített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('26','21','Alacsony falihűtők beépített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('27','21','Falihűtők telepített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('28','21','Csemege hátfal','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('29','21','Hűtőszekrények','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('30','21','Hűtött asztalok','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('31','21','Hűtősziget','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('32','21','Saláta - hidegkonyha hűtők','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('33','21','Mirelithűtők','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('34','21','Sokkoló hűtő','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('35','21','Egyedi aggregátok','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('36','21','Csoport aggregátok','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('37','21','Hűtőegységek','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('38','37','Tető blokkaggregát','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('39','37','Oldalfali blokkaggregát','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('40','37','Split hűtőegységek','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('41','0','Hűtőkamrák és részei','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('42','41','Hűtőkamrák','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('43','41','Ajtó - panel','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('44','0','Hűtetlen berendezések','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('45','44','Áruházi polcrendszerek','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('46','44','Pénztárpultok','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('47','44','Vevőterelő rendszerek','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('48','44','Egyéb termékek','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('49','44','Kenyér-pékárú-zöldség-asztalosipari állványok','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('50','44','Kisgépek','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('51','44','Melegentartók','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('52','44','Sütők-kelesztők','','','0');
INSERT INTO `bos_kategoriak_en` VALUES ('53','44','Raktári polcrendszerek','','','0');
-- --------------------------------------------------------

--
-- Table structure for table `bos_kategoriak_hu`
--

DROP TABLE IF EXISTS `bos_kategoriak_hu`;

CREATE TABLE `bos_kategoriak_hu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `szulo` int(11) NOT NULL,
  `nev` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  `kep` varchar(255) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_kategoriak_hu`
--

INSERT INTO `bos_kategoriak_hu` VALUES ('21','0','Hűtött berendezések','<div id=\"mw-content-text\" lang=\"hu\" dir=\"ltr\" class=\"mw-content-ltr\">\r\n<p>A <b>hűtőgép</b> olyan szerkezet, mellyel mesterségesen a környezetnél alacsonyabb <a href=\"/wiki/H%C5%91m%C3%A9rs%C3%A9klet\" title=\"Hőmérséklet\">hőmérsékletet</a> lehet előállítani és tartósan fenntartani.</p>\r\n<p>Hűtést természetes vagy mesterséges eljárással lehet végezni. Természetes mód az, ha valamilyen hideg tárgyat hozunk kapcsolatba a hűtendő hellyel. Évszázadok óta használatos a vízjéggel való hűtés. Télen képződött jeget (folyóból vagy tóból kivágva) jól szigetelt árnyékos tárolókba, jégvermekbe rakták, itt még a forró nyári napokra is megmaradt annyi jég, hogy fel lehetett használni frissítők készítéséhez.</p>\r\n<p>Mesterséges hűtésre a hűtőgépek (hűtőberendezések) szolgálnak.</p>\r\n<p></p>\r\n\r\n\r\n</div>','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('22','21','Hűtőpultok telepített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('23','21','Hűtőpultok beépített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('24','21','Cukrász-szendvics hűtők','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('25','21','Falihűtők beépített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('26','21','Alacsony falihűtők beépített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('27','21','Falihűtők telepített aggregáttal','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('28','21','Csemege hátfal','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('29','21','Hűtőszekrények','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('30','21','Hűtött asztalok','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('31','21','Hűtősziget','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('32','21','Saláta - hidegkonyha hűtők','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('33','21','Mirelithűtők','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('34','21','Sokkoló hűtő','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('35','21','Egyedi aggregátok','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('36','21','Csoport aggregátok','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('37','21','Hűtőegységek','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('38','37','Tető blokkaggregát','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('39','37','Oldalfali blokkaggregát','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('40','37','Split hűtőegységek','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('41','0','Hűtőkamrák és részei','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('42','41','Hűtőkamrák','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('43','41','Ajtó - panel','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('44','0','Hűtetlen berendezések','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('45','44','Áruházi polcrendszerek','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('46','44','Pénztárpultok','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('47','44','Vevőterelő rendszerek','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('48','44','Egyéb termékek','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('49','44','Kenyér-pékárú-zöldség-asztalosipari állványok','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('50','44','Kisgépek','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('51','44','Melegentartók','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('52','44','Sütők-kelesztők','','','0');
INSERT INTO `bos_kategoriak_hu` VALUES ('53','44','Raktári polcrendszerek','','','0');
-- --------------------------------------------------------

--
-- Table structure for table `bos_konf`
--

DROP TABLE IF EXISTS `bos_konf`;

CREATE TABLE `bos_konf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kulcs` varchar(255) NOT NULL,
  `ertek` text NOT NULL,
  `leiras` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_konf`
--

INSERT INTO `bos_konf` VALUES ('1','Kategóriák.ElemPerSor','3','');
INSERT INTO `bos_konf` VALUES ('2','Kategóriák.termekSzamLatszik','1','');
INSERT INTO `bos_konf` VALUES ('3','Kategóriák.Terméksor','1','');
INSERT INTO `bos_konf` VALUES ('4','Termékek.TerméklistaLapozó','1','');
INSERT INTO `bos_konf` VALUES ('5','Termékek.TerméklistaLapozóTermékszám','12','');
INSERT INTO `bos_konf` VALUES ('6','Termékek.TerméklistaLapozóUrlTag','page-','');
INSERT INTO `bos_konf` VALUES ('7','Áruház.AlapUrl','online-store','');
INSERT INTO `bos_konf` VALUES ('8','Termékek.TermékPerSor','3','');
INSERT INTO `bos_konf` VALUES ('9','Termékek.termekArLatszik','1','');
INSERT INTO `bos_konf` VALUES ('10','Termékek.termekArCsoportok','','');
INSERT INTO `bos_konf` VALUES ('11','Termékek.termekArNetto','1','');
INSERT INTO `bos_konf` VALUES ('12','Termékek.termekArBrutto','1','');
INSERT INTO `bos_konf` VALUES ('29','Áruház.AlapértelmezettPénznem','EUR','');
INSERT INTO `bos_konf` VALUES ('30','Termékek.termekKészletLátszik','0','');
INSERT INTO `bos_konf` VALUES ('31','Termékek.termékkártyaGyorsnézet','0','');
INSERT INTO `bos_konf` VALUES ('32','Termékek.terméklapKosarlatszik','1','');
INSERT INTO `bos_konf` VALUES ('33','Termékek.terméklapKosarTobbetishozzaadhat','1','');
INSERT INTO `bos_konf` VALUES ('34','Termékek.terméklapKosarKeszletszamit','0','');
INSERT INTO `bos_konf` VALUES ('35','Termékek.termeklapNincsRaktaronErdeklodhet','1','');
INSERT INTO `bos_konf` VALUES ('36','Aruhaz.EladóEmailCím','ivan@zente.org','');
INSERT INTO `bos_konf` VALUES ('37','Áruház.KosárUri','cart','');
-- --------------------------------------------------------

--
-- Table structure for table `bos_opciok_hu`
--

DROP TABLE IF EXISTS `bos_opciok_hu`;

CREATE TABLE `bos_opciok_hu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termek_id` int(11) NOT NULL,
  `nev` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  `kep` varchar(255) NOT NULL,
  `ar` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1741 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_opciok_hu`
--

INSERT INTO `bos_opciok_hu` VALUES ('4','29','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('14','77','Automata cseppvíz elpárologtató','','','25900','0');
INSERT INTO `bos_opciok_hu` VALUES ('19','325','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('20','325','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('21','325','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('22','126','Szín felár 120','','','4990','0');
INSERT INTO `bos_opciok_hu` VALUES ('23','126','Rozsdamentes belső burkolatok 120','','','76725','0');
INSERT INTO `bos_opciok_hu` VALUES ('24','128','Rozsdamentes belső burkolatok 120','','','91600','0');
INSERT INTO `bos_opciok_hu` VALUES ('25','128','Szín felár 120','','','7485','0');
INSERT INTO `bos_opciok_hu` VALUES ('26','127','Rozsdamentes belső burkolatok 150','','','84150','0');
INSERT INTO `bos_opciok_hu` VALUES ('27','127','Szín felár 150','','','6250','0');
INSERT INTO `bos_opciok_hu` VALUES ('28','76','Automata cseppvíz elpárologtató','','','25900','0');
INSERT INTO `bos_opciok_hu` VALUES ('29','212','Sziget kitt','','','18740','0');
INSERT INTO `bos_opciok_hu` VALUES ('30','212','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('37','254','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('38','285','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('39','285','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('40','287','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('41','286','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('42','286','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('43','286','10 fm csőkészlet','','','41400','0');
INSERT INTO `bos_opciok_hu` VALUES ('44','7','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('45','165','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('46','167','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('63','323','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('64','323','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('65','323','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('66','326','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('67','326','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('68','326','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('69','327','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('70','327','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('71','327','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('72','329','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('73','329','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('74','329','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('75','328','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('76','328','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('77','328','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('81','174','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('85','237','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('86','236','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('90','235','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('91','234','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('95','40','Rozsdamentes belső ','','','128700','0');
INSERT INTO `bos_opciok_hu` VALUES ('96','40','Plexi tolóajtó','','','74745','0');
INSERT INTO `bos_opciok_hu` VALUES ('97','40','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('98','40','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('99','40','Köztes üvegpolc','','','23265','0');
INSERT INTO `bos_opciok_hu` VALUES ('100','39','Rozsdamentes belső ','','','119050','0');
INSERT INTO `bos_opciok_hu` VALUES ('101','39','Plexi tolóajtó','','','69300','0');
INSERT INTO `bos_opciok_hu` VALUES ('102','39','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('103','39','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('104','39','Köztes üvegpolc','','','20800','0');
INSERT INTO `bos_opciok_hu` VALUES ('105','38','Rozsdamentes belső ','','','95500','0');
INSERT INTO `bos_opciok_hu` VALUES ('106','38','Plexi tolóajtó','','','60900','0');
INSERT INTO `bos_opciok_hu` VALUES ('107','38','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('108','38','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('109','38','Köztes üvegpolc','','','18315','0');
INSERT INTO `bos_opciok_hu` VALUES ('110','37','Rozsdamentes belső ','','','74250','0');
INSERT INTO `bos_opciok_hu` VALUES ('111','37','Plexi tolóajtó','','','49995','0');
INSERT INTO `bos_opciok_hu` VALUES ('112','37','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('113','37','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('114','37','Köztes üvegpolc','','','16830','0');
INSERT INTO `bos_opciok_hu` VALUES ('115','36','Rozsdamentes belső ','','','64350','0');
INSERT INTO `bos_opciok_hu` VALUES ('116','36','Plexi tolóajtó','','','46035','0');
INSERT INTO `bos_opciok_hu` VALUES ('117','36','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('118','36','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('119','36','Köztes üvegpolc','','','16830','0');
INSERT INTO `bos_opciok_hu` VALUES ('120','35','Rozsdamentes belső ','','','48265','0');
INSERT INTO `bos_opciok_hu` VALUES ('121','35','Plexi tolóajtó','','','29700','0');
INSERT INTO `bos_opciok_hu` VALUES ('122','35','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('123','35','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('124','35','Köztes üvegpolc','','','12870','0');
INSERT INTO `bos_opciok_hu` VALUES ('126','49','ABS végelem','','','56000','0');
INSERT INTO `bos_opciok_hu` VALUES ('130','41','Rozsdamentes belső ','','','32175','0');
INSERT INTO `bos_opciok_hu` VALUES ('131','41','Plexi tolóajtó','','','27225','0');
INSERT INTO `bos_opciok_hu` VALUES ('132','41','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('133','41','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('134','41','Köztes üvegpolc','','','27225','0');
INSERT INTO `bos_opciok_hu` VALUES ('135','42','Rozsdamentes belső ','','','48265','0');
INSERT INTO `bos_opciok_hu` VALUES ('136','42','Plexi tolóajtó','','','29700','0');
INSERT INTO `bos_opciok_hu` VALUES ('137','42','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('138','42','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('139','42','Köztes üvegpolc','','','12870','0');
INSERT INTO `bos_opciok_hu` VALUES ('140','43','Rozsdamentes belső ','','','64350','0');
INSERT INTO `bos_opciok_hu` VALUES ('141','43','Plexi tolóajtó','','','46035','0');
INSERT INTO `bos_opciok_hu` VALUES ('142','43','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('143','43','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('144','43','Köztes üvegpolc','','','16830','0');
INSERT INTO `bos_opciok_hu` VALUES ('145','44','Rozsdamentes belső ','','','74250','0');
INSERT INTO `bos_opciok_hu` VALUES ('146','44','Plexi tolóajtó','','','49995','0');
INSERT INTO `bos_opciok_hu` VALUES ('147','44','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('148','44','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('149','44','Köztes üvegpolc','','','16830','0');
INSERT INTO `bos_opciok_hu` VALUES ('150','45','Rozsdamentes belső ','','','95500','0');
INSERT INTO `bos_opciok_hu` VALUES ('151','45','Plexi tolóajtó','','','60900','0');
INSERT INTO `bos_opciok_hu` VALUES ('152','45','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('153','45','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('154','45','Köztes üvegpolc','','','18315','0');
INSERT INTO `bos_opciok_hu` VALUES ('155','46','Rozsdamentes belső ','','','119050','0');
INSERT INTO `bos_opciok_hu` VALUES ('156','46','Plexi tolóajtó','','','69300','0');
INSERT INTO `bos_opciok_hu` VALUES ('157','46','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('158','46','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('159','46','Köztes üvegpolc','','','20800','0');
INSERT INTO `bos_opciok_hu` VALUES ('160','47','Rozsdamentes belső ','','','128700','0');
INSERT INTO `bos_opciok_hu` VALUES ('161','47','Plexi tolóajtó','','','74745','0');
INSERT INTO `bos_opciok_hu` VALUES ('162','47','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('163','47','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('164','47','Köztes üvegpolc','','','23265','0');
INSERT INTO `bos_opciok_hu` VALUES ('165','129','Szín felár 120','','','4990','0');
INSERT INTO `bos_opciok_hu` VALUES ('166','129','Rozsdamentes belső burkolatok 120','','','76725','0');
INSERT INTO `bos_opciok_hu` VALUES ('167','130','Szín felár 150','','','6250','0');
INSERT INTO `bos_opciok_hu` VALUES ('168','130','Rozsdamentes belső burkolatok 150','','','84150','0');
INSERT INTO `bos_opciok_hu` VALUES ('169','131','Szín felár 180','','','7485','0');
INSERT INTO `bos_opciok_hu` VALUES ('170','131','Rozsdamentes belső burkolatok 180','','','91575','0');
INSERT INTO `bos_opciok_hu` VALUES ('173','78','Automata cseppvíz elpárologtató','','','25900','0');
INSERT INTO `bos_opciok_hu` VALUES ('175','208','Kosárkészlet','','','31950','0');
INSERT INTO `bos_opciok_hu` VALUES ('176','287','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('177','287','10 fm csőkészlet','','','41400','0');
INSERT INTO `bos_opciok_hu` VALUES ('178','288','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('179','288','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('180','288','10 fm csőkészlet','','','56100','0');
INSERT INTO `bos_opciok_hu` VALUES ('181','289','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('182','289','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('183','289','10 fm csőkészlet','','','56100','0');
INSERT INTO `bos_opciok_hu` VALUES ('184','290','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('185','290','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('186','290','10 fm csőkészlet','','','56100','0');
INSERT INTO `bos_opciok_hu` VALUES ('187','291','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('188','291','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('189','291','10 fm csőkészlet','','','65000','0');
INSERT INTO `bos_opciok_hu` VALUES ('190','292','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('191','292','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('192','292','10 fm csőkészlet','','','65000','0');
INSERT INTO `bos_opciok_hu` VALUES ('193','254','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('194','255','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('195','255','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('196','256','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('197','256','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('198','257','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('199','257','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('200','258','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('201','258','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('202','260','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('203','260','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('204','259','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('205','259','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('206','261','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('207','261','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('208','262','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('209','262','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('210','209','Elektronikus vez.-  aut. leolvasztás','','','116700','0');
INSERT INTO `bos_opciok_hu` VALUES ('211','209','Rács árukiemelő szett','','','11300','0');
INSERT INTO `bos_opciok_hu` VALUES ('212','210','Rács árukiemelő szett','','','13300','0');
INSERT INTO `bos_opciok_hu` VALUES ('213','210','Elektronikus vez.-  aut. leolvasztás','','','116700','0');
INSERT INTO `bos_opciok_hu` VALUES ('214','211','Elektronikus vez.-  aut. leolvasztás','','','116700','0');
INSERT INTO `bos_opciok_hu` VALUES ('215','211','Rács árukiemelő szett','','','11200','0');
INSERT INTO `bos_opciok_hu` VALUES ('216','203','Színes tetőkeret','','','5000','0');
INSERT INTO `bos_opciok_hu` VALUES ('217','204','Színes tetőkeret','','','5000','0');
INSERT INTO `bos_opciok_hu` VALUES ('218','205','Színes tetőkeret','','','5000','0');
INSERT INTO `bos_opciok_hu` VALUES ('220','181','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('223','23','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('224','23','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('225','29','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('226','24','Ventillációs hűtés','','','46000','0');
INSERT INTO `bos_opciok_hu` VALUES ('227','24','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('228','24','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('229','24','Köztes üvegpolc','','','14400','0');
INSERT INTO `bos_opciok_hu` VALUES ('230','25','Ventillációs hűtés','','','44650','0');
INSERT INTO `bos_opciok_hu` VALUES ('231','25','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('232','25','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('233','25','Köztes üvegpolc','','','16400','0');
INSERT INTO `bos_opciok_hu` VALUES ('234','26','Ventillációs hűtés','','','44650','0');
INSERT INTO `bos_opciok_hu` VALUES ('235','26','Mobil plexi osztó','','','19300','0');
INSERT INTO `bos_opciok_hu` VALUES ('236','26','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('237','26','Köztes üvegpolc','','','16400','0');
INSERT INTO `bos_opciok_hu` VALUES ('238','27','Ventillációs hűtés','','','53200','0');
INSERT INTO `bos_opciok_hu` VALUES ('239','27','Mobil plexi osztó','','','19300','0');
INSERT INTO `bos_opciok_hu` VALUES ('240','27','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('241','27','Köztes üvegpolc','','','22800','0');
INSERT INTO `bos_opciok_hu` VALUES ('242','28','Ventillációs hűtés','','','63600','0');
INSERT INTO `bos_opciok_hu` VALUES ('243','28','Mobil plexi osztó','','','19300','0');
INSERT INTO `bos_opciok_hu` VALUES ('244','28','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('245','28','Köztes üvegpolc','','','28800','0');
INSERT INTO `bos_opciok_hu` VALUES ('247','7','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('248','4','Végelem','','','57400','0');
INSERT INTO `bos_opciok_hu` VALUES ('249','337','Krómozott oszlop 1080 mm','','','11200','0');
INSERT INTO `bos_opciok_hu` VALUES ('250','337','Műanyag karmantyú','','','490','0');
INSERT INTO `bos_opciok_hu` VALUES ('251','337','Korlátcső 1 fm','','','5800','0');
INSERT INTO `bos_opciok_hu` VALUES ('252','102','Panoráma METAL végelem ','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('253','107','Üveg nyílóajtó','','','517500','0');
INSERT INTO `bos_opciok_hu` VALUES ('254','102','Tükrös METAL végelem','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('255','4','Mobil osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('256','103','Panoráma METAL végelem','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('257','103','Szigetelt Alu üveg nyílóajtó','','','224250','0');
INSERT INTO `bos_opciok_hu` VALUES ('258','103','Tükrös METAL végelem','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('259','98','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('260','98','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('261','98','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('262','98','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('267','104','Üveg nyílóajtó','','','250500','0');
INSERT INTO `bos_opciok_hu` VALUES ('268','106','Panoráma METAL végelem','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('269','106','Szigetelt Alu üveg nyílóajtó','','','545100','0');
INSERT INTO `bos_opciok_hu` VALUES ('270','106','Tükrös METAL végelem','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('271','103','Üveg nyílóajtó','','','172500','0');
INSERT INTO `bos_opciok_hu` VALUES ('272','107','Panoráma METAL végelem','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('273','107','Szigetelt Alu üveg nyílóajtó','','','672750','0');
INSERT INTO `bos_opciok_hu` VALUES ('274','107','Tükrös METAL végelem','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('275','108','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('276','108','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('277','108','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('278','108','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('279','109','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('280','109','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('281','109','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('282','109','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('283','110','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('284','110','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('285','110','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('286','110','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('288','257','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('290','256','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('292','302','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('294','301','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('295','123','Panoráma végelem','','','91500','0');
INSERT INTO `bos_opciok_hu` VALUES ('296','259','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('297','123','Tükrös végelem','','','91500','0');
INSERT INTO `bos_opciok_hu` VALUES ('298','258','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('311','60','Laminált színfelár','','','11400','0');
INSERT INTO `bos_opciok_hu` VALUES ('312','60','Rozsdamentes belső','','','43000','0');
INSERT INTO `bos_opciok_hu` VALUES ('313','60','Alumínium felépítmény','','','15840','0');
INSERT INTO `bos_opciok_hu` VALUES ('314','60','Festett belső és kölső fémburkolat','','','22000','0');
INSERT INTO `bos_opciok_hu` VALUES ('315','60','Chrome kocsiütköző','','','18800','0');
INSERT INTO `bos_opciok_hu` VALUES ('316','60','Külső sarokelem 45° hűtetlen','','','311000','0');
INSERT INTO `bos_opciok_hu` VALUES ('317','60','Belső sarokelem 45° hűtetlen','','','290400','0');
INSERT INTO `bos_opciok_hu` VALUES ('318','60','Átadó elem 1 fm.','','','191665','0');
INSERT INTO `bos_opciok_hu` VALUES ('319','63','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('320','62','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('321','62','Átadó elem 1 fm.','','','250500','0');
INSERT INTO `bos_opciok_hu` VALUES ('322','62','Átadó elem 0,7 fm.','','','179200','0');
INSERT INTO `bos_opciok_hu` VALUES ('323','62','Plexi tolóajtó','','','33660','0');
INSERT INTO `bos_opciok_hu` VALUES ('327','263','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('328','263','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('329','264','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('330','264','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('331','265','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('332','265','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('333','266','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('334','266','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('335','267','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('336','267','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('337','268','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('338','268','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('341','269','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('342','269','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('343','293','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('344','293','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('345','294','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('346','294','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('347','295','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('348','295','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('349','285','10 fm csőkészlet','','','41400','0');
INSERT INTO `bos_opciok_hu` VALUES ('350','293','10 fm csőkészlet','','','41400','0');
INSERT INTO `bos_opciok_hu` VALUES ('351','295','10 fm csőkészlet','','','56000','0');
INSERT INTO `bos_opciok_hu` VALUES ('352','296','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('353','296','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('354','296','10 fm csőkészlet','','','56000','0');
INSERT INTO `bos_opciok_hu` VALUES ('355','297','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('356','297','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('357','297','10 fm csőkészlet','','','56000','0');
INSERT INTO `bos_opciok_hu` VALUES ('358','298','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('359','298','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('360','298','10 fm csőkészlet','','','65000','0');
INSERT INTO `bos_opciok_hu` VALUES ('361','299','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('362','299','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('363','299','10 fm csőkészlet','','','65000','0');
INSERT INTO `bos_opciok_hu` VALUES ('364','270','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('365','270','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('366','271','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('367','271','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('368','272','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('369','272','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('370','273','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('371','273','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('372','274','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('373','274','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('374','275','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('375','275','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('376','276','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('377','276','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('378','277','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('379','277','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('382','278','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('383','278','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('384','279','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('385','279','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('386','280','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('387','280','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('388','281','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('389','281','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('390','282','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('391','282','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('392','283','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('393','283','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('394','284','Karterfűtés','','','19255','0');
INSERT INTO `bos_opciok_hu` VALUES ('395','284','Presztosztát','','','24650','0');
INSERT INTO `bos_opciok_hu` VALUES ('396','143','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('397','144','Ajtózár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('398','140','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('399','146','Ajtózár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('400','137','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('401','152','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('402','153','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('403','154','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('404','155','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('405','156','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('406','157','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('407','158','Ajtózár','','','12400','0');
INSERT INTO `bos_opciok_hu` VALUES ('408','215','Kosárkészlet','','','19450','0');
INSERT INTO `bos_opciok_hu` VALUES ('409','216','Kosárkészlet','','','24550','0');
INSERT INTO `bos_opciok_hu` VALUES ('410','217','Kosárkészlet','','','25600','0');
INSERT INTO `bos_opciok_hu` VALUES ('426','51','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('427','51','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('428','51','Végelem bal','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('429','51','Végelem jobb','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('454','2','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('455','2','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('456','2','Végelem bal','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('457','2','Végelem jobb','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('483','3','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('484','3','Mobil osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('485','3','Tuna belső sarokelem 90°','','','1060000','0');
INSERT INTO `bos_opciok_hu` VALUES ('486','3','Végelem','','','57400','0');
INSERT INTO `bos_opciok_hu` VALUES ('487','3','Árutéri lépcső 1000 mm','','','22200','0');
INSERT INTO `bos_opciok_hu` VALUES ('488','111','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('489','111','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('490','111','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('491','111','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('492','112','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('493','112','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('494','112','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('495','112','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('496','113','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('497','113','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('498','113','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('499','113','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('500','114','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('501','114','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('502','114','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('503','114','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('504','115','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('505','115','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('506','115','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('507','115','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('508','116','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('509','116','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('510','116','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('511','116','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('541','6','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('542','6','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('543','6','Végelem bal','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('544','6','Végelem jobb','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('545','6','Köztes üvegpolc 950','','','27300','0');
INSERT INTO `bos_opciok_hu` VALUES ('546','6','Köztes üvegpolc 1250','','','37700','0');
INSERT INTO `bos_opciok_hu` VALUES ('581','52','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('582','52','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('583','52','Végelem bal','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('584','52','Végelem jobb','','','43100','0');
INSERT INTO `bos_opciok_hu` VALUES ('585','52','Köztes üvegpolc 950','','','27300','0');
INSERT INTO `bos_opciok_hu` VALUES ('586','52','Köztes üvegpolc 1250','','','37700','0');
INSERT INTO `bos_opciok_hu` VALUES ('587','52','Fix plexi osztó','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('588','360','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('589','77','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('591','7','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('597','23','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('598','24','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('599','25','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('600','26','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('601','27','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('602','28','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('604','35','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('605','36','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('606','38','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('607','39','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('608','40','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('609','41','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('610','42','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('611','43','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('612','44','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('613','45','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('614','46','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('615','47','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('617','60','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('618','79','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('619','80','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('620','81','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('621','82','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('622','76','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('623','78','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('624','83','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('625','84','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('626','85','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('627','94','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('628','95','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('629','126','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('630','127','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('631','128','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('632','129','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('633','130','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('634','131','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('635','143','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('636','144','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('637','145','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('638','146','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('639','151','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('640','152','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('641','153','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('642','154','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('643','155','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('644','156','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('645','157','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('646','158','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('647','203','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('648','204','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('649','205','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('654','209','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('655','210','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('656','211','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('657','215','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('658','218','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('659','216','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('660','217','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('664','223','Aggregát nélküli kivitel','','','-149000','0');
INSERT INTO `bos_opciok_hu` VALUES ('665','37','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('666','208','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('668','355','nyilóajtók 1 fm','','','10800','0');
INSERT INTO `bos_opciok_hu` VALUES ('670','355','csemegefogas','','','17400','0');
INSERT INTO `bos_opciok_hu` VALUES ('671','355','1 med. mosogató','','','16800','0');
INSERT INTO `bos_opciok_hu` VALUES ('672','355','felső polc','','','5700','0');
INSERT INTO `bos_opciok_hu` VALUES ('673','160','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('674','160','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('675','161','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('676','161','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('677','162','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('678','162','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('679','163','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('680','163','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('681','165','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('682','165','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('683','165','Rozdamentes munkapult hátsó peremmel','','','42175','0');
INSERT INTO `bos_opciok_hu` VALUES ('684','165','Rozsdamentes munkapult','','','14260','0');
INSERT INTO `bos_opciok_hu` VALUES ('685','166','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('686','168','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('687','166','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('688','166','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('689','166','Rozdamentes munkapult hátsó peremmel','','','50500','0');
INSERT INTO `bos_opciok_hu` VALUES ('690','166','Rozsdamentes munkapult','','','16450','0');
INSERT INTO `bos_opciok_hu` VALUES ('691','172','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('692','174','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('693','241','Szerelési díj','','','100000','0');
INSERT INTO `bos_opciok_hu` VALUES ('695','172','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('696','172','Rozdamentes munkapult hátsó peremmel','','','3500','0');
INSERT INTO `bos_opciok_hu` VALUES ('697','172','Gránit munkapult','','','6000','0');
INSERT INTO `bos_opciok_hu` VALUES ('698','321','Szállítási díj','','','165','0');
INSERT INTO `bos_opciok_hu` VALUES ('703','176','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('704','176','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('705','179','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('706','179','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('707','173','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('708','173','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('709','173','Rozdamentes munkapult hátsó peremmel','','','3950','0');
INSERT INTO `bos_opciok_hu` VALUES ('710','173','Gránit munkapult','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('711','175','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('712','175','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('713','177','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('714','177','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('715','178','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('716','178','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('718','180','1 ajtó helyett 2 fiók','','','57420','0');
INSERT INTO `bos_opciok_hu` VALUES ('719','180','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('725','169','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('726','169','Beépített mosogató csapteleppel','','','94600','0');
INSERT INTO `bos_opciok_hu` VALUES ('727','169','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('728','169','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('729','169','Rozdamentes munkapult hátsó peremmel','','','32000','0');
INSERT INTO `bos_opciok_hu` VALUES ('730','169','Rozsdamentes munkapult','','','10300','0');
INSERT INTO `bos_opciok_hu` VALUES ('731','170','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('732','170','Beépített mosogató csapteleppel','','','94600','0');
INSERT INTO `bos_opciok_hu` VALUES ('733','170','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('734','170','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('735','170','Rozdamentes munkapult hátsó peremmel','','','42175','0');
INSERT INTO `bos_opciok_hu` VALUES ('736','170','Rozsdamentes munkapult','','','14260','0');
INSERT INTO `bos_opciok_hu` VALUES ('737','171','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('738','171','Beépített mosogató csapteleppel','','','94600','0');
INSERT INTO `bos_opciok_hu` VALUES ('739','171','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('740','171','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('741','171','Rozdamentes munkapult hátsó peremmel','','','50500','0');
INSERT INTO `bos_opciok_hu` VALUES ('742','171','Rozsdamentes munkapult','','','16450','0');
INSERT INTO `bos_opciok_hu` VALUES ('743','245','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('744','244','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('745','246','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('746','247','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('747','248','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('748','249','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('749','250','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('750','251','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('751','252','Légkondenzátor','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('752','225','Metal végelem bal','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('753','225','Metal végelem jobb','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('754','226','Metal végelem bal','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('755','226','Metal végelem jobb','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('756','227','Metal végelem bal','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('757','227','Metal végelem jobb','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('758','228','Metal végelem bal','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('759','228','Metal végelem jobb','','','82100','0');
INSERT INTO `bos_opciok_hu` VALUES ('761','337','Kocsiáttoló','','','59000','0');
INSERT INTO `bos_opciok_hu` VALUES ('762','379','Vitafilm 1500x350x0,013','','','16430','0');
INSERT INTO `bos_opciok_hu` VALUES ('763','29','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('764','30','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('765','30','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('766','30','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('769','67','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('770','67','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('771','68','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('772','68','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('773','69','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('774','69','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('775','70','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('776','70','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('777','71','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('778','71','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('779','72','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('780','72','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('781','73','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('782','73','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('784','74','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('785','74','Elektronikus cseppvíz elpárologtató','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('790','75','Automata cseppvíz elpárologtató','','','25900','0');
INSERT INTO `bos_opciok_hu` VALUES ('791','75','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('792','182','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('793','336','Krómozott oszlop 1080','','','11200','0');
INSERT INTO `bos_opciok_hu` VALUES ('794','336','Műanyag karmantyú','','','490','0');
INSERT INTO `bos_opciok_hu` VALUES ('795','336','Korlátcső krómozott','','','5800','0');
INSERT INTO `bos_opciok_hu` VALUES ('796','340','Végoszlop 3 tartófüllel','','','22100','0');
INSERT INTO `bos_opciok_hu` VALUES ('797','340','Középoszlop 6 tartófüllel','','','25500','0');
INSERT INTO `bos_opciok_hu` VALUES ('798','340','Korlátcső krómozott','','','5800','0');
INSERT INTO `bos_opciok_hu` VALUES ('799','340','Krómozott oszlop 1080','','','11200','0');
INSERT INTO `bos_opciok_hu` VALUES ('800','340','Műanyag karmantyú','','','490','0');
INSERT INTO `bos_opciok_hu` VALUES ('804','285','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('805','286','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('806','287','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('807','288','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('808','289','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('809','290','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('810','291','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('811','292','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('812','293','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('813','294','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('814','295','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('815','296','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('816','297','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('817','298','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('818','299','Szerelési díj','','','50000','0');
INSERT INTO `bos_opciok_hu` VALUES ('819','294','10 fm csőkészlet','','','65000','0');
INSERT INTO `bos_opciok_hu` VALUES ('820','390','Szerelési díj','','','3300','0');
INSERT INTO `bos_opciok_hu` VALUES ('830','355','bútorkeret láb','','','22935','0');
INSERT INTO `bos_opciok_hu` VALUES ('831','355','bútorkeret tető','','','12870','0');
INSERT INTO `bos_opciok_hu` VALUES ('832','355','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('834','356','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('836','358','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('837','359','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('840','362','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('841','364','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('842','354','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('843','358','Felnyílótetős fiók 600 mm','','','12210','0');
INSERT INTO `bos_opciok_hu` VALUES ('846','358','Leheletvédős rekesz 600 mm','','','7840','0');
INSERT INTO `bos_opciok_hu` VALUES ('848','324','Scannervédő plexi','','','24990','0');
INSERT INTO `bos_opciok_hu` VALUES ('850','358','bútorkeret láb','','','27225','0');
INSERT INTO `bos_opciok_hu` VALUES ('852','324','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('854','358','bútorkeret tető','','','16100','0');
INSERT INTO `bos_opciok_hu` VALUES ('856','242','Légkondenzátor Alfa Laval','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('857','102','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('858','103','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('859','98','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('861','106','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('862','107','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('864','71','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('865','159','Special rácspolc (5 sor)','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('866','73','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('867','74','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('868','63','Átadó elem 1 fm.','','','250500','0');
INSERT INTO `bos_opciok_hu` VALUES ('869','63','Átadó elem 0,7 fm.','','','179200','0');
INSERT INTO `bos_opciok_hu` VALUES ('870','63','Plexi tolóajtó','','','38150','0');
INSERT INTO `bos_opciok_hu` VALUES ('871','64','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('872','64','Átadó elem 1 fm.','','','250500','0');
INSERT INTO `bos_opciok_hu` VALUES ('873','64','Átadó elem 0,7 fm.','','','179200','0');
INSERT INTO `bos_opciok_hu` VALUES ('874','64','Plexi tolóajtó','','','40000','0');
INSERT INTO `bos_opciok_hu` VALUES ('875','68','Üveg tolóajtó','','','189000','0');
INSERT INTO `bos_opciok_hu` VALUES ('876','70','Üveg tolóajtó','','','378000','0');
INSERT INTO `bos_opciok_hu` VALUES ('877','69','Üveg tolóajtó','','','270000','0');
INSERT INTO `bos_opciok_hu` VALUES ('878','76','Üveg tolóajtó','','','189000','0');
INSERT INTO `bos_opciok_hu` VALUES ('879','78','Üveg tolóajtó','','','378000','0');
INSERT INTO `bos_opciok_hu` VALUES ('880','77','Üveg tolóajtó','','','270000','0');
INSERT INTO `bos_opciok_hu` VALUES ('886','320','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('897','238','Fali tartókonzok','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('898','238','Karterfűtés','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('899','238','Presztosztát','','','11500','0');
INSERT INTO `bos_opciok_hu` VALUES ('900','238','Villamos kapcsolószekrény 3F esetén','','','58000','0');
INSERT INTO `bos_opciok_hu` VALUES ('901','238','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('912','239','Fali tartókonzok','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('913','239','Karterfűtés','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('914','239','Presztosztát','','','11500','0');
INSERT INTO `bos_opciok_hu` VALUES ('915','239','Villamos kapcsolószekrény 3F esetén','','','58000','0');
INSERT INTO `bos_opciok_hu` VALUES ('916','239','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('921','300','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('942','50','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('943','50','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('944','50','Végelem bal','','','85140','0');
INSERT INTO `bos_opciok_hu` VALUES ('945','50','Végelem jobb','','','85140','0');
INSERT INTO `bos_opciok_hu` VALUES ('959','48','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('960','48','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('961','48','ABS végelem','','','56000','0');
INSERT INTO `bos_opciok_hu` VALUES ('962','65','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('963','34','Rozsdamentes belső ','','','32200','0');
INSERT INTO `bos_opciok_hu` VALUES ('964','361','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('965','66','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('966','34','Plexi tolóajtó','','','44550','0');
INSERT INTO `bos_opciok_hu` VALUES ('967','34','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('968','34','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('969','34','Köztes üvegpolc','','','8600','0');
INSERT INTO `bos_opciok_hu` VALUES ('970','34','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('975','233','Metal végelem bal','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('976','233','Metal végelem jobb','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('979','232','Metal végelem bal','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('980','232','Metal végelem jobb','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('992','2','Külső sarokelem 90°','','','985000','0');
INSERT INTO `bos_opciok_hu` VALUES ('997','51','Külső sarokelem 90°','','','895000','0');
INSERT INTO `bos_opciok_hu` VALUES ('998','51','Belső sarokelem 90°','','','895000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1004','2','Belső sarokelem 90°','','','985000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1005','141','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1006','142','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1007','138','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1009','255','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1011','254','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1012','132','Inox burkolat','','','66000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1013','132','Rozsdamentes fogas','','','23000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1014','132','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1015','133','Inox burkolat','','','68000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1016','133','Rozsdamentes fogas','','','23000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1017','133','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1018','231','Végelem bal','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1019','231','Végelem jobb','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1020','231','Karos világítás','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1021','230','Végelem bal','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1022','230','Végelem jobb','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1023','230','Karos világítás','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1024','229','Végelem bal','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1025','229','Végelem jobb','','','62000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1026','229','Karos világítás','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1027','357','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1028','324','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1029','13','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1030','13','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1031','13','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1032','13','Ventillációs felár','','','18500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1033','13','Végelem','','','40660','0');
INSERT INTO `bos_opciok_hu` VALUES ('1034','19','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1035','14','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1036','14','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1037','14','Köztes üvegpolc','','','14400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1038','14','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1039','20','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1040','15','Ventillációs hűtés','','','27450','0');
INSERT INTO `bos_opciok_hu` VALUES ('1041','15','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1042','15','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1043','15','Köztes üvegpolc','','','16400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1044','15','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1045','15','Végelem','','','40660','0');
INSERT INTO `bos_opciok_hu` VALUES ('1046','16','Ventillációs hűtés','','','27000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1047','16','Mobil plexi osztó','','','19300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1048','16','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1049','16','Köztes üvegpolc','','','16400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1050','16','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1051','16','Végelem','','','40660','0');
INSERT INTO `bos_opciok_hu` VALUES ('1052','17','Ventillációs hűtés','','','21150','0');
INSERT INTO `bos_opciok_hu` VALUES ('1053','17','Mobil plexi osztó','','','19300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1054','17','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1055','17','Köztes üvegpolc','','','22800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1056','17','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1057','17','Végelem','','','40660','0');
INSERT INTO `bos_opciok_hu` VALUES ('1058','18','Ventillációs hűtés','','','21650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1059','18','Mobil plexi osztó','','','19300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1060','18','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1061','18','Köztes üvegpolc','','','28800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1062','18','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1063','18','Végelem','','','40660','0');
INSERT INTO `bos_opciok_hu` VALUES ('1064','19','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1065','19','Köztes üvegpolc','','','14400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1066','19','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1067','20','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1068','20','Köztes üvegpolc','','','14400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1069','20','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1070','21','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1071','21','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1072','21','Köztes üvegpolc','','','14400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1073','21','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1075','60','Hűtetlen kivitel','','','-226500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1076','59','Laminált színfelár','','','11400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1078','382','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1081','223','Éjszakai takarókészlet','','','78400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1082','365','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1083','198','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1084','198','kosár (max 8 db)','','','3500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1085','135','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1086','8','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1087','8','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1088','8','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1089','8','Rozsdamentes bemutatófelület','','','21120','0');
INSERT INTO `bos_opciok_hu` VALUES ('1095','9','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1096','9','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1097','9','Köztes üvegpolc','','','11400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1098','9','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1099','9','Rozsdamentes bemutatófelület','','','31680','0');
INSERT INTO `bos_opciok_hu` VALUES ('1100','10','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1101','10','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1102','10','Köztes üvegpolc','','','12900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1103','10','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1104','10','Rozsdamentes bemutatófelület','','','42240','0');
INSERT INTO `bos_opciok_hu` VALUES ('1105','11','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1106','11','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1107','11','Köztes üvegpolc','','','15345','0');
INSERT INTO `bos_opciok_hu` VALUES ('1108','11','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1109','11','Rozsdamentes bemutatófelület','','','52800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1110','12','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1111','12','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1112','12','Köztes üvegpolc','','','16850','0');
INSERT INTO `bos_opciok_hu` VALUES ('1113','12','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1114','12','Rozsdamentes bemutatófelület','','','63360','0');
INSERT INTO `bos_opciok_hu` VALUES ('1115','197','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1116','197','Kocsiütköző','','','148500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1117','183','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1118','186','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1120','201','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1121','187','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1122','202','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1123','184','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1124','200','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1125','199','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1126','185','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1127','218','Rács kiemelő','','','15400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1128','106','Üveg nyílóajtó','','','423000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1129','105','Panoráma METAL végelem','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1130','105','Üveg nyílóajtó','','','345000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1131','105','Szigetelt Alu üveg nyílóajtó','','','448500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1132','105','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1133','102','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1134','103','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1135','98','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1136','105','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1137','106','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1138','107','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1140','108','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1141','109','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1142','110','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1143','111','Komplett polc 1 db','','','32400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1144','112','Komplett polc 1 db','','','32400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1145','113','Komplett polc 1 db','','','32400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1146','114','Komplett polc 1 db','','','32400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1147','115','Komplett polc 1 db','','','32400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1148','116','Komplett polc 1 db','','','32400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1149','111','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1150','112','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1151','113','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1152','114','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1153','115','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1154','116','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1155','339','Végoszlop 3 tartófüllel','','','22100','0');
INSERT INTO `bos_opciok_hu` VALUES ('1156','324','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1157','325','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1158','326','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1159','327','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1160','328','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1161','329','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1162','96','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1163','96','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1164','96','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1165','96','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1166','96','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1167','96','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1168','97','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1169','97','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1170','97','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1171','97','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1172','97','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1173','97','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1174','99','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1175','99','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1176','99','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1177','99','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1178','99','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1179','99','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1180','100','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1181','100','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1182','100','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1183','100','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1184','100','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1185','100','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1186','101','Panoráma METAL végelem  bal','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1187','101','Panoráma METAL végelem jobb','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1188','101','Tükrös METAL végelem  bal','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1189','101','Tükrös METAL végelem jobb','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1190','101','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1191','101','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1192','4','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1193','104','Panoráma METAL végelem','','','74300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1194','104','Szigetelt Alu üveg nyílóajtó','','','320850','0');
INSERT INTO `bos_opciok_hu` VALUES ('1195','104','Tükrös METAL végelem','','','69970','0');
INSERT INTO `bos_opciok_hu` VALUES ('1196','104','ABS végelem','','','53800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1197','104','Komplett polc 1 db','','','11900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1200','363','Mérlegtató','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1201','356','Bútorkeret tető','','','16100','0');
INSERT INTO `bos_opciok_hu` VALUES ('1202','356','Bútorkeret láb','','','27225','0');
INSERT INTO `bos_opciok_hu` VALUES ('1233','5','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1234','5','Mobil osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1235','5','Végelem bal','','','80000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1236','5','Végelem jobb','','','80000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1237','5','Árutéri lépcső 1000 mm','','','22200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1238','5','Hűtött sarokelem 90 °','','','1345500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1239','338','Krómozott oszlop 1080','','','11200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1240','338','Műanyag karmantyú','','','490','0');
INSERT INTO `bos_opciok_hu` VALUES ('1241','338','Korlátcső krómozott','','','5800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1242','330','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1243','330','Scannervédő plexi','','','22700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1244','330','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1245','330','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1246','333','Flip-Top kassza','','','120500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1247','330','Flip-Top kassza','','','120500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1248','330','Pénztár számozás világító','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1249','330','Következő vásárló elválasztó','','','2500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1250','330','Bankjegyolvasó tartó','','','8500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1251','331','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1252','331','Scannervédő plexi','','','22700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1253','331','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1254','331','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1255','331','Áruterelő lap','','','9800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1256','331','Flip-Top kassza','','','120500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1257','331','Pénztár számozás világító','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1258','331','Következő vásárló elválasztó','','','2500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1259','331','Bankjegyolvasó tartó','','','8500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1260','332','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1261','332','Scannervédő plexi','','','22700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1262','332','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1263','332','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1264','332','Áruterelő lap','','','9800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1265','332','Flip-Top kassza','','','120500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1266','332','Pénztár számozás világító','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1267','332','Következő vásárló elválasztó','','','2500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1268','332','Bankjegyolvasó tartó','','','8500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1269','333','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1270','333','Scannervédő plexi','','','22700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1271','333','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1272','333','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1273','333','Pénztár számozás világító','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1274','333','Következő vásárló elválasztó','','','2500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1275','333','Bankjegyolvasó tartó','','','8500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1276','334','Flip-Top kassza','','','120500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1277','334','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1278','334','Scannervédő plexi','','','22700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1279','334','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1280','334','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1281','334','Pénztár számozás világító','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1282','334','Következő vásárló elválasztó','','','2500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1283','334','Bankjegyolvasó tartó','','','8500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1284','335','Flip-Top kassza','','','120500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1285','335','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1286','335','Scannervédő plexi','','','22700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1287','335','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1288','335','Energiaoszlop 2,5 m','','','27250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1289','335','Pénztár számozás világító','','','9900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1290','335','Következő vásárló elválasztó','','','2500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1291','335','Bankjegyolvasó tartó','','','8500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1292','14','Végelem','','','40660','0');
INSERT INTO `bos_opciok_hu` VALUES ('1293','13','Aggregát nélküli kivitel','','','-19400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1294','14','Aggregát nélküli kivitel','','','-19800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1295','15','Aggregát nélküli kivitel','','','-21850','0');
INSERT INTO `bos_opciok_hu` VALUES ('1296','16','Aggregát nélküli kivitel','','','-23000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1297','17','Aggregát nélküli kivitel','','','-24700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1298','18','Aggregát nélküli kivitel','','','-25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1303','164','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('1304','168','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('1305','164','1 ajtó helyett 2 fiók','','','82400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1306','164','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1307','164','Rozdamentes munkapult hátsó peremmel','','','32000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1308','164','Rozsdamentes munkapult','','','10300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1309','167','1 ajtó helyett 3 fiók','','','96625','0');
INSERT INTO `bos_opciok_hu` VALUES ('1310','167','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1311','167','Rozdamentes munkapult hátsó peremmel','','','42175','0');
INSERT INTO `bos_opciok_hu` VALUES ('1312','167','Rozsdamentes munkapult','','','14260','0');
INSERT INTO `bos_opciok_hu` VALUES ('1313','168','Beüzemelési díj','','','14000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1314','168','Rozdamentes munkapult hátsó peremmel','','','50500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1315','168','Rozsdamentes munkapult','','','16450','0');
INSERT INTO `bos_opciok_hu` VALUES ('1316','372','Géptartó állvány','','','179900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1331','86','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1332','117','Végelem','','','122350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1333','86','Végelem','','','50250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1334','87','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1335','87','Autómata cseppvíz elpárologtató','','','44550','0');
INSERT INTO `bos_opciok_hu` VALUES ('1336','87','Végelem','','','50250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1337','88','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1338','88','Autómata cseppvíz elpárologtató','','','44550','0');
INSERT INTO `bos_opciok_hu` VALUES ('1339','88','Végelem','','','50250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1340','89','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1341','89','Autómata cseppvíz elpárologtató','','','44550','0');
INSERT INTO `bos_opciok_hu` VALUES ('1342','89','Végelem','','','50250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1343','124','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1344','124','Autómata cseppvíz elpárologtató','','','44550','0');
INSERT INTO `bos_opciok_hu` VALUES ('1345','124','Végelem','','','50250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1346','125','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1347','125','Autómata cseppvíz elpárologtató','','','44550','0');
INSERT INTO `bos_opciok_hu` VALUES ('1348','125','Végelem','','','50250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1349','111','Rácskosár','','','35600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1350','112','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('1351','113','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('1352','114','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('1353','115','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('1354','116','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('1355','72','Rácskosár 1 db','','','35640','0');
INSERT INTO `bos_opciok_hu` VALUES ('1356','53','Tolóajtó KIT előre','','','64350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1357','159','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1358','53','Self-service KIT','','','79200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1359','53','Beüzemelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1360','221','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1361','223','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1364','363','Íves végelem','','','110500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1365','363','Szerelési díj','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1373','49','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1374','344','Gyerekülés','','','810','0');
INSERT INTO `bos_opciok_hu` VALUES ('1375','344','Betétizár 1€/100Ft','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1376','345','Gyerekülés','','','810','0');
INSERT INTO `bos_opciok_hu` VALUES ('1377','345','Betétizár 1€/100Ft','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1378','243','Légkondenzátor Alfa Laval','','','1','0');
INSERT INTO `bos_opciok_hu` VALUES ('1379','243','Lemezes hőcserélő','','','185000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1385','51','Aláépített aggregát','','','334900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1386','307','Küszöb szigetelt aljzathoz','','','11250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1387','306','Küszöb szigetelt aljzathoz','','','12900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1388','315','LED világítás','','','148000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1393','310','Ajtózár','','','39600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1394','311','Ajtózár','','','39600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1395','312','Ajtózár','','','39600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1396','313','Ajtózár','','','39600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1397','314','Ajtózár','','','39600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1398','316','LED világítás','','','227200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1399','317','LED világítás','','','322740','0');
INSERT INTO `bos_opciok_hu` VALUES ('1400','318','LED világítás','','','350500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1401','319','LED világítás','','','481000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1426','31','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1427','31','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1428','31','Köztes üvegpolc ','','','41200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1429','31','Végelem 1db','','','32650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1430','31','Plexi tolóajtó','','','60700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1431','31','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1462','33','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1463','33','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1464','33','Köztes üvegpolc ','','','41200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1465','33','Végelem 1db','','','32650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1466','33','Plexi tolóajtó','','','60700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1467','33','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1472','61','Végelem 1db','','','32650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1473','61','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1474','61','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1475','61','Plexi tolóajtó','','','30800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1476','61','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1506','367','Végelem 1db','','','32650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1507','367','Mobil fa osztó','','','16305','0');
INSERT INTO `bos_opciok_hu` VALUES ('1508','367','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1509','367','Plexi tolóajtó','','','60700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1510','367','Üvegpolc','','','41200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1511','367','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1514','366','Sarokelem 45°','','','136500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1515','366','Végelem 1db','','','32650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1516','366','Sarokelem 90°','','','283000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1525','134','Végelem INOX','','','85270','0');
INSERT INTO `bos_opciok_hu` VALUES ('1526','134','Végelem','','','56850','0');
INSERT INTO `bos_opciok_hu` VALUES ('1527','134','INOX beső','','','448740','0');
INSERT INTO `bos_opciok_hu` VALUES ('1528','134','Aláépített aggregát','','','262700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1531','90','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1532','90','Világítás','','','11000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1533','90','Éjszakai roló','','','23000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1534','91','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1535','91','Világítás','','','11000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1536','91','Éjszakai roló','','','23000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1537','92','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1538','92','Világítás','','','12700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1539','92','Éjszakai roló','','','27650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1540','93','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1541','93','Világítás','','','15400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1542','93','Éjszakai roló','','','43950','0');
INSERT INTO `bos_opciok_hu` VALUES ('1543','218','Önbeállókerék szett','','','32670','0');
INSERT INTO `bos_opciok_hu` VALUES ('1544','219','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1545','219','Rács kiemelő','','','19950','0');
INSERT INTO `bos_opciok_hu` VALUES ('1546','219','Önbeállókerék szett','','','32670','0');
INSERT INTO `bos_opciok_hu` VALUES ('1547','220','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1548','220','Rács kiemelő','','','25500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1549','220','Önbeállókerék szett','','','32670','0');
INSERT INTO `bos_opciok_hu` VALUES ('1550','218','Üveg tolótető','','','118600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1551','219','Üveg tolótető','','','190500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1552','220','Üveg tolótető','','','211700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1553','188','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1554','188','Rács kiemelő','','','15400','0');
INSERT INTO `bos_opciok_hu` VALUES ('1555','188','Önbeállókerék szett','','','32670','0');
INSERT INTO `bos_opciok_hu` VALUES ('1556','188','Üveg tolótető','','','118600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1557','189','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1558','189','Rács kiemelő','','','19950','0');
INSERT INTO `bos_opciok_hu` VALUES ('1559','189','Önbeállókerék szett','','','32670','0');
INSERT INTO `bos_opciok_hu` VALUES ('1560','189','Üveg tolótető','','','190500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1561','190','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1562','190','Rács kiemelő','','','25500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1563','190','Önbeállókerék szett','','','32670','0');
INSERT INTO `bos_opciok_hu` VALUES ('1564','190','Üveg tolótető','','','211700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1565','218','-18/-20 - +2/+8 °C kétkörös kialakítás','','','65340','0');
INSERT INTO `bos_opciok_hu` VALUES ('1566','219','-18/-20 - +2/+8 °C kétkörös kialakítás','','','97700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1567','220','-18/-20 - +2/+8 °C kétkörös kialakítás','','','98700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1568','222','Aggregát nélküli kivitel','','','-137500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1569','222','Éjszakai takarókészlet','','','65300','0');
INSERT INTO `bos_opciok_hu` VALUES ('1570','222','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1571','4','Tuna belső sarokelem 90°','','','1060000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1572','4','Árutéri lépcső 1000 mm','','','22200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1573','207','Kosárkészlet','','','19700','0');
INSERT INTO `bos_opciok_hu` VALUES ('1574','207','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1579','32','Végelem 1db','','','32650','0');
INSERT INTO `bos_opciok_hu` VALUES ('1580','32','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1581','32','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1582','32','Plexi tolóajtó','','','22200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1583','32','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1584','260','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1585','261','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1586','262','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1587','263','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1588','264','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1589','265','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1590','266','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1591','267','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1592','268','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1593','269','Szerelési díj','','','30000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1594','270','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1595','271','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1596','272','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1597','273','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1598','274','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1599','275','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1600','276','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1601','277','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1602','278','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1603','279','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1604','280','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1605','281','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1606','282','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1607','283','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1608','284','Szerelési díj','','','25000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1609','193','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1610','193','edénykészlet műanyag','','','150900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1611','193','kiemelő szett','','','129900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1612','193','szín felár','','','114900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1613','322','Folyosózár kihajtható','','','18000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1614','322','Kosárcsúsztató ','','','19800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1615','322','Zárható fiók','','','9000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1616','339','Középoszlop 6 tartófüllel','','','25500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1617','339','Korlátcső krómozott','','','5800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1618','339','Krómozott oszlop 1080','','','11200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1619','339','Műanyag karmantyú','','','490','0');
INSERT INTO `bos_opciok_hu` VALUES ('1620','322','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1621','323','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1622','324','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1623','325','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1624','326','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1625','327','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1626','328','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1627','329','Szerelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1628','330','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1629','331','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1630','332','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1631','333','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1632','334','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1633','335','Szerelési díj','','','8000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1635','22','Mobil plexi osztó','','','14900','0');
INSERT INTO `bos_opciok_hu` VALUES ('1636','22','Mérlegtartó/vágódeszka','','','12800','0');
INSERT INTO `bos_opciok_hu` VALUES ('1637','22','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1639','206','Színes tetőkeret','','','5000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1640','206','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1641','241','Szállítási díj','','','165','0');
INSERT INTO `bos_opciok_hu` VALUES ('1642','321','Szerelési díj','','','100000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1651','320','Előrács','','','1435','0');
INSERT INTO `bos_opciok_hu` VALUES ('1661','320','Osztó rács','','','750','0');
INSERT INTO `bos_opciok_hu` VALUES ('1662','117','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1663','118','Végelem','','','122350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1664','118','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1667','120','Végelem','','','122350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1668','120','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1669','119','Végelem','','','122350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1670','119','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1671','121','Végelem','','','122350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1672','121','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1673','122','Végelem','','','122350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1674','122','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1675','214','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1676','213','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1678','380','Promo box','','','15350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1679','381','Promo box','','','15350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1681','23','Infra melegentartó (felár/hűtés nélkül)','','','116000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1682','192','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1683','192','Edénykészlet RM GN ','','','94150','0');
INSERT INTO `bos_opciok_hu` VALUES ('1684','194','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1685','192','Tányértartó 1 db','','','37500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1686','191','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1687','191','edénykészlet 3x GN1/1','','','24350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1688','194','1 sor hűtetlen polc','','','227500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1689','194','Szín felár','','','107500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1690','194','Kocsiütköző','','','130000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1691','195','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1692','195','1 sor hűtetlen polc','','','227500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1693','195','Szín felár','','','107500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1694','195','Kocsiütköző','','','130000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1695','196','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1696','196','1 sor hűtetlen polc','','','227500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1697','196','Szín felár','','','107500','0');
INSERT INTO `bos_opciok_hu` VALUES ('1698','196','Kocsiütköző','','','130000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1702','197','Szín felár','','','135250','0');
INSERT INTO `bos_opciok_hu` VALUES ('1703','54','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1705','383','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1706','55','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1707','136','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1708','384','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1709','56','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1711','385','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1712','57','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1714','386','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1715','58','Beüzemelési díj','','','16000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1719','241','Szerelési anyag / fm.','','','12000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1723','224','Metal végelem bal','','','108000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1724','224','Éjszakai takaró','','','73990','0');
INSERT INTO `bos_opciok_hu` VALUES ('1725','224','Rács betét','','','49350','0');
INSERT INTO `bos_opciok_hu` VALUES ('1726','147','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1727','147','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1728','150','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1729','150','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1730','149','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1731','149','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1732','148','Ajtó zár','','','6200','0');
INSERT INTO `bos_opciok_hu` VALUES ('1733','148','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1734','139','Beüzemelési díj','','','10000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1735','68','Szigetelt nyílóajtó','','','164600','0');
INSERT INTO `bos_opciok_hu` VALUES ('1736','68','Szigeteletlen nyílóajtó','','','134000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1737','69','Szigetelt nyílóajtó','','','237150','0');
INSERT INTO `bos_opciok_hu` VALUES ('1738','69','Szigeteletlen nyílóajtó','','','180000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1739','70','Szigetelt nyílóajtó','','','329000','0');
INSERT INTO `bos_opciok_hu` VALUES ('1740','70','Szigeteletlen nyílóajtó','','','267840','0');
-- --------------------------------------------------------

--
-- Table structure for table `bos_penznemek`
--

DROP TABLE IF EXISTS `bos_penznemek`;

CREATE TABLE `bos_penznemek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(3) NOT NULL,
  `nev` varchar(30) NOT NULL,
  `elotag` tinyint(4) NOT NULL DEFAULT '1' COMMENT '0: elore irjuk, 1: utana irjuk az arnak',
  `szimbolum` varchar(10) NOT NULL,
  `decimal` tinyint(4) NOT NULL,
  `decpont` varchar(2) NOT NULL,
  `ezrespont` varchar(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_penznemek`
--

INSERT INTO `bos_penznemek` VALUES ('1','HUF','Hungarian Forint','1','Forint','0','',',');
INSERT INTO `bos_penznemek` VALUES ('2','EUR','Euro','0','&euro;','2','.',' ');
INSERT INTO `bos_penznemek` VALUES ('3','JPY','Japan Yen','0','&#165;','0','.',',');
-- --------------------------------------------------------

--
-- Table structure for table `bos_termekek`
--

DROP TABLE IF EXISTS `bos_termekek`;

CREATE TABLE `bos_termekek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cikkszam` varchar(255) NOT NULL,
  `ar` float NOT NULL,
  `akcios_ar` float NOT NULL,
  `ar_penznem` varchar(5) NOT NULL DEFAULT 'HUF',
  `akcio` int(11) NOT NULL,
  `beszerzesi_ar` float NOT NULL,
  `beszerzesi_ar_penznem` varchar(5) NOT NULL DEFAULT 'HUF',
  `kiemelt` tinyint(4) NOT NULL,
  `publikus` tinyint(4) NOT NULL,
  `adoosztaly_id` int(11) NOT NULL,
  `gyarto_id` int(11) NOT NULL,
  `forgalmazo_id` int(11) NOT NULL,
  `keszlet` int(11) NOT NULL,
  `lefoglalva` int(11) NOT NULL,
  `adatlap` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cikkszam` (`cikkszam`)
) ENGINE=MyISAM AUTO_INCREMENT=391 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_termekek`
--

INSERT INTO `bos_termekek` VALUES ('2','Dolphin L','1110140','0','HUF','0','1557','EUR','1','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('3','Tuna','1232060','0','HUF','0','1728','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('4','Tuna 125 SS','491970','0','HUF','0','690','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('5','Tuna R','1664140','0','HUF','0','2334','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('6','ARTEMIS','816385','0','HUF','0','1145','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('7','Universal','346797','0','HUF','0','678','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('8','DGDMAST100','345774','279900','HUF','1','676','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('9','DGDMAST150','370326','309900','HUF','1','724','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('10','DGDMAST200','426080','359900','HUF','1','833','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('11','DGDMAST250','514569','439900','HUF','1','1006','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('12','DGDMAST300','586691','499900','HUF','1','1147','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('13','Grazia 05 120','398459','0','HUF','0','779','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('14','Grazia 05 150','455235','0','HUF','0','890','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('15','Grazia 05 170','496155','0','HUF','0','970','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('16','Grazia 05 200','545771','0','HUF','0','1067','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('17','Grazia 05 250','614312','0','HUF','0','1201','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('18','Grazia 05 300','722750','0','HUF','0','1413','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('19','Grazia 03 150 BT','882849','0','HUF','0','1726','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('20','Grazia 03 200 BT','1046530','0','HUF','0','2046','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('21','Grazia 03 250 BT','1145760','0','HUF','0','2240','EUR','0','0','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('22','Grazia 03 120','421987','0','HUF','0','825','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('23','Grazia 06 120','425056','0','HUF','0','831','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('24','Grazia 06 150','464442','0','HUF','0','908','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('25','Grazia 06 170','534517','0','HUF','0','1045','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('26','Grazia 06 200','582087','0','HUF','0','1138','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('27','Grazia 06 250','658812','0','HUF','0','1288','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('28','Grazia 06 300','723261','0','HUF','0','1414','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('29','Grazia 03 BS','537587','0','HUF','0','1051','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('30','Grazia 03 KS','537587','0','HUF','0','1051','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('31','KLARA S','1098700','359900','HUF','1','2148','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('32','KLARA TP','1213790','359900','HUF','1','2373','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('33','KLARA V','1248060','359900','HUF','1','2440','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('34','DGDMASTP100','488994','0','HUF','0','956','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('35','DGDMASTP150','581064','0','HUF','0','1136','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('36','DGDMASTP200','644490','0','HUF','0','1260','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('37','DGDMASTP250','736560','0','HUF','0','1440','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('38','DGDMASTP300','912516','0','HUF','0','1784','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('39','DGDMASTP350','1036300','0','HUF','0','2026','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('40','DGDMASTP400','1171850','0','HUF','0','2291','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('41','DGDMASTL100','602547','0','HUF','0','1178','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('42','DGDMASTL150','694617','0','HUF','0','1358','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('43','DGDMASTL200','820958','0','HUF','0','1605','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('44','DGDMASTL250','897683','0','HUF','0','1755','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('45','DGDMASTL300','1059830','0','HUF','0','2072','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('46','DGDMASTL350','1235780','0','HUF','0','2416','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('47','DGDMASTL400','1406110','0','HUF','0','2749','EUR','0','0','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('48','Bellini 83','1373380','0','HUF','0','2685','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('49','Bellini 83 melegentartó','1310460','0','HUF','0','2562','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('50','Costan Rossini','1946770','0','HUF','0','3806','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('51','Dolphin','858452','0','HUF','0','1204','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('52','HEMERA','891250','0','HUF','0','1250','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('53','OHIO II','345263','0','HUF','0','675','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('54','EVO 60','474672','0','HUF','0','928','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('55','EVO 90','534006','0','HUF','0','1044','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('56','EVO 120','652674','0','HUF','0','1276','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('57','EVO 150','712008','0','HUF','0','1392','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('58','EVO 180','890010','0','HUF','0','1740','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('59','LDE 05-90','674669','0','HUF','0','1319','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('60','LDE 05-140','764181','0','HUF','0','1494','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('61','KLARA V ZA','970827','359900','HUF','1','1898','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('62','SY 140B','996402','0','HUF','0','1948','EUR','1','1','3','17','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('63','SY 220B','1255730','0','HUF','0','2455','EUR','0','1','3','17','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('64','SY 290B','1441410','0','HUF','0','2818','EUR','0','1','3','17','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('65','Kubo300','732980','0','HUF','0','1433','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('66','Kubo600','1184630','0','HUF','0','2316','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('67','Pandora 90 SL','524520','0','HUF','0','940','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('68','Pandora 125 SL','538470','0','HUF','0','965','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('69','Pandora 180 SL','712008','0','HUF','0','1276','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('70','Pandora 250 SL','989892','0','HUF','0','1774','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('71','Pandora 90 FV','513360','0','HUF','0','920','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('72','Pandora 125 FV','543492','0','HUF','0','974','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('73','Pandora 180 FV','733770','0','HUF','0','1315','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('74','Pandora 250 FV','1039550','0','HUF','0','1863','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('75','Oxford 70-90','502200','449900','HUF','1','900','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('76','Oxford 70-125','521730','449900','HUF','1','935','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('77','Oxford 70-180','680202','449900','HUF','1','1219','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('78','Oxford 70-250','956412','0','HUF','0','1714','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('79','NCOS_OUV90','846533','0','HUF','0','1655','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('80','COS_OUV120','929907','0','HUF','0','1818','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('81','COS_OUV180','1087960','0','HUF','0','2127','EUR','1','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('82','COS_OUV240','1554960','0','HUF','0','3040','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('83','COS_OP90','741675','0','HUF','0','1450','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('84','COS_OP120','823004','0','HUF','0','1609','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('85','COS_OP180','992310','0','HUF','0','1940','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('86','APL-SL 90','337249','0','HUF','0','473','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('87','APL-SL 125','385733','0','HUF','0','541','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('88','APL-SL 180','562557','0','HUF','0','789','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('89','APL-SL 250','725834','0','HUF','0','1018','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('90','SARA 1400 - 70','418919','0','HUF','0','819','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('91','SARA 1400 - 100','496667','0','HUF','0','971','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('92','SARA 1400 - 133','594875','0','HUF','0','1163','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('93','SARA 1400 - 190','741164','0','HUF','0','1449','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('94','COS_OPSV90','730422','0','HUF','0','1428','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('95','COS_OPSV120','828630','0','HUF','0','1620','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('96','OXFORD SL 80','315580','0','HUF','0','509','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('97','OXFORD 80 SL 125','360840','0','HUF','0','582','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('98','OXFORD 80 SL 180 Falihűtő','525760','0','HUF','0','848','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('99','OXFORD 80 SL 250','680140','0','HUF','0','1097','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('100','OXFORD 80 SL 305','815920','0','HUF','0','1316','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('101','OXFORD 80 SL 375','1005640','0','HUF','0','1622','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('102','Penguin 90 SL 90','362917','0','HUF','0','509','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('103','Penguin 90 SL 125','414966','0','HUF','0','582','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('104','Penguin 90 SL 180','604624','0','HUF','0','848','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('105','Penguin 90 SL 250','782161','0','HUF','0','1097','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('106','Penguin 90 SL 305','938308','0','HUF','0','1316','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('107','Penguin 90 SL 375','1156490','0','HUF','0','1622','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('108','Penguin 90 SL 125 SD','664516','0','HUF','0','932','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('109','Penguin 90 SL 250 SD','1281260','0','HUF','0','1797','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('110','Penguin 90 SL 375 SD','1905140','0','HUF','0','2672','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('111','Penguin 90 FV 90','355787','0','HUF','0','499','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('112','Penguin 90 FV 125','404271','0','HUF','0','567','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('113','Penguin 90 FV 180','589651','0','HUF','0','827','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('114','Penguin 90 FV 250','767188','0','HUF','0','1076','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('115','Penguin 90 FV 305','919770','0','HUF','0','1290','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('116','Penguin 90 FV 375','1130820','0','HUF','0','1586','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('117','PN-LOW-90','333684','0','HUF','0','468','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('118','PN-LOW-125','389297','0','HUF','0','546','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('119','PN-LOW-180','593216','0','HUF','0','832','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('120','PN-LOW-250','787151','0','HUF','0','1104','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('121','PN-LOW-305','902657','0','HUF','0','1266','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('122','PN-LOW-375','1112280','0','HUF','0','1560','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('123','Lion Narrow','1620430','0','HUF','0','3168','EUR','1','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('124','APL-SL 305','939734','0','HUF','0','1318','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('125','APL-SL 375','1155770','0','HUF','0','1621','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('126','RCW 120','434124','0','HUF','0','778','EUR','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('127','RCW 150','520056','0','HUF','0','932','EUR','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('128','RCW 180','605430','0','HUF','0','1085','EUR','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('129','RCW 120 ZZ','390042','0','HUF','0','699','EUR','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('130','RCW 150 ZZ','458118','0','HUF','0','821','EUR','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('131','RCW 180 ZZ','537354','0','HUF','0','963','EUR','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('132','SPIO 101 Wood','737583','0','HUF','0','1442','EUR','1','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('133','SPIO 135 Wood','825561','0','HUF','0','1614','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('134','Kornel','927350','0','HUF','0','1813','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('135','J-400','158664','0','HUF','0','310','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('136','UA600FV','266772','0','HUF','0','521','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('137','UA1000FV','316140','0','HUF','0','618','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('138','EIS 40.2','428637','0','HUF','0','838','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('139','EIS 45.2','586690','0','HUF','0','1147','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('140','POLO 300','0','0','HUF','0','0','HUF','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('141','POLO 450','627099','0','HUF','0','1226','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('142','POLO 900','870573','0','HUF','0','1702','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('143','AFV 700 TN 1P +','393855','279900','HUF','1','770','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('144','AFV 700 TN 1PV +','489506','0','HUF','0','957','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('145','AFV 700 BT 1P -','510477','0','HUF','0','998','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('146','AFV 700 BT 1PV -','627611','0','HUF','0','1227','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('147','AFV 700 TN 2P +','468022','279900','HUF','1','915','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('148','AFV 700 TN+TN','563673','279900','HUF','1','1102','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('149','AFV 700 TN+BT','638352','279900','HUF','1','1248','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('150','AFV 700 BT+BT','744744','279900','HUF','1','1456','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('151','AFV 1400 TN 2P +','509454','369900','HUF','1','996','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('152','AFV 1400 TN 2PV +','659835','0','HUF','0','1290','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('153','AFV 1400 BT 2P -','680806','0','HUF','0','1331','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('154','AFV 1400 BT 2PV -','829653','0','HUF','0','1622','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('155','AFV 1400 TN+TN 2P +','766227','549900','HUF','1','1498','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('156','AFV 1400 TN+TN 2PV +','872619','0','HUF','0','1706','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('157','AFV 1400 TN+BT 2P +-','835791','0','HUF','0','1634','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('158','AFV 1400 TN+BT 2PV +-','936045','0','HUF','0','1830','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('159','USD 374 GD','190960','0','HUF','0','308','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('160','Rugiada Vino','302296','0','HUF','0','591','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('161','Rugiada Kantina','378510','0','HUF','0','740','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('162','Rugiada Vino 1','628122','0','HUF','0','1228','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('163','Rugiada Vino 2','1166730','0','HUF','0','2281','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('164','TF02EKOGN','323780','0','HUF','0','633','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('165','TF03EKOGN','389763','0','HUF','0','762','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('166','TF04EKOGN','429660','0','HUF','0','840','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('167','TF03EKOGNSG','366234','0','HUF','0','716','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('168','TF04EKOGNSG','408689','0','HUF','0','799','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('169','TF02MIDBT','548840','0','HUF','0','1073','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('170','TF03MIDBT','652163','0','HUF','0','1275','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('171','TF04MIDBT','713031','0','HUF','0','1394','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('172','SL02NX','258819','0','HUF','0','506','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('173','SL03NX','370326','0','HUF','0','724','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('174','SL02EKO','225060','0','HUF','0','440','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('175','SL03EKO','321222','0','HUF','0','628','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('176','SL02VC','274164','0','HUF','0','536','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('177','SL03VC','401016','0','HUF','0','784','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('178','SL03PZVR4','652674','0','HUF','0','1276','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('179','SL02C6VR4','712008','0','HUF','0','1392','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('180','SL03C6VR4','842952','0','HUF','0','1648','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('181','DIK_ORION 110','474300','0','HUF','0','850','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('182','DIK_ORION 140','530100','0','HUF','0','950','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('183','Cily PB','512','0','HUF','0','1','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('184','MiniGlobo PB','224037','0','HUF','0','438','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('185','Orinoko GI60','380045','0','HUF','0','743','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('186','Impuls 70 P','512','0','HUF','0','1','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('187','Impuls 100 P','512','0','HUF','0','1','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('188','FIJI NT 150','670577','0','HUF','0','1311','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('189','FIJI NT 200','734514','0','HUF','0','1436','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('190','FIJI TN 250','839883','0','HUF','0','1642','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('191','Carello maxi','577995','0','HUF','0','1130','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('192','OASI 4','1260850','0','HUF','0','2465','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('193','Girotondo','1606110','0','HUF','0','3140','EUR','0','1','3','18','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('194','ECLIPSE Luna','2081800','0','HUF','0','4070','EUR','0','1','3','21','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('195','ECLIPSE Goccia','2199450','0','HUF','0','4300','EUR','0','1','3','21','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('196','ECLIPSE Elisse','2260830','0','HUF','0','4420','EUR','0','1','3','21','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('197','ECLIPSE II','2570290','0','HUF','0','5025','EUR','1','1','3','21','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('198','UDD600SC','250560','0','HUF','0','0','HUF','0','0','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('199','Mini BI-ICE','574415','0','HUF','0','1123','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('200','BI-ICE','635795','0','HUF','0','1243','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('201','Impuls 70 N','512','0','HUF','0','1','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('202','Impuls 200 P','512','0','HUF','0','1','EUR','0','0','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('203','WM305','165726','0','HUF','0','324','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('204','WM505','142197','0','HUF','0','278','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('205','WM605','153961','0','HUF','0','301','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('206','WM705','320100','0','HUF','0','625','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('207','COS_SWING 120','250635','0','HUF','0','490','EUR','0','0','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('208','COS_SWING 170','437332','0','HUF','0','855','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('209','COS_SOUND210','749348','0','HUF','0','1465','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('210','COS_SOUND250','919166','0','HUF','0','1797','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('211','COS_SOUND_MT','825561','0','HUF','0','1614','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('212','COS_BELU_LIN14','905866','0','HUF','0','1771','EUR','1','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('213','COS_BELU_LIN21','1123250','0','HUF','0','2196','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('214','COS_BELU_MT','1252150','0','HUF','0','2448','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('215','GT 150 O','339124','0','HUF','0','663','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('216','GT 200 O','405619','0','HUF','0','793','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('217','GT 250 S','553954','0','HUF','0','1083','EUR','0','1','3','19','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('218','FIJI LT 150','690525','0','HUF','0','1350','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('219','FIJI LT 200','750882','0','HUF','0','1468','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('220','FIJI LT 250','839883','0','HUF','0','1642','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('221','COS_Symphony ABT','1739100','0','HUF','0','3400','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('222','Isabel 150 LT/LT','1270060','0','HUF','0','2483','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('223','Isabel 200 LT/LT','1519670','0','HUF','0','2971','EUR','0','1','3','20','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('224','COMBO','3453460','0','HUF','0','6189','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('225','Flamingo LT 2D','1034250','0','HUF','0','2022','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('226','Flamingo LT 3D','1498180','0','HUF','0','2929','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('227','Flamingo LT 4D','1865440','0','HUF','0','3647','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('228','Flamingo LT 5D','2234230','0','HUF','0','4368','EUR','0','1','3','10','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('229','Cayman2 20 LG300 - 188','0','0','HUF','0','0','HUF','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('230','Cayman2 20 LG300 - 250','0','0','HUF','0','0','HUF','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('231','Cayman2 20 LG300 - 375','0','0','HUF','0','0','HUF','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('232','MIX MNY','2365690','0','HUF','0','4625','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('233','MIX','2573360','0','HUF','0','5031','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('234','AT03ISO','563673','0','HUF','0','1102','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('235','AT05ISO','652674','0','HUF','0','1276','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('236','AT07ISO','1058800','0','HUF','0','2070','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('237','AT10ISO','1069040','0','HUF','0','2090','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('238','Hűtőaggregát normál','0','0','HUF','0','0','HUF','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('239','Hűtőaggregát mirelit','0','0','HUF','0','0','HUF','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('240','Cos_ESAS','1586160','0','HUF','0','3101','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('241','Csőszerelési anyagok','11600','0','HUF','0','22','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('242','FP 20 TN-BT','3685870','0','HUF','0','7206','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('243','FP  TN-BT','3685870','0','HUF','0','7206','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('244','C3C 3GR R404A TN','4698640','0','HUF','0','9186','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('245','ENS 300 3035 SC TN','2808140','0','HUF','0','5490','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('246','ENS 300 3040 SC TN','2901230','0','HUF','0','5672','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('247','ENS 300 3050 SC TN','3004550','0','HUF','0','5874','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('248','ENS 300 3060 SC TN','3168740','0','HUF','0','6195','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('249','ENS 300 3035 SC BT','3177950','0','HUF','0','6213','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('250','ENS 300 3040 SC BT','3229100','0','HUF','0','6313','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('251','ENS 300 3050 SC BT','3388180','0','HUF','0','6624','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('252','ENS 300 3060 SC BT','3561060','0','HUF','0','6962','EUR','0','1','3','14','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('253','Felügyeleti rendszer','294624','0','HUF','0','576','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('254','SFN-030','463419','0','HUF','0','906','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('255','SFN-050','486948','0','HUF','0','952','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('256','SFN-060','506385','0','HUF','0','990','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('257','SFN-075','521730','0','HUF','0','1020','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('258','SFN-100','662904','0','HUF','0','1296','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('259','SFN-120','688991','0','HUF','0','1347','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('260','SFN-122','688991','0','HUF','0','1347','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('261','SFN-150','921212','0','HUF','0','1801','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('262','SFN-200','933999','0','HUF','0','1826','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('263','SFK-120','635795','0','HUF','0','1243','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('264','SFK-170','659835','0','HUF','0','1290','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('265','SFK-201','755997','0','HUF','0','1478','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('266','SFK-202','787710','0','HUF','0','1540','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('267','SFK-203','866993','0','HUF','0','1695','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('268','SFK-300','972362','0','HUF','0','1901','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('269','SFK-400','1094610','0','HUF','0','2140','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('270','ACN-030','392832','0','HUF','0','768','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('271','ACN-050','399993','0','HUF','0','782','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('272','ACN-060','404085','0','HUF','0','790','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('273','ACN-075','435798','0','HUF','0','852','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('274','ACN-100','558047','0','HUF','0','1091','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('275','ACN-122','596921','0','HUF','0','1167','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('276','ACN-120','596921','0','HUF','0','1167','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('277','ACN-150','791802','0','HUF','0','1548','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('278','ACK-120','494109','0','HUF','0','966','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('279','ACK-170','532472','0','HUF','0','1041','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('280','ACK-201','620961','0','HUF','0','1214','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('281','ACK-202','661370','0','HUF','0','1293','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('282','ACK-203','696152','0','HUF','0','1361','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('283','ACK-300','871596','0','HUF','0','1704','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('284','ACK-400','973896','0','HUF','0','1904','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('285','CSN-050','541167','499000','HUF','1','1058','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('286','CSN-060','544236','499000','HUF','1','1064','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('287','CSN-075','567765','0','HUF','0','1110','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('288','CSN-100','696663','0','HUF','0','1362','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('289','CSN-122','730422','0','HUF','0','1428','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('290','CSN-120','730422','0','HUF','0','1428','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('291','CSN-150','879780','0','HUF','0','1720','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('292','CSN-200','901263','0','HUF','0','1762','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('293','CSK-120','625565','0','HUF','0','1223','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('294','CSK-170','675692','0','HUF','0','1321','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('295','CSK-201','765716','0','HUF','0','1497','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('296','CSK-202','831699','0','HUF','0','1626','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('297','CSK-203','840906','0','HUF','0','1644','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('298','CSK-300','954971','0','HUF','0','1867','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('299','CSK-400','1073130','0','HUF','0','2098','EUR','0','1','3','23','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('300','B-Store','0','0','HUF','0','0','HUF','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('301','MiniCTN','542190','0','HUF','0','1060','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('302','MiniCBT','649605','0','HUF','0','1270','EUR','0','1','3','12','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('303','Szendvicspanel ISOBOX 1000','8928','0','HUF','0','16','EUR','0','1','3','24','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('304','Szendvicspanel ISOFRIGO 1000','10044','0','HUF','0','18','EUR','0','1','3','24','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('305','Hűtőkamraajtó 180 N 70','188232','0','HUF','0','368','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('306','Hűtőkamraajtó 180 N 90','194370','0','HUF','0','380','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('307','Mirelit hűtőkamraajtó 240 BT 9','241428','0','HUF','0','472','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('308','Szigetelt ajtó Ufficio 80','154985','0','HUF','0','303','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('309','Szigetelt lengőajtó VA e VIENI','354470','0','HUF','0','693','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('310','Hűtőkamra tolóajtó 880 P 1400','439890','0','HUF','0','860','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('311','Hűtőkamra tolóajtó 880 P 2000','507920','0','HUF','0','993','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('312','Hűtőkamra tolóajtó 880 BT 1400','557535','0','HUF','0','1090','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('313','Hűtőkamra tolóajtó 880 BT 2000','662393','0','HUF','0','1295','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('314','Légzáró tolóajtó 900 ATC','938603','0','HUF','0','1835','EUR','0','1','3','25','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('315','Nyíló üvegajtó SK1P','238871','0','HUF','0','467','EUR','0','1','3','26','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('316','Nyíló üvegajtó SK2P','379022','0','HUF','0','741','EUR','0','1','3','26','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('317','Nyíló üvegajtó SK3P','524799','0','HUF','0','1026','EUR','0','1','3','26','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('318','Nyíló üvegajtó SK4P','680807','0','HUF','0','1331','EUR','0','1','3','26','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('319','Nyíló üvegajtó SK5P','832211','0','HUF','0','1627','EUR','0','1','3','26','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('320','Gondella','0','0','HUF','0','0','HUF','0','1','3','27','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('321','Hűtetlen szerelési díj','511','0','HUF','0','1','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('322','TGYK 150','286952','0','HUF','0','561','EUR','0','0','3','28','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('323','SOM 150S','160425','0','HUF','0','225','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('324','SOM 190S','186000','0','HUF','0','300','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('325','SOM 190SD','204600','0','HUF','0','330','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('326','SOM 210F','372000','0','HUF','0','600','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('327','SOM 230F','387500','0','HUF','0','625','EUR','1','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('328','SOM 230FS','337900','0','HUF','0','545','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('329','SOM 420FTD','675800','0','HUF','0','1090','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('330','MAD 250 FD','560232','0','HUF','0','1004','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('331','MAD 300 FD','627192','0','HUF','0','1124','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('332','MAD 350 FD','694710','0','HUF','0','1245','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('333','MAD 250 FS','536238','0','HUF','0','961','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('334','MAD 300 FS','583110','0','HUF','0','1045','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('335','MAD 350 FS','644490','0','HUF','0','1155','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('336','Lengőajtó','108810','0','HUF','0','195','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('337','Forgókereszt','153295','0','HUF','0','215','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('338','Autómata lengőajtó','1311300','0','HUF','0','2350','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('339','Korlát','24552','0','HUF','0','48','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('340','Plexifal','51662','0','HUF','0','101','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('341','Kocsiütköző','29667','0','HUF','0','58','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('342','Kosárkocsi','9982','7990','HUF','1','14','EUR','0','0','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('343','Bevásárlókosár','1023','890','HUF','1','2','EUR','0','0','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('344','Bevásárlókocsi 75L','21762','0','HUF','0','39','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('345','Bevásárlókocsi 100L','25420','0','HUF','0','41','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('346','Vágólap','10230','0','HUF','0','20','EUR','0','1','3','30','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('347','SLK16','9300','0','HUF','0','15','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('348','SLK18','9920','0','HUF','0','16','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('349','Slager','42455','0','HUF','0','83','EUR','0','1','3','29','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('350','Hústőke','86955','0','HUF','0','170','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('351','Késtartó','27110','0','HUF','0','53','EUR','0','1','3','31','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('352','Húsfogas','56777','0','HUF','0','111','EUR','0','1','3','31','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('353','Elszívóernyő','193859','0','HUF','0','379','EUR','0','1','3','31','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('354','TASEP','39897','0','HUF','0','78','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('355','TASHAT','67518','0','HUF','0','132','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('356','TASKAL','128898','0','HUF','0','252','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('357','TASKKO','161634','0','HUF','0','316','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('358','TASAD','147312','0','HUF','0','288','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('359','TASPAL','100766','0','HUF','0','197','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('360','TASAT','75735','0','HUF','0','148','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('361','TASATÜ','162657','0','HUF','0','318','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('362','TASZAL','145778','0','HUF','0','285','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('363','TASZSZ','144755','0','HUF','0','283','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('364','TASKER','56265','0','HUF','0','110','EUR','0','1','3','22','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('365','LSG-03_N','169818','0','HUF','0','332','EUR','0','1','3','11','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('366','KLARA/KlLAUDIA MP','191813','359900','HUF','1','375','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('367','KLARA P','933488','359900','HUF','1','1825','EUR','0','1','3','13','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('368','Topaz 220','102300','0','HUF','0','200','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('369','HS 10','89001','0','HUF','0','174','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('370','Topaz 275','149358','0','HUF','0','292','EUR','1','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('371','HS 12','136571','0','HUF','0','267','EUR','0','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('372','ANNIVERSARIO 300','953948','0','HUF','0','1865','EUR','1','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('373','TC 8 Vegas','98208','0','HUF','0','192','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('374','G 81','143732','0','HUF','0','281','EUR','1','1','3','15','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('375','TC 22 E','207158','0','HUF','0','405','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('376','TC 22 ICE','567765','0','HUF','0','1110','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('377','UV 16 W','116622','0','HUF','0','228','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('378','SVKF','100254','0','HUF','0','196','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('379','Kézi fóliázó 45K','64449','0','HUF','0','126','EUR','0','1','3','32','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('380','unis HotSpot','151915','0','HUF','0','297','EUR','0','1','3','33','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('381','unis Loire','137082','0','HUF','0','268','EUR','0','1','3','33','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('382','EVO 60 HOT','441936','0','HUF','0','864','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('383','EVO 90 HOT','496666','0','HUF','0','971','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('384','EVO 120 HOT','607150','0','HUF','0','1187','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('385','EVO 150 HOT','662392','0','HUF','0','1295','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('386','EVO 180 HOT','827607','0','HUF','0','1618','EUR','0','1','3','16','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('387','XVC 305 E Kombi gőzpároló sütő','1151900','0','HUF','0','2252','EUR','0','1','3','34','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('388','XFT 135 Arianna Dynamic Sütőke','348332','0','HUF','0','681','EUR','0','1','3','34','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('389','XLT 135 Kelesztő','295647','0','HUF','0','578','EUR','0','1','3','34','0','0','0','');
INSERT INTO `bos_termekek` VALUES ('390','Special','16880','0','HUF','0','33','EUR','0','1','3','27','0','0','0','');
-- --------------------------------------------------------

--
-- Table structure for table `bos_termekkepek`
--

DROP TABLE IF EXISTS `bos_termekkepek`;

CREATE TABLE `bos_termekkepek` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `termek_id` int(11) NOT NULL,
  `file` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL,
  `tag` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48144 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci COMMENT='bos_termekkepek';

--
-- Dumping data for table `bos_termekkepek`
--

INSERT INTO `bos_termekkepek` VALUES ('1','143','6331fb6e1fbcf51c84f7e88e8db59b59','10','');
INSERT INTO `bos_termekkepek` VALUES ('21','143','9a5b8ab748444a97207c8ca963f5c75c','5','');
INSERT INTO `bos_termekkepek` VALUES ('41','143','0b2e9d89e68a5dbd47a2c25f8d8b25f2','0','');
INSERT INTO `bos_termekkepek` VALUES ('32721','374','04db3983fdef8ef0606f7c6b31502034','5','');
INSERT INTO `bos_termekkepek` VALUES ('921','323','b9603d20e1cbe010f3ed6275a0e3b885','5','');
INSERT INTO `bos_termekkepek` VALUES ('941','323','80baf0c8e70acc8c0a70d5befedf754f','10','');
INSERT INTO `bos_termekkepek` VALUES ('961','326','432446d339ded5b5440e848bfc57eabe','5','');
INSERT INTO `bos_termekkepek` VALUES ('981','326','92276dd8621cd0ab9e93f1e90eb80a2e','10','');
INSERT INTO `bos_termekkepek` VALUES ('1101','126','2794f1ac7a5d4610b2694d8ab3227b8f','5','');
INSERT INTO `bos_termekkepek` VALUES ('1121','126','1beb9d5a0e7509732f885737758a2c59','10','');
INSERT INTO `bos_termekkepek` VALUES ('1141','126','7319ab82e4a5fee3bae412ef5a94277c','15','');
INSERT INTO `bos_termekkepek` VALUES ('3081','301','77ecf511cfd57b357a384e225121dc85','5','');
INSERT INTO `bos_termekkepek` VALUES ('3101','301','3a7903fcebdcf15c702766488ede0488','10','');
INSERT INTO `bos_termekkepek` VALUES ('3121','301','21a24b62571f81fd73d1464b2631b5f9','15','');
INSERT INTO `bos_termekkepek` VALUES ('3141','301','9908b7e0ab13119aa8daccbb7f425e10','20','');
INSERT INTO `bos_termekkepek` VALUES ('3161','301','8a0c5592a55d87bf520ed3a881469aaa','25','');
INSERT INTO `bos_termekkepek` VALUES ('3181','301','8027eb663b4240d37039c413385a404f','30','');
INSERT INTO `bos_termekkepek` VALUES ('3201','301','a05c1f53492cc55ba3e9aa65c1e438e0','35','');
INSERT INTO `bos_termekkepek` VALUES ('3221','301','3c08329f4c8eb2cecf789f9818fc4ca8','40','');
INSERT INTO `bos_termekkepek` VALUES ('3241','301','5bcde2a3cc88273733d6f10080dc6c83','45','');
INSERT INTO `bos_termekkepek` VALUES ('3261','301','7344044c1efdb52cf7f286dd8e3abaab','50','');
INSERT INTO `bos_termekkepek` VALUES ('3281','301','74d1e312f2320de5a6c94d0bece3923e','55','');
INSERT INTO `bos_termekkepek` VALUES ('3301','301','576e03a08751f430c44c7154814b306d','60','');
INSERT INTO `bos_termekkepek` VALUES ('3321','301','6e716efd1ac524bbccbb5b57ee0e452a','65','');
INSERT INTO `bos_termekkepek` VALUES ('43641','336','0a15350bdbbdd4b3bede03a525997ca4','5','');
INSERT INTO `bos_termekkepek` VALUES ('3361','337','c2d351e721e407dc4537f0bd78c92e01','10','');
INSERT INTO `bos_termekkepek` VALUES ('43621','336','0c65bb994d28cd47968deb34d79a468b','5','');
INSERT INTO `bos_termekkepek` VALUES ('3401','337','4dd7b34753e452d6f8e73d6fc31e79e8','20','');
INSERT INTO `bos_termekkepek` VALUES ('43681','341','58f5494e7f958bdb46d4f26821bf1106','5','');
INSERT INTO `bos_termekkepek` VALUES ('3441','336','983150897deed57c48ce319b02828221','10','');
INSERT INTO `bos_termekkepek` VALUES ('43701','341','20c759c0c121dfea2f975bbf4f18f177','5','');
INSERT INTO `bos_termekkepek` VALUES ('3601','340','3c93cafeb2ad97da084cc7312cb9ee02','5','');
INSERT INTO `bos_termekkepek` VALUES ('3561','340','983150897deed57c48ce319b02828221','10','');
INSERT INTO `bos_termekkepek` VALUES ('3621','340','d45959550312221e15fde04690b18acd','5','');
INSERT INTO `bos_termekkepek` VALUES ('3641','340','1287d29d6578e1fdbbd41fc0a38aef10','5','');
INSERT INTO `bos_termekkepek` VALUES ('4481','67','e73083572cc595460842f5090da94662','5','');
INSERT INTO `bos_termekkepek` VALUES ('4501','67','7cf498e42c1a1d06960376beb34127bc','10','');
INSERT INTO `bos_termekkepek` VALUES ('4521','67','bef5b4d93c3ae02ace550506a9a936de','15','');
INSERT INTO `bos_termekkepek` VALUES ('4541','68','c864a97baa32fc0554b5102d620ec56d','5','');
INSERT INTO `bos_termekkepek` VALUES ('4561','68','bef5b4d93c3ae02ace550506a9a936de','10','');
INSERT INTO `bos_termekkepek` VALUES ('4581','68','21aeb50352d97fc73992bc9116df8115','15','');
INSERT INTO `bos_termekkepek` VALUES ('4601','69','0e7fb0a3d658c358b4e5e9561f35099e','5','');
INSERT INTO `bos_termekkepek` VALUES ('4621','69','dc699723d65848c81380563ceb0ce81d','10','');
INSERT INTO `bos_termekkepek` VALUES ('4641','69','bef5b4d93c3ae02ace550506a9a936de','15','');
INSERT INTO `bos_termekkepek` VALUES ('4661','70','71b10b95017ebdaa1984b0ded4c2a173','5','');
INSERT INTO `bos_termekkepek` VALUES ('4681','70','6acce6327a3fddea49decb892ea2a274','10','');
INSERT INTO `bos_termekkepek` VALUES ('4701','70','bef5b4d93c3ae02ace550506a9a936de','15','');
INSERT INTO `bos_termekkepek` VALUES ('4721','71','da5ac2a4989f1ac70e4dec5ae331f9ca','5','');
INSERT INTO `bos_termekkepek` VALUES ('4741','71','78252da47ac569c8142bcd049579ce4b','10','');
INSERT INTO `bos_termekkepek` VALUES ('4761','72','b8f6fff8acb6884fa1b75987fed24bbc','5','');
INSERT INTO `bos_termekkepek` VALUES ('4781','72','d6dcbe060b751f079b5fec60ba8112a4','10','');
INSERT INTO `bos_termekkepek` VALUES ('4801','73','40bf82623caded1cd84332d5e3cf4d72','10','');
INSERT INTO `bos_termekkepek` VALUES ('4821','73','52eea859b07495d44fb844cdcecdf239','5','');
INSERT INTO `bos_termekkepek` VALUES ('4841','74','f9b2c114ad004e341ac06bdb07c62fda','10','');
INSERT INTO `bos_termekkepek` VALUES ('4861','74','98b84f413adb58109496b42ebd1263ff','5','');
INSERT INTO `bos_termekkepek` VALUES ('4881','75','e5fb88b398b042f6cccce46bf3fa53e8','5','');
INSERT INTO `bos_termekkepek` VALUES ('4901','75','a9387e43e1fe08c9f4f43718bc2e0405','10','');
INSERT INTO `bos_termekkepek` VALUES ('4921','76','e5fb88b398b042f6cccce46bf3fa53e8','5','');
INSERT INTO `bos_termekkepek` VALUES ('4941','76','a9387e43e1fe08c9f4f43718bc2e0405','10','');
INSERT INTO `bos_termekkepek` VALUES ('4961','77','e5fb88b398b042f6cccce46bf3fa53e8','5','');
INSERT INTO `bos_termekkepek` VALUES ('4981','77','a9387e43e1fe08c9f4f43718bc2e0405','10','');
INSERT INTO `bos_termekkepek` VALUES ('5001','78','e5fb88b398b042f6cccce46bf3fa53e8','5','');
INSERT INTO `bos_termekkepek` VALUES ('5021','78','a9387e43e1fe08c9f4f43718bc2e0405','10','');
INSERT INTO `bos_termekkepek` VALUES ('44181','24','490c466578d8e581e6d28c7161e4acbf','5','');
INSERT INTO `bos_termekkepek` VALUES ('44201','24','afed8f46ba4083785a3e0a3ccf9b8ed0','5','');
INSERT INTO `bos_termekkepek` VALUES ('44121','23','c4c991c9060046f09535273ef0f126fd','5','');
INSERT INTO `bos_termekkepek` VALUES ('44141','23','1e0c883de22f484fe68d3e575e15fe27','5','');
INSERT INTO `bos_termekkepek` VALUES ('5141','342','718572810891feeb6e075caf65eecfce','5','');
INSERT INTO `bos_termekkepek` VALUES ('5221','343','bb5b42e968d61f4ae1d97ab633b3a614','10','');
INSERT INTO `bos_termekkepek` VALUES ('5241','343','a6f719b7a659e2c6c892de9babd93a87','0','');
INSERT INTO `bos_termekkepek` VALUES ('5261','343','6762c47d8938a9d95578ddba03262823','5','');
INSERT INTO `bos_termekkepek` VALUES ('5281','285','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5301','285','21d356cd6aaf25b41e41c022217c5a96','10','');
INSERT INTO `bos_termekkepek` VALUES ('5321','286','bf99ef9164ac828f4bef7a63710757b9','5','');
INSERT INTO `bos_termekkepek` VALUES ('5341','286','28e18b594736efb84994152c28c52eef','10','');
INSERT INTO `bos_termekkepek` VALUES ('5361','287','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5381','287','d18bae1a26ca78b3f8b2212c7c7d101a','10','');
INSERT INTO `bos_termekkepek` VALUES ('5401','288','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5421','288','4dc78ee9d84aeec573c4179447b5f17c','10','');
INSERT INTO `bos_termekkepek` VALUES ('5441','289','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5461','289','1c88fe0eabd5c0ef9290b0a1d42bc1f9','10','');
INSERT INTO `bos_termekkepek` VALUES ('5481','290','2c8a557a834267717040ff8438558dd0','5','');
INSERT INTO `bos_termekkepek` VALUES ('5501','290','28e18b594736efb84994152c28c52eef','10','');
INSERT INTO `bos_termekkepek` VALUES ('5521','291','e723e2ae3e04e8028e119ee592e81974','5','');
INSERT INTO `bos_termekkepek` VALUES ('5541','291','28e18b594736efb84994152c28c52eef','10','');
INSERT INTO `bos_termekkepek` VALUES ('5561','292','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5581','292','3998f40ab605512d599d8b96550e084d','10','');
INSERT INTO `bos_termekkepek` VALUES ('5601','293','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5621','293','bdaccd7781386e6e39630d62b39afc40','10','');
INSERT INTO `bos_termekkepek` VALUES ('5641','294','aac266d74e1180bb04ba13164276ea9b','5','');
INSERT INTO `bos_termekkepek` VALUES ('5661','294','28e18b594736efb84994152c28c52eef','10','');
INSERT INTO `bos_termekkepek` VALUES ('5681','295','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5701','295','623232dcd9011b444a405ae114194245','10','');
INSERT INTO `bos_termekkepek` VALUES ('5721','296','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5741','297','7e9da216f3bfda8c024b94938291ebec','5','');
INSERT INTO `bos_termekkepek` VALUES ('5761','297','28e18b594736efb84994152c28c52eef','10','');
INSERT INTO `bos_termekkepek` VALUES ('5781','298','28e18b594736efb84994152c28c52eef','5','');
INSERT INTO `bos_termekkepek` VALUES ('5801','298','441d9b1d721e29971edc93bca39bf497','10','');
INSERT INTO `bos_termekkepek` VALUES ('5821','299','ce49b7c62930aa818781c9ff64e5d84b','5','');
INSERT INTO `bos_termekkepek` VALUES ('5841','299','28e18b594736efb84994152c28c52eef','10','');
INSERT INTO `bos_termekkepek` VALUES ('5861','254','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('5881','254','a40a93295a0510b2b3af685ceebcf696','10','');
INSERT INTO `bos_termekkepek` VALUES ('5901','390','82f4ec0886a346b2e2b220e1e805b22f','5','');
INSERT INTO `bos_termekkepek` VALUES ('6021','355','41f0c8fdffd77076dbeabebc5b2d07d6','5','');
INSERT INTO `bos_termekkepek` VALUES ('6041','355','9a7524e7a36bca7712594545d17d2160','10','');
INSERT INTO `bos_termekkepek` VALUES ('7401','359','7d3b76212a8ebdd03d45fc49a95a9868','5','');
INSERT INTO `bos_termekkepek` VALUES ('6081','356','4ae37d4f2d2dcd1f1083dc9fb9134ddc','5','');
INSERT INTO `bos_termekkepek` VALUES ('6101','356','e9d63785b2a16eadf652149a2b0ff708','10','');
INSERT INTO `bos_termekkepek` VALUES ('6121','356','ab002bd7abf55a71372511eeb199ecb6','15','');
INSERT INTO `bos_termekkepek` VALUES ('6141','356','949b6216f42ab06201c488deab78781f','20','');
INSERT INTO `bos_termekkepek` VALUES ('6161','356','769b8cf23a5869a604f8ae1416fc48ad','5','');
INSERT INTO `bos_termekkepek` VALUES ('6361','358','4ae37d4f2d2dcd1f1083dc9fb9134ddc','0','');
INSERT INTO `bos_termekkepek` VALUES ('6381','358','e9d63785b2a16eadf652149a2b0ff708','15','');
INSERT INTO `bos_termekkepek` VALUES ('6401','358','ab002bd7abf55a71372511eeb199ecb6','5','');
INSERT INTO `bos_termekkepek` VALUES ('6421','358','949b6216f42ab06201c488deab78781f','10','');
INSERT INTO `bos_termekkepek` VALUES ('6441','358','73793822d8a993bbc5251e65353ce47a','20','');
INSERT INTO `bos_termekkepek` VALUES ('6461','358','f70287c490d07c052ba0c18e14cf82a1','25','');
INSERT INTO `bos_termekkepek` VALUES ('6481','358','9a65d8f681eb161006ac959a570600f5','30','');
INSERT INTO `bos_termekkepek` VALUES ('6501','358','06a0bb9fa9ef795fd7323ad683fc6e0d','35','');
INSERT INTO `bos_termekkepek` VALUES ('6521','358','205225be28612c89833b9716728fefb6','40','');
INSERT INTO `bos_termekkepek` VALUES ('7381','355','faf79d0e0a1721a77cc0dba9ac8b844a','5','');
INSERT INTO `bos_termekkepek` VALUES ('7501','362','146ff1b077e7729999ed0b299a2fe639','5','');
INSERT INTO `bos_termekkepek` VALUES ('7521','362','d6f8d124087ad4c23fe66b89b7893523','10','');
INSERT INTO `bos_termekkepek` VALUES ('7541','362','4e342d4579c83b48ac6b9ed83a646fcb','15','');
INSERT INTO `bos_termekkepek` VALUES ('7561','364','146ff1b077e7729999ed0b299a2fe639','5','');
INSERT INTO `bos_termekkepek` VALUES ('7581','364','d6f8d124087ad4c23fe66b89b7893523','10','');
INSERT INTO `bos_termekkepek` VALUES ('7601','364','4e342d4579c83b48ac6b9ed83a646fcb','15','');
INSERT INTO `bos_termekkepek` VALUES ('7621','354','d662dad6fed28c1ffbe0ef9e988de483','5','');
INSERT INTO `bos_termekkepek` VALUES ('7921','245','4e806c7fb6d8b19ad12794e816eecd34','5','');
INSERT INTO `bos_termekkepek` VALUES ('7941','245','4183587d53e7f375e1f2a1a83bfe306f','10','');
INSERT INTO `bos_termekkepek` VALUES ('8081','242','e9805f420cf6313f17a4d807871d9938','5','');
INSERT INTO `bos_termekkepek` VALUES ('8101','242','656fc9196df1a741238703d3f1c21039','10','');
INSERT INTO `bos_termekkepek` VALUES ('8121','242','de1e7f6da2c60b9bb6768ba10c8ebc28','15','');
INSERT INTO `bos_termekkepek` VALUES ('8141','102','0d1afd58f8213eaf99c8de8734547678','0','');
INSERT INTO `bos_termekkepek` VALUES ('8161','102','37d7465c1cf6b226541c17d5b92034c1','5','');
INSERT INTO `bos_termekkepek` VALUES ('8181','102','40b11f9780532e8b86ba9087df3dcdb6','10','');
INSERT INTO `bos_termekkepek` VALUES ('8201','102','ce49d66b1ddfeb732c56810c4736e3c3','15','');
INSERT INTO `bos_termekkepek` VALUES ('8221','103','8fa7814fed3b1ec42b1c7f857f2dd23f','0','');
INSERT INTO `bos_termekkepek` VALUES ('8241','103','37d7465c1cf6b226541c17d5b92034c1','5','');
INSERT INTO `bos_termekkepek` VALUES ('8261','103','bd2ecba368839b7447db9829ba015e98','10','');
INSERT INTO `bos_termekkepek` VALUES ('8281','103','aa814f45de0c09c92ca1ef778932bdd5','15','');
INSERT INTO `bos_termekkepek` VALUES ('8301','98','37d7465c1cf6b226541c17d5b92034c1','5','');
INSERT INTO `bos_termekkepek` VALUES ('28281','98','fda1e6e7b8c6a594746d60ff48746f12','5','');
INSERT INTO `bos_termekkepek` VALUES ('8341','98','16cc44cd8988e0bf8db384192210c37c','10','');
INSERT INTO `bos_termekkepek` VALUES ('8361','98','fe264dded3eaff218eec0176afde2232','15','');
INSERT INTO `bos_termekkepek` VALUES ('8461','106','f8f23e20475cd553ead1be9c1b031d05','0','');
INSERT INTO `bos_termekkepek` VALUES ('8481','106','37d7465c1cf6b226541c17d5b92034c1','5','');
INSERT INTO `bos_termekkepek` VALUES ('8501','106','32614465d9c058b477fb7aeefed4ead8','10','');
INSERT INTO `bos_termekkepek` VALUES ('8561','107','5282e962ed024c738ce3e1e2b0a247ec','0','');
INSERT INTO `bos_termekkepek` VALUES ('8541','106','65378614c4adbc65cc1643a8bd221e59','15','');
INSERT INTO `bos_termekkepek` VALUES ('8581','107','37d7465c1cf6b226541c17d5b92034c1','5','');
INSERT INTO `bos_termekkepek` VALUES ('8601','107','89fee0513b6668e555959f5dc23238e9','10','');
INSERT INTO `bos_termekkepek` VALUES ('8621','107','ed45e77e699645c84f3f27b7b5ad1e2d','15','');
INSERT INTO `bos_termekkepek` VALUES ('44241','25','37a02e4012f8ec6fc59da19e05d983e3','5','');
INSERT INTO `bos_termekkepek` VALUES ('44261','25','ab33c4de989d08f6474dbc9f945bfd64','5','');
INSERT INTO `bos_termekkepek` VALUES ('44301','26','c1bcf3eecbb395e92353ea6331c948b3','5','');
INSERT INTO `bos_termekkepek` VALUES ('44321','26','41d98b54063973ecbdcbe845ff38ff95','5','');
INSERT INTO `bos_termekkepek` VALUES ('44401','28','25315d7e8e32b1441d08e0663d3e3187','5','');
INSERT INTO `bos_termekkepek` VALUES ('8761','71','ea517968df45ef4db6d4be40cbf898f4','0','');
INSERT INTO `bos_termekkepek` VALUES ('8781','72','62c4b9a1d1083cb06f39c0034e17309b','0','');
INSERT INTO `bos_termekkepek` VALUES ('8801','73','8aad11ad0535421ef52a534d5cce4bd3','0','');
INSERT INTO `bos_termekkepek` VALUES ('8821','74','940fc9d61a6bff3bf77cd4ae012f40b8','0','');
INSERT INTO `bos_termekkepek` VALUES ('8861','225','129da3cf7fd2aaf9c9a665461361fe55','5','');
INSERT INTO `bos_termekkepek` VALUES ('8881','225','3efc323699f126dd1a672e4d84090556','10','');
INSERT INTO `bos_termekkepek` VALUES ('8901','226','129da3cf7fd2aaf9c9a665461361fe55','5','');
INSERT INTO `bos_termekkepek` VALUES ('8921','226','3efc323699f126dd1a672e4d84090556','10','');
INSERT INTO `bos_termekkepek` VALUES ('8941','227','129da3cf7fd2aaf9c9a665461361fe55','5','');
INSERT INTO `bos_termekkepek` VALUES ('8961','227','3efc323699f126dd1a672e4d84090556','10','');
INSERT INTO `bos_termekkepek` VALUES ('8981','228','129da3cf7fd2aaf9c9a665461361fe55','5','');
INSERT INTO `bos_termekkepek` VALUES ('9001','228','3efc323699f126dd1a672e4d84090556','10','');
INSERT INTO `bos_termekkepek` VALUES ('9021','60','a9bd54659f4a895624ba4a669a7ca977','5','');
INSERT INTO `bos_termekkepek` VALUES ('9041','60','4d046659e1ad67b44d425b5a1266bd1f','10','');
INSERT INTO `bos_termekkepek` VALUES ('9061','60','ad19862debc08ee50a8788bb8bf41dcf','15','');
INSERT INTO `bos_termekkepek` VALUES ('9081','60','615730155070075d9a84ea6b707df2d9','20','');
INSERT INTO `bos_termekkepek` VALUES ('9101','60','a136b041dea2aa81140d7d02690cb0cd','25','');
INSERT INTO `bos_termekkepek` VALUES ('9361','123','5a55d6ee22db450394f6f4ff698ce7f9','5','');
INSERT INTO `bos_termekkepek` VALUES ('9381','123','d3630f23e503069de93fd311c61fa7a2','10','');
INSERT INTO `bos_termekkepek` VALUES ('9401','123','62e2b28928b720e72e310d27e17c034b','15','');
INSERT INTO `bos_termekkepek` VALUES ('9421','123','74f4a8b3a417f9509ce5f285f5666a99','20','');
INSERT INTO `bos_termekkepek` VALUES ('9441','123','33e8dc3b7581c60b4a0d05dc7d57ca6d','25','');
INSERT INTO `bos_termekkepek` VALUES ('9461','123','d11ec591b969d3c0d24f85745bca781d','5','');
INSERT INTO `bos_termekkepek` VALUES ('9721','114','e848ee17d7ebb1d2b638fbb55346e950','5','');
INSERT INTO `bos_termekkepek` VALUES ('9741','114','af141196b05c7fe6f8d290c41aa63f96','10','');
INSERT INTO `bos_termekkepek` VALUES ('9761','203','0ebe9b3854a327ccaa19ed6d4ad866ab','5','');
INSERT INTO `bos_termekkepek` VALUES ('9781','204','0ebe9b3854a327ccaa19ed6d4ad866ab','5','');
INSERT INTO `bos_termekkepek` VALUES ('9801','205','0ebe9b3854a327ccaa19ed6d4ad866ab','5','');
INSERT INTO `bos_termekkepek` VALUES ('10001','62','a0dec184f71f5589306a0ce05105d525','0','');
INSERT INTO `bos_termekkepek` VALUES ('10021','62','65aaf6fbf016b5f1db30b039bd629efb','5','');
INSERT INTO `bos_termekkepek` VALUES ('10041','62','789ac257b259758328b164cfe34ba607','40','');
INSERT INTO `bos_termekkepek` VALUES ('10061','62','6c19d516bc5b2b7b1bc254ef98795580','15','');
INSERT INTO `bos_termekkepek` VALUES ('10081','62','118374afdf5025d0612bea7472516433','25','');
INSERT INTO `bos_termekkepek` VALUES ('10101','62','b691888bac47a2e8c14a2f5e4e7562a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('10121','62','e430f779f7ff81de0e5b4c4f5e316672','30','');
INSERT INTO `bos_termekkepek` VALUES ('10141','62','8d0edb3d066ad2480d1f7aa966f0fd52','20','');
INSERT INTO `bos_termekkepek` VALUES ('10161','62','7fe3d16a83f683a0a7f1c029536bebe7','35','');
INSERT INTO `bos_termekkepek` VALUES ('10181','244','3b128f1c635dda6d40b32de6f470b242','5','');
INSERT INTO `bos_termekkepek` VALUES ('10201','244','667cc6c6a80324c92575f13fa843deaf','10','');
INSERT INTO `bos_termekkepek` VALUES ('10221','244','5de402365ca7341b8b1e38b3013768e8','15','');
INSERT INTO `bos_termekkepek` VALUES ('10501','360','0873c683d440633b38beae77d1401248','5','');
INSERT INTO `bos_termekkepek` VALUES ('10521','360','d0f56cc15cd90a7568bd6e0319a0ddbb','10','');
INSERT INTO `bos_termekkepek` VALUES ('10541','29','8442eb8284283fcedcc1d517892d8a0d','5','');
INSERT INTO `bos_termekkepek` VALUES ('10561','29','43e20093467ea41106ceebbb918308c3','10','');
INSERT INTO `bos_termekkepek` VALUES ('10581','29','a59687b426e9eb30465997eb54862a2e','15','');
INSERT INTO `bos_termekkepek` VALUES ('10601','30','8442eb8284283fcedcc1d517892d8a0d','5','');
INSERT INTO `bos_termekkepek` VALUES ('10621','30','df43d6d9d5605203787731b49c5cf8ac','10','');
INSERT INTO `bos_termekkepek` VALUES ('10641','30','ec7ef75f2cd16ee29bc709fd3c655e1e','15','');
INSERT INTO `bos_termekkepek` VALUES ('10661','30','43e20093467ea41106ceebbb918308c3','20','');
INSERT INTO `bos_termekkepek` VALUES ('10681','327','92276dd8621cd0ab9e93f1e90eb80a2e','5','');
INSERT INTO `bos_termekkepek` VALUES ('10701','327','432446d339ded5b5440e848bfc57eabe','10','');
INSERT INTO `bos_termekkepek` VALUES ('10721','328','6eae17727b4e77cfd9d81da1f7176317','5','');
INSERT INTO `bos_termekkepek` VALUES ('10741','328','531d29a813ef9471aad0a5558d449a73','10','');
INSERT INTO `bos_termekkepek` VALUES ('10761','329','ff23b5e3594d0ed1e0e83a7176d2d281','5','');
INSERT INTO `bos_termekkepek` VALUES ('10781','329','8adbd6c3f2280e2aeac1b525830ce976','10','');
INSERT INTO `bos_termekkepek` VALUES ('10801','79','2e255d2d6bf9bb33030246d31f1a79ca','5','');
INSERT INTO `bos_termekkepek` VALUES ('10821','79','ceda11c08b37b8f0f53306428c9b6046','10','');
INSERT INTO `bos_termekkepek` VALUES ('10841','79','e169b4a4f51927e111471c36da8605c9','15','');
INSERT INTO `bos_termekkepek` VALUES ('10861','80','1c54eddf16cb16eacb4e8b2a90c1b852','5','');
INSERT INTO `bos_termekkepek` VALUES ('10881','80','394b41c837196b0dd33240cf24cccbe4','10','');
INSERT INTO `bos_termekkepek` VALUES ('10901','80','e58bdc602126f03fcc5739825d2d6a2f','15','');
INSERT INTO `bos_termekkepek` VALUES ('10921','81','db07748a120b8752dcac49ddfe281b71','5','');
INSERT INTO `bos_termekkepek` VALUES ('10941','81','2f2c8fcf0d0c5fca6b425d66f6649564','10','');
INSERT INTO `bos_termekkepek` VALUES ('10961','81','db117f5cd4c4d7c4612cc00ff8155df5','15','');
INSERT INTO `bos_termekkepek` VALUES ('10981','82','18bbdd93321ecb981eb57b477b75257b','5','');
INSERT INTO `bos_termekkepek` VALUES ('11001','82','73618b43412631e7227113bab86355ee','10','');
INSERT INTO `bos_termekkepek` VALUES ('11021','82','37a21f1772fb76b1151008d6c67a772e','15','');
INSERT INTO `bos_termekkepek` VALUES ('44381','28','4c01db56212bfa9ea334c215774695af','5','');
INSERT INTO `bos_termekkepek` VALUES ('44361','28','b59867ffa3ab0787fe3904e927858be7','5','');
INSERT INTO `bos_termekkepek` VALUES ('11081','2','905d8fc4ffb4275a428a84589810f8f4','5','');
INSERT INTO `bos_termekkepek` VALUES ('11101','2','b2a965a9288be028a20a7dfbb4ef1ffd','10','');
INSERT INTO `bos_termekkepek` VALUES ('11121','2','cef2694016492d408fa157b7c59ce741','15','');
INSERT INTO `bos_termekkepek` VALUES ('11141','165','fa12a7143c24200577be53e74c33f9f6','5','');
INSERT INTO `bos_termekkepek` VALUES ('11161','165','0c43b6b380576955a4f8f2e4ed896878','10','');
INSERT INTO `bos_termekkepek` VALUES ('11181','165','3bf2494248dce446936191f05c9fc152','15','');
INSERT INTO `bos_termekkepek` VALUES ('11201','165','9d223723152194d11fe071289ed8d699','20','');
INSERT INTO `bos_termekkepek` VALUES ('11921','65','d2c1f1b68f6e19a0eabbce44ab7738dd','5','');
INSERT INTO `bos_termekkepek` VALUES ('11941','65','5b6dc45a5cd0dc5a6088694cfea37a1c','10','');
INSERT INTO `bos_termekkepek` VALUES ('11961','65','8478fe41c80431e5343da65c14312d11','0','');
INSERT INTO `bos_termekkepek` VALUES ('12041','66','5b6dc45a5cd0dc5a6088694cfea37a1c','5','');
INSERT INTO `bos_termekkepek` VALUES ('12101','66','37db35cc291fceddcf807acffb973a7e','5','');
INSERT INTO `bos_termekkepek` VALUES ('12081','66','d2c1f1b68f6e19a0eabbce44ab7738dd','15','');
INSERT INTO `bos_termekkepek` VALUES ('12121','145','acb6ae8893c33e5d6e4137df1af36760','5','');
INSERT INTO `bos_termekkepek` VALUES ('12141','145','818cdd667953a348a3d016040c271ce3','10','');
INSERT INTO `bos_termekkepek` VALUES ('12161','145','67317d6dcc4cb778aeb9219565f5456b','15','');
INSERT INTO `bos_termekkepek` VALUES ('12181','151','bc3f700b666ac5263fa4a0a3ebe37fbd','5','');
INSERT INTO `bos_termekkepek` VALUES ('12201','151','f4019657a0c073ede70a1289e3583b2a','10','');
INSERT INTO `bos_termekkepek` VALUES ('12221','151','a9fa34b067b19c21e33f71497b73ae59','15','');
INSERT INTO `bos_termekkepek` VALUES ('12241','153','02fcf87d7e0a9c73d5cb511f8bae4ba4','5','');
INSERT INTO `bos_termekkepek` VALUES ('12261','153','ded7648311bd3ba8efb02b9b7bce5019','10','');
INSERT INTO `bos_termekkepek` VALUES ('12281','153','974aa39be46f0ba832c70a2bcf9b816c','15','');
INSERT INTO `bos_termekkepek` VALUES ('12661','209','4be6c6e14b64c0916944a181ddcf7f2f','5','');
INSERT INTO `bos_termekkepek` VALUES ('12681','209','20a6de86c60a0c868a0dbb9b048b997e','10','');
INSERT INTO `bos_termekkepek` VALUES ('12701','209','6d089f239d7703f20e2918fc59b82659','15','');
INSERT INTO `bos_termekkepek` VALUES ('12721','209','1074afbd8b5b7eb6576c26ecbf6f7e17','20','');
INSERT INTO `bos_termekkepek` VALUES ('12741','209','c550f8f5e59f449bc3180929a5a391e8','25','');
INSERT INTO `bos_termekkepek` VALUES ('12761','209','2bc4cdc1b8653fb1a7c3213c0a9bf1bd','30','');
INSERT INTO `bos_termekkepek` VALUES ('12781','210','2175420f07d46f9cf2b72ddea1621b62','5','');
INSERT INTO `bos_termekkepek` VALUES ('12801','210','4be6c6e14b64c0916944a181ddcf7f2f','10','');
INSERT INTO `bos_termekkepek` VALUES ('12821','210','20a6de86c60a0c868a0dbb9b048b997e','15','');
INSERT INTO `bos_termekkepek` VALUES ('12841','210','6d089f239d7703f20e2918fc59b82659','20','');
INSERT INTO `bos_termekkepek` VALUES ('12861','210','1074afbd8b5b7eb6576c26ecbf6f7e17','25','');
INSERT INTO `bos_termekkepek` VALUES ('12881','210','2bc4cdc1b8653fb1a7c3213c0a9bf1bd','30','');
INSERT INTO `bos_termekkepek` VALUES ('12901','211','4be6c6e14b64c0916944a181ddcf7f2f','5','');
INSERT INTO `bos_termekkepek` VALUES ('12921','211','20a6de86c60a0c868a0dbb9b048b997e','10','');
INSERT INTO `bos_termekkepek` VALUES ('12941','211','6d089f239d7703f20e2918fc59b82659','15','');
INSERT INTO `bos_termekkepek` VALUES ('12961','211','4257ec66a6e623dd67dd6e9da236dde7','20','');
INSERT INTO `bos_termekkepek` VALUES ('12981','211','1074afbd8b5b7eb6576c26ecbf6f7e17','25','');
INSERT INTO `bos_termekkepek` VALUES ('13001','211','94f79e7c90b87514a07c47bdc5498262','30','');
INSERT INTO `bos_termekkepek` VALUES ('13061','361','0873c683d440633b38beae77d1401248','5','');
INSERT INTO `bos_termekkepek` VALUES ('13081','361','4c58556a4800c5f18316dcfbe00480bd','10','');
INSERT INTO `bos_termekkepek` VALUES ('13101','127','2794f1ac7a5d4610b2694d8ab3227b8f','5','');
INSERT INTO `bos_termekkepek` VALUES ('13121','127','1beb9d5a0e7509732f885737758a2c59','10','');
INSERT INTO `bos_termekkepek` VALUES ('13141','127','7319ab82e4a5fee3bae412ef5a94277c','15','');
INSERT INTO `bos_termekkepek` VALUES ('13161','128','2794f1ac7a5d4610b2694d8ab3227b8f','5','');
INSERT INTO `bos_termekkepek` VALUES ('13181','128','1beb9d5a0e7509732f885737758a2c59','10','');
INSERT INTO `bos_termekkepek` VALUES ('13201','128','7319ab82e4a5fee3bae412ef5a94277c','15','');
INSERT INTO `bos_termekkepek` VALUES ('13221','129','e7803c8c6041d459fe3d7db32af97830','5','');
INSERT INTO `bos_termekkepek` VALUES ('13241','129','aae094199bf30b0b1a58e8ec1bbad5ec','10','');
INSERT INTO `bos_termekkepek` VALUES ('13261','130','e7803c8c6041d459fe3d7db32af97830','5','');
INSERT INTO `bos_termekkepek` VALUES ('13281','130','aae094199bf30b0b1a58e8ec1bbad5ec','10','');
INSERT INTO `bos_termekkepek` VALUES ('13301','131','e7803c8c6041d459fe3d7db32af97830','5','');
INSERT INTO `bos_termekkepek` VALUES ('13321','131','aae094199bf30b0b1a58e8ec1bbad5ec','10','');
INSERT INTO `bos_termekkepek` VALUES ('13381','35','1c157f204d830f6a84149692ad5c8616','5','');
INSERT INTO `bos_termekkepek` VALUES ('13401','35','aded499b1cf2f5ba1eb410d547149e1b','10','');
INSERT INTO `bos_termekkepek` VALUES ('13461','34','1c157f204d830f6a84149692ad5c8616','5','');
INSERT INTO `bos_termekkepek` VALUES ('13481','34','aded499b1cf2f5ba1eb410d547149e1b','10','');
INSERT INTO `bos_termekkepek` VALUES ('13501','41','98a9e02cbbd5cfd0869534f8a0737ff5','5','');
INSERT INTO `bos_termekkepek` VALUES ('13521','41','01f8de319075af0e65c7602fa346ecb2','10','');
INSERT INTO `bos_termekkepek` VALUES ('13541','42','a39ff1fdae1147076e02047ae82a19fc','5','');
INSERT INTO `bos_termekkepek` VALUES ('13561','42','b523f2328fd75b66f513b5dd6861706c','10','');
INSERT INTO `bos_termekkepek` VALUES ('13901','232','cfc55f94d020068c6fd6fd18caea0e3f','5','');
INSERT INTO `bos_termekkepek` VALUES ('13841','232','8ba241716ce430e2731acba8d07d529b','10','');
INSERT INTO `bos_termekkepek` VALUES ('13861','232','0a1643cb73c25878cc845d5594059afe','15','');
INSERT INTO `bos_termekkepek` VALUES ('13881','232','bda085fd5db65533be2d418bd918686d','20','');
INSERT INTO `bos_termekkepek` VALUES ('14521','51','b2a965a9288be028a20a7dfbb4ef1ffd','5','');
INSERT INTO `bos_termekkepek` VALUES ('14541','51','4b58db5ec33af76c6d9b968cf2c633b1','10','');
INSERT INTO `bos_termekkepek` VALUES ('14561','51','cef2694016492d408fa157b7c59ce741','15','');
INSERT INTO `bos_termekkepek` VALUES ('14581','208','e92a88a115051af987d0f10e44033ce3','5','');
INSERT INTO `bos_termekkepek` VALUES ('14601','208','a19e49f070ffa5602842b056bd2964d9','10','');
INSERT INTO `bos_termekkepek` VALUES ('14621','208','d2d14a860e8d76ddc192d8be47463544','15','');
INSERT INTO `bos_termekkepek` VALUES ('14681','238','fce40f257a6c2f9bcbd0b1f562a8e276','5','');
INSERT INTO `bos_termekkepek` VALUES ('14741','239','fce40f257a6c2f9bcbd0b1f562a8e276','5','');
INSERT INTO `bos_termekkepek` VALUES ('18761','320','9faddc753dc477e17c8b4b9a8bfb42b1','5','');
INSERT INTO `bos_termekkepek` VALUES ('18781','320','449ad65422325dbcba5862ad5cfca903','10','');
INSERT INTO `bos_termekkepek` VALUES ('18801','320','1a81f073643813cbfc8102ccb388441e','15','');
INSERT INTO `bos_termekkepek` VALUES ('18821','320','a373c07bd3673e17df1e31cd86fa93e3','20','');
INSERT INTO `bos_termekkepek` VALUES ('18841','320','3dec3407148ccbc0c48bad5418f5204a','25','');
INSERT INTO `bos_termekkepek` VALUES ('18861','320','0f3afa3c17dfb95a44c848293b3d586b','30','');
INSERT INTO `bos_termekkepek` VALUES ('18881','320','2e277351c5cfe6dd5bd87b2457a7e1eb','35','');
INSERT INTO `bos_termekkepek` VALUES ('18901','320','bbdaf7580f685c49f0e3e9eaf4615ea1','40','');
INSERT INTO `bos_termekkepek` VALUES ('18921','320','c8bfde373852a0ec6e7e532fd157a12f','45','');
INSERT INTO `bos_termekkepek` VALUES ('18941','320','12edb5b1e9edb971c7e2102673ca7deb','50','');
INSERT INTO `bos_termekkepek` VALUES ('18961','320','757b627a7a9540cbd65ae7197baac7d3','55','');
INSERT INTO `bos_termekkepek` VALUES ('18981','320','2fcca6a13dee046d59bd81a9169f34cb','60','');
INSERT INTO `bos_termekkepek` VALUES ('19001','320','c74f59f3c6b610cc772e2e6f4a0cd124','65','');
INSERT INTO `bos_termekkepek` VALUES ('19021','320','16a359f1f323bca3b0448e6c8ca5bec5','70','');
INSERT INTO `bos_termekkepek` VALUES ('19041','320','2008991cede0af8b4054c72f988f8188','75','');
INSERT INTO `bos_termekkepek` VALUES ('19061','320','bfe6aba8566250a3c320b69138822c03','80','');
INSERT INTO `bos_termekkepek` VALUES ('19081','320','530e685ac1c17f4c13ba184841f66796','85','');
INSERT INTO `bos_termekkepek` VALUES ('19101','320','b89bd6465f1baed1810a82e1d5138f52','90','');
INSERT INTO `bos_termekkepek` VALUES ('19121','320','479886b201b5274accd44efb87c70f2e','95','');
INSERT INTO `bos_termekkepek` VALUES ('19141','320','abd19437d3541945cf100fc11a7e5436','100','');
INSERT INTO `bos_termekkepek` VALUES ('19161','320','be556b501dc49b5734041cb373f7ab7b','105','');
INSERT INTO `bos_termekkepek` VALUES ('19181','320','f42c61b0473deca6a719dfce6c235d8f','110','');
INSERT INTO `bos_termekkepek` VALUES ('19201','320','7bd68c5248f3487b5837947038e2f943','115','');
INSERT INTO `bos_termekkepek` VALUES ('19221','320','843ef058d8c8f5b123fb13f457421178','120','');
INSERT INTO `bos_termekkepek` VALUES ('19241','320','f9512b5be28b1d47d48a0a63309bcc7c','125','');
INSERT INTO `bos_termekkepek` VALUES ('19261','320','533aa9666119d578b635c3bdba609070','130','');
INSERT INTO `bos_termekkepek` VALUES ('19281','320','de661ab890dd45d89ba4a9af195b9790','135','');
INSERT INTO `bos_termekkepek` VALUES ('19301','320','5e86cdd17f928cd2a20e2f946dac5131','140','');
INSERT INTO `bos_termekkepek` VALUES ('19321','320','e7f735ea46ddb653a04b88c3c665081c','145','');
INSERT INTO `bos_termekkepek` VALUES ('19341','320','da245a4af8136e7be100396f1b96ccce','150','');
INSERT INTO `bos_termekkepek` VALUES ('19361','320','84793143cb08280d709ba235b51a594b','155','');
INSERT INTO `bos_termekkepek` VALUES ('19381','320','5da9cf9625826af0d8fd2452c33ad396','160','');
INSERT INTO `bos_termekkepek` VALUES ('19401','320','dab38893f0089a6152a59c1de1b47f09','165','');
INSERT INTO `bos_termekkepek` VALUES ('19421','320','ce34d8eb34760996a3869cef77b01035','170','');
INSERT INTO `bos_termekkepek` VALUES ('19441','320','a8681f254add165d857ccb664b802bed','175','');
INSERT INTO `bos_termekkepek` VALUES ('19461','320','5620424ee670016be3005849e9af9852','180','');
INSERT INTO `bos_termekkepek` VALUES ('19481','320','22a8b63c93772c6b53efc3e83f3b0ebd','185','');
INSERT INTO `bos_termekkepek` VALUES ('19501','320','e85ca00d008a532279b798033d59a4c7','190','');
INSERT INTO `bos_termekkepek` VALUES ('19521','320','00fd113df54e13a1b01bb11732ab481c','195','');
INSERT INTO `bos_termekkepek` VALUES ('19541','320','c6c828fe0f5a2e500f3cb4cf061b1a9f','200','');
INSERT INTO `bos_termekkepek` VALUES ('19641','140','3cf3794a55e9b9732d01bf82af5bb95f','5','');
INSERT INTO `bos_termekkepek` VALUES ('19661','140','5898493fbdf4a44a24084021b2215f85','10','');
INSERT INTO `bos_termekkepek` VALUES ('19681','141','b9d61450739210f3d3f6e122a43ddb11','5','');
INSERT INTO `bos_termekkepek` VALUES ('19701','141','bafe9fd85b8a42c45540de606eb57534','0','');
INSERT INTO `bos_termekkepek` VALUES ('19761','142','74aaa962157fc60f660a8cc3f8e36631','5','');
INSERT INTO `bos_termekkepek` VALUES ('19781','142','13c5761fbc4d10bc361221c281f84190','10','');
INSERT INTO `bos_termekkepek` VALUES ('19861','138','8f6a92c9e94d3c1a373eb9d5f064e51c','5','');
INSERT INTO `bos_termekkepek` VALUES ('19901','138','fb0de27f4631e4caa3061839dcebeadb','10','');
INSERT INTO `bos_termekkepek` VALUES ('19941','138','01021c638c5f1920e5e9a5f9e43f6e5e','5','');
INSERT INTO `bos_termekkepek` VALUES ('20321','132','e36c1f9f67dcf2a9fb4cfc006658015e','20','');
INSERT INTO `bos_termekkepek` VALUES ('20341','132','9050b680935e31aa1e5d17c291741c0d','5','');
INSERT INTO `bos_termekkepek` VALUES ('20361','132','908c5a804c204a58e06d8f7f0b7e53b7','25','');
INSERT INTO `bos_termekkepek` VALUES ('20381','132','7928608b9e9836d0071cc77fcc0f91dd','0','');
INSERT INTO `bos_termekkepek` VALUES ('20401','132','f02960e7904985af88da8437958f4c10','10','');
INSERT INTO `bos_termekkepek` VALUES ('20421','132','83ac958ce3c9798d09d96e94002c42e8','15','');
INSERT INTO `bos_termekkepek` VALUES ('20461','133','e36c1f9f67dcf2a9fb4cfc006658015e','20','');
INSERT INTO `bos_termekkepek` VALUES ('20481','133','9050b680935e31aa1e5d17c291741c0d','5','');
INSERT INTO `bos_termekkepek` VALUES ('20501','133','908c5a804c204a58e06d8f7f0b7e53b7','25','');
INSERT INTO `bos_termekkepek` VALUES ('20521','133','0ff1a2dc87696229db870be7c0781faa','0','');
INSERT INTO `bos_termekkepek` VALUES ('20541','133','f02960e7904985af88da8437958f4c10','15','');
INSERT INTO `bos_termekkepek` VALUES ('20561','133','83ac958ce3c9798d09d96e94002c42e8','10','');
INSERT INTO `bos_termekkepek` VALUES ('20581','181','b270a720f6ac2e8a8c53d968243d5971','5','');
INSERT INTO `bos_termekkepek` VALUES ('20601','181','73de7f96f98fced11e229d691a3cbea0','10','');
INSERT INTO `bos_termekkepek` VALUES ('20621','182','b270a720f6ac2e8a8c53d968243d5971','5','');
INSERT INTO `bos_termekkepek` VALUES ('20641','182','73de7f96f98fced11e229d691a3cbea0','10','');
INSERT INTO `bos_termekkepek` VALUES ('20781','231','9cc7ab76cfb05575445a34ce0075c8d5','10','');
INSERT INTO `bos_termekkepek` VALUES ('20801','231','15d8fddbc12a7572e1001984365f8706','15','');
INSERT INTO `bos_termekkepek` VALUES ('20821','231','2634bac3da54e82b0271f50421ee5315','5','');
INSERT INTO `bos_termekkepek` VALUES ('20841','231','af41a0aae1fa2017102ee97ff68cdd20','20','');
INSERT INTO `bos_termekkepek` VALUES ('20861','231','4461853cc6a73bc75d45fe1ea6e8ee6c','25','');
INSERT INTO `bos_termekkepek` VALUES ('20881','231','b8feb3cce1b12e57ed29dba92abb169a','0','');
INSERT INTO `bos_termekkepek` VALUES ('20901','230','9cc7ab76cfb05575445a34ce0075c8d5','10','');
INSERT INTO `bos_termekkepek` VALUES ('20921','230','15d8fddbc12a7572e1001984365f8706','15','');
INSERT INTO `bos_termekkepek` VALUES ('20941','230','2634bac3da54e82b0271f50421ee5315','5','');
INSERT INTO `bos_termekkepek` VALUES ('20961','230','af41a0aae1fa2017102ee97ff68cdd20','20','');
INSERT INTO `bos_termekkepek` VALUES ('20981','230','4461853cc6a73bc75d45fe1ea6e8ee6c','25','');
INSERT INTO `bos_termekkepek` VALUES ('21001','230','b8feb3cce1b12e57ed29dba92abb169a','0','');
INSERT INTO `bos_termekkepek` VALUES ('21021','229','9cc7ab76cfb05575445a34ce0075c8d5','10','');
INSERT INTO `bos_termekkepek` VALUES ('21041','229','15d8fddbc12a7572e1001984365f8706','15','');
INSERT INTO `bos_termekkepek` VALUES ('21061','229','2634bac3da54e82b0271f50421ee5315','5','');
INSERT INTO `bos_termekkepek` VALUES ('21081','229','af41a0aae1fa2017102ee97ff68cdd20','20','');
INSERT INTO `bos_termekkepek` VALUES ('21101','229','4461853cc6a73bc75d45fe1ea6e8ee6c','25','');
INSERT INTO `bos_termekkepek` VALUES ('21121','229','b8feb3cce1b12e57ed29dba92abb169a','0','');
INSERT INTO `bos_termekkepek` VALUES ('21141','233','8a07e4382e18e3b9f5d2713aeaefc29b','5','');
INSERT INTO `bos_termekkepek` VALUES ('21161','233','82f59cadb0056787dfc4f95ac5811796','10','');
INSERT INTO `bos_termekkepek` VALUES ('21181','378','626b94bd7e595a66bf544b3d07f41bb9','5','');
INSERT INTO `bos_termekkepek` VALUES ('21201','378','14752c7c1b57bd53e30ce5a4cb88c341','5','');
INSERT INTO `bos_termekkepek` VALUES ('21221','377','19e6fdde211a88280424205b456ef721','5','');
INSERT INTO `bos_termekkepek` VALUES ('21241','377','4fd0ba0316294fb5cdcdbd2c1a6abf6f','0','');
INSERT INTO `bos_termekkepek` VALUES ('21261','379','00c391f380538eb146b161f180f811d0','10','');
INSERT INTO `bos_termekkepek` VALUES ('21281','379','166140a231d9a62facf0ca268fda457e','0','');
INSERT INTO `bos_termekkepek` VALUES ('21301','379','01b3296a89ddb57db8da17f47e7296b3','5','');
INSERT INTO `bos_termekkepek` VALUES ('21321','373','ac94c8c5bbec7eaa3e5762abc650e3c6','5','');
INSERT INTO `bos_termekkepek` VALUES ('21341','373','08ba7ce35158279b64af606fcbd6a2ce','5','');
INSERT INTO `bos_termekkepek` VALUES ('21361','83','d281adf33ecbc93e6d6364146dbfa92d','5','');
INSERT INTO `bos_termekkepek` VALUES ('21381','83','62b0a7e14e737518e647951018fcdaa6','10','');
INSERT INTO `bos_termekkepek` VALUES ('21401','84','c4b96e147d004941235298b2e5fb678f','5','');
INSERT INTO `bos_termekkepek` VALUES ('21421','84','7afe9aa70ec82fd1c9909b7cacb885ca','10','');
INSERT INTO `bos_termekkepek` VALUES ('21441','85','ac281a4f6613b8639870ae23016bbaeb','5','');
INSERT INTO `bos_termekkepek` VALUES ('21461','85','1e8eb8eba52caf6af665a6e147d246fa','10','');
INSERT INTO `bos_termekkepek` VALUES ('21541','357','e74ffccbab44ed6be7f6167684652aaa','5','');
INSERT INTO `bos_termekkepek` VALUES ('21561','357','26f442e45f3939150bec252fdc0f6466','10','');
INSERT INTO `bos_termekkepek` VALUES ('21581','357','92f812afafa48c6f79dbf10ea1c4e901','15','');
INSERT INTO `bos_termekkepek` VALUES ('21641','7','6e1fc0524f19fbb70d837cfe1501816c','5','');
INSERT INTO `bos_termekkepek` VALUES ('21661','7','493eeb5203de0ef597b766a30b21b8c5','10','');
INSERT INTO `bos_termekkepek` VALUES ('21681','36','985bcfab2400f00bfc2d3dfd8127e7e3','5','');
INSERT INTO `bos_termekkepek` VALUES ('21701','36','aded499b1cf2f5ba1eb410d547149e1b','10','');
INSERT INTO `bos_termekkepek` VALUES ('21721','37','d035fdf8b4a9e8b98b9d51e116b49b02','5','');
INSERT INTO `bos_termekkepek` VALUES ('21741','37','aded499b1cf2f5ba1eb410d547149e1b','10','');
INSERT INTO `bos_termekkepek` VALUES ('21761','38','0169cf885f882efd795951253db5cdfb','5','');
INSERT INTO `bos_termekkepek` VALUES ('21781','38','aded499b1cf2f5ba1eb410d547149e1b','10','');
INSERT INTO `bos_termekkepek` VALUES ('21801','39','7e4a87d1535b45ecbf1bdcc74aeae875','5','');
INSERT INTO `bos_termekkepek` VALUES ('21821','39','aded499b1cf2f5ba1eb410d547149e1b','10','');
INSERT INTO `bos_termekkepek` VALUES ('21841','40','aded499b1cf2f5ba1eb410d547149e1b','5','');
INSERT INTO `bos_termekkepek` VALUES ('21861','40','7f55132ae44ba21448a1a87050ac8f55','10','');
INSERT INTO `bos_termekkepek` VALUES ('21881','43','8c8da6cfc8dd7cf27862c09d9c803cfc','5','');
INSERT INTO `bos_termekkepek` VALUES ('21901','43','4d955d1b3361aadcee7b34c1a48ef872','10','');
INSERT INTO `bos_termekkepek` VALUES ('21921','44','3165cf660e87b79d662570cbc26b7946','5','');
INSERT INTO `bos_termekkepek` VALUES ('21941','44','4fd5e819b37dfc2766335f1ae045acd5','10','');
INSERT INTO `bos_termekkepek` VALUES ('21961','45','0bfad113286cf6b1bc6dedbdbfc7e5ef','5','');
INSERT INTO `bos_termekkepek` VALUES ('21981','45','3a03f9afd886282d8d1de4e0af465056','10','');
INSERT INTO `bos_termekkepek` VALUES ('22001','46','173e097f39844e299a745824e4c90e04','5','');
INSERT INTO `bos_termekkepek` VALUES ('22021','46','4ccc3735e387537e61269a976a33e412','10','');
INSERT INTO `bos_termekkepek` VALUES ('22041','47','979119b54f7ce1f122d43e04e113a75a','5','');
INSERT INTO `bos_termekkepek` VALUES ('22061','47','e29f5fdb115d065b9b4b576ecd1e5e53','10','');
INSERT INTO `bos_termekkepek` VALUES ('22321','48','105c713f26aceeeb177effaba81c5768','5','');
INSERT INTO `bos_termekkepek` VALUES ('22341','48','ad1caccf91fd561f8efdff685362a85e','10','');
INSERT INTO `bos_termekkepek` VALUES ('22361','48','78e74fe725904ecf96a7fc1f6df0c645','15','');
INSERT INTO `bos_termekkepek` VALUES ('22381','48','76c5322248041c96e5e7677ace8b8cc0','20','');
INSERT INTO `bos_termekkepek` VALUES ('22961','50','ed123099e7f76388ae6c3a83ba7c6b80','5','');
INSERT INTO `bos_termekkepek` VALUES ('22981','50','2202cd8f3d1cd174f6675fb138f652e3','10','');
INSERT INTO `bos_termekkepek` VALUES ('23001','50','0b76fdf0c77a7c5b8b15b0eeb68109b8','15','');
INSERT INTO `bos_termekkepek` VALUES ('23021','50','a6115ed32394915aac1e5502382eaaea','20','');
INSERT INTO `bos_termekkepek` VALUES ('23041','50','765812833515fce06c971624f85391da','25','');
INSERT INTO `bos_termekkepek` VALUES ('23061','50','8ce13ccd4a1b7b88606ba6a8dd415458','30','');
INSERT INTO `bos_termekkepek` VALUES ('23081','50','e4f13ffe472cb84c019250bd53fa5e9b','35','');
INSERT INTO `bos_termekkepek` VALUES ('23101','94','18c41d036d6bc0a2715563dbbc56b94a','5','');
INSERT INTO `bos_termekkepek` VALUES ('23121','94','3aa6ec91cddc6c724c24c73f92d21187','10','');
INSERT INTO `bos_termekkepek` VALUES ('23141','94','97c216cb25ce4c47de15d030c76fed39','15','');
INSERT INTO `bos_termekkepek` VALUES ('23161','95','42ec8f73f8d06c8cbe768098a3103ab3','5','');
INSERT INTO `bos_termekkepek` VALUES ('23181','95','4fc4c179fdfac274861a90eb3c70aeef','10','');
INSERT INTO `bos_termekkepek` VALUES ('23201','95','da9a8522f58d2fd43abc9e2b48a33622','15','');
INSERT INTO `bos_termekkepek` VALUES ('23221','325','d6f8ccbfd742460eb9ddd463c75aea2f','5','');
INSERT INTO `bos_termekkepek` VALUES ('23241','325','b4a24c9a34cbb53b49c920b905f38d29','10','');
INSERT INTO `bos_termekkepek` VALUES ('23261','368','ab222ca8f6a6c105a02ddf6658156235','5','');
INSERT INTO `bos_termekkepek` VALUES ('23301','370','19fadb255d5df879f17dc1cfb7d9969d','5','');
INSERT INTO `bos_termekkepek` VALUES ('32761','371','869d6fd58354e007e30a3ee36ab8652b','5','');
INSERT INTO `bos_termekkepek` VALUES ('23341','375','e4dcab5f7c33b62eb67f0a87c1b7f162','5','');
INSERT INTO `bos_termekkepek` VALUES ('23361','376','493733b1ea397d196293bbf12b839a4f','5','');
INSERT INTO `bos_termekkepek` VALUES ('23441','324','80baf0c8e70acc8c0a70d5befedf754f','5','');
INSERT INTO `bos_termekkepek` VALUES ('23461','324','8aa25d565a8891e68d123138a137622b','10','');
INSERT INTO `bos_termekkepek` VALUES ('23481','13','7da972d166ecdad370f47f20610e2e2d','10','');
INSERT INTO `bos_termekkepek` VALUES ('23501','13','d571a56e0ed10b01d720ea67e9de7bea','5','');
INSERT INTO `bos_termekkepek` VALUES ('23521','13','2d0122e6c17cdb89f8eed4d75b5f5eef','0','');
INSERT INTO `bos_termekkepek` VALUES ('23601','14','5cefe77fe6363fc905997de406a0cd1e','0','');
INSERT INTO `bos_termekkepek` VALUES ('23621','14','b53c8d02b0d527947d06491f519e0fa4','10','');
INSERT INTO `bos_termekkepek` VALUES ('23641','14','16dd8c942ad630be7e5a12b681b3f5c4','5','');
INSERT INTO `bos_termekkepek` VALUES ('23721','15','4223a1d5b9e017dda51515829140e5d2','5','');
INSERT INTO `bos_termekkepek` VALUES ('23741','15','95906345e6ac0203ccdc16a51898187e','10','');
INSERT INTO `bos_termekkepek` VALUES ('23761','15','fa509843f54361a33291efe5c6c53c4d','15','');
INSERT INTO `bos_termekkepek` VALUES ('23781','16','90a562851e9222030339fcf2960c15e9','10','');
INSERT INTO `bos_termekkepek` VALUES ('23801','16','1f10c04f2ec5040b16199ed7795f19cf','5','');
INSERT INTO `bos_termekkepek` VALUES ('23821','16','0a187866618ca3049030ec5014860ae8','0','');
INSERT INTO `bos_termekkepek` VALUES ('23901','17','4abb77b282507cc565dfa37484cbf038','5','');
INSERT INTO `bos_termekkepek` VALUES ('23921','17','69ccd1088c36016d7983c64adc7d9fe1','10','');
INSERT INTO `bos_termekkepek` VALUES ('23941','17','f0b92cf017c046813ace40ba56a14ee9','15','');
INSERT INTO `bos_termekkepek` VALUES ('24021','18','8d4e7eab99697d70f46a556ec9f8f966','10','');
INSERT INTO `bos_termekkepek` VALUES ('24041','18','94c5a9b3adc9c654138dc89cf909b127','0','');
INSERT INTO `bos_termekkepek` VALUES ('24061','18','648e9e6a126696bd6f0eaf62b2b222b0','5','');
INSERT INTO `bos_termekkepek` VALUES ('24121','20','f26b07a1c7a408950e2bcf9c6b354e0c','5','');
INSERT INTO `bos_termekkepek` VALUES ('24141','20','98e45e4601a11c8439a61cf258f3651a','10','');
INSERT INTO `bos_termekkepek` VALUES ('24301','8','2f886ce468cb42271d41a90b6473cc18','5','');
INSERT INTO `bos_termekkepek` VALUES ('24321','8','c699a38d88e2499b4e47f02534c2a4d8','5','');
INSERT INTO `bos_termekkepek` VALUES ('24381','9','e7e04cfae8c198b3a50727d2717db734','5','');
INSERT INTO `bos_termekkepek` VALUES ('24401','9','86e1b797dc3cd603d6ae96f6812ca3bf','10','');
INSERT INTO `bos_termekkepek` VALUES ('26001','197','754ded87195c267f7fe891cf33c9feb6','10','');
INSERT INTO `bos_termekkepek` VALUES ('24821','108','2f3680790ac607007e3443a317871dd5','5','');
INSERT INTO `bos_termekkepek` VALUES ('24841','108','ddaf8b7602e9011c04f95ad0bdf57c8f','10','');
INSERT INTO `bos_termekkepek` VALUES ('24861','109','2f3680790ac607007e3443a317871dd5','5','');
INSERT INTO `bos_termekkepek` VALUES ('24881','109','ddaf8b7602e9011c04f95ad0bdf57c8f','10','');
INSERT INTO `bos_termekkepek` VALUES ('24901','110','2f3680790ac607007e3443a317871dd5','5','');
INSERT INTO `bos_termekkepek` VALUES ('24921','110','ddaf8b7602e9011c04f95ad0bdf57c8f','10','');
INSERT INTO `bos_termekkepek` VALUES ('24941','111','af141196b05c7fe6f8d290c41aa63f96','5','');
INSERT INTO `bos_termekkepek` VALUES ('24961','111','7a7539d744846dbafd1824a15c5687ad','10','');
INSERT INTO `bos_termekkepek` VALUES ('24981','112','af141196b05c7fe6f8d290c41aa63f96','5','');
INSERT INTO `bos_termekkepek` VALUES ('25001','112','8f0ba28049c871e3c7f552a32affdbe5','10','');
INSERT INTO `bos_termekkepek` VALUES ('25021','113','af141196b05c7fe6f8d290c41aa63f96','5','');
INSERT INTO `bos_termekkepek` VALUES ('25041','113','0595e5860fe9c5eaba82fb617087dbc1','10','');
INSERT INTO `bos_termekkepek` VALUES ('25061','115','11fdda320001f8432cb19623193ec2f9','5','');
INSERT INTO `bos_termekkepek` VALUES ('25081','115','af141196b05c7fe6f8d290c41aa63f96','10','');
INSERT INTO `bos_termekkepek` VALUES ('25101','116','af141196b05c7fe6f8d290c41aa63f96','5','');
INSERT INTO `bos_termekkepek` VALUES ('25121','116','00a61ba99599c66d87f80d1de9ebd899','10','');
INSERT INTO `bos_termekkepek` VALUES ('25301','6','8e2055cb2b794f699c7587b98cf88c38','5','');
INSERT INTO `bos_termekkepek` VALUES ('25321','6','85c07abe4ea6242ad08034b14a75b6db','10','');
INSERT INTO `bos_termekkepek` VALUES ('25541','52','5912d89494eb7cc28287049165307ddb','5','');
INSERT INTO `bos_termekkepek` VALUES ('25561','52','fecd1185063ed3d28b657ef4816b63f6','10','');
INSERT INTO `bos_termekkepek` VALUES ('25921','3','d2804bda02d05ee5868227fb24f13c98','5','');
INSERT INTO `bos_termekkepek` VALUES ('25901','3','6278a917f19213013188be0c57c12d44','5','');
INSERT INTO `bos_termekkepek` VALUES ('26021','197','3df416f5b8103355883cfd1517442cda','5','');
INSERT INTO `bos_termekkepek` VALUES ('26041','197','dcc8b2489fd82c46ed902c7552525414','0','');
INSERT INTO `bos_termekkepek` VALUES ('26181','201','c193e32491586a336b2db8341da77a15','5','');
INSERT INTO `bos_termekkepek` VALUES ('26201','187','f7633dd32138e8ff73ba5b69c7e0c88d','5','');
INSERT INTO `bos_termekkepek` VALUES ('26221','202','5a98da640ec400d67adb408d22ce5c9c','5','');
INSERT INTO `bos_termekkepek` VALUES ('26241','184','9d1c2483c8e11d3f64e65e627d6a459c','5','');
INSERT INTO `bos_termekkepek` VALUES ('26261','183','5033fd9ca925f86dbd50b35dcdd85206','5','');
INSERT INTO `bos_termekkepek` VALUES ('26281','186','ba368424b735e1761bc78ea814268bc3','5','');
INSERT INTO `bos_termekkepek` VALUES ('26361','200','87f7c4c6c31849f6804f22cfff870d99','5','');
INSERT INTO `bos_termekkepek` VALUES ('26581','215','90797bef9ef6175e04f3c9383568f9e4','5','');
INSERT INTO `bos_termekkepek` VALUES ('26601','216','b6693cd29e780a2f56a0d941917e8af6','5','');
INSERT INTO `bos_termekkepek` VALUES ('26621','217','a1e1f162f218dfeee8c0eb887f0c57d0','5','');
INSERT INTO `bos_termekkepek` VALUES ('39741','218','4190cb18abc26e8c4382caa68b252687','5','');
INSERT INTO `bos_termekkepek` VALUES ('39721','218','cfdf55d9c4687ca7b7d9b2d2b997135f','5','');
INSERT INTO `bos_termekkepek` VALUES ('26681','155','ca24ace054f22ad5ec0f5314f4f32ca2','5','');
INSERT INTO `bos_termekkepek` VALUES ('26701','155','bc3f700b666ac5263fa4a0a3ebe37fbd','10','');
INSERT INTO `bos_termekkepek` VALUES ('26721','155','f4019657a0c073ede70a1289e3583b2a','15','');
INSERT INTO `bos_termekkepek` VALUES ('26741','199','b3a8c9d7b4dd2c400ce3f7776f1f6cb8','5','');
INSERT INTO `bos_termekkepek` VALUES ('26761','255','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('26781','255','02d796bfb1ab131efd76050ff25865b6','10','');
INSERT INTO `bos_termekkepek` VALUES ('26801','256','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('26821','256','01a23971d24c25a9473cbb80bd9c118d','10','');
INSERT INTO `bos_termekkepek` VALUES ('26841','257','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('26861','258','3582e8234e81952b380296ff26883377','5','');
INSERT INTO `bos_termekkepek` VALUES ('26881','258','f5ab4352328f18b3442fbff3d786f186','10','');
INSERT INTO `bos_termekkepek` VALUES ('26901','260','8d618f48515500cd702cc305ee80acf1','5','');
INSERT INTO `bos_termekkepek` VALUES ('26921','260','f5ab4352328f18b3442fbff3d786f186','10','');
INSERT INTO `bos_termekkepek` VALUES ('26941','259','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('26961','259','d1e132d76b543eff0acb36102d018e12','10','');
INSERT INTO `bos_termekkepek` VALUES ('26981','261','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('27001','261','8ad23dfe1f0141fde8b615539dcd9a66','10','');
INSERT INTO `bos_termekkepek` VALUES ('27021','262','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('27041','262','49041135ae59391cc2adc1fa5c821fb2','10','');
INSERT INTO `bos_termekkepek` VALUES ('27061','263','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('27081','263','f4b27d5d47e27996af75486f6045a8a6','10','');
INSERT INTO `bos_termekkepek` VALUES ('27101','264','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('27121','264','2963dbc550404970787514aa177006e8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27141','265','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('27161','265','e775a20f0553c08010a113c9c67de544','10','');
INSERT INTO `bos_termekkepek` VALUES ('27181','266','f5ab4352328f18b3442fbff3d786f186','5','');
INSERT INTO `bos_termekkepek` VALUES ('27201','266','b699556d36dd386da7c02d83f28686fb','10','');
INSERT INTO `bos_termekkepek` VALUES ('27221','267','f990518da54698e33be2ea01d6b75f59','5','');
INSERT INTO `bos_termekkepek` VALUES ('27241','267','f5ab4352328f18b3442fbff3d786f186','10','');
INSERT INTO `bos_termekkepek` VALUES ('27261','268','21b9e7a8e9be5e75ba5c5b3af5ec3aa3','5','');
INSERT INTO `bos_termekkepek` VALUES ('27281','268','f5ab4352328f18b3442fbff3d786f186','10','');
INSERT INTO `bos_termekkepek` VALUES ('27301','269','7fa66f426416d30c0c885937fed3c9d1','5','');
INSERT INTO `bos_termekkepek` VALUES ('27321','269','f5ab4352328f18b3442fbff3d786f186','10','');
INSERT INTO `bos_termekkepek` VALUES ('27341','270','de2b376e9223ad63d79032a7116a8b54','5','');
INSERT INTO `bos_termekkepek` VALUES ('27361','270','1ede38c6bf62e43dd7ecb9f2bd7417a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27381','271','de2b376e9223ad63d79032a7116a8b54','5','');
INSERT INTO `bos_termekkepek` VALUES ('27401','271','1ede38c6bf62e43dd7ecb9f2bd7417a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27421','272','de2b376e9223ad63d79032a7116a8b54','5','');
INSERT INTO `bos_termekkepek` VALUES ('27441','272','1ede38c6bf62e43dd7ecb9f2bd7417a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27461','273','de2b376e9223ad63d79032a7116a8b54','5','');
INSERT INTO `bos_termekkepek` VALUES ('27481','273','1ede38c6bf62e43dd7ecb9f2bd7417a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27501','274','2fc174708d449ce2801b2216593ca986','5','');
INSERT INTO `bos_termekkepek` VALUES ('27521','274','a5d64b88793021e15f6f29410e83b3dc','10','');
INSERT INTO `bos_termekkepek` VALUES ('27541','275','2fc174708d449ce2801b2216593ca986','5','');
INSERT INTO `bos_termekkepek` VALUES ('27561','275','a5d64b88793021e15f6f29410e83b3dc','10','');
INSERT INTO `bos_termekkepek` VALUES ('27581','276','2fc174708d449ce2801b2216593ca986','5','');
INSERT INTO `bos_termekkepek` VALUES ('27601','276','a5d64b88793021e15f6f29410e83b3dc','10','');
INSERT INTO `bos_termekkepek` VALUES ('27621','277','2411e31f3aff4338fd974ce32d705a4b','5','');
INSERT INTO `bos_termekkepek` VALUES ('27641','277','18a147f0aa6f87e25b54716a534e719d','10','');
INSERT INTO `bos_termekkepek` VALUES ('27661','284','2411e31f3aff4338fd974ce32d705a4b','5','');
INSERT INTO `bos_termekkepek` VALUES ('27681','284','18a147f0aa6f87e25b54716a534e719d','10','');
INSERT INTO `bos_termekkepek` VALUES ('27701','278','de2b376e9223ad63d79032a7116a8b54','5','');
INSERT INTO `bos_termekkepek` VALUES ('27721','278','1ede38c6bf62e43dd7ecb9f2bd7417a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27741','279','de2b376e9223ad63d79032a7116a8b54','5','');
INSERT INTO `bos_termekkepek` VALUES ('27761','279','1ede38c6bf62e43dd7ecb9f2bd7417a8','10','');
INSERT INTO `bos_termekkepek` VALUES ('27781','280','2fc174708d449ce2801b2216593ca986','5','');
INSERT INTO `bos_termekkepek` VALUES ('27801','280','a5d64b88793021e15f6f29410e83b3dc','10','');
INSERT INTO `bos_termekkepek` VALUES ('27821','281','2fc174708d449ce2801b2216593ca986','5','');
INSERT INTO `bos_termekkepek` VALUES ('27841','281','a5d64b88793021e15f6f29410e83b3dc','10','');
INSERT INTO `bos_termekkepek` VALUES ('27861','282','2fc174708d449ce2801b2216593ca986','5','');
INSERT INTO `bos_termekkepek` VALUES ('27881','282','a5d64b88793021e15f6f29410e83b3dc','10','');
INSERT INTO `bos_termekkepek` VALUES ('27901','283','2411e31f3aff4338fd974ce32d705a4b','5','');
INSERT INTO `bos_termekkepek` VALUES ('27921','283','18a147f0aa6f87e25b54716a534e719d','10','');
INSERT INTO `bos_termekkepek` VALUES ('28021','105','367df17877b8e778ae3fdc03213438e3','0','');
INSERT INTO `bos_termekkepek` VALUES ('28041','105','a03fec24df877cc65c037673397ad5c0','35','');
INSERT INTO `bos_termekkepek` VALUES ('28061','105','37d7465c1cf6b226541c17d5b92034c1','40','');
INSERT INTO `bos_termekkepek` VALUES ('28081','105','f902c7bcf5f3eecb256217c1ffe9af43','45','');
INSERT INTO `bos_termekkepek` VALUES ('28501','104','367df17877b8e778ae3fdc03213438e3','0','');
INSERT INTO `bos_termekkepek` VALUES ('28521','104','a03fec24df877cc65c037673397ad5c0','5','');
INSERT INTO `bos_termekkepek` VALUES ('28541','104','37d7465c1cf6b226541c17d5b92034c1','15','');
INSERT INTO `bos_termekkepek` VALUES ('28561','104','f902c7bcf5f3eecb256217c1ffe9af43','10','');
INSERT INTO `bos_termekkepek` VALUES ('29001','5','107387d5d48032f826cb8b095753b055','5','');
INSERT INTO `bos_termekkepek` VALUES ('29021','5','fb29a8d44c020c765bbc8e49335cb083','10','');
INSERT INTO `bos_termekkepek` VALUES ('44781','338','7fdb263344bd6f672fe6320d097150f0','5','');
INSERT INTO `bos_termekkepek` VALUES ('44761','338','a6a8f6bad925fe4167d82a398acc0d10','5','');
INSERT INTO `bos_termekkepek` VALUES ('29261','330','41dbaeec4466f58cd76a81b7add4a279','10','');
INSERT INTO `bos_termekkepek` VALUES ('29281','330','d3e15fa7b836a9643387c99c47c6c03b','0','');
INSERT INTO `bos_termekkepek` VALUES ('29301','330','e4ae5a0d155ae14613e6cd907bf7040a','5','');
INSERT INTO `bos_termekkepek` VALUES ('29321','330','fdfe44e68dc9914b41cab590f0a77ecc','25','');
INSERT INTO `bos_termekkepek` VALUES ('29341','330','9d5f5d96625c457bfc5c97c460a33ac3','15','');
INSERT INTO `bos_termekkepek` VALUES ('29361','330','300e69f72139feaef597540971ab598e','30','');
INSERT INTO `bos_termekkepek` VALUES ('29381','330','ee712b54ff92d404ff6cd6c8f6e41324','35','');
INSERT INTO `bos_termekkepek` VALUES ('29401','330','80a4296f6e5d27fe4df73700906d45f3','20','');
INSERT INTO `bos_termekkepek` VALUES ('29601','333','b840211887fde6a925646799196f9fd6','15','');
INSERT INTO `bos_termekkepek` VALUES ('29621','333','fdfe44e68dc9914b41cab590f0a77ecc','20','');
INSERT INTO `bos_termekkepek` VALUES ('29641','333','ee712b54ff92d404ff6cd6c8f6e41324','25','');
INSERT INTO `bos_termekkepek` VALUES ('29661','333','118f120e664e30d7394651a361a902f2','5','');
INSERT INTO `bos_termekkepek` VALUES ('29681','333','80a4296f6e5d27fe4df73700906d45f3','30','');
INSERT INTO `bos_termekkepek` VALUES ('29701','333','300e69f72139feaef597540971ab598e','35','');
INSERT INTO `bos_termekkepek` VALUES ('29721','333','25778602fbbca77a5b0f193b38cd5e24','10','');
INSERT INTO `bos_termekkepek` VALUES ('29741','333','de11d2d6ee38eb6e66bc4c1b9f9f41ba','0','');
INSERT INTO `bos_termekkepek` VALUES ('29781','334','b840211887fde6a925646799196f9fd6','15','');
INSERT INTO `bos_termekkepek` VALUES ('29801','334','fdfe44e68dc9914b41cab590f0a77ecc','30','');
INSERT INTO `bos_termekkepek` VALUES ('29821','334','ee712b54ff92d404ff6cd6c8f6e41324','25','');
INSERT INTO `bos_termekkepek` VALUES ('29841','334','118f120e664e30d7394651a361a902f2','10','');
INSERT INTO `bos_termekkepek` VALUES ('29861','334','80a4296f6e5d27fe4df73700906d45f3','20','');
INSERT INTO `bos_termekkepek` VALUES ('29881','334','300e69f72139feaef597540971ab598e','35','');
INSERT INTO `bos_termekkepek` VALUES ('29901','334','25778602fbbca77a5b0f193b38cd5e24','5','');
INSERT INTO `bos_termekkepek` VALUES ('29921','334','de11d2d6ee38eb6e66bc4c1b9f9f41ba','0','');
INSERT INTO `bos_termekkepek` VALUES ('29941','335','b840211887fde6a925646799196f9fd6','15','');
INSERT INTO `bos_termekkepek` VALUES ('29961','335','fdfe44e68dc9914b41cab590f0a77ecc','20','');
INSERT INTO `bos_termekkepek` VALUES ('29981','335','ee712b54ff92d404ff6cd6c8f6e41324','25','');
INSERT INTO `bos_termekkepek` VALUES ('30001','335','118f120e664e30d7394651a361a902f2','10','');
INSERT INTO `bos_termekkepek` VALUES ('30021','335','80a4296f6e5d27fe4df73700906d45f3','30','');
INSERT INTO `bos_termekkepek` VALUES ('30041','335','300e69f72139feaef597540971ab598e','35','');
INSERT INTO `bos_termekkepek` VALUES ('30061','335','25778602fbbca77a5b0f193b38cd5e24','5','');
INSERT INTO `bos_termekkepek` VALUES ('30081','335','de11d2d6ee38eb6e66bc4c1b9f9f41ba','0','');
INSERT INTO `bos_termekkepek` VALUES ('30101','332','fdfe44e68dc9914b41cab590f0a77ecc','5','');
INSERT INTO `bos_termekkepek` VALUES ('30121','332','ee712b54ff92d404ff6cd6c8f6e41324','10','');
INSERT INTO `bos_termekkepek` VALUES ('30141','332','41dbaeec4466f58cd76a81b7add4a279','15','');
INSERT INTO `bos_termekkepek` VALUES ('30161','332','80a4296f6e5d27fe4df73700906d45f3','20','');
INSERT INTO `bos_termekkepek` VALUES ('30181','332','300e69f72139feaef597540971ab598e','25','');
INSERT INTO `bos_termekkepek` VALUES ('30201','332','d3e15fa7b836a9643387c99c47c6c03b','30','');
INSERT INTO `bos_termekkepek` VALUES ('30221','332','e4ae5a0d155ae14613e6cd907bf7040a','35','');
INSERT INTO `bos_termekkepek` VALUES ('30241','332','9d5f5d96625c457bfc5c97c460a33ac3','40','');
INSERT INTO `bos_termekkepek` VALUES ('30261','19','f26b07a1c7a408950e2bcf9c6b354e0c','5','');
INSERT INTO `bos_termekkepek` VALUES ('30281','19','98e45e4601a11c8439a61cf258f3651a','10','');
INSERT INTO `bos_termekkepek` VALUES ('30501','164','0c43b6b380576955a4f8f2e4ed896878','5','');
INSERT INTO `bos_termekkepek` VALUES ('30521','164','0035ae72e4bf57671fc69b8cf98d9e09','10','');
INSERT INTO `bos_termekkepek` VALUES ('30541','164','9d223723152194d11fe071289ed8d699','15','');
INSERT INTO `bos_termekkepek` VALUES ('31701','89','59349d7c8145846439782da8c2d78754','5','');
INSERT INTO `bos_termekkepek` VALUES ('31721','89','f1b967e673681c3b9cdbc9c568949344','10','');
INSERT INTO `bos_termekkepek` VALUES ('31741','89','6ef8783d73140a616fc3d27d7330d503','15','');
INSERT INTO `bos_termekkepek` VALUES ('31781','347','857d7f4beefe9d2178bd13fbc24cfe91','5','');
INSERT INTO `bos_termekkepek` VALUES ('31821','349','a99f1451b71fdebc7e8e5904b8958394','5','');
INSERT INTO `bos_termekkepek` VALUES ('31841','348','857d7f4beefe9d2178bd13fbc24cfe91','5','');
INSERT INTO `bos_termekkepek` VALUES ('31861','346','92eefd02ac9805df8339c59abc7186a1','5','');
INSERT INTO `bos_termekkepek` VALUES ('31881','346','9fd3ac60c2e7a7b4b5984fb48bda8492','10','');
INSERT INTO `bos_termekkepek` VALUES ('31901','350','b5e6dcafb5cf4edd8d6b63edb2a26078','5','');
INSERT INTO `bos_termekkepek` VALUES ('31921','351','a241c00a03bc2e135a61c0736b520b45','5','');
INSERT INTO `bos_termekkepek` VALUES ('31941','352','99ee2d61ce22795acc55825466a1150f','5','');
INSERT INTO `bos_termekkepek` VALUES ('31961','352','89e7d05d94977c776e5200e7f24ba989','10','');
INSERT INTO `bos_termekkepek` VALUES ('31981','352','0a5a1e198d8d7139df2137f519c3bf57','15','');
INSERT INTO `bos_termekkepek` VALUES ('32001','146','999bb84ff9a5813ec209176cf121ac13','5','');
INSERT INTO `bos_termekkepek` VALUES ('32021','146','0dd0cf6fd32308b34c6e8b9cb578251f','10','');
INSERT INTO `bos_termekkepek` VALUES ('32041','146','5d5761926807491536627ee274fadad1','15','');
INSERT INTO `bos_termekkepek` VALUES ('32081','160','fce9f6ffd950ba3abd8a1a6ce2d6c7ae','5','');
INSERT INTO `bos_termekkepek` VALUES ('32101','163','ef17814ab770a950071cfdd5c635d5de','5','');
INSERT INTO `bos_termekkepek` VALUES ('32121','162','03f632e8f60a478dfc4f8c8c82f5e8bb','5','');
INSERT INTO `bos_termekkepek` VALUES ('32141','161','29ea5a3acec849579a7a1892738bfb7a','5','');
INSERT INTO `bos_termekkepek` VALUES ('32161','159','5606c4382f50a2c86fa0108c46c9fd32','5','');
INSERT INTO `bos_termekkepek` VALUES ('32181','8','f3b2acfc1259f02c3d3feebbd5b43a12','5','');
INSERT INTO `bos_termekkepek` VALUES ('32201','8','4693dc77a0ebb13fee222ff64a3e0a6b','5','');
INSERT INTO `bos_termekkepek` VALUES ('32221','9','72ab42b79e4ffaf5e7594f9ce9ab0d40','5','');
INSERT INTO `bos_termekkepek` VALUES ('32241','9','42d41a654fbc8ae69d5c1b62a0dbf3cb','5','');
INSERT INTO `bos_termekkepek` VALUES ('32261','10','9b88f2b70675eda29a26e31d70b1b3fe','5','');
INSERT INTO `bos_termekkepek` VALUES ('32281','10','17cdf5a212a12b4fae4690f90d671c89','10','');
INSERT INTO `bos_termekkepek` VALUES ('32301','10','df573a8d8f3728debb2e47e890723d90','5','');
INSERT INTO `bos_termekkepek` VALUES ('32321','10','d54b7c38526426b0db6345a3f0e9aee0','5','');
INSERT INTO `bos_termekkepek` VALUES ('32341','11','26c0307bcec354693a7db0c46f07b85b','5','');
INSERT INTO `bos_termekkepek` VALUES ('32361','11','832e5d231e3545d0900ef685cbb43baa','10','');
INSERT INTO `bos_termekkepek` VALUES ('32381','11','97630c351cefa3de62df5cb6f0dbc033','5','');
INSERT INTO `bos_termekkepek` VALUES ('32401','11','226982ab9123047f22996f01ae642335','5','');
INSERT INTO `bos_termekkepek` VALUES ('32421','12','737db06851aa4224553a728773b50123','5','');
INSERT INTO `bos_termekkepek` VALUES ('32441','12','8ffa6292a6f801969c7c652f514514c1','10','');
INSERT INTO `bos_termekkepek` VALUES ('32461','12','989ca0fe3ec0682c7349593ff5feb4a4','5','');
INSERT INTO `bos_termekkepek` VALUES ('32481','12','27f0b835570a018c935f07f78fc2a5d3','5','');
INSERT INTO `bos_termekkepek` VALUES ('32641','135','0c48d6d0f656b10e4c01ad9f83cbc1e9','5','');
INSERT INTO `bos_termekkepek` VALUES ('32661','137','9a653220a8fe66050eda45db41c3686a','5','');
INSERT INTO `bos_termekkepek` VALUES ('32701','198','71cdbb8ed6294ff8d052c40be6449167','5','');
INSERT INTO `bos_termekkepek` VALUES ('32741','369','0e22aa2a44e7e297c6365f23dbedd92c','5','');
INSERT INTO `bos_termekkepek` VALUES ('32801','365','33f310507cba3f92ef26e1a311998f5d','5','');
INSERT INTO `bos_termekkepek` VALUES ('32901','223','e54eb3dcfb82757f17eafe999d14f97f','5','');
INSERT INTO `bos_termekkepek` VALUES ('32881','223','01b2a470d875b93b22f9029b8ae3eca6','5','');
INSERT INTO `bos_termekkepek` VALUES ('33381','221','a7ae28707e7720133650216b45d0cd70','5','');
INSERT INTO `bos_termekkepek` VALUES ('33401','221','439c56de17208ac25a2abee9beca46f7','10','');
INSERT INTO `bos_termekkepek` VALUES ('33421','221','ba5e54ab9eb48596967bd10302ad0e5b','15','');
INSERT INTO `bos_termekkepek` VALUES ('33501','363','870da5a0930aabe51045bd0a0708a99a','5','');
INSERT INTO `bos_termekkepek` VALUES ('33521','363','8eb3926a025bb86899f452f3dd675f04','10','');
INSERT INTO `bos_termekkepek` VALUES ('33541','363','30fe39681be0abba1a549fd44d5585f7','15','');
INSERT INTO `bos_termekkepek` VALUES ('33621','356','23b3ec0c082bcc9d9b0c4e25989bdd22','5','');
INSERT INTO `bos_termekkepek` VALUES ('33661','358','695553c0c5ba292284a3e1e64da6ed47','5','');
INSERT INTO `bos_termekkepek` VALUES ('33861','387','e5b14c99bcbbec56836bd253c7e3e1a2','5','');
INSERT INTO `bos_termekkepek` VALUES ('33921','388','738d7deb467d69ea5d6c8aacb9613245','5','');
INSERT INTO `bos_termekkepek` VALUES ('33961','389','438be6a47658f47479deb34f558eba4e','5','');
INSERT INTO `bos_termekkepek` VALUES ('34021','344','d1cc779e26f87a95b93365c0dbcf48b1','5','');
INSERT INTO `bos_termekkepek` VALUES ('34041','344','c590b9faf2b899f2c4dfe1a6dbd6d98e','10','');
INSERT INTO `bos_termekkepek` VALUES ('34061','345','d1cc779e26f87a95b93365c0dbcf48b1','5','');
INSERT INTO `bos_termekkepek` VALUES ('34081','345','c590b9faf2b899f2c4dfe1a6dbd6d98e','10','');
INSERT INTO `bos_termekkepek` VALUES ('34261','241','45e0f0d91eb87a71812d4060b66cd113','5','');
INSERT INTO `bos_termekkepek` VALUES ('34281','241','bddd6284cebfb88034d63b4031662957','10','');
INSERT INTO `bos_termekkepek` VALUES ('34381','306','58840eb65da053fbdea5f4d19dd3e00f','5','');
INSERT INTO `bos_termekkepek` VALUES ('34401','306','68a822664a3ea25fc51ef70282d26185','10','');
INSERT INTO `bos_termekkepek` VALUES ('34461','308','d9ead51d7aebbe36e32bef138ab5cbdc','5','');
INSERT INTO `bos_termekkepek` VALUES ('34481','308','51d77b425e84359a1f4b46c585879681','10','');
INSERT INTO `bos_termekkepek` VALUES ('34501','307','6ce77713a2f0322b851806e5cf96d139','5','');
INSERT INTO `bos_termekkepek` VALUES ('34521','307','608b1b8c94c2f574c4cc04470a22c3f8','5','');
INSERT INTO `bos_termekkepek` VALUES ('34541','305','58840eb65da053fbdea5f4d19dd3e00f','5','');
INSERT INTO `bos_termekkepek` VALUES ('34561','305','68a822664a3ea25fc51ef70282d26185','10','');
INSERT INTO `bos_termekkepek` VALUES ('34781','309','75f8f7a9ced8da0635e455405200f2e8','5','');
INSERT INTO `bos_termekkepek` VALUES ('34701','310','58f7f8fa579b1975997cb4392e9eb092','5','');
INSERT INTO `bos_termekkepek` VALUES ('34721','310','912ac8d7c7126069ec3a8d21b8bde572','10','');
INSERT INTO `bos_termekkepek` VALUES ('34801','309','1d388729fedebe69bea4a1c795f49026','10','');
INSERT INTO `bos_termekkepek` VALUES ('34821','311','58f7f8fa579b1975997cb4392e9eb092','5','');
INSERT INTO `bos_termekkepek` VALUES ('34841','311','912ac8d7c7126069ec3a8d21b8bde572','10','');
INSERT INTO `bos_termekkepek` VALUES ('34861','312','58f7f8fa579b1975997cb4392e9eb092','5','');
INSERT INTO `bos_termekkepek` VALUES ('34881','312','912ac8d7c7126069ec3a8d21b8bde572','10','');
INSERT INTO `bos_termekkepek` VALUES ('34901','313','58f7f8fa579b1975997cb4392e9eb092','5','');
INSERT INTO `bos_termekkepek` VALUES ('34921','313','912ac8d7c7126069ec3a8d21b8bde572','10','');
INSERT INTO `bos_termekkepek` VALUES ('34941','314','31334e004c827779be4b47069aea595a','5','');
INSERT INTO `bos_termekkepek` VALUES ('34961','314','41fb0fd43593de7d24448de9f5aea38e','10','');
INSERT INTO `bos_termekkepek` VALUES ('35081','315','2ed2083bf1107ac5e794d2845c1f7bf7','5','');
INSERT INTO `bos_termekkepek` VALUES ('35101','315','3273190d9ea5b34592c382d6aad69908','10','');
INSERT INTO `bos_termekkepek` VALUES ('35121','315','144fdd8be8005ab7206deaaedc515e71','15','');
INSERT INTO `bos_termekkepek` VALUES ('35141','315','4f50a1a15316ea047b71e9aa486dd334','20','');
INSERT INTO `bos_termekkepek` VALUES ('35161','315','d3e33e64b206201c0146f0ab71a89178','25','');
INSERT INTO `bos_termekkepek` VALUES ('35201','316','cc2ec533f2190f0d1df93d7c5b87c85e','5','');
INSERT INTO `bos_termekkepek` VALUES ('35221','316','2ed2083bf1107ac5e794d2845c1f7bf7','10','');
INSERT INTO `bos_termekkepek` VALUES ('35241','316','144fdd8be8005ab7206deaaedc515e71','15','');
INSERT INTO `bos_termekkepek` VALUES ('35261','316','4f50a1a15316ea047b71e9aa486dd334','20','');
INSERT INTO `bos_termekkepek` VALUES ('35281','316','d3e33e64b206201c0146f0ab71a89178','25','');
INSERT INTO `bos_termekkepek` VALUES ('35341','317','2ed2083bf1107ac5e794d2845c1f7bf7','5','');
INSERT INTO `bos_termekkepek` VALUES ('35361','317','144fdd8be8005ab7206deaaedc515e71','10','');
INSERT INTO `bos_termekkepek` VALUES ('35381','317','4f50a1a15316ea047b71e9aa486dd334','15','');
INSERT INTO `bos_termekkepek` VALUES ('35401','317','c33b41e89e420a81275220cbe2cf603c','20','');
INSERT INTO `bos_termekkepek` VALUES ('35421','317','d3e33e64b206201c0146f0ab71a89178','25','');
INSERT INTO `bos_termekkepek` VALUES ('35441','318','2ed2083bf1107ac5e794d2845c1f7bf7','5','');
INSERT INTO `bos_termekkepek` VALUES ('35461','318','e6baae661f54db3aa1d9f76b2c893882','10','');
INSERT INTO `bos_termekkepek` VALUES ('35481','318','144fdd8be8005ab7206deaaedc515e71','15','');
INSERT INTO `bos_termekkepek` VALUES ('35501','318','4f50a1a15316ea047b71e9aa486dd334','20','');
INSERT INTO `bos_termekkepek` VALUES ('35521','318','d3e33e64b206201c0146f0ab71a89178','25','');
INSERT INTO `bos_termekkepek` VALUES ('35561','319','cc65a7dd40f9de3463468921203d2177','5','');
INSERT INTO `bos_termekkepek` VALUES ('35581','319','2ed2083bf1107ac5e794d2845c1f7bf7','10','');
INSERT INTO `bos_termekkepek` VALUES ('35601','319','144fdd8be8005ab7206deaaedc515e71','15','');
INSERT INTO `bos_termekkepek` VALUES ('35621','319','4f50a1a15316ea047b71e9aa486dd334','20','');
INSERT INTO `bos_termekkepek` VALUES ('35641','319','d3e33e64b206201c0146f0ab71a89178','25','');
INSERT INTO `bos_termekkepek` VALUES ('35761','303','5c9b5c47258cf1499c2dc64b7072e735','5','');
INSERT INTO `bos_termekkepek` VALUES ('35781','303','013e18a7ba1d69bb9612e3f12fc31270','10','');
INSERT INTO `bos_termekkepek` VALUES ('35801','303','99687949804f7bd0a24a825122001657','15','');
INSERT INTO `bos_termekkepek` VALUES ('35821','303','b6094ce2d81a4ceef4996f3f0ea04635','20','');
INSERT INTO `bos_termekkepek` VALUES ('35841','303','602ff07380a89e6ad6a167ee9faa822d','25','');
INSERT INTO `bos_termekkepek` VALUES ('35861','304','5c9b5c47258cf1499c2dc64b7072e735','5','');
INSERT INTO `bos_termekkepek` VALUES ('35881','304','013e18a7ba1d69bb9612e3f12fc31270','10','');
INSERT INTO `bos_termekkepek` VALUES ('35901','304','99687949804f7bd0a24a825122001657','15','');
INSERT INTO `bos_termekkepek` VALUES ('35921','304','b6094ce2d81a4ceef4996f3f0ea04635','20','');
INSERT INTO `bos_termekkepek` VALUES ('35941','304','602ff07380a89e6ad6a167ee9faa822d','25','');
INSERT INTO `bos_termekkepek` VALUES ('36221','300','b56c83155c640bf87af59210d57da3a9','5','');
INSERT INTO `bos_termekkepek` VALUES ('36241','300','a6334341d81fb047fb5dedb10148891a','10','');
INSERT INTO `bos_termekkepek` VALUES ('36261','300','23c84f1d392d1453b7a1e380bd6acd0a','15','');
INSERT INTO `bos_termekkepek` VALUES ('36281','300','998331528fa83423269d7650120521a9','20','');
INSERT INTO `bos_termekkepek` VALUES ('36301','300','234058f7d692a0bfc8b3e2c2190d6cb0','25','');
INSERT INTO `bos_termekkepek` VALUES ('36321','300','2513677eb88849cbcb7db9aa4971d5ed','30','');
INSERT INTO `bos_termekkepek` VALUES ('36341','300','74105d373a71b517ed650caabb9c2cb8','35','');
INSERT INTO `bos_termekkepek` VALUES ('36361','300','0791cd7b835e1bdc3996e6689f55f66d','40','');
INSERT INTO `bos_termekkepek` VALUES ('36381','300','166c34b3cd41bb05714da5ee00ecebbd','45','');
INSERT INTO `bos_termekkepek` VALUES ('36401','300','91cb24d2002c6a0558e557a9138e112f','50','');
INSERT INTO `bos_termekkepek` VALUES ('36421','300','3d506847947985f1615169e670d9ec3d','55','');
INSERT INTO `bos_termekkepek` VALUES ('36441','300','1e74f51b4cd0aaa6c3e0b77c71851ea7','60','');
INSERT INTO `bos_termekkepek` VALUES ('36461','300','b8cb83e13975b41b54dd67ad22984d83','65','');
INSERT INTO `bos_termekkepek` VALUES ('36721','21','f26b07a1c7a408950e2bcf9c6b354e0c','5','');
INSERT INTO `bos_termekkepek` VALUES ('36741','21','98e45e4601a11c8439a61cf258f3651a','10','');
INSERT INTO `bos_termekkepek` VALUES ('38681','366','57ea3f20b28beb898c7b3aa14d6973eb','5','');
INSERT INTO `bos_termekkepek` VALUES ('38701','366','811cf46d61c9ae564bf7fa4b5abc639b','10','');
INSERT INTO `bos_termekkepek` VALUES ('38721','366','26a468ededb15b85ca1e561c3f59bb66','15','');
INSERT INTO `bos_termekkepek` VALUES ('38741','366','d71b5bc5076243ac22b8e30d988b67ae','20','');
INSERT INTO `bos_termekkepek` VALUES ('38761','366','cdc77d46ae05abc0e240ee9be06dd0fa','25','');
INSERT INTO `bos_termekkepek` VALUES ('38781','366','ab243ac315c8f9c55254cbafe079b3c5','30','');
INSERT INTO `bos_termekkepek` VALUES ('38801','366','6f5025169fec8d015379f68364f3da3b','35','');
INSERT INTO `bos_termekkepek` VALUES ('38821','366','10903e7bf25d8788d2fa8409bc964e18','40','');
INSERT INTO `bos_termekkepek` VALUES ('38841','366','6da5e300f4ba3a07eda6b9ffa8b62166','45','');
INSERT INTO `bos_termekkepek` VALUES ('38861','366','36855ae97e2115133b421f92b664e5da','50','');
INSERT INTO `bos_termekkepek` VALUES ('38881','366','47cec7c0f18dcae6c157ede4849cbbf6','55','');
INSERT INTO `bos_termekkepek` VALUES ('38901','366','b2b15ebd5efb07739c0327a9300ded7e','60','');
INSERT INTO `bos_termekkepek` VALUES ('38921','366','13ec9935e17e00bed6ec8f06230e33a9','65','');
INSERT INTO `bos_termekkepek` VALUES ('39061','134','ba6b5587aa7cfdb5ea922e018a419426','5','');
INSERT INTO `bos_termekkepek` VALUES ('39081','134','f452fb191386e040c8dbb8e6e90da113','10','');
INSERT INTO `bos_termekkepek` VALUES ('39101','134','abecf276bb4e9462a32b1a3fa3bee18e','15','');
INSERT INTO `bos_termekkepek` VALUES ('39301','90','6b247abc8be9f278b078413009da554b','5','');
INSERT INTO `bos_termekkepek` VALUES ('39321','90','a992fdba69fe794820cb261f31df434d','10','');
INSERT INTO `bos_termekkepek` VALUES ('39341','90','a348103be8a2dc7ecb06fdfc979cb7bb','15','');
INSERT INTO `bos_termekkepek` VALUES ('39361','91','6b247abc8be9f278b078413009da554b','5','');
INSERT INTO `bos_termekkepek` VALUES ('39381','91','a992fdba69fe794820cb261f31df434d','10','');
INSERT INTO `bos_termekkepek` VALUES ('39401','91','a348103be8a2dc7ecb06fdfc979cb7bb','15','');
INSERT INTO `bos_termekkepek` VALUES ('39541','92','6b247abc8be9f278b078413009da554b','5','');
INSERT INTO `bos_termekkepek` VALUES ('39561','92','a992fdba69fe794820cb261f31df434d','10','');
INSERT INTO `bos_termekkepek` VALUES ('39581','92','a348103be8a2dc7ecb06fdfc979cb7bb','15','');
INSERT INTO `bos_termekkepek` VALUES ('39661','93','6b247abc8be9f278b078413009da554b','5','');
INSERT INTO `bos_termekkepek` VALUES ('39681','93','a992fdba69fe794820cb261f31df434d','10','');
INSERT INTO `bos_termekkepek` VALUES ('39701','93','a348103be8a2dc7ecb06fdfc979cb7bb','15','');
INSERT INTO `bos_termekkepek` VALUES ('39761','218','1b50430453690d6b7e42d983403253dd','5','');
INSERT INTO `bos_termekkepek` VALUES ('39781','218','20a10bf50ab85bd19336937ea36501bd','5','');
INSERT INTO `bos_termekkepek` VALUES ('39801','218','975d9f0ff98ed5bc3f6c862609372b59','5','');
INSERT INTO `bos_termekkepek` VALUES ('39821','218','228ebc0df5ea0340c2f5558d368bdea5','5','');
INSERT INTO `bos_termekkepek` VALUES ('39841','218','2175420f07d46f9cf2b72ddea1621b62','5','');
INSERT INTO `bos_termekkepek` VALUES ('40001','219','20a10bf50ab85bd19336937ea36501bd','5','');
INSERT INTO `bos_termekkepek` VALUES ('40021','219','cfdf55d9c4687ca7b7d9b2d2b997135f','10','');
INSERT INTO `bos_termekkepek` VALUES ('40041','219','2175420f07d46f9cf2b72ddea1621b62','15','');
INSERT INTO `bos_termekkepek` VALUES ('40061','219','4190cb18abc26e8c4382caa68b252687','20','');
INSERT INTO `bos_termekkepek` VALUES ('40081','219','228ebc0df5ea0340c2f5558d368bdea5','25','');
INSERT INTO `bos_termekkepek` VALUES ('40101','219','1b50430453690d6b7e42d983403253dd','30','');
INSERT INTO `bos_termekkepek` VALUES ('40121','219','975d9f0ff98ed5bc3f6c862609372b59','35','');
INSERT INTO `bos_termekkepek` VALUES ('40141','220','20a10bf50ab85bd19336937ea36501bd','5','');
INSERT INTO `bos_termekkepek` VALUES ('40161','220','cfdf55d9c4687ca7b7d9b2d2b997135f','10','');
INSERT INTO `bos_termekkepek` VALUES ('40181','220','2175420f07d46f9cf2b72ddea1621b62','15','');
INSERT INTO `bos_termekkepek` VALUES ('40201','220','4190cb18abc26e8c4382caa68b252687','20','');
INSERT INTO `bos_termekkepek` VALUES ('40221','220','228ebc0df5ea0340c2f5558d368bdea5','25','');
INSERT INTO `bos_termekkepek` VALUES ('40241','220','1b50430453690d6b7e42d983403253dd','30','');
INSERT INTO `bos_termekkepek` VALUES ('40261','220','975d9f0ff98ed5bc3f6c862609372b59','35','');
INSERT INTO `bos_termekkepek` VALUES ('40281','189','20a10bf50ab85bd19336937ea36501bd','5','');
INSERT INTO `bos_termekkepek` VALUES ('40301','189','cfdf55d9c4687ca7b7d9b2d2b997135f','10','');
INSERT INTO `bos_termekkepek` VALUES ('40321','189','2175420f07d46f9cf2b72ddea1621b62','15','');
INSERT INTO `bos_termekkepek` VALUES ('40341','189','4190cb18abc26e8c4382caa68b252687','20','');
INSERT INTO `bos_termekkepek` VALUES ('40361','189','228ebc0df5ea0340c2f5558d368bdea5','25','');
INSERT INTO `bos_termekkepek` VALUES ('40381','189','1b50430453690d6b7e42d983403253dd','30','');
INSERT INTO `bos_termekkepek` VALUES ('40401','189','975d9f0ff98ed5bc3f6c862609372b59','35','');
INSERT INTO `bos_termekkepek` VALUES ('40421','188','20a10bf50ab85bd19336937ea36501bd','5','');
INSERT INTO `bos_termekkepek` VALUES ('40441','188','cfdf55d9c4687ca7b7d9b2d2b997135f','10','');
INSERT INTO `bos_termekkepek` VALUES ('40461','188','2175420f07d46f9cf2b72ddea1621b62','15','');
INSERT INTO `bos_termekkepek` VALUES ('40481','188','4190cb18abc26e8c4382caa68b252687','20','');
INSERT INTO `bos_termekkepek` VALUES ('40501','188','228ebc0df5ea0340c2f5558d368bdea5','25','');
INSERT INTO `bos_termekkepek` VALUES ('40521','188','1b50430453690d6b7e42d983403253dd','30','');
INSERT INTO `bos_termekkepek` VALUES ('40541','188','975d9f0ff98ed5bc3f6c862609372b59','35','');
INSERT INTO `bos_termekkepek` VALUES ('41441','104','0388946ad3ee4d2a9a862fd416fb2589','40','');
INSERT INTO `bos_termekkepek` VALUES ('41361','103','e87cd4b26b86ffbd93b5a6eb2653af38','20','');
INSERT INTO `bos_termekkepek` VALUES ('41261','103','5e9b67564cc86f911da3996df630e09f','25','');
INSERT INTO `bos_termekkepek` VALUES ('41281','103','4476e32b33f766b1d4c85efc0ded3eda','30','');
INSERT INTO `bos_termekkepek` VALUES ('41301','103','6eb34969890feee779f0ab29c3520d46','35','');
INSERT INTO `bos_termekkepek` VALUES ('41321','103','12b931c1ff7bbb2653af0c6d6f019a28','40','');
INSERT INTO `bos_termekkepek` VALUES ('41341','103','9b4f523bc0bbeb448798cf4b49cd1c1a','45','');
INSERT INTO `bos_termekkepek` VALUES ('41421','104','9379ffb164c5becfb0a55e1c0c2f6d37','30','');
INSERT INTO `bos_termekkepek` VALUES ('41381','104','3bcea4dc3e8af48f24fd7627712865e0','20','');
INSERT INTO `bos_termekkepek` VALUES ('41401','104','d716e744f795f9ab363aca5844552539','35','');
INSERT INTO `bos_termekkepek` VALUES ('40881','105','94075db0d38a61eaabe44c25e8bb6423','5','');
INSERT INTO `bos_termekkepek` VALUES ('40901','105','d8ff1cee5ba71c2867f9dbc20b139f38','15','');
INSERT INTO `bos_termekkepek` VALUES ('40921','105','b390a5c96dc3c24d11b40f0226f74379','20','');
INSERT INTO `bos_termekkepek` VALUES ('40941','105','7c9cbbecfe19a9ede7d845d1046ae280','25','');
INSERT INTO `bos_termekkepek` VALUES ('40961','105','89e025a41bec94fd1216ed72743379ef','30','');
INSERT INTO `bos_termekkepek` VALUES ('40981','105','5fb2730cdffd7a73d54cf688412dfca6','10','');
INSERT INTO `bos_termekkepek` VALUES ('41001','106','a79eb2a090f114be0b909879f792ba76','25','');
INSERT INTO `bos_termekkepek` VALUES ('41021','106','204dbb40a6272d96566a58b0b2387bc5','30','');
INSERT INTO `bos_termekkepek` VALUES ('41041','106','92fba86e5806823e61ab77295b2266d6','35','');
INSERT INTO `bos_termekkepek` VALUES ('41061','106','97c5c03913aa3c82d5e0caf0107e390e','40','');
INSERT INTO `bos_termekkepek` VALUES ('41081','106','0a862b7f915e2f79eff460ae76894d50','45','');
INSERT INTO `bos_termekkepek` VALUES ('41101','106','310079ed28ae0df2bf9230b464f7f3bc','20','');
INSERT INTO `bos_termekkepek` VALUES ('41121','107','3ce77534cdf2de47e6ab36e8280dcd39','25','');
INSERT INTO `bos_termekkepek` VALUES ('41141','107','c87d89893794fe71c967c89772630b15','30','');
INSERT INTO `bos_termekkepek` VALUES ('41161','107','fedd8047d1721f2e52b590144837d220','35','');
INSERT INTO `bos_termekkepek` VALUES ('41181','107','cece71e6f3adbf6c3a49f21f265f41f4','40','');
INSERT INTO `bos_termekkepek` VALUES ('41201','107','b40a13d4ebea2ec3499f265928cac1d8','45','');
INSERT INTO `bos_termekkepek` VALUES ('41221','107','b40a13d4ebea2ec3499f265928cac1d8','50','');
INSERT INTO `bos_termekkepek` VALUES ('41241','107','9b50cc0ff1d30ff93ec6451218b12b90','20','');
INSERT INTO `bos_termekkepek` VALUES ('41461','104','05e2f6ab6a650e3b2bfc18fe8c081bbd','45','');
INSERT INTO `bos_termekkepek` VALUES ('41481','104','a2a5e1dc29759ed291e5ba5ecf1d6cbd','25','');
INSERT INTO `bos_termekkepek` VALUES ('41841','5','f11467d555d7202d402c812d3ba2dd06','5','');
INSERT INTO `bos_termekkepek` VALUES ('41861','5','db432a7b6489655715cc2be28787f08c','5','');
INSERT INTO `bos_termekkepek` VALUES ('41881','5','f3945e1de291464abf90ae242e534786','5','');
INSERT INTO `bos_termekkepek` VALUES ('41981','4','c2a58db8b46ccab0d043e74164407627','5','');
INSERT INTO `bos_termekkepek` VALUES ('42001','4','807396ea5c4b85709e143db19d1e0dbb','10','');
INSERT INTO `bos_termekkepek` VALUES ('42021','4','d41a5e6a3828794912c5accce6945e32','15','');
INSERT INTO `bos_termekkepek` VALUES ('42041','4','5cb9bfe47af6ab2014bd3c69b9fec8a2','20','');
INSERT INTO `bos_termekkepek` VALUES ('42121','207','e92a88a115051af987d0f10e44033ce3','5','');
INSERT INTO `bos_termekkepek` VALUES ('42141','207','a19e49f070ffa5602842b056bd2964d9','10','');
INSERT INTO `bos_termekkepek` VALUES ('42161','207','d2d14a860e8d76ddc192d8be47463544','15','');
INSERT INTO `bos_termekkepek` VALUES ('42341','49','105c713f26aceeeb177effaba81c5768','5','');
INSERT INTO `bos_termekkepek` VALUES ('42361','49','ad1caccf91fd561f8efdff685362a85e','10','');
INSERT INTO `bos_termekkepek` VALUES ('42381','49','78e74fe725904ecf96a7fc1f6df0c645','15','');
INSERT INTO `bos_termekkepek` VALUES ('42401','49','76c5322248041c96e5e7677ace8b8cc0','20','');
INSERT INTO `bos_termekkepek` VALUES ('42481','96','40b11f9780532e8b86ba9087df3dcdb6','5','');
INSERT INTO `bos_termekkepek` VALUES ('42501','96','37d7465c1cf6b226541c17d5b92034c1','10','');
INSERT INTO `bos_termekkepek` VALUES ('42521','96','8de6774e62a2984bc1c5e2dd52ff23ea','15','');
INSERT INTO `bos_termekkepek` VALUES ('42541','96','ce49d66b1ddfeb732c56810c4736e3c3','20','');
INSERT INTO `bos_termekkepek` VALUES ('42561','97','aa814f45de0c09c92ca1ef778932bdd5','5','');
INSERT INTO `bos_termekkepek` VALUES ('42581','97','bd2ecba368839b7447db9829ba015e98','10','');
INSERT INTO `bos_termekkepek` VALUES ('42601','97','37d7465c1cf6b226541c17d5b92034c1','15','');
INSERT INTO `bos_termekkepek` VALUES ('42621','97','3f13cf4ddf6fc50c0d39a1d5aeb57dd8','20','');
INSERT INTO `bos_termekkepek` VALUES ('42641','99','1e60bf71283dac0b8777b83250813e56','5','');
INSERT INTO `bos_termekkepek` VALUES ('42661','99','a03fec24df877cc65c037673397ad5c0','10','');
INSERT INTO `bos_termekkepek` VALUES ('42681','99','37d7465c1cf6b226541c17d5b92034c1','15','');
INSERT INTO `bos_termekkepek` VALUES ('42701','99','f902c7bcf5f3eecb256217c1ffe9af43','20','');
INSERT INTO `bos_termekkepek` VALUES ('42721','100','32614465d9c058b477fb7aeefed4ead8','5','');
INSERT INTO `bos_termekkepek` VALUES ('42741','100','2537e733af9b96cf59f35d6d400dab95','10','');
INSERT INTO `bos_termekkepek` VALUES ('42761','100','65378614c4adbc65cc1643a8bd221e59','15','');
INSERT INTO `bos_termekkepek` VALUES ('42781','100','37d7465c1cf6b226541c17d5b92034c1','20','');
INSERT INTO `bos_termekkepek` VALUES ('42801','101','ed45e77e699645c84f3f27b7b5ad1e2d','5','');
INSERT INTO `bos_termekkepek` VALUES ('42821','101','e704bb84211a84111e4c138a17e68edb','10','');
INSERT INTO `bos_termekkepek` VALUES ('42841','101','89fee0513b6668e555959f5dc23238e9','15','');
INSERT INTO `bos_termekkepek` VALUES ('42861','101','37d7465c1cf6b226541c17d5b92034c1','20','');
INSERT INTO `bos_termekkepek` VALUES ('42881','86','59349d7c8145846439782da8c2d78754','5','');
INSERT INTO `bos_termekkepek` VALUES ('42901','86','f1b967e673681c3b9cdbc9c568949344','10','');
INSERT INTO `bos_termekkepek` VALUES ('42921','86','6ef8783d73140a616fc3d27d7330d503','15','');
INSERT INTO `bos_termekkepek` VALUES ('42941','125','59349d7c8145846439782da8c2d78754','5','');
INSERT INTO `bos_termekkepek` VALUES ('42961','125','f1b967e673681c3b9cdbc9c568949344','10','');
INSERT INTO `bos_termekkepek` VALUES ('42981','125','6ef8783d73140a616fc3d27d7330d503','15','');
INSERT INTO `bos_termekkepek` VALUES ('43001','302','c5e84de96767b5d217316c52c0d20071','5','');
INSERT INTO `bos_termekkepek` VALUES ('43021','302','9984afcda87bbb6790b0f12f22b2f482','10','');
INSERT INTO `bos_termekkepek` VALUES ('43041','302','abea68db3a8e20728c555dd5c9fb8a45','15','');
INSERT INTO `bos_termekkepek` VALUES ('43061','302','07b23962a4a6ff6c3e13fc24e0e12caa','20','');
INSERT INTO `bos_termekkepek` VALUES ('43081','302','501f06dacc74edd54022151f71c8960b','25','');
INSERT INTO `bos_termekkepek` VALUES ('43101','302','1bbaee70eb04695d1e80c7a9135d5148','30','');
INSERT INTO `bos_termekkepek` VALUES ('43121','302','8678915eec5bccbba43c3ef53ff19b46','35','');
INSERT INTO `bos_termekkepek` VALUES ('43141','302','29a4715e01c0bc217d96855f193c7be9','40','');
INSERT INTO `bos_termekkepek` VALUES ('43261','193','e82ba7292d1c4fbfbf1933dc51f62e60','5','');
INSERT INTO `bos_termekkepek` VALUES ('43281','193','68c65932168bf9c9824ce31801b6596c','10','');
INSERT INTO `bos_termekkepek` VALUES ('43301','193','93fa5305171a1110d0de679cdd304bb0','15','');
INSERT INTO `bos_termekkepek` VALUES ('43321','193','b27cf97c2e282465fba1406e71ffb55c','20','');
INSERT INTO `bos_termekkepek` VALUES ('43341','193','64055d56367c68545b1ad9b86faee20c','25','');
INSERT INTO `bos_termekkepek` VALUES ('43421','323','24b7e056016caf9dd296647bf0d59905','5','');
INSERT INTO `bos_termekkepek` VALUES ('43441','325','8d518efb0d0c9dcfc08b0b18ad8a93a5','5','');
INSERT INTO `bos_termekkepek` VALUES ('43461','326','270cc148ad35ad0afafa4b0e0c5a9411','5','');
INSERT INTO `bos_termekkepek` VALUES ('43501','327','39d50f6c9228b8f3c8830c18b8f91ab2','5','');
INSERT INTO `bos_termekkepek` VALUES ('43521','328','669e438da787f1f50c0f7045e93c757b','5','');
INSERT INTO `bos_termekkepek` VALUES ('43541','329','b442e83a5556fa523514fdf47e8e4e1b','5','');
INSERT INTO `bos_termekkepek` VALUES ('43561','337','e9cf01c03f0979eb59cc770a5b87a542','5','');
INSERT INTO `bos_termekkepek` VALUES ('43581','337','beff6192b303004cb1f9ba17a08c75f7','5','');
INSERT INTO `bos_termekkepek` VALUES ('43601','337','67de0228d7fd9f3f78d0e57157bfe4e5','5','');
INSERT INTO `bos_termekkepek` VALUES ('43661','336','d0e7b521c18b09876cb7693e42880dba','5','');
INSERT INTO `bos_termekkepek` VALUES ('43861','324','466db9fb788b8edd0537c821cc55fd95','5','');
INSERT INTO `bos_termekkepek` VALUES ('43901','322','29f7ac3adbe1d3500c9404388aedcbfd','5','');
INSERT INTO `bos_termekkepek` VALUES ('43921','322','cd3fff756f16103877d212315a8103a7','10','');
INSERT INTO `bos_termekkepek` VALUES ('43941','322','6a67ad49461a5940715c8257a5902b79','15','');
INSERT INTO `bos_termekkepek` VALUES ('43961','331','fdfe44e68dc9914b41cab590f0a77ecc','5','');
INSERT INTO `bos_termekkepek` VALUES ('43981','331','ee712b54ff92d404ff6cd6c8f6e41324','10','');
INSERT INTO `bos_termekkepek` VALUES ('44001','331','41dbaeec4466f58cd76a81b7add4a279','15','');
INSERT INTO `bos_termekkepek` VALUES ('44021','331','80a4296f6e5d27fe4df73700906d45f3','20','');
INSERT INTO `bos_termekkepek` VALUES ('44041','331','300e69f72139feaef597540971ab598e','25','');
INSERT INTO `bos_termekkepek` VALUES ('44061','331','d3e15fa7b836a9643387c99c47c6c03b','30','');
INSERT INTO `bos_termekkepek` VALUES ('44081','331','e4ae5a0d155ae14613e6cd907bf7040a','35','');
INSERT INTO `bos_termekkepek` VALUES ('44101','331','9d5f5d96625c457bfc5c97c460a33ac3','40','');
INSERT INTO `bos_termekkepek` VALUES ('44161','23','19e8c00cbce50470515dfdca9f9f2160','5','');
INSERT INTO `bos_termekkepek` VALUES ('44221','24','0ecb6def4bf7a132d3578be7a14c47d7','5','');
INSERT INTO `bos_termekkepek` VALUES ('44281','25','320bc77bd3a1f546c4a40a63aed9516c','5','');
INSERT INTO `bos_termekkepek` VALUES ('44341','26','feaa3aa9821868db73534710d1945c9c','5','');
INSERT INTO `bos_termekkepek` VALUES ('44481','27','157223647790a8de0fc63859177dfc48','5','');
INSERT INTO `bos_termekkepek` VALUES ('44501','27','300e886725c1be52bfffc0692c6a656d','5','');
INSERT INTO `bos_termekkepek` VALUES ('44521','27','a1950012395742fd71f4fdd4cba9414e','5','');
INSERT INTO `bos_termekkepek` VALUES ('44841','356','15bdba7cd9655f1c60a10e1a05e69a7b','5','');
INSERT INTO `bos_termekkepek` VALUES ('44861','356','f41fbaae00729036036e9ad8944ef6da','5','');
INSERT INTO `bos_termekkepek` VALUES ('44901','358','30af388e8b327168172aeef9e3626bcf','5','');
INSERT INTO `bos_termekkepek` VALUES ('45101','212','6cc6e45d2f9cf66facfacd3554b52577','5','');
INSERT INTO `bos_termekkepek` VALUES ('45121','212','396bb0f13e994cc5f55bed43158f8b7d','10','');
INSERT INTO `bos_termekkepek` VALUES ('45141','212','4eed1153c6579c3f4d406da8b34fc1d7','15','');
INSERT INTO `bos_termekkepek` VALUES ('45161','212','d606046667548c7d7aedd977e9028e92','20','');
INSERT INTO `bos_termekkepek` VALUES ('45181','213','6cc6e45d2f9cf66facfacd3554b52577','5','');
INSERT INTO `bos_termekkepek` VALUES ('45201','213','396bb0f13e994cc5f55bed43158f8b7d','10','');
INSERT INTO `bos_termekkepek` VALUES ('45461','214','82cdf867e9579d284a93d3c0223448d3','5','');
INSERT INTO `bos_termekkepek` VALUES ('45241','213','d606046667548c7d7aedd977e9028e92','20','');
INSERT INTO `bos_termekkepek` VALUES ('45261','212','1c2d742497419ba12659967817ae3b3d','5','');
INSERT INTO `bos_termekkepek` VALUES ('45281','212','5d541464ac15bfb8f0a537b010d13fa1','5','');
INSERT INTO `bos_termekkepek` VALUES ('45301','212','2dcf3faa111f9fea4396c3dc793c3a6f','5','');
INSERT INTO `bos_termekkepek` VALUES ('45321','213','0fb4162278cc01c72c141ac5f009ffa1','5','');
INSERT INTO `bos_termekkepek` VALUES ('45341','213','80cd71aee683e52c643ad59a03524806','5','');
INSERT INTO `bos_termekkepek` VALUES ('45361','213','b328487b2b7959c38fd3c33a17bb6a00','5','');
INSERT INTO `bos_termekkepek` VALUES ('45401','214','396bb0f13e994cc5f55bed43158f8b7d','10','');
INSERT INTO `bos_termekkepek` VALUES ('45421','214','4eed1153c6579c3f4d406da8b34fc1d7','15','');
INSERT INTO `bos_termekkepek` VALUES ('45441','214','d606046667548c7d7aedd977e9028e92','20','');
INSERT INTO `bos_termekkepek` VALUES ('45481','214','e1944d5edda7b859fb00f89723312217','5','');
INSERT INTO `bos_termekkepek` VALUES ('46061','192','8afc0327c6d499ed20c99d2a2502c7b9','5','');
INSERT INTO `bos_termekkepek` VALUES ('46081','192','3a1f43d84d14dbfe63867595caff9b93','10','');
INSERT INTO `bos_termekkepek` VALUES ('46161','191','e45a7c5931ad7229fd89e9fe455f599f','5','');
INSERT INTO `bos_termekkepek` VALUES ('46181','191','1e431c67b87c17e668d7a061a142d114','10','');
INSERT INTO `bos_termekkepek` VALUES ('46201','191','060f31652cdb061a8e40a99c31190d6a','15','');
INSERT INTO `bos_termekkepek` VALUES ('46221','191','b258cda7f6762de012fddbb6477f5190','20','');
INSERT INTO `bos_termekkepek` VALUES ('46241','194','15986f2f3afd0772ab6e3a0539902b5d','5','');
INSERT INTO `bos_termekkepek` VALUES ('46261','194','21379351172bcf5a2ab06efa45b49125','10','');
INSERT INTO `bos_termekkepek` VALUES ('46281','194','b48dd74849193d450c924dedf620968d','15','');
INSERT INTO `bos_termekkepek` VALUES ('46421','195','a1dbaf0b478ece4e74c08f37ed97cce8','5','');
INSERT INTO `bos_termekkepek` VALUES ('46441','195','83c9ba9457bc1008ff3c52dee3043a44','10','');
INSERT INTO `bos_termekkepek` VALUES ('46461','195','c37ab80b0b3a2b3ec46f2710eba74692','15','');
INSERT INTO `bos_termekkepek` VALUES ('46481','196','5ab388a9689b74dae58b9eb2c7c02c66','5','');
INSERT INTO `bos_termekkepek` VALUES ('46501','196','6000db4c19f0ab158c1b1bf5b06ce151','10','');
INSERT INTO `bos_termekkepek` VALUES ('46521','196','b856243e684b903de6085dc70830792a','15','');
INSERT INTO `bos_termekkepek` VALUES ('46941','54','e8257cae02495b31c4e89a4ae812499c','5','');
INSERT INTO `bos_termekkepek` VALUES ('46961','54','162299482dc87e5476ba2310bfa7ea27','10','');
INSERT INTO `bos_termekkepek` VALUES ('46981','54','a8e00783e9207f221fa083bd2b5f39cc','15','');
INSERT INTO `bos_termekkepek` VALUES ('47001','54','1e63210059f01f7825bf4e5d0352683d','20','');
INSERT INTO `bos_termekkepek` VALUES ('47021','54','617e53d130358a136b2d4e42c3fa9ff1','25','');
INSERT INTO `bos_termekkepek` VALUES ('47041','54','729081de27a1cb64372f306a8d21f7b5','30','');
INSERT INTO `bos_termekkepek` VALUES ('47061','54','e1c883f9046bc9dae1b51a0508c212d9','35','');
INSERT INTO `bos_termekkepek` VALUES ('47081','54','fd4b6debb021b6e14c908031985449b8','40','');
INSERT INTO `bos_termekkepek` VALUES ('47101','54','2365a4db507862569752334234a29b50','45','');
INSERT INTO `bos_termekkepek` VALUES ('47121','54','5212e2d543ab29ea0aae8bb633be388a','50','');
INSERT INTO `bos_termekkepek` VALUES ('47141','54','e4896f958ff28a66eb6b8e62520a7ba8','55','');
INSERT INTO `bos_termekkepek` VALUES ('47161','54','a557b94600800ab14144442984f14a6c','60','');
INSERT INTO `bos_termekkepek` VALUES ('47181','54','5919cc509bc0234c73ddec244c930964','65','');
INSERT INTO `bos_termekkepek` VALUES ('47201','54','483d57221cc733958be20869f1c7e400','70','');
INSERT INTO `bos_termekkepek` VALUES ('47221','54','4f3bb9df29a7062aaea183b5c065732b','75','');
INSERT INTO `bos_termekkepek` VALUES ('47241','54','617c2059b465399ae28717b107144003','5','');
INSERT INTO `bos_termekkepek` VALUES ('47281','383','e8257cae02495b31c4e89a4ae812499c','5','');
INSERT INTO `bos_termekkepek` VALUES ('47301','383','162299482dc87e5476ba2310bfa7ea27','10','');
INSERT INTO `bos_termekkepek` VALUES ('47321','383','a8e00783e9207f221fa083bd2b5f39cc','15','');
INSERT INTO `bos_termekkepek` VALUES ('47341','383','1e63210059f01f7825bf4e5d0352683d','20','');
INSERT INTO `bos_termekkepek` VALUES ('47361','383','617e53d130358a136b2d4e42c3fa9ff1','25','');
INSERT INTO `bos_termekkepek` VALUES ('47381','383','729081de27a1cb64372f306a8d21f7b5','30','');
INSERT INTO `bos_termekkepek` VALUES ('47401','383','e1c883f9046bc9dae1b51a0508c212d9','35','');
INSERT INTO `bos_termekkepek` VALUES ('47421','383','fd4b6debb021b6e14c908031985449b8','40','');
INSERT INTO `bos_termekkepek` VALUES ('47441','383','2365a4db507862569752334234a29b50','45','');
INSERT INTO `bos_termekkepek` VALUES ('47461','383','5212e2d543ab29ea0aae8bb633be388a','50','');
INSERT INTO `bos_termekkepek` VALUES ('47481','383','e4896f958ff28a66eb6b8e62520a7ba8','55','');
INSERT INTO `bos_termekkepek` VALUES ('47501','383','a557b94600800ab14144442984f14a6c','60','');
INSERT INTO `bos_termekkepek` VALUES ('47521','383','617c2059b465399ae28717b107144003','65','');
INSERT INTO `bos_termekkepek` VALUES ('47541','383','5919cc509bc0234c73ddec244c930964','70','');
INSERT INTO `bos_termekkepek` VALUES ('47561','383','483d57221cc733958be20869f1c7e400','75','');
INSERT INTO `bos_termekkepek` VALUES ('47581','383','4f3bb9df29a7062aaea183b5c065732b','80','');
INSERT INTO `bos_termekkepek` VALUES ('47601','385','e8257cae02495b31c4e89a4ae812499c','5','');
INSERT INTO `bos_termekkepek` VALUES ('47621','385','162299482dc87e5476ba2310bfa7ea27','10','');
INSERT INTO `bos_termekkepek` VALUES ('47641','385','a8e00783e9207f221fa083bd2b5f39cc','15','');
INSERT INTO `bos_termekkepek` VALUES ('47661','385','1e63210059f01f7825bf4e5d0352683d','20','');
INSERT INTO `bos_termekkepek` VALUES ('47681','385','617e53d130358a136b2d4e42c3fa9ff1','25','');
INSERT INTO `bos_termekkepek` VALUES ('47701','385','729081de27a1cb64372f306a8d21f7b5','30','');
INSERT INTO `bos_termekkepek` VALUES ('47721','385','e1c883f9046bc9dae1b51a0508c212d9','35','');
INSERT INTO `bos_termekkepek` VALUES ('47741','385','fd4b6debb021b6e14c908031985449b8','40','');
INSERT INTO `bos_termekkepek` VALUES ('47761','385','2365a4db507862569752334234a29b50','45','');
INSERT INTO `bos_termekkepek` VALUES ('47781','385','5212e2d543ab29ea0aae8bb633be388a','50','');
INSERT INTO `bos_termekkepek` VALUES ('47801','385','e4896f958ff28a66eb6b8e62520a7ba8','55','');
INSERT INTO `bos_termekkepek` VALUES ('47821','385','a557b94600800ab14144442984f14a6c','60','');
INSERT INTO `bos_termekkepek` VALUES ('47841','385','617c2059b465399ae28717b107144003','65','');
INSERT INTO `bos_termekkepek` VALUES ('47861','385','5919cc509bc0234c73ddec244c930964','70','');
INSERT INTO `bos_termekkepek` VALUES ('47881','385','483d57221cc733958be20869f1c7e400','75','');
INSERT INTO `bos_termekkepek` VALUES ('47901','385','4f3bb9df29a7062aaea183b5c065732b','80','');
INSERT INTO `bos_termekkepek` VALUES ('48002','168','a3884720eab9814211499d13339b7586','5','');
INSERT INTO `bos_termekkepek` VALUES ('48003','168','9d223723152194d11fe071289ed8d699','10','');
INSERT INTO `bos_termekkepek` VALUES ('48004','168','0c43b6b380576955a4f8f2e4ed896878','15','');
INSERT INTO `bos_termekkepek` VALUES ('48055','8','03fa53a658469b896ddb745d2706d470','5','');
INSERT INTO `bos_termekkepek` VALUES ('48056','9','23caa86a7dadfef1317f9af86bd135e1','5','');
INSERT INTO `bos_termekkepek` VALUES ('48057','10','94c0804be8acc161769e9acb77d8ec32','5','');
INSERT INTO `bos_termekkepek` VALUES ('48058','11','de556ca8eba0fc417ac22b46cd3d0c84','5','');
INSERT INTO `bos_termekkepek` VALUES ('48059','12','45cd103b30041c230b512eed3695a7e1','5','');
INSERT INTO `bos_termekkepek` VALUES ('48067','147','108cc6c3f63ebd8d4356829d2d84344b','5','');
INSERT INTO `bos_termekkepek` VALUES ('48077','149','5b39e8d3d63d7e3ddc6c800f7bc64de5','5','');
INSERT INTO `bos_termekkepek` VALUES ('48074','150','d19ac2e656681f6cd7adde630d8a1478','5','');
INSERT INTO `bos_termekkepek` VALUES ('48076','149','52aa08e72d7bd681c7441b4f9a37d033','5','');
INSERT INTO `bos_termekkepek` VALUES ('48071','147','ebfbe4ec25030dd143c51cf4f807b0a7','5','');
INSERT INTO `bos_termekkepek` VALUES ('48072','148','0acedbe660712601203336d1bcc3de49','5','');
INSERT INTO `bos_termekkepek` VALUES ('48073','148','76bf79e9a0a4c128d97dbd6900773f4b','5','');
INSERT INTO `bos_termekkepek` VALUES ('48075','150','a6c7a5fbe48026d388b77d21c618300d','5','');
INSERT INTO `bos_termekkepek` VALUES ('48079','136','9a653220a8fe66050eda45db41c3686a','5','');
INSERT INTO `bos_termekkepek` VALUES ('48086','22','3ef413c41b38a5cbc1e5a88d9ce5f3a8','5','');
INSERT INTO `bos_termekkepek` VALUES ('48087','22','a0d63af8542e13942f6ac9577a738af3','10','');
INSERT INTO `bos_termekkepek` VALUES ('48088','22','5b592a9e37c3474d12c4a05a1ef50598','15','');
INSERT INTO `bos_termekkepek` VALUES ('48089','57','5919cc509bc0234c73ddec244c930964','5','');
INSERT INTO `bos_termekkepek` VALUES ('48090','57','4f3bb9df29a7062aaea183b5c065732b','10','');
INSERT INTO `bos_termekkepek` VALUES ('48091','57','1e63210059f01f7825bf4e5d0352683d','15','');
INSERT INTO `bos_termekkepek` VALUES ('48092','57','617c2059b465399ae28717b107144003','20','');
INSERT INTO `bos_termekkepek` VALUES ('48093','57','e8257cae02495b31c4e89a4ae812499c','25','');
INSERT INTO `bos_termekkepek` VALUES ('48094','57','162299482dc87e5476ba2310bfa7ea27','30','');
INSERT INTO `bos_termekkepek` VALUES ('48095','57','fd4b6debb021b6e14c908031985449b8','35','');
INSERT INTO `bos_termekkepek` VALUES ('48096','57','483d57221cc733958be20869f1c7e400','40','');
INSERT INTO `bos_termekkepek` VALUES ('48097','57','2365a4db507862569752334234a29b50','45','');
INSERT INTO `bos_termekkepek` VALUES ('48098','57','729081de27a1cb64372f306a8d21f7b5','50','');
INSERT INTO `bos_termekkepek` VALUES ('48099','57','a8e00783e9207f221fa083bd2b5f39cc','55','');
INSERT INTO `bos_termekkepek` VALUES ('48100','57','a557b94600800ab14144442984f14a6c','60','');
INSERT INTO `bos_termekkepek` VALUES ('48101','57','e4896f958ff28a66eb6b8e62520a7ba8','65','');
INSERT INTO `bos_termekkepek` VALUES ('48102','57','e1c883f9046bc9dae1b51a0508c212d9','70','');
INSERT INTO `bos_termekkepek` VALUES ('48103','57','617e53d130358a136b2d4e42c3fa9ff1','75','');
INSERT INTO `bos_termekkepek` VALUES ('48104','57','5212e2d543ab29ea0aae8bb633be388a','80','');
INSERT INTO `bos_termekkepek` VALUES ('48109','372','0f5f11318128225f5ae305bff7d67ba0','5','');
INSERT INTO `bos_termekkepek` VALUES ('48110','372','ffdd909cdb7fa6ea26c328488c4b8b76','10','');
INSERT INTO `bos_termekkepek` VALUES ('48111','372','67a381bd43fbf14a0a122b8ae1bb271a','15','');
INSERT INTO `bos_termekkepek` VALUES ('48112','372','d0761d981bce5c8706808bb62e20f3b9','20','');
INSERT INTO `bos_termekkepek` VALUES ('48113','372','4672a4597e331b354e95d558b9a63287','25','');
INSERT INTO `bos_termekkepek` VALUES ('48114','372','23ea75a4cda09058d2e7dc1ce52242d8','30','');
-- --------------------------------------------------------

--
-- Table structure for table `bos_termekleiras`
--

DROP TABLE IF EXISTS `bos_termekleiras`;

CREATE TABLE `bos_termekleiras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termek_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `termek_id` (`termek_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_termekleiras`
--

-- --------------------------------------------------------

--
-- Table structure for table `bos_termekleiras_hu`
--

DROP TABLE IF EXISTS `bos_termekleiras_hu`;

CREATE TABLE `bos_termekleiras_hu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termek_id` int(11) NOT NULL,
  `nev` varchar(255) NOT NULL,
  `ant_id` int(11) NOT NULL,
  `sorszam` int(11) NOT NULL,
  `arres` float NOT NULL,
  `arfolyam` float NOT NULL,
  `frissitheto` int(11) NOT NULL,
  `tartozekok` text NOT NULL,
  `kisleiras` text NOT NULL,
  `vtsz` varchar(255) NOT NULL,
  `parameterek` text NOT NULL,
  `tipus` varchar(255) NOT NULL,
  `jellemzok` text NOT NULL,
  `pdfpic2` varchar(255) NOT NULL,
  `pdfpic1` varchar(255) NOT NULL,
  `megjegyzes` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `termek_id` (`termek_id`)
) ENGINE=InnoDB AUTO_INCREMENT=391 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_termekleiras_hu`
--

INSERT INTO `bos_termekleiras_hu` VALUES ('2','2','Dolphin 375 L hűtőpult','350','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 3750 mm\r\n- szélesség :  1200 mm\r\n- magasság : 1190 mm\r\n- bemutató felület: 900 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 2166 W','Dolphin 375 L','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('3','3','Tuna 375 hűtőpult','356','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 3750 mm\r\n- szélesség :  1200 mm\r\n- magasság : 1215 mm\r\n- bemutató felület: 866 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 2166 W','Tuna 375','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('4','4','Tuna 125 önkiszolgáló hűtőpult','5901','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 1250 mm\r\n- szélesség :  1200 mm\r\n- magasság : 1215 mm\r\n- bemutató felület: 866 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 723 W','Tuna 125 önkiszolgáló hűtőpult','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('5','5','Tuna 375 R hűtőpult','3801','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 3750 mm\r\n- szélesség :  1220 mm\r\n- magasság : 1150 mm\r\n- bemutató felület: 900 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 2250 W','Tuna 375 R','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- \\\"Retro\\\" frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('6','6','Artemis 285','368','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 2850 mm\r\n- szélesség :  1150 mm\r\n- magasság : 1200 mm\r\n- bemutató felület: 790 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1860 W','Artemis 285','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('7','7','UNIVERSAL  04 150 hűtőpult','261','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1505\r\n- szélesség : 1000\r\n- magasság :  608\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A','UNIVERSAL  04 150','- manuális leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belső\r\n- világítás\r\n- munkapult\r\n- pultra helyezhető kivitel\r\n- köztes üvegpolccal\r\n- plexi tolóajtóval','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('8','8','MASTER 100 R hűtőpult','3221','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1040\r\n- szélesség : 900\r\n- magasság : 1262\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 430 W\r\n - villamos teljesítmény: 310 W +40 W világítás\r\n - Spanyol aggregát egységgel','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- \\\"retro\\\" frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt bemutatófelület\r\n- alsó hűtött tároló\r\n- resopal munkapult\r\n- plexi tolóajtó','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('9','9','MASTER 150 R hűtőpult','3241','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1540\r\n- szélesség : 900\r\n- magasság : 1262\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 480 W\r\n - villamos teljesítmény: 360 W +40 W világítás\r\n - Spanyol aggregát egységgel','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- \\\"retro\\\" frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt bemutatófelület\r\n- alsó hűtött tároló\r\n- resopal munkapult\r\n - plexi tolóajtó','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('10','10','MASTER 200 R hűtőpult','3261','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2000\r\n- szélesség : 900\r\n- magasság : 1262\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n - hűtőteljesítmény: 640 W\r\n - villamos teljesítmény: 487 W +60 W világítás\r\n - Spanyol aggregát egységgel','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- \\\"retro\\\" frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt bemutatófelület\r\n- alsó hűtött tároló\r\n- resopal munkapult\r\n- plexi tolóajtó','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('11','11','MASTER 250 R hűtőpult','3281','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2480\r\n- szélesség : 900\r\n- magasság : 1262\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 800 W\r\n - villamos teljesítmény: 665 W +80 W világítás\r\n - Spanyol aggregát egységgel','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- \\\"retro\\\" frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt bemutatófelület\r\n- alsó hűtött tároló\r\n- resopal munkapult\r\n - plexi tolóajtó','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('12','12','MASTER 300 R hűtőpult','3301','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 3000\r\n- szélesség : 900\r\n- magasság : 1262\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 960 W\r\n - villamos teljesítmény: 792 W +80 W világítás\r\n - Spanyol aggregát egységgel','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- \\\"retro\\\" frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt bemutatófelület\r\n- alsó hűtött tároló\r\n- resopal munkapult\r\n- plexi tolóajtó','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('13','13','GRAZIA 5 120 hűtőpult','3021','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1160 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1120 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 270 W','GRAZIA 5 120','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('14','14','GRAZIA 5 150 hűtőpult','3041','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1425 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 286 W','GRAZIA 5 150','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('15','15','GRAZIA 5 170 hűtőpult','3061','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1625 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 347 W','GRAZIA 5 170','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('16','16','GRAZIA 5 200 hűtőpult','3081','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1925 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 428 W','GRAZIA 5 200','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('17','17','GRAZIA 5 250 hűtőpult','3101','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2250 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 519 W','GRAZIA 5 250','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('18','18','GRAZIA 5 300 hűtőpult','3121','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2850 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 662 W','GRAZIA 5 300','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('19','19','GRAZIA 5 150 hűtőpult BT','3141','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1425 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1250 mm\r\n- hűtési tartomány: -15/-18 °C\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 286 W','GRAZIA 5 150 BT','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('20','20','GRAZIA 5 200 hűtőpult BT','3161','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1925 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1250 mm\r\n- hűtési tartomány: -15/-18 °C\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: ---- W','GRAZIA 5 200 BT','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('21','21','GRAZIA 5 250 hűtőpult BT','3181','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2250 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1250 mm\r\n- hűtési tartomány: -15/-18 °C\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: ---- W','GRAZIA 5 250 BT','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('22','22','GRAZIA 3 120 hűtőpult','6061','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1125 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1205mm\r\n- magasság : 1150 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 270 W','GRAZIA 3 120','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('23','23','GRAZIA 6 120 hűtőpult','254','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1125 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 270 W','GRAZIA 6 120','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('24','24','GRAZIA 6 150 hűtőpult','255','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1425 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 286 W','GRAZIA 6 150','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('25','25','GRAZIA 6 170 hűtőpult','256','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1625 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 347 W','GRAZIA 6 170','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('26','26','GRAZIA 6 200 hűtőpult','257','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1925 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 428 W','GRAZIA 6 200','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('27','27','GRAZIA 6 250 hűtőpult','258','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2250 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 519 W','GRAZIA 6 250','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('28','28','GRAZIA 6 300 hűtőpult','259','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 2850 mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1105 mm\r\n- magasság : 1240 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 662 W','GRAZIA 6 300','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('29','29','GRAZIA 3 belső sarokelem','1441','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1350mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1350 mm\r\n- magasság : 1197 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 347 W','GRAZIA 3 NW90','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('30','30','GRAZIA 3 külső sarokelem','1461','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 1360mm\r\n- végelem:           45 mm/db\r\n- szélesség :  1360 mm\r\n- magasság : 1197 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 347 W','GRAZIA 3 NZ90','- automatikus elektromos leolvasztás\r\n- páramentesítő fűtés\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventillációs hűtés\r\n- rozsdamentes belsőtér\r\n- rozsdamentes alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('31','31','KLARA 36 S  hűtőpult','5261','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 3600\r\n- szélesség : 1200\r\n- magasság : 1200\r\n- hűtési tartomány: -1/+7\r\n- hűtőközeg: R 404A\r\n - hűtőteljesítmény: 1314 W\r\n - villamos telj.: 1050 W','KLARA 36 S  hűtőpult','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- RM. bemutatófelület\r\n- alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('32','32','KLARA 18 TP melegentaró','5961','0','1.65','0','1','','Megelentartópult','8418','- hosszúság : 1800\r\n- szélesség : 1200\r\n- magasság : 1200\r\n- hőfoktartomány: +65 °C','KLARA 18 TP melegentaró','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- RM. bemutatófelület\r\n- rozsdamentes munkapult\r\n- vízfürdős melegen-tartó GN\r\n - infrahíd','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('33','33','KLARA 36 V  hűtőpult','5381','0','1.65','0','1','','Hűtőpult beépített aggregáttal','8418','- hosszúság : 3600\r\n- szélesség : 1200\r\n- magasság : 1200\r\n- hűtési tartomány: -1/+5\r\n- hűtőközeg: R 404A\r\n - hűtőteljesítmény: 1661 W\r\n - villamos telj.: 1735 W','KLARA 36 V  hűtőpult','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventillációs hűtés\r\n- RM. bemutatófelület\r\n- alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('34','34','MASTER PLUS 100 hűtőpult','2661','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 1060\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 414 W','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('35','35','MASTER PLUS 150 hűtőpult','174','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 1540\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 585 W','Master','- automatikus leolvasztás\r\n- beépített aggregáttal\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('36','36','MASTER PLUS 200 hűtőpult','173','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 2020\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 648 W','Master','- automatikus leolvasztás\r\n- beépített aggregáttal\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('37','37','MASTER PLUS 250 hűtőpult','172','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 2500\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 795 W','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('38','38','MASTER PLUS 300 hűtőpult','171','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 2980\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 823 W','Master','- automatikus leolvasztás\r\n- beépített aggregáttal\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('39','39','MASTER PLUS 350 hűtőpult','170','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 3700\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 971 W','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('40','40','MASTER PLUS 400 hűtőpult','169','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 3940\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1119 W','Master','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('41','41','MASTER LUX 100 hűtőpult','176','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 1060\r\n- szélesség : 1140\r\n- magasság : 1255\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 414 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('42','42','MASTER LUX 150 hűtőpult','177','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 1540\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 585 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('43','43','MASTER LUX 200 hűtőpult','178','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 2020\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 648 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('44','44','MASTER LUX 250 hűtőpult','179','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 2500\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 795 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('45','45','MASTER LUX 300 hűtőpult','180','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 2980\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 823 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('46','46','MASTER LUX 350 hűtőpult','181','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 3700\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 971 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('47','47','MASTER LUX 400 hűtőpult','182','0','1.65','0','1','','Hűtőpult beépített vagy telepített aggregáttal','8418','- hosszúság : 3940\r\n- szélesség : 1140\r\n- magasság : 1300\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1119 W','MasterLux','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus/ ventilációs hűtés\r\n- porszórt felületkezelés\r\n- alsó hűtött tároló\r\n- resopal munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('48','48','Costan Bellini 83/375 hűtőpult','2581','0','1.65','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 3750 mm\r\n- szélesség :  1160 mm\r\n- magasság : 1280 mm\r\n- bemutató felület: 825 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1050 W /-7°C','Costan Bellini 83/375','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- alsó hűtött tároló\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('49','49','Costan Bellini 83/93 melegentartó','4681','0','1.65','0','1','','Melegentartó pult','8418','- hosszúság : 950 mm\r\n- szélesség :  1160 mm\r\n- magasság : 1280 mm\r\n- bemutató felület: 825 mm\r\n- fűtési tartomány: +60 °C','Costan Bellini 83/93 melegentartó','- vízfürdős kialakítás\r\n- felső infrahíd\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- rozsdamentes bemutatófelület\r\n- GN edényzettel\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('50','50','Costan Rossini 375 hűtőpult','2481','0','1.65','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 3750 mm\r\n- szélesség :  1080 mm\r\n- magasság : 1200 mm\r\n- bemutató felület: 850 mm\r\n- hűtési tartomány: 0/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1140 W /-7°C','Costan Rossini 375','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('51','51','Dolphin 285 hűtőpult','343','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 2850 mm\r\n- szélesség :  1150 mm\r\n- magasság : 1150 mm\r\n- bemutató felület: 790 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1497 W','Dolphin 285','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('52','52','Hemera 285','374','0','2.3','0','1','','Hűtőpult telepített aggregáttal','8418','- hosszúság : 2850 mm\r\n- szélesség :  1100 mm\r\n- magasság : 1245 mm\r\n- bemutató felület: 790 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1860W','Hemera 285','- automatikus leolvasztás\r\n- aggregát nélkül\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes bemutatófelület\r\n- páramentesítés\r\n- rozsdamentes munkapult\r\n- világítás\r\n- hűtött alsó tároló','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('53','53','Ohio II','4361','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 1210 mm\r\n- szélesség :    630 mm\r\n- magasság   635 mm\r\n- hűtési tartomány: +2/+12\r\n- hűtőközeg: R 404A','Ohio II','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- frontüveg\r\n- ventilációs hűtés\r\n- rozsdamentes belső\r\n- világítás\r\n- üveg tolóajtó\r\n- köztes üvegpolccal\r\n- rozsdamentes front','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('54','54','EVO 60','6541','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 600 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtő teljesítmény: 400 W','EVO 60','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs hűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('55','55','EVO 90','6561','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 900 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtő teljesítmény: 400 W','EVO 90','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs hűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('56','56','EVO 120','6581','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 1200 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtő teljesítmény: 687 W','EVO 120','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs hűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('57','57','EVO 150','6601','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 1500 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtő teljesítmény: 717 W','EVO 150','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs hűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('58','58','EVO 180','6621','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 1800 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A\r\n- hűtő teljesítmény: 754 W','EVO 180','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs hűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('59','59','ELEGANT 05-90','3201','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 940 mm\r\n- szélesség :    750 mm\r\n- magasság   1309 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A','ELEGANT 05-90','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- laminált belső cseresznye\r\n- világítás\r\n- üveg tolóajtó\r\n- köztes üvegpolccal\r\n- laminált cseresznye front','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('60','60','ELEGANT 05-140','302','0','1.65','0','1','','Hűtővitrin beépített aggregáttal','8418','- hosszúság : 1440 mm\r\n- szélesség :    750 mm\r\n- magasság   1309 mm\r\n- hűtési tartomány: +2/+5\r\n- hűtőközeg: R 404A','ELEGANT 05-140','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventilációs hűtés\r\n- laminált belső cseresznye\r\n- világítás\r\n- üveg tolóajtó\r\n- köztes üvegpolccal\r\n- laminált cseresznye front','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('61','61','KLARA 18 V ZA  cukrászvitrin','5421','0','1.65','0','1','','Cukrászpult beépített aggregáttal','8418','- hosszúság : 1800\r\n- szélesség : 1200\r\n- magasság : 1200\r\n- hűtési tartomány: 2/+7\r\n- hűtőközeg: R 404A\r\n - hűtőteljesítmény: 1093 W\r\n - villamos telj.: 1011 W','KLARA 18 V ZA  cukrászvitrin','- automatikus leolvasztás\r\n- beépített aggregát\r\n- ABS oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- ventillációs hűtés\r\n- RM. bemutatófelület\r\n- alsó tortafiók\r\n- 2 sor üvegpolc\r\n- rozsdamentes munkapult','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('62','62','Symphony 140','1921','0','1.65','0','1','','Cukrászhűtő beépített aggregáttal','8418','- hosszúság :   1400 mm\r\n- szélesség :    1000 mm\r\n- magasság     1270 mm\r\n- hűtési tartomány: +4/+6\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 596 W','Symphony 140','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belső\r\n- világítás\r\n- rozsdamentes tortafiók\r\n- köztes üvegpolccal\r\n- arany vagy alumínium keret\r\n- egyedi színekben','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('63','63','Symphony 220','1941','0','1.65','0','1','','Cukrászhűtő beépített aggregáttal','8418','- hosszúság :   2200 mm\r\n- szélesség :    1000 mm\r\n- magasság     1270 mm\r\n- hűtési tartomány: +4/+6\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 817 W','Symphony 220','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belső\r\n- világítás\r\n- rozsdamentes tortafiók\r\n- köztes üvegpolccal\r\n- arany vagy alumínium keret\r\n- egyedi színekben','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('64','64','Symphony 290','1961','0','1.65','0','1','','Cukrászhűtő beépített aggregáttal','8418','- hosszúság :   2900 mm\r\n- szélesség :    1000 mm\r\n- magasság     1270 mm\r\n- hűtési tartomány: +4/+6\r\n- hűtőközeg: R 404A\r\n- hűtőteljesítmény: 1054 W','Symphony 290','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- kocsiütközővel\r\n- elektronikus hőfokbeállítás\r\n- ívelt frontüveg\r\n- statikus hűtés\r\n- rozsdamentes belső\r\n- világítás\r\n- rozsdamentes tortafiók\r\n- köztes üvegpolccal\r\n- arany vagy alumínium keret\r\n- egyedi színekben','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('65','65','Kubo Expo 300','2601','0','1.65','0','1','','Cukrászhűtő beépített aggregáttal','8418','- hosszúság :   700 mm\r\n- szélesség :    768 mm\r\n- magasság     1475 mm\r\n- hűtési tartomány: +2/+10\r\n- hűtőközeg: R 134A\r\n- kapacitás: 300 liter\r\n- hűtőteljesítmény: 500 W','Kubo Expo 300','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- mobil kivitel\r\n- elektronikus hőfokbeállítás\r\n- szigetelt nyíló üvegajtó\r\n- rácspolc hűtés\r\n- rozsdamentes belső\r\n- LED világítás\r\n- alumínium keret','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('66','66','Kubo Expo 600','2621','0','1.65','0','1','','Cukrászhűtő beépített aggregáttal','8418','- hosszúság :   1355 mm\r\n- szélesség :    768 mm\r\n- magasság     1475 mm\r\n- hűtési tartomány: +2/+10\r\n- hűtőközeg: R 134A\r\n- kapacitás: 600 liter\r\n- hűtőteljesítmény: 700 W','Kubo Expo 600','- elektronikus leolvasztás\r\n- beépített aggregát\r\n- üveg oldalakkal\r\n- mobil kivitel\r\n- elektronikus hőfokbeállítás\r\n- szigetelt nyíló üvegajtó\r\n- rácspolc hűtés\r\n- rozsdamentes belső\r\n- LED világítás\r\n- alumínium keret','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('67','67','Pandora 90 SL falihűtő','1501','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 900 mm\r\nVégelem:     50 mm/db\r\nMélység:       880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6 °C\r\nVillamos teljesítmény:550 W\r\nHűtőteljesítmény: 945 W','Pandora 90 SL','- beépített aggregát\r\n - 3 sor 400 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsókád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('68','68','Pandora 125 SL falihűtő','1521','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 1250 mm\r\nVégelem:          50 mm/db\r\nMélység:         870 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6 °C\r\nVillamos teljesítmény:700 W\r\nHűtőteljesítmény: 1313 W','Pandora 125 SL','- beépített aggregát\r\n - 3 sor 400 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('69','69','Pandora 180 SL falihűtő','1541','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 1800 mm\r\nVégelem:          50 mm/db\r\nMélység:         880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6 °C\r\nVillamos teljesítmény:750 W\r\nHűtőteljesítmény: 1890 W','Pandora 180 SL','- beépített aggregát\r\n - 3 sor 400 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('70','70','Pandora 250 SL falihűtő','1561','0','1.8','0','1','','Fali tej hűtő beépített aggregáttal','8418','Hosszúság: 2500 mm\r\nVégelem:          50 mm/db\r\nMélység:         880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6 °C\r\nVillamos teljesítmény:1000 W\r\nHűtőteljesítmény: 2450 W','Pandora 250 SL','- beépített aggregát\r\n - 3 sor 400 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('71','71','Pandora 90 FV fali zöldséghűtő','1581','0','1.8','0','1','','Fali zöldséghűtő beépített aggregáttal','8418','Hosszúság: 900 mm\r\nVégelem:     50 mm/db\r\nMélység:       880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nVillamos teljesítmény:550 W\r\nHűtőteljesítmény: 945 W','Pandora 90 FV','- beépített aggregát\r\n - 2 sor 400 mm-es dönthető polc\r\n - döntött tükör\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsókád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('72','72','Pandora 125 FV fali zöldséghűtő','1601','0','1.8','0','1','','Fali zöldséghűtő beépített aggregáttal','8418','Hosszúság: 1250 mm\r\nVégelem:          50 mm/db\r\nMélység:         880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -+2/+8 °C\r\nVillamos teljesítmény:700 W\r\nHűtőteljesítmény: 1313 W','Pandora 125 FV','- beépített aggregát\r\n - 2 sor 400 mm-es dönthető polc\r\n - döntött tükör\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('73','73','Pandora 180 FV fali zöldséghűtő','1621','0','1.8','0','1','','Fali zöldséghűtő beépített aggregáttal','8418','Hosszúság: 1800 mm\r\nVégelem:          50 mm/db\r\nMélység:         880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nVillamos teljesítmény:750 W\r\nHűtőteljesítmény: 1890 W','Pandora 180 FV','- beépített aggregát\r\n - 2 sor 400 mm-es dönthető polc\r\n - döntött tükör\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('74','74','Pandora 250 FV fali zöldséghűtő','1661','0','1.8','0','1','','Fali zöldség hűtő beépített aggregáttal','8418','Hosszúság: 2500 mm\r\nVégelem:          50 mm/db\r\nMélység:         880 mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nVillamos teljesítmény:1000 W\r\nHűtőteljesítmény: 2452 W','Pandora 250 FV','- beépített aggregát\r\n - 2 sor 400 mm-es dönthető polc\r\n - döntött tükör\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - cseppvíztartály\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('75','75','Oxford 70/90 falihűtő','1681','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 900 mm\r\nVégelem:     45 mm/db\r\nMélység:       700mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','Oxford 70/90','- beépített aggregát\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - tükrös végelem\r\n - cseppvíztálca\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('76','76','Oxford 70/125 falihűtő','199','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 1250 mm\r\nVégelem:     45 mm/db\r\nMélység:       700mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','Oxford 70/125','- beépített aggregát\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - tükrös végelem\r\n - cseppvíztálca\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('77','77','Oxford 70/180 falihűtő','200','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 1800 mm\r\nVégelem:     45 mm/db\r\nMélység:       700mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','Oxford 70/180 falihűtő','- beépített aggregát\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - tükrös végelem\r\n - cseppvíztálca\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('78','78','Oxford 70/250 falihűtő','201','0','1.8','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 2500 mm\r\nVégelem:     45 mm/db\r\nMélység:       700mm\r\nMagasság:  1935 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','Oxford 70/250','- beépített aggregát\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - tükrös végelem\r\n - cseppvíztálca\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('79','79','Ouverture 90 falihűtő','140','0','1.65','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 918 mm\r\nVégelem:     45 mm/db\r\nMélység:       845 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Ouverture 90','- beépített aggregát\r\n - 4 sor 410 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - külön irányú mosóvíz elvezetés\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('80','80','Ouverture 120 falihűtő','196','0','1.65','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 1224 mm\r\nVégelem:     45 mm/db\r\nMélység:       845 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Ouverture 120','- beépített aggregát\r\n - 4 sor 410 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - külön irányú mosóvíz elvezetés\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('81','81','Ouverture 180 falihűtő','197','0','1.65','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 1836 mm\r\nVégelem:     45 mm/db\r\nMélység:       845 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Ouverture 180','- beépített aggregát\r\n - 4 sor 410 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - külön irányú mosóvíz elvezetés\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('82','82','Ouverture 240 falihűtő','198','0','1.65','0','1','','Fali tejhűtő beépített aggregáttal','8418','Hosszúság: 2448 mm\r\nVégelem:     45 mm/db\r\nMélység:       845 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Ouverture 240','- beépített aggregát\r\n - 4 sor 410 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - külön irányú mosóvíz elvezetés\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n- két kompresszoros kivitel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('83','83','Opera STD 90 falihűtő','251','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság: 950 mm\r\nMélység:       705 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Opera STD 90','- beépített aggregát\r\n - 3 sor 360 mm-es dönthető polc\r\n - 1 sor 310 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('84','84','Opera STD 120 falihűtő','252','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság: 1280 mm\r\nMélység:         705 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Opera STD 120','- beépített aggregát\r\n - 3 sor 360 mm-es dönthető polc\r\n - 1 sor 310 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('85','85','Opera STD 180 falihűtő','253','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság: 1835 mm\r\nMélység:         705 mm\r\nMagasság:  2000 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Opera STD 180','- beépített aggregát\r\n - 3 sor 360 mm-es dönthető polc\r\n - 1 sor 310 mm-es dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('86','86','Apollo SL 90 falihűtő','4161','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  900 mm\r\nMélység:        900 mm\r\nMagasság:  1475 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1076 W','Apollo SL 90 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('87','87','Apollo SL 125 falihűtő','4181','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  1250 mm\r\nMélység:        900 mm\r\nMagasság:  1475 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1254 W','Apollo SL 125 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('88','88','Apollo SL 180 falihűtő','4201','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  1800 mm\r\nMélység:        900 mm\r\nMagasság:  1475 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1506 W','Apollo SL 180 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('89','89','Apollo SL 250 falihűtő','4221','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  2500 mm\r\nMélység:        900 mm\r\nMagasság:  1475 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 2279 W','Apollo SL 250 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('90','90','SARA 1400/700 falihűtő','5701','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság:  705 mm\r\nMélység:        695 mm\r\nMagasság:  1426 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 816 W\r\nVillamos teljesítmény: 430 W','SARA 1400/700 falihűtő','- beépített aggregát\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('91','91','SARA 1400/1000 falihűtő','5721','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság:  1017 mm\r\nMélység:        695 mm\r\nMagasság:  1426 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1093 W\r\nVillamos teljesítmény: 630 W','SARA 1400/1000 falihűtő','- beépített aggregát\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('92','92','SARA 1400/1330 falihűtő','5741','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság:  1330 mm\r\nMélység:        695 mm\r\nMagasság:  1426 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1314 W\r\nVillamos teljesítmény: 950 W','SARA 1400/1330 falihűtő','- beépített aggregát\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('93','93','SARA 1400/1995 falihűtő','5761','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság:  1995 mm\r\nMélység:        695 mm\r\nMagasság:  1426 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1661W\r\nVillamos teljesítmény: 1200 W','SARA 1400/1995falihűtő','- beépített aggregát\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('94','94','Opera SV 14/90 falihűtő','262','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság:  950 mm\r\nMélység:        705 mm\r\nMagasság:  1460 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Opera SV 14/90','- beépített aggregát\r\n - 4 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('95','95','Opera SV 14/120 falihűtő','264','0','1.65','0','1','','Fali hűtő beépített aggregáttal','8418','Hosszúság:  1250 mm\r\nMélység:          705 mm\r\nMagasság:   1450 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','New Opera SV 14/120','- beépített aggregát\r\n - 4 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - automatikus cseppvíz elpárologtatás\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('96','96','OXFORD 80 SL 90 Falihűtő','3561','0','2','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  900 mm\r\nVégelem:     45 mm/db\r\nMélység:       835mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:945 W','OXFORD SL 90','- aggregát nélkül\r\n - 5 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('97','97','OXFORD 80 SL 125 Falihűtő','3581','0','2','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  1250 mm\r\nVégelem:     45 mm/db\r\nMélység:       835mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:1313 W','OXFORD 80 SL 125','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('98','98','OXFORD 80 SL 180 Falihűtő','286','0','2','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  1800 mm\r\nVégelem:     45 mm/db\r\nMélység:       835mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:2150 W','OXFORD 80 SL 180','- aggregát nélkül\r\n - 5 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('99','99','OXFORD 80 SL  250 Falihűtő','3601','0','2','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  2500 mm\r\nVégelem:     45 mm/db\r\nMélység:       835mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3025 W','OXFORD 80 SL 250','- aggregát nélkül\r\n - 5 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('100','100','OXFORD 80 SL 305 Falihűtő','3621','0','2','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  3050 mm\r\nVégelem:     45 mm/db\r\nMélység:       835mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3690 W','OXFORD 80 SL 305','- aggregát nélkül\r\n - 5 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('101','101','OXFORD 80 SL 375 Falihűtő','3641','0','2','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nVégelem:     45 mm/db\r\nMélység:       835mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:4536 W','OXFORD 80 SL 375','- aggregát nélkül\r\n - 5 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('102','102','Penguin 90 SL 90 Falihűtő','284','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  900 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:945 W','Penguin 90 SL 90','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('103','103','Penguin 90 SL 125 Falihűtő','285','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  1250 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:1313 W','Penguin 90 SL 125','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('104','104','Penguin 90 SL 180 Falihűtő','3661','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  1800 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:2150 W','Penguin 90 SL 180','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('105','105','Penguin 90 SL 250 Falihűtő','3541','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  2500 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3025 W','Penguin 90 SL 250','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('106','106','Penguin 90 SL 305 Falihűtő','288','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  3050 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3690 W','Penguin 90 SL 305','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('107','107','Penguin 90 SL 375 Falihűtő','289','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:4536 W','Penguin 90 SL 375','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('108','108','Penguin 90 SL 125 SD Falihűtő','290','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  1250 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:1100 W','Penguin 90 SL 125 SD','- aggregát nélkül\r\n - hőszigetelt tolóajtókkal\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('109','109','Penguin 90 SL 250 SD Falihűtő','291','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  2500 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:2900 W','Penguin 90 SL 250 SD','- aggregát nélkül\r\n - hőszigetelt tolóajtókkal\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('110','110','Penguin 90 SL 375 SD Falihűtő','292','0','2.3','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3900 W','Penguin 90 SL 375 SD','- aggregát nélkül\r\n - hőszigetelt tolóajtókkal\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('111','111','Penguin 90 FV 90 Zöldséghűtő','357','0','2.3','0','1','','Fali zöldséghűtő telepített aggregáttal','8418','Hosszúság:  900 mm\r\nVégelem:     45 mm/db\r\nMélység:       905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:945 W','Penguin 90 FV90','- aggregát nélkül\r\n - 2 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - döntött tükör','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('112','112','Penguin 90 FV 125 Zöldséghűtő','358','0','2.3','0','1','','Fali zöldséghűtő telepített aggregáttal','8418','Hosszúság:  1250 mm\r\nVégelem:          45 mm/db\r\nMélység:         905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:1075 W','Penguin 90 FV 125','- aggregát nélkül\r\n - 2 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - döntött tükör','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('113','113','Penguin 90 FV 180 Zöldséghűtő','359','0','2.3','0','1','','Fali zöldséghűtő telepített aggregáttal','8418','Hosszúság:  1800 mm\r\nVégelem:           45 mm/db\r\nMélység:         905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:1972 W','Penguin 90 FV 180','- aggregát nélkül\r\n - 2 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - döntött tükör','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('114','114','Penguin 90 FV 250 Zöldséghűtő','360','0','2.3','0','1','','Fali zöldséghűtő telepített aggregáttal','8418','Hosszúság:  2500 mm\r\nVégelem:           45 mm/db\r\nMélység:         905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:2990 W','Penguin 90 FV 250','- aggregát nélkül\r\n - 2 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - döntött tükör','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('115','115','Penguin 90 FV 305 Zöldséghűtő','361','0','2.3','0','1','','Fali zöldséghűtő telepített aggregáttal','8418','Hosszúság:  3050 mm\r\nVégelem:           45 mm/db\r\nMélység:         905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3648 W','Penguin 90 FV 305','- aggregát nélkül\r\n - 2 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - döntött tükör','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('116','116','Penguin 90 FV 375 Zöldséghűtő','362','0','2.3','0','1','','Fali zöldséghűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nVégelem:           45 mm/db\r\nMélység:         905mm\r\nMagasság:  2020 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:3648 W','Penguin 90 FV 375','- aggregát nélkül\r\n - 2 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - döntött tükör','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('117','117','Penguin Low SL 90 falihűtő','6141','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  900 mm\r\nMélység:        1007 mm\r\nMagasság:  1520 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1076 W','Penguin Low SL 90 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('118','118','Penguin Low SL 125 falihűtő','6161','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  1250 mm\r\nMélység:        1007 mm\r\nMagasság:  1520 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 1313 W','Penguin Low SL 125 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('119','119','Penguin Low SL 180 falihűtő','6221','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  1800 mm\r\nMélység:        1007 mm\r\nMagasság:  1520 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 2150 W','Penguin Low SL 180 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('120','120','Penguin Low SL 250 falihűtő','6201','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  2500 mm\r\nMélység:        1007 mm\r\nMagasság:  1520 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 3025 W','Penguin Low SL 250  falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('121','121','Penguin Low SL 305 falihűtő','6241','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  3050 mm\r\nMélység:        1007 mm\r\nMagasság:  1520 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 3690 W','Penguin Low SL 305  falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('122','122','Penguin Low SL 375 falihűtő','6261','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nMélység:        1007 mm\r\nMagasság:  1520 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 4535 W','Penguin Low SL 375  falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('123','123','Lion Narrow 375','296','0','1.65','0','1','','Fali tejhűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nVégelem:           45 mm/db\r\nMélység:         885 mm\r\nMagasság:  2030 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény igény:4200 W','Lion Narrow 375','- aggregát nélkül\r\n - 4 sor polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - végelem nélkül\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('124','124','Apollo SL 305 falihűtő','4241','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  3050 mm\r\nMélység:        900 mm\r\nMagasság:  1475 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 2650 W','Apollo SL 305 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('125','125','Apollo SL 375 falihűtő','4261','0','2.3','0','1','','Fali hűtő telepített aggregáttal','8418','Hosszúság:  3750 mm\r\nMélység:        900 mm\r\nMagasság:  1475 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C\r\nHűtőteljesítmény: 2950 W','Apollo SL 375 falihűtő','- aggregát nélkül\r\n - 2 sor dönthető polc\r\n - elektronikus vezérlés\r\n - beépített világítás\r\n - panoráma végelem\r\n - kocsiütköző\r\n - ártartó sín polconként\r\n - ventilációs hűtés\r\n - manuális éjszakai roló\r\n - rozsdamentes alsó tároló','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('126','126','RODAN 120 B','138','0','1.8','0','1','Rozsdamentes belső burkolat','Füstöltáru hűtő','8418','Hosszúság: 1244mm\r\nMagasság:   1550 mm\r\nMélység:        400/657 mm\r\nHűtési tartomány:  +14/+16 °C','RCW 120','- beépített aggregáttal\r\n - elektronikus vezérléssel\r\n - 1 sor polccal\r\n - 2 sor rozsdamentes fogassal\r\n - ventilációs hűtés\r\n - világító fríz\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('127','127','RODAN 150 B','183','0','1.8','0','1','Rozsdamentes belső burkolat','Füstöltáru hűtő','8418','Hosszúság: 1544mm\r\nMagasság:   1550 mm\r\nMélység:        400/657 mm\r\nHűtési tartomány:  +14/+16 °C','RCW 150','- beépített aggregáttal\r\n - elektronikus vezérléssel\r\n - 1 sor polccal\r\n - 2 sor rozsdamentes fogassal\r\n - ventilációs hűtés\r\n - világító fríz\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('128','128','RODAN 180 B','184','0','1.8','0','1','Rozsdamentes belső burkolat','Füstöltáru hűtő','8418','Hosszúság: 1844mm\r\nMagasság:   1550 mm\r\nMélység:        400/657 mm\r\nHűtési tartomány:  +14/+16 °C','RCW 180','- beépített aggregáttal\r\n - elektronikus vezérléssel\r\n - 1 sor polccal\r\n - 2 sor rozsdamentes fogassal\r\n - ventilációs hűtés\r\n - világító fríz\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('129','129','RODAN 120 T','185','0','1.8','0','1','Rozsdamentes belső burkolat','Füstöltáru hűtő','8418','Hosszúság: 1244mm\r\nMagasság:   1550 mm\r\nMélység:        400/657 mm\r\nHűtési tartomány:  +14/+16 °C','RCW 120 ZZ','- telepített aggregáttal\r\n - elektronikus vezérléssel\r\n - 1 sor polccal\r\n - 2 sor rozsdamentes fogassal\r\n - ventilációs hűtés\r\n - világító fríz\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('130','130','RODAN 150 T','186','0','1.8','0','1','Rozsdamentes belső burkolat','Füstöltáru hűtő','8418','Hosszúság: 1544mm\r\nMagasság:   1550 mm\r\nMélység:        400/657 mm\r\nHűtési tartomány:  +14/+16 °C','RCW 150 ZZ','- telepített aggregáttal\r\n - elektronikus vezérléssel\r\n - 1 sor polccal\r\n - 2 sor rozsdamentes fogassal\r\n - ventilációs hűtés\r\n - világító fríz\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('131','131','RODAN 180 T','187','0','1.8','0','1','Rozsdamentes belső burkolat','Füstöltáru hűtő','8418','Hosszúság: 1844mm\r\nMagasság:   1550 mm\r\nMélység:        400/657 mm\r\nHűtési tartomány:  +14/+16 °C','RCW 180 ZZ','- telepített aggregáttal\r\n - elektronikus vezérléssel\r\n - 1 sor polccal\r\n - 2 sor rozsdamentes fogassal\r\n - ventilációs hűtés\r\n - világító fríz\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('132','132','SPIO 101 Wood','2881','0','1.65','0','1','Inox burkolat','Füstöltáru hűtő','8418','Hosszúság: 968mm\r\nMagasság:   1140 mm\r\nMélység:        580 mm\r\nHűtési tartomány:  +5/+8 °C\r\nTeljesítmény: 396 W\r\nHűtőközeg: R 134a','SPIO 101 Wood','- beépített aggregáttal\r\n - elektronikus vezérléssel\r\n - belső világítás\r\n - 2 sor polccal\r\n - ventilációs hűtés\r\n - üveg tolóajtó\r\n - inox belső\r\n - döntött tükör','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('133','133','SPIO 135 Wood','2901','0','1.65','0','1','Inox burkolat','Füstöltáru hűtő','8418','Hosszúság: 1335mm\r\nMagasság:   1140 mm\r\nMélység:        580 mm\r\nHűtési tartomány:  +5/+8 °C\r\nTeljesítmény: 396 W\r\nHűtőközeg: R 134a','SPIO 135 Wood','- beépített aggregáttal\r\n - elektronikus vezérléssel\r\n - belső világítás\r\n - 2 sor polccal\r\n - ventilációs hűtés\r\n - üveg tolóajtó\r\n - inox belső\r\n - döntött tükör','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('134','134','Kornel 2500','5681','0','1.65','0','1','','Füstöltáru hűtő','8418','Hosszúság: 2500mm\r\nMagasság:   2153 mm\r\nMélység:        642 mm\r\nHűtési tartomány:  +2/+8 °C\r\nTeljesítmény: -- W\r\nHűtőközeg: R 404 A','Kornel 2500','- aggregát nélkül\r\n - éjszakai rolóval\r\n - elektronikus vezérléssel\r\n - belső világítás\r\n - 4 sor polccal\r\n - ventilációs hűtés\r\n - alsó tárolószekrény\r\n - festett belső','','','25 °C hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('135','135','Üvegajtós hűtő UA400J','4401','0','1.65','0','0','','Hűtővitrin','8418','Hosszúság: 595 mm\r\nMélység:       620 mm\r\nMagasság  1950 mm\r\nHűtési tartomány: 0/+8 °C\r\nŰrtartalom: 360 l\r\nTeljesítmény: 205 W\r\nFogyasztás: 1,8 kW/24h','Üvegajtós hűtő UA400J','. festett külső és alu. belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 5 sor rácspolc (műanyag bevonatú fém)\r\n - görgős lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventilációs hűtés\r\n - CFC mentes szigetelés\r\n - belső világítás\r\n - ajtózár\r\n - világító fríz','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('136','136','Üvegajtós hűtő UA600FV','6723','0','1.65','0','0','','Hűtővitrin nyíló ajtóval','8418','Hosszúság: 900mm\r\nMélység:       725 mm\r\nMagasság  2050 mm\r\nÜrtartalom: 600 l\r\nHűtési tartomány: 0/+8 °C\r\nTeljesítmény: 450 W\r\nFogyasztás: 2,95 kW/24h','Üvegajtós hűtő UA600FV','. festett külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 4 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventilációs hűtés\r\n - CFC mentes szigetelés\r\n - belső világítás\r\n - ajtózár\r\n - világító fríz','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('137','137','Üvegajtós hűtő UA1000FV','4381','0','1.65','0','0','','Hűtővitrin tolóajtóval','8418','Hosszúság: 1110mm\r\nMélység:       725 mm\r\nMagasság  2105 mm\r\nÜrtartalom: 900 l\r\nHűtési tartomány: 0/+8 °C\r\nTeljesítmény: 600 W\r\nFogyasztás: 4,5 kW/24h','Üvegajtós hűtő UA1000FV','. festett külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 4 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventilációs hűtés\r\n - CFC mentes szigetelés\r\n - belső világítás\r\n - ajtózár\r\n - világító fríz','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('138','138','EIS 40.2','2841','0','1.65','0','1','','Üvegajtós mirelithűtő','8418','Hosszúság: 670 mm\r\nMélység:       692 mm\r\nMagasság  1957 mm\r\nHűtési tartomány: -25 °C\r\nŰrtartalom: 397 l\r\nVillamos teljesítmény: 620 W','EIS 40.2','. festett külső és belső borítás\r\n - mechanikus vezérlés\r\n - beépített aggregát\r\n - 4 sor rácspolc\r\n - állítható lábak\r\n - cseppvíz elpárologtatás\r\n - statikus hűtés\r\n - CFC mentes szigetelés','','','20 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('139','139','EIS 45.2','6730','0','1.65','0','1','','Üvegajtós mirelithűtő','8418','Hosszúság: 670 mm\r\nMélység:       692 mm\r\nMagasság  1957 mm\r\nHűtési tartomány: -30 °C\r\nŰrtartalom: 397 l\r\nVillamos teljesítmény: 620 W','EIS 45.2','. festett külső és belső borítás\r\n - mechanikus vezérlés\r\n - beépített aggregát\r\n - 4 sor rácspolc\r\n - állítható lábak\r\n - cseppvíz elpárologtatás\r\n - ventillációs hűtés\r\n - CFC mentes szigetelés','','','20 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('140','140','POLO 300','2781','0','1.65','0','1','','Üvegajtós mirelithűtő','8418','Hosszúság: 595 mm\r\nMélység:       640 mm\r\nMagasság  1840 mm\r\nHűtési tartomány: -12/-22 °C\r\nŰrtartalom: 270 l\r\nVillamos teljesítmény: 620 W','POLO 300','. festett külső és belső borítás\r\n - mechanikus vezérlés\r\n - beépített aggregát\r\n - 6 sor rácspolc 460x400 mm\r\n - állítható lábak\r\n - cseppvíz elpárologtatás\r\n - ventilációs hűtés\r\n - CFC mentes szigetelés\r\n - belső világítás','','','30 °C környezeti hőmérséklet\r\n55 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('141','141','POLO 450','2801','0','1.65','0','1','','Üvegajtós mirelithűtő','8418','Hosszúság: 680 mm\r\nMélység:       740 mm\r\nMagasság  1965 mm\r\nHűtési tartomány: -18/-24 °C\r\nŰrtartalom: 578 l\r\nVillamos teljesítmény: 1000 W','POLO 450','. festett külső és belső borítás\r\n - mechanikus vezérlés\r\n - beépített aggregát\r\n - 4 sor rácspolc 548x488 mm\r\n - állítható lábak\r\n - cseppvíz elpárologtatás\r\n - ventilációs hűtés\r\n - CFC mentes szigetelés\r\n - belső világítás\r\n - fríz világítás','','','35 °C környezeti hőmérséklet\r\n75 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('142','142','POLO 900','2821','0','1.65','0','1','','Üvegajtós mirelithűtő','8418','Hosszúság: 1360 mm\r\nMélység:       740 mm\r\nMagasság  1965 mm\r\nHűtési tartomány: -18/-24 °C\r\nŰrtartalom: 1078 l\r\nVillamos teljesítmény: 1100 W','POLO 900','. festett külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 2x4 sor rácspolc 615x445 mm\r\n - állítható lábak\r\n - cseppvíz elpárologtatás\r\n - ventilációs hűtés\r\n - CFC mentes szigetelés\r\n - belső világítás\r\n - fríz világítás','','','30 °C környezeti hőmérséklet\r\n75 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('143','143','AFV 700 TN 1P +','139','0','1.65','0','1','','Rozsdamentes háttérhűtő normál','8418','Hosszúság: 740 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C','AFV 700 TN 1P +','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('144','144','AFV 700 TN 1PV +','190','0','1.65','0','1','','Rozsdamentes üvegajtós hűtő normál','8418','Hosszúság: 740 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C','AFV 700 TN 1PV +','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('145','145','AFV 700 BT 1P -','146','0','1.65','0','1','','Rozsdamentes háttérhűtő mirelit','8418','Hosszúság: 740 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -18/-20 °C','AFV 700 BT 1P -','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('146','146','AFV 700 BT 1PV -','191','0','1.65','0','1','','Rozsdamentes üvegajtós hűtő mirelit','8418','Hosszúság: 740 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -18/-20 °C','AFV 700 BT 1PV -','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('147','147','AFV 700 TN 2P +','6726','0','1.65','0','1','','Rozsdamentes háttérhűtő normál','8418','Hosszúság: 710 mm\r\nMélység:       800 mm\r\nMagasság  2030 mm\r\nHűtési tartomány: 0/+10 °C','AFV 700 TN 2P +','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret\r\n - 1 légterű','','','43 °C környezeti hőmérséklet\r\n65 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('148','148','AFV 700 TN+TN','6729','0','1.65','0','1','','Rozsdamentes háttérhűtő normál/normál  2 légtérrel','8418','Hosszúság: 710 mm\r\nMélység:       800 mm\r\nMagasság  2030 mm\r\nHűtési tart. TN: 0/+10 °C','AFV 700 TN+TN','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret\r\n - 2 légterű','','','43 °C környezeti hőmérséklet\r\n65 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('149','149','AFV 700 TN+BT','6728','0','1.65','0','1','','Rozsdamentes háttérhűtő mirelit/normál  2 légtérrel','8418','Hosszúság: 710 mm\r\nMélység:       800 mm\r\nMagasság  2030 mm\r\nHűtési tart. BT: -18/-22 °C\r\nHűtési tart. TN: 0/+10 °C','AFV 700 TN+BT','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret\r\n - 2 légterű','','','43 °C környezeti hőmérséklet\r\n65 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('150','150','AFV 700 BT+BT','6727','0','1.65','0','1','','Rozsdamentes háttérhűtő mirelit 2 légtérrel','8418','Hosszúság: 710 mm\r\nMélység:       800 mm\r\nMagasság  2030 mm\r\nHűtési tartomány: -18/-22 °C','AFV 700 BT+BT','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret\r\n - 2 légterű','','','43 °C környezeti hőmérséklet\r\n65 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('151','151','AFV 1400 TN 2P +','148','0','1.65','0','1','','Rozsdamentes háttérhűtő normál','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C','AFV 1400 TN 2P +','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('152','152','AFV 1400 TN 2PV +','192','0','1.65','0','1','','Rozsdamentes üvegajtós hűtő normál','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C','AFV 1400 TN 2PV +','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('153','153','AFV 1400 BT 2P -','147','0','1.65','0','1','','Rozsdamentes háttérhűtő mirelit','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -18/-20 °C','AFV 1400 BT 2P -','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('154','154','AFV 1400 BT2PV -','193','0','1.65','0','1','','Rozsdamentes üvegajtós hűtő normál','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -18/-22 °C','AFV 1400 BT 2PV -','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('155','155','AFV 1400 TN+TN 2P +','188','0','1.65','0','1','','Rozsdamentes háttérhűtő normál \r\nkét hűtött térrel','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C','AFV 1400 TN+TN 2P +','. rozsdamentes külső és belső borítás\r\n- osztott tér\r\n- két kompresszoros\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('156','156','AFV 1400 TN+TN 2PV +','194','0','1.65','0','1','','Rozsdamentes üvegajtós hűtő normál,\r\nkét hűtött térrel','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C','AFV 1400 TN+TN 2PV +','. rozsdamentes külső és belső borítás\r\n- osztott tér\r\n- két kompresszoros\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('157','157','AFV 1400 TN+BT 2P +-','189','0','1.65','0','1','','Rozsdamentes háttérhűtő \r\nnormál/mély két hűtött térrel','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C/-18/-20 °C','AFV 1400 TN+BT 2P +-','. rozsdamentes külső és belső borítás\r\n- osztott tér\r\n- két kompresszoros\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('158','158','AFV 1400 TN+BT 2PV +-','195','0','1.65','0','1','','Rozsdamentes üvegajtós hűtő \r\nnormál/mély két hűtött térrel','8418','Hosszúság: 1440 mm\r\nMélység:       815 mm\r\nMagasság  2010 mm\r\nHűtési tartomány: -2/+8 °C/-18/-20 °C','AFV 1400 TN+BT 2PV +-','. rozsdamentes külső és belső borítás\r\n- osztott tér\r\n- két kompresszoros\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 3 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 2/1 belső méret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('159','159','Wine USD','4341','0','2','0','1','','Bor bemutatóhűtő','8418','Hosszúság: 595 mm\r\nMélység:       640 mm\r\nMagasság  1840 mm\r\nŰrtartalom:   345 liter\r\nHűtési tartomány: +10/+18 °C\r\nVillamos teljesítmény: 270 W\r\nHűtőközeg: R134A','Wine USD','- festett külső és belső borítás\r\n - hőszigetelt üvegajtó\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 5 sor fa rácspolc\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventillációs technológia\r\n - CFC mentes szigetelés\r\n - világítással','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('160','160','Rugiada Vino','541','0','1.65','0','1','','Bor bemutatóhűtő','8418','Hosszúság: 593 mm\r\nMélység:       611 mm\r\nMagasság  1830 mm\r\nŰrtartalom:   375 liter\r\nHűtési tartomány: +3/+8 °C','Rugiada Vino','. festett külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 5 sor rácspolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventillációs technológia\r\n - CFC mentes szigetelés\r\n - világítással','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('161','161','Rugiada Kantina','561','0','1.65','0','1','','Bor bemutatóhűtő','8418','Hosszúság: 593 mm\r\nMélység:       611 mm\r\nMagasság  1830 mm\r\nŰrtartalom:   375 liter\r\nHűtési tartomány: +3/+8 °C','Rugiada Kantina','. festett külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 5 sor rüvegpolc (műanyag bevonatú fém)\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventillációs technológia\r\n - CFC mentes szigetelés\r\n - belső világítás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('162','162','Vino 1','581','0','1.65','0','1','','Bor bemutatóhűtő','8418','Hosszúság: 673 mm\r\nMélység:       606 mm\r\nMagasság  1905 mm\r\nŰrtartalom:   390 liter\r\nHűtési tartomány: +3/+14 °C','Vino 1','. fa külső és festett belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 6 sor farácspolc\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventilációs technológia\r\n - CFC mentes szigetelés\r\n - világítással','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('163','163','Vino 2','601','0','1.65','0','1','','Bor bemutatóhűtő','8418','Hosszúság: 1266 mm\r\nMélység:         606 mm\r\nMagasság  1905 mm\r\nŰrtartalom:   780 liter\r\nHűtési tartomány: +3/+14 °C','Vino 2','. fa külső és festett belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - 6 sor farácspolc\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - ventilációs technológia\r\n - CFC mentes szigetelés\r\n - világítással','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('164','164','Hűtött asztal 2 ajtós VSG','3981','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 1200 mm\r\nMélység:       700 mm\r\nMagasság  850/920 mm\r\nKapacitás: 460 l\r\nHűtési tartomány: 0/+10 °C\r\nHűtőteljesítmény: 495 W\r\nHűtőközeg: R404A','TF02EKOGNSG','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - aggregát nélkül\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('165','165','Hűtött asztal 3 ajtós V','701','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 1870 mm\r\nMélység:       700 mm\r\nMagasság  850/920 mm\r\nKapacitás: 460 l\r\nHűtési tartomány: 0/+10 °C\r\nHűtőteljesítmény: 495 W\r\nHűtőközeg: R404A','TF03EKOGN','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('166','166','Hűtött asztal 4 ajtós V','721','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 2320 mm\r\nMélység:       700 mm\r\nMagasság  850/920 mm\r\nKapacitás: 620 l\r\nHűtési tartomány: 0/+10 °C\r\nHűtőteljesítmény: 495 W\r\nHűtőközeg: R404A','TF04EKOGN','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('167','167','Hűtött asztal 3 ajtós VSG','4001','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 1650 mm\r\nMélység:       700 mm\r\nMagasság  850/920 mm\r\nKapacitás: 460 l\r\nHűtési tartomány: 0/+10 °C\r\nHűtőteljesítmény: 495 W\r\nHűtőközeg: R404A','TF03EKOGN','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - aggregát nélkül\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('168','168','Hűtött asztal 4 ajtós VSG','4021','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 2100 mm\r\nMélység:       700 mm\r\nMagasság  850/920 mm\r\nKapacitás: 620 l\r\nHűtési tartomány: 0/+10 °C\r\nHűtőteljesítmény: 495 W\r\nHűtőközeg: R404A','TF04EKOGNSG','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - aggregát nélkül\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('169','169','Hűtött asztal 2 ajtós V BT','1061','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 1420 mm\r\nMélység:       715 mm\r\nMagasság  850/920 mm\r\nKapacitás: 460 l\r\nHűtési tartomány: -18/-22 °C\r\nHűtőteljesítmény: 655 W\r\nHűtőközeg: R404A','TF02MIDBT','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('170','170','Hűtött asztal 3 ajtós VBT','1081','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 1870 mm\r\nMélység:       715 mm\r\nMagasság  850/920 mm\r\nKapacitás: 460 l\r\nHűtési tartomány: -18/-20 °C\r\nHűtőteljesítmény: 700 W\r\nHűtőközeg: R404A','TF03EKOGN','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('171','171','Hűtött asztal 4 ajtós VBT','1101','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 2330 mm\r\nMélység:       715 mm\r\nMagasság  850/920 mm\r\nKapacitás: 620 l\r\nHűtési tartomány: -18/-22 °C\r\nHűtőteljesítmény: 728 W\r\nHűtőközeg: R404A','TF04MIDBT','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - GN 1/1 belső méret','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('172','172','Hűtött asztal 2 ajtós','761','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 904 mm\r\nMélység:       700 mm\r\nMagasság    880 mm\r\nKapacitás: 220 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 280 W\r\nHűtőközeg: R404A','SL02NX','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('173','173','Hűtött asztal 3 ajtós','861','0','1.65','0','0','','Rozsdamentes hűtött asztal','8418','Hosszúság: 1384 mm\r\nMélység:       700 mm\r\nMagasság    880 mm\r\nKapacitás: 350 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 300 W\r\nHűtőközeg: R404A','SL02NX','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('174','174','Zöldségfeltét hűtőasztal','741','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 900 mm\r\nMélység:       700 mm\r\nMagasság    880 mm\r\nKapacitás: 220 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 280 W\r\nHűtőközeg: R404A','SL02EKO','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - nyitható munkapult\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('175','175','Zöldségfeltét hűtőasztal 3A','881','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 1380 mm\r\nMélység:       700 mm\r\nMagasság    880 mm\r\nKapacitás: 350 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 300 W\r\nHűtőközeg: R404A','SL03EKO','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - nyitható munkapult\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('176','176','Zöldségfeltét hűtőasztal VC','821','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 900 mm\r\nMélység:       700 mm\r\nMagasság    1150 mm\r\nKapacitás: 220 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 280 W\r\nHűtőközeg: R404A','SL02VC','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - ívelt üveg leheletvédővel\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('177','177','Zöldségfeltét hűtőasztal 3AVC','901','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 1380 mm\r\nMélység:       700 mm\r\nMagasság    1150 mm\r\nKapacitás: 350 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 300 W\r\nHűtőközeg: R404A','SL03VC','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - ívelt üveg leheletvédővel\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('178','178','Hűtött asztal hűtöt vitrinnel 3A','921','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 1410 mm\r\nMélység:         700 mm\r\nMagasság    1525 mm\r\nKapacitás: 220 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 280 W\r\nHűtőközeg: R404A','SL03PZVR4','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - hűtött felépítmény\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('179','179','Hűtött asztal hűtöt vitrinnel 2A6F','841','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 1410 mm\r\nMélység:         700 mm\r\nMagasság    1525 mm\r\nKapacitás: 220 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 280 W\r\nHűtőközeg: R404A','SL02C6VR4','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - hűtött felépítmény\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('180','180','Hűtött asztal hűtöt vitrinnel 3A6F','941','0','1.65','0','0','','Rozsdamentes hűtött zöldség előkészítő asztal','8418','Hosszúság: 1900 mm\r\nMélység:         700 mm\r\nMagasság    1525 mm\r\nKapacitás: 350 l\r\nHűtési tartomány: +4/+10 °C\r\nHűtőteljesítmény: 300 W\r\nHűtőközeg: R404A','SL03C6VR4','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - automata cseppvíz elpárologtatás\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - statikus hűtés\r\n - hűtött felépítmény\r\n - GN edényzet nem tartozék','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('181','181','ORION 110','1701','0','1.8','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 1100 mm\r\nMélység:       1242 mm\r\nMagasság:     840 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','ORION 110','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('182','182','ORION 140','1721','0','1.8','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 1400 mm\r\nMélység:       1242 mm\r\nMagasság:     840 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','ORION 140','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('183','183','Cily PB hűtősziget','3341','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 600 mm\r\nMélység:       600 mm\r\nMagasság:   890 mm\r\nR 134 A környezetbarát hűtőközeg\r\nHőfok: 0/+10°C','Cily PB hűtősziget','- nyíló üvegtetővel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('184','184','MiniGlobo PB hűtősziget','3461','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 560 mm\r\nMélység:       676 mm\r\nMagasság:   1161 mm\r\nR 134 A környezetbarát hűtőközeg\r\nHőfok: 0/0°C','MiniGlobo PB hűtősziget','- toló üvegtetővel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventilációs hűtés\r\n - display','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('185','185','Orinoko GI 60 hűtősziget','3521','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság:   650 mm\r\nMélység:         622 mm\r\nMagasság:   1127 mm\r\nR 507 környezetbarát hűtőközeg\r\nHőfok: +3/+7°C\r\nvillamos teljesítmény: 415 W','Orinoko GI 60 hűtősziget','- nyitott kivitel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventilációs hűtés\r\n - display','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('186','186','Impuls 70 P hűtősziget','3361','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 707 mm\r\nMélység:       750 mm\r\nMagasság:   1058 mm\r\nR 134 A környezetbarát hűtőközeg\r\nHőfok: 0/+7°C','Impuls 70 P hűtősziget','- nyitott kivitel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('187','187','Impuls 100 N hűtősziget','3421','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1007 mm\r\nMélység:         750 mm\r\nMagasság:   1058 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25/-10°C','Impuls 100 N hűtősziget','- nyitott kivitel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('188','188','FIJI NT 150','5821','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 1500 mm\r\nMélység:       1005mm\r\nMagasság:     910 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','FIJI NT 150','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('189','189','FIJI NT 200','5841','0','1.65','0','1','','normálhűtő beépített aggregáttal','8418','Hosszúság: 2000 mm\r\nMélység:       1005mm\r\nMagasság:     910 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -+2/+8°C','FIJI NT 200','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('190','190','FIJI TN 250','5861','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 2500 mm\r\nMélység:       1005mm\r\nMagasság:     910 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+8 °C','FIJI TN 250','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('191','191','Carello Maxi hűtősziget','6441','0','1.65','0','1','','saláta és hidegkonyha hűtő beépített aggregáttal','8418','Hosszúság: 1139 mm\r\nMélység:       718 mm\r\nMagasság:   1110mm\r\nAlapszín: tölgy-cseresznye-juhar\r\nR 134a környezetbarát hűtőközeg\r\nHőfok: +4/+10°C','Carello Maxi hűtősziget','- zárt kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - plexi kupola\r\n - önbeálló kerekekkel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('192','192','OASI 4 hűtősziget','6421','0','1.65','0','1','','saláta és hidegkonyha hűtő beépített aggregáttal','8418','Hosszúság: 1075 mm\r\nMélység:       1075 mm\r\nMagasság:   1319/1710mm\r\nAlapszín: tölgy-cseresznye-juhar\r\nR 134a környezetbarát hűtőközeg\r\nHőfok: +4/+10°C','OASI 4 hűtősziget','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - motoros plexi kupola\r\n - önbeálló kerekekkel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('193','193','Girotondo hűtősziget','5981','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 1500 mm\r\nMélység:       1500 mm\r\nMagasság:   1595/1212mm\r\nAlapszín: rozsdamentes\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','Girotondo hűtősziget','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventilációs hűtés\r\n - motoros kupola\r\n - önbeálló kerekekkel\r\n - LED világítás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('194','194','ECLIPSE Luna hűtősziget','6461','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 1410 mm\r\nMélység:       1410 mm\r\nMagasság:   950 mm\r\nAlapszín: szürke\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','ECLIPSE Luna hűtősziget','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - panoráma oldalak\r\n - ventilációs hűtés\r\n - hűtetlen felépítmény 1 polcos\r\n - önbeálló kerekekkel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('195','195','ECLIPSE Goccia hűtősziget','6481','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 1755 mm\r\nMélység:       1410 mm\r\nMagasság:   950 mm\r\nAlapszín: szürke\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','ECLIPSE Goccia hűtősziget','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - panoráma oldalak\r\n - ventilációs hűtés\r\n - hűtetlen felépítmény 1 polcos\r\n - önbeálló kerekekkel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('196','196','ECLIPSE Elisse hűtősziget','6501','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 2105 mm\r\nMélység:       1410 mm\r\nMagasság:   950 mm\r\nAlapszín: szürke\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','ECLIPSE Elisse hűtősziget','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - panoráma oldalak\r\n - ventilációs hűtés\r\n - hűtetlen felépítmény 1 polcos\r\n - önbeálló kerekekkel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('197','197','ECLIPSE II hűtősziget','3321','0','1.65','0','1','','normál hűtő beépített aggregáttal','8418','Hosszúság: 2000 mm\r\nMélység:       2000 mm\r\nMagasság:   1185 mm\r\nAlapszín: szürke\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: +2/+6°C','ECLIPSE II hűtősziget','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventilációs hűtés\r\n - hűtött felépítmény 2 polcos\r\n - önbeálló kerekekkel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('198','198','Mirelit sziget UDD 600 SC','4421','0','1.65','0','0','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2055 mm\r\nMélység:         629 mm\r\nMagasság:     892 mm\r\nTeljesítmény: 440 W\r\nEnergiafogyasztás: 7,2 kW/24h\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -12/-20 °C','Mirelit sziget UDD 600 SC','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - alacsony energiafogyasztás\r\n - kosárkészlet *opcionális','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('199','199','Mini BI-ICE mirelit látványhűtő','3501','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság:   800 mm\r\nMélység:         793mm\r\nMagasság:   1270 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25/-10°C','Mini BI-ICE mirelit látványhűtő','- tolótetős kivitel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('200','200','BI-ICE mirelit látványhűtő','3481','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1258 mm\r\nMélység:         793mm\r\nMagasság:   1270 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25/-10°C','BI-ICE mirelit látványhűtő','- tolótetős kivitel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventilációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('201','201','Impuls 70 N hűtősziget','3401','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 707 mm\r\nMélység:       750 mm\r\nMagasság:   1058 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25/-10°C','Impuls 70 N hűtősziget','- nyitott kivitel\r\n - plexi huzatvédő\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('202','202','Impuls 200 N hűtősziget','3441','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1429 mm\r\nMélység:       1018 mm\r\nMagasság:   1058 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25/-10°C','Impuls 200 N hűtősziget','- nyitott kivitel\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('203','203','OKA 100','244','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1011 mm\r\nMélység:         613 mm\r\nMagasság:     905 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','OKA 100','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - alacsony energiafogyasztás\r\n - kosárkészlet','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('204','204','OKA 120','245','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1230 mm\r\nMélység:         615 mm\r\nMagasság:     905 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','OKA 120','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - alacsony energiafogyasztás\r\n - kosárkészlet','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('205','205','OKA 130','246','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1325 mm\r\nMélység:         615 mm\r\nMagasság:     905 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','OKA 130','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - alacsony energiafogyasztás\r\n - kosárkészlet','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('206','206','OKA 200','6101','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2048 mm\r\nMélység:         615 mm\r\nMagasság:     905 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','OKA 200','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - alacsony energiafogyasztás\r\n - kosárkészlet','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('207','207','Costan Swing 120 BT','5921','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1230 mm\r\nMélység:         725 mm\r\nMagasság:     875 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','Costan Swing 120 BT','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - kocsiütköző\r\n - ajtókeret fűtés\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('208','208','Costan Swing 170 BT','238','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1730 mm\r\nMélység:         725 mm\r\nMagasság:     875 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','Costan Swing 170 BT','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - kocsiütköző\r\n - ajtókeret fűtés\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('209','209','Costan Sound 2100 BT/TN','234','0','1.65','0','1','','Tolótetős mirelit/normál hűtő beépített aggregáttal','8418','Hosszúság: 2095 mm\r\nMélység:         890 mm\r\nMagasság:     801 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 -+2/+4°C','Costan Sound 2100 BT/TN','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - beépített belső világítás\r\n - mechanikus vezérlés\r\n - palást kondenzátor\r\n - kocsiütköző\r\n - ajtókeret fűtés\r\n - ártató sín\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('210','210','Costan Sound 2500 BT/TN','235','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2495 mm\r\nMélység:         890 mm\r\nMagasság:     801 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20- +2/+4 °C','Costan Sound 2500 BT/TN','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - beépített belső világítás\r\n - mechanikus vezérlés\r\n - palást kondenzátor\r\n - kocsiütköző\r\n - ajtókeret fűtés\r\n - ártató sín\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('211','211','Costan Sound MT BT/TN','236','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1845 mm\r\nMélység:         860 mm\r\nMagasság:     801 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 -+2/+4°C','Costan Sound MT BT/TN','- ívelt üveg tolótető\r\n - beépített aggregát\r\n - beépített belső világítás\r\n - mechanikus vezérlés\r\n - palást kondenzátor\r\n - kocsiütköző\r\n - ajtókeret fűtés\r\n - ártató sín\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('212','212','Costan Beluga 140','6281','0','1.65','0','1','','Tolótetős mirelit panorámahűtő beépített aggregáttal','8418','Hosszúság: 1400 mm\r\nMélység:         990 mm\r\nMagasság:     1025/925 mm\r\nTérfogat: 430 l\r\nR 290 környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nNévleges fogyasztás: 415 W','Costan Beluga 140','- betolható üvegtető\r\n - beépített aggregát\r\n - beépített LED világítás\r\n - elektronikus vezérlés\r\n - automata elektromos leolvasztás\r\n - statikus hűtés\r\n - ajtókeret fűtés\r\n - ártató sín\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('213','213','Costan Beluga 210','6301','0','1.65','0','1','','Tolótetős mirelit panorámahűtő beépített aggregáttal','8418','Hosszúság: 2100 mm\r\nMélység:         990 mm\r\nMagasság:     1025/925 mm\r\nTérfogat: 660 l\r\nR 290 környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nNévleges fogyasztás: 610 W','Costan Beluga 210','- betolható üvegtető\r\n - beépített aggregát\r\n - beépített LED világítás\r\n - elektronikus vezérlés\r\n - automata elektromos leolvasztás\r\n - statikus hűtés\r\n - ajtókeret fűtés\r\n - ártató sín\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('214','214','Costan Beluga MT','6321','0','1.65','0','1','','Tolótetős mirelit panorámahűtő beépített aggregáttal','8418','Hosszúság: 2130 mm\r\nMélység:         990 mm\r\nMagasság:     925/825 mm\r\nTérfogat: 510 l\r\nR 290 környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nNévleges fogyasztás: 610 W','Costan Beluga MT','- betolható üvegtető\r\n - beépített aggregát\r\n - beépített LED világítás\r\n - elektronikus vezérlés\r\n - automata elektromos leolvasztás\r\n - statikus hűtés\r\n - ajtókeret fűtés\r\n - ártató sín\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('215','215','Gamma 150 O','335','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1552 mm\r\nMélység:         845 mm\r\nMagasság:     792 mm\r\nKapacitás:      376 liter\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25 °C','Gamma 150 O','- üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - kocsiütköző\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('216','216','Gamma 200 O','336','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2052 mm\r\nMélység:         845 mm\r\nMagasság:     792 mm\r\nKapacitás:      538 liter\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','Gamma 200 O','- üveg tolótető\r\n - beépített aggregát\r\n - mechanikus vezérlés\r\n - kocsiütköző\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('217','217','Gamma 250 S','337','0','1.65','0','1','','Tolótetős mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2552 mm\r\nMélység:         967 mm\r\nMagasság:     792 mm\r\nKapacitás:      752 liter\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -25 °C','Gamma 250 S','- üveg tolótető\r\n - beépített aggregát\r\n - osztott hűtőtér\r\n - mechanikus vezérlés\r\n - kocsiütköző\r\n - csökkentett hőkibocsájtás\r\n - alacsony energiafogyasztás','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('218','218','FIJI LT 150','239','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 1500 mm\r\nMélység:       1005mm\r\nMagasság:     910 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','FIJI LT 150','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('219','219','FIJI LT 200','5781','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2000 mm\r\nMélység:       1005mm\r\nMagasság:     910 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','FIJI LT 200','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('220','220','FIJI LT 250','5801','0','1.65','0','1','','mirelit hűtő beépített aggregáttal','8418','Hosszúság: 2500 mm\r\nMélység:       1005mm\r\nMagasság:     910 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','FIJI LT 250','- nyitott kialakítás\r\n - beépített aggregát\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - panoráma oldalak\r\n - ventillációs hűtés','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('221','221','Costan Symphony 200 ABT','4481','0','1.65','0','1','','mélyhűtő szett beépített aggregáttal','8418','Hosszúság: 2000 mm\r\nMélység:       1020 mm\r\nMagasság:   1995 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C','Costan Symphony 200 ABT','- nyitott kád\r\n - beépített aggregát 2 db\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - üvegajtók a felső szekrényen\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - éjszakai takaró a kádra','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('222','222','Isabel 150 LT/LT','5881','0','1.65','0','1','','mélyhűtő szett beépített aggregáttal','8418','Hosszúság: 1500 mm\r\nMélység:         885 mm\r\nMagasság:   1995 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nHűtőteljesítmény vitrin: 460 W\r\nHűtőteljesítmény kád: 480 W','Isabel 150 LT/LT','- nyitott kád\r\n - beépített aggregát 2 db\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - üvegajtók a felső szekrényen\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('223','223','Isabel 200 LT/LT','243','0','1.65','0','1','','mélyhűtő szett beépített aggregáttal','8418','Hosszúság: 2000 mm\r\nMélység:         885 mm\r\nMagasság:   1995 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nHűtőteljesítmény vitrin: 660 W\r\nHűtőteljesítmény kád: 680 W','Isabel 200 LT/LT','- nyitott kád\r\n - beépített aggregát 2 db\r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - üvegajtók a felső szekrényen\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('224','224','COMBO 375','6725','0','1.8','0','1','','Mélyhűtő szett telepített aggregáttal','8418','Hosszúság: 3 750mm\r\nMélység:       1 275 mm\r\nMagasság:   2 050 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -20/-22 °C\r\nHűtőteljesítmény igény:   1 785 W (kád)\r\nHűtőteljesítmény igény:   2 040 W (vitrin)','COMBO 375','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok\r\n - nyitott alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('225','225','Flamingo LT 2D','1301','0','1.65','0','1','','Üvegajtós mélyhűtő telepített aggregáttal','8418','Hosszúság: 1575 mm\r\nMélység:         930 mm\r\nMagasság:   2010 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nHűtőteljesítmény igény: 1550 W\r\nNyílóajtók száma: 2 db','Flamingó LT 2D','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('226','226','Flamingo LT 3D','1321','0','1.65','0','1','','Üvegajtós mélyhűtő telepített aggregáttal','8418','Hosszúság: 2365 mm\r\nMélység:         930 mm\r\nMagasság:   2010 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nHűtőteljesítmény igény: 2250 W\r\nNyílóajtók száma: 3db','Flamingó LT 3D','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('227','227','Flamingo LT 4D','1341','0','1.65','0','1','','Üvegajtós mélyhűtő telepített aggregáttal','8418','Hosszúság: 3150 mm\r\nMélység:         930 mm\r\nMagasság:   2010 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nHűtőteljesítmény igény: 3000 W\r\nNyílóajtók száma: 4db','Flamingó LT 4D','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('228','228','Flamingo LT 5D','1361','0','1.65','0','1','','Üvegajtós mélyhűtő telepített aggregáttal','8418','Hosszúság: 3940 mm\r\nMélység:         930 mm\r\nMagasság:   2010 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -18/-20 °C\r\nHűtőteljesítmény igény: 3750 W\r\nNyílóajtók száma: 5db','Flamingó LT 5D','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('229','229','Cayman 2 20 LG 300 - 188','2961','0','1.65','0','1','Karos világítás','Mélyhűtő szett telepített aggregáttal','8418','Hosszúság: 1 875mm\r\nMélység:       1 178 mm\r\nMagasság:   2100 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -22/-24 °C\r\nHűtőteljesítmény igény: 1 365 W \r\nElpárolgási hőmérséklet: -35 °C','Cayman 2 20 LG 300 - 188','- nyíló üvegajtók 3 db\r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok\r\n - üveg tolótetős alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('230','230','Cayman 2 20 LG 300 - 250','2941','0','1.65','0','1','Karos világítás','Mélyhűtő szett telepített aggregáttal','8418','Hosszúság: 2 500mm\r\nMélység:       1 178 mm\r\nMagasság:   2100 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -22/-24 °C\r\nHűtőteljesítmény igény: 1 815 W \r\nElpárolgási hőmérséklet: -35 °C','Cayman 2 20 LG 300 - 250','- nyíló üvegajtók 4 db\r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok\r\n - üveg tolótetős alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('231','231','Cayman 2 20 LG 300 - 375','2921','0','1.65','0','1','Karos világítás','Mélyhűtő szett telepített aggregáttal','8418','Hosszúság: 3 750mm\r\nMélység:       1 178 mm\r\nMagasság:   2100 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -22/-24 °C\r\nHűtőteljesítmény igény: 2 720 W \r\nElpárolgási hőmérséklet: -35 °C','Cayman 2 20 LG 300 - 375','- nyíló üvegajtók 6 db\r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok\r\n - üveg tolótetős alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('232','232','MIX 3,75 TT','2761','0','1.65','0','1','','Mélyhűtő szett telepített aggregáttal','8418','Hosszúság: 3 750 mm\r\nMélység:       1 140 mm\r\nMagasság:   2 050 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -20/-22 °C\r\nHűtőteljesítmény igény:   3 900 W','MIX 3,75 TT','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok\r\n - alsó kád üveg tolótetővel','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('233','233','MIX 3,75','2721','0','1.65','0','1','','Mélyhűtő szett telepített aggregáttal','8418','Hosszúság: 3 750mm\r\nMélység:       1 105 mm\r\nMagasság:   1985 mm\r\nR 404 A környezetbarát hűtőközeg\r\nHőfok: -20/-22 °C\r\nHűtőteljesítmény igény: 1 800 W (kád)\r\nHűtőteljesítmény igény: 2 025 W (vitrin)','MIX 3,75','- nyíló üvegajtók \r\n - telepített aggregát \r\n - elektronikus vezérlés\r\n - kocsiütköző\r\n - ventilációs hűtés\r\n - beépített világítás\r\n - fűtött ajtókeret\r\n - rácspolcok\r\n - nyitott alsó kád','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('234','234','Sokkoló hűtő 3P','961','0','1.65','0','0','','Rozsdamentes sokkoló hűtő','8418','Hosszúság: 750 mm\r\nMélység:       740 mm\r\nMagasság  720/750 mm\r\nHűtőkapacitás: +70°C>  +3°C max. 14 kg    90 perc\r\nHűtőkapacitás: +70°C> -18°C max. 11 kg 240 perc\r\nHűtőteljesítmény: 1150 W\r\nHűtőközeg: R404A/R507','AT03ISO','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - 3xGN 1/1 belső méret\r\n - 3db 60x40x7 cm tálca','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('235','235','Sokkoló hűtő 5P','981','0','1.65','0','0','','Rozsdamentes sokkoló hűtő','8418','Hosszúság: 750 mm\r\nMélység:       740 mm\r\nMagasság  850/880 mm\r\nHűtőkapacitás: +70°C>  +3°C max. 20 kg    90 perc\r\nHűtőkapacitás: +70°C> -18°C max. 15 kg 240 perc\r\nHűtőteljesítmény: 1424 W\r\nHűtőközeg: R404A/R507','AT05ISO','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - 5xGN 1/1 belső méret\r\n - 5 db 60x40x7 cm tálca','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('236','236','Sokkoló hűtő 7P','1001','0','1.65','0','0','','Rozsdamentes sokkoló hűtő','8418','Hosszúság: 750 mm\r\nMélység:       740 mm\r\nMagasság  1260/1290 mm\r\nHűtőkapacitás: +70°C>  +3°C max. 25 kg    90 perc\r\nHűtőkapacitás: +70°C> -18°C max. 20 kg 240 perc\r\nHűtőteljesítmény: 1490 W\r\nHűtőközeg: R404A/R507','AT07ISO','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - 7xGN 1/1 belső méret\r\n - 7 db 60x40x10,5 cm tálca','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('237','237','Sokkoló hűtő 10P','1021','0','1.65','0','0','','Rozsdamentes sokkoló hűtő','8418','Hosszúság: 750 mm\r\nMélység:       740 mm\r\nMagasság  1260/1290 mm\r\nHűtőkapacitás: +70°C>  +3°C max. 25 kg    90 perc\r\nHűtőkapacitás: +70°C> -18°C max. 20 kg 240 perc\r\nHűtőteljesítmény: 1490 W\r\nHűtőközeg: R404A/R507','AT10ISO','. rozsdamentes külső és belső borítás\r\n - elektronikus vezérlés\r\n - beépített aggregát\r\n - rozsdamentes ajtó\r\n - állítható lábak\r\n - monoblock technológia\r\n - CFC mentes szigetelés\r\n - 10xGN 1/1 belső méret\r\n - 10 db 60x40x7 cm tálca','','','43 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('238','238','Hűtőaggregát normál 4','2141','0','1.65','0','1','','Hűtőaggregát egység','8414','Hűtőközeg: R 404 A\r\nElpárolgási hőm .-10 °C','Hűtőaggregát normál 4','- légkondenzátorral\r\n - vezérlő szekrénnyel\r\n - nyitott kivitel\r\n - rezgés csillapított egység\r\n - + 35 °C környezeti hőm\r\n - 60 % páratartalom mellett','','','Kérem töltse ki az adatbekérőt!');
INSERT INTO `bos_termekleiras_hu` VALUES ('239','239','Hűtőaggregát mirelit 4','2201','0','1.65','0','1','','Hűtőaggregát egység','8414','Hűtőközeg: R 404 A\r\nElpárolgási hőm .-32 °C','Hűtőaggregát mirelit 4','- légkondenzátorral\r\n - vezérlő szekrénnyel\r\n - nyitott kivitel\r\n - rezgés csillapított egység\r\n - + 35 °C környezeti hőm\r\n - 60 % páratartalom mellett','','','Kérem töltse ki az adatbekérőt!');
INSERT INTO `bos_termekleiras_hu` VALUES ('240','240','ESAS 1100 BT-','282','0','1.65','0','1','','Kompakt aggregát egység','8414','hosszúság. 1150 mm\r\nmélység:         450 mm\r\nmagasság:  1397 mm\r\nsúly:               162 kg\r\nhűtő teljesítmény -35° : 7,1 kW\r\nvillamos teljesítmény:    6,8kW\r\nhűtőközeg: R 404 A','ESAS 1100 BT-','- 1 x ZF33K4E hermetikus kompresszor\r\n - légkondenzátorral\r\n - vezérlő szekrénnyel\r\n - kültéri dobozolás\r\n - hangszigetelt kivitel\r\n - rezgés csillapított egység','','','35 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('241','241','Csőszerelési anyagok, munkadíj, szállítás','781','0','1.65','0','1','','Fitingek anyagok munkadíj','8414','','Csőszerelési anyagok','- R 404A hűtőközeg\r\n - rézcsövek\r\n - hőszigetelő anyagok\r\n - elektromos kábelek\r\n - fitingek\r\n - függesztő és stabilizáló elemek\r\n - elektronikus hűtési vezérlő elemek','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('242','242','Egyedi csoportaggregát  BT','1901','0','1.65','0','1','','Mirelit csoportaggregát','8414','Hűtőközeg: R 404 A\r\nElpárolgási hőmérséklet: -32 °C','','- vezérlő szekrénnyel\r\n - elektronikus vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('243','243','Egyedi csoportaggregát  BT/TN','4741','0','1.65','0','1','','Csoportaggregát','8414','Hűtőközeg: R 404 A\r\nElpárolgási hőmérséklet: -10 °C /TN\r\nElpárolgási hőmérséklet: -32 °C /BT','','- vezérlő szekrénnyel\r\n - elektronikus vezérléssel\r\n - rezgés csillapított egység\r\n - maszív hegesztett keret\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('244','244','Csoportaggregát EpltaGloo  TN','1121','0','1.65','0','1','','Normál csoportaggregát','8414','hosszúság. 1995 mm\r\nmélység:         960 mm\r\nmagasság:  1380 mm\r\nsúly:               771 kg\r\nhűtő teljesítmény -10° :  47,49 kW\r\nvillamos teljesítmény:    22,74 kW\r\nHűtőközeg: R 404 A','Csoportaggregát EpltaGloo  TN','- 3 db fél hermetikus Bitzer kompresszor\r\n - vezérlő szekrénnyel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('245','245','Csoportaggregát  20kW TN','1141','0','1.65','0','1','','Normál csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               530 kg\r\nhűtő teljesítmény -10° :  20 kW\r\nvillamos teljesítmény:       8 kW\r\nHűtőközeg: R 404 A','ENS 300 3035 SC TN','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS26K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('246','246','Csoportaggregát  23kW TN','1161','0','1.65','0','1','','Normál csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               530 kg\r\nhűtő teljesítmény -10° :  23 kW\r\nvillamos teljesítmény:       8 kW\r\nHűtőközeg: R 404 A','ENS 300 3040 SC TN','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS30K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('247','247','Csoportaggregát  29kW TN','1181','0','1.65','0','1','','Normál csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               540 kg\r\nhűtő teljesítmény -10° :  29 kW\r\nvillamos teljesítmény:     11 kW\r\nHűtőközeg: R 404 A','ENS 300 3050 SC TN','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS38K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('248','248','Csoportaggregát  34kW TN','1201','0','1.65','0','1','','Normál csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               560 kg\r\nhűtő teljesítmény -10° :  34 kW\r\nvillamos teljesítmény:     13 kW\r\nHűtőközeg: R 404 A','ENS 300 3060 SC TN','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS45K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('249','249','Csoportaggregát  8kW BT','1221','0','1.65','0','1','','Mirelit csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               530 kg\r\nhűtő teljesítmény -35° :  8 kW\r\nvillamos teljesítmény:    7 kW\r\nHűtőközeg: R 404 A','ENS 300 3035 SC BT','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS11K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('250','250','Csoportaggregát  9 kW BT','1241','0','1.65','0','1','','Mirelit csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               530 kg\r\nhűtő teljesítmény -35° :  9 kW\r\nvillamos teljesítmény:    8 kW\r\nHűtőközeg: R 404 A','ENS 300 3040 SC BT','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS13K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('251','251','Csoportaggregát  11 kW BT','1261','0','1.65','0','1','','Mirelit csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               540 kg\r\nhűtő teljesítmény -35° :  11 kW\r\nvillamos teljesítmény:     9 kW\r\nHűtőközeg: R 404 A','ENS 300 3050 SC BT','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS15K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('252','252','Csoportaggregát  13 kW BT','1281','0','1.65','0','1','','Mirelit csoportaggregát','8414','hosszúság. 1720 mm\r\nmélység:         800 mm\r\nmagasság:  1380 mm\r\nsúly:               560 kg\r\nhűtő teljesítmény -35° :  13 kW\r\nvillamos teljesítmény:     11kW\r\nHűtőközeg: R 404 A','ENS 300 3060 SC BT','- 3 db hermetikus Copeland Schroll kompresszor\r\n - vezérlő szekrénnyel\r\n - Danfoss vezérléssel\r\n - rezgés csillapított egység\r\n - szegecskötések a rezgéscsillapítás érdekében\r\n - hangszigetelő dobozolás\r\n - 3xZS18K4E\r\n - kondenzátor nem tartozék','','','45 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('253','253','XWEB felügyeleti rendszer','4761','0','1.65','0','1','','Felügyeleti rendszer','8438','230/1/50','XWEB felügyeleti rendszer','- 17mérési pont\r\n - 24 órás működés\r\n - adatok kiértékelése grafikus formában\r\n - távoli elérés\r\n - távoli beavatkozás lehetősége\r\n - riasztások beállítása\r\n - HACCP adat tárolás\r\n - hőfok, ajtónyitás, olvasztás rögzítése','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('254','254','SFN-030 + Tetőblokk','221','0','1.65','0','1','','Kompakt hűtőegység 7 m3 +','8414','Kamratérfogat: 7 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','SFN-030 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('255','255','SFN-050 + Tetőblokk','222','0','1.65','0','1','','Kompakt hűtőegység 10 m3 +','8414','Kamratérfogat: 10 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','SFN-050','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('256','256','SFN-060 + Tetőblokk','223','0','1.65','0','1','','Kompakt hűtőegység 12 m3 +','8414','Kamratérfogat: 12 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','SFN-060','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('257','257','SFN-075 + Tetőblokk','224','0','1.65','0','1','','Kompakt hűtőegység 16 m3 +','8414','Kamratérfogat: 16 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','SFN-075','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('258','258','SFN-100 + Tetőblokk','225','0','1.65','0','1','','Kompakt hűtőegység 18 m3 +','8414','Kamratérfogat: 18 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','SFN-100','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('259','259','SFN-120 + Tetőblokk','227','0','1.65','0','1','','Kompakt hűtőegység 20 m3 +','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','SFN-120','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('260','260','SFN-122 + Tetőblokk','226','0','1.65','0','1','','Kompakt hűtőegység 20 m3 +','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','SFN-122','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('261','261','SFN-150 + Tetőblokk','228','0','1.65','0','1','','Kompakt hűtőegység 40 m3 +','8414','Kamratérfogat: 40 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','SFN-150','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('262','262','SFN-200 + Tetőblokk','229','0','1.65','0','1','','Kompakt hűtőegység 60 m3 +','8414','Kamratérfogat: 60 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','SFN-200','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('263','263','SFK-120 - Tetőblokk','304','0','1.65','0','1','','Kompakt hűtőegység 5 m3 -','8414','Kamratérfogat: 5 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 220 V 1F 16A','SFK 120 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('264','264','SFK-170 - Tetőblokk','305','0','1.65','0','1','','Kompakt hűtőegység 7 m3 -','8414','Kamratérfogat: 7 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 220 V 1F 16A','SFK 170 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('265','265','SFK-201 - Tetőblokk','306','0','1.65','0','1','','Kompakt hűtőegység 10 m3 -','8414','Kamratérfogat: 10 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 220 V 1F 16A','SFK 201 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('266','266','SFK-202 - Tetőblokk','307','0','1.65','0','1','','Kompakt hűtőegység 13 m3 -','8414','Kamratérfogat: 13 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','SFK 202 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('267','267','SFK-203 - Tetőblokk','308','0','1.65','0','1','','Kompakt hűtőegység 20 m3 -','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','SFK 203 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('268','268','SFK-300 - Tetőblokk','309','0','1.65','0','1','','Kompakt hűtőegység 22 m3 -','8414','Kamratérfogat: 22 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','SFK 300 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('269','269','SFK-400 - Tetőblokk','311','0','1.65','0','1','','Kompakt hűtőegység 35 m3 -','8414','Kamratérfogat: 35 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','SFK 400 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - tetőbe építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 32 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('270','270','ACN-030 + Hűtőblokk','319','0','1.65','0','1','','Kompakt hűtőegység 7 m3 +','8414','Kamratérfogat: 7 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','ACN-030 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('271','271','ACN-050 + Hűtőblokk','320','0','1.65','0','1','','Kompakt hűtőegység 10 m3 +','8414','Kamratérfogat: 10 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','ACN-050 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('272','272','ACN-060 + Hűtőblokk','321','0','1.65','0','1','','Kompakt hűtőegység 12 m3 +','8414','Kamratérfogat: 12 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','ACN-060 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('273','273','ACN-075 + Hűtőblokk','322','0','1.65','0','1','','Kompakt hűtőegység 16 m3 +','8414','Kamratérfogat: 16 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','ACN-075 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('274','274','ACN-100 + Hűtőblokk','323','0','1.65','0','1','','Kompakt hűtőegység 18 m3 +','8414','Kamratérfogat: 18 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','ACN-100 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('275','275','ACN-122 + Hűtőblokk','324','0','1.65','0','1','','Kompakt hűtőegység 20 m3 +','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','ACN-122 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('276','276','ACN-120 + Hűtőblokk','325','0','1.65','0','1','','Kompakt hűtőegység 20 m3 +','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','ACN-120 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('277','277','ACN-150 + Hűtőblokk','326','0','1.65','0','1','','Kompakt hűtőegység 40 m3 +','8414','Kamratérfogat: 40 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','ACN-150 +','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('278','278','ACK-120 - Hűtőblokk','328','0','1.65','0','1','','Kompakt hűtőegység 5m3 -','8414','Kamratérfogat: 5 m3\r\nKamra hőmérséklet: -18/ -20 °C\r\nFeszültség igény: 220 V 1F 16A','ACK-120 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('279','279','ACK-170 - Hűtőblokk','329','0','1.65','0','1','','Kompakt hűtőegység 7 m3 -','8414','Kamratérfogat: 7 m3\r\nKamra hőmérséklet: -18/ -20 °C\r\nFeszültség igény: 220 V 1F 16A','ACK-170 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('280','280','ACK-201 - Hűtőblokk','330','0','1.65','0','1','','Kompakt hűtőegység 10 m3 -','8414','Kamratérfogat: 10 m3\r\nKamra hőmérséklet: -18/ -20 °C\r\nFeszültség igény: 220 V 1F 16A','ACK-201 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('281','281','ACK-202 - Hűtőblokk','331','0','1.65','0','1','','Kompakt hűtőegység 13 m3 -','8414','Kamratérfogat: 13 m3\r\nKamra hőmérséklet: -18/ -20 °C\r\nFeszültség igény: 400 V 3F 16A','ACK-202 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('282','282','ACK-203 - Hűtőblokk','332','0','1.65','0','1','','Kompakt hűtőegység 20 m3 -','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: -18/ -20 °C\r\nFeszültség igény: 400 V 3F 16A','ACK-203 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('283','283','ACK-300 - Hűtőblokk','333','0','1.65','0','1','','Kompakt hűtőegység 22 m3 -','8414','Kamratérfogat: 22 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','ACK-300 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('284','284','ACK-400 - Hűtőblokk','334','0','1.65','0','1','','Kompakt hűtőegység 35 m3 -','8414','Kamratérfogat: 35 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','ACK-400 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - oldalfalba építhető\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 35 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('285','285','CSN-050 + Split hűtőegység','143','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 10 m3 +','8414','Kamratérfogat: 10 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','CSN-050','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('286','286','CSN-060 + Split hűtőegység','214','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 12 m3 +','8414','Kamratérfogat: 12 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','CSN-060','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('287','287','CSN-075 + Split hűtőegység','215','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 14 m3 +','8414','Kamratérfogat: 14 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','CSN-075','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('288','288','CSN-100 + Split hűtőegység','216','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 20 m3 +','8414','Kamratérfogat: 20 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','CSN-100','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('289','289','CSN-122 + Split hűtőegység','217','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 24 m3 +','8414','Kamratérfogat: 24 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 220 V 1F 16A','CSN-122','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('290','290','CSN-120 + Split hűtőegység','218','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 24 m3 +','8414','Kamratérfogat: 24 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','CSN-120','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('291','291','CSN-150 + Split hűtőegység','219','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 45 m3 +','8414','Kamratérfogat: 45 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','CSN-150','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('292','292','CSN-200 + Split hűtőegység','220','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 58 m3 +','8414','Kamratérfogat: 58 m3\r\nKamra hőmérséklet: +2/+4 °C\r\nFeszültség igény: 400 V 3F 16A','CSN-200','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 80 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('293','293','CSK-120 - Split hűtőegység','312','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 7 m3 -','8414','Kamratérfogat: 7 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 220 V 1F 16A','CSK-120 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('294','294','CSK-170 - Split hűtőegység','313','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 9 m3 -','8414','Kamratérfogat: 7 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 220 V 1F 16A','CSK-170 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('295','295','CSK-201 - Split hűtőegység','314','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 12 m3 -','8414','Kamratérfogat: 12 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 220 V 1F 16A','CSK-201 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('296','296','CSK-202 - Split hűtőegység','315','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 15 m3 -','8414','Kamratérfogat: 15 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','CSK-202 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('297','297','CSK-203 - Split hűtőegység','316','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 22 m3 -','8414','Kamratérfogat: 22 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','CSK-203 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('298','298','CSK-300 - Split hűtőegység','317','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 24 m3 -','8414','Kamratérfogat: 24 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','CSK-300 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('299','299','CSK-400 - Split hűtőegység','318','0','1.65','0','1','10 fm csőkészlet\r\npresztosztát\r\nkarterfűtés','Kompakt hűtőegység 36 m3 -','8414','Kamratérfogat: 36 m3\r\nKamra hőmérséklet: -18/-20 °C\r\nFeszültség igény: 400 V 3F 16A','CSK-400 -','- kompakt egység\r\n - kültéri dobozolás\r\n - leolvasztó fűtés\r\n - elektronikus vezérlés\r\n - digitális hőfokkijelzés\r\n - 5 fm csőkészlet\r\n - R 404A hűtőközeg\r\nA megadott m3 adatok hőszigetelt helységre vonatkoznak, maximum 2°C  eltérésű betárolási hőmérsékleten. Ajánlott szigetelés 100 mm-es szendvicspanel.','','','25 °C környezeti hőmérséklet\r\n60% maximum páratartalom\r\n- 10 °C elpárolgási hőmérsékleten');
INSERT INTO `bos_termekleiras_hu` VALUES ('300','300','Mirelit hűtőkamra3','2341','0','1.65','0','1','','Hűtőkamra egyedi kialakítással mirelit','8418','Egyedi igények alapján','Mirelit hűtőkamra3','- 100 mm-es acélfegyverzetes  szendvicspanel\r\n - szivacs illesztés\r\n - nút-féder panelkötés\r\n - MIV olasz ajtókkal\r\n - ajtók keretfűtéssel\r\n - készre szerelve\r\n - műanyag egészségügyi lábazattal\r\n - beépített világítás\r\n - csúszásmentes szigetelt aljzat\r\n - nyomáskompenzációs szelep','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('301','301','MiniCelle TN','265','0','1.65','0','1','','Hűtőkamra','8418','Hosszúság: 1000 mm\r\nSzélesség:   1000 mm\r\nMagasság:    2120 mm\r\nŰrtartalom:    1315 liter\r\nHőfok tartomány: 0/+8 °C','MiniCelle TN','- 60 mm-es acélfegyverzetes szendvicspanel\r\n - oldható békazáras panelkötés\r\n - szigetelt rozsdamentes aljzat\r\n - 3 sor rozsdamentes polc vagy húsakasztó rúd\r\n - ventilációs hűtés\r\n - automata leolvasztás\r\n - tetőblokk aggregát\r\n - 700x1680 mm-es ajtónyílás\r\n - lapra szerelt egységcsomagban\r\n - egyszerűen összeépíthető','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('302','302','MiniCelle BT','266','0','1.65','0','1','','Hűtőkamra','8418','Hosszúság: 1000 mm\r\nSzélesség:   1000 mm\r\nMagasság:    2120 mm\r\nŰrtartalom:    1315 liter\r\nHőfok tartomány: -18/-20 °C','MiniCelle BT','- 60 mm-es acélfegyverzetes szendvicspanel\r\n - oldható békazáras panelkötés\r\n - szigetelt rozsdamentes aljzat\r\n - 3 sor rozsdamentes polc vagy húsakasztó rúd\r\n - ventilációs hűtés\r\n - automata leolvasztás\r\n - tetőblokk aggregát\r\n - 700x1680 mm-es ajtónyílás\r\n - lapra szerelt egységcsomagban\r\n - egyszerűen összeépíthető','','','25 °C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('303','303','Szendvicspanel ISOBOX 1000','5141','0','1.8','0','1','','Szendvicspanel ISOBOX 1000','9403','szélesség: 1000 mm\r\nvastagság: 80 mm\r\nsűrűség 40 kg/m3\r\nszigetelő képesség: 0,020 w/mK\r\ntűzállóság: EW 60 (EN 13501-2)\r\ntűzreakció: C-S3-D0','Szendvicspanel ISOBOX 1000','- acélfegyverzetes\r\n - cink+porszórt felület\r\n - nút-féder panelkötés\r\n - szivacsbetét az illesztésnél\r\n - mikró bordázott\r\n - 0< °C  hőmérséklethez\r\n - RAL 9010','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('304','304','Szendvicspanel ISOFRIGO 1000','5161','0','1.8','0','1','','Szendvicspanel ISOFRIGO 1000','9403','szélesség: 1000 mm\r\nvastagság: 100 mm\r\nsűrűség 40 kg/m3\r\nszigetelő képesség: 0,022 w/mK\r\ntűzállóság: EW 60 (EN 13501-2)\r\ntűzreakció: C-S3-D0','Szendvicspanel ISOFRIGO 1000','- acélfegyverzetes\r\n - cink+porszórt felület\r\n - nút-féder panelkötés\r\n - szivacsbetét az illesztésnél\r\n - mikró bordázott\r\n - mirelit  hőmérséklethez\r\n - RAL 9010','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('305','305','Hűtőkamraajtó 180 N 70','4841','0','1.65','0','1','','Hűtőkamraajtó normál','9403','ajtólap szélesség: 700 mm\r\najtólap magasság: 1900 mm\r\najtólap vastagság: 70 mm\r\nkeret szélesség: +200 mm\r\nkeret magasság: + 150 mm\r\ntömörsége: 38/40 Kg/cu m','Hűtőkamraajtó 180 N 70','- zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('306','306','Hűtőkamraajtó 180 N 90','4821','0','1.65','0','1','','Hűtőkamraajtó normál','9403','ajtólap szélesség: 900 mm\r\najtólap magasság: 1900 mm\r\najtólap vastagság: 70 mm\r\nkeret szélesség: +200 mm\r\nkeret magasság: + 150 mm\r\ntömörsége: 38/40 Kg/cu m','Hűtőkamraajtó 180 N 90','- zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('307','307','Mirelit hűtőkamraajtó 240 BT 90','4881','0','1.65','0','1','','Mirelit hűtőkamraajtó','9403','ajtólap szélesség: 900 mm\r\najtólap magasság: 1900 mm\r\najtólap vastagság: 90 mm\r\nkeret szélesség: +200 mm\r\nkeret magasság: + 150 mm\r\ntömörsége: 38/40 Kg/cu m','Mirelit hűtőkamraajtó 240 BT 90','- zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - elektromos keretfűtés','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('308','308','Szigetelt ajtó Ufficio 80','4861','0','1.65','0','1','','Szigetelt ajtó normál','9403','ajtólap szélesség: 800 mm\r\najtólap magasság: 2000 mm\r\najtólap vastagság: 40 mm\r\nkeret szélesség: +130 mm\r\nkeret magasság: + 65 mm\r\ntömörsége: 38/40 Kg/cu m','Szigetelt ajtó Ufficio 80','- zárható kivitel\r\n - fém zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('309','309','Szigetelt lengőajtó VA e VIENI 1400','4921','0','1.65','0','1','','Szigetelt lengőajtó normál','9403','ajtólap szélesség: 2x700 mm\r\najtólap magasság: 2000 mm\r\najtólap vastagság: 40 mm\r\nkeret szélesség: +134 mm\r\nkeret magasság: + 67 mm\r\ntömörsége: 38/40 Kg/cu m','Szigetelt lengőajtó VA e VIENI 1400','- lengőzsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - ablak 350 mm\r\n - 300 mm-es RM lábazati burkolat','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('310','310','Hűtőkamra tolóajtó 880 P 1400','4941','0','1.65','0','1','','Hűtőkamra tolóajtó normál','9403','ajtólap szélesség:  1400 mm\r\najtólap magasság: 2000 mm\r\najtólap vastagság: 70 mm\r\nkeret szélesség: +280 mm\r\nkeret magasság: + 160 mm\r\nbeépítési méret: ajtólap * 2+520 mm\r\ntömörsége: 38/40 Kg/cu m','Hűtőkamra tolóajtó 880 P 1400','- nem zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - acél csúszka','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('311','311','Hűtőkamra tolóajtó 880 P 2000','4961','0','1.65','0','1','','Hűtőkamra tolóajtó normál','9403','ajtólap szélesség:  2000 mm\r\najtólap magasság: 2500 mm\r\najtólap vastagság: 70 mm\r\nkeret szélesség: +280 mm\r\nkeret magasság: + 160 mm\r\nbeépítési méret: ajtólap * 2+520 mm\r\ntömörsége: 38/40 Kg/cu m','Hűtőkamra tolóajtó 880 P 2000','- nem zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - acél cúszka','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('312','312','Hűtőkamra tolóajtó 880 BT 1400','4981','0','1.65','0','1','','Hűtőkamra tolóajtó normál','9403','ajtólap szélesség:  1400 mm\r\najtólap magasság: 2000 mm\r\najtólap vastagság: 120 mm\r\nkeret szélesség: +280 mm\r\nkeret magasság: + 160 mm\r\nbeépítési méret: ajtólap * 2+520 mm\r\ntömörsége: 38/40 Kg/cu m','Hűtőkamra tolóajtó 880 BT 1400','- nem zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - acél csúszka','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('313','313','Hűtőkamra tolóajtó 880 BT 2000','5001','0','1.65','0','1','','Hűtőkamra tolóajtó normál','9403','ajtólap szélesség:  2000 mm\r\najtólap magasság: 2500 mm\r\najtólap vastagság: 120 mm\r\nkeret szélesség: +280 mm\r\nkeret magasság: + 160 mm\r\nbeépítési méret: ajtólap * 2+520 mm\r\ntömörsége: 38/40 Kg/cu m','Hűtőkamra tolóajtó 880 BT 2000','- nem zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - acél cúszka','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('314','314','Légzáró tolóajtó 900 ATC','5021','0','1.65','0','1','','Légzáró tolóajtó normál','9403','ajtólap szélesség:  2000 mm\r\najtólap magasság: 2500 mm\r\najtólap vastagság: 90 mm\r\nkeret szélesség: +280 mm\r\nkeret magasság: + 160 mm\r\nbeépítési méret: ajtólap * 2+520 mm\r\ntömörsége: 38/40 Kg/cu m','Légzáró tolóajtó 900 ATC','- nem zárható kivitel\r\n - foszforeszkáló belső vésznyitó\r\n - kiemelő zsanér\r\n - acélfegyverzetes ajtólap\r\n - cinkelt + porszórt felületkezelés\r\n - CFC mentes szigetelés\r\n - EPDM szigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - műanyag ajtótokprofil\r\n - acél csúszka\r\n - betekintő ablakkal','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('315','315','Nyíló üvegajtó SK22 1P','5041','0','1.65','0','1','','Nyíló üvegajtó SK1P','9403','ajtólap szélesség: 660 mm/db\r\najtólap magasság: 1806 mm\r\nbeépítési méret: sz: 754 mm\r\nbeépítési méret: m: 1900 mm','Nyíló üvegajtó SK22 1P','- nyíló kivitel\r\n - szigetelt kétrétegű üveg\r\n - keretbe épített neon világítás\r\n - alumínium tok\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - alumínium húzófogantyú\r\n - szendvicspanelbe építhető','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('316','316','Nyíló üvegajtó SK22 2P','5061','0','1.65','0','1','','Nyíló üvegajtó SK2P','9403','ajtólap szélesség: 660 mm/db\r\najtólap magasság: 1806 mm\r\nbeépítési méret: sz: 1428 mm\r\nbeépítési méret: m: 1900 mm','Nyíló üvegajtó SK22 2P','- nyíló kivitel\r\n - szigetelt kétrétegű üveg\r\n - keretbe épített neon világítás\r\n - alumínium tok\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - alumínium húzófogantyú\r\n - szendvicspanelbe építhető','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('317','317','Nyíló üvegajtó SK22 3P','5081','0','1.65','0','1','','Nyíló üvegajtó SK3P','9403','ajtólap szélesség: 660 mm/db\r\najtólap magasság: 1806 mm\r\nbeépítési méret: sz: 2120mm\r\nbeépítési méret: m: 1900 mm','Nyíló üvegajtó SK22 3P','- nyíló kivitel\r\n - szigetelt kétrétegű üveg\r\n - keretbe épített neon világítás\r\n - alumínium tok\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - alumínium húzófogantyú\r\n - szendvicspanelbe építhető','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('318','318','Nyíló üvegajtó SK22 4P','5101','0','1.65','0','1','','Nyíló üvegajtó SK4P','9403','ajtólap szélesség: 660 mm/db\r\najtólap magasság: 1806 mm\r\nbeépítési méret: sz: 2778 mm\r\nbeépítési méret: m: 1900 mm','Nyíló üvegajtó SK22 4P','- nyíló kivitel\r\n - szigetelt kétrétegű üveg\r\n - keretbe épített neon világítás\r\n - alumínium tok\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - alumínium húzófogantyú\r\n - szendvicspanelbe építhető','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('319','319','Nyíló üvegajtó SK22 5P','5121','0','1.65','0','1','','Nyíló üvegajtó SK5P','9403','ajtólap szélesség: 660 mm/db\r\najtólap magasság: 1806 mm\r\nbeépítési méret: sz: 3452 mm\r\nbeépítési méret: m: 1900 mm','Nyíló üvegajtó SK22 5P','- nyíló kivitel\r\n - szigetelt kétrétegű üveg\r\n - keretbe épített neon világítás\r\n - alumínium tok\r\n - CFC mentes szigetelés\r\n - gumiszigetelés az ajtólapon\r\n - alumínium ajtókeret\r\n - alumínium húzófogantyú\r\n - szendvicspanelbe építhető','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('320','320','Áruházi gondola 4','2081','0','1.65','0','1','','Modul bolti fém polcrendszer','9403','Egyedi igények alapján','Áruházi gondola 4','- fém szerkezet\r\n - porszórt felületkezelés\r\n - állítható lábak\r\n - megerősített polclapok\r\n - dönthető polcok\r\n - műanyag ártartósín\r\n - alapszín RAL 9012','','','Kérjük töltse ki az igényfelmérőt');
INSERT INTO `bos_termekleiras_hu` VALUES ('321','321','Hűtetlen bútorok szerelési díja','6121','0','1.65','0','1','','','29.23.91','','Hűtetlen bútorok szerelési díja','- polcok szerelése\r\n - pozicionálás\r\n - pénztárpoltok szerelése\r\n - vevő irányítók kialakítása\r\n - rezsi anyagok\r\n - függesztő és stabilizáló elemek\r\n - csomagoló anyagok hulladékának\r\n   elszállítása megrendelő feladata','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('322','322','Gyorskassza','6001','0','1.65','0','1','- kosár csúsztató\r\n - zárható fiók','Pénztárpult statikus','9403','Hosszúság: 1500 mm\r\nMélység:          600 mm\r\nMagasság:      770/900/1200 mm','Gyorskassza','- bútorlap szerkezet\r\n - rozsdamentes munkapult\r\n - sík elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - plexi előlap\r\n - resopal pénztárasztallap','','','Alap szín:\r\n               - bükk');
INSERT INTO `bos_termekleiras_hu` VALUES ('323','323','Som 150 S pénztárpult','160','0','2.3','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult statikus','9403','Hosszúság: 1500 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm','Som 150 S','- fém kivitel\r\n - rozsdamentes munkapult\r\n - sík elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('324','324','Som 190 S pénztárpult','3001','0','2','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult statikus','9403','Hosszúság: 1900 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm','Som 190 S','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('325','325','Som 190 SD pénztárpult','137','0','2','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult statikus','9403','Hosszúság: 1900 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm','Som 190 SD','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('326','326','Som 210F pénztárpult','161','0','2','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 2100 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:     850 mm','Som 210F','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - lábpedál működéssel','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('327','327','Som 230F pénztárpult','162','0','2','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 2300 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:   1050 mm','Som 230F','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - lábpedál működés','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('328','328','Som 230FS pénztárpult','164','0','2','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 2300 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:   1050 mm','Som 230FS','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - lábpedál működés','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('329','329','Som 420FTD pénztárpult','163','0','2','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 4200 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:   1050 mm','Som 420FTD','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - lábpedál működés','','','Alap szín:\r\n               - lábak szürke\r\n               - oldallemez antracit');
INSERT INTO `bos_termekleiras_hu` VALUES ('330','330','MAD 250 FD pénztárpult','3841','0','1.8','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 2500 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:    1000 mm','MAD 250 FD pénztárpult','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - fotocellás működéssel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('331','331','MAD 300 FD pénztárpult','3861','0','1.8','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 3000 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:   1500 mm','MAD 300 FD pénztárpult','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - fotocellás működéssel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('332','332','MAD 350 FD pénztárpult','3881','0','1.8','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 3500 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:    2000 mm','MAD 350 FD pénztárpult','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - fotocellás működéssel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('333','333','MAD 250 FS pénztárpult','3901','0','1.8','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 2500 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:    1000 mm','MAD 250 FS pénztárpult','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - fotocellás működéssel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('334','334','MAD 300 FS pénztárpult','3921','0','1.8','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 3000 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:    1500 mm','MAD 300 FS pénztárpult','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - fotocellás működéssel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('335','335','MAD 350 FS pénztárpult','3941','0','1.8','0','1','- folyosózár kihajtható \r\n - scannervédő plexi \r\n - zárható fiók','Pénztárpult futószalaggal','9403','Hosszúság: 3500 mm\r\nMélység:        1100 mm\r\nMagasság:      850 mm\r\nFutószalag:   2000 mm','MAD 350 FS pénztárpult','- fém kivitel\r\n - rozsdamentes munkapult\r\n - süllyesztett elpakoló\r\n - oldaltakarás\r\n - kábel átvezető\r\n - kocsiütköző betét\r\n - resopal pénztárasztallap\r\n - fotocellás működéssel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('336','336','Egykaros kapu','268','0','1.8','0','1','','Vevő beengedő','9403','Hosszúság:  750 mm\r\nMélység:          80 mm.\r\nMagasság:   1070 mm\r\nÁtjáró:              600 mm','Egykaros kapu','- krómozott kivitel\r\n - változtatható nyitásirány','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('337','337','Forgókereszt','267','0','2.3','0','1','','Vevő beengedő','9403','Hosszúság: 1500 mm\r\nMélység:       1050 mm.\r\nMagasság:   1060 mm\r\nÁtjáró:              600 mm','Forgókereszt','- krómozott kivitel\r\n - változtatható forgásirány\r\n - kifordítható\r\n - íves oszloppal','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('338','338','Autómata lengőajtó','3821','0','1.8','0','1','','Vevő beengedő','9403','Hosszúság:  2340 mm\r\nMélység:        1300 mm.\r\nMagasság:   1070 mm\r\nÁtjáró:            1723 mm','Autómata lengőajtó','- krómozott kivitel\r\n - egyirányú nyitásirány\r\n - bevezető korlát\r\n - fotocellás működés','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('339','339','Korlát','6021','0','1.65','0','1','','Vevőterelő korlát','9403','Hosszúság:  1000 mm\r\nMélység:          40 mm.\r\nMagasság:   1080 mm','Korlát','- krómozott oszlopok\r\n - padlóhoz dűbelezhető\r\n - 40 mm-es korlátcső\r\n - kezdő oszloppal 1 m korláttal\r\n - 2 soros kialakítás','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('340','340','Plexifal','1801','0','1.65','0','1','','Plexi térelválasztófal','9403','Hosszúság:  1000 mm\r\nMélység:          40 mm.\r\nMagasság:   1800 mm','Plexifal','- krómozott oszlopok\r\n - padlóhoz dűbelezhető\r\n - 6 mm-es plexi\r\n - kezdő oszloppal 1 m plexifallal','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('341','341','Kocsiütköző','269','0','1.65','0','1','','Kocsiütköző','9403','Hosszúság:  1000 mm\r\nMélység:            54 mm.\r\nMagasság:     100 mm\r\nÁtjáró:              600 mm','Kocsiütköző','- alumínium profil\r\n - műanyag kocsiütköző\r\n - szintezhető fémtalp\r\n - műanyag végzárók\r\n - műanyag sarokprofil','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('342','342','Kosárkocsi','211','0','2.3','0','1','','Bevásárlókosár tartó kocsi','9403','Méret: 450x300x800','Kosárkocsi','- horganyzott kivitel\r\n- önbeálló kerekekkel\r\n- fém szerkezet\r\n - nyéllel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('343','343','Műanyag bevásárlókosár','1841','0','1.65','0','1','','Műanyag bevásárlókosár','9403','Méret: 480x300x200','Műanyag bevásárlókosár','- műanyag kivitel\r\n- két füllel\r\n- 20 literes\r\n - piros/fekete','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('344','344','Bevásárlókocsi 75L','4701','0','1.8','0','1','','Bevásárlókocsi 75L','9403','Méret: 75 liter','Bevásárlókocsi 75L','- horganyzott kivitel\r\n- önbeálló kerekekkel\r\n- fém szerkezet\r\n- logózható fogantyúval','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('345','345','Bevásárlókocsi 100L','4721','0','2','0','1','','Bevásárlókocsi 100L','9403','Méret: 100 liter','Bevásárlókocsi 100L','- horganyzott kivitel\r\n- önbeálló kerekekkel\r\n- fém szerkezet\r\n- logózható fogantyúval','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('346','346','Poliethilén vágólap','212','0','1.65','0','1','','Vágólap','','Méret: 400x300x25','Poliethilén vágólap','- polietilén alapanyag\r\n- lehajtható csúszófülek\r\n- zöld, piros, fehér,sárga színben','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('347','347','Kis akciós rácsasztal','4281','0','2','0','1','','Akciós rácsasztal','9403','hosszúság: 540 mm\r\nmélység:      380 mm\r\nmagasság:  770 mm','Kis akciós rácsasztal','- drótvázas kivitel\r\n - horganyzott felület\r\n - állítható rakodási mélység','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('348','348','Nagy akciós rácsasztal','4301','0','2','0','1','','Akciós rácsasztal','9403','hosszúság: 540 mm\r\nmélység:      540 mm\r\nmagasság:  770 mm','Nagy akciós rácsasztal','- drótvázas kivitel\r\n - horganyzott felület\r\n - állítható rakodási mélység','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('349','349','Sláger asztal','4321','0','1.65','0','1','','Akciós rácsasztal','9403','hosszúság: 1200 mm\r\nmélység:        800 mm\r\nmagasság:    770 mm','Sláger asztal','- drótvázas kivitel\r\n - horganyzott felület\r\n - állítható rakodási mélység\r\n - zártszelvény lábak','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('350','350','Hústőke rakott','2221','0','1.65','0','1','','Hústőke rakott','9403','magasság. 910mm\r\nhosszúság: 400 mm\r\nmélység:      400 mm\r\ntőke: 400x400x420 mm','Hústőke rakott','- fa lábak\r\n - rakott tőkerész\r\n - gőzölt bükk kivitel\r\n - újra gyalulható\r\n - dekoratív üzlettéri kivitel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('351','351','Rozsdamentes késtartó','213','0','1.65','0','1','','Rozsdamentes késtartó','','Méret: 400x300x150','Rozsdamentes késtartó','- polietilén betét\r\n- rozsdamentes szerkezet\r\n- pultra szerelhető','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('352','352','Húsfogas kamrába','375','0','1.65','0','1','','Rozsdamentes húsfogas','9403','magasság. 2400 mm\r\nhosszúság: 1000 mm\r\n40x40 mm-es zártszelvény','Húsfogas kamrába','- rozsdamentes zártszelvény lábal\r\n - rozsdamentes zártszelvény összekötők\r\n - rozsdamentes esztergált horgok\r\n - egysoros kivitel\r\n - egyedi méretben','','','Az ár 1 fm. fogasra vonatkozik!');
INSERT INTO `bos_termekleiras_hu` VALUES ('353','353','Elszívóernyő','4661','0','1.65','0','1','','Elszívóernyő','9403','magasság. 500 mm\r\nhosszúság: 1000 mm\r\nmélység: 900 mm','Elszívóernyő','- rozsdamentes anyagból\r\n - zsírfogó rács\r\n - elszívómotor nélkül\r\n - oldalfali kivitel','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('354','354','Elpakoló asztal','1421','0','1.65','0','1','','Elpakoló asztal','9403','hosszúság: 1000 mm\r\nmélység:        400 mm\r\nmagasság:    900 mm.','Elpakoló asztal','- laminált szerkezet\r\n - szintezhető lábak\r\n - műanyag élzárás\r\n - resopal munkapult\r\n - hátsó perem','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('355','355','Háttérpult','501','0','1.65','0','1','','Háttérpult','9403','hosszúság: 1000 mm\r\nmélység:        600 mm\r\nmagasság: 2000 mm.','Háttérpult','- laminált szerkezet\r\n - resoal munkapult\r\n - MDF festett fríz\r\n - szintező lábak\r\n - műanyag élzárás\r\n - beépített világítás\r\n - alsó polc','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('356','356','Kenyeres állvány 125','621','0','1.65','0','1','','Kenyeres állvány','9403','hosszúság: 1250 mm\r\nmélység:        400 mm\r\nmagasság: 2000 mm.','Kenyeres állvány 125','- laminált szerkezet\r\n - bükkfa rácspolc\r\n - MDF festett fríz\r\n - döntött tükör\r\n - morzsatálca\r\n - szintezhető lábak\r\n - világítás','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('357','357','Kenyeres konténer 125','2981','0','1.65','0','1','','Kenyeres konténer','9403','hosszúság: 1250 mm\r\nmélység:        450 mm\r\nmagasság: 1600mm.','Kenyeres konténer 125','- fa szerkezet\r\n - bükkfa rácspolc\r\n - masszív, dekoratív kivitel\r\n - fékezhető kerekekkel\r\n - mobil kivitel\r\n - morzsa gyűtő','','','Egyedi kialakításról érdeklődjön!');
INSERT INTO `bos_termekleiras_hu` VALUES ('358','358','Zsemle-kifli adagoló 125','641','0','1.65','0','1','','Zsemle-kifli adagoló','9403','hosszúság: 1250 mm\r\nmélység:        400 mm\r\nmagasság: 2000 mm.','Zsemle-kifli adagoló 125','- laminált szerkezet\r\n - bükkfa rácspolc\r\n - MDF festett fríz\r\n - döntött tükör\r\n - morzsatálca\r\n - szintezhető lábak\r\n - világítás\r\n - osztott tároló','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('359','359','Önkiszolgáló állvány','441','0','1.65','0','1','','Önkiszolgáló állvány','9403','hosszúság:   750 mm\r\nmélység:        600 mm\r\nmagasság: 1300 mm.','Önkuszolgáló állvány','- laminált szerkezet\r\n - plexi leheletvédő\r\n - MDF festett fríz\r\n - automata plexinyitás\r\n - morzsatálca\r\n - szintezhető lábak\r\n - világítás\r\n - 3 fiókos','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('360','360','Átadó asztal G','2241','0','1.65','0','1','','Átadó asztal','9403','hosszúság: 1000 mm\r\nmélység:        600 mm\r\nmagasság:    900 mm.','Átadó asztal G','- laminált szerkezet\r\n - szintezhető lábak\r\n - műanyag élzárás\r\n - resopal munkapult','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('361','361','Bemutató vitrin','2641','0','1.65','0','1','','Péksüteményes bemutató vitrin','9403','hosszúság: 1000 mm\r\nmélység:        860 mm\r\nmagasság:    1350mm.','Bemutató vitrin','- laminált szerkezet\r\n - szintezhető lábak\r\n - műanyag élzárás\r\n - resopal munkapult\r\n - üveg leheletvédő\r\n - 2 sor üvegpolc','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('362','362','Zöldséges állvány 125','661','0','1.65','0','1','','Zöldséges állvány','9403','hosszúság: 1250 mm\r\nmélység:        800 mm\r\nmagasság: 2000 mm.','Zöldséges állvány 125','- fenyő szerkezet\r\n - bükkfa rácspolc\r\n - laminált fríz\r\n - döntött tükör\r\n - 3 szögben állítható polc\r\n - szintezhető lábak\r\n - világítás\r\n - alsó polc kivehető a takarítás könnyítésére','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('363','363','Zöldséges sziget 125','4521','0','1.65','0','1','','Zöldséges állvány','9403','hosszúság: 1250 mm\r\nmélység:      1500 mm\r\nmagasság: 1300 mm.\r\nrácsméret:   1250x1200 mm.','Zöldséges sziget 125','- fenyő szerkezet\r\n - bükkfa rácspolc\r\n - döntött polc\r\n - szintezhető lábak\r\n - kétoldalas kivitel\r\n - rácspolc kivehető a takarítás könnyítésére\r\n - sorolható','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('364','364','Zöldséges raklapkeret','481','0','1.65','0','1','','Zöldséges raklapkeret','9403','hosszúság: 1200 mm\r\nmélység:        800 mm\r\nmagasság:    700 mm.','Zöldséges raklapkeret','- fenyő szerkezet\r\n - vízbázisú pác\r\n - EUR raklapra illeszthető','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('365','365','Átadó asztal LSG','4441','0','1.65','0','1','','Átadó asztal','9403','hosszúság: 1000 mm\r\nmélység:      1100 mm\r\nmagasság:    910 mm.','Átadó asztal LSG','- fém szerkezet\r\n - szintezhető lábak\r\n - műanyag élzárás\r\n - resopal munkapult','','','Egyedi kialakításokért érdeklődjön');
INSERT INTO `bos_termekleiras_hu` VALUES ('366','366','KLARA/KLAUDIA 15 MP átadópult','5601','0','1.65','0','1','','Átadópult','8418','- hosszúság : 1500\r\n- szélesség : 1200\r\n- magasság : 890\r\n - villamos telj.: 0 W','KLARA/KLAUDIA 15 MP átadópult','- ABS oldalakkal\r\n- kocsiütközővel\r\n- ívelt frontüveg\r\n- RM. bemutatófelület\r\n- alsó tároló\r\n- rozsdamentes munkapult','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('367','367','KLARA 36 P pékárupult','5541','0','1.65','0','1','','Pékárupult','8418','- hosszúság : 3600\r\n- szélesség : 1200\r\n- magasság : 1200\r\n - villamos telj.: 60 W','KLARA 36 P pékárupult','- ABS oldalakkal\r\n- kocsiütközővel\r\n- ívelt frontüveg\r\n- RM. bemutatófelület\r\n- alsó tároló\r\n- rozsdamentes munkapult\r\n- hűtés nélkül\r\n- farács betét','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('368','368','Topaz 220 felvágott szeletelő','202','0','1.65','0','1','','Felvágott szeletelő','8438','Feszültségigény: 230 V','Topaz 220','- 220 mm-es korongátmérő\r\n- állítható szeletvastagság\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- élezőkorong\r\n- döntött vágólap\r\n- bronzperselyes szán','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('369','369','HS 10/250 felvágott szeletelő','203','0','1.65','0','1','','Felvágott szeletelő','8438','Feszültségigény: 230 V','HS 10/250','- 250 mm-es korongátmérő\r\n- állítható szeletvastagság\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- élezőkorong\r\n- döntött vágólap\r\n- bronzperselyes szán','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('370','370','Topaz 275 felvágott szeletelő','204','0','1.65','0','1','','Felvágott szeletelő','8438','Feszültségigény: 230 V','Topaz 275','- 275 mm-es korongátmérő\r\n- állítható szeletvastagság\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- élezőkorong\r\n- döntött vágólap\r\n- bronzperselyes szán','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('371','371','HS 12/300 felvágott szeletelő','205','0','1.65','0','1','','Felvágott szeletelő','8438','Feszültségigény: 230 V','HS 12/300','- 300 mm-es korongátmérő\r\n- állítható szeletvastagság\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- élezőkorong\r\n- döntött vágólap\r\n- bronzperselyes szán','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('372','372','Anniversario 300 sonka szeletelő','4041','0','1.65','0','1','','Sonka szeletelő','8438','960x920x860 mm\r\n60 kg','Anniversario 300 sonka szeletelő','- 300 mm-es korongátmérő\r\n- állítható szeletvastagság\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- élezőkorong\r\n-  függőleges vágólap\r\n- bronzperselyes szán\r\n - mechanikus kivitel\r\n- jubileumi kiadás','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('373','373','TC 8 Vegas húsdaráló','207','0','1.65','0','1','','Húsdaráló','8438','Feszültségigény: 230 V 1 fázis','TC 8 Vegas','- 80 kg/óra teljesítmény\r\n- rozsdamentes gyűjtőtálca\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- tartozék tömő\r\n- visszaindítási lehetőség','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('374','374','G 81/180 húsdaráló','206','0','1.65','0','1','','Húsdaráló','8438','Feszültségigény: 230 V 1 fázis\r\nVálaszható 400 V 3 fázis','G 81/180','- 180 kg/óra teljesítmény\r\n- rozsdamentes gyűjtőtálca\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- tartozék tömő\r\n- visszaindítási lehetőség','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('375','375','TC 22 E húsdaráló','381','0','1.65','0','1','','Húsdaráló','8438','Feszültségigény: 230 V 1 fázis\r\nVálaszható 400 V 3 fázis','TC 22 E','- 250 kg/óra teljesítmény\r\n- rozsdamentes gyűjtőtálca\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- tartozék tömő\r\n- visszaindítási lehetőség','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('376','376','TC 22 ICE hűtött húsdaráló','208','0','1.65','0','1','','Hűtött húsdaráló','8438','Feszültségigény: 230 V 1 fázis\r\nHűtés: 0/+2°C','TC 22 ICE','- 150 kg/óra teljesítmény\r\n- rozsdamentes gyűjtőtálca\r\n- könnyűfém gépház\r\n- adonizált felületkezelés\r\n- nemesacél kés\r\n- tartozék tömő\r\n- visszaindítási lehetőség\r\n- hűtött tároló','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('377','377','Eszköz sterilizáló','210','0','1.65','0','1','','UV eszköz fertőtlenítő','8438','Feszültségigény: 230 V 1 fázis\r\nMéret A: 400 mm\r\nMéret B: 140 mm\r\nMéret C: 520 mm\r\nMéret D: 620 mm','Eszköz sterilizáló','- rozsdamentes gépelemek\r\n- elektromos működés\r\n- 16 W UV lámpa\r\n- egyszerű működtetés\r\n- sötétített üvegajtóval\r\n- túlmelegedés védelemmel\r\n- belső világítással\r\n- időzítővel\r\n- biztonsági zárral','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('378','378','Késfertőtlenítő','248','0','1.65','0','1','','Vízfürdős késfertőtlenítő','8438','Feszültség igény: 230 V 16 A\r\nHőmérséklet tartomány: +60/+85 °C\r\nMéret A: 310 mm\r\nMéret B: 140 mm\r\nMéret C: 318 mm\r\nMéret D: 500 mm','Késfertőtlenítő','- rozsdamentes kivitel\r\n - vízfürdős technológia\r\n - beépített hőmérő\r\n - leeresztő szelep\r\n - állítható vízhőfok\r\n - túlhevülés védelem\r\n - fertőtlenítő folyadékkal is használható','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('379','379','Kézi fóliázó 45K','209','0','1.65','0','1','','Mechanikus kézifóliázó','8438','Feszültségigény: 230 V 1 fázis\r\nMéret A: 485 mm\r\nMéret B: 600 mm\r\nMéret C: \r\nMéret D: 140 mm','Kézi fóliázó 45K','- rozsdamentes gépelemek\r\n- mechanikus működés\r\n- fűtött görgő\r\n- egyszerű működtetés\r\n- habtálcás csomagoláshoz','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('380','380','Melegentartó HotSpot','6361','0','1.65','0','1','','Melegentartó pékáru és sültek részére','8438','hossz.: 606 mm\r\nmély.:   446 mm\r\nmag.:   452 mm\r\nteljesítmény: 1050 W\r\nvilágítás: 2x15 W\r\nhőfok: +30/+90°C','Melegentartó HotSpot','- mechanikus vezérlés\r\n- páratartalom szabályozás 5%-55% között\r\n- nyíló plexi ajtókkal két oldalon\r\n- mindkét polc világítva\r\n- elektromos fűtés\r\n- rozsdamentes kivitel\r\n- egyedi RAL színekben\r\n- masszív kialakítás','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('381','381','Melegentartó Loire','6381','0','1.65','0','1','','Melegentartó pékáru és sültek részére','8438','hossz.: 606 mm\r\nmély.:   420 mm\r\nmag.:   452 mm\r\nteljesítmény: 1050 W\r\nvilágítás: 2x15 W\r\nhőfok: +30/+90°C','Melegentartó Loire','- mechanikus vezérlés\r\n- nyíló plexi ajtókkal két oldalon\r\n- mindkét polc világítva\r\n- elektromos fűtés\r\n- rozsdamentes kivitel\r\n- egyedi RAL színekben\r\n- masszív kialakítás','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('382','382','EVO 60 HOT','6641','0','1.65','0','1','','Melegentartó vitrin','8418','- hosszúság : 600 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm','EVO 60 HOT','- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs fűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('383','383','EVO 90 HOT','6661','0','1.65','0','1','','Melegentartó vitrin','8418','- hosszúság : 900 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm','EVO 90 HOT','- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs fűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('384','384','EVO 120 HOT','6681','0','1.65','0','1','','Melegentartó vitrin','8418','- hosszúság : 1200 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm','EVO 120 HOT','- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs fűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('385','385','EVO 150 HOT','6701','0','1.65','0','1','','Melegentartó vitrin','8418','- hosszúság : 1500 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm','EVO 150 HOT','- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs fűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('386','386','EVO 180 HOT','6721','0','1.65','0','1','','Melegentartó vitrin','8418','- hosszúság : 1800 mm\r\n- szélesség :    785 mm\r\n- magasság   1400 mm','EVO 180 HOT','- szigetelt üveg oldalakkal\r\n- elektronikus hőfokbeállítás\r\n- szigetelt ívelt frontüveg\r\n- ventilációs fűtés\r\n- világítás\r\n- szigetelt üveg tolóajtó\r\n- 3 sor üvegpolccal','','','25°C környezeti hőmérséklet\r\n60 % páratartalom mellett');
INSERT INTO `bos_termekleiras_hu` VALUES ('387','387','XVC 305 E Kombi gőzpároló sütő','4601','0','1.65','0','1','','Kombi gőzpároló sűtő','84198180','Feszültségigény: 400 V\r\nTeljesítmény: 6 kW\r\nSúly: 59 kg\r\nMéret: 750x792x632 mm','XVC 305 E Kombi gőzpároló sütő','- 10 beépített program\r\n- 99 beállítható saját programhely\r\n- 6 fokozatú ventilátor\r\n- LED technológiás belső világítás\r\n- 5 GN 1/1 kapacitás\r\n- tálca távolság 67mm\r\n- vezérlés érintőpanellel\r\n- sütési hőfok max. 260 °C\r\n- előmelegítés 300 °C','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('388','388','XFT 135 Arianna Dynamic Sütőkemence','4621','0','1.65','0','1','','Látványsűtő','84198180','Feszültségigény: 230 V\r\nTeljesítmény: 3 kW\r\nSúly: 31 kg\r\nMéret: 600x651x509 mm','XFT 135 Arianna Dynamic Sütőkemence','- digitális vezérlés\r\n- alsó programóra\r\n- légkeverés\r\n- párásítás\r\n- elszívás és kelesztés vezérlés\r\n- idő és hőfok beállítás\r\n- belső világítás\r\n- 3 lépésre osztható sütési ciklus\r\n- 4 x 460x330 tálca kapacitás\r\n- max. hőfok: 260 °C','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('389','389','XLT 135 Kelesztő','4641','0','1.65','0','1','','Látványsütő kelesztő','84198180','Feszültségigény: 230 V\r\nTeljesítmény: 1,2 kW\r\nSúly: 22 kg\r\nMéret: 600x650x757 mm','XLT 135 Kelesztő','- kapacitás: 8 db 460 x 330 tálca\r\n- tálvatávolság 70 mm\r\n- max. hőmérséklet: 50 °C\r\n- sütőről vezérelve','','','');
INSERT INTO `bos_termekleiras_hu` VALUES ('390','390','Special fémpolc','293','0','1.65','0','1','','Raktári fémpolc','9403','hosszúság: 1000 mm\r\nmélység:        400 mm\r\nmagasság:  2000 mm\r\nteherbírás:     400 kg\r\nteherbírás polconként 100 kg.','Special fémpolc','- fém kivitel\r\n- porszórt felületkezelés\r\n- műanyag lábbetét\r\n- csavarkötés\r\n- 5 sor polc','','','Egységcsomagban, szétszerelve szállítjuk!');
-- --------------------------------------------------------

--
-- Table structure for table `bos_termekmezok`
--

DROP TABLE IF EXISTS `bos_termekmezok`;

CREATE TABLE `bos_termekmezok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kulcs` varchar(20) NOT NULL,
  `nev` varchar(255) NOT NULL,
  `tipus` varchar(20) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_termekmezok`
--

INSERT INTO `bos_termekmezok` VALUES ('1','nev','Terméknév','VARCHAR(255)','0');
INSERT INTO `bos_termekmezok` VALUES ('9','ant_id','Antshop ID','INT','0');
INSERT INTO `bos_termekmezok` VALUES ('10','sorszam','Sorszám','INT','0');
INSERT INTO `bos_termekmezok` VALUES ('11','arres','Árrés','FLOAT','0');
INSERT INTO `bos_termekmezok` VALUES ('12','arfolyam','Frissítési árfolyam','FLOAT','0');
INSERT INTO `bos_termekmezok` VALUES ('13','frissitheto','Frissíthető','INT','0');
INSERT INTO `bos_termekmezok` VALUES ('14','pdfpic1','PDF kép 1','VARCHAR(255)','0');
INSERT INTO `bos_termekmezok` VALUES ('15','pdfpic2','PDF kép 2','VARCHAR(255)','0');
INSERT INTO `bos_termekmezok` VALUES ('16','jellemzok','Műszaki jellemzők','TEXT','0');
INSERT INTO `bos_termekmezok` VALUES ('17','tipus','Típus','VARCHAR(255)','0');
INSERT INTO `bos_termekmezok` VALUES ('18','parameterek','Műsszaki paraméterek','TEXT','0');
INSERT INTO `bos_termekmezok` VALUES ('19','vtsz','Vtsz','VARCHAR(255)','0');
INSERT INTO `bos_termekmezok` VALUES ('20','kisleiras','Kis leírás','TEXT','0');
INSERT INTO `bos_termekmezok` VALUES ('21','tartozekok','Rendelhető tartozékok','TEXT','0');
INSERT INTO `bos_termekmezok` VALUES ('22','megjegyzes','Megjegyzés','TEXT','0');
-- --------------------------------------------------------

--
-- Table structure for table `bos_termekxadatlap`
--

DROP TABLE IF EXISTS `bos_termekxadatlap`;

CREATE TABLE `bos_termekxadatlap` (
  `termek_id` int(11) NOT NULL,
  `adatlap_csoport_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_termekxadatlap`
--

INSERT INTO `bos_termekxadatlap` VALUES ('244','9');
INSERT INTO `bos_termekxadatlap` VALUES ('242','9');
INSERT INTO `bos_termekxadatlap` VALUES ('320','6');
INSERT INTO `bos_termekxadatlap` VALUES ('238','7');
INSERT INTO `bos_termekxadatlap` VALUES ('239','8');
INSERT INTO `bos_termekxadatlap` VALUES ('300','2');
INSERT INTO `bos_termekxadatlap` VALUES ('243','9');
-- --------------------------------------------------------

--
-- Table structure for table `bos_termekxkategoria`
--

DROP TABLE IF EXISTS `bos_termekxkategoria`;

CREATE TABLE `bos_termekxkategoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `termek_id` int(11) NOT NULL,
  `kategoria_id` int(11) NOT NULL,
  `nyelv` varchar(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `termek_id` (`termek_id`,`kategoria_id`)
) ENGINE=InnoDB AUTO_INCREMENT=542 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bos_termekxkategoria`
--

INSERT INTO `bos_termekxkategoria` VALUES ('1','1','14','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('23','7','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('29','8','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('30','9','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('31','10','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('32','11','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('33','12','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('34','13','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('35','14','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('36','15','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('37','16','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('38','17','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('39','18','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('40','19','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('41','20','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('42','21','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('43','22','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('44','23','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('45','24','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('46','25','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('47','26','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('48','27','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('49','28','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('50','29','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('51','30','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('56','31','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('58','32','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('64','33','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('65','34','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('66','35','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('67','36','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('68','37','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('69','38','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('70','39','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('71','40','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('72','41','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('73','42','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('74','43','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('75','44','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('76','45','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('77','46','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('78','47','23','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('84','48','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('85','49','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('91','50','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('96','51','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('102','2','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('109','3','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('110','4','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('116','5','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('122','6','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('128','52','22','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('129','53','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('130','54','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('131','55','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('132','56','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('133','57','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('134','58','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('135','59','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('136','60','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('138','61','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('139','62','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('140','63','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('141','64','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('142','65','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('143','66','24','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('144','67','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('145','68','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('146','69','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('147','70','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('148','71','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('149','72','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('150','73','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('151','74','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('152','75','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('153','76','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('154','77','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('155','78','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('156','79','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('157','80','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('158','81','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('159','82','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('160','83','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('161','84','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('162','85','25','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('167','90','26','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('168','91','26','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('169','92','26','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('170','93','26','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('171','94','26','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('172','95','26','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('173','96','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('174','97','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('175','98','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('176','99','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('177','100','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('178','101','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('179','102','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('180','103','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('181','104','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('182','105','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('183','106','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('184','107','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('185','108','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('186','109','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('187','110','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('188','111','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('189','112','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('190','113','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('191','114','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('192','115','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('193','116','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('194','117','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('195','118','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('196','119','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('197','120','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('198','121','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('199','122','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('203','123','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('204','86','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('205','87','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('206','88','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('207','89','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('208','124','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('209','125','27','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('210','126','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('211','127','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('212','128','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('213','129','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('214','130','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('215','131','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('216','132','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('217','133','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('221','134','28','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('222','135','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('223','136','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('224','137','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('225','138','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('226','139','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('227','140','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('228','141','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('229','142','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('230','143','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('231','144','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('232','145','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('233','146','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('234','147','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('235','148','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('236','149','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('237','150','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('238','151','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('239','152','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('240','153','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('241','154','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('242','155','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('243','156','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('244','157','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('245','158','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('246','159','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('247','160','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('248','161','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('249','162','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('250','163','29','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('252','165','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('253','166','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('254','164','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('255','167','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('256','168','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('257','169','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('258','170','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('259','171','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('260','172','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('261','173','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('262','174','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('263','175','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('264','176','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('265','177','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('266','178','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('267','179','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('268','180','30','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('269','181','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('270','182','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('271','183','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('272','184','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('273','185','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('274','186','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('276','188','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('277','189','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('278','190','31','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('279','191','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('280','192','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('281','193','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('282','194','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('283','195','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('284','196','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('286','197','32','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('287','198','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('288','199','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('289','200','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('290','201','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('291','187','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('292','202','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('293','203','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('294','204','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('295','205','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('297','206','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('298','207','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('299','208','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('303','209','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('304','210','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('305','211','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('306','212','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('307','213','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('308','214','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('309','215','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('310','216','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('311','217','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('312','218','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('313','219','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('314','220','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('316','221','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('317','222','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('318','223','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('320','224','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('321','225','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('322','226','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('323','227','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('324','228','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('325','229','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('326','230','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('327','231','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('329','232','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('332','233','33','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('333','234','34','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('334','235','34','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('335','236','34','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('336','237','34','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('352','240','35','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('353','241','35','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('356','238','35','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('359','239','35','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('362','242','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('363','243','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('364','244','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('365','245','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('366','246','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('367','247','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('368','248','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('369','249','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('370','250','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('371','251','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('372','252','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('373','253','36','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('374','254','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('375','255','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('376','256','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('377','257','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('378','258','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('379','259','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('380','260','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('381','261','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('382','262','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('383','263','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('384','264','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('385','265','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('386','266','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('387','267','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('388','268','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('389','269','38','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('390','270','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('391','271','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('392','272','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('393','273','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('394','274','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('395','275','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('396','276','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('397','277','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('398','278','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('399','279','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('400','280','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('401','281','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('402','282','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('403','283','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('404','284','39','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('405','285','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('406','286','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('407','287','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('408','288','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('409','289','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('410','290','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('411','291','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('412','292','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('413','293','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('414','294','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('415','295','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('416','296','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('417','297','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('418','298','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('419','299','40','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('421','301','42','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('422','302','42','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('430','300','42','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('431','303','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('432','304','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('433','305','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('434','306','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('435','307','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('436','308','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('438','309','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('439','310','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('440','311','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('441','312','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('442','313','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('443','314','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('444','315','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('445','316','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('446','317','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('447','318','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('448','319','43','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('458','320','45','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('459','321','45','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('460','322','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('461','323','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('462','324','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('463','325','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('464','326','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('465','327','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('466','328','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('467','329','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('468','330','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('469','331','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('470','332','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('471','333','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('472','334','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('473','335','46','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('474','336','47','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('475','337','47','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('476','338','47','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('477','339','47','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('478','340','47','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('479','341','47','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('480','342','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('481','343','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('482','344','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('483','345','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('484','346','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('485','347','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('486','348','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('487','349','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('488','350','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('489','351','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('490','352','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('491','353','48','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('493','355','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('495','356','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('496','357','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('498','358','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('500','359','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('502','360','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('503','361','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('505','362','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('506','363','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('507','364','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('508','354','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('509','365','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('512','366','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('518','367','49','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('519','368','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('520','369','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('521','370','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('522','371','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('523','372','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('524','373','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('525','374','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('526','375','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('527','376','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('528','377','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('529','378','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('530','379','50','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('531','380','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('532','381','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('533','382','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('534','383','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('535','384','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('536','385','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('537','386','51','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('538','387','52','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('539','388','52','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('540','389','52','hu','0');
INSERT INTO `bos_termekxkategoria` VALUES ('541','390','53','hu','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_beepulo_belepesek`
--

DROP TABLE IF EXISTS `bow_beepulo_belepesek`;

CREATE TABLE `bow_beepulo_belepesek` (
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`nev`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bow_beepulo_belepesek`
--

INSERT INTO `bow_beepulo_belepesek` VALUES ('_aruhaz_termékképszerkesztés','70');
INSERT INTO `bow_beepulo_belepesek` VALUES ('_szerkeszto_Szövegszerkesztés','70');
INSERT INTO `bow_beepulo_belepesek` VALUES ('HTML osztály létrehozása','30');
INSERT INTO `bow_beepulo_belepesek` VALUES ('Modul futtatás','50');
INSERT INTO `bow_beepulo_belepesek` VALUES ('Oldal modul futtatás','40');
INSERT INTO `bow_beepulo_belepesek` VALUES ('Rendszer indítás','10');
INSERT INTO `bow_beepulo_belepesek` VALUES ('Rendszerfutás vége','100');
INSERT INTO `bow_beepulo_belepesek` VALUES ('Tagok létrehozása','20');
INSERT INTO `bow_beepulo_belepesek` VALUES ('Tartalom hozzáfűzés','55');
INSERT INTO `bow_beepulo_belepesek` VALUES ('URL létrehozása','25');
-- --------------------------------------------------------

--
-- Table structure for table `bow_beepulok`
--

DROP TABLE IF EXISTS `bow_beepulok`;

CREATE TABLE `bow_beepulok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `file` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `belepes` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  `kikapcsolva` tinyint(4) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bow_beepulok`
--

INSERT INTO `bow_beepulok` VALUES ('5','Social buttons for blog posts','b_socbuttons.php','Oldal modul futtatás','1','10');
INSERT INTO `bow_beepulok` VALUES ('6','Felhasználó beléptetése','bow_login.php','Tagok létrehozása','0','0');
INSERT INTO `bow_beepulok` VALUES ('7','Palánta - felhaszáló ellenőrzés','palanta_ellenorzo.php','HTML osztály létrehozása','0','0');
INSERT INTO `bow_beepulok` VALUES ('8','Facebook scriptek','facebook_login.php','HTML osztály létrehozása','0','0');
INSERT INTO `bow_beepulok` VALUES ('9','Admin elület elérésének korlátozása','admin_kapu.php','URL létrehozása','0','0');
INSERT INTO `bow_beepulok` VALUES ('10','Galéria szövegekhez','_szoveg_galeria.php','_szerkeszto_Szövegszerkesztés','1','0');
INSERT INTO `bow_beepulok` VALUES ('11','Galéria shortcode','_szoveg_galeria_shortcode.php','Tartalom hozzáfűzés','1','0');
INSERT INTO `bow_beepulok` VALUES ('12','Termékképek kezelése','_aruhaz_termekkep.php','_aruhaz_termékképszerkesztés','0','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_cimkek`
--

DROP TABLE IF EXISTS `bow_cimkek`;

CREATE TABLE `bow_cimkek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cimke` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  `csoportok` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `feliratkozhat` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bow_cimkek`
--

INSERT INTO `bow_cimkek` VALUES ('13','Könyvajánló','','','');
INSERT INTO `bow_cimkek` VALUES ('14','Blog','<p>teszt</p>','','7,5,4');
INSERT INTO `bow_cimkek` VALUES ('15','GYIK','Gyakran ismételt kérdések','','');
INSERT INTO `bow_cimkek` VALUES ('16','Hírlevél','<p>Heti rendszeres h&iacute;rlevel&uuml;nk</p>','','7');
INSERT INTO `bow_cimkek` VALUES ('17','Események','','','7,5,4,6,1');
-- --------------------------------------------------------

--
-- Table structure for table `bow_cimkexszoveg`
--

DROP TABLE IF EXISTS `bow_cimkexszoveg`;

CREATE TABLE `bow_cimkexszoveg` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cimke_id` int(11) NOT NULL,
  `szoveg_id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_cimkexszoveg`
--

INSERT INTO `bow_cimkexszoveg` VALUES ('123','14','27','10');
-- --------------------------------------------------------

--
-- Table structure for table `bow_config`
--

DROP TABLE IF EXISTS `bow_config`;

CREATE TABLE `bow_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `csoport` varchar(40) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kulcs` varchar(40) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `ertek` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tipus` varchar(2) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'tb',
  `opciok` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kulcs` (`kulcs`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_config`
--

INSERT INTO `bow_config` VALUES ('21','Rendszer','ADMIN_URI','parancsnok','tb','');
INSERT INTO `bow_config` VALUES ('22','Rendszer','ADMIN_STILUS','adminisztracio','tb','');
INSERT INTO `bow_config` VALUES ('23','Rendszer','REGISZTRALT_ALAP_CSOPORT','5','tb','');
INSERT INTO `bow_config` VALUES ('24','Rendszer','PHP_HIBA','0','tb','');
INSERT INTO `bow_config` VALUES ('25','Facebook','FBCONNECT_APPID','713154658701266','tb','');
INSERT INTO `bow_config` VALUES ('26','Rendszer','ALAP_NYELV','hu','tb','');
INSERT INTO `bow_config` VALUES ('27','Rendszer','STILUS','b-store','tb','');
INSERT INTO `bow_config` VALUES ('29','E-mail bellítások','NOTIF_EMAIL','fejlesztes@palanta.hu','tb','');
INSERT INTO `bow_config` VALUES ('30','Weboldal általános beállítások','FELTOLTES_KEPKONYVTAR','feltoltesek/tiny/','tb','');
INSERT INTO `bow_config` VALUES ('31','Weboldal általános beállítások','CMS_KONYVTAR','web/b-store/','tb','');
INSERT INTO `bow_config` VALUES ('32','Weboldal általános beállítások','BEJEGYZES_URL','bejegyzesek/','tb','');
INSERT INTO `bow_config` VALUES ('33','E-mail bellítások','EMAIL_FELADO_NEV','Palánta Iskola','tb','');
INSERT INTO `bow_config` VALUES ('34','E-mail bellítások','EMAIL_FELADO_CIM','fejlesztes@palanta.hu','tb','');
INSERT INTO `bow_config` VALUES ('35','E-mail bellítások','EMAIL_SMTP_HOST','mail.zente.org','tb','');
INSERT INTO `bow_config` VALUES ('36','E-mail bellítások','EMAIL_SMTP_PORT','26','tb','');
INSERT INTO `bow_config` VALUES ('37','E-mail bellítások','EMAIL_SMTP_USER','ivan@zente.org','tb','');
INSERT INTO `bow_config` VALUES ('38','E-mail bellítások','EMAIL_SMTP_PASS','joha100NNA','tb','');
INSERT INTO `bow_config` VALUES ('39','Weboldal általános beállítások','BASE_URL','http://127.0.0.1:8888/web/b-store/','tb','');
INSERT INTO `bow_config` VALUES ('40','Weboldal általános beállítások','BOW_META_TITLE','B-Store Kft.','tb','');
INSERT INTO `bow_config` VALUES ('41','Weboldal általános beállítások','BOW_META_KEYWORDS','Hűtött és hűtetlen üzletberendezés, hűtőpult, hűtőkamra, pénztárpult, falihűtő','tb','');
INSERT INTO `bow_config` VALUES ('42','Weboldal általános beállítások','BOW_META_DESC','- Hűtött és hűtetlen üzletberendezés, hűtőpult, hűtőkamra, pénztárpult, falihűtő','tb','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_csoportok`
--

DROP TABLE IF EXISTS `bow_csoportok`;

CREATE TABLE `bow_csoportok` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) NOT NULL,
  `leiras` varchar(255) NOT NULL,
  `alapertelmezett` tinyint(4) NOT NULL DEFAULT '0',
  `rang` int(11) NOT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT '0',
  `regisztralt_alapertelmezett` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_csoportok`
--

INSERT INTO `bow_csoportok` VALUES ('1','Titkárság','Titkársági dolgozó','0','1000','1','0');
INSERT INTO `bow_csoportok` VALUES ('4','Látogató','Látogató','1','0','0','0');
INSERT INTO `bow_csoportok` VALUES ('5','Fejlesztő','Honlapfejlesztő','0','2000','1','0');
INSERT INTO `bow_csoportok` VALUES ('6','Rejtett tartalom','Elrejtett tartalmak','0','500','0','0');
INSERT INTO `bow_csoportok` VALUES ('7','Bejelentkezett tag','Bejelentkezett látogató','0','50','0','1');
-- --------------------------------------------------------

--
-- Table structure for table `bow_esemenyek`
--

DROP TABLE IF EXISTS `bow_esemenyek`;

CREATE TABLE `bow_esemenyek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `cim` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_esemenyek`
--

INSERT INTO `bow_esemenyek` VALUES ('1','2014-03-14','1848/49-es szabadság-harc emléknapja','');
INSERT INTO `bow_esemenyek` VALUES ('2','2014-03-22','Tavaszi játszóház','');
INSERT INTO `bow_esemenyek` VALUES ('3','2014-03-26','Nyílt nap','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_feliratkozas`
--

DROP TABLE IF EXISTS `bow_feliratkozas`;

CREATE TABLE `bow_feliratkozas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cimke_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_feliratkozas`
--

INSERT INTO `bow_feliratkozas` VALUES ('5','7','8');
INSERT INTO `bow_feliratkozas` VALUES ('6','14','12');
INSERT INTO `bow_feliratkozas` VALUES ('7','14','14');
INSERT INTO `bow_feliratkozas` VALUES ('8','14','13');
INSERT INTO `bow_feliratkozas` VALUES ('9','14','15');
-- --------------------------------------------------------

--
-- Table structure for table `bow_fordito`
--

DROP TABLE IF EXISTS `bow_fordito`;

CREATE TABLE `bow_fordito` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `szoveg` text NOT NULL,
  `hivo` varchar(100) NOT NULL,
  `nyelv` varchar(5) NOT NULL,
  `hash` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=923 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_fordito`
--

INSERT INTO `bow_fordito` VALUES ('97','Opció neve:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','198f6c50004a2c476a625042cd69c9bd');
INSERT INTO `bow_fordito` VALUES ('98','Opció leírás:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','cc8397f30f1d0dd70a7da33c39033b7d');
INSERT INTO `bow_fordito` VALUES ('99','Opciós ár:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','81fbc580f6ed3ac1488e74f138a74484');
INSERT INTO `bow_fordito` VALUES ('100','Sorrend:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','d71a4ae5e47ac5e2702c40b0ae7d9038');
INSERT INTO `bow_fordito` VALUES ('101','-- OPCIÓ TÖRLÉS --','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','2fd3eadf980ffc559c2320fd8f7cbd0e');
INSERT INTO `bow_fordito` VALUES ('102','Töröljem ezt az opciót?','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','d4ade91e16fcb13488dc331f2fefcd92');
INSERT INTO `bow_fordito` VALUES ('103','Mentés után kerül törlésre az opció','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','hu','b13a275eb3afd8699019325a65f4be28');
INSERT INTO `bow_fordito` VALUES ('104','KATEGÓRIÁK','temak/responsive/html/_aruhaz_widgetek_sidebar.php','hu','e84c8aac75043a4cf79f6ae4e4c7a0dc');
INSERT INTO `bow_fordito` VALUES ('105','KATEGÓRIÁK','temak/b-store/html/_aruhaz_widgetek_sidebar.php','hu','6d9bab87057c796f0dd07d9b5d4d8b54');
INSERT INTO `bow_fordito` VALUES ('106','Alkategóriák','temak/b-store/html/_aruhaz_widgetek_sidebar.php','hu','a25bbaec77a0ae5b316dc97c15606f8f');
INSERT INTO `bow_fordito` VALUES ('107','KATETGÓRIÁK','temak/b-store/html/_aruhaz_widgetek_sidebar.php','hu','328db0a0b10a8d46bdc8dcf01cf04c31');
INSERT INTO `bow_fordito` VALUES ('108','Telepítés','modulok/_modulok/_modulokkomp.php','hu','02a996b236a8714e3fcc09392b82c1e8');
INSERT INTO `bow_fordito` VALUES ('109','Adja meg e-mail címét','osztaly/osztaly_html.php(337) : eval()\'d code','hu','af252fdcd51a5da629023fafcc7b9586');
INSERT INTO `bow_fordito` VALUES ('110','Jelszó','osztaly/osztaly_html.php(337) : eval()\'d code','hu','3d0d0a868d2a76986874244d909cab59');
INSERT INTO `bow_fordito` VALUES ('111','Elvelejtette jelszavát?','osztaly/osztaly_html.php(337) : eval()\'d code','hu','69f34666ce2afe2eb2cc18771cc31f30');
INSERT INTO `bow_fordito` VALUES ('112','Adja meg e-mail címét és jelszavát','osztaly/osztaly_html.php(337) : eval()\'d code','hu','727776e2d60ee5543391c30b5809e4ad');
INSERT INTO `bow_fordito` VALUES ('113','Jelszó','osztaly/osztaly_html.php(337) : eval()\'d code','hu','58d27b8466a9fa32307eb7c1c71e8356');
INSERT INTO `bow_fordito` VALUES ('114','Adja meg e-mail címét, hogy elküldhessük a visszaállító kódot!','osztaly/osztaly_html.php(337) : eval()\'d code','hu','8fd2b99a49c31b15545b0c4e0a9ad9cc');
INSERT INTO `bow_fordito` VALUES ('115','Vissza','osztaly/osztaly_html.php(337) : eval()\'d code','hu','0fcc8a4cf12d728c8e92f54a248f3cae');
INSERT INTO `bow_fordito` VALUES ('116','Elfelejtett jelszó','osztaly/osztaly_html.php(337) : eval()\'d code','hu','4eeba915a71be3ab89223d18854e83e9');
INSERT INTO `bow_fordito` VALUES ('117','Elküldés','osztaly/osztaly_html.php(337) : eval()\'d code','hu','e40cc75b0faccc6ebc3a5173f7e00970');
INSERT INTO `bow_fordito` VALUES ('118','Hibás e-mail vagy jelszó','modulok/unicorn/bow_beepulok.php','hu','49efbe0772cb4eb6e751da5da4d1555e');
INSERT INTO `bow_fordito` VALUES ('119','E-mail visszaállító linket elküldtük e-mail címére','modulok/unicorn/bow_beepulok.php','hu','67b8b3cc5691f8c06500e97942bf62bf');
INSERT INTO `bow_fordito` VALUES ('120','Ismeretlen e-mail cím!','modulok/unicorn/bow_beepulok.php','hu','d65d0cbbd66561f468c39b9678ea939b');
INSERT INTO `bow_fordito` VALUES ('121','Addja meg új jelszavát!','osztaly/osztaly_html.php(337) : eval()\'d code','hu','5ff131f234fe38dfb05e2bcc2fc8346c');
INSERT INTO `bow_fordito` VALUES ('122','Új jelszó','osztaly/osztaly_html.php(337) : eval()\'d code','hu','4210f9c6f50945c6b80272590b934686');
INSERT INTO `bow_fordito` VALUES ('123','Belépés','osztaly/osztaly_html.php(337) : eval()\'d code','hu','16b7eeaae5ca5bd523b4f8edf398f420');
INSERT INTO `bow_fordito` VALUES ('124','Módosítás','osztaly/osztaly_html.php(337) : eval()\'d code','hu','eaf259b03d75e20c800f88d3f5fc2eca');
INSERT INTO `bow_fordito` VALUES ('125','Adminisztráció','osztaly/osztaly_html.php(337) : eval()\'d code','hu','5b5f470ff2d9e54473ca2f7bc88fee14');
INSERT INTO `bow_fordito` VALUES ('126','Vezérlőpult','osztaly/osztaly_html.php(337) : eval()\'d code','hu','568a5647d438d9f2c48833b5ea621b57');
INSERT INTO `bow_fordito` VALUES ('127','Oldalépítő','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','a4ff1be2925b3566dc628c448b015ba2');
INSERT INTO `bow_fordito` VALUES ('128','Szövegcimkék','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','8fb02215e424b33523783f266d7c28b6');
INSERT INTO `bow_fordito` VALUES ('129','Vezérlőpult','modulok/unicorn/osztaly_unicorn_futtato.php','hu','46f4ec9e41347addb21e1b8ec0e7edf3');
INSERT INTO `bow_fordito` VALUES ('130','Szövegcímkék','modulok/cimkek/init.php','hu','c5b1c7a0d912e602bdf4b3e22a2327f5');
INSERT INTO `bow_fordito` VALUES ('131','Modul beállító függvény nem található','modulok/unicorn/osztaly_unicorn_futtato.php','hu','7b68c71a690c779d46634b800aea2af6');
INSERT INTO `bow_fordito` VALUES ('132','Létrehozott oldalak listája','modulok/_oldalak/unicornoldalepito.php','hu','a4ac556639b03bfc2bd0a5b7c82122c2');
INSERT INTO `bow_fordito` VALUES ('133','Biztos vagy benne?','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','77875dfb6bc9083c4b8facea0dce3038');
INSERT INTO `bow_fordito` VALUES ('134','Kijelöltek törlése','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','e96041fee48c44812619f65d9ca2cb08');
INSERT INTO `bow_fordito` VALUES ('135','Keresés','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','0bdac2cd570ac320e7d194665b55c4c0');
INSERT INTO `bow_fordito` VALUES ('136','Adminisztráció','osztaly/osztaly_html.php(351) : eval()\'d code','hu','82236b5009cd88b3c2046f70571826d0');
INSERT INTO `bow_fordito` VALUES ('137','Vezérlőpult','osztaly/osztaly_html.php(351) : eval()\'d code','hu','8d25a4be0e059bfa4f8813302796bbd5');
INSERT INTO `bow_fordito` VALUES ('138','Adminisztráció','osztaly/osztaly_html.php(352) : eval()\'d code','hu','73f0e3a7b41b98c297a27c29d507412c');
INSERT INTO `bow_fordito` VALUES ('139','Vezérlőpult','osztaly/osztaly_html.php(352) : eval()\'d code','hu','5b36abb67adacf0aea5a4018f90bebc4');
INSERT INTO `bow_fordito` VALUES ('140','Törlés sikeres','modulok/_oldalak/unicornoldalepito.php','hu','6101990880d8936bde5fc6ac36b22236');
INSERT INTO `bow_fordito` VALUES ('141','A kijelölt elemeket eltávolítottam!','modulok/_oldalak/unicornoldalepito.php','hu','b66ecdc9179c582abc88b8d0857ee377');
INSERT INTO `bow_fordito` VALUES ('142','Hiba','modulok/_oldalak/unicornoldalepito.php','hu','96722d2972d8a8b1d119834ab6e57f98');
INSERT INTO `bow_fordito` VALUES ('143','Az adatsor nem található!','modulok/_oldalak/unicornoldalepito.php','hu','86fb253878aa45988e01e81c530b0601');
INSERT INTO `bow_fordito` VALUES ('144','Típus nem támogatott','modulok/unicorn/osztaly_unicorn_futtato.php','hu','46ac937de9e194f1a6e92ebe19a7d9ef');
INSERT INTO `bow_fordito` VALUES ('145','Oldal címe','modulok/_oldalak/unicornoldalepito.php','hu','c13e1f0b6621cc80c9dacce497cef2d6');
INSERT INTO `bow_fordito` VALUES ('146','Adminisztrációs felületen ezen a néven találod meg','modulok/_oldalak/unicornoldalepito.php','hu','c465af5d4c09d6185fc8699e601fc025');
INSERT INTO `bow_fordito` VALUES ('147','SEO kulcsszavak','modulok/_oldalak/unicornoldalepito.php','hu','afb7c18480eb1cd60b5e0e5959997b9d');
INSERT INTO `bow_fordito` VALUES ('148','Kulcsszavak a keresők számára','modulok/_oldalak/unicornoldalepito.php','hu','73c99763da991c8e3e68934aca69ce71');
INSERT INTO `bow_fordito` VALUES ('149','SEO leírás','modulok/_oldalak/unicornoldalepito.php','hu','63bb60d0d5aa42ef4a059fb450ff7ef7');
INSERT INTO `bow_fordito` VALUES ('150','Rövid leírás keresők számára','modulok/_oldalak/unicornoldalepito.php','hu','55169d9df1e1d2044fbf0074761bbf4c');
INSERT INTO `bow_fordito` VALUES ('151','Meta adatok működése','modulok/_oldalak/unicornoldalepito.php','hu','6e49911143b417497957eeff546c1b8d');
INSERT INTO `bow_fordito` VALUES ('152','Hogyan működjenek a meta adatok','modulok/_oldalak/unicornoldalepito.php','hu','6182f50b2aeccefb542818412adb80fa');
INSERT INTO `bow_fordito` VALUES ('153','Nyelv','modulok/_oldalak/unicornoldalepito.php','hu','51224e230346ac905c950d65cd54e953');
INSERT INTO `bow_fordito` VALUES ('154','Ennek az oldalnak a nyelve','modulok/_oldalak/unicornoldalepito.php','hu','78ecd4839523b26b97e875afb3fa7610');
INSERT INTO `bow_fordito` VALUES ('155','Az oldal témasablonja','modulok/_oldalak/unicornoldalepito.php','hu','4d635cc43ee4a08ee7be6990e43f8399');
INSERT INTO `bow_fordito` VALUES ('156','Egy-egy oldalhoz eltérő téma is megadható','modulok/_oldalak/unicornoldalepito.php','hu','28bbd94c5a2bb22013b4ea8f4df8e8c0');
INSERT INTO `bow_fordito` VALUES ('157','Az oldal URL-je','modulok/_oldalak/unicornoldalepito.php','hu','31e10fb2b189e9b42ccd513a782749e1');
INSERT INTO `bow_fordito` VALUES ('158','Milyen url alatt lesz elérhető az oldal','modulok/_oldalak/unicornoldalepito.php','hu','b5a7afceef967b822d975ca26acf3751');
INSERT INTO `bow_fordito` VALUES ('159','Speciális működésű oldal','modulok/_oldalak/unicornoldalepito.php','hu','1e2bb893476441724ca975876f2610da');
INSERT INTO `bow_fordito` VALUES ('160','Itt lehet beállítani speciális rendeltetésű oldalakat','modulok/_oldalak/unicornoldalepito.php','hu','9b9af7ed7751accab59fd4e54c67baa9');
INSERT INTO `bow_fordito` VALUES ('161','Oldal szerkesztése','modulok/_oldalak/unicornoldalepito.php','hu','3067caf3de025187e73078a7ad582297');
INSERT INTO `bow_fordito` VALUES ('162','Weboldalak szerkesztése','modulok/_oldalak/unicornoldalepito.php','hu','eb0ad5e9d71243b996e01ad184aa4e25');
INSERT INTO `bow_fordito` VALUES ('163','Mentés','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','b39ec41c49865ad01bb52f3ccce97a70');
INSERT INTO `bow_fordito` VALUES ('164','Hozzá illesztjük az alap meta tagokhoz','modulok/_oldalak/unicornoldalepito.php','hu','b895ec147596e07aa17e303bca297e98');
INSERT INTO `bow_fordito` VALUES ('165','Felülírjuk az általános beállítást','modulok/_oldalak/unicornoldalepito.php','hu','e2e34fdcd3831b5cfdf893f93316d395');
INSERT INTO `bow_fordito` VALUES ('166','Főoldal','osztaly/osztaly_html.php(352) : eval()\'d code','hu','348cabca66c7297907632712f3ec61af');
INSERT INTO `bow_fordito` VALUES ('167','Alapértelmezett','modulok/_oldalak/unicornoldalepito.php','hu','9542f6e7cb6cdf403b64f4d190e5571c');
INSERT INTO `bow_fordito` VALUES ('168','Csak ékezet nélküli ASCII karakterek használhatóak','modulok/_oldalak/unicornoldalepito.php','hu','26da276fea9dab353fae6668f27b083f');
INSERT INTO `bow_fordito` VALUES ('169','Nem speciális oldal','modulok/_oldalak/unicornoldalepito.php','hu','594027673a0b8c3cb69448ba8bc2fb24');
INSERT INTO `bow_fordito` VALUES ('170','Főoldal','modulok/_oldalak/unicornoldalepito.php','hu','3bff647c96ca705c1a48814c03830557');
INSERT INTO `bow_fordito` VALUES ('171','404-es hibaoldal','modulok/_oldalak/unicornoldalepito.php','hu','1605f3a6047220a59fe5a3c97665571d');
INSERT INTO `bow_fordito` VALUES ('172','Nincs hozzáférés hibaoldal','modulok/_oldalak/unicornoldalepito.php','hu','69ed68e00a1c03c29724c675d4c616cc');
INSERT INTO `bow_fordito` VALUES ('173','Mégsem','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','0e599f0022ff5fa6738ed7d5eac40709');
INSERT INTO `bow_fordito` VALUES ('174','Mentés sikeres','modulok/_oldalak/unicornoldalepito.php','hu','45336f72e4a76cb21ab6985dd70d08c0');
INSERT INTO `bow_fordito` VALUES ('175','Korlátozás','modulok/_oldalak/unicornoldalepito.php','hu','a56cb4043735e4867a641b39d588a50d');
INSERT INTO `bow_fordito` VALUES ('176','Oldal megjelenítésének szabályozása a felhasználói szintek alapján','modulok/_oldalak/unicornoldalepito.php','hu','fef3466d464b266e852c012c423a36cd');
INSERT INTO `bow_fordito` VALUES ('177','Nincs kiválasztva semmi','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','05440a3b0bd5a1218e01d0b7538e3ea2');
INSERT INTO `bow_fordito` VALUES ('178','Oldalak listája','modulok/_oldalak/unicornoldalepito.php','hu','dfbdfbf6fae1bb1a9dfa43769e0c48b4');
INSERT INTO `bow_fordito` VALUES ('179','Megjelenített modulok listája','modulok/_oldalak/unicornoldalepito.php','hu','74c7551265a72841e388b0c404f06b5f');
INSERT INTO `bow_fordito` VALUES ('180','Megjelenített tartalmak listája','modulok/_oldalak/unicornoldalepito.php','hu','c5588b47423888c05e24ae3fa1cb8f54');
INSERT INTO `bow_fordito` VALUES ('181','Oldal neve','modulok/_oldalak/unicornoldalepito.php','hu','9704263536368b4432e15de9c0252afd');
INSERT INTO `bow_fordito` VALUES ('182','Kulcsszavak','modulok/_oldalak/unicornoldalepito.php','hu','5286b0902b8b92d0bf2e3bf9690c1b5f');
INSERT INTO `bow_fordito` VALUES ('183','Új oldal hozzáadása','modulok/_oldalak/unicornoldalepito.php','hu','67df727617c94b2ad2f3b648214531e6');
INSERT INTO `bow_fordito` VALUES ('184','Tartalom hozzáadása az oldalhoz','modulok/_oldalak/unicornoldalepito.php','hu','bd79b933a7b49113375495661b8d4fc0');
INSERT INTO `bow_fordito` VALUES ('185','Hibás paramterek','modulok/_oldalak/unicornoldalepito.php','hu','9e3831d5e8ff667029af7f99f6af568e');
INSERT INTO `bow_fordito` VALUES ('186','Modul nincs telepítve','modulok/_oldalak/unicornoldalepito.php','hu','015a5287ae39c06abac58174362021b2');
INSERT INTO `bow_fordito` VALUES ('187','Modul futtató függvény nem található','modulok/_oldalak/unicornoldalepito.php','hu','6f967cc1fee3bc4082ee74d5f3378143');
INSERT INTO `bow_fordito` VALUES ('188','Beállító függvény hiányzik','modulok/_oldalak/unicornoldalepito.php','hu','4a7087c24aa9a7da0ebc320b94f39d54');
INSERT INTO `bow_fordito` VALUES ('189','Beállító függvény hiányzik','modulok/_oldalak/unicornoldalepito.php','hu','5e0c1a9289a0f8ec68c58b853df5664e');
INSERT INTO `bow_fordito` VALUES ('190','Sz','modulok/_szovegek/init.php','hu','873718c7e8a0355ac5311cf2af4443e9');
INSERT INTO `bow_fordito` VALUES ('191','Szöveges tartalom szerkesztése','modulok/_szovegek/init.php','hu','58732cc47939b74bb8e5e540b16eae3b');
INSERT INTO `bow_fordito` VALUES ('192','Létező szöveg kiválasztása','modulok/_szovegek/init.php','hu','965c0372d042f910079340ef9cb3b8ce');
INSERT INTO `bow_fordito` VALUES ('193','Korábban létrehozott szöveg hozzáadásához válassz ki egyet, és kattints a mentés gombra!','modulok/_szovegek/init.php','hu','71d03c5d43c36728c640ee494263ffe4');
INSERT INTO `bow_fordito` VALUES ('194','Nem kérem, újat készítek','modulok/_szovegek/init.php','hu','42f5e3bb83d26cf9add2132ded50d015');
INSERT INTO `bow_fordito` VALUES ('195','Korábban elkészített szöveg hozzáadása','modulok/_szovegek/init.php','hu','5f1600502a93dcfebbebf8d4e21a0686');
INSERT INTO `bow_fordito` VALUES ('196','Cím','modulok/_szovegek/init.php','hu','493b59fa3489a3a20d11f40a816f36ca');
INSERT INTO `bow_fordito` VALUES ('197','Bevezető','modulok/_szovegek/init.php','hu','d76f45eca4a58bab4ea79bfac11b04e9');
INSERT INTO `bow_fordito` VALUES ('198','Tartalom','modulok/_szovegek/init.php','hu','abe9b144fefbc66ac356e93a27ec48de');
INSERT INTO `bow_fordito` VALUES ('199','Cikkgaléria nem elérhető','beepulok/_szoveg_galeria.php','hu','38671a6ae94ace616c149c69e7383703');
INSERT INTO `bow_fordito` VALUES ('200','<strong>Kérlek, mentsd el először a szöveget, majd utána nyisd meg újra és hozzáadhatsz galériát is</strong>','beepulok/_szoveg_galeria.php','hu','e40dbd5dee09fa67b0897a6aaeb7d9e9');
INSERT INTO `bow_fordito` VALUES ('201','Sikeres tartalomfelvitel','modulok/_oldalak/unicornoldalepito.php','hu','1b985eacaff5a8de9de4381ff2706603');
INSERT INTO `bow_fordito` VALUES ('202','Új tartalom hozzáadásra került','modulok/_oldalak/unicornoldalepito.php','hu','72b41ee7613c58d705ededfa33b68a9c');
INSERT INTO `bow_fordito` VALUES ('203','Tartalmak','modulok/_oldalak/unicornoldalepito.php','hu','a9d41be8635571bff62b7da95359b13f');
INSERT INTO `bow_fordito` VALUES ('204','Tartalom neve','modulok/_oldalak/unicornoldalepito.php','hu','2778fc55f54c205147412a63ffcff8ed');
INSERT INTO `bow_fordito` VALUES ('205','Szerkesztés','modulok/_oldalak/unicornoldalepito.php','hu','fede51ac3513c0baff5a92b9a4ff5922');
INSERT INTO `bow_fordito` VALUES ('206','Korlátozás','modulok/_oldalak/unicornoldalepito.php','hu','139b2074caa9f34cebf7f925f09ea958');
INSERT INTO `bow_fordito` VALUES ('207','Törlés','modulok/_oldalak/unicornoldalepito.php','hu','a1ef1166e2f763974eaab2c9d61e97cd');
INSERT INTO `bow_fordito` VALUES ('208','Nincs beállítási lehetőség','modulok/_oldalak/unicornoldalepito.php','hu','1576ea8109b027f826dbac0dbd464ffb');
INSERT INTO `bow_fordito` VALUES ('209','Ennél a tartalomnál nincs beállítási lehetőség','modulok/_oldalak/unicornoldalepito.php','hu','90abb5f49373973c1b1a13d07b0823fc');
INSERT INTO `bow_fordito` VALUES ('210','Sikeres tartalom módosítás','modulok/_oldalak/unicornoldalepito.php','hu','ba430b4a2d47c30b64f598844c5f3565');
INSERT INTO `bow_fordito` VALUES ('211','Tartalom módosítása sikerült','modulok/_oldalak/unicornoldalepito.php','hu','f98a7bfd5c018c8abd21b920831123f9');
INSERT INTO `bow_fordito` VALUES ('212','Korlátozások','modulok/_oldalak/unicornoldalepito.php','hu','3eb53b731d03eb0639c19676dbb63756');
INSERT INTO `bow_fordito` VALUES ('213','Sorrend','modulok/_oldalak/unicornoldalepito.php','hu','32a29045abe510130e16daf7fdd6bf94');
INSERT INTO `bow_fordito` VALUES ('214','Sorrend mentése','modulok/_oldalak/unicornoldalepito.php','hu','f74f7bcee77656af252a472c1b200295');
INSERT INTO `bow_fordito` VALUES ('215','Adminisztráció','osztaly/osztaly_html.php(355) : eval()\'d code','hu','3d2ed0eaa71434ef1ceca1a49ecd6362');
INSERT INTO `bow_fordito` VALUES ('216','Vezérlőpult','osztaly/osztaly_html.php(355) : eval()\'d code','hu','65716b34fbbac8ddd499e7cb71d88fd1');
INSERT INTO `bow_fordito` VALUES ('217','Főoldal','osztaly/osztaly_html.php(355) : eval()\'d code','hu','62ac83c2c38b70bdcf470183f9efd8e4');
INSERT INTO `bow_fordito` VALUES ('218','Sorrend mentés','modulok/_oldalak/unicornoldalepito.php','hu','b4dcd289b4eab5667e16e8faf58aaf78');
INSERT INTO `bow_fordito` VALUES ('219','Sorrend mentése sikeres','modulok/_oldalak/unicornoldalepito.php','hu','002c4a18b62f312d24a33ad5fe368a41');
INSERT INTO `bow_fordito` VALUES ('220','Csoportok beállítása','modulok/_oldalak/unicornoldalepito.php','hu','304f8ad4ec4e5315f3f4237fd225cf68');
INSERT INTO `bow_fordito` VALUES ('221','Az itt beállított csoportok fogják látni a tartalmat. Ha nincs senki beállítva, akkor mindenki láthatja.','modulok/_oldalak/unicornoldalepito.php','hu','0138805b427385f08763aa25032fbaef');
INSERT INTO `bow_fordito` VALUES ('222','Válaszd ki, kik láthatják a tartalmakat, vagy ne válassz ki semmit, így mindenki láthatja','modulok/_oldalak/unicornoldalepito.php','hu','b43bba72da0b03d99ddb5ce095db04ef');
INSERT INTO `bow_fordito` VALUES ('223','Jogosultság mentés kész','modulok/_oldalak/unicornoldalepito.php','hu','71e6b5cb1acb626731ea5bdc948c9360');
INSERT INTO `bow_fordito` VALUES ('224','A tartalom csoporthoz rendelése sikeres','modulok/_oldalak/unicornoldalepito.php','hu','5984b5629fcc001572be3185621dcdee');
INSERT INTO `bow_fordito` VALUES ('225','Szövegszerkesztő','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','6ce64df4888771ff74c0973e9d676f5d');
INSERT INTO `bow_fordito` VALUES ('226','Szövegszerkesztő','modulok/_szerkeszto/init.php','hu','19e3aed2f660d284b1f6ae0fcd12fd11');
INSERT INTO `bow_fordito` VALUES ('227','Szövegek','modulok/_szerkeszto/unicornposteditor.php','hu','70cc6699201870c6a4a95ccec11aea8b');
INSERT INTO `bow_fordito` VALUES ('228','Tartalom neve','modulok/_szerkeszto/unicornposteditor.php','hu','de6b0345e217260931b7b6247d8e7e5b');
INSERT INTO `bow_fordito` VALUES ('229','Sorrend','modulok/_szerkeszto/unicornposteditor.php','hu','48184379c277fc422000c540cbf87110');
INSERT INTO `bow_fordito` VALUES ('230','Szerkesztés','modulok/_szerkeszto/unicornposteditor.php','hu','1aabe0b65eb55d53476fb4d5f3512178');
INSERT INTO `bow_fordito` VALUES ('231','Törlés','modulok/_szerkeszto/unicornposteditor.php','hu','64d83e80535935dca33b6571cc20d6c4');
INSERT INTO `bow_fordito` VALUES ('232','Új bejegyzés felvitele','modulok/_szerkeszto/unicornposteditor.php','hu','ce011bd5e6cd46286cde8f759c919819');
INSERT INTO `bow_fordito` VALUES ('233','Szűrés szövegcimkére: ','modulok/_szerkeszto/unicornposteditor.php','hu','2eba19092978954496fba1105354f3dd');
INSERT INTO `bow_fordito` VALUES ('234','Nincs kiválasztva cimke','modulok/_szerkeszto/unicornposteditor.php','hu','4a6688ac81dcbb1557e92b5bf77a49b1');
INSERT INTO `bow_fordito` VALUES ('235','Törlés sikeres','modulok/_szerkeszto/unicornposteditor.php','hu','26de36643787ceb4cf09826baff80dc0');
INSERT INTO `bow_fordito` VALUES ('236','A kijelölt elemeket eltávolítottam!','modulok/_szerkeszto/unicornposteditor.php','hu','d1d6a99d9118db493082c20fed1a6722');
INSERT INTO `bow_fordito` VALUES ('237','Szöveges tartalom szerkesztése','modulok/_szerkeszto/unicornposteditor.php','hu','8987bbcbb78aa540c3249f9dfb43cb25');
INSERT INTO `bow_fordito` VALUES ('238','Cím','modulok/_szerkeszto/unicornposteditor.php','hu','beb9976a9dc018fe20cfcffe1dcd5d92');
INSERT INTO `bow_fordito` VALUES ('239','Bevezető','modulok/_szerkeszto/unicornposteditor.php','hu','84a55c57df564210b3976cc93bad50ed');
INSERT INTO `bow_fordito` VALUES ('240','Tartalom','modulok/_szerkeszto/unicornposteditor.php','hu','65ba4ad967c01cdc516adc24a6261097');
INSERT INTO `bow_fordito` VALUES ('241','Szöveg mentése sikeres','modulok/_szerkeszto/unicornposteditor.php','hu','5127ed5da82da38ab45afe5f7ed81e2f');
INSERT INTO `bow_fordito` VALUES ('242','Cimkék','modulok/_szerkeszto/unicornposteditor.php','hu','ad28e04db0cc5536fc7fbb6e8c656642');
INSERT INTO `bow_fordito` VALUES ('243','Sorrend mentése','modulok/_szerkeszto/unicornposteditor.php','hu','2d7a1d61ba340d29dcfe765d685ff965');
INSERT INTO `bow_fordito` VALUES ('244','File feltöltése','modulok/cikkgaleria/init.php','hu','13447eabd4a0e9eae33a1cee00d63e12');
INSERT INTO `bow_fordito` VALUES ('245','Mentés után hozzáadhat galériát','modulok/cikkgaleria/init.php','hu','e534dd764ec0b13e27f8620a48deb5fa');
INSERT INTO `bow_fordito` VALUES ('246','Galéria plugin','modulok/cikkgaleria/init.php','hu','f4cc3e26e84027ba03bc421288d05514');
INSERT INTO `bow_fordito` VALUES ('247','Feltölt','modulok/cikkgaleria/init.php','hu','99ff9dc44b8251cd61737c6d9fdfade2');
INSERT INTO `bow_fordito` VALUES ('248','Módosítás','modulok/cikkgaleria/lista.php','hu','886104bce9f06144d82a2c9626cd63e3');
INSERT INTO `bow_fordito` VALUES ('249','Törlés','modulok/cikkgaleria/lista.php','hu','361e08fd77735c3724ba24bdc578911c');
INSERT INTO `bow_fordito` VALUES ('250','A főkép a cikk mentése után lesz beállítva.','modulok/cikkgaleria/lista.php','hu','c7c7a71121ba8dd887b792f535713e94');
INSERT INTO `bow_fordito` VALUES ('251','Legyen ez a főkép','modulok/cikkgaleria/lista.php','hu','f17d582fe602ed10c9ad885083e768ec');
INSERT INTO `bow_fordito` VALUES ('252','Shortcode beillesztés','modulok/shortcode/init.php','hu','00a71b61f290a2f59c6af1e85cd06d8e');
INSERT INTO `bow_fordito` VALUES ('253','Elérhető shortcode-ok','modulok/shortcode/init.php','hu','9c65acab128c1378cff0e6218cf665a4');
INSERT INTO `bow_fordito` VALUES ('254','Vissza','modulok/shortcode/shortcode_handler.php','hu','2ce0adb0b7517ebe6120971db3b0514d');
INSERT INTO `bow_fordito` VALUES ('255','Nem található galéria ehhez a szöveghez','modulok/cikkgaleria/init.php','hu','59971f7697f50799f41d6620de31d675');
INSERT INTO `bow_fordito` VALUES ('256','Igazítás: középre','modulok/cikkgaleria/init.php','hu','37225ae3af8a2993dd2a8afaa9e474ba');
INSERT INTO `bow_fordito` VALUES ('257','Igazítás: balra','modulok/cikkgaleria/init.php','hu','47b7c3ed65d2f0ae0d9e6e84ba535183');
INSERT INTO `bow_fordito` VALUES ('258','Igazítás: jobbra','modulok/cikkgaleria/init.php','hu','7402c22f35a9143ed698e597faaf798f');
INSERT INTO `bow_fordito` VALUES ('259','Nem nyit galériát','modulok/cikkgaleria/init.php','hu','d794af74812746b55c524acdb98bac3f');
INSERT INTO `bow_fordito` VALUES ('260','Galéria megnyitása kattintásra','modulok/cikkgaleria/init.php','hu','b9e4eb38bd8bb2e33845e8516f2b27b5');
INSERT INTO `bow_fordito` VALUES ('261','Beillesztés','modulok/cikkgaleria/init.php','hu','e256cd3aa685dcfe010a598d6dd71019');
INSERT INTO `bow_fordito` VALUES ('262','Betőltés folyamatban!','modulok/shortcode/init.php','hu','7313c071f47dd4a75450ea36633f5091');
INSERT INTO `bow_fordito` VALUES ('263','Cikk galéria komponens','modulok/cikkgaleria/init.php','hu','9d55b0a2053d0c7676b26df281003b7b');
INSERT INTO `bow_fordito` VALUES ('264','A HTML szerkesztővel használható','modulok/shortcode/init.php','hu','fd0e1e2e71958406b150481e4b0d996d');
INSERT INTO `bow_fordito` VALUES ('265','Modul beállító nem található','modulok/unicorn/osztaly_unicorn_futtato.php','hu','b9adf0b8d1f0ac7074607f94092158a5');
INSERT INTO `bow_fordito` VALUES ('266','Menüszerkesztő','modulok/_menu/init.php','hu','a8c73467ef1e0d3370eadaeb5471edf1');
INSERT INTO `bow_fordito` VALUES ('267','Menüszerkesztő','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','c824873893bdedf393b649b32d045a7e');
INSERT INTO `bow_fordito` VALUES ('268','Menücsoport kiválaszta','modulok/_menu/_menu.php','hu','086a6349e0e01453b423d0b340829a80');
INSERT INTO `bow_fordito` VALUES ('269','Új menüpont','modulok/_menu/_menu.php','hu','d003ccfdbfea1d2b557913836492f468');
INSERT INTO `bow_fordito` VALUES ('270','Új menüpont hozzáadása','modulok/_menu/ajaxmenueditor.php','hu','0a5d7a2ba6b456ad7a327b9f75a2369a');
INSERT INTO `bow_fordito` VALUES ('271','Frissítés','modulok/_menu/ajaxmenueditor.php','hu','4532fe3896c74005c9f2a6673ee63551');
INSERT INTO `bow_fordito` VALUES ('272','Menü felirat','modulok/_menu/ajaxmenueditor.php','hu','24c4e76918eaa34bb146b74ce80cc99d');
INSERT INTO `bow_fordito` VALUES ('273','Nyelv','modulok/_menu/ajaxmenueditor.php','hu','9daa10b39e75c5689f67110acf178e7a');
INSERT INTO `bow_fordito` VALUES ('274','Típus és link','modulok/_menu/ajaxmenueditor.php','hu','857aaf3c15d3c5fbd1dfd7335f110928');
INSERT INTO `bow_fordito` VALUES ('275','Oldal link','modulok/_menu/ajaxmenueditor.php','hu','d288e69284071548cd7769da68ea6016');
INSERT INTO `bow_fordito` VALUES ('276','Külső link','modulok/_menu/ajaxmenueditor.php','hu','bd3c0f1f49baf9f3382759c11ee48442');
INSERT INTO `bow_fordito` VALUES ('277','Komponens oldal','modulok/_menu/ajaxmenueditor.php','hu','aa189c25fcd47c9b1257a547ba57d2ea');
INSERT INTO `bow_fordito` VALUES ('278','Válassz az elkészült oldalak közül','modulok/_menu/ajaxmenueditor.php','hu','5fce5534b85716a07ffe924bb6e062f2');
INSERT INTO `bow_fordito` VALUES ('279','Mentés','modulok/_menu/ajaxmenueditor.php','hu','77eee5592b79dfded223085103177c0b');
INSERT INTO `bow_fordito` VALUES ('280','Add meg a linket (http://example.com/site)','modulok/_menu/ajaxmenueditor.php','hu','30f7912eee139588d923c72c1f325b98');
INSERT INTO `bow_fordito` VALUES ('281','Rendelkezésre álló komponensek','modulok/_menu/ajaxmenueditor.php','hu','7ee8ccbf72aac9903bf3b6feba932dd6');
INSERT INTO `bow_fordito` VALUES ('282','..::betöltés::..','modulok/_menu/ajaxmenueditor.php','hu','d3f97280cd37b7c63764ad5c1b9f4725');
INSERT INTO `bow_fordito` VALUES ('283','Mozgass a helyemre...','modulok/_menu/ajaxmenueditor.php','hu','b6a4d7e3510fd9fa8d6655139d33f853');
INSERT INTO `bow_fordito` VALUES ('284','Betöltés','modulok/_menu/ajaxmenueditor.php','hu','5de099b1e3ff6ef48f50abac07bc01ed');
INSERT INTO `bow_fordito` VALUES ('285','Biztos vagy benne?','modulok/_menu/ajaxmenueditor.php','hu','cbee689385eb708c0b137929f1e08528');
INSERT INTO `bow_fordito` VALUES ('286','Menücsoport kiválasztása','modulok/_menu/ajaxmenueditor.php','hu','1e37d235f6a569f05429e23dfffa047a');
INSERT INTO `bow_fordito` VALUES ('287','Új menücsoport','modulok/_menu/ajaxmenueditor.php','hu','30bdfe0c4b3cd37915440263493846ac');
INSERT INTO `bow_fordito` VALUES ('288','Menüpont szerkesztése','modulok/_menu/ajaxmenueditor.php','hu','4d52c557435d0bf3bc2285c75403d18a');
INSERT INTO `bow_fordito` VALUES ('289','Menü csoport','modulok/_menu/ajaxmenueditor.php','hu','046b66241b9f3732f7569b0cb27dc0cd');
INSERT INTO `bow_fordito` VALUES ('290','Menüpont felirat','modulok/_menu/ajaxmenueditor.php','hu','afbc801421be30b8c0a6366fd5e5e8d9');
INSERT INTO `bow_fordito` VALUES ('291','Kik láthatják? (hagyd üresen, ha mindenki láthatja)','modulok/_menu/ajaxmenueditor.php','hu','12374d198a3ffba621dafb1c257f1a09');
INSERT INTO `bow_fordito` VALUES ('292','Cimke','modulok/cimkek/init.php','hu','b955edaff41fdbc7c188dad8dce1020f');
INSERT INTO `bow_fordito` VALUES ('293','Szerkesztés','modulok/cimkek/init.php','hu','2e9a5b0200c64e428993b1762fdff4a7');
INSERT INTO `bow_fordito` VALUES ('294','Törlés','modulok/cimkek/init.php','hu','deea323d797a3e88a860ee29ca1c5cb4');
INSERT INTO `bow_fordito` VALUES ('295','Törlés sikeres','modulok/cimkek/init.php','hu','1e9835f3b6c091641b8dd2d63aa7da30');
INSERT INTO `bow_fordito` VALUES ('296','A kijelölt elemeket eltávolítottam!','modulok/cimkek/init.php','hu','d2c5c2420e009629d1b464a7ad578724');
INSERT INTO `bow_fordito` VALUES ('297','Cimke szerkesztése','modulok/cimkek/init.php','hu','e3fbf40bbd9efbc46941d4133a856dbe');
INSERT INTO `bow_fordito` VALUES ('298','Leírás','modulok/cimkek/init.php','hu','c6a37371da811783bf14cc57e830e687');
INSERT INTO `bow_fordito` VALUES ('299','Feliratkozás (pl. rendszeres hírlevélhez)','modulok/cimkek/init.php','hu','d17cf88723388b8f2182467e826ed87c');
INSERT INTO `bow_fordito` VALUES ('300','Mentés sikeres','modulok/cimkek/init.php','hu','cf92644aba186ea4b913c8d7a568845c');
INSERT INTO `bow_fordito` VALUES ('301','Új cimke','modulok/cimkek/init.php','hu','1dd1ab740d82b4e50f9cfc5601af8a7d');
INSERT INTO `bow_fordito` VALUES ('302','Fordító','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','e14c9e4e85118f9a91e4d3fd3a29ab41');
INSERT INTO `bow_fordito` VALUES ('303','Fordító','modulok/translate/init.php','hu','b2d0df829ea2a200ca19ecb044d35744');
INSERT INTO `bow_fordito` VALUES ('304','Fordító tábla','modulok/translate/translatorunicorn.php','hu','1713f88bee3e48763cf124f6e0361424');
INSERT INTO `bow_fordito` VALUES ('305','No post gallery','beepulok/_szoveg_galeria.php','en','38671a6ae94ace616c149c69e7383703');
INSERT INTO `bow_fordito` VALUES ('306','Align: center','modulok/cikkgaleria/init.php','en','37225ae3af8a2993dd2a8afaa9e474ba');
INSERT INTO `bow_fordito` VALUES ('307','Align: left','modulok/cikkgaleria/init.php','en','47b7c3ed65d2f0ae0d9e6e84ba535183');
INSERT INTO `bow_fordito` VALUES ('308','Align: right','modulok/cikkgaleria/init.php','en','7402c22f35a9143ed698e597faaf798f');
INSERT INTO `bow_fordito` VALUES ('309','No open gallery','modulok/cikkgaleria/init.php','en','d794af74812746b55c524acdb98bac3f');
INSERT INTO `bow_fordito` VALUES ('310','Open gallery','modulok/cikkgaleria/init.php','en','b9e4eb38bd8bb2e33845e8516f2b27b5');
INSERT INTO `bow_fordito` VALUES ('311','CATEGORIES','temak/responsive/html/_aruhaz_widgetek_sidebar.php','en','e84c8aac75043a4cf79f6ae4e4c7a0dc');
INSERT INTO `bow_fordito` VALUES ('312','CATEGORIES','temak/b-store/html/_aruhaz_widgetek_sidebar.php','en','328db0a0b10a8d46bdc8dcf01cf04c31');
INSERT INTO `bow_fordito` VALUES ('313','Subcategories','temak/b-store/html/_aruhaz_widgetek_sidebar.php','en','a25bbaec77a0ae5b316dc97c15606f8f');
INSERT INTO `bow_fordito` VALUES ('314','CATEGORIES','temak/b-store/html/_aruhaz_widgetek_sidebar.php','en','6d9bab87057c796f0dd07d9b5d4d8b54');
INSERT INTO `bow_fordito` VALUES ('315','Main page','osztaly/osztaly_html.php(355) : eval()\'d code','en','62ac83c2c38b70bdcf470183f9efd8e4');
INSERT INTO `bow_fordito` VALUES ('316','Dashboard','osztaly/osztaly_html.php(355) : eval()\'d code','en','65716b34fbbac8ddd499e7cb71d88fd1');
INSERT INTO `bow_fordito` VALUES ('317','Administration','osztaly/osztaly_html.php(355) : eval()\'d code','en','3d2ed0eaa71434ef1ceca1a49ecd6362');
INSERT INTO `bow_fordito` VALUES ('318','Insert','modulok/cikkgaleria/init.php','en','e256cd3aa685dcfe010a598d6dd71019');
INSERT INTO `bow_fordito` VALUES ('319','Translator','modulok/translate/init.php','en','b2d0df829ea2a200ca19ecb044d35744');
INSERT INTO `bow_fordito` VALUES ('320','Translator table','modulok/translate/translatorunicorn.php','en','1713f88bee3e48763cf124f6e0361424');
INSERT INTO `bow_fordito` VALUES ('321','Menu Editor','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','c824873893bdedf393b649b32d045a7e');
INSERT INTO `bow_fordito` VALUES ('322','Translator','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','e14c9e4e85118f9a91e4d3fd3a29ab41');
INSERT INTO `bow_fordito` VALUES ('323','Page builder','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','a4ff1be2925b3566dc628c448b015ba2');
INSERT INTO `bow_fordito` VALUES ('324','Post tags','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','8fb02215e424b33523783f266d7c28b6');
INSERT INTO `bow_fordito` VALUES ('325','Save','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','b39ec41c49865ad01bb52f3ccce97a70');
INSERT INTO `bow_fordito` VALUES ('326','Menu editor','modulok/_menu/init.php','en','a8c73467ef1e0d3370eadaeb5471edf1');
INSERT INTO `bow_fordito` VALUES ('327','Select menu group','modulok/_menu/_menu.php','en','086a6349e0e01453b423d0b340829a80');
INSERT INTO `bow_fordito` VALUES ('328','New menu item','modulok/_menu/_menu.php','en','d003ccfdbfea1d2b557913836492f468');
INSERT INTO `bow_fordito` VALUES ('329','Install','modulok/_modulok/_modulokkomp.php','en','02a996b236a8714e3fcc09392b82c1e8');
INSERT INTO `bow_fordito` VALUES ('330','Menu editor','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','56764cde68cf7805dc8fe74fe1ea507b');
INSERT INTO `bow_fordito` VALUES ('331','Loading','modulok/shortcode/init.php','en','7313c071f47dd4a75450ea36633f5091');
INSERT INTO `bow_fordito` VALUES ('332','Insert shortcode','modulok/shortcode/init.php','en','00a71b61f290a2f59c6af1e85cd06d8e');
INSERT INTO `bow_fordito` VALUES ('333','Adja meg e-mail címét és jelszavát','osztaly/osztaly_html.php(355) : eval()\'d code','hu','164234880bd89bcf3a1c7fc87e9c7d7c');
INSERT INTO `bow_fordito` VALUES ('334','Jelszó','osztaly/osztaly_html.php(355) : eval()\'d code','hu','fb3e0cce837aa0416d0245f5ee548e9b');
INSERT INTO `bow_fordito` VALUES ('335','Elfelejtett jelszó','osztaly/osztaly_html.php(355) : eval()\'d code','hu','8334bfbdb6620e0f0b12df91feb0d28c');
INSERT INTO `bow_fordito` VALUES ('336','Adja meg e-mail címét, hogy elküldhessük a visszaállító kódot!','osztaly/osztaly_html.php(355) : eval()\'d code','hu','87272accb94dffdf3869ee4f16596e14');
INSERT INTO `bow_fordito` VALUES ('337','Vissza','osztaly/osztaly_html.php(355) : eval()\'d code','hu','fbfd4cf3f69e4f90f4d0bbf1c71c78e5');
INSERT INTO `bow_fordito` VALUES ('338','Elküldés','osztaly/osztaly_html.php(355) : eval()\'d code','hu','69d14574b132baa062fe71371fd9db42');
INSERT INTO `bow_fordito` VALUES ('339','Adja meg új jelszavát!','osztaly/osztaly_html.php(355) : eval()\'d code','hu','833376b453dc9d8899eeb5ef862ee607');
INSERT INTO `bow_fordito` VALUES ('340','Új jelszó','osztaly/osztaly_html.php(355) : eval()\'d code','hu','3c1f02a34714e848b68507c85a40df66');
INSERT INTO `bow_fordito` VALUES ('341','Módosítás','osztaly/osztaly_html.php(355) : eval()\'d code','hu','337f639ca25b3a1f9105a0d5be6e0f6a');
INSERT INTO `bow_fordito` VALUES ('342','Gallery plugin','modulok/cikkgaleria/init.php','en','f4cc3e26e84027ba03bc421288d05514');
INSERT INTO `bow_fordito` VALUES ('343','Tartalom hasábok','modulok/bootstrap/init.php','hu','46844928417be6e5763b352348406100');
INSERT INTO `bow_fordito` VALUES ('344','Beillesztés','modulok/bootstrap/init.php','hu','03829218085d99d736ea3ecfe7fffeff');
INSERT INTO `bow_fordito` VALUES ('345','Csak kép','modulok/cikkgaleria/init.php','hu','247063ca5bbffcd415ee7b935335ca0a');
INSERT INTO `bow_fordito` VALUES ('346','Felhasználók','modulok/fiokbeallitasok/init.php','hu','85fdfb40c07e0c7216e87bd5f8d74eb0');
INSERT INTO `bow_fordito` VALUES ('347','Felhasználók','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','f48f4a82c10fc6527e490a176f9cbca0');
INSERT INTO `bow_fordito` VALUES ('348','Szövegek','modulok/fiokbeallitasok/accounteditor.php','hu','66f96f800311ea55398e9f81c1e27f1a');
INSERT INTO `bow_fordito` VALUES ('349','Név','modulok/fiokbeallitasok/accounteditor.php','hu','04a5d0508b6326768456589451ab078f');
INSERT INTO `bow_fordito` VALUES ('350','Szerkesztés','modulok/fiokbeallitasok/accounteditor.php','hu','835b314463985b1f4630da7445d49d25');
INSERT INTO `bow_fordito` VALUES ('351','Törlés','modulok/fiokbeallitasok/accounteditor.php','hu','1a321fb46356986b16314dafb6b5c4ec');
INSERT INTO `bow_fordito` VALUES ('352','Törlés sikeres','modulok/fiokbeallitasok/accounteditor.php','hu','e602de77ab63c66a0243af0b5fed9a17');
INSERT INTO `bow_fordito` VALUES ('353','A kijelölt felhasználókat eltávolítottam!','modulok/fiokbeallitasok/accounteditor.php','hu','d0d89ad26304e1e379fb9d59093f7700');
INSERT INTO `bow_fordito` VALUES ('354','Felhasználó szerkesztése','modulok/fiokbeallitasok/accounteditor.php','hu','8aa83f887c1e286ea9666022c3dd4460');
INSERT INTO `bow_fordito` VALUES ('355','Jelszó','modulok/fiokbeallitasok/accounteditor.php','hu','fcac09b965d4c3092380653c0c8b8794');
INSERT INTO `bow_fordito` VALUES ('356','Hagyd üresen, ha nem szeretnéd módosítani','modulok/fiokbeallitasok/accounteditor.php','hu','7548b581e8d06fafaae861feea738c54');
INSERT INTO `bow_fordito` VALUES ('357','Felhasználói csoport','modulok/fiokbeallitasok/accounteditor.php','hu','10f5c9e694e8a11af8f491b2a9951fe1');
INSERT INTO `bow_fordito` VALUES ('358','Felhasználó mentés','modulok/fiokbeallitasok/accounteditor.php','hu','881e4171f1aa476d1a49afeefad65544');
INSERT INTO `bow_fordito` VALUES ('359','Az adatokat elmentettem.','modulok/fiokbeallitasok/accounteditor.php','hu','9cfb521a6cadf30a529dfaec6ac50bb3');
INSERT INTO `bow_fordito` VALUES ('360','Új felhasználó hozzáadása','modulok/fiokbeallitasok/accounteditor.php','hu','feddffabc3bd53560b310a495eb028ae');
INSERT INTO `bow_fordito` VALUES ('361','igen','modulok/fiokbeallitasok/accounteditor.php','hu','952a2f5a66edddda61ed481970b288fe');
INSERT INTO `bow_fordito` VALUES ('362','nem','modulok/fiokbeallitasok/accounteditor.php','hu','851c7c208b7c918953ddcb9d114b7e0e');
INSERT INTO `bow_fordito` VALUES ('363','Küldjünk levelet a felhasználónak a regisztrációról?','modulok/fiokbeallitasok/accounteditor.php','hu','9fcab6d8548cfb46fd8f55de75808189');
INSERT INTO `bow_fordito` VALUES ('364','Mentés után küldhetünk levelet a felhasználónak a regisztrációról. Levélsablon kulcsa','modulok/fiokbeallitasok/accounteditor.php','hu','651a99bac7733479d63c0ac25f9b2e2b');
INSERT INTO `bow_fordito` VALUES ('365','Regisztrációs levél','modulok/fiokbeallitasok/accounteditor.php','hu','88f6ff2eb3175c5e4baf91da81a928a1');
INSERT INTO `bow_fordito` VALUES ('366','Levél kiküldse folyamatban.','modulok/fiokbeallitasok/accounteditor.php','hu','b6d361d4a3d2d1cf3cb7f5c0de0f2ff5');
INSERT INTO `bow_fordito` VALUES ('367','Regisztrációs mezők','modulok/fiokbeallitasok/init.php','hu','a361d0cb456b43355dc0c48ec7d02032');
INSERT INTO `bow_fordito` VALUES ('368','Regisztrációs mezők','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','78c656d349f8e10e9d7fad70a7845b48');
INSERT INTO `bow_fordito` VALUES ('369','Regisztrációs mezők','modulok/fiokbeallitasok/formeditor.php','hu','118eb0968bb5d5f7b2100159b33fccb5');
INSERT INTO `bow_fordito` VALUES ('370','Felirat','modulok/fiokbeallitasok/formeditor.php','hu','d1d29b9d241ebe9d4c7e0b0fb5a81ef1');
INSERT INTO `bow_fordito` VALUES ('371','Tipus','modulok/fiokbeallitasok/formeditor.php','hu','9299d967630a0ffd1e1fb7ee88ae8e55');
INSERT INTO `bow_fordito` VALUES ('372','Lehetőségek','modulok/fiokbeallitasok/formeditor.php','hu','ebf38e5db4ec478aad5f6460ddd7d11f');
INSERT INTO `bow_fordito` VALUES ('373','...','modulok/fiokbeallitasok/formeditor.php','hu','b6f8c576dbc80b8a5ebf5c8ec9e9c481');
INSERT INTO `bow_fordito` VALUES ('374','Milyen kérelem ez?','modulok/fiokbeallitasok/formeditor.php','hu','39bf584a8278cba23ddfe4beeeda2dea');
INSERT INTO `bow_fordito` VALUES ('375','','modulok/fiokbeallitasok/formeditor.php','hu','630ff928a905848e5ee48b6808623a7e');
INSERT INTO `bow_fordito` VALUES ('376','Cégnév','modulok/fiokbeallitasok/formeditor.php','hu','2e331afec0f4f7137e293752da22824b');
INSERT INTO `bow_fordito` VALUES ('377','Cím','modulok/fiokbeallitasok/formeditor.php','hu','24c763b529bd49000ef9a7c5b24658b1');
INSERT INTO `bow_fordito` VALUES ('378','Kategóriák','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_kategoriaoldal_kategorialista.php','hu','8b39e70c80b3c194c5c37ba3d78ce95a');
INSERT INTO `bow_fordito` VALUES ('379','Paste','modulok/bootstrap/init.php','en','03829218085d99d736ea3ecfe7fffeff');
INSERT INTO `bow_fordito` VALUES ('380','Columns','modulok/bootstrap/init.php','en','46844928417be6e5763b352348406100');
INSERT INTO `bow_fordito` VALUES ('381','Only picture','modulok/cikkgaleria/init.php','en','247063ca5bbffcd415ee7b935335ca0a');
INSERT INTO `bow_fordito` VALUES ('382','File upload','modulok/cikkgaleria/init.php','en','13447eabd4a0e9eae33a1cee00d63e12');
INSERT INTO `bow_fordito` VALUES ('383','Upload','modulok/cikkgaleria/init.php','en','99ff9dc44b8251cd61737c6d9fdfade2');
INSERT INTO `bow_fordito` VALUES ('384','After save you can add gallerys','modulok/cikkgaleria/init.php','en','e534dd764ec0b13e27f8620a48deb5fa');
INSERT INTO `bow_fordito` VALUES ('385','Could not find gallery for this content','modulok/cikkgaleria/init.php','en','59971f7697f50799f41d6620de31d675');
INSERT INTO `bow_fordito` VALUES ('386','Modify','modulok/cikkgaleria/lista.php','en','886104bce9f06144d82a2c9626cd63e3');
INSERT INTO `bow_fordito` VALUES ('387','Delete','modulok/cikkgaleria/lista.php','en','361e08fd77735c3724ba24bdc578911c');
INSERT INTO `bow_fordito` VALUES ('388','Main picture will be set after saving content','modulok/cikkgaleria/lista.php','en','c7c7a71121ba8dd887b792f535713e94');
INSERT INTO `bow_fordito` VALUES ('389','Set as main picture','modulok/cikkgaleria/lista.php','en','f17d582fe602ed10c9ad885083e768ec');
INSERT INTO `bow_fordito` VALUES ('390','Tag','modulok/cimkek/init.php','en','b955edaff41fdbc7c188dad8dce1020f');
INSERT INTO `bow_fordito` VALUES ('391','Edit','modulok/cimkek/init.php','en','2e9a5b0200c64e428993b1762fdff4a7');
INSERT INTO `bow_fordito` VALUES ('392','Delete','modulok/cimkek/init.php','en','deea323d797a3e88a860ee29ca1c5cb4');
INSERT INTO `bow_fordito` VALUES ('393','Successfully deleted','modulok/cimkek/init.php','en','1e9835f3b6c091641b8dd2d63aa7da30');
INSERT INTO `bow_fordito` VALUES ('394','The selected items are removed','modulok/cimkek/init.php','en','d2c5c2420e009629d1b464a7ad578724');
INSERT INTO `bow_fordito` VALUES ('395','Edit tags','modulok/cimkek/init.php','en','e3fbf40bbd9efbc46941d4133a856dbe');
INSERT INTO `bow_fordito` VALUES ('396','Description','modulok/cimkek/init.php','en','c6a37371da811783bf14cc57e830e687');
INSERT INTO `bow_fordito` VALUES ('397','Subscribe','modulok/cimkek/init.php','en','d17cf88723388b8f2182467e826ed87c');
INSERT INTO `bow_fordito` VALUES ('398','Successfully saved','modulok/cimkek/init.php','en','cf92644aba186ea4b913c8d7a568845c');
INSERT INTO `bow_fordito` VALUES ('399','New label','modulok/cimkek/init.php','en','1dd1ab740d82b4e50f9cfc5601af8a7d');
INSERT INTO `bow_fordito` VALUES ('400','Content tag','modulok/cimkek/init.php','en','c5b1c7a0d912e602bdf4b3e22a2327f5');
INSERT INTO `bow_fordito` VALUES ('401','Contents','modulok/fiokbeallitasok/accounteditor.php','en','66f96f800311ea55398e9f81c1e27f1a');
INSERT INTO `bow_fordito` VALUES ('402','Name','modulok/fiokbeallitasok/accounteditor.php','en','04a5d0508b6326768456589451ab078f');
INSERT INTO `bow_fordito` VALUES ('403','Edit','modulok/fiokbeallitasok/accounteditor.php','en','835b314463985b1f4630da7445d49d25');
INSERT INTO `bow_fordito` VALUES ('404','Delete','modulok/fiokbeallitasok/accounteditor.php','en','1a321fb46356986b16314dafb6b5c4ec');
INSERT INTO `bow_fordito` VALUES ('405','Successfully deleted','modulok/fiokbeallitasok/accounteditor.php','en','e602de77ab63c66a0243af0b5fed9a17');
INSERT INTO `bow_fordito` VALUES ('406','The selected users have been removed','modulok/fiokbeallitasok/accounteditor.php','en','d0d89ad26304e1e379fb9d59093f7700');
INSERT INTO `bow_fordito` VALUES ('407','User settings','modulok/fiokbeallitasok/accounteditor.php','en','8aa83f887c1e286ea9666022c3dd4460');
INSERT INTO `bow_fordito` VALUES ('408','Password','modulok/fiokbeallitasok/accounteditor.php','en','fcac09b965d4c3092380653c0c8b8794');
INSERT INTO `bow_fordito` VALUES ('409','Leave it empty if you don\'t want to change it','modulok/fiokbeallitasok/accounteditor.php','en','7548b581e8d06fafaae861feea738c54');
INSERT INTO `bow_fordito` VALUES ('410','User group','modulok/fiokbeallitasok/accounteditor.php','en','10f5c9e694e8a11af8f491b2a9951fe1');
INSERT INTO `bow_fordito` VALUES ('411','Save changes','modulok/fiokbeallitasok/accounteditor.php','en','881e4171f1aa476d1a49afeefad65544');
INSERT INTO `bow_fordito` VALUES ('412','Successfully saved','modulok/fiokbeallitasok/accounteditor.php','en','9cfb521a6cadf30a529dfaec6ac50bb3');
INSERT INTO `bow_fordito` VALUES ('413','Add new user','modulok/fiokbeallitasok/accounteditor.php','en','feddffabc3bd53560b310a495eb028ae');
INSERT INTO `bow_fordito` VALUES ('414','yes','modulok/fiokbeallitasok/accounteditor.php','en','952a2f5a66edddda61ed481970b288fe');
INSERT INTO `bow_fordito` VALUES ('415','no','modulok/fiokbeallitasok/accounteditor.php','en','851c7c208b7c918953ddcb9d114b7e0e');
INSERT INTO `bow_fordito` VALUES ('416','Do you want to send message to the user from registration?','modulok/fiokbeallitasok/accounteditor.php','en','9fcab6d8548cfb46fd8f55de75808189');
INSERT INTO `bow_fordito` VALUES ('417','After saving can we send message to the user from registration.','modulok/fiokbeallitasok/accounteditor.php','en','651a99bac7733479d63c0ac25f9b2e2b');
INSERT INTO `bow_fordito` VALUES ('418','Message of registration','modulok/fiokbeallitasok/accounteditor.php','en','88f6ff2eb3175c5e4baf91da81a928a1');
INSERT INTO `bow_fordito` VALUES ('419','Still pending.','modulok/fiokbeallitasok/accounteditor.php','en','b6d361d4a3d2d1cf3cb7f5c0de0f2ff5');
INSERT INTO `bow_fordito` VALUES ('420','Registration form','modulok/fiokbeallitasok/formeditor.php','en','118eb0968bb5d5f7b2100159b33fccb5');
INSERT INTO `bow_fordito` VALUES ('421','Label','modulok/fiokbeallitasok/formeditor.php','en','d1d29b9d241ebe9d4c7e0b0fb5a81ef1');
INSERT INTO `bow_fordito` VALUES ('422','Type','modulok/fiokbeallitasok/formeditor.php','en','9299d967630a0ffd1e1fb7ee88ae8e55');
INSERT INTO `bow_fordito` VALUES ('423','Options','modulok/fiokbeallitasok/formeditor.php','en','ebf38e5db4ec478aad5f6460ddd7d11f');
INSERT INTO `bow_fordito` VALUES ('424','...','modulok/fiokbeallitasok/formeditor.php','en','b6f8c576dbc80b8a5ebf5c8ec9e9c481');
INSERT INTO `bow_fordito` VALUES ('425','What would you like to use this website for?','modulok/fiokbeallitasok/formeditor.php','en','39bf584a8278cba23ddfe4beeeda2dea');
INSERT INTO `bow_fordito` VALUES ('426','','modulok/fiokbeallitasok/formeditor.php','en','630ff928a905848e5ee48b6808623a7e');
INSERT INTO `bow_fordito` VALUES ('427','Company name','modulok/fiokbeallitasok/formeditor.php','en','2e331afec0f4f7137e293752da22824b');
INSERT INTO `bow_fordito` VALUES ('428','Address','modulok/fiokbeallitasok/formeditor.php','en','24c763b529bd49000ef9a7c5b24658b1');
INSERT INTO `bow_fordito` VALUES ('429','Users','modulok/fiokbeallitasok/init.php','en','85fdfb40c07e0c7216e87bd5f8d74eb0');
INSERT INTO `bow_fordito` VALUES ('430','Registration form','modulok/fiokbeallitasok/init.php','en','a361d0cb456b43355dc0c48ec7d02032');
INSERT INTO `bow_fordito` VALUES ('431','Suitable for HTML editor','modulok/shortcode/init.php','en','fd0e1e2e71958406b150481e4b0d996d');
INSERT INTO `bow_fordito` VALUES ('432','Available shortcodes','modulok/shortcode/init.php','en','9c65acab128c1378cff0e6218cf665a4');
INSERT INTO `bow_fordito` VALUES ('433','Back','modulok/shortcode/shortcode_handler.php','en','2ce0adb0b7517ebe6120971db3b0514d');
INSERT INTO `bow_fordito` VALUES ('434','Wrong e-mail or password','modulok/unicorn/bow_beepulok.php','en','49efbe0772cb4eb6e751da5da4d1555e');
INSERT INTO `bow_fordito` VALUES ('435','Your password recovery link has been sent to your e-mail address','modulok/unicorn/bow_beepulok.php','en','67b8b3cc5691f8c06500e97942bf62bf');
INSERT INTO `bow_fordito` VALUES ('436','Unknown e-mail address!','modulok/unicorn/bow_beepulok.php','en','d65d0cbbd66561f468c39b9678ea939b');
INSERT INTO `bow_fordito` VALUES ('437','Module adjustment not found','modulok/unicorn/osztaly_unicorn_futtato.php','en','b9adf0b8d1f0ac7074607f94092158a5');
INSERT INTO `bow_fordito` VALUES ('438','Control panel','modulok/unicorn/osztaly_unicorn_futtato.php','en','46f4ec9e41347addb21e1b8ec0e7edf3');
INSERT INTO `bow_fordito` VALUES ('439','Module adjustment function not found','modulok/unicorn/osztaly_unicorn_futtato.php','en','7b68c71a690c779d46634b800aea2af6');
INSERT INTO `bow_fordito` VALUES ('440','Type not supported','modulok/unicorn/osztaly_unicorn_futtato.php','en','46ac937de9e194f1a6e92ebe19a7d9ef');
INSERT INTO `bow_fordito` VALUES ('441','Users','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','f48f4a82c10fc6527e490a176f9cbca0');
INSERT INTO `bow_fordito` VALUES ('442','Users','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','6a8dfaaae4fca1c86f458d3a08c190d3');
INSERT INTO `bow_fordito` VALUES ('443','Registration form','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','0cef260e78dbfffba6f0efa8462bc780');
INSERT INTO `bow_fordito` VALUES ('444','Modify','osztaly/osztaly_html.php(355) : eval()\'d code','en','337f639ca25b3a1f9105a0d5be6e0f6a');
INSERT INTO `bow_fordito` VALUES ('445','Please enter your e-mail address and password','osztaly/osztaly_html.php(355) : eval()\'d code','en','164234880bd89bcf3a1c7fc87e9c7d7c');
INSERT INTO `bow_fordito` VALUES ('446','New password','osztaly/osztaly_html.php(355) : eval()\'d code','en','3c1f02a34714e848b68507c85a40df66');
INSERT INTO `bow_fordito` VALUES ('447','Please set your new password!','osztaly/osztaly_html.php(355) : eval()\'d code','en','833376b453dc9d8899eeb5ef862ee607');
INSERT INTO `bow_fordito` VALUES ('448','Send','osztaly/osztaly_html.php(355) : eval()\'d code','en','69d14574b132baa062fe71371fd9db42');
INSERT INTO `bow_fordito` VALUES ('449','Back','osztaly/osztaly_html.php(355) : eval()\'d code','en','fbfd4cf3f69e4f90f4d0bbf1c71c78e5');
INSERT INTO `bow_fordito` VALUES ('450','Please enter your email address to send you the reset code!','osztaly/osztaly_html.php(355) : eval()\'d code','en','87272accb94dffdf3869ee4f16596e14');
INSERT INTO `bow_fordito` VALUES ('451','Forgotten password','osztaly/osztaly_html.php(355) : eval()\'d code','en','8334bfbdb6620e0f0b12df91feb0d28c');
INSERT INTO `bow_fordito` VALUES ('452','Password','osztaly/osztaly_html.php(355) : eval()\'d code','en','fb3e0cce837aa0416d0245f5ee548e9b');
INSERT INTO `bow_fordito` VALUES ('453','Home','osztaly/osztaly_html.php(352) : eval()\'d code','en','348cabca66c7297907632712f3ec61af');
INSERT INTO `bow_fordito` VALUES ('454','Dashboard','osztaly/osztaly_html.php(352) : eval()\'d code','en','5b36abb67adacf0aea5a4018f90bebc4');
INSERT INTO `bow_fordito` VALUES ('455','Administration','osztaly/osztaly_html.php(352) : eval()\'d code','en','73f0e3a7b41b98c297a27c29d507412c');
INSERT INTO `bow_fordito` VALUES ('456','Dashboard','osztaly/osztaly_html.php(351) : eval()\'d code','en','8d25a4be0e059bfa4f8813302796bbd5');
INSERT INTO `bow_fordito` VALUES ('457','Administration','osztaly/osztaly_html.php(351) : eval()\'d code','en','82236b5009cd88b3c2046f70571826d0');
INSERT INTO `bow_fordito` VALUES ('458','Dashboard','osztaly/osztaly_html.php(337) : eval()\'d code','en','568a5647d438d9f2c48833b5ea621b57');
INSERT INTO `bow_fordito` VALUES ('459','Administration','osztaly/osztaly_html.php(337) : eval()\'d code','en','5b5f470ff2d9e54473ca2f7bc88fee14');
INSERT INTO `bow_fordito` VALUES ('460','Password','osztaly/osztaly_html.php(337) : eval()\'d code','en','58d27b8466a9fa32307eb7c1c71e8356');
INSERT INTO `bow_fordito` VALUES ('461','Please enter your email address to send you the reset code!','osztaly/osztaly_html.php(337) : eval()\'d code','en','8fd2b99a49c31b15545b0c4e0a9ad9cc');
INSERT INTO `bow_fordito` VALUES ('462','Please enter your new password!','osztaly/osztaly_html.php(337) : eval()\'d code','en','5ff131f234fe38dfb05e2bcc2fc8346c');
INSERT INTO `bow_fordito` VALUES ('463','New password','osztaly/osztaly_html.php(337) : eval()\'d code','en','4210f9c6f50945c6b80272590b934686');
INSERT INTO `bow_fordito` VALUES ('464','Login','osztaly/osztaly_html.php(337) : eval()\'d code','en','16b7eeaae5ca5bd523b4f8edf398f420');
INSERT INTO `bow_fordito` VALUES ('465','Back','osztaly/osztaly_html.php(337) : eval()\'d code','en','0fcc8a4cf12d728c8e92f54a248f3cae');
INSERT INTO `bow_fordito` VALUES ('466','Send','osztaly/osztaly_html.php(337) : eval()\'d code','en','e40cc75b0faccc6ebc3a5173f7e00970');
INSERT INTO `bow_fordito` VALUES ('467','Forgotten password','osztaly/osztaly_html.php(337) : eval()\'d code','en','4eeba915a71be3ab89223d18854e83e9');
INSERT INTO `bow_fordito` VALUES ('468','Please enter your e-mail address and password','osztaly/osztaly_html.php(337) : eval()\'d code','en','727776e2d60ee5543391c30b5809e4ad');
INSERT INTO `bow_fordito` VALUES ('469','Lost password?','osztaly/osztaly_html.php(337) : eval()\'d code','en','69f34666ce2afe2eb2cc18771cc31f30');
INSERT INTO `bow_fordito` VALUES ('470','Password','osztaly/osztaly_html.php(337) : eval()\'d code','en','3d0d0a868d2a76986874244d909cab59');
INSERT INTO `bow_fordito` VALUES ('471','Please enter your e-mail address and password','osztaly/osztaly_html.php(337) : eval()\'d code','en','af252fdcd51a5da629023fafcc7b9586');
INSERT INTO `bow_fordito` VALUES ('472','Title','modulok/_szovegek/init.php','en','493b59fa3489a3a20d11f40a816f36ca');
INSERT INTO `bow_fordito` VALUES ('473','Introductory','modulok/_szovegek/init.php','en','d76f45eca4a58bab4ea79bfac11b04e9');
INSERT INTO `bow_fordito` VALUES ('474','Content','modulok/_szovegek/init.php','en','abe9b144fefbc66ac356e93a27ec48de');
INSERT INTO `bow_fordito` VALUES ('475','Edit content','modulok/_szovegek/init.php','en','58732cc47939b74bb8e5e540b16eae3b');
INSERT INTO `bow_fordito` VALUES ('476','Select an existing text','modulok/_szovegek/init.php','en','965c0372d042f910079340ef9cb3b8ce');
INSERT INTO `bow_fordito` VALUES ('477','To add a previously created text select one and click the save button!','modulok/_szovegek/init.php','en','71d03c5d43c36728c640ee494263ffe4');
INSERT INTO `bow_fordito` VALUES ('478','No, i make a new content','modulok/_szovegek/init.php','en','42f5e3bb83d26cf9add2132ded50d015');
INSERT INTO `bow_fordito` VALUES ('479','NONE','modulok/_szovegek/init.php','en','873718c7e8a0355ac5311cf2af4443e9');
INSERT INTO `bow_fordito` VALUES ('480','Tags','modulok/_szerkeszto/unicornposteditor.php','en','ad28e04db0cc5536fc7fbb6e8c656642');
INSERT INTO `bow_fordito` VALUES ('481','Save sequence','modulok/_szerkeszto/unicornposteditor.php','en','2d7a1d61ba340d29dcfe765d685ff965');
INSERT INTO `bow_fordito` VALUES ('482','Content successfully saved','modulok/_szerkeszto/unicornposteditor.php','en','5127ed5da82da38ab45afe5f7ed81e2f');
INSERT INTO `bow_fordito` VALUES ('483','Content','modulok/_szerkeszto/unicornposteditor.php','en','65ba4ad967c01cdc516adc24a6261097');
INSERT INTO `bow_fordito` VALUES ('484','Introductory','modulok/_szerkeszto/unicornposteditor.php','en','84a55c57df564210b3976cc93bad50ed');
INSERT INTO `bow_fordito` VALUES ('485','Add text prepared earlier','modulok/_szovegek/init.php','en','5f1600502a93dcfebbebf8d4e21a0686');
INSERT INTO `bow_fordito` VALUES ('486','Title','modulok/_szerkeszto/unicornposteditor.php','en','beb9976a9dc018fe20cfcffe1dcd5d92');
INSERT INTO `bow_fordito` VALUES ('487','Contents','modulok/_szerkeszto/unicornposteditor.php','en','70cc6699201870c6a4a95ccec11aea8b');
INSERT INTO `bow_fordito` VALUES ('488','Content name','modulok/_szerkeszto/unicornposteditor.php','en','de6b0345e217260931b7b6247d8e7e5b');
INSERT INTO `bow_fordito` VALUES ('489','Sequence','modulok/_szerkeszto/unicornposteditor.php','en','48184379c277fc422000c540cbf87110');
INSERT INTO `bow_fordito` VALUES ('490','Edit','modulok/_szerkeszto/unicornposteditor.php','en','1aabe0b65eb55d53476fb4d5f3512178');
INSERT INTO `bow_fordito` VALUES ('491','Delete','modulok/_szerkeszto/unicornposteditor.php','en','64d83e80535935dca33b6571cc20d6c4');
INSERT INTO `bow_fordito` VALUES ('492','Add new content','modulok/_szerkeszto/unicornposteditor.php','en','ce011bd5e6cd46286cde8f759c919819');
INSERT INTO `bow_fordito` VALUES ('493','Edit content','modulok/_szerkeszto/unicornposteditor.php','en','8987bbcbb78aa540c3249f9dfb43cb25');
INSERT INTO `bow_fordito` VALUES ('494','Delete done','modulok/_szerkeszto/unicornposteditor.php','en','26de36643787ceb4cf09826baff80dc0');
INSERT INTO `bow_fordito` VALUES ('495','No tag selected','modulok/_szerkeszto/unicornposteditor.php','en','4a6688ac81dcbb1557e92b5bf77a49b1');
INSERT INTO `bow_fordito` VALUES ('496','Filter content tags','modulok/_szerkeszto/unicornposteditor.php','en','2eba19092978954496fba1105354f3dd');
INSERT INTO `bow_fordito` VALUES ('497','The selected items are removed!','modulok/_szerkeszto/unicornposteditor.php','en','d1d6a99d9118db493082c20fed1a6722');
INSERT INTO `bow_fordito` VALUES ('498','Post Editor','modulok/_szerkeszto/init.php','en','19e3aed2f660d284b1f6ae0fcd12fd11');
INSERT INTO `bow_fordito` VALUES ('499','<strong>Please, save the text first and then open it again and you can add your gallery as well</strong>','beepulok/_szoveg_galeria.php','en','e40dbd5dee09fa67b0897a6aaeb7d9e9');
INSERT INTO `bow_fordito` VALUES ('500','Content gallery component','modulok/cikkgaleria/init.php','en','9d55b0a2053d0c7676b26df281003b7b');
INSERT INTO `bow_fordito` VALUES ('501','Registration fields','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','78c656d349f8e10e9d7fad70a7845b48');
INSERT INTO `bow_fordito` VALUES ('502','Are you sure?','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','77875dfb6bc9083c4b8facea0dce3038');
INSERT INTO `bow_fordito` VALUES ('503','Delete selected','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','e96041fee48c44812619f65d9ca2cb08');
INSERT INTO `bow_fordito` VALUES ('504','Search','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','0bdac2cd570ac320e7d194665b55c4c0');
INSERT INTO `bow_fordito` VALUES ('505','Cancel','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','0e599f0022ff5fa6738ed7d5eac40709');
INSERT INTO `bow_fordito` VALUES ('506','Nothing selected','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','05440a3b0bd5a1218e01d0b7538e3ea2');
INSERT INTO `bow_fordito` VALUES ('507','Post Editor','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','6ce64df4888771ff74c0973e9d676f5d');
INSERT INTO `bow_fordito` VALUES ('508','Calegories','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_kategoriaoldal_kategorialista.php','en','8b39e70c80b3c194c5c37ba3d78ce95a');
INSERT INTO `bow_fordito` VALUES ('509','Option name:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','198f6c50004a2c476a625042cd69c9bd');
INSERT INTO `bow_fordito` VALUES ('510','Option description:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','cc8397f30f1d0dd70a7da33c39033b7d');
INSERT INTO `bow_fordito` VALUES ('511','Option price:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','81fbc580f6ed3ac1488e74f138a74484');
INSERT INTO `bow_fordito` VALUES ('512','Sequence:','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','d71a4ae5e47ac5e2702c40b0ae7d9038');
INSERT INTO `bow_fordito` VALUES ('513','-- DELETE OPTION --','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','2fd3eadf980ffc559c2320fd8f7cbd0e');
INSERT INTO `bow_fordito` VALUES ('514','Delete this option?','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','d4ade91e16fcb13488dc331f2fefcd92');
INSERT INTO `bow_fordito` VALUES ('515','After saving this option will be deleted','modulok/_aruhaz/termekek/html/termek_opciok_hu.php','en','b13a275eb3afd8699019325a65f4be28');
INSERT INTO `bow_fordito` VALUES ('516','Add new menu','modulok/_menu/ajaxmenueditor.php','en','0a5d7a2ba6b456ad7a327b9f75a2369a');
INSERT INTO `bow_fordito` VALUES ('517','Refresh','modulok/_menu/ajaxmenueditor.php','en','4532fe3896c74005c9f2a6673ee63551');
INSERT INTO `bow_fordito` VALUES ('518','Menu label','modulok/_menu/ajaxmenueditor.php','en','24c4e76918eaa34bb146b74ce80cc99d');
INSERT INTO `bow_fordito` VALUES ('519','Language','modulok/_menu/ajaxmenueditor.php','en','9daa10b39e75c5689f67110acf178e7a');
INSERT INTO `bow_fordito` VALUES ('520','Type and link','modulok/_menu/ajaxmenueditor.php','en','857aaf3c15d3c5fbd1dfd7335f110928');
INSERT INTO `bow_fordito` VALUES ('521','Page link','modulok/_menu/ajaxmenueditor.php','en','d288e69284071548cd7769da68ea6016');
INSERT INTO `bow_fordito` VALUES ('522','External link','modulok/_menu/ajaxmenueditor.php','en','bd3c0f1f49baf9f3382759c11ee48442');
INSERT INTO `bow_fordito` VALUES ('523','Component page','modulok/_menu/ajaxmenueditor.php','en','aa189c25fcd47c9b1257a547ba57d2ea');
INSERT INTO `bow_fordito` VALUES ('524','Choose from existed pages','modulok/_menu/ajaxmenueditor.php','en','5fce5534b85716a07ffe924bb6e062f2');
INSERT INTO `bow_fordito` VALUES ('525','Save','modulok/_menu/ajaxmenueditor.php','en','77eee5592b79dfded223085103177c0b');
INSERT INTO `bow_fordito` VALUES ('526','Please enter the link (http://example.com/site)','modulok/_menu/ajaxmenueditor.php','en','30f7912eee139588d923c72c1f325b98');
INSERT INTO `bow_fordito` VALUES ('527','Available components','modulok/_menu/ajaxmenueditor.php','en','7ee8ccbf72aac9903bf3b6feba932dd6');
INSERT INTO `bow_fordito` VALUES ('528','..::loading::..','modulok/_menu/ajaxmenueditor.php','en','d3f97280cd37b7c63764ad5c1b9f4725');
INSERT INTO `bow_fordito` VALUES ('529','Move to my place...','modulok/_menu/ajaxmenueditor.php','en','b6a4d7e3510fd9fa8d6655139d33f853');
INSERT INTO `bow_fordito` VALUES ('530','Loading','modulok/_menu/ajaxmenueditor.php','en','5de099b1e3ff6ef48f50abac07bc01ed');
INSERT INTO `bow_fordito` VALUES ('531','Are you sure?','modulok/_menu/ajaxmenueditor.php','en','cbee689385eb708c0b137929f1e08528');
INSERT INTO `bow_fordito` VALUES ('532','Select menu group','modulok/_menu/ajaxmenueditor.php','en','1e37d235f6a569f05429e23dfffa047a');
INSERT INTO `bow_fordito` VALUES ('533','New menu group','modulok/_menu/ajaxmenueditor.php','en','30bdfe0c4b3cd37915440263493846ac');
INSERT INTO `bow_fordito` VALUES ('534','Edit menu item','modulok/_menu/ajaxmenueditor.php','en','4d52c557435d0bf3bc2285c75403d18a');
INSERT INTO `bow_fordito` VALUES ('535','Menu group','modulok/_menu/ajaxmenueditor.php','en','046b66241b9f3732f7569b0cb27dc0cd');
INSERT INTO `bow_fordito` VALUES ('536','Menu item','modulok/_menu/ajaxmenueditor.php','en','afbc801421be30b8c0a6366fd5e5e8d9');
INSERT INTO `bow_fordito` VALUES ('537','Who can see? (leave blank if everyone can see)','modulok/_menu/ajaxmenueditor.php','en','12374d198a3ffba621dafb1c257f1a09');
INSERT INTO `bow_fordito` VALUES ('538','Edit page','modulok/_oldalak/unicornoldalepito.php','en','3067caf3de025187e73078a7ad582297');
INSERT INTO `bow_fordito` VALUES ('539','Edit websites','modulok/_oldalak/unicornoldalepito.php','en','eb0ad5e9d71243b996e01ad184aa4e25');
INSERT INTO `bow_fordito` VALUES ('540','Edit','modulok/_oldalak/unicornoldalepito.php','en','fede51ac3513c0baff5a92b9a4ff5922');
INSERT INTO `bow_fordito` VALUES ('541','List of available pages','modulok/_oldalak/unicornoldalepito.php','en','a4ac556639b03bfc2bd0a5b7c82122c2');
INSERT INTO `bow_fordito` VALUES ('542','Successfully deleted','modulok/_oldalak/unicornoldalepito.php','en','6101990880d8936bde5fc6ac36b22236');
INSERT INTO `bow_fordito` VALUES ('543','The selected items are removed!','modulok/_oldalak/unicornoldalepito.php','en','b66ecdc9179c582abc88b8d0857ee377');
INSERT INTO `bow_fordito` VALUES ('544','Error','modulok/_oldalak/unicornoldalepito.php','en','96722d2972d8a8b1d119834ab6e57f98');
INSERT INTO `bow_fordito` VALUES ('545','The data set can not be found!','modulok/_oldalak/unicornoldalepito.php','en','86fb253878aa45988e01e81c530b0601');
INSERT INTO `bow_fordito` VALUES ('546','Page title','modulok/_oldalak/unicornoldalepito.php','en','c13e1f0b6621cc80c9dacce497cef2d6');
INSERT INTO `bow_fordito` VALUES ('547','On the administration interface you can find with this name','modulok/_oldalak/unicornoldalepito.php','en','c465af5d4c09d6185fc8699e601fc025');
INSERT INTO `bow_fordito` VALUES ('548','SEO keywords','modulok/_oldalak/unicornoldalepito.php','en','afb7c18480eb1cd60b5e0e5959997b9d');
INSERT INTO `bow_fordito` VALUES ('549','Keywords for the search engines','modulok/_oldalak/unicornoldalepito.php','en','73c99763da991c8e3e68934aca69ce71');
INSERT INTO `bow_fordito` VALUES ('550','SEO description','modulok/_oldalak/unicornoldalepito.php','en','63bb60d0d5aa42ef4a059fb450ff7ef7');
INSERT INTO `bow_fordito` VALUES ('551','Short description for search engines','modulok/_oldalak/unicornoldalepito.php','en','55169d9df1e1d2044fbf0074761bbf4c');
INSERT INTO `bow_fordito` VALUES ('552','Metadata operation','modulok/_oldalak/unicornoldalepito.php','en','6e49911143b417497957eeff546c1b8d');
INSERT INTO `bow_fordito` VALUES ('553','How to operate the metadata','modulok/_oldalak/unicornoldalepito.php','en','6182f50b2aeccefb542818412adb80fa');
INSERT INTO `bow_fordito` VALUES ('554','Language','modulok/_oldalak/unicornoldalepito.php','en','51224e230346ac905c950d65cd54e953');
INSERT INTO `bow_fordito` VALUES ('555','Language of this page','modulok/_oldalak/unicornoldalepito.php','en','78ecd4839523b26b97e875afb3fa7610');
INSERT INTO `bow_fordito` VALUES ('556','Theme of this page','modulok/_oldalak/unicornoldalepito.php','en','4d635cc43ee4a08ee7be6990e43f8399');
INSERT INTO `bow_fordito` VALUES ('557','Than one page can be specified topic','modulok/_oldalak/unicornoldalepito.php','en','28bbd94c5a2bb22013b4ea8f4df8e8c0');
INSERT INTO `bow_fordito` VALUES ('558','Site\'s URL','modulok/_oldalak/unicornoldalepito.php','en','31e10fb2b189e9b42ccd513a782749e1');
INSERT INTO `bow_fordito` VALUES ('559','What will be available in the page url','modulok/_oldalak/unicornoldalepito.php','en','b5a7afceef967b822d975ca26acf3751');
INSERT INTO `bow_fordito` VALUES ('560','Special action page','modulok/_oldalak/unicornoldalepito.php','en','1e2bb893476441724ca975876f2610da');
INSERT INTO `bow_fordito` VALUES ('561','You can configure special-purpose sites','modulok/_oldalak/unicornoldalepito.php','en','9b9af7ed7751accab59fd4e54c67baa9');
INSERT INTO `bow_fordito` VALUES ('562','Fitted to the basic meta tags','modulok/_oldalak/unicornoldalepito.php','en','b895ec147596e07aa17e303bca297e98');
INSERT INTO `bow_fordito` VALUES ('563','Overrides the global setting','modulok/_oldalak/unicornoldalepito.php','en','e2e34fdcd3831b5cfdf893f93316d395');
INSERT INTO `bow_fordito` VALUES ('564','Default','modulok/_oldalak/unicornoldalepito.php','en','9542f6e7cb6cdf403b64f4d190e5571c');
INSERT INTO `bow_fordito` VALUES ('565','Only unaccented ASCII characters can be used','modulok/_oldalak/unicornoldalepito.php','en','26da276fea9dab353fae6668f27b083f');
INSERT INTO `bow_fordito` VALUES ('566','No special page','modulok/_oldalak/unicornoldalepito.php','en','594027673a0b8c3cb69448ba8bc2fb24');
INSERT INTO `bow_fordito` VALUES ('567','Home','modulok/_oldalak/unicornoldalepito.php','en','3bff647c96ca705c1a48814c03830557');
INSERT INTO `bow_fordito` VALUES ('568','404 error page','modulok/_oldalak/unicornoldalepito.php','en','1605f3a6047220a59fe5a3c97665571d');
INSERT INTO `bow_fordito` VALUES ('569','No access error page','modulok/_oldalak/unicornoldalepito.php','en','69ed68e00a1c03c29724c675d4c616cc');
INSERT INTO `bow_fordito` VALUES ('570','Successfully saved','modulok/_oldalak/unicornoldalepito.php','en','45336f72e4a76cb21ab6985dd70d08c0');
INSERT INTO `bow_fordito` VALUES ('571','Restriction','modulok/_oldalak/unicornoldalepito.php','en','a56cb4043735e4867a641b39d588a50d');
INSERT INTO `bow_fordito` VALUES ('572','List of pages','modulok/_oldalak/unicornoldalepito.php','en','dfbdfbf6fae1bb1a9dfa43769e0c48b4');
INSERT INTO `bow_fordito` VALUES ('573','Displayed list of modules','modulok/_oldalak/unicornoldalepito.php','en','74c7551265a72841e388b0c404f06b5f');
INSERT INTO `bow_fordito` VALUES ('574','Displayed list of contents','modulok/_oldalak/unicornoldalepito.php','en','c5588b47423888c05e24ae3fa1cb8f54');
INSERT INTO `bow_fordito` VALUES ('575','Page title','modulok/_oldalak/unicornoldalepito.php','en','9704263536368b4432e15de9c0252afd');
INSERT INTO `bow_fordito` VALUES ('576','Keywords','modulok/_oldalak/unicornoldalepito.php','en','5286b0902b8b92d0bf2e3bf9690c1b5f');
INSERT INTO `bow_fordito` VALUES ('577','Add new page','modulok/_oldalak/unicornoldalepito.php','en','67df727617c94b2ad2f3b648214531e6');
INSERT INTO `bow_fordito` VALUES ('578','Add content to page','modulok/_oldalak/unicornoldalepito.php','en','bd79b933a7b49113375495661b8d4fc0');
INSERT INTO `bow_fordito` VALUES ('579','Wrong parameters','modulok/_oldalak/unicornoldalepito.php','en','9e3831d5e8ff667029af7f99f6af568e');
INSERT INTO `bow_fordito` VALUES ('580','Module is not installed','modulok/_oldalak/unicornoldalepito.php','en','015a5287ae39c06abac58174362021b2');
INSERT INTO `bow_fordito` VALUES ('581','Module running function not found','modulok/_oldalak/unicornoldalepito.php','en','6f967cc1fee3bc4082ee74d5f3378143');
INSERT INTO `bow_fordito` VALUES ('582','Adjustment function is missing','modulok/_oldalak/unicornoldalepito.php','en','4a7087c24aa9a7da0ebc320b94f39d54');
INSERT INTO `bow_fordito` VALUES ('583','Adjustment function is missing','modulok/_oldalak/unicornoldalepito.php','en','5e0c1a9289a0f8ec68c58b853df5664e');
INSERT INTO `bow_fordito` VALUES ('584','Content added successfully','modulok/_oldalak/unicornoldalepito.php','en','1b985eacaff5a8de9de4381ff2706603');
INSERT INTO `bow_fordito` VALUES ('585','New content added successfully','modulok/_oldalak/unicornoldalepito.php','en','72b41ee7613c58d705ededfa33b68a9c');
INSERT INTO `bow_fordito` VALUES ('586','Contents','modulok/_oldalak/unicornoldalepito.php','en','a9d41be8635571bff62b7da95359b13f');
INSERT INTO `bow_fordito` VALUES ('587','Content name','modulok/_oldalak/unicornoldalepito.php','en','2778fc55f54c205147412a63ffcff8ed');
INSERT INTO `bow_fordito` VALUES ('588','Restriction','modulok/_oldalak/unicornoldalepito.php','en','139b2074caa9f34cebf7f925f09ea958');
INSERT INTO `bow_fordito` VALUES ('589','Delete','modulok/_oldalak/unicornoldalepito.php','en','a1ef1166e2f763974eaab2c9d61e97cd');
INSERT INTO `bow_fordito` VALUES ('590','No setting','modulok/_oldalak/unicornoldalepito.php','en','1576ea8109b027f826dbac0dbd464ffb');
INSERT INTO `bow_fordito` VALUES ('591','No setting for this content','modulok/_oldalak/unicornoldalepito.php','en','90abb5f49373973c1b1a13d07b0823fc');
INSERT INTO `bow_fordito` VALUES ('592','Content changed successfully','modulok/_oldalak/unicornoldalepito.php','en','ba430b4a2d47c30b64f598844c5f3565');
INSERT INTO `bow_fordito` VALUES ('593','Content changed successfully','modulok/_oldalak/unicornoldalepito.php','en','f98a7bfd5c018c8abd21b920831123f9');
INSERT INTO `bow_fordito` VALUES ('594','Restriction','modulok/_oldalak/unicornoldalepito.php','en','3eb53b731d03eb0639c19676dbb63756');
INSERT INTO `bow_fordito` VALUES ('595','Sequence','modulok/_oldalak/unicornoldalepito.php','en','32a29045abe510130e16daf7fdd6bf94');
INSERT INTO `bow_fordito` VALUES ('596','Save sequence','modulok/_oldalak/unicornoldalepito.php','en','f74f7bcee77656af252a472c1b200295');
INSERT INTO `bow_fordito` VALUES ('597','Save sequence','modulok/_oldalak/unicornoldalepito.php','en','b4dcd289b4eab5667e16e8faf58aaf78');
INSERT INTO `bow_fordito` VALUES ('598','Sequence saved successfully','modulok/_oldalak/unicornoldalepito.php','en','002c4a18b62f312d24a33ad5fe368a41');
INSERT INTO `bow_fordito` VALUES ('599','Edit groups','modulok/_oldalak/unicornoldalepito.php','en','304f8ad4ec4e5315f3f4237fd225cf68');
INSERT INTO `bow_fordito` VALUES ('600','The groups will be set here to see the content. If there is no one set, everyone can see it.','modulok/_oldalak/unicornoldalepito.php','en','0138805b427385f08763aa25032fbaef');
INSERT INTO `bow_fordito` VALUES ('601','Choose who can see the content, or do not select anything, so everyone can see','modulok/_oldalak/unicornoldalepito.php','en','b43bba72da0b03d99ddb5ce095db04ef');
INSERT INTO `bow_fordito` VALUES ('602','Permission saved successfully','modulok/_oldalak/unicornoldalepito.php','en','71e6b5cb1acb626731ea5bdc948c9360');
INSERT INTO `bow_fordito` VALUES ('603','The order of group content successfully saved','modulok/_oldalak/unicornoldalepito.php','en','5984b5629fcc001572be3185621dcdee');
INSERT INTO `bow_fordito` VALUES ('604','Page appearing control on users level','modulok/_oldalak/unicornoldalepito.php','en','fef3466d464b266e852c012c423a36cd');
INSERT INTO `bow_fordito` VALUES ('605','Általános beállítások','modulok/_beallitasok/init.php','hu','899ab51a5afd640bdfd13b5c6a9cf16b');
INSERT INTO `bow_fordito` VALUES ('606','Migráció','modulok/_migracio/init.php','hu','39afb3833b7306df6aa7f07cf57a2890');
INSERT INTO `bow_fordito` VALUES ('607','Modulok','modulok/_modulok/init.php','hu','583fe09f851ff59bc64518503d1e5bc1');
INSERT INTO `bow_fordito` VALUES ('608','Témakezelő','modulok/_temak/init.php','hu','28132b4b054d57890c7f0f1c54a54b46');
INSERT INTO `bow_fordito` VALUES ('609','Új esemény címe','modulok/events/eventsajaxunicorn.php','hu','18cc65f2c9a225fabe3f6ff91d5c76d6');
INSERT INTO `bow_fordito` VALUES ('610','Dátum','modulok/events/eventsajaxunicorn.php','hu','c79efe71befd8c5ece5be47f193817ca');
INSERT INTO `bow_fordito` VALUES ('611','Leírás','modulok/events/eventsajaxunicorn.php','hu','8b583b127a21cbf91049d10375683703');
INSERT INTO `bow_fordito` VALUES ('612','Cím és dátum megadása kötelező!','modulok/events/eventsajaxunicorn.php','hu','b9a08c93f20eb1bc10f880d4f1435c90');
INSERT INTO `bow_fordito` VALUES ('613','Új esemény mentése','modulok/events/eventsajaxunicorn.php','hu','2b199bfb3d24cb8624015af295a43283');
INSERT INTO `bow_fordito` VALUES ('614','Widget','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','0eab28733943702bac73ce2035afcd5e');
INSERT INTO `bow_fordito` VALUES ('615','Általános beállítások','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','301e8600c7fa1b8302e959879d3bebbe');
INSERT INTO `bow_fordito` VALUES ('616','Migráció','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','d1723c7bd5a34514fd8017e485693a7c');
INSERT INTO `bow_fordito` VALUES ('617','Modulok','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','295ef7a75280adf50b58654b8e3e9930');
INSERT INTO `bow_fordito` VALUES ('618','Témakezelő','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','a2ffed15e8b29f9aaa1023945810fda4');
INSERT INTO `bow_fordito` VALUES ('619','Witgetek','modulok/_widget/widgetunicorn.php','hu','3c547995ffb933350e15186d0b7642e7');
INSERT INTO `bow_fordito` VALUES ('620','Elérhető widgetek','modulok/_widget/widgetunicorn.php','hu','9507dd1f537805c98bc04d9c6cc9e8eb');
INSERT INTO `bow_fordito` VALUES ('621','Events','modulok/_widget/_widgetkomp.php','hu','a0c061d71e906cebf8a16b58c29e55d4');
INSERT INTO `bow_fordito` VALUES ('622','Events listing','modulok/_widget/_widgetkomp.php','hu','9be4a4a4593685f83cbcb01798a4b263');
INSERT INTO `bow_fordito` VALUES ('623','Belépés/fiók','modulok/_widget/_widgetkomp.php','hu','aa7b0654629ad1a2fab68fb6b88bacd3');
INSERT INTO `bow_fordito` VALUES ('624','Belépés és fiók panel','modulok/_widget/_widgetkomp.php','hu','a3ec2a225d6728ed916137632180ccca');
INSERT INTO `bow_fordito` VALUES ('625','Kategória doboz','modulok/_widget/_widgetkomp.php','hu','b985193e6637082b711a4f94e1b66913');
INSERT INTO `bow_fordito` VALUES ('626','Áruház kategóriák megjelenítése','modulok/_widget/_widgetkomp.php','hu','0e66fe9a57cceaf80206bb7aad24dad4');
INSERT INTO `bow_fordito` VALUES ('627','Helyfoglalo','modulok/_widget/_widgetkomp.php','hu','bcf0e65b49e6ebdf52159348f7738482');
INSERT INTO `bow_fordito` VALUES ('628','Üres helyőrző','modulok/_widget/_widgetkomp.php','hu','e5f3139d4553b2c532029c344a95c02f');
INSERT INTO `bow_fordito` VALUES ('629','Mozgass a megfelelő pozícióba','modulok/_widget/_widgetkomp.php','hu','2d782f2db8f9fa9f836dffec40acad81');
INSERT INTO `bow_fordito` VALUES ('630','Stílus pozíciók','modulok/_widget/widgetunicorn.php','hu','118d93704bc2da9bfd80d6f33f54e569');
INSERT INTO `bow_fordito` VALUES ('631','Slider','modulok/_widget/_widgetkomp.php','hu','c1eb05680fa6f8a9320a407d30dbd57d');
INSERT INTO `bow_fordito` VALUES ('632','Tartalom melletti bal sáv','modulok/_widget/_widgetkomp.php','hu','7fed85795a4afff03540da0b8c529261');
INSERT INTO `bow_fordito` VALUES ('633','Jobb sidebar','modulok/_widget/_widgetkomp.php','hu','8a66b15d1f5a0c1dc082a86d7ffe893e');
INSERT INTO `bow_fordito` VALUES ('634','Widget szerkesztő','modulok/_widget/widgetunicorn.php','hu','953c2ef0a42c66a2ff10c62d5944658d');
INSERT INTO `bow_fordito` VALUES ('635','Mentés','modulok/_widget/widgetunicorn.php','hu','6b96cfcc9b9655263e7a6ae905620bb2');
INSERT INTO `bow_fordito` VALUES ('636','Mégsem','modulok/_widget/widgetunicorn.php','hu','8c5f9917a112fb8846c96dc8319329bc');
INSERT INTO `bow_fordito` VALUES ('637','Betöltés folyamatban...','modulok/_widget/widgetunicorn.php','hu','7a36efd9b0061d1498ca054a6e2d9025');
INSERT INTO `bow_fordito` VALUES ('638','Mozgass a megfelelő pozícióba','modulok/_widget/widgetunicorn.php','hu','b171392bd304e333a015784fbde9c97f');
INSERT INTO `bow_fordito` VALUES ('639','Szerkesztés','modulok/_widget/_widgetkomp.php','hu','3a2dc5342cc29e05cd209c242f40fc9a');
INSERT INTO `bow_fordito` VALUES ('640','A törlés végleges!','modulok/_widget/_widgetkomp.php','hu','43ec3c641fc975a7201fe4e05e30be04');
INSERT INTO `bow_fordito` VALUES ('641','Törlés','modulok/_widget/_widgetkomp.php','hu','0f35a118f6e6119918962e8876b7e4a8');
INSERT INTO `bow_fordito` VALUES ('642','Beállítások','modulok/_widget/ajaxwidgetunicorn.php','hu','b4d11eaf020e8ef78a943be59b660739');
INSERT INTO `bow_fordito` VALUES ('643','Korlátozások','modulok/_widget/ajaxwidgetunicorn.php','hu','4dbce8d54554070f529744a55c74df40');
INSERT INTO `bow_fordito` VALUES ('644','Doboz felirat:','modulok/events/events.php','hu','f6bef264389f1a420189e0dfb3d13d54');
INSERT INTO `bow_fordito` VALUES ('645','Esemény törlése','modulok/events/eventsajaxunicorn.php','hu','b06412ae72484ae5e4c9b03e592e2f40');
INSERT INTO `bow_fordito` VALUES ('646','Események','modulok/events/eventsajaxunicorn.php','hu','94b95fe6752d297f2f9d8b50d2ec564c');
INSERT INTO `bow_fordito` VALUES ('647','Törlés','modulok/events/eventsajaxunicorn.php','hu','a47f09dc5e549f1a0277730c4e61471e');
INSERT INTO `bow_fordito` VALUES ('648','Esemény törlése megtörtént!','modulok/events/eventsajaxunicorn.php','hu','dded4b1767a9d31ee608bbd4ffc41b1a');
INSERT INTO `bow_fordito` VALUES ('649','Migrációs eszköz','modulok/_migracio/_migraciokomp.php','hu','1faa01d0ed00260b86fb87900096feb9');
INSERT INTO `bow_fordito` VALUES ('650','Sikeres művelet','modulok/_migracio/_migracio.php','hu','b92a0ccec429036c9b8785de6c5ed8fc');
INSERT INTO `bow_fordito` VALUES ('651','Telepítő példány létrejött','modulok/_migracio/_migracio.php','hu','b73c0f69448fcd3f07a32dd9eb112489');
INSERT INTO `bow_fordito` VALUES ('652','Modulok listája','modulok/_modulok/_modulok.php','hu','6f3833b7c057dafb64592f937b3c292f');
INSERT INTO `bow_fordito` VALUES ('653','Bekapcsolt modulok','modulok/_modulok/_modulok.php','hu','17c59b9a6ec02b735213c4f9e1c95b76');
INSERT INTO `bow_fordito` VALUES ('654','Modulnév','modulok/_modulok/_modulokkomp.php','hu','4e067fa6556ad0cd0386b6c92990cda7');
INSERT INTO `bow_fordito` VALUES ('655','Leírás','modulok/_modulok/_modulokkomp.php','hu','8825a156bb2a9986ecc0389c68db3ec8');
INSERT INTO `bow_fordito` VALUES ('656','Műveletek','modulok/_modulok/_modulokkomp.php','hu','af9c6363c60b96658c5d8e680bcb06a7');
INSERT INTO `bow_fordito` VALUES ('657','Kikapcsol','modulok/_modulok/_modulokkomp.php','hu','06946163663fd52b4daf5b05a8bee84d');
INSERT INTO `bow_fordito` VALUES ('658','Kikapcsolt modulok','modulok/_modulok/_modulok.php','hu','4cbc29a042fefa398cb757a573b8388c');
INSERT INTO `bow_fordito` VALUES ('659','Bekapcsolás','modulok/_modulok/_modulokkomp.php','hu','75fc8f7aa624214527e4ac23860c8b7f');
INSERT INTO `bow_fordito` VALUES ('660','Telepíthető modulok','modulok/_modulok/_modulok.php','hu','09a0192e7771a1f5e7e218e5851062bb');
INSERT INTO `bow_fordito` VALUES ('661','Frissítés készítő','modulok/updatemaker/init.php','hu','a5a17c8e3cd76e21870a3c92ffe93eb0');
INSERT INTO `bow_fordito` VALUES ('662','Frissítés készítő','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','dcd9b3dfad6ef2d2cf9c8fc59081452b');
INSERT INTO `bow_fordito` VALUES ('663','Frissítés készítése','modulok/updatemaker/updatemaker.php','hu','a3e859fd975eeb3a64124cc22bc02c23');
INSERT INTO `bow_fordito` VALUES ('664','Kiadás dátumtól: (YYYY-mm-dd HH:ii','modulok/updatemaker/updatemaker.php','hu','055f6cd0ca66e8b5a315bed7487573ff');
INSERT INTO `bow_fordito` VALUES ('665','Kiadás dátumtól: (YYYY-mm-dd HH:ii)','modulok/updatemaker/updatemaker.php','hu','e1fe245ec9eb27aced447d542ef2c862');
INSERT INTO `bow_fordito` VALUES ('666','Frissitési pldány létrejött itt: ','modulok/updatemaker/updatemaker.php','hu','f7399322380f50849e1bf5e43ee0726c');
INSERT INTO `bow_fordito` VALUES ('667','Frissítés feltöltése (ZIP)','modulok/updatemaker/updatemaker.php','hu','89ccc8b77173dc23555251516a30f4a6');
INSERT INTO `bow_fordito` VALUES ('668','Zip file','modulok/updatemaker/updatemaker.php','hu','0c6b328ade9fcb35ce878252fbf1c7b3');
INSERT INTO `bow_fordito` VALUES ('669','Frissitési példány létrejött itt: ','modulok/updatemaker/updatemaker.php','hu','1608d88b043f0f50deba46beb613e6f5');
INSERT INTO `bow_fordito` VALUES ('670','ZIP file feltöltését várjuk!','modulok/updatemaker/updatemaker.php','hu','da13855d26a46d610e991f9a939fd031');
INSERT INTO `bow_fordito` VALUES ('671','ZIP file feltöltése sikertelen!','modulok/updatemaker/updatemaker.php','hu','04ce165a10d166a3e9ad6eb0c2b3a1fe');
INSERT INTO `bow_fordito` VALUES ('672','Zip file kicsomagolásra került','modulok/updatemaker/updatemaker.php','hu','258f5121b22bd8e2556897c00909132b');
INSERT INTO `bow_fordito` VALUES ('673','A kitömörítés helye:','modulok/updatemaker/updatemaker.php','hu','e4bd4db243736a4205bde6700fc1860a');
INSERT INTO `bow_fordito` VALUES ('674','Modul bekapcsolva','modulok/_modulok/_modulok.php','hu','9a1194551886d4744b049a4ab220ac85');
INSERT INTO `bow_fordito` VALUES ('675','Kapcsolat űrlap','modulok/_widget/_widgetkomp.php','hu','de60d63bb50aee89af4fdcec895d47b6');
INSERT INTO `bow_fordito` VALUES ('676','Egyszerű kapcsolat doboz','modulok/_widget/_widgetkomp.php','hu','21437d248ca627b7d46f3663da86c259');
INSERT INTO `bow_fordito` VALUES ('677','Név','modulok/kapcsolatdoboz/html/kapcsolatdoboz_kapcsolatdoboz_form.php','hu','2c6011d4b49b90cc1379f1932dae8268');
INSERT INTO `bow_fordito` VALUES ('678','E-mail cím','modulok/kapcsolatdoboz/html/kapcsolatdoboz_kapcsolatdoboz_form.php','hu','939ab544ef4e4d827f0d6aa81b56f253');
INSERT INTO `bow_fordito` VALUES ('679','Üzenet','modulok/kapcsolatdoboz/html/kapcsolatdoboz_kapcsolatdoboz_form.php','hu','ca67c4ca42f60b14d9930618e7dc4f6e');
INSERT INTO `bow_fordito` VALUES ('680','Küldés','modulok/kapcsolatdoboz/html/kapcsolatdoboz_kapcsolatdoboz_form.php','hu','fa2b778236ba3934a952ff7c00b5a40f');
INSERT INTO `bow_fordito` VALUES ('681','Sikeresen elküldte üzenetét','modulok/kapcsolatdoboz/html/kapcsolatdoboz_kapcsolatdoboz_sended.php','hu','b893a7c0b5f2f1d4cb85545aa9cd26dc');
INSERT INTO `bow_fordito` VALUES ('682','Eszközök','modulok/translate/init.php','hu','beb1e8fd05978b66fd5146ad2375e53d');
INSERT INTO `bow_fordito` VALUES ('683','Eszközök','modulok/_menu/init.php','hu','b52f9292c2b255f4d1658ca85d750a91');
INSERT INTO `bow_fordito` VALUES ('684','Eszközök','modulok/_modulok/init.php','hu','5be6ce127001f9d4bf1e742870524016');
INSERT INTO `bow_fordito` VALUES ('685','Eszközök','modulok/_temak/init.php','hu','4d29d6e46aaf48a234804e7640e09a54');
INSERT INTO `bow_fordito` VALUES ('686','Lev','modulok/levelek/init.php','hu','1ef0e9f1bf367b8d9d6ae8de53baafbb');
INSERT INTO `bow_fordito` VALUES ('687','Eszk','modulok/levelek/init.php','hu','dd3cbaf7d035355ca891d01f45170e0e');
INSERT INTO `bow_fordito` VALUES ('688','Lev','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','63122656d3b2375ea214938af574045d');
INSERT INTO `bow_fordito` VALUES ('689','Levélkészítő','modulok/levelek/init.php','hu','9fa6a68df0f96b6f19db72285e736c1c');
INSERT INTO `bow_fordito` VALUES ('690','Eszközök','modulok/levelek/init.php','hu','73d38b6bb82f0b246750c73f3a66b6e6');
INSERT INTO `bow_fordito` VALUES ('691','Levélkészítő','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','3ca4fabac0baaf2473fd167e904e0134');
INSERT INTO `bow_fordito` VALUES ('692','Rendszerlevelek','modulok/levelek/levelek.php','hu','f2a093bc45c37cac8d7fe4f0e2fc2c95');
INSERT INTO `bow_fordito` VALUES ('693','T','modulok/levelek/levelek.php','hu','4ef957c54600cd0ddd652ecda8ee620d');
INSERT INTO `bow_fordito` VALUES ('694','V','modulok/levelek/levelek.php','hu','22c9de0bdb115a4dbe0a90fdfeefa53f');
INSERT INTO `bow_fordito` VALUES ('695','Szerkeszt','modulok/levelek/levelek.php','hu','f28435f9fe562b0ca8be2bca107e35f3');
INSERT INTO `bow_fordito` VALUES ('696','T','modulok/levelek/levelek.php','hu','c04f39b7c7344c99a309564d63641ca2');
INSERT INTO `bow_fordito` VALUES ('697','','modulok/levelek/levelek.php','hu','6b834a5b76d422d3f527b23c630a912f');
INSERT INTO `bow_fordito` VALUES ('698','Tárgy','modulok/levelek/levelek.php','hu','62b14e64e6df3affa2b7ac0c01059a50');
INSERT INTO `bow_fordito` VALUES ('699','Válaszcím','modulok/levelek/levelek.php','hu','4fee5f5cd2d6d74049d28698abb447cb');
INSERT INTO `bow_fordito` VALUES ('700','Szerkesztés','modulok/levelek/levelek.php','hu','4f1f20f58621da0831db6128ce0cb949');
INSERT INTO `bow_fordito` VALUES ('701','Törlés','modulok/levelek/levelek.php','hu','3e08f5632d06dc5b3ad9397963f2c708');
INSERT INTO `bow_fordito` VALUES ('702','Új levél létrehozása','modulok/levelek/levelek.php','hu','4a21497e0f473093760b59ed621ecb72');
INSERT INTO `bow_fordito` VALUES ('703','Levélszerkesztő','modulok/levelek/levelek.php','hu','3b700058f2275f34bee35300d009fbec');
INSERT INTO `bow_fordito` VALUES ('704','Levél szerkesztése','modulok/levelek/levelek.php','hu','4785d5d91150864c1bc75dd3dc79bc99');
INSERT INTO `bow_fordito` VALUES ('705','Válasz cím','modulok/levelek/levelek.php','hu','53012a84ee4b8c59679b1736d028eef6');
INSERT INTO `bow_fordito` VALUES ('706','Válasz név','modulok/levelek/levelek.php','hu','76b534ced8a2970470e9701e4d36d745');
INSERT INTO `bow_fordito` VALUES ('707','Szöveg','modulok/levelek/levelek.php','hu','38296a3991f7599ed82d26fcc25a3fea');
INSERT INTO `bow_fordito` VALUES ('708','Egyedi kulcs','modulok/levelek/levelek.php','hu','ba4fe3dd34df18bc1ba8d908b8d4039c');
INSERT INTO `bow_fordito` VALUES ('709','Frissítve','modulok/levelek/levelek.php','hu','c0e0bd555f9ea344e2df99c913739285');
INSERT INTO `bow_fordito` VALUES ('710','Translator','modulok/unicorn/osztaly_unicorn_futtatokomp.php','en','d706659e4b34658f40a67b41709670e4');
INSERT INTO `bow_fordito` VALUES ('711','Témakezelő','modulok/_temak/_temak.php','en','f20bacebaba90359cb3527264f8728dc');
INSERT INTO `bow_fordito` VALUES ('712','Táma könyvtár klónozó','modulok/_temak/_temakkomp.php','en','26a3798ad527018e431c7f964d035949');
INSERT INTO `bow_fordito` VALUES ('713','Téma könyvtár másolása ez alapján:','modulok/_temak/_temakkomp.php','en','311023cbfca8075d59007ffec5cf3f9d');
INSERT INTO `bow_fordito` VALUES ('714','Témakönyvtár','modulok/_temak/_temakkomp.php','en','da408bf5e227118a5ceba855107285c1');
INSERT INTO `bow_fordito` VALUES ('715','Másolás','modulok/_temak/_temakkomp.php','en','222ac71e4c01ce1f936c1a0f3e34a574');
INSERT INTO `bow_fordito` VALUES ('716','Kép','modulok/_temak/_temakkomp.php','en','bf779452f8aeea83d1406eeadff0c127');
INSERT INTO `bow_fordito` VALUES ('717','Név','modulok/_temak/_temakkomp.php','en','4c4894ff2a33f2ce015169436380de15');
INSERT INTO `bow_fordito` VALUES ('718','Szerző','modulok/_temak/_temakkomp.php','en','dbd748e4aeec33e080dfe71c48cc5ff5');
INSERT INTO `bow_fordito` VALUES ('719','Kiválasztás','modulok/_temak/_temakkomp.php','en','2381165a5f39c207f531796a617bd3b2');
INSERT INTO `bow_fordito` VALUES ('720','','modulok/fiokbeallitasok/formeditor.php','en','9e9d242aa555b1e2b3f8be0a91be7984');
INSERT INTO `bow_fordito` VALUES ('721','Új input hozzáadása a regisztrációs formhoz','modulok/fiokbeallitasok/formeditor.php','en','bad4a8240ab43b8d9e308c5978432ac4');
INSERT INTO `bow_fordito` VALUES ('722','Form elemek','modulok/fiokbeallitasok/init.php','en','88faaf8946a0611137e171fdd1e50e8a');
INSERT INTO `bow_fordito` VALUES ('723','Kötelező','modulok/fiokbeallitasok/formeditor.php','en','8d11c4859f228dade94434fb8ee2ab94');
INSERT INTO `bow_fordito` VALUES ('724','Ellenőrzés','modulok/fiokbeallitasok/formeditor.php','en','97f5be815fc62cbd63283a857ffe5cdf');
INSERT INTO `bow_fordito` VALUES ('725','Nincs','modulok/fiokbeallitasok/formeditor.php','en','220426218ed0d0ad201a8fc1e522508c');
INSERT INTO `bow_fordito` VALUES ('726','Minimum 3 karakter','modulok/fiokbeallitasok/formeditor.php','en','c2fcec53e7642a6fe2758ceb84d271a8');
INSERT INTO `bow_fordito` VALUES ('727','Weboldalcím','modulok/fiokbeallitasok/formeditor.php','en','3bdaf279c91b2c0b2adec326aa2b27a7');
INSERT INTO `bow_fordito` VALUES ('728','Egész szám','modulok/fiokbeallitasok/formeditor.php','en','3ca2d89cd528c0befdacefeb5333a552');
INSERT INTO `bow_fordito` VALUES ('729','Választható elemek (vesszővel elválasztva, kulcs:érték formában)','modulok/fiokbeallitasok/formeditor.php','en','bf83257596661534941d67fe5132d910');
INSERT INTO `bow_fordito` VALUES ('730','Példa: alma:Alma, korte:Körte, banan:Banán','modulok/fiokbeallitasok/formeditor.php','en','e5eaebc09c64bf376cae369f1857b255');
INSERT INTO `bow_fordito` VALUES ('731','Mentés','modulok/fiokbeallitasok/formeditor.php','en','46f088688d1fe350ab093ba928a116bd');
INSERT INTO `bow_fordito` VALUES ('732','Űrlap mentése sikeres!','modulok/fiokbeallitasok/formeditor.php','en','d293b36a04aca0e6b7e4c064c4b602eb');
INSERT INTO `bow_fordito` VALUES ('733','Mentett mezők','modulok/fiokbeallitasok/formeditor.php','en','03cc5746e68dbad85b420d6fe6a4af9e');
INSERT INTO `bow_fordito` VALUES ('734','Típus','modulok/fiokbeallitasok/formeditor.php','en','514f0a22f5a70abf1f3b6122c4ccc298');
INSERT INTO `bow_fordito` VALUES ('735','Szerkeszt','modulok/fiokbeallitasok/formeditor.php','en','b1c707ada273953449f20a675a34eb5c');
INSERT INTO `bow_fordito` VALUES ('736','Töröl','modulok/fiokbeallitasok/formeditor.php','en','bb695f5e697036083e4633beb09cb78a');
INSERT INTO `bow_fordito` VALUES ('737','Kötelező szószám (0 ha nem kötelező)','modulok/fiokbeallitasok/formeditor.php','en','0377dee7d80381944bda869e7ebccadf');
INSERT INTO `bow_fordito` VALUES ('738','Törlés sikeres!','modulok/fiokbeallitasok/formeditor.php','en','f2de4d07157a6800405b6da8ac1bb667');
INSERT INTO `bow_fordito` VALUES ('739','Regisztrációs oldal','modulok/fiokbeallitasok/init.php','hu','7ab0691f922f739f247805fa2ba72035');
INSERT INTO `bow_fordito` VALUES ('740','Post Widget','modulok/_widget/_widgetkomp.php','hu','27388c99e625f751d4c567294de055d3');
INSERT INTO `bow_fordito` VALUES ('741','Post kirakása widgetbe','modulok/_widget/_widgetkomp.php','hu','507d154c66a6f4d00b19900963e9846b');
INSERT INTO `bow_fordito` VALUES ('742','HTML a szöveg előtt','modulok/_szerkeszto/widgetek.php','hu','a57ca9a6c8b668ca5adc4dd8395abc20');
INSERT INTO `bow_fordito` VALUES ('743','HTML a szöveg után','modulok/_szerkeszto/widgetek.php','hu','e6933e401be422d40d993250cdc18d5c');
INSERT INTO `bow_fordito` VALUES ('744','Fiók oldal','modulok/fiokbeallitasok/init.php','hu','cd22df69aa8c38e91af326266eb49ae0');
INSERT INTO `bow_fordito` VALUES ('745','Általános beállítások','modulok/_beallitasok/_beallitasok.php','en','e842423e6ac3c47716c5ae02cf3f818a');
INSERT INTO `bow_fordito` VALUES ('746','A konfigurációs adatok szerkesztése befolyásolja az oldal működését, és el is ronthatja azt! Csak akkor szerkeszd, ha tudod mit csinálsz!','modulok/_beallitasok/_beallitasokkomp.php','en','e70d6a1ee0abc496baf5f4d80caf3552');
INSERT INTO `bow_fordito` VALUES ('747','E-mail bellítások','modulok/_beallitasok/_beallitasokkomp.php','en','c20841225f0b0cc2286bee94e15885cf');
INSERT INTO `bow_fordito` VALUES ('748','Kész!','modulok/_beallitasok/_beallitasokkomp.php','en','8ff08b9ea0ebb893634ca8c5b242a37a');
INSERT INTO `bow_fordito` VALUES ('749','Facebook','modulok/_beallitasok/_beallitasokkomp.php','en','3d23008c951ad86412b7c2fbd7c64fb9');
INSERT INTO `bow_fordito` VALUES ('750','Rendszer','modulok/_beallitasok/_beallitasokkomp.php','en','4b66e9b7ff74bbc9eed234b87a6cdbdc');
INSERT INTO `bow_fordito` VALUES ('751','Weboldal általános beállítások','modulok/_beallitasok/_beallitasokkomp.php','en','432ee6df535d40beb38f722400c77032');
INSERT INTO `bow_fordito` VALUES ('752','Oldal megtekintése','osztaly/osztaly_html.php(355) : eval()\'d code','en','4aca048608872766ea1a2b848a4d1889');
INSERT INTO `bow_fordito` VALUES ('753','Feliratkozásaid','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_feliratkozas.php','en','351d681a85772aea89281b9e23cb9a42');
INSERT INTO `bow_fordito` VALUES ('754','Ha itt bepipálod az adott témaköröket, akkor értesítést kaphatsz az ebben a témában felkerülő újabb írásokról.','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_feliratkozas.php','en','bc5329a6a8d32a9c022743c1535102b4');
INSERT INTO `bow_fordito` VALUES ('755','Mentem a beállításokat','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_feliratkozas.php','en','7e131c3e4b9bd4b83aa63fa24eb54dc9');
INSERT INTO `bow_fordito` VALUES ('756','Név','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_alapadatok.php','en','a8b454e9b12018718488ca000e8995f6');
INSERT INTO `bow_fordito` VALUES ('757','E-mail','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_alapadatok.php','en','f1ec0506f0351ffae27d1b16aff3e5ac');
INSERT INTO `bow_fordito` VALUES ('758','Jelszó','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_alapadatok.php','en','f7426d21f93d5647338dd111d69ab650');
INSERT INTO `bow_fordito` VALUES ('759','Csoport','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_alapadatok.php','en','50cfb7c7acbf8983014da1a454983ac9');
INSERT INTO `bow_fordito` VALUES ('760','Weboldal','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_alapadatok.php','en','bb8dbbe9dc9d2895d7f770b71880456d');
INSERT INTO `bow_fordito` VALUES ('761','Mentés','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_alapadatok.php','en','c4be0a06808f67d2151b9a869ce86861');
INSERT INTO `bow_fordito` VALUES ('762','Regisztráció','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_regisztracio.php','en','ad0c11339a78c084c23c3599409662a4');
INSERT INTO `bow_fordito` VALUES ('763','Neved','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_regisztracio.php','en','ca87be7309fe7c4e67e9accaa2a44d5d');
INSERT INTO `bow_fordito` VALUES ('764','Email','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_regisztracio.php','en','5507c136246b9a02a9bbd2c72b059619');
INSERT INTO `bow_fordito` VALUES ('765','Jelszó','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_regisztracio.php','en','d6378e3add7abd58c7b2933a844341d5');
INSERT INTO `bow_fordito` VALUES ('766','Jelszó újra','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_regisztracio.php','en','c9312895e9752c329714a0f88504f039');
INSERT INTO `bow_fordito` VALUES ('767','Írd be az eredményt','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasokkomp_regisztracio.php','en','1dc1431e37f97162ade4d38238ead75c');
INSERT INTO `bow_fordito` VALUES ('768','Belépek','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasok_belepes.php','en','44a29afcce5368465a35312cca6c6131');
INSERT INTO `bow_fordito` VALUES ('769','Belépés Facebook-kal','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasok_belepes.php','en','02e65f6a01daa43e7a7554f98bdc6ba7');
INSERT INTO `bow_fordito` VALUES ('770','Hibás adatok','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasok_belepes.php','en','d684c6d9934b7f5b7d76428835c92d4d');
INSERT INTO `bow_fordito` VALUES ('771','Elfelejtett jelszó','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasok_belepes.php','en','412486552368a739a32a8b6c8d9e436d');
INSERT INTO `bow_fordito` VALUES ('772','Regisztráció','modulok/fiokbeallitasok/html/fiokbeallitasok_fiokbeallitasok_belepes.php','en','04cc1f32a774072ef9ae5e88d32c8cc2');
INSERT INTO `bow_fordito` VALUES ('773','Blogoldal címe','modulok/blogmotor/init.php','hu','826eb0c66d62dbc9850271cda647f10e');
INSERT INTO `bow_fordito` VALUES ('774','Alapértelmezett','modulok/blogmotor/init.php','hu','d908dc12303089f9732eab3211ee8799');
INSERT INTO `bow_fordito` VALUES ('775','Blogoldal beállítása','modulok/blogmotor/init.php','hu','7bdc879d1f87620729d5090e5e8d7656');
INSERT INTO `bow_fordito` VALUES ('776','Cimke kiválasztása','modulok/blogmotor/init.php','hu','8783bfb0b8980c92d43485443842792b');
INSERT INTO `bow_fordito` VALUES ('777','Cikmék létrehozása itt:','modulok/blogmotor/init.php','hu','8992a259829a81514af77c0e8dfae3fe');
INSERT INTO `bow_fordito` VALUES ('778','Megjelenítés sablonja','modulok/blogmotor/init.php','hu','bd671632d0fc214e86fe6cdd0b26764e');
INSERT INTO `bow_fordito` VALUES ('779','Alapértelmezett sablon a blogmotor modul html mappájában','modulok/blogmotor/init.php','hu','98194dd4f13cd3075328471c7798f6fa');
INSERT INTO `bow_fordito` VALUES ('780','Csak bevezető megjelenítése','modulok/blogmotor/init.php','hu','121c9442a87d0a27f1a8134a41523312');
INSERT INTO `bow_fordito` VALUES ('781','Nem','modulok/blogmotor/init.php','hu','f20f80c4a50bfa39b1386409aebb39c6');
INSERT INTO `bow_fordito` VALUES ('782','Igen','modulok/blogmotor/init.php','hu','24a27ccf824fd38b27f8f900c4873651');
INSERT INTO `bow_fordito` VALUES ('783','Megjelenített cikkek száma','modulok/blogmotor/init.php','hu','66d360eb5a8bdf5843f7f88cde103ed1');
INSERT INTO `bow_fordito` VALUES ('784','Rendezés','modulok/blogmotor/init.php','hu','4799de885ec10a17c2dc4d686c82171f');
INSERT INTO `bow_fordito` VALUES ('785','Sablon módósító','modulok/blogmotor/init.php','hu','41542a4f9f9db2cb28cf03023b0d92bd');
INSERT INTO `bow_fordito` VALUES ('786','Alapértelmezett sablon a blogmotor modul html mappájában van, ha nem adsz meg semmit, az lesz a megjelenítő, ha megadsz akkor létrehoz egy módosítható másolatot a template könyvtárában','modulok/blogmotor/init.php','hu','a50584186049eb638e6a461de0211e1e');
INSERT INTO `bow_fordito` VALUES ('787','A konfigurációs értékek megváltoztak!','modulok/_beallitasok/_beallitasok.php','hu','ec1a342ccade04621c068e82c7229f2d');
INSERT INTO `bow_fordito` VALUES ('788','Mini kosár','modulok/_widget/_widgetkomp.php','hu','b220c18f5f577cbf754a6bc48172fe59');
INSERT INTO `bow_fordito` VALUES ('789','Áruház főoldal','modulok/_aruhaz/aruhazoldalak.php','hu','cf0a4e3c3c7dc61d1717b6179fe2c6be');
INSERT INTO `bow_fordito` VALUES ('790','Termékek','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_fejlec.php','hu','d5b03dcdeb2c88a3a394e7a64d2f21c7');
INSERT INTO `bow_fordito` VALUES ('791','Lapok','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_lapozo.php','hu','3c468ded33adf49c607d0288555b0121');
INSERT INTO `bow_fordito` VALUES ('792','Nettó ár','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_termekkartya.php','hu','c68d086a627c60469b95ef6a1a6f1b45');
INSERT INTO `bow_fordito` VALUES ('793','Ár ÁFÁ-val','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_termekkartya.php','hu','43a237a52494e0bebb0b75b0dc76c08c');
INSERT INTO `bow_fordito` VALUES ('794','termekNincs','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_termekkartya.php','hu','5537bdc269ec76889bb55aa3b88405c2');
INSERT INTO `bow_fordito` VALUES ('795','Elfogyott','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_termekkartya.php','hu','f46eea829a859feb5f294d55cac4d6ec');
INSERT INTO `bow_fordito` VALUES ('796','Bővebb információk','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklista_termekkartya.php','hu','53e9c2ee6031e1c96bd228658013e05a');
INSERT INTO `bow_fordito` VALUES ('797','Sajnos nem találom a terméket!','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_nincs_ilyen_termek.php','hu','eee7ecced458f1036d5a27821ba9e730');
INSERT INTO `bow_fordito` VALUES ('798','Nettó ár','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','9b0a672163821cd9b29e7f217369b492');
INSERT INTO `bow_fordito` VALUES ('799','Ár ÁFÁ-val','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','4619d66b5e7938107e6f73414475f502');
INSERT INTO `bow_fordito` VALUES ('800','A termék sajnos kifogyott, kérem érdeklődjön irodánkban az elérhetőségét illetően:','modulok/_aruhaz/html/_aruhaz_osztaly_termek_termeklap_erdeklodoform.php','hu','bab7f493e81214a906d06c40624e0226');
INSERT INTO `bow_fordito` VALUES ('801','Név','modulok/_aruhaz/html/_aruhaz_osztaly_termek_termeklap_erdeklodoform.php','hu','8a526188b38af2690d8458b3c0c59785');
INSERT INTO `bow_fordito` VALUES ('802','E-mail cím','modulok/_aruhaz/html/_aruhaz_osztaly_termek_termeklap_erdeklodoform.php','hu','8504663a9b0b379dc71d2cf920d7fe04');
INSERT INTO `bow_fordito` VALUES ('803','Üzenet','modulok/_aruhaz/html/_aruhaz_osztaly_termek_termeklap_erdeklodoform.php','hu','87a1205dfc8663fc866d7baf51fab7a1');
INSERT INTO `bow_fordito` VALUES ('804','Küldés','modulok/_aruhaz/html/_aruhaz_osztaly_termek_termeklap_erdeklodoform.php','hu','6476c86e3cab9735f405ffd664a5b9f2');
INSERT INTO `bow_fordito` VALUES ('805','Érdeklődését továbbíitottam.','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_erdeklodoform_sikeres_kuldes.php','hu','73b4023880d1f1f8545baf9b12612531');
INSERT INTO `bow_fordito` VALUES ('806','A termék sajnos kifogyott, kérem érdeklődjön irodánkban az elérhetőségét illetően:','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_erdeklodoform.php','hu','56fbebe71d81a62acd2d0436984aa1cf');
INSERT INTO `bow_fordito` VALUES ('807','Név','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_erdeklodoform.php','hu','e0dfdb39944b2e4fd7415e5bb9c50723');
INSERT INTO `bow_fordito` VALUES ('808','E-mail cím','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_erdeklodoform.php','hu','8bc569fde40a81f61d66b631209846a4');
INSERT INTO `bow_fordito` VALUES ('809','Üzenet','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_erdeklodoform.php','hu','f5da52a7cf508617ecfa3ddd3d5f0434');
INSERT INTO `bow_fordito` VALUES ('810','Küldés','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_erdeklodoform.php','hu','9576d289c177bdb81a574f7a2fa2cef6');
INSERT INTO `bow_fordito` VALUES ('811','Helytelen E-mail cím','modulok/_aruhaz/osztaly/osztaly_kosar.php','hu','67133c1e8c93d7e8dfdfcaddcfa69baa');
INSERT INTO `bow_fordito` VALUES ('812','Típus','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','8ae42d8bf17e7be2bed7f042829bdce7');
INSERT INTO `bow_fordito` VALUES ('813','További jellemzők','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','2658058ee65c8f7ce5baad2baec492bc');
INSERT INTO `bow_fordito` VALUES ('814','Cikkszám','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','7afecc48d722cb31f6c753496b0f2c47');
INSERT INTO `bow_fordito` VALUES ('815','Opci','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_kosar_mennyiseg_hozzaad.php','hu','934cb8050cbf29e2ec4b11f7dcc8d158');
INSERT INTO `bow_fordito` VALUES ('816','Opciók','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_kosar_mennyiseg_hozzaad.php','hu','dcfdaaf156d06a88c171d1b58bf2a48e');
INSERT INTO `bow_fordito` VALUES ('817','Nincs kiválasztva','modulok/_aruhaz/html/_aruhaz_osztaly_termek_opcio_select.php','hu','361d2bd1818e74cfba5ffcf86dac8799');
INSERT INTO `bow_fordito` VALUES ('818','Opciok','modulok/_aruhaz/html/_aruhaz_osztaly_termek_opcio_select.php','hu','453b756b986bbbf1c5eb16973b3e8f97');
INSERT INTO `bow_fordito` VALUES ('819','Add meg az összeget:','modulok/kapcsolatdoboz/html/kapcsolatdoboz_kapcsolatdoboz_form.php','hu','0cde98fa282d3c607abb7abdb0905840');
INSERT INTO `bow_fordito` VALUES ('820','Nem megfelelő ellenőrzőkód','modulok/kapcsolatdoboz/kapcsolatdoboz.php','hu','cc1f8165476210daab388ef95992af54');
INSERT INTO `bow_fordito` VALUES ('821','Nem megfelelő E-mail cím','modulok/kapcsolatdoboz/kapcsolatdoboz.php','hu','527c0bbc9a19c1340191146a19d36113');
INSERT INTO `bow_fordito` VALUES ('822','Adja meg a kívánt jellemzőket','modulok/_aruhaz/html/_aruhaz_osztaly_termek_termek_urlap.php','hu','a62b920fe04e58624f8eaef3c0d2be90');
INSERT INTO `bow_fordito` VALUES ('823','Scribd dokumentum ID','modulok/scribd/init.php','hu','e52f4c0410a60a9566fd67bd8d3cce3a');
INSERT INTO `bow_fordito` VALUES ('824','Beillesztés','modulok/scribd/init.php','hu','e01d26e43cdc9f86bd10b3e3be3d3bdc');
INSERT INTO `bow_fordito` VALUES ('825','Minikosár','modulok/_widget/_widgetkomp.php','hu','579e70a674a4c75a04c4f48a967cdf9f');
INSERT INTO `bow_fordito` VALUES ('826','Kosár widget','modulok/_widget/_widgetkomp.php','hu','4255afa1760842a61a3c7c556631ad19');
INSERT INTO `bow_fordito` VALUES ('827','','modulok/_widget/_widgetkomp.php','hu','ab13913d55b4f36b9b65bb5c6610b05b');
INSERT INTO `bow_fordito` VALUES ('828','Nincs beállítási lehetőség...','modulok/_widget/ajaxwidgetunicorn.php','hu','7e8b6ff6e00a10c9eaece9e775870d36');
INSERT INTO `bow_fordito` VALUES ('829','Pénznemválasztó','modulok/_aruhaz/widgetek.php','hu','4a48cd6d21316cc39fcac2a9430c5c82');
INSERT INTO `bow_fordito` VALUES ('830','Nincs megjelenítve','modulok/_aruhaz/widgetek.php','hu','61034eb77f8f765995e9012a3d15571f');
INSERT INTO `bow_fordito` VALUES ('831','Megjelenítve','modulok/_aruhaz/widgetek.php','hu','6ce24594777246d27dd0104e5efac07f');
INSERT INTO `bow_fordito` VALUES ('832','Megjelenítés','modulok/_aruhaz/widgetek.php','hu','c22b3ffcfdbdad8c1145801da8c44d1d');
INSERT INTO `bow_fordito` VALUES ('833','Terméklista','modulok/_aruhaz/widgetek.php','hu','3c97cb2d1ec4c9d86394ec0ab906e221');
INSERT INTO `bow_fordito` VALUES ('834','Egyszerű','modulok/_aruhaz/widgetek.php','hu','10abdb076888f2b730cd927136984462');
INSERT INTO `bow_fordito` VALUES ('835','Kosár','modulok/_aruhaz/html/_aruhaz_widgetek_minikosar.php','hu','372e24b91531dc0f02ac7b6e42549ec5');
INSERT INTO `bow_fordito` VALUES ('836','Az Ön kosara még üres','modulok/_aruhaz/osztaly/osztaly_kosar.php','hu','7cc5f9cb899ce1462453ef81abe085ca');
INSERT INTO `bow_fordito` VALUES ('837','Kosár','modulok/_aruhaz/html/_aruhaz_widgetek_widgetkosar.php','hu','906408b14578b40a0196e33c506a0456');
INSERT INTO `bow_fordito` VALUES ('838','Még nincs termék a kosarában','modulok/_aruhaz/html/_aruhaz_widgetek_widgetkosar.php','hu','09b39dc410fe37093d92989fb80a0986');
INSERT INTO `bow_fordito` VALUES ('839','Contact Form beállítás','modulok/unicorn/osztaly_unicorn_futtatokomp.php','hu','6c24f58ab3da021c3cf92579d111040b');
INSERT INTO `bow_fordito` VALUES ('840','Kapcsolat űrlap szerkesztése','modulok/contact/init.php','hu','dce413494ab7adeb5d4d91c9e8be6961');
INSERT INTO `bow_fordito` VALUES ('841','Űrlapok létrehozása és szerkesztése','modulok/contact/contactformunicorn.php','hu','1dedb900df080928b6873e572d0a83d6');
INSERT INTO `bow_fordito` VALUES ('842','A modul segítségével létrehozhatsz kapcsolati űrlapokat, melyeket azután a post editorban shortcode-dal hozzáadhatsz az oldaladhoz.','modulok/contact/contactformunicorn.php','hu','9351876c0257168c223912e8243ad6ac');
INSERT INTO `bow_fordito` VALUES ('843','Űrlap éltrehozása','modulok/contact/contactformunicorn.php','hu','fc5c4693809baf9b8f8e3c153146892c');
INSERT INTO `bow_fordito` VALUES ('844','Cím','modulok/contact/contactformunicorn.php','hu','2febf1b6ef27c03ed3c83fcde93f3806');
INSERT INTO `bow_fordito` VALUES ('845','Címzett e-mail','modulok/contact/contactformunicorn.php','hu','d5b1f493e8bbcafb679e4f3547cc7f5b');
INSERT INTO `bow_fordito` VALUES ('846','Mentés utáni köszönet','modulok/contact/contactformunicorn.php','hu','61075543fab6449a7068d4afb2593fad');
INSERT INTO `bow_fordito` VALUES ('847','Mentés után átirányítás (hagyd üresen ha nem szüksges);','modulok/contact/contactformunicorn.php','hu','9f14396f40501497c2a7a901f9ffd37e');
INSERT INTO `bow_fordito` VALUES ('848','Mentés sikeres','modulok/contact/contactformunicorn.php','hu','bc3002a7a96e4740fc8c96999ccd7cc9');
INSERT INTO `bow_fordito` VALUES ('849','Hozzáadhatsz mezőket az űrlaphoz.','modulok/contact/contactformunicorn.php','hu','e7a46e97a9129e01e90c8dcb3cc66fdd');
INSERT INTO `bow_fordito` VALUES ('850','Szövegcímkék','modulok/contact/contactformunicorn.php','hu','0c00839a1a6c70141389619f2ee4a974');
INSERT INTO `bow_fordito` VALUES ('851','Cimke','modulok/contact/contactformunicorn.php','hu','4c2ca2852ab1a5909e386650ffd2634b');
INSERT INTO `bow_fordito` VALUES ('852','Szerkesztés','modulok/contact/contactformunicorn.php','hu','be6fd97fe1482c946576c187d913e577');
INSERT INTO `bow_fordito` VALUES ('853','Törlés','modulok/contact/contactformunicorn.php','hu','15f5ea29e81e7690773957f941215c3e');
INSERT INTO `bow_fordito` VALUES ('854','Új cimke','modulok/contact/contactformunicorn.php','hu','2445244c01891bd7584ec9eb63082644');
INSERT INTO `bow_fordito` VALUES ('855','Mezők','modulok/contact/contactformunicorn.php','hu','d0e3f31643fff4ba5ebc11a26265fcac');
INSERT INTO `bow_fordito` VALUES ('856','Cim','modulok/contact/contactformunicorn.php','hu','e2e605308377b8aeaee050fc7edb0047');
INSERT INTO `bow_fordito` VALUES ('857','Új űrlap','modulok/contact/contactformunicorn.php','hu','5fecb830c3c697da042191c709a6c3e5');
INSERT INTO `bow_fordito` VALUES ('858','Köszönjük érdeklődését, hamarosan keresni fogjuk.','modulok/contact/contactformunicorn.php','hu','5d0df0023d2fed02edf23f34d986c62b');
INSERT INTO `bow_fordito` VALUES ('859','Űrlap léltrehozása','modulok/contact/contactformunicorn.php','hu','f5c1e7070878177035f5e91a4843bc7d');
INSERT INTO `bow_fordito` VALUES ('860','Törlés sikeres','modulok/contact/contactformunicorn.php','hu','4dd5caba80d81f9aeeb407aa195b7b36');
INSERT INTO `bow_fordito` VALUES ('861','A kijelölt elemeket eltávolítottam!','modulok/contact/contactformunicorn.php','hu','5c59648bcde5c30e6502935b449d6c85');
INSERT INTO `bow_fordito` VALUES ('862','Mezők szerkesztése','modulok/contact/contactformunicorn.php','hu','82a9dbd2af087703da1efc637a4a7ed6');
INSERT INTO `bow_fordito` VALUES ('863','Mező hozzáadása:','modulok/contact/contactformunicorn.php','hu','cbf105fe9ee127d10b87b076efd3bec2');
INSERT INTO `bow_fordito` VALUES ('864','Hozzáadom','modulok/contact/contactformunicorn.php','hu','82814508e811581af782fdd94943da67');
INSERT INTO `bow_fordito` VALUES ('865','Ellenőrzés','modulok/contact/contactformunicorn.php','hu','4ff9c923ec882348758e7481eddbd1f3');
INSERT INTO `bow_fordito` VALUES ('866','Lehetőségek, vesszővel elválasztva: minchar(katarkerszám),maxchar(karakterszám), mask(Jquery.inputmask), date(dateformat)','modulok/contact/contactformunicorn.php','hu','aaa0ee026d140ff46029c5baa95bd3b6');
INSERT INTO `bow_fordito` VALUES ('867','Extra attributum','modulok/contact/contactformunicorn.php','hu','825ba28b5528eeaf7d31291ad9e0b2db');
INSERT INTO `bow_fordito` VALUES ('868','Hozzáadható bármilyen attributum (stílus, id stb)','modulok/contact/contactformunicorn.php','hu','3b78ccaae64cd658df9041c817f62b8d');
INSERT INTO `bow_fordito` VALUES ('869','Segítség','modulok/contact/contactformunicorn.php','hu','f77425fbff4c26741ca0049f557ec16b');
INSERT INTO `bow_fordito` VALUES ('870','Felirat','modulok/contact/contactformunicorn.php','hu','d05910c86d71481ab3a8ba4e72ce28f3');
INSERT INTO `bow_fordito` VALUES ('871','Mező felvitel sikeres','modulok/contact/contactformunicorn.php','hu','2e4ac1d0d3335bca43c199927d9e1d9f');
INSERT INTO `bow_fordito` VALUES ('872','Az új mező a mezőlista végén található','modulok/contact/contactformunicorn.php','hu','93d05a559011ca9d3814574aa535218a');
INSERT INTO `bow_fordito` VALUES ('873','Mező felvitel','modulok/contact/contactformunicorn.php','hu','2e999be2da525bce6504784ca6653cbe');
INSERT INTO `bow_fordito` VALUES ('874','Mező melletti felirat','modulok/contact/contactformunicorn.php','hu','e0f480df647b9757dd569b6c9d00d37a');
INSERT INTO `bow_fordito` VALUES ('875','Segítség szöveg','modulok/contact/contactformunicorn.php','hu','7d9a7025fc23ecb211390db9af6ba198');
INSERT INTO `bow_fordito` VALUES ('876','Nincs még mező ebben az űrlapban!','modulok/contact/contactformunicorn.php','hu','c65199984d6533ed9b8c9e7a8a2a8c81');
INSERT INTO `bow_fordito` VALUES ('877','Töröld ezt az űrlapelemet!','modulok/contact/contactformunicorn.php','hu','8ce7e3bb0c469b3808e9d15dc432a781');
INSERT INTO `bow_fordito` VALUES ('878','sorrend','modulok/contact/contactformunicorn.php','hu','0cded7aa51ae5e29d3d10cdffa16098e');
INSERT INTO `bow_fordito` VALUES ('879','Lehetőségek, vesszővel elválasztva: minchar(katarkerszám),maxchar(karakterszám)','modulok/contact/contactformunicorn.php','hu','4b2c767d29973f04fb4575fa55dd3f07');
INSERT INTO `bow_fordito` VALUES ('880','Lista','modulok/contact/contactformunicorn.php','hu','2f34ce1bb26199285d0f3d753d747e1a');
INSERT INTO `bow_fordito` VALUES ('881','Az opciók érték:felirat párokban, vesszővel elválasztva, ! jellel jelőlve a selected elemet: alma:Alma,korte:Körte, !eger:Egér','modulok/contact/contactformunicorn.php','hu','4752bfcd2afd650d17e6da5ab3e9afda');
INSERT INTO `bow_fordito` VALUES ('882','Radio gombok melletti felirat','modulok/contact/contactformunicorn.php','hu','d99fe156d6db23cc5c526a244cc71c97');
INSERT INTO `bow_fordito` VALUES ('883','Checkbox gombok melletti felirat','modulok/contact/contactformunicorn.php','hu','7116aacfdb88139b3a8d965d1e2130dc');
INSERT INTO `bow_fordito` VALUES ('884','Űrlap kiválasztása','modulok/contact/init.php','hu','38dd892d29c784ed1f8d777a26a42931');
INSERT INTO `bow_fordito` VALUES ('885',' file nem elérhető','kozos_fuggvenyek.php','hu','3c4165bf7465979e879917df5e430e8d');
INSERT INTO `bow_fordito` VALUES ('886','Űrlapok','modulok/contact/contactformunicorn.php','hu','09ed38292d2e3bb89c96118a15080fe8');
INSERT INTO `bow_fordito` VALUES ('887','Küldés','modulok/contact/html/contact_contact_submit.php','hu','e94faaf50a257f27f773d296718c86ce');
INSERT INTO `bow_fordito` VALUES ('888','Beilleszt','modulok/contact/init.php','hu','2b01f57eeccd4674d6354cf95c6ca7d4');
INSERT INTO `bow_fordito` VALUES ('889','Hibás E-mail cím','modulok/contact/contactajax.php','hu','59b1a158ef97dcc96077be29d4dcee72');
INSERT INTO `bow_fordito` VALUES ('890','Kérjük, legalább 20 karakter hosszú legyen','modulok/contact/contactajax.php','hu','a5be2369c03e431195cc496b8cee6bc2');
INSERT INTO `bow_fordito` VALUES ('891','Kérjük, legalább 3 karakter hosszú legyen','modulok/contact/contactajax.php','hu','d0b1ebe692c44e3390f88e5f2cdc5172');
INSERT INTO `bow_fordito` VALUES ('892','Kérjük, maximum 100 karakter hosszú legyen','modulok/contact/contactajax.php','hu','6d5cc57a83d183bc406528e36cb7d0a2');
INSERT INTO `bow_fordito` VALUES ('893','Köszönjük érdeklődését, hamarosan keresni fogjuk.','modulok/contact/contactajax.php','hu','a123b6c73f822303cd0f3cae3fcd7054');
INSERT INTO `bow_fordito` VALUES ('894','Kapcsolat üzenet érkezett','modulok/contact/contactajax.php','hu','36c3ed719291f577abb972c9a13724e3');
INSERT INTO `bow_fordito` VALUES ('895','Oldal megtekintése','osztaly/osztaly_html.php(359) : eval()\'d code','hu','d84b04960eeb970ea3a2a92134a91d7d');
INSERT INTO `bow_fordito` VALUES ('896','Adminisztráció','osztaly/osztaly_html.php(359) : eval()\'d code','hu','fa9da280e30396a57f6da7b237043cbc');
INSERT INTO `bow_fordito` VALUES ('897','Vezérlőpult','osztaly/osztaly_html.php(359) : eval()\'d code','hu','14edacaffe4f76f5115d194311c99753');
INSERT INTO `bow_fordito` VALUES ('898','Főoldal','osztaly/osztaly_html.php(359) : eval()\'d code','hu','e4dbfe6ff45d702aa43f8d9d16287a53');
INSERT INTO `bow_fordito` VALUES ('899','Téma könyvtár másolása?','modulok/updatemaker/updatemaker.php','hu','7fc8d873b7f7e36d61381867800990d4');
INSERT INTO `bow_fordito` VALUES ('900','Ebben a kategóriában nincs termék!','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_kategoriaoldal_nicstermek.php','hu','26a2d632e8e24b92e8a03ac471f454c6');
INSERT INTO `bow_fordito` VALUES ('901','Term','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_kosar_mennyiseg_hozzaad.php','hu','94564f08e911e490aac81b25c9d57681');
INSERT INTO `bow_fordito` VALUES ('902','Krem t','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_kosar_mennyiseg_hozzaad.php','hu','80adabba9c32f4d2bceb261992533f0d');
INSERT INTO `bow_fordito` VALUES ('903','Termék hozzáadása a kosárhoz','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_kosar_mennyiseg_hozzaad.php','hu','c46b1b9300f681e8be6c82e966f92ebf');
INSERT INTO `bow_fordito` VALUES ('904','Kérem türelmét...','modulok/_aruhaz/html/_aruhaz_osztaly_kosar_termeklap_kosar_mennyiseg_hozzaad.php','hu','9f2bb28db76cb549a88833aa1af76cdf');
INSERT INTO `bow_fordito` VALUES ('905','Kérem türelmét...','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap_javascript.php','hu','1b7fb6a190f3d3924ecd13820e02f4c6');
INSERT INTO `bow_fordito` VALUES ('906','Termék a kosárba került!','modulok/_aruhaz/ajax_kosar.php','hu','865b5a3ea34d062655eee00e39650a6d');
INSERT INTO `bow_fordito` VALUES ('907','Vissza','modulok/_aruhaz/ajax_kosar.php','hu','690b39a7649dfce4dc4d284d75db9d35');
INSERT INTO `bow_fordito` VALUES ('908','Kosár megtekintése','modulok/_aruhaz/ajax_kosar.php','hu','fce059302fb58d1e59b430ffa9afca36');
INSERT INTO `bow_fordito` VALUES ('909','Termékek száma: ','modulok/_aruhaz/osztaly/osztaly_kosar.php','hu','4aff2ff593174c27c2c36dd4f08cabe6');
INSERT INTO `bow_fordito` VALUES ('910','Összár: ','modulok/_aruhaz/osztaly/osztaly_kosar.php','hu','4ad3de1a8f00411b4e68d65954d20709');
INSERT INTO `bow_fordito` VALUES ('911','Nettó ár: ','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','7e7e6e87b1bfa4eac04c92f6983daee1');
INSERT INTO `bow_fordito` VALUES ('912','Bruttó ár: ','modulok/_aruhaz/html/_aruhaz_aruhazoldalak_termeklap.php','hu','c8b8bec203cfd2b5fd750d20dea4e718');
INSERT INTO `bow_fordito` VALUES ('913','Opciók','modulok/_aruhaz/html/_aruhaz_osztaly_termek_opcio_select.php','hu','2267d6d8f68951e6ff2099078f7098ca');
INSERT INTO `bow_fordito` VALUES ('914','Addja meg e-mail címét és jelszavát','osztaly/osztaly_html.php(359) : eval()\'d code','hu','ceec035a077474640cc9440fc8c62ac6');
INSERT INTO `bow_fordito` VALUES ('915','Jelszó','osztaly/osztaly_html.php(359) : eval()\'d code','hu','0b786aff091b8305db346c242f20cd62');
INSERT INTO `bow_fordito` VALUES ('916','Elfelejtett jelszó','osztaly/osztaly_html.php(359) : eval()\'d code','hu','241d389dfbd2329e06ac9d6ea4e8dd06');
INSERT INTO `bow_fordito` VALUES ('917','Addja meg e-mail címét, hogy elküldhessük a visszaállító kódot!','osztaly/osztaly_html.php(359) : eval()\'d code','hu','96bf8c81a6483ca7aebde1f039dc41ba');
INSERT INTO `bow_fordito` VALUES ('918','Vissza','osztaly/osztaly_html.php(359) : eval()\'d code','hu','0aa215db164bb07edecdb47e6e6c0ea8');
INSERT INTO `bow_fordito` VALUES ('919','Elküldés','osztaly/osztaly_html.php(359) : eval()\'d code','hu','116527d33205c4b8be8ff04965394894');
INSERT INTO `bow_fordito` VALUES ('920','Addja meg új jelszavát!','osztaly/osztaly_html.php(359) : eval()\'d code','hu','686707ef747abf7120d07774b3354ea0');
INSERT INTO `bow_fordito` VALUES ('921','Új jelszó','osztaly/osztaly_html.php(359) : eval()\'d code','hu','d1b8bcd8c07129f583c8d1c2724f5c0c');
INSERT INTO `bow_fordito` VALUES ('922','Módosítás','osztaly/osztaly_html.php(359) : eval()\'d code','hu','1d9d57b8c474f706ad4788c17a3f215a');
-- --------------------------------------------------------

--
-- Table structure for table `bow_hirlevel_feliratkozas`
--

DROP TABLE IF EXISTS `bow_hirlevel_feliratkozas`;

CREATE TABLE `bow_hirlevel_feliratkozas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `cimke` varchar(255) NOT NULL,
  `megerosito_kod` varchar(255) NOT NULL,
  `megerositve` tinyint(4) NOT NULL,
  `kikuldve` int(11) NOT NULL,
  `szoveg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_hirlevel_feliratkozas`
--

-- --------------------------------------------------------

--
-- Table structure for table `bow_hirlevel_keszlevelek`
--

DROP TABLE IF EXISTS `bow_hirlevel_keszlevelek`;

CREATE TABLE `bow_hirlevel_keszlevelek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kod` varchar(10) NOT NULL,
  `tartalom` text NOT NULL,
  `szoveg_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_hirlevel_keszlevelek`
--

-- --------------------------------------------------------

--
-- Table structure for table `bow_infoslider`
--

DROP TABLE IF EXISTS `bow_infoslider`;

CREATE TABLE `bow_infoslider` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kep` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `cim` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_infoslider`
--

INSERT INTO `bow_infoslider` VALUES ('2','1378726374497.jpg','','20');
INSERT INTO `bow_infoslider` VALUES ('3','1378726521484.jpg','','10');
INSERT INTO `bow_infoslider` VALUES ('4','1378726532307.jpg','','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_infoslider_info`
--

DROP TABLE IF EXISTS `bow_infoslider_info`;

CREATE TABLE `bow_infoslider_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `cim` varchar(255) COLLATE utf8_hungarian_ci NOT NULL,
  `leiras` text COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bow_infoslider_info`
--

INSERT INTO `bow_infoslider_info` VALUES ('6','2014-02-08','Hívogató','');
INSERT INTO `bow_infoslider_info` VALUES ('7','2014-02-22','Hívogató','');
INSERT INTO `bow_infoslider_info` VALUES ('8','2014-02-11','Nyílt nap','');
INSERT INTO `bow_infoslider_info` VALUES ('9','2014-02-19','Játékklub','');
INSERT INTO `bow_infoslider_info` VALUES ('10','2014-02-24','Farsang','');
INSERT INTO `bow_infoslider_info` VALUES ('11','2014-02-24','Kommunista diktatúra ál-dozatainak emléknapja','');
INSERT INTO `bow_infoslider_info` VALUES ('12','2014-02-25',' Tavaszváró szünet kezdete','25-től 28-ig');
INSERT INTO `bow_infoslider_info` VALUES ('13','2014-03-05','Befizetési napok','');
INSERT INTO `bow_infoslider_info` VALUES ('14','2014-03-07','Befizetési napok','');
INSERT INTO `bow_infoslider_info` VALUES ('15','2014-03-22','Tavaszi játszóház','');
INSERT INTO `bow_infoslider_info` VALUES ('16','2014-03-26','Nyílt nap','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_kapcsolat_uzenetek`
--

DROP TABLE IF EXISTS `bow_kapcsolat_uzenetek`;

CREATE TABLE `bow_kapcsolat_uzenetek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `uzenet` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `datum` datetime NOT NULL,
  `megtekintve` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_kapcsolat_uzenetek`
--

INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('1','teszt','bow4ex@gmail.com','teszt üzi, hajrá','2013-09-23 17:27:00','1');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('2','ceglédi Iván','ivan@alkoltal.hu','Teszt üzenet ez!','2013-09-25 12:25:00','1');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('3','ceglédi Iván','bowforex@gmail.com','Na haver, ezen még dolgoznod kell! de azért király!','2013-10-29 13:34:00','1');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('4','cEGLÉDI iván','ivan@zente.org','Halihó, ajrá ó.','2014-02-03 15:53:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('5','cEGLÉDI iván','ivan@zente.org','Halihó, ajrá ó.','2014-02-03 15:53:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('6','cEGLÉDI iván','ivan@zente.org','Halihó, ajrá ó.','2014-02-03 15:55:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('7','cEGLÉDI iván','ivan@zente.org','teszt üzi','2014-03-13 16:01:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('8','cEGLÉDI iván','ivan@zente.org','teszt üzi','2014-03-13 16:02:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('9','cEGLÉDI iván','ivan@zente.org','teszt üzi','2014-03-13 16:04:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('10','','','','2014-03-24 18:30:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('11','','ivan@zente.org','teszt','2014-03-24 18:32:00','0');
INSERT INTO `bow_kapcsolat_uzenetek` VALUES ('12','\'1\' or 1=1; --','ivan@zente.org','tezst','2014-04-16 09:20:00','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_levelek`
--

DROP TABLE IF EXISTS `bow_levelek`;

CREATE TABLE `bow_levelek` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `targy` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `valaszcim` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `valasznev` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `szoveg` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kulcs` varchar(50) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_levelek`
--

INSERT INTO `bow_levelek` VALUES ('1','Jelszó visszaállítási kérelem','kuka@zente.org','BowCms','<p><strong>Kedves Felhaszn&aacute;l&oacute;nk!</strong></p>\r\n<p>Valaki jelsz&oacute;helyre&aacute;ll&iacute;t&aacute;si k&eacute;relmet k&uuml;ld&ouml;tt oldalunkr&oacute;l. Amennyiben ez nem Te volt&aacute;l, nincs dolgod a tov&aacute;bbiakban.</p>\r\n<p>Ha Te k&eacute;rted az &uacute;j jelsz&oacute;t, k&eacute;rj&uuml;k kattints erre a linkere:</p>\r\n<p>|link|</p>\r\n<p>A link csak egyszer haszn&aacute;lhat&oacute;, ha a jelsz&oacute;vissza&aacute;ll&iacute;t&aacute;si folyamat megszakad, &uacute;jra kell k&eacute;rned azt honlapunkon!</p>\r\n<p>&nbsp;</p>\r\n<p>Sz&eacute;p napot k&iacute;v&aacute;n:</p>\r\n<p><strong><em>Bow CMS</em></strong></p>','jelszo_visszaallito');
INSERT INTO `bow_levelek` VALUES ('2','Fiókodat aktiváltuk!','kuka@zente.org','BowCms','<p><strong>Kedves |nev|,</strong></p>\r\n<p>&nbsp;</p>\r\n<p>Csoportmegh&iacute;v&aacute;sod l&eacute;trej&ouml;tt, mostant&oacute;l a Te csoportod:</p>\r\n<p><strong>|csoportnev|</strong></p>\r\n<p>&nbsp;</p>\r\n<p>&Uuml;dv&ouml;zlettel:</p>\r\n<p><em>BowCMS</em></p>','csoport_valasztas_er');
INSERT INTO `bow_levelek` VALUES ('3','Jelszó változtatás','kuka@zente.org','BowCms','<p><strong>Kedves |nev|</strong>,</p>\r\n<p>oldalunkon megv&aacute;ltoztattad jelszavadat. Az &uacute;j jelsz&oacute;: <strong>|jelszo|</strong>, legk&ouml;zelebb k&eacute;rj&uuml;k, m&aacute;r ezzel l&eacute;pj be.</p>\r\n<p>&Uuml;dv&ouml;zlettel:</p>\r\n<p><em>BowCMS</em></p>','palanta_jelszovaltoz');
INSERT INTO `bow_levelek` VALUES ('4','Kapcsolat ','fejlesztes@palanta.hu','Palánta Iskola','<p><strong>Kedves Adminisztr&aacute;tor</strong>,</p>\r\n<p>weboldaladr&oacute;l &uuml;zenet &eacute;rkezett:</p>\r\n<p>Felad&oacute;: |felado|</p>\r\n<p>&Uuml;zenete:</p>\r\n<p><strong>|uzenet|</strong></p>\r\n<p>&Uuml;dv&ouml;zlettel:</p>\r\n<p><em>BowCMS Weboldal Robot</em></p>','kapcsolatform_uzenet');
INSERT INTO `bow_levelek` VALUES ('5','Jelszó visszaállíítás','noreply@example.com','B-store Kft','<p><strong>Kedves felhaszn&aacute;l&oacute;nk,</strong></p>\r\n<p>&Ouml;n jelsz&oacute; vissza&aacute;ll&iacute;t&aacute;st ignyelt oldalunkon. Az &Ouml;n jelsz&oacute; vissza&aacute;ll&iacute;t&oacute; linkje:</p>\r\n<p><a href=\"|link|\">|link|</a></p>\r\n<p>&Uuml;dv&ouml;zlettel:</p>\r\n<p><strong>B-store Kft. weboldala</strong></p>\r\n<p>&nbsp;</p>','unicorn_jelzovisszaallitas');
INSERT INTO `bow_levelek` VALUES ('6','Adminisztrátorunk regisztrálta Önt!','info@bowcms.hu','Bow CMS','<p><strong>Kedves |nev|,</strong></p>\r\n<p>Adminisztr&aacute;torunk regisztr&aacute;lta &Ouml;nt oldalunkon, bel&eacute;p&eacute;si adatai:</p>\r\n<p>E-mail c&iacute;m: <strong>|email|</strong><br />Jelsz&oacute;: <strong>|jelszo|</strong><br /><br />L&aacute;togasson el oldalunkra!</p>\r\n<p>|weburl|</p>\r\n<p><strong>&Uuml;dv&ouml;zlettel:</strong><br /><strong>BOW CMS</strong></p>','reg_by_admin_mail');
INSERT INTO `bow_levelek` VALUES ('7','Érdeklődés kifogyott termékről','info@b-store.hu','B-store KFT.','<p><strong>&Eacute;rdeklőd&eacute;s Term&eacute;kről:</strong></p>\r\n<p>Term&eacute;k: |termek|</p>\r\n<p>&Eacute;rdeklődő adatai:</p>\r\n<p>N&eacute;v:&nbsp;<strong>|nev|<br /></strong>E-mail:&nbsp;<strong>|email|<br /></strong>Megjegyz&eacute;&eacute;s:&nbsp;<strong>|megjegyzes|</strong></p>\r\n<p>B-sore Web&aacute;ruh&aacute;z</p>','aruhaz_erdeklodes');
INSERT INTO `bow_levelek` VALUES ('8','Érdeklődés BowCms','info@thesupreme.me.uk','Bow Cms','<p><strong>Tisztelt Adminisztr&aacute;tor,</strong></p>\r\n<p>az oldalr&oacute;l &eacute;rdeklőd&eacute;s &eacute;rkezett. Kit&ouml;lt&ouml;tt adatok:</p>\r\n<p>|adat|</p>\r\n<p>Sz&eacute;p napot:</p>\r\n<p><strong>bowCMS</strong></p>','bowContact_admin');
-- --------------------------------------------------------

--
-- Table structure for table `bow_menuk`
--

DROP TABLE IF EXISTS `bow_menuk`;

CREATE TABLE `bow_menuk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menucsoport` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `link` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `kulso` tinyint(4) NOT NULL,
  `szint` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `cim` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nyelv` varchar(5) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL DEFAULT 'hu',
  `tipus` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_menuk`
--

INSERT INTO `bow_menuk` VALUES ('6','Főmenű','fooldal','0','0','0','Főoldal','hu','1');
INSERT INTO `bow_menuk` VALUES ('21','Főmenű','blog','0','0','20','Blog','hu','1');
INSERT INTO `bow_menuk` VALUES ('23','Főmenű','rolunk','0','0','30','Rólunk','hu','1');
INSERT INTO `bow_menuk` VALUES ('40','Főmenű','hirek','0','0','10','Hírek','hu','1');
INSERT INTO `bow_menuk` VALUES ('41','Főmenű','shop','0','0','40','Shop','hu','3');
-- --------------------------------------------------------

--
-- Table structure for table `bow_menuxcsoport`
--

DROP TABLE IF EXISTS `bow_menuxcsoport`;

CREATE TABLE `bow_menuxcsoport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) NOT NULL,
  `csoport_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_menuxcsoport`
--

INSERT INTO `bow_menuxcsoport` VALUES ('8','38','4');
INSERT INTO `bow_menuxcsoport` VALUES ('9','39','7');
INSERT INTO `bow_menuxcsoport` VALUES ('10','39','5');
INSERT INTO `bow_menuxcsoport` VALUES ('11','39','6');
INSERT INTO `bow_menuxcsoport` VALUES ('12','39','1');
-- --------------------------------------------------------

--
-- Table structure for table `bow_meta`
--

DROP TABLE IF EXISTS `bow_meta`;

CREATE TABLE `bow_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategoria` varchar(255) NOT NULL,
  `kulcs` varchar(255) NOT NULL,
  `meta_id` int(11) NOT NULL,
  `nev` varchar(100) NOT NULL,
  `ertek` text NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kategoria` (`kategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_meta`
--

INSERT INTO `bow_meta` VALUES ('1','contact_form','urlappeldany','0','','a:5:{s:2:\"id\";s:1:\"0\";s:3:\"cim\";s:22:\"Kapcsolat oldal űrlap\";s:5:\"email\";s:14:\"ivan@zente.org\";s:8:\"koszonet\";s:56:\"Köszönjük érdeklődését, hamarosan keresni fogjuk.\";s:8:\"redirect\";s:0:\"\";}','0');
INSERT INTO `bow_meta` VALUES ('2','contact_form','urlapelem','1','','a:5:{s:5:\"tipus\";s:4:\"text\";s:5:\"label\";s:15:\"Mi a mai menü?\";s:10:\"ellenorzes\";s:25:\"minchar(20), maxchar(100)\";s:4:\"attr\";s:0:\"\";s:4:\"help\";s:20:\"Mi lesz ma az ebéd?\";}','0');
INSERT INTO `bow_meta` VALUES ('3','contact_form','urlapelem','1','','a:5:{s:5:\"tipus\";s:5:\"input\";s:5:\"label\";s:12:\"Subidubidú?\";s:10:\"ellenorzes\";s:32:\"minchar(6), maxchar(10),password\";s:4:\"attr\";s:0:\"\";s:4:\"help\";s:17:\"Frédi és Béni?\";}','0');
INSERT INTO `bow_meta` VALUES ('4','contact_form','urlapelem','1','','a:5:{s:5:\"tipus\";s:6:\"select\";s:5:\"label\";s:13:\"Honnan tudod?\";s:5:\"lista\";s:21:\"egy:Egy,!ketto:Kettő\";s:4:\"attr\";s:0:\"\";s:4:\"help\";s:119:\"Ez egy olyan kérdés, amire mielőbb válaszolsz, annál jobb. Ha nem akarod megmondani, akkor menj te a jó édes....\";}','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_modulok`
--

DROP TABLE IF EXISTS `bow_modulok`;

CREATE TABLE `bow_modulok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `modul` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL COMMENT 'Modul útvonal',
  `funkcio` varchar(255) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL COMMENT 'Rövi leírás',
  `osztaly` varchar(50) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `metodus` varchar(50) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `csoport` int(11) NOT NULL,
  `modulnev` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL COMMENT 'Megjelenített név',
  `modulleiras` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL COMMENT 'Bővebb leírás',
  `beallito` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL COMMENT 'Admin file neve, modulkönyvtárban',
  `letiltva` tinyint(4) NOT NULL,
  `dinamikus_beallito` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `rendszermodul` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_modulok`
--

INSERT INTO `bow_modulok` VALUES ('1','peldaModul','Ez egy példamodul','peldaModul','kiiratas','0','Példa modul','Ez egy példa modul, amely szemlélteti a modulok felépítését','admin.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('2','_adminMenu','Adminisztrációs felület, modulok menüpontok','_adminMenu','modulkezeloLinkek','0','Modulkezelő hivatkozások','Adminisztrációs felületre helyezi el a modulkezelés lehetőségeit','','0','','1');
INSERT INTO `bow_modulok` VALUES ('3','_modulok','Modulok kezelése','_modulok','telepito','0','Modulok kezelése','','','0','','1');
INSERT INTO `bow_modulok` VALUES ('7','_modulok','Modulok listázása, szerkesztése','_modulok','lista','0','Modulok Adminisztrációs eszköz','Modulok listázása, ki be kapcsolása, általános beállítások','','0','','1');
INSERT INTO `bow_modulok` VALUES ('8','_oldalak','Oldalak felépítése','_oldalak','lista','0','Oldalkezelő','Oldalak elemeinek összeállítása','','0','','1');
INSERT INTO `bow_modulok` VALUES ('9','_szovegek','Egyszerűú szöveg megjelenítése','_szovegek','cikkMegjeleito','0','Szövegeksablonok és szöveg oldalmodul','Egyszerű szöveg oldalakra','beallito.php','0','oldal_beallito.php','0');
INSERT INTO `bow_modulok` VALUES ('10','_temak','Témák kezelése','_temak','lista','0','Témakezelő','','','0','','1');
INSERT INTO `bow_modulok` VALUES ('11','_beallitasok','Konfigurációs beállítások szerkesztése','_beallitasok','lista','0','Konfiguráció','Rendszer konfiguráció szerkesztése','','0','','1');
INSERT INTO `bow_modulok` VALUES ('12','_beepulok','Beépülő programok futtatása','_beepulok','lista','0','Bepülő szerkesztő','Bepülő modulok szerkesztése','','0','','1');
INSERT INTO `bow_modulok` VALUES ('13','_menu','Menücsoportok kezelése','_menu','menucsoportok','0','Menükezelő','Menücsoportok kezelése','','0','','1');
INSERT INTO `bow_modulok` VALUES ('14','InfoSlider','Képek és bejegyzéskivonatok megjelenítése','infoSlider','megjelenit','0','InfoSlider kép és szövegcsoport megjelenítő','Az InfoSlider képeket jelenít meg az oldalon és képújság szerűen szövegcimke szerint meghatározott szöveget és címet jelenít meg ','beallito.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('15','epitoKocka','Weblap elemek oldalakra','epitoKocka','megjelenit','0','epitoKocka html elemek','Oldal felépítéséhez hasznlható HTML elemek','beallito.php','0','oldal_beallito.php','0');
INSERT INTO `bow_modulok` VALUES ('16','idezetek','Idézetek megjelenítése adott szövegcimke alapján','idezetek','megjelenit','0','Idézetek','Szövegcimke alapján megadott szövegcsoportok véletlenszerű megjelenítése','','0','oldal_beallito.php','0');
INSERT INTO `bow_modulok` VALUES ('17','levelek','Levelek készítése','levelek','levelek','0','Levélsablon készítő','Levélstruktúrák létrehozása rendszer s hírlevelekhez','beallito.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('18','fiokbeallitasok','Felhasználói jelszó visszaállítás','fiokbeallitasok','jelszovisszaallitas','0','fiokbeallitasok','Felhasználói fiókok beállítások','','0','','0');
INSERT INTO `bow_modulok` VALUES ('19','facebook','Facebook kapcsolat létrehozása, kezelése','facebook','kapcsolodas','0','Facebook kapcsolása fiókhoz','Facebook kapcsolása a fiókhoz','','0','','0');
INSERT INTO `bow_modulok` VALUES ('20','fiokbeallitasok','Regisztráció','fiokbeallitasok','regisztracio','0','Regisztráció','Regisztráció','','0','','0');
INSERT INTO `bow_modulok` VALUES ('21','fiokbeallitasok','Fiókbeállítások','fiokbeallitasok','beallitas','0','Fiókbeállítások','Fiókbeállítások','beallitas.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('22','cimkek','Szövegek szervezése cimkék segítségével','cimkek','feliratkozo','0','Feliratkozás cimkék alapján szöveges tartalmakra','Feliratkozás cimkék alapján szöveges tartalmakra','beallito.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('23','_szerkeszto','Szövegek kezelése','_szerkeszto','szovegek','0','Szövegek kezelése','Szövegek kezelése, csoportosítása cimkékkel','beallito.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('24','kapcsolatDoboz','Oldalsávon kis kapcsolati űrlapot jelenít meg','kapcsolatDoboz','kirak','0','Kapcsolati űrlap doboz','Kapcsolati űrlap doboz','beallito.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('25','blogmotor','Blog megjelenítése cimke alapján','blogmotor','megjelenit','0','Blogmotor','Blog megjelenítése cimke alapján','','0','oldal_beallito.php','0');
INSERT INTO `bow_modulok` VALUES ('26','arcok','Munkatársak megjelenítése','arcok','lista','0','Munkatrsak megjelenítse','Munkatrsak megjelenítse','beallito.php','0','oldal_beallito.php','0');
INSERT INTO `bow_modulok` VALUES ('27','bloglinkek','Blog bejegyzések linkjeinek megjelenítése','bloglinkek','linksor','0','Blog linkek','Blog bejegyzés linkek megjelenítése','beallito.php','0','','0');
INSERT INTO `bow_modulok` VALUES ('29','_widget','Widgetek elhelyezése a témába pozíciók alapján','_widget','poziciok','0','Widgetek','Widgetek elhelyezése a témába pozíciók alapján','','0','','1');
INSERT INTO `bow_modulok` VALUES ('30','_migracio','Migrációs eszköz','_migracio','start','0','Migrációs eszköz','Migrációs eszköz','','0','','1');
INSERT INTO `bow_modulok` VALUES ('31','_aruhaz','Webáruház admin modul','_aruhaz','kozpont','0','Webáruház admin modul','Webáruház admin modul','','0','','1');
INSERT INTO `bow_modulok` VALUES ('32','egyedihtml','Egyedi HTMl tartalom','egyedihtml','megjelenit','0','Egyedi HTML','Egyedi HTML oldaltartalom','','0','oldal_beallito.php','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_modulxcsoport`
--

DROP TABLE IF EXISTS `bow_modulxcsoport`;

CREATE TABLE `bow_modulxcsoport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldalxmodul_id` int(11) NOT NULL,
  `csoport_id` int(11) NOT NULL,
  `oldalxtartalom_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_modulxcsoport`
--

-- --------------------------------------------------------

--
-- Table structure for table `bow_nyelvek`
--

DROP TABLE IF EXISTS `bow_nyelvek`;

CREATE TABLE `bow_nyelvek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kulcs` varchar(2) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `nev` varchar(20) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `meta` varchar(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_nyelvek`
--

INSERT INTO `bow_nyelvek` VALUES ('1','hu','Magyar','hu_HU');
INSERT INTO `bow_nyelvek` VALUES ('2','en','English','en_GB');
-- --------------------------------------------------------

--
-- Table structure for table `bow_oldalak`
--

DROP TABLE IF EXISTS `bow_oldalak`;

CREATE TABLE `bow_oldalak` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lertehozva` int(11) NOT NULL,
  `letrehozo` int(11) NOT NULL,
  `cim` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_felulir` tinyint(4) NOT NULL,
  `nyelv` varchar(2) NOT NULL DEFAULT 'hu',
  `tema` varchar(100) NOT NULL,
  `url` varchar(200) NOT NULL,
  `specialis` varchar(20) NOT NULL,
  `egyedi_fej` varchar(20) NOT NULL,
  `egyedi_lab` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_oldalak`
--

INSERT INTO `bow_oldalak` VALUES ('1','1391448083','13','Főoldal','PALÁNTA ISKOLA FŐOLDAL','A Palánta Iskolában a nevelés, képzés célja az autonóm (stabil belső értékrenddel bíró, folyamatos önképzést igénylő), harmonikus, boldogságra képes személyiség kifejlődéséhez való segítségnyújtás.','0','hu','','fooldal','fooldal','','');
INSERT INTO `bow_oldalak` VALUES ('5','1371481679','0','Hozzáférés megtagadva','','','0','','','hozzaferes-megtagadva','nincshozzaferes','','');
INSERT INTO `bow_oldalak` VALUES ('6','1371481710','0','Oldal nem található','','','0','','','oldal-nem-talalhato','404','','');
INSERT INTO `bow_oldalak` VALUES ('29','0','0','Hírek','Palánta hírek','Hírek érdekességek a Palánta iskola mindennapjaiból. A palánta facebook oldalának bejegyzései','0','hu','','hirek','','','');
INSERT INTO `bow_oldalak` VALUES ('18','1381333723','10','Rólunk','','','0','hu','','rolunk','','','');
INSERT INTO `bow_oldalak` VALUES ('20','1383044458','12','Blog','','','0','hu','','blog','','','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_oldalxcsoport`
--

DROP TABLE IF EXISTS `bow_oldalxcsoport`;

CREATE TABLE `bow_oldalxcsoport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldal_id` int(11) NOT NULL,
  `csoport_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_oldalxcsoport`
--

-- --------------------------------------------------------

--
-- Table structure for table `bow_oldalxmodul`
--

DROP TABLE IF EXISTS `bow_oldalxmodul`;

CREATE TABLE `bow_oldalxmodul` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldal_id` int(11) NOT NULL,
  `modul_id` int(11) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `modultabla_id` int(11) NOT NULL,
  `adat` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `csoport_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_oldalxmodul`
--

INSERT INTO `bow_oldalxmodul` VALUES ('15','5','9','0','Nincs hozzáférésed ehhez az oldalhoz','0','6','0');
INSERT INTO `bow_oldalxmodul` VALUES ('18','6','9','0','Egy szöveg: A keresett oldal nem található!','0','8','0');
INSERT INTO `bow_oldalxmodul` VALUES ('63','18','25','0','Blogmotor - Rólunk','0','a:12:{s:8:\"cimke_id\";s:2:\"12\";s:9:\"mennyiseg\";s:4:\"1000\";s:13:\"fokep_meret_x\";s:3:\"180\";s:13:\"fokep_meret_y\";s:3:\"120\";s:12:\"fokep_stilus\";s:0:\"\";s:10:\"fokep_attr\";s:0:\"\";s:15:\"lista_sablon_id\";s:1:\"1\";s:17:\"egyedul_sablon_id\";s:1:\"5\";s:5:\"akcio\";s:1:\"1\";s:13:\"tovabb_sablon\";s:0:\"\";s:13:\"vissza_sablon\";s:0:\"\";s:8:\"blog_url\";s:0:\"\";}','0');
INSERT INTO `bow_oldalxmodul` VALUES ('67','1','9','1000','Egy szöveg: Üdvözöljük!!!','0','24','0');
INSERT INTO `bow_oldalxmodul` VALUES ('75','20','25','0','Blogmotor - Blog','0','a:8:{s:8:\"cimke_id\";s:2:\"14\";s:9:\"mennyiseg\";s:2:\"10\";s:6:\"lapozo\";s:1:\"1\";s:8:\"rendezes\";s:1:\"0\";s:12:\"lista_sablon\";s:19:\"cikk_blog_lista.php\";s:14:\"egyedul_sablon\";s:21:\"cikk_blog_egycikk.php\";s:5:\"akcio\";s:1:\"1\";s:8:\"blog_url\";s:0:\"\";}','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_oldalxtartalom`
--

DROP TABLE IF EXISTS `bow_oldalxtartalom`;

CREATE TABLE `bow_oldalxtartalom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldal_id` int(11) NOT NULL,
  `tartalom_id` int(11) NOT NULL,
  `fuggveny` varchar(255) NOT NULL,
  `adat` text NOT NULL,
  `cim` varchar(255) NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_oldalxtartalom`
--

INSERT INTO `bow_oldalxtartalom` VALUES ('4','1','1','szovegMegjelenites','a:1:{s:9:\"szoveg_id\";s:2:\"22\";}','One text for pages - Üdvözöllek a Palánta Iskola honlapján!','11');
INSERT INTO `bow_oldalxtartalom` VALUES ('13','29','1','szovegMegjelenites','a:1:{s:9:\"szoveg_id\";i:73;}','One text for pages - Hamarosan...','0');
INSERT INTO `bow_oldalxtartalom` VALUES ('14','18','1','szovegMegjelenites','a:1:{s:9:\"szoveg_id\";i:73;}','One text for pages - Hamarosan...','0');
INSERT INTO `bow_oldalxtartalom` VALUES ('15','20','1','blogmotor','a:5:{s:8:\"cimke_id\";s:2:\"14\";s:6:\"sablon\";s:0:\"\";s:12:\"csakbevezeto\";s:1:\"1\";s:9:\"mennyiseg\";s:2:\"10\";s:8:\"rendezes\";s:1:\"0\";}','Blog megjelenítése cimke alapján - Blog','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_oldalxurl`
--

DROP TABLE IF EXISTS `bow_oldalxurl`;

CREATE TABLE `bow_oldalxurl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `oldal_id` int(11) NOT NULL,
  `url` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_oldalxurl`
--

INSERT INTO `bow_oldalxurl` VALUES ('1','1','fooldal');
INSERT INTO `bow_oldalxurl` VALUES ('2','1','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_rendszer`
--

DROP TABLE IF EXISTS `bow_rendszer`;

CREATE TABLE `bow_rendszer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mappa` varchar(100) NOT NULL,
  `leiras` varchar(255) NOT NULL,
  `statusz` int(11) NOT NULL,
  `nev` varchar(255) NOT NULL,
  `rendszer` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_rendszer`
--

INSERT INTO `bow_rendszer` VALUES ('1','blogmotor',' You can add text tags for blogengine, and for other use\r','1',' Creating, and editind tags\r','0');
INSERT INTO `bow_rendszer` VALUES ('2','_szerkeszto',' Editing posts\r','1',' Post Editor\r','1');
INSERT INTO `bow_rendszer` VALUES ('3','_szovegek',' Text element for pages\r','1',' Text for pages\r','1');
INSERT INTO `bow_rendszer` VALUES ('4','arcok',' Showing staff\'s details\r','0',' Staff\r','0');
INSERT INTO `bow_rendszer` VALUES ('5','bloglinkek',' Make blog menus with text tags\r','0',' Blog links\r','0');
INSERT INTO `bow_rendszer` VALUES ('6','cimkek',' Editing text tags in the admin area\r','1',' Text tags editing\r','0');
INSERT INTO `bow_rendszer` VALUES ('7','egyedihtml',' Custom html for pages and widgets\r','0',' Custom HTML\r','0');
INSERT INTO `bow_rendszer` VALUES ('9','epitoKocka',' Elements for pages and modules\r','0',' Design elements\r','0');
INSERT INTO `bow_rendszer` VALUES ('10','facebook',' Link local account to facebook\r','0',' Facebook connect\r','0');
INSERT INTO `bow_rendszer` VALUES ('11','fiokbeallitasok',' Account settings for users\r','1',' My account\r','1');
INSERT INTO `bow_rendszer` VALUES ('12','idezetek',' Blockquote modul by text tags\r','0',' Blockquote\r','0');
INSERT INTO `bow_rendszer` VALUES ('13','infoSlider',' Slider and text group displaying\r','0',' Infoslider\r','0');
INSERT INTO `bow_rendszer` VALUES ('14','kapcsolatDoboz',' Simple contact form\r','1',' Contact widget\r','0');
INSERT INTO `bow_rendszer` VALUES ('15','levelek',' Editing letter templates\r','1',' System letter templates\r','0');
INSERT INTO `bow_rendszer` VALUES ('16','unicorn',' Extend system with responsive administration tools\r','1',' Unicorn admin system\r','1');
INSERT INTO `bow_rendszer` VALUES ('17','_aruhaz',' Full features webshop component\r','1',' Bow Shop Component\r','0');
INSERT INTO `bow_rendszer` VALUES ('18','_oldalak',' Page builder admin tools\r','1',' Page Builder\r','1');
INSERT INTO `bow_rendszer` VALUES ('19','cikkgaleria',' Upload images and use shortcode to insert images and galleries in posts and pages','1',' Gallery Plugin','1');
INSERT INTO `bow_rendszer` VALUES ('20','shortcode',' Common shortcode interface for admin post editor\r','1',' Shortcodes\r','1');
INSERT INTO `bow_rendszer` VALUES ('21','_menu',' Editing menu groups and items\r','1',' Menu Editor\r','1');
INSERT INTO `bow_rendszer` VALUES ('22','translate',' Site translator\r','1',' Translator\r','0');
INSERT INTO `bow_rendszer` VALUES ('23','bootstrap',' Create nice columns in posts\r','1',' Bootstrap Columns Shortcodes\r','0');
INSERT INTO `bow_rendszer` VALUES ('24','fbwall',' Displaying facebook post on my website','1',' Facebook Wall','0');
INSERT INTO `bow_rendszer` VALUES ('25','_widget',' Widget position manager\r','1',' Widgets\r','0');
INSERT INTO `bow_rendszer` VALUES ('26','_modulok',' Modul installation tools\r','1',' Add or remove modules\r','0');
INSERT INTO `bow_rendszer` VALUES ('27','_beallitasok',' Editing general configuration\r','1',' General settings\r','0');
INSERT INTO `bow_rendszer` VALUES ('28','_migracio',' Migration and backup tool\r','1',' Migration\r','0');
INSERT INTO `bow_rendszer` VALUES ('29','_temak',' Theme settings\r','1',' Themes\r','0');
INSERT INTO `bow_rendszer` VALUES ('30','events',' Listing events\r','1',' Events widget\r','0');
INSERT INTO `bow_rendszer` VALUES ('31','updatemaker',' Bow developer helper\r','1',' Update Maker\r','0');
INSERT INTO `bow_rendszer` VALUES ('32','scribd',' View uploaded PDF documents via Scribd\r','1',' SCRIBD Plugin\r','0');
INSERT INTO `bow_rendszer` VALUES ('33','contact',' Contact form for webpages\r','1',' Contact form\r','0');
INSERT INTO `bow_rendszer` VALUES ('34','xss_filter',' XSS INPUT FILTER, Important sequrity update!\r','1',' XSS Filter\r','0');
-- --------------------------------------------------------

--
-- Table structure for table `bow_szovegek`
--

DROP TABLE IF EXISTS `bow_szovegek`;

CREATE TABLE `bow_szovegek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cim` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `bevezeto` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `tartalom` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `csakbevezeto` tinyint(4) NOT NULL,
  `szerzo` int(11) NOT NULL,
  `letrehozva` int(11) NOT NULL,
  `cimke` varchar(500) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `sablon` varchar(100) NOT NULL,
  `fokep` varchar(300) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_szovegek`
--

INSERT INTO `bow_szovegek` VALUES ('6','Nincs hozzáférésed ehhez az oldalhoz','Kérdésed esetén keresd az oldal adminisztrátorát!','','1','0','1371569916','','0','');
INSERT INTO `bow_szovegek` VALUES ('8','A keresett oldal nem található!','Az oldalt nem létezik, esetleg megszűnt... Kérjük folytassa a <a href=\"/fooldal\">Főoldalon</a>\r\n','<p>[galeriakep igazit=\"kozep\" &nbsp;galnyit=\"0\" &nbsp;id=\"49\" ]</p>','0','0','1371576771','','0','');
INSERT INTO `bow_szovegek` VALUES ('15','Elfelejtett jelszó','Hogyan segíthetünk?','<p>K&eacute;rj&uuml;k, add meg regisztr&aacute;ci&oacute;s e-mail c&iacute;medet, &iacute;gy elk&uuml;ld&uuml;nk sz&aacute;modra egy jelsz&oacute;be&aacute;ll&iacute;t&oacute; linket. A linket megnyitva megadhatod &uacute;j jelszavadat.</p>\r\n<p>Aj&aacute;nljuk tov&aacute;bb&aacute;, hogy bel&eacute;p&eacute;s ut&aacute;n k&ouml;sd &ouml;ssze Pal&aacute;nta fi&oacute;kodat Facebook fi&oacute;koddal, &iacute;gy legk&ouml;zelebb egy gombnyom&aacute;ssal bel&eacute;phetsz oldalunkra!</p>','0','0','1378805040','Rendszer','2','');
INSERT INTO `bow_szovegek` VALUES ('16','Jelszó megváltoztatása','Add meg új jelszavadat!','<p>Itt m&oacute;dos&iacute;thatod jelszavadat. Miut&aacute;n megv&aacute;ltoztattad jelszavadat, javasoljuk, t&aacute;rs&iacute;tsd hozz&aacute;f&eacute;r&eacute;sedet Facebook fi&oacute;koddal, &iacute;gy legk&ouml;zelebb egy kattint&aacute;ssal belphetsz oldalunkra!</p>','0','0','1378825508','Rendszer','2','');
INSERT INTO `bow_szovegek` VALUES ('17','Kapcsolódj a Facebook segítségéve!','Facebook azonosítás','<p>Amennyiben &ouml;sszekapcsolod fi&oacute;kodat a Facebook hozz&aacute;f&eacute;r&eacute;seddel, k&ouml;nnyebben l&eacute;phetsz be oldalunkra. K&eacute;sőbbiekben egy&eacute;b szolg&aacute;ltat&aacute;st is szeretn&eacute;nk bekapcsolni, amely megk&ouml;nny&iacute;ti a b&ouml;ng&eacute;szst, tartalmak megoszt&aacute;s&aacute;t.</p>','0','8','1378889909','Rendszer','2','');
INSERT INTO `bow_szovegek` VALUES ('18','Regisztrálj a honlapra','Add meg adataidat','<p>Regisztr&aacute;ci&oacute;ddal fi&oacute;kot hozhatsz l&eacute;tre oldalunkon. Amennyiben sz&uuml;lő vagy tan&aacute;r vagy, rendszer&uuml;nk&ouml;n kereszt&uuml;l olvashatod csoportod &uuml;zenőfal&aacute;t, el&eacute;rheted gyermekeid oszt&aacute;lyt&aacute;rsait, illetve sz&uuml;leit, levelet k&uuml;ldhetsz nekik, &eacute;s egy&eacute;bb szolg&aacute;ltat&aacute;sokat vehetsz ig&eacute;nybe:</p>\r\n<ul>\r\n<li>Sz&uuml;lők&eacute;nt olvashatod gyermeked oszt&aacute;ly&aacute;nak &uuml;zenőfal&aacute;t, oda Te magad is &iacute;rhatsz</li>\r\n<li>Tan&aacute;rk&eacute;nt el&eacute;rheted az &uuml;zenőfalakat &eacute;s a regisztr&aacute;lt sz&uuml;lőket</li>\r\n<li>&Eacute;rtes&iacute;tst &aacute;ll&iacute;thatsz be &uacute;j h&iacute;rek &eacute;s cikkek megjelen&eacute;s&eacute;hez</li>\r\n</ul>\r\n<p>Pal&aacute;nt&aacute;s fi&oacute;kodat &ouml;sszekapcsolhatod Facebook adatlapoddal is, &iacute;gy egy kattint&aacute;ssal l&eacute;phetsz be a rendszerbe.</p>','0','0','1378910783','','2','');
INSERT INTO `bow_szovegek` VALUES ('19','Fiókod','Beállítások : ez  eyg  postz vagy mi\' \"++ \'\" $','','0','10','1378975849','','0','');
INSERT INTO `bow_szovegek` VALUES ('22','Üdvözöljük honlapunkon!','','<hr />\r\n<p>Tisztelt L&aacute;togat&oacute;! Most nagyon egyszerűen k&eacute;rhet aj&aacute;nlatot &uuml;zletberendez&eacute;s, hűtőpult, p&eacute;nzt&aacute;rpult, falihűtő, hűtőkamra, polcrendszer webkatal&oacute;gusunkb&oacute;l. V&aacute;lassza ki az &Ouml;nnek megfelelő term&eacute;ket, tegye a virtu&aacute;lis kos&aacute;rba, kattintson a &Aacute;RAJ&Aacute;NLAT K&Eacute;R&Eacute;SE gombra. R&ouml;vid időn bel&uuml;l megk&uuml;ldj&uuml;k &aacute;raj&aacute;nlatunkat &eacute;s a sz&aacute;ll&iacute;t&aacute;si felt&eacute;teleket.</p>\r\n<p><em><strong>Amennyiben nem tal&aacute;lja a keresett &uuml;zletberendez&eacute;st&nbsp;</strong><strong>&eacute;rdeklődj&ouml;n el&eacute;rhetős&eacute;geinken! Mindenre van megold&aacute;sunk!</strong></em></p>\r\n<hr />\r\n<p style=\"text-align: left;\">[bscol class=\"col-md-6\" ]</p>\r\n<p>A B-STORE Kft. 1998-ban alakult csal&aacute;di v&aacute;llalkoz&aacute;s, az&oacute;ta is ebben a form&aacute;ban műk&ouml;dik. C&eacute;lunk, hogy c&eacute;g&uuml;nk egy szem&eacute;lyben tudja kiel&eacute;g&iacute;teni megrendelőik &uuml;zletberendez&eacute;ssel kapcsolatos ig&eacute;nyeit, ez&eacute;rt a tervez&eacute;stől (3D l&aacute;tv&aacute;nytervek) szorosan egy&uuml;ttműk&ouml;d&uuml;nk partnereinkkel. B&uacute;torainkkal berendezhetők k&uuml;l&ouml;nf&eacute;le &eacute;lelmiszer&uuml;zletek, bev&aacute;s&aacute;rl&oacute;k&ouml;zpontok, h&uacute;sboltok, p&eacute;k&aacute;ru&uuml;zletek, stb. Term&eacute;keink sz&eacute;les v&aacute;laszt&eacute;ka - hűtőpult, p&eacute;nzt&aacute;rpult, falihűtő, hűtőkamra, polcrendszer lehetőv&eacute; teszi, hogy mind &aacute;rban, mind k&uuml;llem&eacute;ben mindenki megtal&aacute;lja a sz&aacute;m&aacute;ra legmegfelelőbbet</p>\r\n<p>[postgallery id=\"76\" gallery=\"22\" popup=\"onlypic\" align=\"center\" ]</p>\r\n<p>[/bscol]</p>\r\n<p>[bscol class=\"col-md-6\" ]</p>\r\n<p>[postgallery id=\"77\" gallery=\"22\" popup=\"onlypic\" align=\"center\" ]</p>\r\n<p>Ebben nagy seg&iacute;ts&eacute;g&uuml;nkre van a&nbsp;&nbsp;<strong>&nbsp;</strong><span style=\"background-color: #ffffff; color: #ff6600;\"><strong>COSTAN Refrigeration</strong></span>&nbsp;Olasz hűtőb&uacute;torgy&aacute;r, valamint a Belga&nbsp;&nbsp;<strong>&nbsp;<span style=\"color: #808000;\">GONDELLA</span></strong>&nbsp;&aacute;ruh&aacute;zi polcrendszerek gy&aacute;rt&oacute;ja melyeknek Magyarorsz&aacute;gi&nbsp; k&eacute;pviselője vagyunk. Ezeken fel&uuml;l t&ouml;bb gy&aacute;rt&oacute;val &aacute;llunk m&eacute;g kereskedelmi kapcsolatban. Az &eacute;rt&eacute;kes&iacute;t&eacute;sen t&uacute;l 24 &oacute;r&aacute;s orsz&aacute;gos szerv&iacute;zszolg&aacute;ltat&aacute;st k&iacute;n&aacute;lunk &uuml;gyfeleink r&eacute;sz&eacute;re garanci&aacute;n bel&uuml;l &eacute;s azon t&uacute;l. &Uuml;zletberendez&eacute;s, hűtőpult, p&eacute;nzt&aacute;rpult, falihűtő, hűtőkamra, polcrendszer v&aacute;s&aacute;rl&aacute;sa előtt k&eacute;rj&uuml;k, hogy &aacute;raj&aacute;nlatk&eacute;r&eacute;ssel forduljon c&eacute;g&uuml;nk k&eacute;pviselőj&eacute;hez.</p>\r\n<p>[/bscol]</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p><strong>&Eacute;rdeklődj&ouml;n a lenti űrlapon kereszt&uuml;l:</strong></p>\r\n<p>[contact id=\"1\" ]</p>\r\n<p><strong>&nbsp;</strong></p>\r\n<p>&nbsp;</p>','0','10','1379952283','','0','feltoltesek/cikkgaleria/22/cf1911ad3395e5652e6ff9c9d23c5a9d.JPG');
INSERT INTO `bow_szovegek` VALUES ('26','Belépés','Kérem, add meg email címedet és jelszavadat!','','0','12','1382109098','','0','');
INSERT INTO `bow_szovegek` VALUES ('27','Üdvözlöm a blogban!','','<p>[postgallery id=\"78\" gallery=\"27\" popup=\"yes\" align=\"center\" ]</p>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra lectus et arcu lobortis, vel ornare neque congue. Pellentesque metus purus, sagittis non facilisis sit amet, interdum sed erat. Aenean varius posuere nulla, egestas dignissim urna tincidunt accumsan. &nbsp;Pellentesque a urna sed erat consequat aliquam. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum rhoncus orci nec dolor faucibus, ut volutpat purus malesuada. Fusce et varius nulla, eget tristique velit. Nulla quis sodales ipsum. Curabitur commodo sed sapien ac tincidunt. Vestibulum commodo nisi in lectus pretium suscipit.</p>\r\n<p>[postgallery id=\"79\" gallery=\"27\" popup=\"yes\" align=\"left\" ]In ut pretium lacus. Donec in dolor id elit sagittis pulvinar in at erat. Nunc vitae convallis augue. Quisque ut laoreet est. Donec pharetra suscipit consectetur. Praesent semper mauris vitae lobortis viverra. Nullam vitae pulvinar risus, ut aliquam odio. Mauris eget nisi dictum, fringilla enim sed, consequat velit. Fusce congue nec nisl id sodales. Pellentesque tempor enim at orci porta adipiscing. Curabitur semper egestas adipiscing. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Nulla porttitor at leo in feugiat.</p>\r\n<p>[postgallery id=\"80\" gallery=\"27\" popup=\"yes\" align=\"right\" ]Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Quisque pellentesque vitae nisl sit amet facilisis. Nullam ut tortor lobortis, laoreet justo ut, laoreet metus. Ut laoreet varius ante, ac accumsan lectus condimentum rutrum. Nulla sapien elit, faucibus et nunc eu, egestas sollicitudin lectus. Ut rutrum, dui interdum adipiscing gravida, velit erat scelerisque nunc, sed accumsan libero sem non quam. Sed dignissim urna in turpis ornare, id varius turpis varius. Donec elementum lacus sed urna vestibulum aliquet.</p>\r\n<p>[scribd docid=\"214437946\" ]</p>','0','12','1383045124','','0','feltoltesek/cikkgaleria/27/f8c9d4e083f6677d7cffcf5aaa29ba58.JPG');
INSERT INTO `bow_szovegek` VALUES ('72','Bootstrap hasábok a tiny-ből megoldva ','A tinymcehez írtam shortcode-okat, hogy könnyebb legyen bootstrap hasábokat csinálni a tiny-vel\r\n\r\ncls5knclk5s cls lcs lcskclsclks cls clks lcksl csl clksclksc slk clksjc lksj lkc slkcjslk clsc lks lkcs lkcs lkcslkc lks clks clks clks clks clks clks clks clks c\r\n\r\ncsl clsjcl sclks lkcjslk clks jclk5s jcls jlkcj ljkjcslkj cslkj cslkj clksj lckjs lkcj lks clks jclksj lkcsj lkc slkcj slk clks5 jcls jlcs jlck5s lkc5s','<p>[bscol class=\"col-md-4\" ]</p>\r\n<h4>Első oszlop</h4>\r\n<p><em>Mindenf&eacute;le sz&ouml;veg j&ouml;n ide, mindenf&eacute;le sz&ouml;veg j&ouml;n ide.<br /> [postgallery id=\"70\" gallery=\"72\" popup=\"onlypic\" align=\"center\" ][/bscol][bscol class=\"col-md-4\" ]</em></p>\r\n<h4>M&aacute;sodik oszlop</h4>\r\n<p><em><em>Mindenf&eacute;le sz&ouml;veg j&ouml;n ide, mindenf&eacute;le sz&ouml;veg j&ouml;n ide.<br /> [/bscol][bscol class=\"col-md-4\" ]</em></em></p>\r\n<h4>Harmadik oszlop</h4>\r\n<p><em><em><em>Mindenf&eacute;le sz&ouml;veg j&ouml;n ide, mindenf&eacute;le sz&ouml;veg j&ouml;n ide.<br /> [postgallery id=\"72\" gallery=\"72\" popup=\"onlypic\" align=\"center\" ][/bscol]</em></em></em></p>\r\n<p>Ez &nbsp;has&aacute;bos cucc shortcoddal megoldva</p>\r\n<p>A gal&eacute;riak&eacute;p shortcode csak k&eacute;p megjelen&iacute;t&eacute;sre &aacute;ll&iacute;tva a k&eacute;p kit&ouml;lti a has&aacute;b sz&eacute;less&eacute;get.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>[bscol class=\"col-md-3\" ]</p>\r\n<h4>Első oszlop</h4>\r\n<p>[/bscol][bscol class=\"col-md-3\" ]</p>\r\n<h4>M&aacute;sodik oszlop</h4>\r\n<p>[/bscol][bscol class=\"col-md-6\" ]</p>\r\n<h4>Harmadik sz&eacute;lesebb oszlop</h4>\r\n<p>[postgallery id=\"71\" gallery=\"72\" popup=\"onlypic\" align=\"center\" ][/bscol]</p>\r\n<p>Ide meg kell egy lez&aacute;r&aacute;s</p>','0','15','1393844431','','','feltoltesek/cikkgaleria/72/9033f7c8f9cd85b96a678f496cbeb0ec.JPG');
INSERT INTO `bow_szovegek` VALUES ('73','Hamarosan...','','','0','15','1394645484','','','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_tagok`
--

DROP TABLE IF EXISTS `bow_tagok`;

CREATE TABLE `bow_tagok` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nev` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `jelszo` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `csoport_id` int(11) NOT NULL,
  `regtipus` tinyint(4) NOT NULL COMMENT '0: normal, 1: facebook, 2: google',
  `nem` varchar(10) NOT NULL,
  `weboldal` varchar(255) NOT NULL,
  `fbuid` varchar(255) NOT NULL,
  `accesstoken` varchar(255) NOT NULL,
  `aktivitas` int(11) NOT NULL,
  `aszf` tinyint(4) NOT NULL DEFAULT '0',
  `regido` int(11) NOT NULL,
  `visszaallito_kod` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_tagok`
--

INSERT INTO `bow_tagok` VALUES ('15','Ceglédi Iván Péter','0','NWFiZDA2ZDZmNmVmMGUwMjJlMTFiOGE0MWY1N2ViZGE=','ivan@zente.org','5','0','','','-1','','1397652716','0','1391427249','NjBlNGFhMzY4N2E1MWUyMzk2NzFmZTg3ZGFjNGU5MjU=');
INSERT INTO `bow_tagok` VALUES ('24','Ceglédi Iván Péter','0','NWFiZDA2ZDZmNmVmMGUwMjJlMTFiOGE0MWY1N2ViZGE=','bowforex@gmail.com','7','0','','','','','1395077404','0','1395077299','');
-- --------------------------------------------------------

--
-- Table structure for table `bow_widgetxcsoport`
--

DROP TABLE IF EXISTS `bow_widgetxcsoport`;

CREATE TABLE `bow_widgetxcsoport` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) NOT NULL,
  `csoport_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bow_widgetxcsoport`
--

INSERT INTO `bow_widgetxcsoport` VALUES ('3','174','6');
INSERT INTO `bow_widgetxcsoport` VALUES ('4','174','7');
-- --------------------------------------------------------

--
-- Table structure for table `bow_widgetxoldal`
--

DROP TABLE IF EXISTS `bow_widgetxoldal`;

CREATE TABLE `bow_widgetxoldal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `widget_id` int(11) NOT NULL,
  `oldal_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `bow_widgetxoldal`
--

INSERT INTO `bow_widgetxoldal` VALUES ('5','66','1');
INSERT INTO `bow_widgetxoldal` VALUES ('6','74','1');
INSERT INTO `bow_widgetxoldal` VALUES ('7','75','1');
INSERT INTO `bow_widgetxoldal` VALUES ('9','78','1');
INSERT INTO `bow_widgetxoldal` VALUES ('14','174','1');
-- --------------------------------------------------------

--
-- Table structure for table `bow_widgetxpozicio`
--

DROP TABLE IF EXISTS `bow_widgetxpozicio`;

CREATE TABLE `bow_widgetxpozicio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tema` varchar(200) NOT NULL,
  `pozicio` varchar(100) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `fuggveny` varchar(200) NOT NULL,
  `sorrend` int(11) NOT NULL,
  `nev` varchar(200) CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  `adat` text CHARACTER SET utf8 COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=192 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bow_widgetxpozicio`
--

INSERT INTO `bow_widgetxpozicio` VALUES ('66','responsive','respoSlider','wgHelyfoglalo','7','Helyfoglalo','a:1:{s:3:\"cim\";s:4:\"Cím\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('67','responsive','topFullWith','wgoldalcim','8','Oldal cím megjelenítése','a:1:{s:5:\"tolto\";s:1:\"1\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('68','responsive','footerBal','wgFiok','9','Belépés/fiók','a:1:{s:3:\"cim\";s:0:\"\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('69','responsive','jobbSav','wgBloglink','10','Bloglinkek','a:4:{s:3:\"cim\";s:6:\"Cikkek\";s:9:\"blogoldal\";s:10:\"75_blog_14\";s:5:\"limit\";s:3:\"100\";s:8:\"rendezes\";s:1:\"0\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('73','responsive','jobbSav','kategoriaWidget','11','Kategória doboz','');
INSERT INTO `bow_widgetxpozicio` VALUES ('80','','','','1','','');
INSERT INTO `bow_widgetxpozicio` VALUES ('81','b-store','jobbSav','wgFiok','30','Belépés/fiók','a:1:{s:3:\"cim\";s:19:\"Fiókbeállítások\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('82','','','','2','','');
INSERT INTO `bow_widgetxpozicio` VALUES ('181','palantamob','jobbSav','wgFiok','40','','a:1:{s:3:\"cim\";s:5:\"Fiók\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('184','palantamob','jobbSav','wgEvents','20','','a:1:{s:3:\"cim\";s:20:\"Közelgő események\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('185','palantamob','jobbSav','wgKapcsolatDoboz','30','','a:4:{s:3:\"cim\";s:4:\"Cím\";s:7:\"cimzett\";s:14:\"ivan@zente.org\";s:6:\"leiras\";s:97:\"Várjuk szíves érdeklődését a Palánta Iskolával, felvétellel, hivogatókkal kapcsolatban.\";s:8:\"koszonet\";s:67:\"Köszönjük érdeklődését, a levelet elküldtem a titkárságra\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('186','palantamob','jobbSav','wgPost','10','','a:3:{s:9:\"szoveg_id\";s:2:\"27\";s:8:\"elo_html\";s:37:\"<div class=\"row\" style=\"color: #ea3\">\";s:8:\"uto_html\";s:6:\"</div>\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('189','b-store','jobbSav','wgKapcsolatDoboz','10','','a:4:{s:3:\"cim\";s:9:\"Kapcsolat\";s:7:\"cimzett\";s:14:\"ivan@zente.org\";s:6:\"leiras\";s:47:\"Véleményével, kérdésével keressen minket!\";s:8:\"koszonet\";s:30:\"Köszönjük érdeklődését!\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('190','b-store','miniKosar','kosarWidget','10','','a:2:{s:9:\"penznemek\";s:1:\"0\";s:8:\"egyszeru\";s:1:\"1\";}');
INSERT INTO `bow_widgetxpozicio` VALUES ('191','b-store','jobbSav','kosarWidget','20','','a:2:{s:9:\"penznemek\";s:1:\"1\";s:8:\"egyszeru\";s:1:\"0\";}');
-- --------------------------------------------------------

--
-- Table structure for table `egyedi_tagok`
--

DROP TABLE IF EXISTS `egyedi_tagok`;

CREATE TABLE `egyedi_tagok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_id` int(11) NOT NULL,
  `magadrol` text NOT NULL,
  `holhallotta` varchar(255) NOT NULL,
  `mikor` varchar(255) NOT NULL,
  `jonap` varchar(255) NOT NULL,
  `inpoi` int(11) NOT NULL,
  `megjegy` text NOT NULL,
  `naja` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `egyedi_tagok`
--

INSERT INTO `egyedi_tagok` VALUES ('2','23','blabl',' 1',' 1','','0','micsodát',' kulcs2');
INSERT INTO `egyedi_tagok` VALUES ('3','24','blabl',' 1',' 1','','0','micsodát',' kulcs2');
-- --------------------------------------------------------

--
-- Table structure for table `palanta_arcok`
--

DROP TABLE IF EXISTS `palanta_arcok`;

CREATE TABLE `palanta_arcok` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nev` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `bemutato` text COLLATE utf8_hungarian_ci NOT NULL,
  `email` varchar(200) COLLATE utf8_hungarian_ci NOT NULL,
  `elerhetoseg` text COLLATE utf8_hungarian_ci NOT NULL,
  `publikus` tinyint(4) NOT NULL,
  `kep` varchar(100) COLLATE utf8_hungarian_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_hungarian_ci;

--
-- Dumping data for table `palanta_arcok`
--

INSERT INTO `palanta_arcok` VALUES ('1','Esztergomi Kata','Aliquam adipiscing mauris ut odio suscipit eleifend. Aliquam ut luctus quam, ut consequat neque. In ut libero lacus. Mauris orci tellus, mattis at ante eu, fringilla pretium neque.','esztergomi.kata@palanta.hu','','0','arcok_138061943543.png');
INSERT INTO `palanta_arcok` VALUES ('2','Mayer Ágnes','Szakmai vezető\r\nAliquam adipiscing mauris ut odio suscipit eleifend. Aliquam ut luctus quam, ut consequat neque. In ut libero lacus. Mauris orci tellus, mattis at ante eu, fringilla pretium neque.','mayer.agnes@palanta.hu','','0','');
INSERT INTO `palanta_arcok` VALUES ('3','Thoma Zsuzsanna','Aliquam adipiscing mauris ut odio suscipit eleifend. Aliquam ut luctus quam, ut consequat neque. In ut libero lacus. Mauris orci tellus, mattis at ante eu, fringilla pretium neque.','titkarsag@palanta.hu','','0','');
