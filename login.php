<?php

/**
 * 
 * BowCMS 2.0 ajax login
 * 
 * Cegl�di Iv�n
 * 2013
 */
 
session_start();

// UTF-8 felj�c elk�ld�se

header("Content-type: text/html; charset=utf-8");

// Alap el�rsi �tvonal

define ('BASE', dirname(__FILE__).'/');

// Glob�lis datastore oszt�ly bet�lt�se

include(BASE.'osztaly/osztaly_memoria.php');

// Alap be�ll�t�sok bet�lt�se

include(BASE.'osztaly/osztaly_beallitas.php');
Beallitas::beallitasBetolt('adatbazis');

// Hibajelz�s kapcsol�sa

// megvizsg�ljuk, hogy telep�tve van-e a rendszer
include(BASE.'osztaly/osztaly_telepito.php');

Telepito::vizsgalat();

if (TELEPITES) exit;

// Tovbbi konstansok bet�lt�se
include_once BASE.'osztaly/sqlfuggvenyek.php';

Beallitas::beallitasSqlBetolt('Rendszer');
Beallitas::beallitasSqlBetolt('Weboldal �ltal�nos be�ll�t�sok');
Beallitas::beallitasSqlBetolt('E-mail bell�t�sok');

ini_set('DISPLAY_ERRORS', PHP_HIBA);

include(BASE.'osztaly/osztaly_hibakezelo.php');
include BASE.'osztaly/osztaly_munkamenet.php';
include BASE.'osztaly/osztaly_tagok.php';
include BASE.'osztaly/osztaly_beepulok.php';
include BASE.'osztaly/osztaly_levelezo.php';

$session = Munkamenet::getIstance();

$tag = Tagok::peldany();

$fbuid = $_GET['fbuid'];

$sql = "SELECT * FROM bow_tagok WHERE fbuid = '$fbuid' LIMIT 1";
$rs = sqluniv3($sql);
if (isset($rs['id'])) {
    print '2';
    $tag->beleptet($rs['id']);
} else {
    print '1';
}
