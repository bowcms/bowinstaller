<?php

/**
 * bowcms függvny könyvtár
 * változatos, gyakran előforduló feladatokra
 * 
 */

function osztalyIndit($osztaly, $metodus, $param = '')
{
    $hivo = debug_backtrace();
    $mappa = dirname($hivo[0]['file']);

    require_once ($mappa . '/' . $osztaly . '.php');
    $o = new $osztaly;
    if (file_exists($mappa . '/' . $osztaly . 'Komp.php')) {
        require_once ($mappa . '/' . $osztaly . 'Komp.php');

        $kompNev = $osztaly . 'Komp';
        if (class_exists($kompNev)) {
            $komp = new $kompNev;
            $o->komp = $komp;
        }

    }
    $o->$metodus($param);
}

function modulBekapcsolva($mappa)
{
    $sql = "SELECT id FROM bow_rendszer WHERE mappa = '$mappa' AND statusz = 1";
    $rs = sqluniv3($sql);
    if (isset($rs['id'])) {
        return true;
    } else {
        return false;
    }
}
function bowMeta($kategoria = '', $kulcs = '', $meta_id = 0) {
    $w = array();
    
    if ($kategoria != '') $w[] = " kategoria = '$kategoria' ";
    if ($kulcs != '') $w[] = " kulcs = '$kulcs' ";
    if ($meta_id != 0) $w[] = " meta_id = '$meta_id' ";
    
    if (empty($w)) return false;
    $sql = "SELECT * FROM bow_meta WHERE ".(implode(' and ', $w))." ORDER BY sorrend asc ";
    $rs = sqlAssocArr($sql);
    if (empty($rs)) return false;
    foreach($rs as $k => $sor) {
        $rs[$k]['ertek'] = @unserialize($sor['ertek']);
    }
    return $rs;
    
}
function bowMetaFelvitel($kategoria, $kulcs, $ertek,$meta_id = 0, $datum = '',  $nev = '') {
    
    $adat = array(
        'kategoria' => $kategoria,
        'kulcs' => $kulcs,
        'ertek' => $ertek,
        'meta_id' => $meta_id,
        'nev' => $nev
    );
    if ($datum!='') {
        $adat['datum'] = $datum;
    }
    sqlfelvitel($adat, 'bow_meta');
    return mysql_insert_id();
    
}
function bowMetaMentes($kategoria, $kulcs, $ertek,$meta_id = 0, $datum = '',  $nev = '') {
    $van = bowMeta($kategoria,$kulcs, $meta_id);
    
    $adat = array(
        'kategoria' => $kategoria,
        'kulcs' => $kulcs,
        'ertek' => $ertek,
        'meta_id' => $meta_id,
        'nev' => $nev
    );
    if ($datum!='') {
        $adat['datum'] = $datum;
    }
    if (!empty($van)) {
        $adat['id'] = $van[0]['id'];
        sqlmodositas($adat, 'bow_meta');
        return $adat['id'];
    } else {
        sqlfelvitel($adat, 'bow_meta');
        return mysql_insert_id();
    }
    
    
    
}
function bowMetaId($id) {
    
    $sql = "SELECT * FROM bow_meta WHERE id = $id ";
    $rs = sqlAssocRow($sql);
    
    $rs['ertek'] = @unserialize($rs['ertek']);
    
    return $rs;
    
}
function bowUrl($uri) {
    return BASE_URL.$uri;
}
function modulElotolto()
{
    $dir = BASE . 'modulok/';

    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {

                if ($file == '.' or $file == '..')
                    continue;

                if (is_file($dir . $file . '/init.php')) {
                    if (modulBekapcsolva($file)) {
                        include ($dir . $file . '/init.php');
                    }

                }
            }
        }
    }
    return;
}

function modulBejegyzes($funkcio, $fuggv, $dinamikusBeallito = false)
{
    $eleres = debug_backtrace();
    $eleresUt = dirname($eleres[0]['file']);
    $eleresUt = str_replace('\\', '/', $eleresUt);
    $eleresUt = trim($eleresUt, '/');
    $exp = explode('/', $eleresUt);
    $modul = end($exp);

    Memoria::tombHozzafuz('modulBejegyzes', array(
        'funkcio' => $funkcio,
        'fuggveny' => $fuggv,
        'modul' => $modul,
        'dinamikusBeallito' => $dinamikusBeallito));

}

function vanHelyorzo($nev)
{
    $html = Html::peldany();
    //print_r ($html->stilusvaltozok);
    if (isset($html->stilusvaltozok[$nev])) {
        return true;
    } else {
        return false;
    }
}

function adminMenu($cim, $gombPozicio, $gombCsoport, $fuggveny, $ikon = '')
{

    $eleres = debug_backtrace();
    $eleresUt = dirname($eleres[0]['file']) . '/';
    $eleresUt = str_replace("\\", '/', $eleresUt);
    $eleresUt = trim($eleresUt, '/');
    $exp = explode('/', $eleresUt);
    $modul = end($exp);

    Memoria::tombHozzafuz('adminMenu', array(
        'cim' => $cim,
        'gombPozicio' => $gombPozicio,
        'gombCsoport' => $gombCsoport,
        'fuggveny' => $fuggveny,
        'ikon' => $ikon,
        'modul' => $modul));
}

function osztalyTolto($osztaly)
{
    $file = BASE . 'osztaly/osztaly_' . $osztaly . '.php';
    if (!file_exists($file)) {
        print '<strong>' . $file . ' nem elérhető</strong>';
        return;
    } else {
        if (class_exists($osztaly)) {
            return 1;
        }
        include_once ($file);
    }
}
function modulOsztalyTolto($osztaly)
{
    $komp = $osztaly.'Komp';
    $file = BASE . 'modulok/'.$osztaly.'/' . $osztaly . '.php';
    $file2 = BASE . 'modulok/'.$osztaly.'/' . $osztaly . 'Komp.php';
    
    if (!file_exists($file)) {
        print '<strong>' . $file . __f(' file nem elérhető').'</strong>';
        return;
    } else {
        require_once ($file);
        $o = new $osztaly();
        if (file_exists($file2)) {
            require_once ($file2);
            $o->komp = new $komp();
            
        }
        return $o;
    }
}
function magic_off()
{

    if (get_magic_quotes_gpc()) {
        $process = array(
            &$_GET,
            &$_POST,
            &$_COOKIE,
            &$_REQUEST);
        while (list($key, $val) = each($process)) {
            foreach ($val as $k => $v) {
                unset($process[$key][$k]);
                if (is_array($v)) {
                    $process[$key][stripslashes($k)] = $v;
                    $process[] = &$process[$key][stripslashes($k)];
                } else {
                    $process[$key][stripslashes($k)] = stripslashes($v);
                }
            }
        }
        unset($process);
    }
}

function isEmail($email)
{
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return false;
    } else {
        return true;
    }
}
function emberEllenorzo($imp = false)
{
    if ($imp !== false) {
        if ((int)$imp == $_SESSION['emberEllenorzo']) {

            return true;

        } else {

            return false;
        }
    }
    $r1 = rand(0, 10);
    $r2 = rand(0, 10);

    $str = $r1 . ' + ' . $r2 . ' = ';
    $_SESSION['emberEllenorzo'] = $r1 + $r2;
    return $str;

}

function bowConfirm()
{
    return ' onclick="if(confirm(\'Biztos vagy benne?\')==false) return false" ';
}

function kiskepKocka($url, $meret, $attr = '')
{
    print '<img ' . $attr . ' src="' . BASE_URL .
        '/osztaly/phpthumb/phpThumb.php?src=' . $url . '&w=' . $meret . '&h=' . $meret .
        '&zc=1" />';
}
function kiskepKockaStr($url, $meret, $attr = '')
{
    return '<img ' . $attr . ' src="' . BASE_URL .
        '/osztaly/phpthumb/phpThumb.php?src=' . $url . '&w=' . $meret . '&h=' . $meret .
        '&zc=1" />';
}
function kiskepKockaUrl($url, $meret)
{
    return BASE_URL . '/osztaly/phpthumb/phpThumb.php?src=' . $url . '&w=' . $meret .
        '&h=' . $meret . '&zc=1';
}
function kiskepXYStr($url, $meret_x, $meret_y, $attr = '')
{
    return '<img ' . $attr . ' src="' . BASE_URL .
        '/osztaly/phpthumb/phpThumb.php?src=' . $url . '&w=' . $meret_x . '&h=' . $meret_y .
        '&zc=1" />';
}
function kiskepXYUrl($url, $meret_x, $meret_y)
{
    return BASE_URL . '/osztaly/phpthumb/phpThumb.php?src=' . $url . '&w=' . $meret_x .
        '&h=' . $meret_y . '&zc=1';
}
function strToUrl($txt)
{
    $cim = mb_strtolower($txt, 'utf-8');

    $csere = explode(' ', 'Í É Á Ű Ő Ú Ö Ü Ó í á ű ő ú ü ó ö é');
    $mire = explode(' ', 'i e a u o u o u o i a u o u u o o e');

    foreach ($csere as $k => $v) {
        $pos = mb_strpos($cim, $v, 0, 'UTF-8');

        if ($pos !== false)
            $cim = mb_substr($cim, 0, $pos, 'UTF-8') . $mire[$k] . mb_substr($cim, $pos + 1,
                mb_strlen($cim), 'UTF-8');
    }


    $cim = str_replace(' ', '-', $cim);

    $cim = preg_replace('/[^(\x20-\x7F)]*/', '', $cim);
    $cim = preg_replace("/[^A-Za-z0-9 -]/", "", $cim);

    return $cim;
}

function kiterjesztes($str)
{
    $r = explode('.', $str);
    if (!empty($r)) {
        return end($r);

    } else {
        return false;
    }
}
function vanModul($pozicio)
{
    $sql = "SELECT id FROM bow_widgetxpozicio WHERE tema = '" . STILUS .
        "' AND pozicio = '" . $pozicio . "' LIMIT 1";
    $rs = sqluniv4($sql);

    if (empty($rs))
        return false;

    $url = Url::$peldany;
    $oldalId = (int)$url->oldal_id;

    // ha az oldal nem 0 akkor adott oldalhoz van rendelve
    $vanWidget = false;

    foreach ($rs as $sor) {
        $sql = "SELECT * FROM bow_widgetxoldal WHERE widget_id = " . $sor['id'] . " ";
        $vanOldalKorlatozas = sqluniv3($sql);
        if (!empty($vanOldalKorlatozas)) {
            // találtunk ide való oldalt
            $sql = "SELECT * FROM bow_widgetxoldal WHERE widget_id = " . $sor['id'] .
                " and oldal_id = $oldalId";

            $vanOldalKorlatozas = sqluniv3($sql);

            if (empty($vanOldalKorlatozas))
                continue;
            return true;
        } else {
            return true;
        }

    }


    return false;
}

function __f($str, $nyelv = '')
{
    if ($nyelv == '')
        $nyelv = $_SESSION["__nyelv"];
    $t = debug_backtrace();
    $base = BASE;

    $t[0]['file'] = str_replace('\\', '/', $t[0]['file']);
    $base = str_replace('\\', '/', $base);


    $hivo = str_replace(strtolower($base), '', strtolower($t[0]['file']));
    $hash = md5($hivo . '-' . $str . '-hu');
    $sql = "SELECT szoveg FROM bow_fordito WHERE hash = '$hash' and nyelv = '$nyelv' LIMIT 1";
    $rs = sqluniv3($sql);

    if (empty($rs)) {
        $sql = "SELECT szoveg FROM bow_fordito WHERE hash = '$hash' LIMIT 1";
        $rs = sqluniv3($sql);
    }
    if (empty($rs)) {
        $f = array(
            'hivo' => $hivo,
            'hash' => $hash,
            'szoveg' => $str,
            'nyelv' => $nyelv);
        sqladat($f, 'bow_fordito');

        return $str;
    }
    return $rs['szoveg'];
}

function htmlElem($modosito = '')
{
    if (!is_dir(BASE . 'temak/' . STILUS . '/html')) {
        mkdir(BASE . 'temak/' . STILUS . '/html');
    }
    list(, $caller) = debug_backtrace(false);
    $fname = strtolower($caller['class']) . '_' . strtolower($caller['function']) .
        '_' . $modosito . '.php';
    $path = BASE . 'temak/' . STILUS . '/html/' . $fname;
    if (!file_exists($path)) {
        file_put_contents($path, '<b>Html elem hiányzik: ' . $path . '</b>');
    }
    return $path;
}

function modulHtmlElem($modulNev, $modosito = '')
{
    if (!is_dir(BASE . 'temak/' . STILUS . '/html')) {
        mkdir(BASE . 'temak/' . STILUS . '/html');
    }
    if (!is_dir(BASE . 'modulok/' . $modulNev . '/html')) {
        mkdir(BASE . 'modulok/' . $modulNev . '/html');
    }
    $t = debug_backtrace(false);
    $file = strtolower($t[0]['file']);
    $file = str_replace(dirname($file), '', $file);
    $file = trim($file, '\\');
    $file = trim($file, '/');
    $file = str_replace(kiterjesztes($file), '', $file);
    $file = trim($file, '.');


    $fname = $modulNev . '_' . $file . (($modosito != '') ? '_' . $modosito : '') .
        '.php';
    $path = BASE . 'temak/' . STILUS . '/html/' . $fname;
    if (!file_exists($path)) {

        // témáknál nincs, nézzük a modul könyvtárban
        $path = BASE . 'modulok/' . $modulNev . '/html/' . $fname;
        if (!file_exists($path)) {
            file_put_contents($path, '<b>Html elem hiányzik: ' . $path . '</b>');
        }

    }
    return $path;
}

function epitoElemek()
{
    $dir = BASE . 'temak/' . STILUS . '/html/';
    $fileok = array();
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {

                if ($file == '.' or $file == '..')
                    continue;

                if (strpos($file, 'ep_') !== 0)
                    continue;
                if (strpos($file, '.php') === false)
                    continue;

                $fileok[] = $file;
            }
        }
    }

    return $fileok;
}

function szovegSablonok()
{
    $dir = BASE . 'temak/' . STILUS . '/html/';
    $fileok = array();
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {

                if ($file == '.' or $file == '..')
                    continue;

                if (strpos($file, 'cikk_') !== 0)
                    continue;
                if (strpos($file, '.php') === false)
                    continue;

                $fileok[] = $file;
            }
        }
    }

    return $fileok;
}

function osszesFile($dir, $szuro = '')
{
    $fileok = array();
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {

                if ($file == '.' or $file == '..')
                    continue;

                $ex = explode('.', $file);
                $ex = end($ex);

                if ($szuro != '') {
                    if ($ex != $szuro)
                        continue;
                }

                $fileok[] = $file;
            }
        }
    }
    return $fileok;
}

function epitoBetolt($file)
{
    $dir = BASE . 'temak/' . STILUS . '/html/';
    if (!file_exists($dir . $file)) {
        file_put_contents($dir . $file, '<b>Html elem hiányzik: ' . $dir . $file .
            '</b>');
    }
    return $dir . $file;
}

function konyvtarMasolo($src, $dst, $kovetes = false)
{
    $dir = opendir($src);
    if ($kovetes)
        print "Megnyitás: " . $src . "\n";
    @mkdir($dst);
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (strpos($file, '.git') !== false)
                continue;
            if (is_dir($src . '/' . $file)) {
                konyvtarMasolo($src . '/' . $file, $dst . '/' . $file, $kovetes);
            } else {
                if ($kovetes)
                    print "Másolás: " . $src . '/' . $file . " ide: " . $dst . '/' . $file . "\n";
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function shortcodeMintaAdatok($prefix, $string)
{
    $minta = shortcodeMinta($prefix);
    preg_match_all($minta, $string, $talalat);
    $ret = array();

    foreach ($talalat[0] as $k => $v) {
        $ret[$k]['kod'] = trim($v);

        if ($talalat[5][$k] != '') {
            $ret[$k]['inner'] = $talalat[5][$k];
        }

        $attr = trim($talalat[3][$k]);
        $attr = str_replace('&nbsp;', ' ', $attr);
        $attr = explode(' ', $attr);
        if (!empty($attr))
            foreach ($attr as $sor) {
                $reszek = explode('=', $sor);

                if (isset($reszek[0]) and isset($reszek[1]))
                    $ret[$k]['attr'][trim($reszek[0])] = trim($reszek[1], '" ');
            }
    }
    return $ret;

}

function shortcodeMinta($shortcode_tag)
{

    $pattern = '/\[(\[?)(' . $shortcode_tag . ')(?![\w-])([^\]\/]*(?:\/(?!\])[^\]\/]*)*?)(?:(\/)\]|\](?:([^\[]*+(?:\[(?!\/\2\])[^\[]*+)*+)\[\/\2\])?)(\]?)/';
    // 6: Optional second closing brocket for escaping shortcodes: [[tag]]
    return $pattern;
}
function shortcodeTorles($prefix, $string)
{
    $minta = shortcodeMinta($prefix);
    preg_match_all($minta, $string, $talalat);
    $ret = array();

    foreach ($talalat[0] as $k => $v) {
        $string = str_replace(trim($v), '', $string);
    }
    return $string;

}


function szovegKulcsSzerint($kulcs)
{
    $sql = "SELECT sz.* FROM bow_szovegek sz, bow_meta m WHERE m.kategoria = 'szovegkulcs' AND m.kulcs = '$kulcs' AND m.meta_id = sz.id LIMIT 1";
    return sqluniv3($sql);
}
function szovegCimkeSzerint($kulcs, $rendezes = '')
{
    $sql = "SELECT * FROM bow_szovegek sz, bow_cimkek c, bow_cimkexszoveg x WHERE c.cimke = '$kulcs' AND c.id = x.cimke_id AND x.szoveg_id = sz.id $rendezes";

    return sqluniv4($sql);
}


function sajatMasolatWebrol($src, $almappa = 'masolatok/')
{

    $fileNev = end(explode('/', $src));
    $kit = kiterjesztes($fileNev);

    $ujNev = md5($src) . '.' . $kit;

    $hova = BASE . 'feltoltesek/' . $almappa;
    if (!is_dir($hova))
        mkdir($hova);

    if (file_exists($hova . $ujNev))
        return array('path' => $hova . $ujNev);

    file_put_contents($hova . $ujNev, file_get_contents($src));

    return array('path' => $hova . $ujNev);

}

function modulKereso($fuggveny)
{
    $modulok = Memoria::olvas('modulBejegyzes');
    foreach ($modulok as $modul) {
        if ($modul['fuggveny'] == $fuggveny)
            return $modul;
    }
    return false;
}


function valasztoLista($tabla, $kulcsMezo, $ertekMezo, $rendezes)
{
    $sql = "SELECT $kulcsMezo, $ertekMezo FROM  $tabla ORDER BY $rendezes";
    $rs = sqluniv4($sql);
    $ret = array();
    if (!empty($rs))
        foreach ($rs as $sor) {
            $ret[$sor[$kulcsMezo]] = $sor[$ertekMezo];
        }

    return $ret;
}

function konyvtarTomb($dir)
{
    $dir = opendir($src);
    $ret = array();
    while (false !== ($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                $ret[$dir] = $dir;
            }

        }
    }
    closedir($dir);
    return $ret;
}

function cUrl($url)
{
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    //curl_setopt($ch, CURLOPT_POST, 1);
    //curl_setopt($ch, CURLOPT_POSTFIELDS,
    //    "postvar1=value1&postvar2=value2&postvar3=value3");

    // in real life you should use something like:
    // curl_setopt($ch, CURLOPT_POSTFIELDS,
    //          http_build_query(array('postvar1' => 'value1')));

    // receive server response ...
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $server_output = curl_exec($ch);

    curl_close($ch);
    return $server_output;
}

function widgetKereses($fuggveny) {
    $w = Beepulok::$widgetek;
    foreach ($w as $sor) {
        if ($sor['fuggveny']==$fuggveny) return $sor;
    }
    return false;
}

function vanKomponens ($uri) {
    $komponensek = Beepulok::$komponens;
    if (!empty($komponensek)) {
        foreach ($komponensek as $komp) {
            if ($komp['uri']== $uri) {
                return $komp;
            }
        }
    }
    return false;
}
