
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta name="language" content="hu" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{* stilusutvonal *}img/bstorelogo.png" /> 
    <link rel="bookmark icon" href="{* stilusutvonal *}img/bstorelogo.png" /> 

    <!-- Bootstrap core CSS -->
    <link href="{* stilusutvonal *}css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{* stilusutvonal *}css/heroic-features.css" rel="stylesheet">

    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
          </button>

          <a class="navbar-brand" href="<?= BASE_URL; ?>"><img style="margin-top: -3px;" src="{* stilusutvonal *}img/bstorelogo.png"/></a>
          
        </div>
        
        <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    {* respMenu(Főmenü) *}
                </ul>
            </div>
            <!-- /.navbar-collapse -->

      </div>
    </div>
    
    
    <?php
    if (vanModul('slider')):
    
    ?>
      {* slider *}
    
  
    
    <?php
    endif;
    ?>
    
    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        [TARTALOM]
      </div>

      <hr>

      <footer>
        <p>&copy; Company 2014</p>
      </footer>
    </div> <!-- /container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="{* stilusutvonal *}js/bootstrap.min.js"></script>
    
  </body>
</html>
