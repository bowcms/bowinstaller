var bowInit = false;

$().ready(function() {
    if (bowInit) return;
    bowInit = true;
    $('.katOpen').css('cursor','pointer');
    $('.katOpen').click(function(){ bowKatLenyilo(this); });
    
    $(".katMenu a").hover(function(){
        $(this).animate({ color: '#fed900'}, "slow");
    }, function() {
        $(this).animate({ color: '#000000'}, "slow"); //original color
    });
})

function bowKatLenyilo(b) {
    szint = $(b).parent().attr('class');
    szint = szint.split(' ');
    forras = -1;
    for(i = 0; i < szint.length; i++) {
        if (szint[i].indexOf('katSzint_')!=-1) {
            forras = szint[i].replace('katSzint_', '');
        }
    }
    if(forras==-1) return;
    forras = parseInt(forras);
    ujClass = 'katSzint_'+(forras+1);
    b = $(b).parent().next();
    
    while($(b).hasClass(ujClass)) {
        
        if ($(b).hasClass('katOpened')) {
            $(b).removeClass('katOpened');
            $(b).slideUp();
        } else {
            $(b).addClass('katOpened');
            $(b).slideDown();
        }
        
        b = $(b).next();
    }
}
