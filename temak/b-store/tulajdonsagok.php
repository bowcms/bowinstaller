<?php

/**
 * 
 * bootstrap téma tulajdonságok
 * 
 */

$t = array(
    'nev' => 'Bootstrap B-Store template',
    'szerzoLink' => '',
    'szerzo' => 'An o\'Nime',
    
    'kiskep' => 'kiskep.jpg'
);

// WIDGET POZCIÓK

$poziciok = array(
    'slider' => array(
        'nev' => 'Slider'),
    'tartalomBalSav' => array(
        'nev' => 'Tartalom melletti bal oldalsáv'),
    'miniKosar' => array(
        'nev' => 'Mini kosár'),
    'jobbSav'  => array(
        'nev' => 'Jobb oldalsáv'),
    'jobbFoot'  => array(
        'nev' => 'Footer jobb (Kapcsolat blokk)')
);



