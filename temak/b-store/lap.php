<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>{* meta_title *}</title>
		<meta name="keywords" content="{* meta_keywords*}" />
		<meta name="description" content="{* meta_description *}" />
		<meta name="language" content="hu" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="{* stilusutvonal *}img/logo.png" />
		<link rel="bookmark icon" href="{* stilusutvonal *}img/logo.png" />

		<!-- Bootstrap core CSS -->
		<link href="{* stilusutvonal *}css/bootstrap.css" rel="stylesheet">

		<!-- Add custom CSS here -->
		<link href="{* stilusutvonal *}css/bstore.css" rel="stylesheet">
		<link href="{* stilusutvonal *}font-awesome/css/font-awesome.min.css" rel="stylesheet">

		<script src="{* stilusutvonal *}js/jquery-1.10.2.js"></script>
		<script src="{* stilusutvonal *}js/bow.js?r=<?= rand(1000, 9999); ?>"></script>
		<link href='http://fonts.googleapis.com/css?family=Pontano+Sans|Quattrocento+Sans:400,400italic,700,700italic|Oswald:400,300,700&subset=latin,cyrillic,latin-ext,cyrillic-ext' rel='stylesheet' type='text/css'>

	</head>

	<body>

		<div class="section top">
			<div class="container">
				<div class="row">
					<nav class="navbar navbar-bg" role="navigation">
						<div class="container-fluid">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a class="navbar-brand" href="<?= BASE_URL; ?>"><img src="{* stilusutvonal *}img/logo.png" /></a>
							</div><!-- /.navbar-header -->
							<div class="collapse navbar-collapse navbar-ex1-collapse">
								<ul class="nav navbar-nav navbar-right">
									{* respMenu(Főmenü) *}
								</ul>
							</div><!-- /.navbar-collapse -->
						</div><!-- /.container -->
					</nav>
				</div><!-- /.row -->
			</div><!-- /container -->
		</div><!-- /section -->

		<div class="clear-both"></div>
					<?php
					if (vanModul('slider')):
					?>
					
					<div id="myCarousel" class="carousel slide">
						<!-- Indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>

						<!-- Wrapper for slides -->
						<div class="carousel-inner">
							<div class="item active">
								<div class="fill" style="background-image:url('{* stilusutvonal *}img/slider/1.jpg');"></div>
								<div class="carousel-caption">
									<h1>Jöjjön ide valami szöveg... vagy ne.</h1>
								</div>
							</div>
							<div class="item">
								<div class="fill" style="background-image:url('{* stilusutvonal *}img/slider/1.jpg');"></div>
								<div class="carousel-caption">
									<h1>Jöjjön ide valami szöveg... vagy ne.</h1>
								</div>
							</div>
							<div class="item">
								<div class="fill" style="background-image:url('{* stilusutvonal *}img/slider/1.jpg');"></div>
								<div class="carousel-caption">
									<h1>Jöjjön ide valami szöveg... vagy ne.<a href="http://b-store.hu">Akár egy link is.</a></h1>
								</div>
							</div>
						</div>

						<!-- Controls -->
						<a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="icon-prev"></span> </a>
						<a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="icon-next"></span> </a>
					</div>
					
					<?php
					endif;
					?>
				

		<!-- innen jön a tartalmi rész -->
		<div class="section flakes"></div><!-- .section -->
		<div class="section">
			<div class="container">
				<div class="row">

					<div class="section">
						<div class="container">
							<div class="row utvonal">
								{* navUtvonal *}
							</div><!-- /row utvonal -->
						</div><!-- /container -->
					</div><!-- /section -->

					<?php
					$tartalomCol = 12;
					if (vanModul('tartalomBalSav')){
					$tartalomCol = $tartalomCol-3;
					// bal sidebaros elrendezés
					?>

					<div class="col-lg-3">
						{* tartalomBalSav *}
					</div>

					<?php
					};
					if (vanModul('jobbSav')){
					$tartalomCol = $tartalomCol-3;
					}
					?>

					<div class="col-lg-<?= $tartalomCol; ?> ">
						[TARTALOM]
					</div>

					<?php

					if (vanModul('jobbSav')){

					// bal sidebaros elrendezés
					?>

					<div class="col-lg-3">
						{* jobbSav *}
						<!--{* miniKosar *}-->
					</div>

					<?php
					};
					?>
				</div><!-- /.row -->
			</div><!-- /container -->
		</div><!-- /section -->

		<div class="section hr">
			<div class="container">
				<div class="row">
					<p><img src="/temak/b-store/img/3flake.png" />
					</p>
				</div><!-- /.row -->
			</div><!-- /container -->
		</div><!-- /section -->

		<div class="section footer">
			<div class="container">
				<div class="row">
					<div class="col-md-3">
						<br />
						<img src="/temak/b-store/img/logo2.png" />
						<ul class="contacts">
							<li><span class="glyphicon glyphicon-home"></span>2831 Tarján Héregi út 3.</li>
							<li><span class="glyphicon glyphicon-phone-alt"></span>06 30 / 633-04-94<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;06 30 / 820-48-51</li>
							<li><span class="glyphicon glyphicon-envelope"></span><a href="mailto:info@b-store.hu">info@b-store.hu</a></li>
							<li><span class="glyphicon glyphicon-map-marker"></span>koordináták ide</li>
						</ul>
					</div>
					<div class="col-md-5"><img src="/temak/b-store/img/map.png" style="padding: 8px 0" />
					</div>
					<div class="col-md-4">
						{* jobbFoot *}
					</div>
				</div><!-- /.row -->
			</div><!-- /container -->
		</div><!-- /section -->

		<div class="section hr">
			<div class="container">
				<div class="row">
					<p><img src="/temak/b-store/img/3flake.png" />
					</p>
				</div><!-- /.row -->
			</div><!-- /container -->
		</div><!-- /section -->

		<!-- JavaScript -->
		<script src="{* stilusutvonal *}js/bootstrap.js"></script>
		<script src="{* stilusutvonal *}js/bstore.js"></script>
		<script src="{* stilusutvonal *}js/bow.js"></script>
	</body>

</html>
