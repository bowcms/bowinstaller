<?php
$cikk['tartalom'] = str_replace('<p>','',$cikk['tartalom']);
$cikk['tartalom'] = str_replace('</p>','<br />',$cikk['tartalom']);


?>
<div class="section">
<div class="page-header">
  <h1><?= $cikk['cim']; ?> <small style="float: right;"><?= date('Y m. d.', $cikk['letrehozva'])?></small></h1>
  <p class="lead"><?= nl2br($cikk['bevezeto']);?></p>
  <p><?= $cikk['tartalom']; ?></p>
</div>
</div>
