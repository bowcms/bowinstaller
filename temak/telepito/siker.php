<?php

/**
 * siker.php
 * 
 * Installácio befejezés html elemei
 * 
 * @author Cworrek
 * @copyright 2013
 */



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bow CMS telepítse</title>
<link rel="stylesheet" type="text/css" href="temak/telepito/css/style.css" />

</head>

<body>
    <div class="main">
        <div style="text-align: center;">
            <img src="temak/telepito/images/bow.jpg" />
        </div>
        <h1>Bow CMS telepítő felület
            <span class="slogen">Egyszerű és hatékony</span>
        </h1>
        
        <p class="msg">Gratulálok, sikeresen telepítetted a Bow Tartalomkezelő Rendszert. Kezdéshez kattints ide: <a href="index.php">Rajta!</a></p>
        
    </div>
</body>
</html>