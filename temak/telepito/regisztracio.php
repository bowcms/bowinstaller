<?php

/**
 * regisztracio.php
 * 
 * Installácio html elemei
 * 
 * @author Cworrek
 * @copyright 2013
 */



?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bow CMS telepítse</title>
<link rel="stylesheet" type="text/css" href="temak/telepito/css/style.css" />

</head>

<body>
    <div class="main">
        <div style="text-align: center;">
            <img src="temak/telepito/images/bow.jpg" />
        </div>
        <h1>Bow CMS telepítő felület
            <span class="slogen">Egyszerű és hatékony</span>
        </h1>
        <?php
        if (!is_writable(BASE.'beallitas/adatbazis.set.php')) {
            ?>
            <h2 style="color: red;text-align: center;">
                A konfigurációs file nem írható! Krjük adj írásjogot a <?= BASE.'beallitas/adatbazis.set.php' ?> file-nak!
            </h2>
            <?php
        }
        ?>
        <p class="msg">Örömmel üdvözöllek a BOW tartalomkezelő alkalmazás telepítő felületén. Néhány adat megadásával telepítheted a rendszert.</p>
        <div style="text-align: center;color:red"><?= Hitelesito::hibaKiiratas('dbhosthiba');?></div>
        <form method="post">
            <div class="elvalaszto">
                <img src="temak/telepito/images/db.png" />
                Adatbázis beállítások
            </div>
            <p>
                <label>Adatbázis kiszolgáló</label>
                <input name="a[DBHOST]" value="<?= $_POST['a']['DBHOST'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('DBHOST');?></div>
            </p>
            <p>
                <label>Adatbázis felhasználó</label>
                <input name="a[DBUSER]" value="<?= $_POST['a']['DBUSER'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('DBUSER');?></div>
            </p>
            <p>
                <label>Adatbázis jelszó</label>
                <input name="a[DBPASS]" value="<?= $_POST['a']['DBPASS'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('DBPASS');?></div>
            </p>
            <p>
                <label>Adatbázis név</label>
                <input name="a[DBNAME]" value="<?= $_POST['a']['DBNAME'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('DBNAME');?></div>
            </p>
            <div class="elvalaszto">
                <img src="temak/telepito/images/user.png" />
                Adminisztrátor felhasználó beállítása 
            </div>
            <p>
                <label>Vezetéknév</label>
                <input name="u[vezeteknev]" value="<?= $_POST['u']['vezeteknev'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('vezeteknev');?></div>
            </p>
            <p>
                <label>Keresztnév</label>
                <input name="u[keresztnev]" value="<?= $_POST['u']['keresztnev'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('keresztnev');?></div>
            </p>
            <p>
                <label>E-mail cím</label>
                <input name="u[email]" value="<?= $_POST['u']['email'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('email');?></div>
            </p>
            <p>
                <label>Jelszó</label>
                <input name="u[pwd]" value="<?= $_POST['u']['pwd'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('pwd');?></div>
            </p>
           
            
            <div class="elvalaszto">
                <img src="temak/telepito/images/home.png" />
                Webhely beállítások
            </div>
            <p>
                <label>Webhely URL-je (pl. http://valami.hu/enoldalam/)</label>
                <input name="w[BASE_URL]" value="<?= $_POST['w']['BASE_URL'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('BASE_URL');?></div>
            </p>
            <p>
                <label>CMS könyvtár (pl. enoldalam/ vagy ha gyökér könyvtárban vagyunk, akkor üres)</label>
                <input name="w[CMS_KONYVTAR]" value="<?= $_POST['w']['CMS_KONYVTAR'];?>" />
                
            </p>
            <p>
                <label>Oldalad címe</label>
                <input name="w[BOW_META_TITLE]" value="<?= $_POST['w']['BOW_META_TITLE'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('BOW_META_TITLE');?></div>
            </p>
            <p>
                <label>Kulcsszavak a keresőmotorok oldalak számára</label>
                <input name="w[BOW_META_KEYWORDS]" value="<?= $_POST['w']['BOW_META_KEYWORDS'];?>" />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('BOW_META_KEYWORDS');?></div>
            </p>
            <p>
                <label>Oldalad rövid leírása a keresőmotorok számára</label>
                <textarea name="w[BOW_META_DESC]" ><?= $_POST['w']['BOW_META_DESC'];?></textarea> 
                <div class="hibak"><?= Hitelesito::hibaKiiratas('BOW_META_DESC');?></div>
            </p>
            <div class="elvalaszto">
                <img src="temak/telepito/images/email.png" />
                E-mail küldés beállítás
            </div>
            <p>
                <label>Webhelyről kimenő levelek feladója</label>
                <input name="e[EMAIL_FELADO_NEV]" value="<?= $_POST['e']['EMAIL_FELADO_NEV'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('EMAIL_FELADO_NEV');?></div>
            </p>
            <p>
                <label>Webhelyről kimenő levelek e-mail címe</label>
                <input name="e[EMAIL_FELADO_CIM]" value="<?= $_POST['e']['EMAIL_FELADO_CIM'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('EMAIL_FELADO_CIM');?></div>
            </p>
            <p>
                <label>SMTP Host (hagyd üresen, ha az alapértelmezett küldést használod)</label>
                <input name="e[EMAIL_SMTP_HOST]" value="<?= $_POST['e']['EMAIL_SMTP_HOST'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('EMAIL_SMTP_HOST');?></div>
            </p>
            <p>
                <label>SMTP Port</label>
                <input name="e[EMAIL_SMTP_PORT]" value="<?= $_POST['e']['EMAIL_SMTP_PORT'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('EMAIL_SMTP_PORT');?></div>
            </p>
            <p>
                <label>SMTP felhasználó</label>
                <input name="e[EMAIL_SMTP_USER]" value="<?= $_POST['e']['EMAIL_SMTP_USER'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('EMAIL_SMTP_USER');?></div>
            </p>
            <p>
                <label>SMTP jelszó</label>
                <input name="e[EMAIL_SMTP_PASS]" value="<?= $_POST['e']['EMAIL_SMTP_PASS'];?>"  />
                <div class="hibak"><?= Hitelesito::hibaKiiratas('EMAIL_SMTP_PASS');?></div>
            </p>
            
            
            <br />
            <p style="text-align: center;">
                
                <input  type="submit" value="Indulhat a telepítés!" />
            </p>
            
            
        </form>
    </div>
</body>
</html>