(function( $ ){

  var methods = {
     init : function( options ) {
       
       return this.each(function(){
         
         var $this = $(this),
             data = $this.data('ajaxAblak');
             
        
         // If the plugin hasn't been initialized yet
         if ( ! data ) {
            
           if($('#ajaxFatyol').length == 0) {
                
                var fatyol = $('<div>');
                $(fatyol).attr('id', 'ajaxFatyol');
                $('body').append(fatyol);
                $('body').css('position', 'relative');
                $('body').css('top', '0');
                $('body').css('left', '0');
                $('body').css('margin', '0'); 
                $('body').css('padding', '0'); 
           } else {
                var fatyol = $('#ajaxFatyol');
           }
           if ($('#jqAjaxAblak').length == 0) {
                var ablak = $('<div id="jqAjaxAblak"><div id="ContentFrame"><div id="jqAjAblContent" class="jqAjaxAblCnt"></div></div></div>');
                $('body').append(ablak);
           } else {
                var ablak = $('#jqAjaxAblak');
           }

           $(this).data('ajaxAblak', {
               hivo : $this,
               fatyolSzin : '#000000',
               osztaly: 'ajaxAblak',
               fatyol: $('#ajaxFatyol'),
               ablak: $('#jqAjaxAblak'),
               szelesseg: 600,
               magassag: 400,
               href: ''
           });
           
           var $this = $(this),
           data = $this.data('ajaxAblak');
           h = (($(window).height()>$('body').height())?$(window).height():$('body').height());
           w = (($(window).width()>$('body').width())?$(window).width():$('body').width());
           $('#ajaxFatyol').attr('style','background:'+data.fatyolSzin+';top:0;left:0;width:'+w+'px;height:1600px;filter:alpha(opacity=50);-moz-opacity:0.5;opacity: 0.5;display:none');
           
           
         }
         $this.click(function (e) {
            e.preventDefault();
            var $this = $(this),
            data = $this.data('ajaxAblak');
            $this.ajaxAblak('show');
         });
         
       });
     },
     destroy : function( ) {

       return this.each(function(){

         var $this = $(this),
             data = $this.data('ajaxAblak');

         $(window).unbind('.'+data.osztaly);
         data.ajaxAblak.remove();
         $this.removeData('ajaxAblak');

       })

     },
     show : function( ) {
        
        return this.each(function(){
         var  pos = $(this).position();

         var $this = $(this),
         data = $this.data('ajaxAblak'), pos = $this.position();
         
         $('#ContentFrame .jqAjaxButton').remove();
         $('#ContentFrame br').remove();
         $('#jqAjAblContent').html('<center>Betöltés folyamatban!</center>');
         
         h = $(window).height();
                w = $(window).width();
                
                if (h<data.magassag) {
                    data.magassag = parseInt(h-120);
                }
                if (w<data.szelesseg) {
                    data.szelesseg = parseInt(w-120);
                }
                
                ablak = data.ablak;
                $(ablak).css('width',data.szelesseg+'px');
                //$(ablak).css('height',data.magassag+'px');
                
                //$('#jqAjAblContent').css('height',(data.magassag-80)+'px');
                //$('#ContentFrame').css('height',(data.magassag-20)+'px');
                
                
                wtop = parseInt((h-data.magassag)/2);
                wleft = parseInt((w-data.szelesseg)/2);
                
                wtop = pos.top+600;
                // katt
                FB.Canvas.scrollTo(0,wtop-50);
                
                $(ablak).css('top',wtop+'px');
                $(ablak).css('left',wleft+'px');
                
                data.top = wtop;
                data.left = wleft;
                
                th = $('#jqAjAblContent').height();
                
                $('#jqAjaxAblak').fadeIn();
                
                fatyol = data.fatyol;
                $(fatyol).show();
         
            loadThis = $this.attr('href');
            
         $.ajax({
            url: loadThis,
            success: function(d) {
                $('#ContentFrame .jqAjaxButton').remove();
                $('#ContentFrame br').remove();
                $('#jqAjAblContent').html(d);
                
                gombok = $('#jqAjAblContent .jqAjaxButton');
                //$('#ContentFrame').append('<br />')
                if (gombok.length > 0) {
                    for (_i = 0;_i<gombok.length; _i++) {
                        gomb = gombok[_i];
                        if ( $(gomb).hasClass('Bezar')) {
                            
                            $(gomb).click(function(e){
                                e.preventDefault();
                                $this.ajaxAblak('close');
                                
                            });    
                        } else {
                            $(gomb).ajaxAblak();
                        }
                        $('#ContentFrame').append(gomb);
                        
                    }
                    //$('#ContentFrame').append('<br />');
                }
                $('#jqAjAblContent .jqAjaxButton').remove();
                
            }
        });
         

       })
     },
     close : function () { 
        var $this = $(this),
        data = $this.data('ajaxAblak');
        $('#jqAjaxAblak').fadeOut();
        $('#ajaxFatyol').fadeOut();
        
     }
     
  };

  $.fn.ajaxAblak = function( method ) {
    
    if ( methods[method] ) {
      return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
        
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
    }    
  
  };

})( jQuery );
