<?php
include_once("../../sql/sqlconfig.php");
include_once("../../sql/sqlfuggveny.php");
include_once ("../../osztaly/kozos_fuggvenyek.php");

//ini_set('display_errors', 1);

if (isset($_POST['nev'])) {
    $nev = trim($_POST['nev']);
    $email = trim($_POST['email']);
    
    if ($nev == '') {
        $hiba = 'Kérjük, add meg nevedet!';
    }
    if ($email == '') {
        $hiba = 'Kérjük, add meg email címedet!';
    }
    
    if (!joEmail($email)) {
        $hiba = 'E-mail címed nem megfelelő';
    }
    $sql = "SELECT * FROM arkad_regi WHERE app = 'bazar' AND email = '$email' LIMIT 1";
    //print $sql;
    if ($hiba == '') {
        $rs = sqluniv3($sql);
        if (isset($rs['id'])) {
            $hiba = 'Ezzel az email címmel már létezik regisztráció!';
        }
    }
    
    if ($hiba == '') {
        
        $a = array('nev' => $nev, 'email' => $email, 'datum' => date('Y-m-d'), 'app' => 'bazar', 'egyeb' => $_POST['egyeb']);
        sqlfelvitel($a, 'arkad_regi');
        ?>
        <h3>Köszönjük, regisztrációdat rögzítettük!</h3>
        <?php
        exit;
    }
}

?>

<form onsubmit="return false;" class="myform">
<?php if ($hiba != ''):?>
<h2 style="text-align: center; color: blue"><?= $hiba; ?></h2>
<?php endif; ?>
<p>
    <label>Neved:</label>
    <input name="nev" type="text" value="<?= $_POST['nev']?>"/>
</p>
<p>
    <label>E-mail címed:</label>
    <input name="email" type="text" value="<?= $_POST['email']?>"/>
</p>
<p>
    <label>Egyéb elérhetőségek:</label>
    <input name="egyeb" type="text" value="<?= $_POST['egyeb']?>"/>
</p>

<p>
    <label> </label>
    <input type="button" value="Regisztrálok" onclick="$(this).val('Folyamatban...');this.disabled=true;$.post('regisztracio.php', $('.myform').serialize(), function(d){ $('#jqAjAblContent').html(d) });" />
</p>



</form>
