<div style="height: 380px;overflow-y: scroll; color: #fff;" id="szab">
<style>
#szab p {
    border: none !important;
    margin: 10px !important;
    padding: 0px !important;
    
}
</style>
<H3 CLASS="western"><FONT >&ldquo;<FONT FACE="Calibri, sans-serif"><FONT SIZE=3>Fórum
arca&rdquo;</FONT></FONT></FONT></H3>
<P CLASS="western"><FONT ><FONT FACE="Calibri, sans-serif">Nyereményjáték-szabályzat</FONT></FONT></P>
<P CLASS="western"><FONT ><FONT FACE="Calibri, sans-serif">A
nyereményjátékban részt vehet az, aki valamennyi az alábbiakban
felsorolt feltételnek eleget tesz:</FONT></FONT></P>
<P CLASS="western"><FONT >&nbsp;<FONT FACE="Calibri, sans-serif">18.
életévét betöltötte,</FONT></FONT></P>
<P CLASS="western"><FONT ><FONT FACE="Calibri, sans-serif">2013.&nbsp;
január 23.&nbsp;16:00&nbsp;óra&nbsp;és&nbsp;január&nbsp;27.&nbsp;20:00&nbsp;óra&nbsp;között&nbsp;a
Fórum Debrecen Facebook&nbsp;oldalán
(</FONT></FONT><FONT COLOR="#000080"><U><A HREF="https://www.facebook.com/forumdebrecen"><FONT FACE="Calibri, sans-serif">https://www.facebook.com/forumdebrecen</FONT></A></U></FONT><FONT FACE="Calibri, sans-serif">
)</FONT><FONT ><FONT FACE="Calibri, sans-serif">&nbsp;a
Fórum Arca játékosaira szavaz,</FONT></FONT></P>
<P CLASS="western"><STRONG><FONT ><FONT FACE="Calibri, sans-serif">A
nyereményjátékban 2&nbsp;db&nbsp;5&nbsp;000 Ft értékű Sugarbird
vásárlási utalvány kerül&nbsp;kisorsolásra.</FONT></FONT></STRONG></P>
<P CLASS="western"><FONT >&nbsp;<FONT FACE="Calibri, sans-serif">A
játékban csak hiánytalanul kitöltött regisztrációval lehet részt
venni, ami jelen esetben az alkalmazás teljes engedélyezését jelenti.</FONT></FONT></P>
<P CLASS="western"><FONT ><FONT FACE="Calibri, sans-serif">A
játék független a Facebooktól, a Facebook nem vesz részt benne és nem
támogatja.</FONT></FONT></P>
<P CLASS="western"><FONT ><FONT FACE="Calibri, sans-serif">A&nbsp;résztvevők&nbsp;vállalják,&nbsp;hogy&nbsp;a&nbsp;&bdquo;Fórum
Arca&rdquo;&nbsp; játék időtartama&nbsp;alatt,&nbsp;valamint&nbsp;a&nbsp;díjátadáskor&nbsp;a
szervezők róluk felvételeket készíthetnek és a fotókat, amelyeken
felismerhetők és beazonosíthatók, a Fórum Debrecen üzemeltetője , az
</FONT></FONT><FONT COLOR=""><FONT FACE="Calibri, sans-serif"><SPAN STYLE=" ">ECE
Projektmanagement G.m.b.H. &amp; Co. KG </SPAN></FONT></FONT><FONT ><FONT FACE="Calibri, sans-serif">.&nbsp;nyomtatott&nbsp;és
elektronikus&nbsp;anyagaiban&nbsp;felhasználhatja.&nbsp;Továbbá&nbsp;hozzájárulnak&nbsp;ahhoz,&nbsp;hogy&nbsp;adataikat&nbsp;a
Bevásárlóközpont adatbázisában rögzítse, és marketing céljaira
használja (pl. eseményekről, kiállításokról, nyereményjátékokról,
stb. értesítés, infó-levél küldése). Amennyiben a résztvevők ehhez
nem járulnak hozzá, azt az&nbsp;</FONT></FONT><FONT FACE="Calibri, sans-serif">info@forumdebrecen.hu</FONT><FONT ><FONT FACE="Calibri, sans-serif">&nbsp;e-mail
címen jelezhetik, amelyet a szervezők egy válasz e-maillel tudomásul
vesznek.</FONT></FONT></P>
<P CLASS="western"><EM><FONT ><FONT FACE="Calibri, sans-serif">Szavazni&nbsp;január&nbsp;27-én&nbsp;20:00&nbsp;óráig&nbsp;lehet,&nbsp;a&nbsp;gy&Aring;&lsquo;ztest&nbsp;pedig&nbsp;telefonon&nbsp;értesítjük.</FONT></FONT></EM></P>
<P CLASS="western"><FONT >&nbsp;<FONT FACE="Calibri, sans-serif">A
nyeremények készpénzre nem válthatók!</FONT></FONT></P>
<P CLASS="western"><FONT ><FONT FACE="Calibri, sans-serif">A&nbsp;Nyertes&nbsp;az&nbsp;</FONT></FONT><FONT COLOR=""><FONT FACE="Calibri, sans-serif"><SPAN STYLE=" ">ECE
Projektmanagement G.m.b.H. &amp; Co. KG
</SPAN></FONT></FONT><FONT ><FONT FACE="Calibri, sans-serif">&nbsp;facebook&nbsp;oldalán&nbsp;található&nbsp;&bdquo;Fórum
arca&rdquo;&nbsp;alkalmazásban való szavazatával és regisztrációjával
igazolja, hogy a játékszabályokat megismerte, tudomásul vette és
elfogadta.
A&nbsp;nyereményjáték&nbsp;kapcsán&nbsp;felmerülő&lsquo;&nbsp;valamennyi&nbsp;további&nbsp;költséget&nbsp;az&nbsp;</FONT></FONT><FONT COLOR=""><FONT FACE="Calibri, sans-serif"><SPAN STYLE=" ">ECE
Projektmanagement G.m.b.H. &amp; Co. KG </SPAN></FONT></FONT><FONT ><FONT FACE="Calibri, sans-serif">Bevásárlóközpont
viseli. A&nbsp;játékban&nbsp;az </FONT></FONT><FONT COLOR=""><FONT FACE="Calibri, sans-serif"><SPAN STYLE=" ">ECE
Projektmanagement G.m.b.H. &amp; Co.
KG</SPAN></FONT></FONT><FONT ><FONT FACE="Calibri, sans-serif">&nbsp;dolgozói&nbsp;és&nbsp;hozzátartozóik&nbsp;nem&nbsp;vehetnek&nbsp;részt.</FONT></FONT></P>
</div>

<a class="jqAjaxButton Bezar">Bezár</a>
