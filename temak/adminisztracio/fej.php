<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="hu" />

	
    
<!-- CSS -->
<link href="{* stilusutvonal *}style/css/transdmin.css?r=<?= rand(100,999);?>" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="screen" href="style/css/ie6.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="screen" href="style/css/ie7.css" /><![endif]-->

<!-- JavaScripts-->
<script type="text/javascript" src="<?= BASE_URL; ?>js/jquery.js"></script>
<script type="text/javascript" src="{* stilusutvonal *}style/js/jNice.js"></script>
<script type="text/javascript">
    // <!--
    $().ready(function() {
        $('tr:odd').css('background-color', '#fff');
    });
    
    // -!>
</script>
</head>
<?php
$url = Url::$peldany;
$p1 = $url->reszek[1];
$mem = Memoria::peldany();
?>
<body>
	<div id="wrapper">
    	<!-- h1 tag stays for the logo, you can use the a tag for linking the index page -->
    	<h1 style="background: none;"><a href="<?= ADMIN_URL; ?>">{* meta_title *}</a></h1>
        
        <!-- You can name the links with lowercase, they will be transformed to uppercase by CSS, we prefered to name them with uppercase to have the same effect with disabled stylesheet -->
        <ul id="mainNav">
        	<li><a  href="<?= ADMIN_URL ?>_kozpont" <?= ($p1 == '_kozpont')?' class="active" ':''?> >KÖZPONT</a></li> <!-- Use the "active" class for the active menu item  -->
        	<li><a href="<?= ADMIN_URL ?>_oldalak/lista" <?= ($p1 == '_oldalak')?' class="active" ':''?>  >OLDALAK</a></li>
        	<li><a href="<?= ADMIN_URL ?>_widget/poziciok" <?= ($p1 == '_widget')?' class="active" ':''?>  >WIDGETEK</a></li>
        	<li><a href="<?= ADMIN_URL ?>_menu/menucsoportok" <?= ($p1 == '_menu')?' class="active" ':''?> >MENÜK</a></li>
        	<li><a href="<?= ADMIN_URL ?>_modulok/lista/beallit/23" <?= ($p1 == '_modulok')?' class="active" ':''?> >SZÖVEGEK</a></li>
        	<li><a href="<?= ADMIN_URL ?>_aruhaz/kozpont" <?= ($p1 == '_aruhaz')?' class="active" ':''?> >ÁRUHÁZ</a></li>
        	
            <li class="logout"><a href="#">KILÉPÉS</a></li>
        </ul>
        <!-- // #end mainNav -->
        
        <div id="containerHolder">
			<div id="container" style="<?= (($mem->olvas('teljes_szeles_admin'))?'background: #fff;':'')?>;">
                <?php
                
                if (!$mem->olvas('teljes_szeles_admin')) {
                ?>
        		<div id="sidebar" >
                    {* _adminMenu.modulkezeloLinkek() *}
                	
                    <!-- // .sideNav -->
                </div>
                <?php
                }
                ?>
                <!-- // #sidebar -->
                
                <!-- h2 stays for breadcrumbs -->
                <div class="kezelok">{* kezelogombok *}</div>
                <h2  style="<?= (($mem->olvas('teljes_szeles_admin'))?'width: initial;float:none;padding-left:20px':'')?>;">{* lapCim *}</h2>
                
                <div id="main" style="<?= (($mem->olvas('teljes_szeles_admin'))?'width: 98%;':'')?>;">
