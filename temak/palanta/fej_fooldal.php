<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="hu" />

<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<link href="{* stilusutvonal *}css/style.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="{* stilusutvonal *}js/jquery.min.js"></script>


<script>
    $().ready(function() {
        lista = $('.lenyilo');
        for(i = 0 ; i < lista.length; i++) {
            
            szulo = $(lista[i]).parent();
            //$(lista[i]).width() = $(szulo).width();
            ujlista = $(lista[i]).clone();
            $(lista[i]).remove();
            $(ujlista).attr('id', 'llista'+i);
            $(szulo).attr('id', 'glista'+i);
            
            $('body').append(ujlista);
            l= $(szulo).position();
            $(ujlista).css('top', l.top);
            ll = parseInt(l.left)-10;
            $(ujlista).css('left', ll+"px");
            $(ujlista).css('display', 'none');
            $('#glista'+i).mouseover(function() {
                $('.lenyilo').hide();
                id = $(this).attr('id');
                id = id.replace('glista', '');
                $('#llista'+id).show();
                $('#llista'+id).mouseleave(function() {
                    $(this).fadeOut();
                });
            })
            
            
            
        }    
    });
    
    
</script>
</head>

<body style="position: relative;">
{* body_utan *}
    <div class="wrapper">
    <div class="head">
        <div class="hlogo">
            <a href="<?= BASE_URL; ?>" title="{* meta_title *}">
                <img src="{* stilusutvonal *}images/logo.png"/>
            </a>
        </div>
        <div class="hhmenu">
            <ul>
                {* palantaMenu(Főmenü) *}
            </ul>
        </div>
    </div>
    
    {* topFullWith *}
    
    <div class="page">
        <?php
        if (vanModul('balsav')):
        ?>
        <div class="jobbblokk">
        {* balsav *}
        </div>
        <?php
        endif;
        ?>
        {* uzenetbox *}
        {* pageTop *}