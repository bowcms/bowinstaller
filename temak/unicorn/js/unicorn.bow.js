/**
*   Unicorn Bow javascript
*/

$().ready(function() {
    $("span.icon input:checkbox, th input:checkbox").click(function() {
	  
		var checkedStatus = this.checked;
		var checkbox = $(this).parents('.widget-box').find('tr td:first-child input:checkbox');		
		checkbox.each(function() {
			this.checked = checkedStatus;
			if (checkedStatus == this.checked) {
				$(this).closest('.checker > span').removeClass('checked');
			}
			if (this.checked) {
				$(this).closest('.checker > span').addClass('checked');
			}
		});
	});	
    
    $('.alert-block a.close').click(function() {
        $(this).parent().slideUp();
    })
})

function kotegeltTorlesUniBow(o) {
    var ret = '';
    
		var checkbox = $(o).parents('.widget-box').find('tr td:first-child input:checkbox');	
		checkbox.each(function() {
			if (this.checked) ret = ret + '_' +$(this).val();
		});
        
        
        return ret;
}
function kotegeltSorrendMentes(o) {
    var ret = '';
    inp = $('.sorrendInp');
    for(i = 0; i < inp.length; i++) ret += $(inp[i]).attr('rel')+'_'+$(inp[i]).val()+'|';
    
    return ret;
}
function gitterUzenet(cim, leiras) {
   
    $.gritter.add({
			title:	cim,
			text:	leiras,
			sticky: false });
}
