<!DOCTYPE html>
<html lang="en">
	<head>
		<title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/bootstrap.min.css" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/bootstrap-responsive.min.css" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/fullcalendar.css" />	
		<link rel="stylesheet" href="{* stilusutvonal *}css/unicorn.main.css" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/unicorn.blue.css" class="skin-color" />
        <link rel="stylesheet" href="{* stilusutvonal *}css/jquery.gritter.css" />
        <link rel="stylesheet" href="{* stilusutvonal *}css/colorpicker.css" />
        <link rel="stylesheet" href="{* stilusutvonal *}css/datepicker.css" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/uniform.css" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/select2.css" />
        
        <script src="{* stilusutvonal *}js/excanvas.min.js"></script>
        <script src="{* stilusutvonal *}js/jquery.min.js"></script>
        <script src="{* stilusutvonal *}js/jquery.ui.custom.js"></script>
        
        <script src="{* stilusutvonal *}js/bootstrap.min.js"></script>
        <script src="{* stilusutvonal *}js/bootstrap-colorpicker.js"></script>
        <script src="{* stilusutvonal *}js/bootstrap-datepicker.js"></script>
        <script src="{* stilusutvonal *}js/jquery.peity.min.js"></script>
            
        <script src="{* stilusutvonal *}js/jquery.uniform.js"></script>
        <script src="{* stilusutvonal *}js/jquery.gritter.min.js"></script>
        <script src="{* stilusutvonal *}js/select2.min.js"></script>
        <script src="{* stilusutvonal *}js/unicorn.js"></script>
        <script src="{* stilusutvonal *}js/unicorn.form_common.js"></script>
        <script src="{* stilusutvonal *}js/unicorn.bow.js"></script>
       
  </head>
	<body>
		
		
		<div id="header">
			<h1><a href="./dashboard.html">Unicorn Admin</a></h1>		
		</div>
		
		
		<div id="user-nav" class="navbar navbar-inverse">
            <ul class="nav btn-group">
                <li class="btn btn-inverse"><a title="" href="?nyelvbeallit=hu"><i class="icon icon-flag"></i> <span class="text">Magyar</span></a></li>
                <li class="btn btn-inverse"><a title="" href="?nyelvbeallit=en"><i class="icon icon-flag"></i> <span class="text">English</span></a></li>
                <li class="btn btn-inverse"><a title="" target="_blank" href="<?= BASE_URL; ?>"><i class="icon icon-play-circle" ></i> <span class="text"><?= __f('Oldal megtekintése')?></span></a></li>
                				
                <!--<li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-user"></i> <span class="text">Profile</span></a></li>
                <li class="btn btn-inverse"><a title="" href="#"><i class="icon icon-cog"></i> <span class="text">Settings</span></a></li>-->
                <li class="btn btn-inverse"><a title="" href="<?= BASE_URL; ?>uniadmin/kilep"><i class="icon icon-share-alt"></i> <span class="text">Logout</span></a></li>
            </ul>
        </div>
            
		<div id="sidebar">
			<a href="<?= BASE_URL; ?>uniadmin" class="visible-phone"><i class="icon icon-home"></i> <?= __f('Adminisztráció'); ?></a>
			<ul>
				<li><a href="<?= BASE_URL; ?>uniadmin"><i class="icon icon-home"></i> <span><?= __f('Vezérlőpult'); ?></span></a></li>
				{* unicornOldalMenu *}
                
			</ul>
		
		</div>
		
		
		
		<div id="content">
			<div id="content-header">
				<h1>{*  lapCim *}</h1>
                <div class="btn-group">
                    {* unicornFomenu *}
                </div>
				
			</div>
			<div id="breadcrumb">
				<a href="<?= BASE_URL.'uniadmin'; ?>" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> <?= __f('Főoldal'); ?></a>
				{* adminUtvonal *}
			</div>
			<div class="container-fluid">
				<div class="row-fluid">
					<div class="span12 center" id="tartalomDiv" style="text-align: center;">
                        [TARTALOM]				
						
					</div>	
				</div>
				
				<div class="row-fluid">
					<div id="footer" class="span12">
						<?= date('Y');?> &copy; Unicorn Admin for BowCMS.
					</div>
				</div>
			</div>
		</div>
		

            
	</body>
</html>
