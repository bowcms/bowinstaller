<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Unicorn Admin</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/bootstrap.min.css" />
		<link rel="stylesheet" href="{* stilusutvonal *}css/bootstrap-responsive.min.css" />
        <link rel="stylesheet" href="{* stilusutvonal *}css/unicorn.login.css" />
        <script src="{* stilusutvonal *}js/jquery.min.js"></script>  
        <script src="{* stilusutvonal *}js/unicorn.login.js"></script> 
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    
        </head>
    <body>
        <div id="logo">
            <img src="{* stilusutvonal *}img/logo.png" alt="" />
        </div>
        <div id="loginbox">            
            <form id="loginform" class="form-vertical" method="post" action="<?= BASE_URL; ?>uniadmin" >
				<p><?= __f('Addja meg e-mail címét és jelszavát')?></p>
                <?php if (vanHelyorzo('login_hiba')):?>
                <strong class="warningalert">{* login_hiba *}</strong>
                <?php endif;?>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span><input name="unicorn[email]" type="text" placeholder="E-mail" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-lock"></i></span><input name="unicorn[jelszo]" type="password" placeholder="<?= __f('Jelszó'); ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link" id="to-recover"><?= __f('Elfelejtett jelszó'); ?></a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Login" /></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical" method="post" />
				<p><?= __f('Addja meg e-mail címét, hogy elküldhessük a visszaállító kódot!'); ?></p>
                
				<div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span><input name="unicorn[elfelejtett_jelszo]" type="text" placeholder="E-mail" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link" id="to-login">&lt; <?= __f('Vissza'); ?></a></span>
                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="<?= __f('Elküldés'); ?>" /></span>
                </div>
            </form>
            <?php
            
            ?>
            <form id="newpass" action="#" class="form-vertical" method="post" />
				<p><?= __f('Addja meg új jelszavát!'); ?></p>
                
				<div class="control-group">
                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-envelope"></i></span><input id="ujjelszo" name="unicorn[ujjelszo]" type="password" placeholder="<?= __f('Új jelszó'); ?>" />
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-right"><input type="submit" id="ujjelszoSubmit" class="btn btn-inverse" value="<?= __f('Módosítás'); ?>" onclick="tesztUniUjjelszo(this);" /></span>
                </div>
            </form>
        </div>
        
        <script>
        $().ready(function(e){
            $('#ujjelszoSubmit').click(function(e){
                e.preventDefault();
            })
        })
        function tesztUniUjjelszo(o) {
            j = $('#ujjelszo');
            
            if ($(j).val().length < 6) {
                $(j).css('color', 'red').css('background', '#aef2c2');
                
                return false;
            }
            $('#newpass').submit();
        }
        </script>
    </body>
</html>
