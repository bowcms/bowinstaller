<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="hu" />
<link href="{* stilusutvonal *}style.css" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="{* stilusutvonal *}js/jquery.min.js"></script>


</head>
<body>
<div id="wrapper">
	<div id="header">
		<div id="logo">
			<h1><a href="<?= BASE_URL; ?>" title="{* meta_title *}">BowCMS</a></h1>
			<p><a href="<?= BASE_URL; ?>">Tartalomkezelő rendszer</a> </p>
		</div>
	</div>
	<!-- end #header -->
	<div id="menu">
		<ul>
			{* stilusMenu(Főmenü) *}
		</ul>
	</div>
	<!-- end #menu -->
	<div id="page">
		<div id="content">
            
            {* infoSliderTartalom *}
            {* uzenetbox *}
            {* leagazasok *}