<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="hu" />

<link rel="shortcut icon" href="http://www.derby-web-design-agency.co.uk/themes/response/images/favicon.png" /> 
<link rel="bookmark icon" href="http://www.derby-web-design-agency.co.uk/themes/response/images/favicon.png" /> 
<link href="{* stilusutvonal *}css/main.css" rel="stylesheet" type="text/css">
<meta name="apple-mobile-web-app-capable" content="yes" /> 
<meta name="apple-mobile-web-app-status-bar-style" content="grey" /> 
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" /> 
<script src="{* stilusutvonal *}js/jquery.min.js"></script>
<script type="text/javascript" src="{* stilusutvonal *}js/jquery.nivo.slider.js"></script>
<script src="{* stilusutvonal *}js/jquery.prettyPhoto.js" type="text/javascript"></script>
<script src="{* stilusutvonal *}js/twitter.js"></script>    
<script src="{* stilusutvonal *}js/custom.js"></script>   
<script>
	//// Start Simple Sliders ////
	$(function() {
		setInterval("rotateDiv()", 10000);
	});
		
		function rotateDiv() {
		var currentDiv=$("#simpleslider div.current");
		var nextDiv= currentDiv.next ();
		if (nextDiv.length ==0)
			nextDiv=$("#simpleslider div:first");
		
		currentDiv.removeClass('current').addClass('previous').fadeOut('2000');
		nextDiv.fadeIn('3000').addClass('current',function() {
			currentDiv.fadeOut('2000', function () {currentDiv.removeClass('previous');});
		});
	
	}
	//// End Simple Sliders //// 
</script> 
<script type="text/javascript" charset="utf-8">
  $(document).ready(function(){
    $("a[rel^='prettyPhoto']").prettyPhoto();
  });
</script>



</head>
<body style="position: relative;">
{* body_utan *}
<div id="header">
    	<!-- Start navigation area -->
        <div id="navigation">

        	<div id="navigation_wrap">

                <!-- Start contact info area -->
                <div id="conteactinfo"><strong>Email:</strong> info@domainname.com  |  <strong>Phone:</strong> (+44) 1234 567 890</div>
                <!-- End contact info area -->
                <!-- Start navigation -->
                <div id="navi">
                
                    <ul>
                        {* respMenu(Főmenü) *}
                    </ul>
                </div>
                <!-- End navigation -->
                
			</div>
        
        </div>
        <!-- End navigation area -->
        <div class="clear"></div>
    <!-- Start Social & Logo area -->
        <div id="header_small">
        	<!-- Start Social area -->
        	<div class="social">
            	
                <ul>
                <li><a href="#" class="social-google"></a></li>
                <li><a href="#" class="social-facebook"></a></li>
                <li><a href="#" class="social-twitter"></a></li>
                
                </ul>
                
            </div>
            <!-- End Socialarea -->
            
            <!-- Start Logo area -->
            <div id="logo">
              <a href="<?= BASE_URL; ?>" title="Response">BOWCMS</a>
            </div>
            <!-- End Logo area -->
        
        </div>
        <!-- End Social & Logo area -->

</div>

<div id="main">
    <?php
    
    if (vanModul('respoSlider')):
    ?>
    <!-- Start Slider Wrapping -->
    <div id="sliderwrap">
		
        <!-- Start Slider -->
        <div id="slider" class="nivoSlider">
            <img src="{* stilusutvonal *}images/slider-banners/slider01.jpg" alt=""/>
            <img src="{* stilusutvonal *}images/slider-banners/slider02.jpg" alt=""/>
            <img src="{* stilusutvonal *}images/slider-banners/slider03.jpg" alt=""/>
        </div>
        <!-- End Slider -->
        <!-- Start Slider HTML Captions -->
        <div id="htmlcaption" class="nivo-html-caption">
            <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>.
        </div>
        <!-- End Slider HTML Captions -->
    
    </div>
    <br style="clear: both;"/><br /><br />
    <?php
    endif;
    ?>
    <!-- End Slider Wrapping -->
    <!-- Start H1 Title -->
    <div class="titlesnormal">
    
    	<h1>{* topFullWith *}</h1>
        
        <span></span>
    
    </div>
    
    <!-- End H1 Title -->
    <!-- Start Main Body Wrap -->
    <div id="main-wrap">
        
        <?php
        if (vanModul('ajanloDobozBal')):
        ?>
    	<!-- Start Featured Boxes -->
        <div class="boxes-third boxes-first">
        
        	<div class="boxes-padding">
            
            	<div class="bti">
                	<div class="featured-images"><img src="images/responsive-icon.png" width="72" height="53" alt="Responsive"></div>
                	<div class="featured-titles">Responsive Html</div>
                </div>
                <div class="featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non ipsum nunc, nec sagittis tellus.</div>
            
            </div>
            
            <span class="box-arrow"></span>
        
        </div>
        <?php
        endif;
        ?>
        
        <?php
        if (vanModul('ajanloDobozKozep')):
        ?>
        <div class="boxes-third">
        
        	<div class="boxes-padding">
            
            	<div class="bti">
                    <div class="featured-images"><img src="images/cleansleek-icon.png" width="66" height="53" alt="Responsive"></div>
                    <div class="featured-titles">clean & sleek</div>
                </div>
                <div class="featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non ipsum nunc, nec sagittis tellus.</div>
            
            </div>
            
            <span class="box-arrow"></span>
        
        </div>
        <?php
        endif;
        ?>
        
        <?php
        if (vanModul('ajanloDobozJobb')):
        ?>
        <div class="boxes-third boxes-last">
        
        	<div class="boxes-padding">
            	
                <div class="bti">
                    <div class="featured-images"><img src="images/google-icon.png" width="54" height="53" alt="Responsive"></div>
                    <div class="featured-titles">cross browser</div>
                </div>
                <div class="featured-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non ipsum nunc, nec sagittis tellus.</div>
            
            </div>
            
            <span class="box-arrow"></span>
        
        </div>
        <!-- End Featured Boxes -->
        <?php
        endif;
        ?>
        
        <?php
        if (vanModul('legujabbDobozBal')):
        ?>
        
        <!-- Start Latest 3 Projects -->
		<div class="boxes-third boxes-first">
        
       	  <div class="latestthree">
            
           	<div class="title">
                Tuts Premium
                <span class="titlearrow"></span>
                </div>
                <div class="latestthreeimage"><a href="images/latest-projects/larger-images/largeimage.jpg" rel="prettyPhoto" title="Lorem ipsum dolor sit amet"><img src="images/latest-projects/project01.jpg" alt="Lorem ipsum dolor sit amet" width="305" height="132" border="0"/></a></div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non ipsum nunc
                <span class="textarrow"></span>
                </div>
            
          </div>
        
        </div>
        <?php
        endif;
        ?>
        
        <?php
        if (vanModul('legujabbDobozKozep')):
        ?>
		<div class="boxes-third">
        
       	  <div class="latestthree">
            
           	<div class="title">
                Themeforest
                <span class="titlearrow"></span>
                </div>
                <div class="latestthreeimage"><a href="images/latest-projects/larger-images/largeimage.jpg" rel="prettyPhoto" title="Lorem ipsum dolor sit amet"><img src="images/latest-projects/project02.jpg" alt="Lorem ipsum dolor sit amet" width="305" height="132" border="0"/></a></div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non ipsum nunc
                <span class="textarrow"></span>
                </div>
            
          </div>
        
        </div>
        <?php
        endif;
        ?>
        
        <?php
        if (vanModul('legujabbDobozJobb')):
        ?>
        <div class="boxes-third boxes-last">
        
       	  <div class="latestthree">
            
           	<div class="title">
                Graphicriver
                <span class="titlearrow"></span>
                </div>
                <div class="latestthreeimage"><a href="images/latest-projects/larger-images/largeimage.jpg" rel="prettyPhoto" title="Lorem ipsum dolor sit amet"><img src="images/latest-projects/project03.jpg" alt="Lorem ipsum dolor sit amet" width="305" height="132" border="0"/></a></div>
            <div class="text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas non ipsum nunc
                <span class="textarrow"></span>
                </div>
            
          </div>
        
        </div>
        <?php
        endif;
        ?>
        
        <!-- End Latest 3 Projects -->
    
    </div>
    <!-- End Main Body Wrap -->
    <div id="main-wrap">
        
        <!-- Start Latest Project titles -->
        
                
                
    
    
        {* uzenetbox *}
        {* pageTop *}
        
        <?php
        if (vanModul('jobbSav')):
        ?>
        <div class="leftsection">
            
        <?php
        endif;
        ?>
        
        