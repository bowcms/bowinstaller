<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <meta name="author" content="Palanta.hu">

    
    <!-- Bootstrap core CSS -->
    <link href="{* stilusutvonal *}css/bootstrap.css" rel="stylesheet">

    <!-- Add custom CSS here -->
    <link href="{* stilusutvonal *}css/business-casual.css" rel="stylesheet">
</head>

<body>

    <div class="brand">Palánta.hu</div>
    <div class="address-bar">Palánta Általános iskola 2085 Pilisvörösvár, Fő út 134<br />Tel/Fax: +3626374457</div>
    <nav class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Palánta Iskola</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav">
                    {* respMenu(Főmenü) *}
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    
        <?php
        if (vanModul('slider')):
        ?>
    <div class="container">
        <div class="row">
            <div class="box">
                <div class="col-lg-12 text-center">
                    <div id="carousel-example-generic" class="carousel slide">
                        <!-- Indicators -->
                        <ol class="carousel-indicators hidden-xs">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            {* slider *}
                            
                        </div>

                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="icon-next"></span>
                        </a>
                    </div>
                    <h2>
                        <small>Welcome to</small>
                    </h2>
                    <h1>
                        <span class="brand-name">Business Casual</span>
                    </h1>
                    <hr class="tagline-divider">
                    <h2>
                        <small>By <strong>Start Bootstrap</strong></small>
                    </h2>
                </div>
                </div>
            </div>
        </div>
        <?php
        endif;
        ?>
    <div class="container">
        <div class="row">
            <div class="box">
                <?php
                $tartalomCol = 12;
                if (vanModul('tartalomBalSav')) {
                    $tartalomCol -= 3;
                    ?>
                <div class="col-lg-3">
                    {* tartalomBalSav *}
                </div>
                    <?php
                }
                if (vanModul('jobbSav')) {
                    $tartalomCol -= 3;
                }
                ?>
                <div class="col-lg-<?= $tartalomCol; ?>">
                    [TARTALOM]    
                
                </div>
                <?php
                if (vanModul('jobbSav')) {
                    ?>
                <div class="col-lg-3">
                    {* jobbSav *}
                </div>
                    <?php
                }
                ?>
            </div>
        </div>

       
    </div>
    <!-- /.container -->

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <p>Palánta.hu <?= date('Y');?></p>
                </div>
            </div>
        </div>
    </footer>

    <!-- JavaScript -->
    <script src="{* stilusutvonal *}js/jquery-1.10.2.js"></script>
    <script src="{* stilusutvonal *}js/bootstrap.js"></script>
    <script>
    // Activates the Carousel
    $('.carousel').carousel({
        interval: 5000
    })
    </script>
</body>

</html>
