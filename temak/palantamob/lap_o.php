
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{* meta_title *}</title>
    <meta name="keywords" content="{* meta_keywords*}" />
    <meta name="description" content="{* meta_description *}" />
    <meta name="language" content="hu" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{* stilusutvonal *}img/bstorelogo.png" /> 
    <link rel="bookmark icon" href="{* stilusutvonal *}img/bstorelogo.png" /> 

    <!-- Bootstrap core CSS -->
    <link href="{* stilusutvonal *}css/bootstrap.min.css" rel="stylesheet">


    <!-- Add custom CSS here -->
    <link href="{* stilusutvonal *}css/modern-business.css" rel="stylesheet">
    <link href="{* stilusutvonal *}font-awesome/css/font-awesome.min.css" rel="stylesheet">
    
    <script src="{* stilusutvonal *}js/jquery-1.10.2.js"></script>
    <script src="{* stilusutvonal *}js/bow.js?r=<?= rand(1000,9999);?>"></script>
    
</head>

<body>
    
    

        <div class="container headercontainer">

            <div class="row">
                <div class="col-lg-9">
                    <img src="{* stilusutvonal *}img/bstorelogo.jpg" />
                </div>
                
                    <div class="col-lg-3" >
                    {* miniKosar *}
                    </div>
                
            </div>
            
            <div class="row">
                <nav class="navbar navbar-bg " role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!-- You'll want to use a responsive image option so this logo looks good on devices - I recommend using something like retina.js (do a quick Google search for it and you'll find it) -->
                <a class="navbar-brand" href="<?= BASE_URL; ?>">B-STORE KFT.</a>
                     
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav navbar-right">
                    {* respMenu(Főmenü) *}
                    
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
            </div>
        </div>
    
    <div class="section">

        <div class="container">
    <div class="row utvonal">
                {* navUtvonal *}
    </div>
    </div>
    </div>
    <?php
    if (vanModul('slider')):
    ?>
    <div id="myCarousel" class="carousel slide">
        
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            {* slider *}
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </div>
    <?php
    endif;
    ?>
    
    
    <div class="section">

        <div class="container">

            <div class="row">
                
                
                
                
                <?php
                $tartalomCol = 12;
                if (vanModul('tartalomBalSav')){
                    $tartalomCol = 8;
                    // bal sidebaros elrendezés
                ?>
                
                <div class="col-lg-4 ">
                {* tartalomBalSav *}
                </div>
                
                <?php
                };
                ?>
                
                <div class="col-lg-<?= $tartalomCol; ?> ">
                [TARTALOM]
                </div>
                
                
                
                
                
            </div>
        </div>
    </div>
    
    
    

    

    
    
    <!-- JavaScript -->
    <script src="{* stilusutvonal *}js/bootstrap.js"></script>
    <script src="{* stilusutvonal *}js/modern-business.js"></script>
    <script src="{* stilusutvonal *}js/bow.js"></script>
</body>

</html>
