<?php
define('CSAK_KERET', 1);
$base = (dirname(dirname(dirname(dirname(dirname(dirname( __FILE__ ))))))) .
    '/';
include ($base . 'ajaxkeret.php');

if (isset($_GET['modositas'])) {
    if ($_GET['torles']==1) {
        $sql = "SELECT * FROM bos_termekkepek WHERE id = ".(int)$_GET['modositas'];
        $kep = sqluniv3($sql);
        unlink(BASE.'feltoltesek/termekek/'.$kep['termek_id'].'/thumbnail/'.$kep['file']);
        unlink(BASE.'feltoltesek/termekek/'.$kep['termek_id'].'/'.$kep['file']);
        
        sqltorles('bos_termekkepek',(int)$_GET['modositas'] );
    } else {
        $a = $_POST['a'];
    
        $a = $_POST['a'];
        $a['id'] = (int)$_GET['modositas'];
        
        sqladat($a, 'bos_termekkepek');
    }
    
    
}

$id = (int)$_GET['termekId'];

include ($base . 'osztaly/osztaly_kepek.php');

fileEllenorzes($id);

$sql = "SELECT * FROM bos_termekkepek WHERE termek_id = $id ORDER BY sorrend ASC ";
$talalat = sqluniv4($sql);

if (empty($talalat)) {
    ?><strong>Még nincs galéria hozzárendelve ehhez a cikkhez</strong><?php
} else {
    foreach ($talalat as $sor) {
        kepszerkeszto($sor);
    }
}

function kepszerkeszto($kep) {
    
    $filePath = BASE.'feltoltesek/termekek/'.$kep['termek_id'].'/thumbnail/'.$kep['file'];
    if (!file_exists($filePath)) {
        $sql = "DELETE FROM bos_termekkepek WHERE id = ".$kep['id'];
        sqluniv($sql);
        return;
    }
    
    $url = BASE_URL.'feltoltesek/termekek/'.$kep['termek_id'].'/thumbnail/'.$kep['file'];
    $shortkod = '[kepgal igazit="jobb|bal|kozep" kepid="'.$kep['id'].'" galid="'.$kep['termek_id'].'" galerianyit="igen|nem"]';
    ?>
    <div class="galSzerkeszto">
        <img height="80" src="<?= $url; ?>" />
        <b><?= $kep['file']; ?></b><br />
        <form method="post" onsubmit="return: false" id="galSzerkForm_<?= $kep['id']; ?>" >
            <textarea style="height: 30px;float: none;" id="leiras_<?= $kep['id']; ?>" name="a[leiras]"><?= $kep['leiras']; ?></textarea><br />
            Sorrend: <input name="a[sorrend]" id="sorrend_<?= $kep['id']; ?>" value="<?= $kep['sorrend']; ?>" />
            <br />
            <a href="javascript:void(0);" onclick="termekkepSzerkesztoMentes(<?= $kep['id']; ?>, this, 0);return false;" class="galGomb">Módosítás</a>
            <a href="javascript:void(0);" onclick="termekkepSzerkesztoMentes(<?= $kep['id']; ?>, this, 1);return false;" class="galGomb">Törlés</a>
            
            <a href="javascript:void(0);" onclick="$('#fokepfeluliras').val('<?= $kep['file'];?>');alert('A főkép a cikk mentése után lesz beállítva.');return false;" class="galGomb">Legyen ez a főkép</a><br /><br />
            
        </form>
    </div>
    <?php
}
function fileEllenorzes($id)
{
    $dir = BASE."feltoltesek/termekek/$id/";
    $kitMinta = '/\.(gif|jpe?g|png)$/i';
    
    // Open a known directory, and proceed to read its contents
    if (is_dir($dir)) {
        if ($dh = opendir($dir)) {
            while (($file = readdir($dh)) !== false) {
                if (filetype($dir . $file) == 'file') {
                    if(preg_match($kitMinta, $file, $talalat)) {
                        // kép
                        $sql = "SELECT * FROM bos_termekkepek WHERE termek_id = $id AND file = '$file' ";
                        $talalat = sqluniv3($sql);
                        if (empty($talalat)) {
                            atalakitas($id, $file);
                        } else {
                            
                        }
                    }
                    
                }
                
            }
            closedir($dh);
        }
    }

}

function atalakitas($id, $file) {
    print $file.'<br />';
    $extArr = explode('.', $file);
    $ext = end($extArr);
    $fileName = str_replace('.'.$ext, '', $file);
    
    $dir = BASE."feltoltesek/termekek/$id/";
    $image = new Image($dir.$file);
    if ($image->getHeight() > 600 or $image->getWidth() > 800) {
        $image->resize(800,600,'fit');
    } else {
        $image->resize($image->getWidth(),$image->getHeight(),'fit');
    }
    
    $image->save($fileName, $dir);
    
    $image->resize(300,220, 'fit');
    $image->save($fileName, $dir.'thumbnail/');
    
    $ujNev = md5(base64_encode($id.'_'.$fileName)).'.'.$ext;
    rename($dir.$file, $dir.$ujNev);
    rename($dir.'thumbnail/'.$file, $dir.'thumbnail/'.$ujNev);
    
    $kepAdat = array(
    'termek_id' => $id,
    'file' => $ujNev,
    'leiras' => $file
    );
    sqladat ($kepAdat, 'bos_termekkepek');
    
    
}