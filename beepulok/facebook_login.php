<?php
 // Beépülő kódja ide

$html = Html::peldany();

$html->helyorzoFelvetelStart();

?>

<div id="fb-root"></div>
<script>
 window.fbAsyncInit = function() {
  FB.init({ appId: '<?= FBCONNECT_APPID;?>',
	    status: true,
	    cookie: true,
	    xfbml: true,
	    oauth: true});

  
  // run once with current status and whenever the status changes
  //FB.getLoginStatus(updateButton);
  //FB.Event.subscribe('auth.statusChange', updateButton);
};
function updateButton(response) {
    var button = $('.fb-auth');

    if (response.authResponse) {
      //user is already logged in and connected
      var userInfo = document.getElementById('user-info');
      var access_token =   FB.getAuthResponse()['accessToken']; //mindig frissítsük bejelentkezésnél

      FB.api('/me', function(response) {
         
         ajaxBeleptet(response, access_token)

      });




    } else {
      //user is not connected to your app or logged out
      //alert('Nem lép be 2');      
    }
  }

(function() {
  var e = document.createElement('script'); e.async = true;
  e.src = document.location.protocol
    + '//connect.facebook.net/en_US/all.js';
  document.getElementById('fb-root').appendChild(e);
}());
function fbLogout() {
	FB.logout(function(response) {
          $('.login_ok').hide();
          $('.login').fadeIn();
          	});

}

function dump(arr,level) {
	var dumped_text = "";
	if(!level) level = 0;
	
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	
	if(typeof(arr) == 'object') { //Array/Hashes/Objects 
		for(var item in arr) {
			var value = arr[item];
			
			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}

function fbLogin() {
    
	FB.login(function(response) {
			
            if (response.authResponse) {
				FB.api('/me', function(response) {
					
                	
                    //$('.login').hide(0);
                    //$('.login_ok').show();
                    //$('.user_image').html('<img src="https://graph.facebook.com/' + response.id + '/picture" />');
                    //$('.user_name').html(response.name);
                    
					FB.getLoginStatus(updateButton);
                    FB.Event.subscribe('auth.statusChange', updateButton);
                });
          } else {
            //user cancelled login or did not grant authorization
            
          }
        }, {scope:'email,offline_access'});
}

function ajaxBeleptet(response, access_token) {
      //  alert(dump(response));
      var str='fbuid='+response.id;
      str += '&first_name='+response.first_name;
      str += '&last_name='+response.last_name;
      str += '&link='+response.link;
      str += '&email='+response.email;
      str += '&gender='+response.gender;
      str += '&access_token='+access_token;
      
      <?php
      // ha már be van lépve, akkor ezzel kapcsoljuk össze az accountját
      $tag = Tagok::peldany();
      if ($tag->id  > 0):
      ?>
      window.location.href = '<?= BASE_URL; ?>facebook_osszekapcsolas?'+str;
      <?php
      endif;
      ?>
      <?php
      // ha már be van lépve, akkor ezzel kapcsoljuk össze az accountját
      $tag = Tagok::peldany();
      if ($tag->id  == 0):
      ?>
      $.get("<?= BASE_URL?>login.php?"+str, function(e) {
        if (e == '1') {
            window.location.href = '<?= BASE_URL; ?>regisztracio?'+str;
        }
        if (e == '2') {
            window.location.href = '<?= BASE_URL; ?>';
        }
        
      })
      
      <?php
      endif;
      ?>
      
     
      

}

</script>
<?php

$html->helyorzoFelvetelStop('body_utan');