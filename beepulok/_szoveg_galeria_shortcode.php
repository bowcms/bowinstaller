<?php
 // Beépülő kódja ide
if (defined('UNICORN')) return ;
global $tartalom;
$memo = Memoria::olvas('colorbox');
$html = Html::peldany();

$url = Url::$peldany;
if ($url->admin) return;
$cikkIdArr = array();
if (!$memo) {
    Memoria::ir('colorbox', true);
    
    $html->headerStart();
    
    $galeriaId = rand(1000,9999);        
    ?>
    <link rel="stylesheet" href="<?= BASE_URL; ?>beepulok/_szoveg_galeria/colorbox/colorbox.css" />
		<script src="<?= BASE_URL; ?>beepulok/_szoveg_galeria/colorbox/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				//$('.group4').colorbox({rel:'group4', slideshow:true, width:"75%", height:"75%"});
				$('.galeriaMegnyitas').click(function() {
				    id = this.rel;
                    
				    $("a[rel='group_"+id+"']").colorbox({open:true,slideshow:true, width:"75%", height:"75%"});
				  
				});
				
			});
            function galeriaMegnyitas(id) {
                
                $("a[rel='group_"+id+"']").colorbox({open:true,slideshow:true, width:"75%", height:"75%"});
            }
		</script>
    <?php
    $html->headerStop();
}

$kodok= shortcodeMintaAdatok('galeriakep', $tartalom);

if (!empty($kodok)) {
    foreach($kodok as $kod) {
        $sql = "SELECT * FROM beepulo_cikkgaleria WHERE id = ".(int)$kod['attr']['id'];
        $kep = sqluniv3($sql);
        if (empty($kep)) continue;
        $cikkId = $kep['cikkek_id'];
        $cikkIdArr[$cikkId] = 1;
        $img = '<img src="'.BASE_URL.'feltoltesek/cikkgaleria/'.$kep['cikkek_id'].'/thumbnail/'.$kep['file'].'" />';
        if ($kod['attr']['igazit'] == 'jobb') {
            $out = '<div class="galeriakep" style="float:right" >';   
        } elseif ($kod['attr']['igazit'] == 'bal') {
            $out = '<div class="galeriakep" style="float:left" >';   
        } else {
            $out = '<center><div class="galeriakep"  >';
        }
        
        $out .= $img;
        $out .= '<br />'.$kep['leiras'];
        
        if ((int)$kod['attr']['galnyit']==1) {
            $out .= '<br /><a onclick="galeriaMegnyitas('.$cikkId.');" rel="'.$cikkId.'" href="javascript:void(0);" target="_balank">Galéria megnyitásához kattints ide!</a>';
            
        }
        
        if ($kod['attr']['igazit'] == 'jobb') {
            $out .= '</div >';   
        } elseif ($kod['attr']['igazit'] == 'bal') {
            $out .= '</div >';   
        } else {
            $out .= '</div></center>';
        }
        
        $tartalom = str_replace($kod['kod'], $out, $tartalom);
        
    }
    
    ob_start();
    print '<div style="diplay:none">';
    
    if (!empty($cikkIdArr)) {
        foreach($cikkIdArr as $cikkId => $v) {
            $sql = "SELECT * FROM beepulo_cikkgaleria WHERE cikkek_id = ".(int)$cikkId." ORDER by sorrend ASC";
            $kepek = sqluniv4($sql);
            if (empty($kepek)) continue;
            if ($kep['cikkek_id']=='') continue;
            foreach ($kepek as $kep) {
                if (is_file(BASE.'feltoltesek/cikkgaleria/'.$kep['cikkek_id'].'/'.$kep['file']))
                    print '<a class="galreiaNyitas group4" rel="group_'.$cikkId.'" href="'.BASE_URL.'feltoltesek/cikkgaleria/'.$kep['cikkek_id'].'/'.$kep['file'].'" title="'.$kep['leiras'].'" target="_balank"> </a>'; 
        
            }
            
        }
        
    }
    print '</div>';
    $tartalom .= ob_get_contents();
    
    ob_end_clean();
    
}

