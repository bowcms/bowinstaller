<?php

/**
 * 
 * BowCMS 2.0
 * 
 * Ceglédi Iván
 * 2013
 */
 
session_start();

// UTF-8 feljéc elküldése

header("Content-type: text/html; charset=utf-8");

// kataktzervédelem bekapcs

//set_magic_quotes_runtime(false);


// Alap elérsi útvonal

define ('BASE', dirname(__FILE__).'/');
include(BASE.'kozos_fuggvenyek.php');
magic_off();
// Globális datastore osztály betöltése

include(BASE.'osztaly/osztaly_memoria.php');
include BASE.'osztaly/osztaly_munkamenet.php';
include BASE.'osztaly/osztaly_beepulok.php';
include(BASE.'osztaly/osztaly_beallitas.php');
Beallitas::beallitasBetolt('adatbazis');
include_once BASE.'osztaly/sqlfuggvenyek.php';

// megvizsgáljuk, hogy telepítve van-e a rendszer
include(BASE.'osztaly/osztaly_telepito.php');

Telepito::vizsgalat();

if (TELEPITES) exit;
Beallitas::beallitasSqlBetolt('Rendszer');
Beallitas::beallitasSqlBetolt('Weboldal általános beállítások');
Beallitas::beallitasSqlBetolt('E-mail bellítások');
Beallitas::beallitasSqlBetolt('Facebook');


if (!isset($_SESSION['__nyelv'])) {
    
    $_SESSION['__nyelv'] = ALAP_NYELV;
}
if (isset($_REQUEST['nyelvbeallit'])) {
    $_SESSION['__nyelv'] = $_REQUEST['nyelvbeallit'];
}


modulElotolto();
Beepulok::autoLoad();

Beepulok::futtat('Adatbázis konfigurációk betöltése');

// modulok és beépülők registrálása



// Alap beállítások betöltése


// Hibajelzés kapcsolása



// Tovbbi konstansok betöltése



ini_set('DISPLAY_ERRORS', PHP_HIBA);

include(BASE.'osztaly/osztaly_hibakezelo.php');

include BASE.'osztaly/osztaly_tagok.php';

include BASE.'osztaly/osztaly_levelezo.php';

Beepulok::futtat('Rendszer indítás');

$session = Munkamenet::getIstance();

$tag = Tagok::peldany();
// nyelv beállítás
Beepulok::futtat('Tagok létrehozása');




include BASE.'osztaly/osztaly_url.php';
$url = Url::peldany();
Beepulok::futtat('URL létrehozása');

// offline mód?

$offLine = bowMeta('weboldal','offline');
if (!defined('UNICORN')) // ha admin, akkor hagyjuk
if ($offLine) {
    $offLine = $offLine[0]['ertek'];
    if ($offLine==1) {
        include (BASE.'temak/'.STILUS.'/index'.$_SESSION['__nyelv'].'.html');
        exit;
    }    
}


include BASE.'osztaly/osztaly_html.php';
$html = Html::peldany();
Beepulok::futtat('HTML osztály létrehozása');
include BASE.'osztaly/osztaly_bow.php';
$bow = Bow::peldany();
$bow->start();
Beepulok::futtat('Rendszerfutás vége');





