<?php
/**
 * Szöveg modul beállító
 * 
 * bow_oldalxmodul paraméter: a bow_szovegek tábla id, mást nem mentünk.
 * 
 * amit kapunk:
 * 
 * $oldalxmodulId: a modul és az oldalt össze kapcsoló tábla sorának id-je (ha nem újat viszünk fel, értéke > 0)
 * $oldalId: annak az oldalnak az id-je ahova a beállításokat és a modult mentjük
 * $modulId: ennek a modulnak az ID-je a bow_modulok táblában
 * $adat: a kapcsolt táblába mentett sor tartalma associatív tömbben
 * 
 * 
 * amit beállíthatunk: 
 * 
 * $sikeresBeallitas true ra ha megvagyunk a mentéssel, így visszakapjuk az adminban a modullistát
 * 
 * amit használhatunk:
 * 
 * _oldalak::parameterFelvitel($oldalId, $modulId, 'Bejegyzéscím', 'mentett adatok (szám szöveg, serializált tömb is lehet)', $oldalxmodulId);
 * 
 * 
 */
if (isset($_POST['epito_id'])) {
    
    _oldalak::parameterFelvitel($oldalId, $modulId, 'Weblap elem - '.$_POST['epito_id'], $_POST['epito_id'], $_POST['oldalxmodul_id']);
    // ezzel jelezzük, hogy kész vagyunk
    $sikeresBeallitas = true;
    $html = Html::peldany();
    $html->uzenetKiiratas('Sikeresen mentettem a szöveget!');
} else {

    
    $epito = epitoElemek();
    
    if (isset($adat['adat'])) {
        $id = (int)$adat['adat'];
    } else {
        $id = '';
    }

    
?>
<form method="post" action="<?= EPITO_URL; ?>" class="jNice">
    
    
    <p>
        <label>Válassz a korábban létrehozott építőkövekből:<br /><i>Építőkövek létrehozhatóak a modulbeállításoknál</i></label>
        <select name="epito_id">
            <?php
            foreach ($epito as $sor):
            ?>
            <option value="<?= $sor?>" <?= (($id==$sor)?' selected="selected" ':'')?> ><?= $sor; ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    <p>
        <label> </label>
        <input type="submit" value="Kész vagyok" />
    </p>
    <!-- a köv 2 paramáter megadása fontos, ez vezérli a tartalom hozzáfűzést -->
    <input type="hidden" name="hozzaad" value="<?= $_POST['hozzaad'] ?>" />
    <input type="hidden" name="oldalxmodul_id" value="<?= $oldalxmodulId; ?>" />
    
    
    
</form>

<?php
}
