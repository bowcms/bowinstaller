<?php
/**
 * Title: My account
 * Author: bowCms
 * Web: http://example.com
 * Description: Account settings for users and admin
 * 
 **/

adminMenu(__f('Felhasználók'), 'unicornOldalMenu',__f('Felhasználók'), 'acoountEditorUnicorn', 'icon-user');
adminMenu(__f('Regisztrációs mezők'), 'unicornOldalMenu',__f('Felhasználók'), 'formEditorUnicorn', 'icon-info-sign');

Beepulok::regisztralKomponens('regForm',__f('Fiók oldal'),'fiokom');
modulBejegyzes('User wall and documents', 'userWall', false);

function userWall($a) {
    $m = modulOsztalyTolto('fiokbeallitasok');
    $m->fal($a);
}

function acoountEditorUnicorn($feladat = '', $id = '') {
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Felhasználók'));
    include (dirname(__FILE__).'/accountEditor.php');
}

function formEditorUnicorn($feladat = '', $id = '') {
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Form elemek'));
    include (dirname(__FILE__).'/formEditor.php');
}

function regForm($feladat = '', $id = '') {
    $o = modulOsztalyTolto('fiokbeallitasok');
    $o->beallitas();
}
            
?>