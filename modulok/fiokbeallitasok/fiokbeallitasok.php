<?php

class Fiokbeallitasok
{
    
    public function fal($a) {
        $url = Url::$peldany;
        $tag = Tagok::peldany();
        $html = Html::$peldany;
        $csoport = $tag->adat['csoport_id'];
        
        $csoportAdat = bowMeta('csoport_uzenofal', $tag->csoportNev);
        
        if (!$csoportAdat) {
            bowMetaFelvitel('csoport_uzenofal',$tag->csoportNev,1,0);
        }
        $csoportAdat = bowMeta('csoport_uzenofal', $tag->csoportNev);
        $csoportId = $csoportAdat[0]['id'];
        if (!isset($csoportAdat[0]['datum'])) {
            $sql = "ALTER TABLE  `bow_meta` ADD  `datum` DATETIME NOT NULL ";
            sqluniv($sql);
        }
        
        if (isset($_POST['wall'])) {
            $_POST['wall']['datum'] = date('Y-m-d H:i');
            $wall = serialize($_POST['wall']);
            $uzenetId = bowMetaFelvitel('csoport_nev', 'uzenet', $wall,$csoportAdat[0]['id'], date('Y-m-d H:i'));
            ?>
            <div class="alert alert-success"><?= __f('Üzenet metése sikeres!'); ?></div>
            <?php
        }
        if (isset($_FILES['csatol'])) {
            $dir = BASE.'feltoltesek/fal/';
            if (!is_dir($dir)) mkdir($dir);
            
            foreach ($_FILES['csatol']['name'] as $k => $v) {
                $ujnev = $_FILES['csatol']['name'][$k];
                $regiNev = $ujnev;
                $exp = kiterjesztes($ujnev);
                $ujnev = md5($ujnev);
                $ujnev .= '.'.$exp;
               
                @move_uploaded_file($_FILES['csatol']['tmp_name'][$k], $dir.$ujnev); 
                if (is_file($dir.$ujnev)){
                    
                    bowMetaFelvitel('csoport_nev', 'csatolmany', $ujnev,$uzenetId, date('Y-m-d H:i'), $regiNev);
                }
                
            }
        }
        
        
        
        
        include(modulHtmlElem('fiokbeallitasok','uzenofal'));
        
        
        $html->bodyVegeStart();
        ?>
        <script type="text/javascript">
        $('#wallTab a').click(function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
        </script>
        
        
        
        
        
        <?php
        $html->bodyVegeStop();
        
    }
    
    public function wgFiok($cim) {
        $tag = Tagok::peldany();
        
        if ($tag->id == 0) {
            include(modulHtmlElem('fiokbeallitasok','belepes'));
        } else {
            include(modulHtmlElem('fiokbeallitasok','fiok'));
        }
    }
    
    public function beallitas()
    {
        $tag = Tagok::peldany();
        
        if ($tag->id==0) {
            $this->regisztracio();
            return;
        }
        
        $sql = "SELECT * FROM egyedi_tagok WHERE tag_id = " . $tag->id;
        $adatok = sqluniv3($sql);
        if (isset($_POST['feliratkozas'])) {
            sqltorles('bow_feliratkozas', 'tag_id', $tag->id);
            foreach ($_POST['feliratkozas'] as $cimkeId) {
                $a['cimke_id'] = $cimkeId;
                $a['tag_id'] = $tag->id;
                sqladat($a, 'bow_feliratkozas');
                
            }
            $html = Html::peldany();
            $html->uzenetKiiratas('Feliratkozásaidat rögzítettük');
        }
        if (isset($_POST['editform'])) {
            
            $hiba = array();
            $html = Html::peldany();
            
            $hiba['email'] = '';
            $hiba['nev'] = '';
            $hiba['jelszo'] = '';
            $hiba['ellenorzo'] = '';
            $hibak = false;

            $a = $_POST['a'];
            if (strlen(trim($a['nev'])) < 2) {
                $hiba['nev'] = 'Név nem megfelelő';
                $hibak = true;
            }
           
            if ($a['jelszo']!='')
            if (strlen(trim($a['jelszo'])) < 6) {
                $hiba['jelszo'] = 'Jelszó túl rövid';
                $hibak = true;
            }
            
            if ($a['jelszo']!='') {
                $jsz = $a['jelszo'];
                $a['jelszo'] = base64_encode(md5($a['jelszo']));
                $u = getRows('bow_tagok', $a['id']);
                
                $level = Levelezo::peldany();
                $level->sablon('palanta_jelszovaltoz');
                $level->helyettesitoHozzaad('nev', $a['nev']);
                $level->helyettesitoHozzaad('jelszo', $jsz);
                $level->kuldes($u['email']);
                
                $html->uzenetKiiratas('Jelszó változtatása sikeres!');
                
            }
            sqladat($a, 'bow_tagok');
            
            $html->uzenetKiiratas('Sikeres módosítás');
            
        }
        if (isset($_POST['kerelem'])) {
            $f = array(
                'kerelem' => $_POST['kerelem'],
                'tag_id' => $tag->id
                );
            sqlfelvitel($f, 'egyedi_tagok');
            
            if ($_POST['kerelem'] == 1) {
                if (isset($_POST['gy'])) {
                    foreach ($_POST['gy'] as $k => $gyerek) {
                        if (trim($gyerek)=='') continue;
                        $sql = "INSERT INTO palanta_gyerekek SET tag_id = ".$tag->id.",
                                    nev = '$gyerek',
                                    iskolakezdes = ".$_POST['kezdes'][$k]."
                        ";
                        sqluniv($sql);
                    }
                }
            }
            $this->komp->elsoAdatKoszonet();
            return;
        }
        
        
            $this->komp->fej($tag);
            $this->komp->adatszerkeszto();
        
    }

    public function regisztracio()
    {
        $hiba = array();
        if (!isset($_POST['a'])) {
            $a = array();

            $a['email'] = '';
            $a['nev'] = '';

            if (isset($_GET['fbuid'])) {
                $a['fbuid'] = $_GET['fbuid'];
                $a['email'] = ($_GET['email'] == 'undefined') ? '' : $_GET['email'];
                $a['nev'] = $_GET['last_name'] . ' ' . $_GET['first_name'];
                $a['nem'] = $_GET['gender'];
            }
            $hiba = array();
            $hiba['email'] = '';
            $hiba['nev'] = '';
            $hiba['jelszo'] = '';
            $hiba['ellenorzo'] = '';
            $this->komp->regisztracioAdatlap($a, $hiba);
        } else {
            $hiba['email'] = '';
            $hiba['nev'] = '';
            $hiba['jelszo'] = '';
            $hiba['ellenorzo'] = '';
            $hibak = false;

            $a = $_POST['a'];
            if (strlen(trim($a['nev'])) < 2) {
                $hiba['nev'] = 'Név nem megfelelő';
                $hibak = true;
            }
            if (strlen(trim($a['email'])) < 2) {
                $hiba['email'] = 'Email cím nem megfelelő';
                $hibak = true;
            }
            if (!isEmail($a['email'])) {
                $hiba['email'] = 'Nem valós email cím';
                $hibak = true;
            }
            $email = getRows('bow_tagok', "'" . $a['email'] . "'", 'email');
            if (isset($email['id'])) {
                $hiba['email'] = 'Ez az e-mail cím már használatban van!';
                $hibak = true;
            }
            if (strlen(trim($a['jelszo'])) < 6) {
                $hiba['jelszo'] = 'Jelszó túl rövid';
                $hibak = true;
            }
            if ($a['jelszo'] != $_POST['jelszo2']) {
                $hiba['jelszo'] = 'A két jelszó nem egyezik';
                $hibak = true;
            }
            if (!emberEllenorzo($_POST['ellenorzo'])) {
                $hiba['ellenorzo'] = 'Nem volt jó a korábbi eredmény!';
                $hibak = true;
            }
            if ($hibak) {
                $this->komp->regisztracioAdatlap($a, $hiba);
                return;
            }
            // minden ok, nyomassuk be a dbbe
            
            $alap = getRows('bow_csoportok', 1, 'regisztralt_alapertelmezett');
            $a['csoport_id'] = $alap['id'];
            $a['jelszo'] = base64_encode(md5($a['jelszo']));
            $a['regido'] = time();
            
            sqlfelvitel($a, 'bow_tagok');
            $tag_id = mysql_insert_id();
            $b = $_POST['b'];
            $b['tag_id'] = $tag_id;
            sqlfelvitel($b, 'egyedi_tagok');
            $tag = Tagok::peldany();
            $tag->beleptet($tag_id);
            $this->komp->sikeresRegisztacio();
        }
    }

    public function jelszovisszaallitas()
    {

        $kod = base64_decode($_GET['c']);
        $sql = "SELECT * FROM bow_tagok WHERE visszaallito_kod = '$kod'";
        $tag = sqluniv3($sql);
        if (!isset($tag['id'])) {
            $html = Html::peldany();
            $html->hibaKiiratas('Lejárt vagy nem megfelelő kérelem, nem hajtható végre!',
                'red page_title');
        } else {
            $id = $tag['id'];
            $sql = "SELECT * FROM bow_epitokocka WHERE kulcs = 'Rendszer_jelszo_ujrakero' ";
            $sablon = sqluniv3($sql);
            $hiba = false;
            $html = Html::peldany();
            if (isset($_POST['forgot_pwd'])) {
                if ($_POST['forgot_pwd'] != $_POST['forgot_pwd2']) {
                    $hiba = true;
                    $html->hibaKiiratas('A két jelszó nem egyezik!', 'red page_title');
                }
                if (strlen($_POST['forgot_pwd']) < 6) {
                    $hiba = true;
                    $html->hibaKiiratas('A jelszó túl rövid!', 'red page_title');
                }
                if (!$hiba) {
                    // nincs hiba
                    $sql = "UPDATE bow_tagok SET jelszo = '" . base64_encode(md5($_POST['forgot_pwd'])) .
                        "' , visszaallito_kod = '' WHERE id = $id";
                    sqluniv($sql);
                    $html->uzenetKiiratas('Jelszó sikeresen módosítva!', 'green page_title');
                    return;
                }
            }
            print $sablon["kod"];
        }

    }
}
