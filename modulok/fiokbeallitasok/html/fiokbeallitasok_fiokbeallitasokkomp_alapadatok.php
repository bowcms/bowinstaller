<div class="col-lg-6">
<form  method="post" action="<?= BASE_URL; ?>fiokom" id="contact_form"  class="form-vertical">
    <input type="hidden" name="a[id]" value="<?= $a['id']; ?>"/>
    <input type="hidden" name="editform" value="1"/>
    <h3>Adataid</h3>
    <div class="control-group">
        <label class="control-label"><?= __f('Név'); ?></label>
        <div class="controls">
            <input name="a[nev]" style="width: 90%;"  value="<?= $a['nev']?>" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><?= __f('E-mail'); ?></label>
        <div class="controls">
            <input name="a[email]" style="width: 90%;"  value="<?= $a['email']?>" disabled="disabled" />
        </div>
        
    </div>
    <div class="control-group">
        <label class="control-label"><?= __f('Jelszó'); ?>: <em>(hagyd üresen, ha nem módosítod)</em></label>
        <div class="controls">
            <input name="a[jelszo]" style="width: 90%;" value="" type="password"  />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><?= __f('Csoport'); ?></label>
        <div class="controls">
         <?php
            $sql = "SELECT * FROM bow_csoportok WHERE id = ".$a['csoport_id'];
            $uRs = sqluniv3($sql);
            $csoportNev = $uRs['nev'];
            print '<input  disabled="disabled" value="'.$csoportNev.'" >';
            
            
         ?>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><?= __f('Weboldal'); ?></label>
        <div class="controls">
            <input name="a[weboldal]" style="width: 90%;" value="<?= $a['weboldal']?>"  />
        </div>
    </div>
    <?php
    if ($a['csoport_id']==3):
    ?>
    
    <?php
    endif;
    ?>
    <div class="control-group">
        <div class="controls">
        <br />
            <input type="submit" value="<?= __f('Mentés'); ?>" class="btn btn-info" />
        </div>
    </div>
    
</form>
</div>