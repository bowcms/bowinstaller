<div class="col-lg-12">
<form  id="contact_form" method="post" class="form-horizontal" >
						<div class="well">
                            <h3><?= __f('Regisztráció'); ?></h3>
                        </div>
                        <div class="control-group">
							<label class="control-label"><?= __f('Neved');?>* <em style="color:red"><?= $hiba['nev']?></em></label>
                            <div class="controls">
							<input type="text" name="a[nev]" style="width:90%" value="<?= $a['nev']?>" />
                            </div>
						</div>
						<div class="control-group">
							<label class="control-label"><?= __f('Email');?>* <em style="color:red"><?= $hiba['email']?></em></label>
							<div class="controls">
                            <input type="text" name="a[email]" style="width:90%" value="<?= $a['email']?>" />
                            </div>
						</div>
						<div class="control-group" >
							<label class="control-label"><?= __f('Jelszó');?>*<i>legalább 6 karakter</i> <em style="color:red"><?= $hiba['jelszo']?></em></label>
							<div class="controls">
                            <input type="password" name="a[jelszo]" style="width:90%" />
                            </div>
						</div>
                        <div class="control-group" >
							<label class="control-label"><?= __f('Jelszó újra');?></label>
							<div class="controls">
                            <input type="password" name="jelszo2" style="width:90%" />
                            </div>
						</div>
                        
                        <div class="control-group" >
							<label class="control-label"><?= __f('Írd be az eredményt');?>: <?= emberEllenorzo(); ?> <em style="color:red"><?= $hiba['ellenorzo']?></em></label>
							<div class="controls">
                            <input type="text" name="ellenorzo"  />
                            </div>
						</div>
                        <?php
                        
                        $this->regFormExtra();
                        
                        if (!isset($a['fbuid'])) $a['fbuid']=$a['nem']='';
                        ?>
						<input type="hidden" name="a[fbuid]" value="<?= $a['fbuid']?>"/>
                        <input type="hidden" name="a[nem]" value="<?= $a['nem']?>"/>
                        <br /><br />
						<div class="control-group">
							<div class="controls">
                            <input type="submit" class="btn btn-primary" value="Regisztrálok" name="submit" id="submit" />
                            </div>
						</div>
					</form>
</div>