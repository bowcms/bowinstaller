<div class="control-group">
    <label class="control-label"><?= $sor['label']; ?></label>
    <div class="controls">
        
        <select name="b[<?= $kulcs; ?>]">
            <?php
            foreach($lista as $elem):
                $reszek = explode(':', $elem);
                $ertek = $reszek[1];
                $val = $reszek[0];
            ?>
            <option value="<?= $val; ?>" <?= (($val==$b[$kulcs])?' selected="selected" ':''); ?> ><?= $ertek; ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </div>
</div>