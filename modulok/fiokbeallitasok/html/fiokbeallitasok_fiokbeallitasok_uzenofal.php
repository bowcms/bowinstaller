<h3><?= $tag->csoportNev; ?> <?= __f('csoport üzenőfala'); ?></h3>
   


    <div class="tab-pane" id="home">
  
    <?php
    
    $uzenetek = sqlAssocArr('SELECT * FROM bow_meta WHERE meta_id = '.$csoportId.' ORDER BY datum DESC');
    if (!empty($uzenetek)) {
        foreach ($uzenetek as $uzenet) {
            $csatolmanyok = sqlAssocArr('SELECT * FROM bow_meta WHERE meta_id = '.$uzenet['id'].' AND kulcs = \'csatolmany\' ORDER BY datum DESC');
            $msg = unserialize($uzenet['ertek']);
            ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <b style="float: right;"><?= $uzenet['datum']; ?></b>
                <?= $msg['title']; ?>
                
            </div>
            <div class="panel-body"><?= $msg['msg'];?></div>
            <?php
            if (!empty($csatolmanyok)) {
                $dir = BASE_URL.'feltoltesek/fal/';
                ?>
                <div class="well">
                <strong><?= __f('Csatolmányok:');?> </strong>
                <div class="btn-group btn-group-xs">
                <?php
                $c = 'warning';
                foreach($csatolmanyok as $csatolmany) {
                    if ($c == 'warning') $c = 'danger'; else $c = 'warning'; 
                    ?>
                    <a class="btn btn-<?= $c; ?>" href="<?= $dir.$csatolmany['ertek']?>" target="_blank">
                        <?= $csatolmany['nev'];?>
                    </a>
                    &nbsp;
                    <?php
                }
                
                ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <br />
        </div>
            <?php
        }
    }
    
    ?>
    
    <h4><?= __f('Új üzenet')?></h4>
    <div class="row">
    <form method="post" enctype="multipart/form-data">
  <div class="col-lg-12">
    
    <div class="input-group " style="width: 100%;">
        <label class="label-inverse"><?= __f('Üzenet címe')?>:</label>  
      <input type="text" name="wall[title]" class="form-control" >
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-12">
    <div class="input-group" style="width: 100%;">
      <label class="label-inverse"><?= __f('Üzenet')?>:</label>  
      <textarea name="wall[msg]" class="form-control" ></textarea>
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-12">
    
    <div class="input-group" style="width: 100%;">
        <label class="label-inverse"><?= __f('Csatolmányok')?>:</label>  
      <input type="file" name="csatol[]" multiple="" class="form-control" >
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 -->
  <div class="col-lg-12">
    
    <div class="input-group" style="width: 100%;">
        <br />
      <button type="submit"  class="btn btn-info"  ><?= __f('Üzenet elküldése')?></button>
    </div><!-- /input-group -->
  </div>
</div><!-- /.row -->
    </form>
  </div>
