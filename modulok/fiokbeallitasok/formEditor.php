<?php
$base = (dirname(dirname(dirname(__file__)))) . '/';
if (!defined('BASE_URL')) {
    define('CSAK_KERET', 1);

    include ($base . 'ajaxkeret.php');
}
$valtozas = false;
if ($id == '')
    $id = 0;
else
    $id = (int)$id;

$file = dirname(__file__) . '/customUserFormAdmin.php';
$form = @unserialize(file_get_contents($file));

if ($feladat=='torol') {
    unset($form[$id]);
    $valtozas = true;
    if (!file_put_contents($file, serialize($form))) {
        $u->uzenetKiiras(__f('Űrlap mentése sikertelen: adj írási jogot ide: '), $file,
            'warning');
    } else {
        $u->uzenetKiiras(__f('Törlés sikeres!'), '');
        $valtozas = true;
    }
}
if (isset($_POST['a'])) {
    $a = $_POST['a'];
    $a['label'] = htmlspecialchars($a['label']);
    if ($id > 0) {
        $form[$id] = $a;
    } else {
        if (empty($form)) {
            $form[1] = $a;
        } else {
            $form[] = $a;
        }

    }
    if (!file_put_contents($file, serialize($form))) {
        $u->uzenetKiiras(__f('Űrlap mentése sikertelen: adj írási jogot ide: '), $file,
            'warning');
    } else {
        $u->uzenetKiiras(__f('Űrlap mentése sikeres!'), '');
        $valtozas = true;
    }
}
if ($valtozas) {
    $sql = "SHOW COLUMNS FROM egyedi_tagok";
    $rs = sqlAssocArr($sql);
    $mezok = array();
    foreach ($rs as $sor) {
        
        if ($sor['Field'] == 'id' ) {
            continue;
        }
        if ($sor['Field'] == 'tag_id' ) {
            continue;
        }
        
        $mezok[$sor['Field']] = 1;
    }

    if (!empty($form)) {
        foreach ($form as $sor) {
            if (!isset($sor['key']))
                continue;
            if (!isset($mezok[$sor['key']])) {
                switch ($sor['tipus']) {

                    case 'input':
                        if ($sor['kell'] == 4)
                            $tipus = 'INT';
                        else
                            $tipus = 'VARCHAR(255)';
                        break;
                    case 'text':
                        $tipus = 'TEXT';
                        break;
                    default:
                        $tipus = 'VARCHAR(255)';
                        break;

                }
                $sql = "ALTER TABLE  `egyedi_tagok` ADD  `" . $sor['key'] . "` $tipus NOT NULL ";

                sqluniv($sql);

            }
            unset($mezok[$sor['key']]);
        }

        if (!empty($mezok)) {
            foreach ($mezok as $k => $v) {
                $sql = "ALTER TABLE  `egyedi_tagok` DROP  `$k` ";
                
                sqluniv($sql);
            }
        }
    }
}
?>
<div class="btn-group">
    
    <button class="btn"><?= __f('Új input hozzáadása a regisztrációs formhoz'); ?></button>
									  <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
									  <ul class="dropdown-menu">
										<li><a href="<?= UNIC_URL; ?>input">Input</a></li>
										<li><a href="<?= UNIC_URL; ?>text">Text</a></li>
										<li><a href="<?= UNIC_URL; ?>select">Select</a></li>
										<li><a href="<?= UNIC_URL; ?>radio">Radio</a></li>
                                        <li><a href="<?= UNIC_URL; ?>checkbox">Checkbox</a></li>
                                        <li><a href="<?= UNIC_URL; ?>felirat"><?= __f('Felirat') ?></a></li>
									  </ul>
</div>

<?php
$submit = false;
if ($feladat == 'input'):
    $submit = true;
    if (!isset($form[$id])):
        $form[$id] = array(
            'label' => '',
            'key' => '',
            'kell' => 0);
    endif;
?>
    <div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Text input</h5>
							</div>
                            <div class="widget-content nopadding">
								<form action="<?= UNIC_URL . 'mentes/' . $id; ?>" method="post" method="get" class="form-horizontal" />
									<input name="a[tipus]" value="input" type="hidden" />
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="text" name="a[label]" value="<?= $form[$id]['label']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">Key (ascii)</label>
										<div class="controls">
											<input type="text" name="a[key]" value="<?= $form[$id]['key']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"><?= __f('Ellenőrzés') ?></label>
										<div class="controls">
											<select name="a[kell]" >
                                                <option value="0" <?= $form[$id]['kell'] ==
0 ?> ><?= __f('Nincs') ?></option>
                                                <option value="1" <?= $form[$id]['kell'] ==
1 ?> ><?= __f('Minimum 3 karakter') ?></option>
                                                <option value="2" <?= $form[$id]['kell'] ==
2 ?> >E-mail</option>
                                                <option value="3" <?= $form[$id]['kell'] ==
3 ?> ><?= __f('Weboldalcím') ?></option>
                                                <option value="4" <?= $form[$id]['kell'] ==
4 ?> ><?= __f('Egész szám') ?></option>
                                            
                                            </select>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"> </label>
										<div class="controls">
											<input type="submit" class="btn btn-info" value="<?= __f('Mentés'); ?>" />
										</div>
									</div>
                                </form>
                            </div>
                        </div>
         </div>
<?php
endif;
?>

<?php
if ($feladat == 'text'):
    $submit = true;
    if (!isset($form[$id])):
        $form[$id] = array(
            'label' => '',
            'key' => '',
            'kell' => 0);
    endif;
?>
    <div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Text</h5>
							</div>
                            <div class="widget-content nopadding">
								<form action="<?= UNIC_URL . 'mentes/' . $id; ?>" method="post" method="get" class="form-horizontal" />
									<input name="a[tipus]" value="text" type="hidden" />
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="text" name="a[label]" value="<?= $form[$id]['label']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">Key (ascii)</label>
										<div class="controls">
											<input type="text" name="a[key]" value="<?= $form[$id]['key']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"><?= __f('Kötelező szószám (0 ha nem kötelező)') ?></label>
										<div class="controls">
											<input type="text" name="a[kell]" value="<?= $form[$id]['kell']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"> </label>
										<div class="controls">
											<input type="submit" class="btn btn-info" value="<?= __f('Mentés'); ?>" />
										</div>
									</div>
                                </form>
                            </div>
                        </div>
         </div>
<?php
endif;
?>

<?php
if ($feladat == 'select'):
    $submit = true;
    if (!isset($form[$id])):
        $form[$id] = array(
            'label' => '',
            'key' => '',
            'elemek' => 'kulcs1:Szöveg1, kulcs2:Szöveg2');
    endif;
?>
    <div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Select</h5>
							</div>
                            <div class="widget-content nopadding">
								<form action="<?= UNIC_URL . 'mentes/' . $id; ?>" method="post" method="get" class="form-horizontal" />
									<input name="a[tipus]" value="select" type="hidden" />
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="text" name="a[label]" value="<?= $form[$id]['label']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">Key (ascii)</label>
										<div class="controls">
											<input type="text" name="a[key]" value="<?= $form[$id]['key']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"><?= __f('Választható elemek (vesszővel elválasztva, kulcs:érték formában)') ?></label>
										<div class="controls">
											<input type="text" name="a[elemek]" value="<?= $form[$id]['elemek']; ?>" />
                                            <span class="help-block"><?= __f('Példa: alma:Alma, korte:Körte, banan:Banán'); ?></span>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"> </label>
										<div class="controls">
											<input type="submit" class="btn btn-info" value="<?= __f('Mentés'); ?>" />
										</div>
									</div>
                                </form>
                            </div>
                        </div>
         </div>
<?php
endif;
?>
<?php
if ($feladat == 'radio'):
    $submit = true;
    if (!isset($form[$id])):
        $form[$id] = array(
            'label' => '',
            'key' => '',
            'elemek' => 'kulcs1:Szöveg1, kulcs2:Szöveg2');
    endif;
?>
    <div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Radio</h5>
							</div>
                            <div class="widget-content nopadding">
								<form action="<?= UNIC_URL . 'mentes/' . $id; ?>" method="post" method="get" class="form-horizontal" />
									<input name="a[tipus]" value="radio" type="hidden" />
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="text" name="a[label]" value="<?= $form[$id]['label']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">Key (ascii)</label>
										<div class="controls">
											<input type="text" name="a[key]" value="<?= $form[$id]['key']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"><?= __f('Választható elemek (vesszővel elválasztva, kulcs:érték formában)') ?></label>
										<div class="controls">
											<input type="text" name="a[elemek]" value="<?= $form[$id]['elemek']; ?>" />
                                            <span class="help-block"><?= __f('Példa: alma:Alma, korte:Körte, banan:Banán'); ?></span>
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"> </label>
										<div class="controls">
											<input type="submit" class="btn btn-info" value="<?= __f('Mentés'); ?>" />
										</div>
									</div>
                                </form>
                            </div>
                        </div>
         </div>
<?php
endif;
?>

<?php
if ($feladat == 'checkbox'):
    $submit = true;
    if (!isset($form[$id])):
        $form[$id] = array('label' => '', 'key' => '');
    endif;
?>
    <div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5>Checkbox</h5>
							</div>
                            <div class="widget-content nopadding">
								<form action="<?= UNIC_URL . 'mentes/' . $id; ?>" method="post" method="get" class="form-horizontal" />
									<input name="a[tipus]" value="checkbox" type="hidden" />
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="text" name="a[label]" value="<?= $form[$id]['label']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">Key (ascii)</label>
										<div class="controls">
											<input type="text" name="a[key]" value="<?= $form[$id]['key']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label"> </label>
										<div class="controls">
											<input type="submit" class="btn btn-info" value="<?= __f('Mentés'); ?>" />
										</div>
									</div>
                                </form>
                            </div>
                        </div>
         </div>
<?php
endif;
?>

<?php
if ($feladat == 'felirat'):
    $submit = true;
    if (!isset($form[$id])):
        $form[$id] = array('label' => '');
    endif;
?>
    <div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5><?= __f('Felirat') ?></h5>
							</div>
                            <div class="widget-content nopadding">
								<form action="<?= UNIC_URL . 'mentes/' . $id; ?>" method="post" method="get" class="form-horizontal" />
									<input name="a[tipus]" value="felirat" type="hidden" />
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="text" name="a[label]" value="<?= $form[$id]['label']; ?>" />
										</div>
									</div>
                                    <div class="control-group">
										<label class="control-label">Label</label>
										<div class="controls">
											<input type="submit" class="btn btn-info" value="<?= __f('Mentés'); ?>" />
										</div>
									</div>
                                    
                                </form>
                            </div>
                        </div>
         </div>
<?php
endif;
?>

<?php

if (!empty($form)):
?>
                        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5><?= __f('Mentett mezők') ?></h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th>Label</th>
											<th><?= __f('Típus'); ?></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
<?php


    foreach ($form as $k => $sor):
        if (isset($sor['tipus'])):

?>
                                    <tr>
                                        <td><?= $sor['label'] ?></td>
                                        <td><?= $sor['tipus'] ?></td>
                                        <td>
                                            <a class="btn" href="<?= UNIC_URL .
$sor['tipus'] . '/' . $k; ?>"><?= __f('Szerkeszt'); ?></a>
                                            <a class="btn" href="<?= UNIC_URL .
'torol/' . $k; ?>"><?= __f('Töröl'); ?></a>
                                        
                                        </td>
                                        
                                    </tr>
<?php
        endif;
    endforeach;
?>
                                    </tbody>
								</table>							
							</div>
						</div>
<?php
endif;
?>



                    
                        