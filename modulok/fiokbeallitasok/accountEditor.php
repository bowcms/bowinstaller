<?php


// user törlés
if (isset($_POST['a'])) {
    $a = $_POST['a'];
    if ($_POST['jelszo']!="") {
        $a['jelszo'] = base64_encode(md5($_POST['jelszo']));
    }
    sqladat($a, 'bow_tagok');
    
    if (isset($_POST['levelkuldes'])) {
        if ($_POST['levelkuldes']==1) {
            $u->komp->popUpNoti(__f('Regisztrációs levél'), __f('Levél kiküldése folyamatban.'));
            $level = Levelezo::peldany();
            $level->helyettesitoHozzaad('nev' , $a['nev']);
            $level->helyettesitoHozzaad('jelszo' , $_POST['jelszo']);
            $level->helyettesitoHozzaad('email' , $a['email']);
            $level->helyettesitoHozzaad('weburl' , '<a href="'.BASE_URL.'">'.BASE_URL.'</a>');
            
            $level->sablon('reg_by_admin_mail');
            $level->kuldes($a['email']);
            
        }
    }
    
    $u->komp->popUpNoti(__f('Felhasználó mentés'), __f('Az adatokat elmentettem.'));
}

if ($feladat == 'delete') {
    $ids = explode('_', $id);
    foreach ($ids as $id) {
        if (is_numeric($id)) {

            $sql = "DELETE FROM egyedi_tagok WHERE tag_id = $id ";
            sqluniv($sql);
            $sql = "DELETE FROM bow_tagok WHERE id = $id ";
            
            sqluniv($sql);
            


        }
    }
    $u->komp->popUpNoti(__f('Törlés sikeres'), __f('A kijelölt felhasználókat eltávolítottam!'));
    header('Location: '.UNIC_URL.'ts');
}
if ($feladat == 'ts') {
    $u->komp->popUpNoti(__f('Törlés sikeres'), __f('A kijelölt felhasználókat eltávolítottam!'));
    
}

if ($feladat == 'edit') {
    $id = (int)$id;
    $a = sqluniv3("select * from bow_tagok WHERE id = $id");
    if (!isset($a['id'])) {
        $a = array();
        $a['id'] = 0;
        $a['nev'] = '';
        $a['email'] = '';
        $a['csoport_id'] = '';
        
        
            
    }
    $u->komp->adminFormFej(__f('Felhasználó szerkesztése'), UNIC_URL);
    $u->komp->adminFormHidden('a[id]',$a['id']);
        
    $u->komp->adminFormInput(__f('Név'),'a[nev]', $a['nev']);
    $u->komp->adminFormInput('Email','a[email]', $a['email']);
    
    print '<hr />';   
    
    $u->komp->adminFormInput(__f('Jelszó'),'jelszo','', __f('Hagyd üresen, ha nem szeretnéd módosítani'));
    
    $lista = valasztoLista('bow_csoportok', 'id', 'nev', ' rang asc');
    $u->komp->adminFormSelect(__f('Felhasználói csoport'),'a[csoport_id]',$a['csoport_id'],$lista,'');
    
    if ($a['id']==0) {
        $lista = array('1' => __f('igen'), '2' => __f('nem'));
        $u->komp->adminFormRadio(__f('Küldjünk levelet a felhasználónak a regisztrációról?'),'levelkuldes',$lista,1,__f('Mentés után küldhetünk levelet a felhasználónak a regisztrációról. Levélsablon kulcsa').': reg_by_admin_mail' );
    }
    
    $u->komp->adminFormLab();
    
    return;
    }
    

$tablaOpciok = array(
        'tablacim' => __f('Szövegek'),
        'mezok' => array('id' => 'ID', 'nev' => __f('Név'),'nev' => __f('Név'),'email' => 'Email','aktivitas' => 'Utólsó aktivitás'),
        'vezerlok' => array(
            'edit' => __f('Szerkesztés'),
            'delete' => __f('Törlés')),
        'filter' => 'nev, email',
        'callback' => array('email' => 'accountEditorMailtoLink','aktivitas' => 'accountEditorDateFormat'),
        'ujElemGomb' => __f('Új felhasználó hozzáadása'));
        

    
    
    
$u->szerkesztoTabla('bow_tagok', $tablaOpciok);

function accountEditorMailtoLink($e) {
    
    return '<a href="mailto:'.$e.'">'.$e.'</a>';
}
function accountEditorDateFormat($e) {
    if ($e == 0) {
        return '';
    }
    return date('Y-m-d H:i', $e);
}