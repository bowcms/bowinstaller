<?php

class fiokbeallitasokKomp {
    
    public function elsoAdatKoszonet() {
         ?>
         <div class="green">
            Köszönjük, a kérelmet rögzítettük! Hamarosan emailben értesítünk, hogy használatba veheted csoportod
         szolgáltatásait!
         </div>
         <?php
    }
    public function fej($t) {
        ?>
        <div class="well">
            <strong>Üdvözöllek <?= $t->adat['nev'];?>!</strong><br />
            <em>Csoportod: <?= $t->csoportNev; ?></em>
        </div>
        <?php
    }
    public function adatszerkeszto() {
        $tag = Tagok::peldany();
        $a = getRows('bow_tagok', $tag->id);
        $b = getRows('egyedi_tagok', $tag->id, 'tag_id');
        
        $sql = "SELECT * FROM bow_cimkek WHERE feliratkozhat != '' ORDER BY cimke ASC";
        $rs = sqluniv4($sql);
        $rs2 = array();
        foreach ($rs as $cSor) {
            $feliratkozhat = $cSor['feliratkozhat'];
            $feliratkozhat = explode(',', $feliratkozhat);
            $feliratkozhat = array_flip($feliratkozhat);
        
            if (array_key_exists($tag->adat['csoport_id'], $feliratkozhat)) {
                $rs2[] = $cSor;
            }
        }
        include(modulHtmlElem('fiokbeallitasok', 'alapadatok'));
        if (!empty($rs2)) {
            $rs = $rs2;
        
            include(modulHtmlElem('fiokbeallitasok','feliratkozas'));
        }
        
        
    }
    
    
    public function regisztracioAdatlap($a, $hiba) {
       include (modulHtmlElem('fiokbeallitasok', 'regisztracio'));
    }
    public function regFormExtra() {
        $inp = @file_get_contents(dirname(__FILE__).'/customUserFormAdmin.php');
        $inp = @unserialize($inp);
        
        if (isset($_POST['b'])) {
            $b = $_POST['b'];
        } else {
            if (is_array($inp)) {
                foreach ($inp as $sor) {
                    if (isset($sor['key'])) $b[$sor['key']] = '';
                }
            }
        }
        foreach ($inp as $sor) {
            if (isset($sor['key'])) $kulcs = $sor['key'];
            switch ($sor['tipus']) {
                case 'felirat':
                    include(modulHtmlElem('fiokbeallitasok','f_felirat'));
                break;
                case 'text':
                    include(modulHtmlElem('fiokbeallitasok','f_text'));
                break;
                case 'select':
                    $lista = explode(',', $sor['elemek']);
                    include(modulHtmlElem('fiokbeallitasok','f_select'));
                break;
                case 'radio':
                    $lista = explode(',', $sor['elemek']);
                    include(modulHtmlElem('fiokbeallitasok','f_radio'));
                break;
                case 'checkbox':
                    include(modulHtmlElem('fiokbeallitasok','f_checkbox'));
                break;
                case 'input':
                    include(modulHtmlElem('fiokbeallitasok','f_input'));
                break;
                
            }
        }
        
    }
    public function sikeresRegisztacio() {
        ?>
        
        <h1 class="page_title red">Sikeres regisztráció</h1>
        <a class="btn btn-info" href="<?= BASE_URL; ?>">Tovább</a>
        <?php
    }
}