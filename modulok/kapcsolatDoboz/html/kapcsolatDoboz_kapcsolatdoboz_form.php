<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><?= $cim; ?></div>
  <div class="panel-body"><?= $leiras; ?>
  <?php if (isset($hiba)):?>
  <div class="alert alert-warning"><?= $hiba; ?></div>
  <?php endif; ?>
  <form method="post">
  <div class="input-group" style="width: 100%;">
    
    <input type="text" name="kapcsolatForm[nev]" class="form-control" placeholder="<?= __f('Név'); ?>" />
  </div>
  <div class="input-group" style="width: 100%;">
    <span class="input-group-addon">@</span>
    <input type="text" name="kapcsolatForm[email]" class="form-control" placeholder="<?= __f('E-mail cím'); ?>" />
  </div>
  <div class="input-group" style="width: 100%;">
    <textarea name="kapcsolatForm[uzenet]" class="form-control" placeholder="<?= __f('Üzenet'); ?>" ></textarea>
  </div>
  <div class="input-group" style="width: 100%;">
    
    <input type="text" name="ellenorzo" class="form-control" placeholder="<?= __f('Add meg az összeget:').' '.$a.' + '.$b.' ='; ?>" />
  </div>
  <br />
  <div class="input-group" style="width: 100%;text-align: center;">
    <input type="submit" class="btn" value="<?= __f('Küldés');?>" />
  </div>
  
  </form>
  
  </div>
</div>