<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Kapcsolat űrlap',
        'fuggveny' => 'wgKapcsolatDoboz',
        'leiras' => 'Egyszerű kapcsolat doboz'
);
function wgKapcsolatDoboz ($a) {
    $o = modulOsztalyTolto('kapcsolatDoboz');
    $o->wgKapcsolatDoboz($a['cim'],$a['cimzett'], $a['leiras'], $a['koszonet']);
}
function wgKapcsolatDobozSet($a) {
    if ($a=='') {
        $a = array(
            'cim' => 'Cím',
            'cimzett' => '',
            'leiras' => '',
            "koszonet" => ''
        );
    } else {
        $a = unserialize($a);
    }
    $sql = "SELECT * FROM bow_cimkek ORDER BY cimke ASC";
    $rs = sqluniv4($sql);
    
    ?>
    <p>
        <label>Dobozcím:</label>
        <input name="a[cim]" value="<?= $a['cim'];?>"/>
    </p>
    <p>
        <label>Címzett:</label>
        <input name="a[cimzett]" value="<?= $a['cimzett'];?>"/>
    </p>
    <p>
        <label>Leírás (HTML):</label>
        <textarea name="a[leiras]"><?= $a['leiras'];?></textarea>
    </p>
    <p>
        <label>Köszönet (HTML):</label>
        <textarea name="a[koszonet]"><?= $a['koszonet'];?></textarea>
    </p>
    
    
    <?php
}