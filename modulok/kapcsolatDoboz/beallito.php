<?php

$sql = "SELECT * FROM bow_kapcsolat_uzenetek ORDER BY datum DESC LIMIT 20";
$rs = sqluniv4($sql);
if (empty($rs)) {
    print '<h2>Nincs üzenet a rendszerben!</h2>';
}
foreach ($rs as $sor):
?>
<div class="kapcs_lista round15">
    <span>Email: <a href="mailto:<?= $sor['email'];?>"><?= $sor['email'];?></a><br /><?= $sor['datum'];?></span>
    
    <h1><?= $sor['nev']?></h1>
    <br /><br />
    <code><?= nl2br($sor['uzenet']);?></code>
</div>
<?php
endforeach;

$html = Html::peldany();

$html->headerStart();
?>
<style>
.kapcs_lista {
    background: #fff;
    margin: 10px;
    padding: 15px;
    border: 1px solid #aaa;
}
.kapcs_lista h1{
    font-size: 16px;
    background: none;
    display: inline;
    clear: none;
}
.kapcs_lista span {
    font-size: 14px;
    display: block;
    float: right;
    text-align: right;
}
.kapcs_lista a {
    color: #6AD56A;
}
.kapcs_lista a:hover {
    color: #31AE31;
}
.kapcs_lista code {
    font-size: 14px;
}


</style>
<?php
$html->headerStop();

sqluniv("UPDATE bow_kapcsolat_uzenetek SET megtekintve = 1 ");