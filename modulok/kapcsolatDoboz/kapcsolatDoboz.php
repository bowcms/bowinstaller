<?php

class kapcsolatDoboz
{

    function wgKapcsolatDoboz($cim, $cimzett, $leiras, $koszonet)
    {
        $uzenet = '';
        if (isset($_POST['kapcsolatForm'])) {
            if ($_SESSION['kapcsolatDoboz']['ellenorzo'] == (int)$_POST['ellenorzo'] and isEmail($_POST['kapcsolatForm']['email'])) {


                $cimzett = $cimzett;
                $feladoNev = $_POST['kapcsolatForm']['nev'];
                $feladoEmail = $_POST['kapcsolatForm']['email'];
                $kerdes = $_POST['kapcsolatForm']['uzenet'];

                $level = Levelezo::peldany();
                $level->sablon('kapcsolatform_uzenet');
                $level->helyettesitoHozzaad('felado', '<a href="mailto:' . $feladoEmail . '">' .
                    $feladoNev . ', ' . $feladoEmail . '</a>');
                $level->helyettesitoHozzaad('uzenet', $kerdes);
                $level->kuldes($cimzett);

                $a = $_POST['kapcsolatForm'];
                $a['datum'] = date('Y-m-d H:i');
                sqladat($a, 'bow_kapcsolat_uzenetek');


                include (modulHtmlElem('kapcsolatDoboz', 'sended'));
                return;
            } else {
                if (!isEmail($_POST['kapcsolatForm']['email'])) $hiba = __f('Nem megfelelő E-mail cím'); 
                else 
                    $hiba = __f('Nem megfelelő ellenőrzőkód');
            }

        }
        $a = rand(1, 9);
        $b = rand(1, 9);
        $_SESSION['kapcsolatDoboz']['ellenorzo'] = $a + $b;
        include (modulHtmlElem('kapcsolatDoboz', 'form'));
    }

    function kirak()
    {
        $uzenet = '';
        if (isset($_POST['kapcsolatForm'])) {

            $cimzett = NOTIF_EMAIL;
            $feladoNev = $_POST['kapcsolatForm']['nev'];
            $feladoEmail = $_POST['kapcsolatForm']['email'];
            $kerdes = $_POST['kapcsolatForm']['uzenet'];

            $level = Levelezo::peldany();
            $level->sablon('kapcsolatform_uzenet');
            $level->helyettesitoHozzaad('felado', '<a href="mailto:' . $feladoEmail . '">' .
                $feladoNev . ', ' . $feladoEmail . '</a>');
            $level->helyettesitoHozzaad('uzenet', $kerdes);
            $level->kuldes($cimzett);

            $a = $_POST['kapcsolatForm'];
            $a['datum'] = date('Y-m-d H:i');
            sqladat($a, 'bow_kapcsolat_uzenetek');


            $uzenet = ('Köszönjük érdeklődésedet, üzenetedet továbbítottuk!');

        }

        $this->komp->kapcsolatForm($uzenet);

    }
}
