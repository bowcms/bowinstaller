<?php
/**
 * Title: Gallery Plugin
 * Author: bowCms
 * Web: http://example.com
 * Description: Upload images and use shortcode to insert images and galleries in posts and pages
 * 
 **/

Beepulok::regisztral('postEditorFormBottom', "galleryEditor");
Beepulok::regisztralShortcode('postgallery', 'PostGalleryShortcode');

//Beepulok::regisztralKomponens('GalleryPage', __f('Cikk galéria komponens'), 'galeria');


function PostGalleryShortcode($adat)
{
    
    if (!Memoria::olvas('colorbox')) {
        PostGalleryShortcodeColorboxHozzaadas($adat);
    }
    $sql = "SELECT * FROM beepulo_cikkgaleria WHERE id = " . (int)$adat['id'];
    $kep = sqluniv3($sql);
    if (empty($kep))
        return;
    $cikkId = $kep['cikkek_id'];
    $cikkIdArr[$cikkId] = 1;
    $img = '<img src="' . BASE_URL . 'feltoltesek/cikkgaleria/' . $kep['cikkek_id'] .
        '/thumbnail/' . $kep['file'] . '" style="width:100%" />';
    if ($adat['popup']=='onlypic') {
        $img = '<img src="' . BASE_URL . 'feltoltesek/cikkgaleria/' . $kep['cikkek_id'] .
        '/' . $kep['file'] . '" style="width:100%" />';
        print $img;
        return;
    }
    if ($adat['align'] == 'right') {
        $out = '<div class="galeriakep" style="float:right" >';
    } elseif ($adat['align'] == 'left') {
        $out = '<div class="galeriakep" style="float:left" >';
    } else {
        $out = '<center><div class="galeriakep"  >';
    }

    $out .= $img;
    $out .= '<br />' . $kep['leiras'];
    
    $popUp = false;
    if ((int)$adat['popup'] == 'yes') {
        $popUp = true;
        $out .= '<br /><a onclick="galeriaMegnyitas(' . $cikkId . ');" rel="' . $cikkId .
            '" href="javascript:void(0);" target="_balank">Galéria megnyitásához kattints ide!</a>';

    }

    if ($adat['align'] == 'right') {
        $out .= '</div >';
    } elseif ($adat['align'] == 'left') {
        $out .= '</div >';
    } else {
        $out .= '</div></center>';
    }

    if ($popUp) {
        if (!Memoria::olvas('PostGalleryAdded_'.$kep['cikkek_id'])) {
            Memoria::ir('PostGalleryAdded_'.$kep['cikkek_id'], true);
            
            PostGalleryShortcodeAddGallery($kep['cikkek_id']);
        }
        
    }
    print $out;
}
function PostGalleryShortcodeAddGallery($cikkId) {
    $html = Html::peldany();
    $html->bodyVegeStart();
    $sql = "SELECT * FROM beepulo_cikkgaleria WHERE cikkek_id = ".(int)$cikkId." ORDER by sorrend ASC";
            $kepek = sqluniv4($sql);
            if (empty($kepek)) continue;
            
            foreach ($kepek as $kep) {
                if (is_file(BASE.'feltoltesek/cikkgaleria/'.$kep['cikkek_id'].'/'.$kep['file']))
                    print '<a class="galreiaNyitas group4" rel="group_'.$cikkId.'" href="'.BASE_URL.'feltoltesek/cikkgaleria/'.$kep['cikkek_id'].'/'.$kep['file'].'" title="'.$kep['leiras'].'" target="_balank"> </a>'; 
        
            }
            
    $html->bodyVegeStop();            
}

function PostGalleryShortcodeColorboxHozzaadas($adat)
{
    $html = Html::peldany();

    $url = Url::$peldany;
    if ($url->admin)
        return;

    Memoria::ir('colorbox', true);

    $html->headerStart();

    $galeriaId = rand(1000, 9999);
?>
    <link rel="stylesheet" href="<?= BASE_URL; ?>beepulok/_szoveg_galeria/colorbox/colorbox.css" />
		<script src="<?= BASE_URL; ?>beepulok/_szoveg_galeria/colorbox/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				//$('.group4').colorbox({rel:'group4', slideshow:true, width:"75%", height:"75%"});
				$('.galeriaMegnyitas').click(function() {
				    id = this.rel;
                    
				    $("a[rel='group_"+id+"']").colorbox({open:true,slideshow:true, width:"75%", height:"75%"});
				  
				});
				
			});
            function galeriaMegnyitas(id) {
                
                $("a[rel='group_"+id+"']").colorbox({open:true,slideshow:true, width:"75%", height:"75%"});
            }
		</script>
    <?php
    $html->headerStop();


}

function PostGalleryShortcodeInsert($cikkId)
{

    $sql = "SELECT * FROM beepulo_cikkgaleria WHERE cikkek_id = " . $cikkId .
        " ORDER BY sorrend ASC";
    $rs = sqluniv4($sql);
    if (empty($rs)) {
?>
        <div class="alert alert-info"><?= __f('Nem található galéria ehhez a szöveghez') ?></div>
        <?php
        return;
    } else {
        $kepBase = BASE_URL . 'feltoltesek/cikkgaleria/' . $cikkId . '/thumbnail/';
        foreach ($rs as $sor):
?>
        <div class="row-fluid">
            <div class="span4">
                <img src="<?= $kepBase . $sor['file']; ?>" height="60" />
            </div>
            <div class="span4">
                <select id="shortCodeGallery_align_<?= $sor['id']; ?>" style="width: 170px;">
                    <option value="center"><?= __f('Igazítás: középre') ?></option>
                    <option value="left"><?= __f('Igazítás: balra') ?></option>
                    <option value="right"><?= __f('Igazítás: jobbra') ?></option>
                </select><br />
                <select id="shortCodeGallery_open_<?= $sor['id']; ?>" style="width: 170px;">
                    <option value="yes"><?= __f('Galéria megnyitása kattintásra') ?></option>
                    <option value="no"><?= __f('Nem nyit galériát') ?></option>
                    <option value="onlypic"><?= __f('Csak kép') ?></option>
                </select><br />
                
            </div>
            <div class="span4" style="text-align: right;">
                <button type="button" onclick="insertShortCode('postgallery',{'id':<?= $sor['id'] ?>,'gallery':'<?= $sor['cikkek_id'] ?>','popup': $('#shortCodeGallery_open_<?= $sor['id']; ?>').val(),'align' : $('#shortCodeGallery_align_<?= $sor['id']; ?>').val()});" class="btn btn-primary"><?= __f('Beillesztés'); ?></button>
            </div>
            
        </div>
        <hr />
        <?php
        endforeach;
    }
}

function galleryEditor($sz)
{
    if ($sz['id'] == 0) {
        // new post
        print '<h4>' . __f('Mentés után hozzáadhat galériát') . '</h4>';
        return;
    }
    $admin = osztaly_unicorn_futtato::peldany();
?>
    <div class="control-group">
<div id="upload-wrapper">
<div align="center">
<h3><?= __f('Galéria plugin'); ?></h3>
<input name="FileInput" id="FileInput" type="file" /> 

<a id="ajaxLoader-submit-btn" class="label label-success" ><?= __f('Feltölt'); ?></a>
<br />vagy<br />
<input placeholder="<?= __f('Url'); ?>" name="kapUrl" id="kapUrl"/> 
<a id="ajaxLoader-url-btn" href="javascript:void(0);" onclick="$('#galleryList').load('<?= BASE_URL; ?>modulok/cikkgaleria/url_upload.php?cikkId=<?= $sz['id']; ?>&url='+$('#kapUrl').val());" class="label label-success" ><?= __f('Url feltöltése'); ?></a>

<img src="<?= BASE_URL; ?>modulok/cikkgaleria/images/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
<div id="progressbox" ><div id="progressbar"></div ><div id="statustxt">0%</div></div>
<div id="output"></div>
<div id="galleryList"></div>

</div>

</div>

    </div>
    
    <?php

    $html = Html::peldany();
    $html->headerStart();
?>
<script type="text/javascript" src="<?= BASE_URL; ?>modulok/cikkgaleria/js/jquery.form.min.js"></script>

<script type="text/javascript">
$(document).ready(function() {
    $('#bowUnicornForm').submit(function(){
        $('#galleryList').remove();
    })
     $('#galleryList').load('<?= BASE_URL; ?>modulok/cikkgaleria/lista.php?cikkId=<?= $sz['id']; ?>');
	var options = { 
			target:   '#output',   // target element(s) to be updated with server response 
			beforeSubmit:  beforeSubmit,  // pre-submit callback 
			success:       afterSuccess,  // post-submit callback 
			uploadProgress: OnProgress, //upload progress callback 
			resetForm: true ,       // reset the form after successful submit 
            uploader: '<?= BASE_URL; ?>modulok/cikkgaleria/processupload.php?cikkId=<?= $sz['id']; ?>'
		}; 
		
	 $('#ajaxLoader-submit-btn').click(function() {
	   
			$(this).parents('form').ajaxSubmit(options);  			
			// always return false to prevent standard browser submit and page navigation 
			return false; 
		}); 
		

//function after succesful file upload (when server response)
function afterSuccess()
{
        $('#output').hide();
	$('#submit-btn').show(); //hide submit button
	$('#loading-img').hide(); //hide submit button
	$('#progressbox').delay( 1000 ).fadeOut(); //hide progress bar
    $('#galleryList').load('<?= BASE_URL; ?>modulok/cikkgaleria/lista.php?cikkId=<?= $sz['id']; ?>');

}

//function to check file size before uploading.
function beforeSubmit(){
    //check whether browser fully supports all File API
    $('#output').show().html('');
   if (window.File && window.FileReader && window.FileList && window.Blob)
	{
		
		if( !$('#FileInput').val()) //check empty input filed
		{
			$("#output").html("Are you kidding me?");
			return false
		}
		
		var fsize = $('#FileInput')[0].files[0].size; //get file size
		var ftype = $('#FileInput')[0].files[0].type; // get file type
		

		//allow file types 
		switch(ftype)
        {
            case 'image/png': 
			case 'image/gif': 
			case 'image/jpeg': 
			case 'image/pjpeg':
			
                break;
            default:
                $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
				return false
        }
		
		//Allowed file size is less than 5 MB (1048576)
		if(fsize>5242880) 
		{
			$("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big file! <br />File is too big, it should be less than 5 MB.");
			return false
		}
				
		$('#submit-btn').hide(); //hide submit button
		$('#loading-img').show(); //hide submit button
		$("#output").html("");  
	}
	else
	{
		//Output error to older unsupported browsers that doesn't support HTML5 File API
		$("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
		return false;
	}
}

//progress bar function
function OnProgress(event, position, total, percentComplete)
{
    //Progress bar
	$('#progressbox').show();
    $('#progressbar').width(percentComplete + '%') //update progressbar percent complete
    $('#statustxt').html(percentComplete + '%'); //update status text
    if(percentComplete>50)
        {
            $('#statustxt').css('color','#000'); //change status text to white after 50%
        }
}

//function to format bites bit.ly/19yoIPO
function bytesToSize(bytes) {
   var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
   if (bytes == 0) return '0 Bytes';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

}); 


function galeriaFrissites() {
    $('#galleryList').html('<center>..Betöltés..</center>');
    $('#galleryList').load('<?= BASE_URL; ?>modulok/cikkgaleria/lista.php?cikkId=<?= $sz['id'] ?>');

}
function galeriaSzerkesztoMentes(id, obj, torles) {
    formom = $(obj).parent();
    leiras = $(formom).children('textarea').val();
    sorrend = $(formom).children('input').val();
   

    $('#galleryList').html('<center style="font-size:16px;">..Betöltés..</center>');
    var adat = 'imgList[sorrend]='+encodeURIComponent(sorrend)+'&imgList[leiras]='+encodeURIComponent(leiras);
    //alert(adat);
    
    $.post('<?= BASE_URL; ?>modulok/cikkgaleria/lista.php?modositas='+id+'&torles='+torles+'&cikkId=<?= $sz['id'] ?>', adat, function(e){
        $('#galleryList').html(e) ;
    });
    
}

function shortcode(id, igazitas,galnyit) {
    
    shortc = '[galeriakep igazit="'+igazitas+'"  galnyit="'+galnyit+'"  id="'+id+'" ]';
    var path = shortc;
    path = path.replace(/ &amp;gt; /g,".");
        //console.log(path);
    addtoppath(path);
    document.execCommand('copy', true);
    
    
}
function addtoppath(path) {
        //console.log(path);
        $('#copypath').val(path);
        $('#toppathwrap').show();
        $('#copypath').focus();
        $('#copypath').select();
    }   

$().ready(function(){
    galeriaFrissites();
});

$(document).ready(function(){
    //store nodepath value to clipboard (copy to top of page)
    
    //initially hide copy window
    $('#toppathwrap').hide();

    
});


</script>

<style>


.galSzerkeszto {
    padding: 10px;
    margin-bottom: 4px;
    border: 1px solid #aaa;
    background: yellow;
    background: rgb(255,255,255); /* Old browsers */
/* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMTAwJSIgeDI9IjEwMCUiIHkyPSIwJSI+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjZmZmZmZmIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNDclIiBzdG9wLWNvbG9yPSIjZjZmNmY2IiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2VkZWRlZCIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=);
background: -moz-linear-gradient(45deg,  rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left bottom, right top, color-stop(0%,rgba(255,255,255,1)), color-stop(47%,rgba(246,246,246,1)), color-stop(100%,rgba(237,237,237,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(45deg,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(45deg,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(45deg,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* IE10+ */
background: linear-gradient(45deg,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=1 ); /* IE6-8 fallback on horizontal gradient */

}
.galSzerkeszto img {
    float: right;
    margin: 5px;
    border: 3px solid #fff;
    height: 80px;
}
.galSzerkeszto textarea {
    height: 30px;
    width: 70%;
}
.galSzerkeszto button,.galSzerkeszto form a, .galSzerkeszto.gomb, a.galGomb {
    float: none;
    display: inline-block;
    background: green;
    color: #fff;
    padding: 3px 10px 3px 10px;
    
}

</style>
    <?php
    $html->headerStop();


}

?>