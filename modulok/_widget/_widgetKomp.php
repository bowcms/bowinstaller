<?php

class _widgetKomp {
    
    public function egyWidgetSor($w) {
        $widget = widgetKereses($w['fuggveny']);
        ?>
        <div class="wgBox elemek clearfix" data-wgid="<?= $w['id']?>" style="border-bottom: 1px solid #aaa;">
        
        <div class="row" style="cursor: move;">
            <div class="span6 text-left" style="padding-top: 5px;">
                <b ><?= __f($widget['nev']);?></b>
            </div>
            <div class="span6 text-left">
                <div class="btn-group">
                <button class="btn btn-info" onclick="modalOpen(<?= $w['id']?>);"><?= __f('Szerkesztés')?></button>
                <button class="btn btn-danger" onclick="if(confirm('<?= __f('A törlés végleges!'); ?>')==true)hivas('torol', '<?= $w['pozicio']?>', '<?= $w['id']; ?>');"><?= __f('Törlés')?></button>
            </div>
            </div>
            
            
            </div>
        </div>
       
        
        <div class="sortGrid" id="<?= $w['pozicio'].'_'.$w['id']?>"></div>
        <?php
    }
    
    
    public function poziciok($p){
        $html = Html::peldany();
        ?>
        <div class="pozicioKontener">
            
            <?php
            if (empty($p)) {
                print '<p>'.__f('Nincs elérhető pozíció, pozíciók megadhatók a téma tulajdonsagok filejában').'</p>';
            } else {
                foreach ($p as $kulcs => $ertek) {
                    ?>
                    
                    <div class="new-update clearfix text-right pozicioDiv"  >
                        <a title="" href="#"><strong><?= __f($ertek['nev']);?></strong></a>
                        <div class="tarolo clearfix kapcsoltTablak" id="<?= $kulcs; ?>" style="padding-top: 7px;"></div>
                        
                        <br style="clear: both;"/>
                        
                    </div>
                    
                    <?php
                    $html->headerStart();
                    ?>
                    <script type="text/javascript">
                        $().ready(function(){
                            hivas('lista', '<?= $kulcs?>', null);
                            
                        });
                    </script>
                    <?php
                    $html->headerStop();
                    
                }
            }
            ?>
        </div>
        <?php
    }
    
    public function widgetek($w){
        
        ?>
        <div class="widgetKontener ">
            
            <?php
            if (empty($w)) {
                print '<p>'.__f('Nincs elérhető widget').'</p>';
            } else {
                foreach ($w as $ertek) {
                    ?>
                    <div class="elemek new-update text-right widgetpeldany" id="<?= $ertek['fuggveny']; ?>" data-wgid="0" style="text-align: left;" >
                        <strong><?= __f($ertek['nev']);?></strong>
                        <?= __f($ertek['leiras']);?>
                    </div>
                    
                    <?php
                }
            }
            ?>
        </div>
        <script type="text/javascript">
            var cont;
            $().ready(function(){
                var cont = $('.widgetKontener').html();
$( ".widgetKontener" ).sortable({
    connectWith: ".kapcsoltTablak",
    forcePlaceholderSize: false,
     placeholder: "helyorzo",
    cursor: "crosshair", cursorAt: { top: 0, left: 0 },
    helper: function() {
        //debugger;
                return "<div style=\"background:#fff;padding:3px;color:#888;position: static;\"><?= __f('Mozgass a megfelelő pozícióba');?></div>";
    },
    
    stop: function() {
        copyHelper && copyHelper.remove();
         $('.widgetKontener').html(cont);
    }
});
    
                
            });
        </script>
        <?php
    }
    
    
    
}