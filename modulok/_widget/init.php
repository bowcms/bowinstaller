<?php
/**
 * Title: Widgets
 * Author: bowCms
 * Web: http://example.com
 * Description: Widget position manager
 * 
 **/

adminMenu('Widget', 'unicornFomenu', 'widgetUnicorn', 'widgetUnicorn', 'icon-th-large');
adminMenu('Widget', 'unicornOldalMenu',__f('Weboldal'), 'widgetUnicorn', 'icon-th-large');


function widgetUnicorn($feladat = '', $id = '') {
    include (dirname(__FILE__).'/widgetUnicorn.php');
}