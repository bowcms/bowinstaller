<?php

define('CSAK_KERET', 1);
$base = (dirname(dirname(dirname(__file__)))) . '/';
include ($base . 'ajaxkeret.php');
include ('_widgetKomp.php');
$komp = new _widgetKomp();

if (isset($_POST['wgid'])) {
    $a = $_POST['a'];
    $a = serialize($a);
    $id = $_POST['wgid'];
    $m = array('id' => $id, 'adat' => $a);

    sqladat($m, 'bow_widgetxpozicio');
    $_REQUEST['wgid'] = $id;

    sqltorles('bow_widgetxcsoport', $_REQUEST['wgid'], 'widget_id');
    sqltorles('bow_widgetxoldal', $_REQUEST['wgid'], 'widget_id');

    if (isset($_REQUEST['csoport'])) {
        foreach ($_REQUEST['csoport'] as $csoport_id => $e) {
            $a = array();
            $a['csoport_id'] = $csoport_id;
            $a['widget_id'] = $_REQUEST['wgid'];
            sqladat($a, 'bow_widgetxcsoport');
        }
    }
    unset($a);
    if (isset($_REQUEST['oldal'])) {
        foreach ($_REQUEST['oldal'] as $oldal_id => $e) {
            $a = array();
            $a['oldal_id'] = $oldal_id;
            $a['widget_id'] = $_REQUEST['wgid'];
            sqladat($a, 'bow_widgetxoldal');
        }
    }
}


if (isset($_GET['id'])) {
    $tag = Tagok::peldany();

    $sql = "SELECT * FROM bow_widgetxpozicio WHERE id = " . (int)$_GET['id'];
    $rs = sqlAssocRow($sql);
    $wget['id'] = (int)$_GET['id'];
    if (empty($rs)) {
        print '<strong>' . __f('Hiba történt...') . '</strong>';
        exit;
    }
    $fuggveny = $rs['fuggveny'] . 'Set';
    if (function_exists($fuggveny)) {
        print '<input name="wgid" value="' . $rs['id'] . '" type="hidden" />';
?>
        
    <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
        <li class="active"><a href="#red" data-toggle="tab"><?= __f('Beállítások') ?></a></li>
        <li><a href="#orange" data-toggle="tab"><?= __f('Korlátozások') ?></a></li>
    </ul>
    <div id="my-tab-content" class="tab-content">
        <div class="tab-pane active" id="red">
            <?php
        call_user_func($fuggveny, $rs['adat']);
?>
        </div>
        <div class="tab-pane" id="orange">
            <label>Csoportkorlátozás</label>
                        <?php
        $sql = "SELECT * FROM bow_csoportok WHERE rang < " . $tag->rang;
        $csRs = sqluniv4($sql);
        foreach ($csRs as $cs):
            $sql = "SELECT id FROM bow_widgetxcsoport WHERE csoport_id = " . $cs['id'] .
                " AND widget_id = " . $wget['id'];
            $csxRs = sqluniv3($sql);

            if (isset($csxRs['id'])) {
                $selected = ' checked="checked" ';
            } else {
                $selected = '';
            }

?>
                        <input style="width: 20px;" type="checkbox" <?= $selected; ?> value="1" name="csoport[<?= $cs['id']; ?>]" /> - <?= $cs['nev']; ?><br />
                        <?php
        endforeach;
?>
                        <label>Oldalak</label>
                        <?php
        $sql = "SELECT * FROM bow_oldalak Order BY cim ASC ";
        $oRs = sqluniv4($sql);
        foreach ($oRs as $o):
            $sql = "SELECT id FROM bow_widgetxoldal WHERE oldal_id = " . $o['id'] .
                " AND widget_id = " . $wget['id'];

            $csxRs = sqluniv3($sql);
            if (isset($csxRs['id'])) {
                $selected = ' checked="checked" ';
            } else {
                $selected = '';
            }

?>
                        <input style="width: 20px;" type="checkbox" <?= $selected; ?> value="1" name="oldal[<?= $o['id']; ?>]" /> - <?= $o['cim']; ?><br />
                        <?php
        endforeach;
?>
        </div>
        
    </div>
        
        
    
                                
<script type="text/javascript">
    
        $('#tabs').tab();
    
</script>    
       
        <?php

    } else {
        print '<strong>' . __f('Nincs beállítási lehetőség...') . '</strong>';
        exit;
    }
    exit;
}


$tag = Tagok::peldany();
if ($tag->id == 0)
    return;
// TODO: megcsinálni a komolyabb jogosultságot

$pozicio = ((isset($_GET['p'])) ? $_GET['p'] : null);
$feladat = ((isset($_GET['f'])) ? $_GET['f'] : null);
$wid = ((isset($_GET['wid'])) ? $_GET['wid'] : null);
$elem = ((isset($_GET['e'])) ? $_GET['e'] : null);
$s = ((isset($_GET['s'])) ? $_GET['s'] : null);
if ($s == 'undefined')
    $s = false;
$z = $s;

switch ($feladat) {

    case 'add':


        $sql = "SELECT MAX(sorrend) ms FROM bow_widgetxpozicio WHERE tema = '" . $_SESSION['_wg_tema'] .
            "' AND pozicio = '$pozicio'";
        $rs = sqlAssocRow($sql);
        $sorrend = $rs['ms'] + 10;

        if ($s) {
            $sql = "SELECT sorrend FROM bow_widgetxpozicio WHERE id = " . $s . " ";

            $rs = sqlAssocRow($sql);
            if (!empty($rs))
                $sorrend = $rs['sorrend'] + 5;

        }


        $sql = "INSERT INTO bow_widgetxpozicio SET tema = '" . $_SESSION['_wg_tema'] .
            "', pozicio = '$pozicio', fuggveny='$elem', sorrend =  $sorrend ";
        sqluniv($sql);


        sorrend($pozicio);
        // kiiratás
        $sql = "SELECT * FROM bow_widgetxpozicio WHERE tema = '" . $_SESSION['_wg_tema'] .
            "' AND pozicio = '$pozicio' ORDER BY sorrend ASC";
        $rs = sqlAssocArr($sql);
        foreach ($rs as $w) {
            $komp->egyWidgetSor($w);
        }


        break;

        break;
    case 'torol':
        $sql = "DELETE FROM bow_widgetxpozicio WHERE id = " . (int)$elem;
        sqluniv($sql);
        $sql = "SELECT * FROM bow_widgetxpozicio WHERE tema = '" . $_SESSION['_wg_tema'] .
            "' AND pozicio = '$pozicio' ORDER BY sorrend ASC";

        $rs = sqlAssocArr($sql);
        foreach ($rs as $w) {
            $komp->egyWidgetSor($w);
        }
        break;
    case 'sort':
        if (trim($elem) != '') {
            $elemek = explode('_', trim($elem, '_'));
            $s = 10;
            foreach ($elemek as $id) {
                if ($id == 0) {
                    // hozzáadjuk, új elem
                    $sql = "INSERT INTO bow_widgetxpozicio SET tema = '" . $_SESSION['_wg_tema'] .
                        "', pozicio = '$pozicio', fuggveny='$z', sorrend =  $s ";
                    sqluniv($sql);

                } else {
                    $sql = "UPDATE bow_widgetxpozicio SET sorrend = " . $s . ",  pozicio = '$pozicio' WHERE id = " .
                        $id;
                    sqluniv($sql);
                }

                $s += 10;
            }
        }
    case 'lista':
        $sql = "SELECT * FROM bow_widgetxpozicio WHERE tema = '" . $_SESSION['_wg_tema'] .
            "' AND pozicio = '$pozicio' ORDER BY sorrend ASC";
        $rs = sqlAssocArr($sql);
        foreach ($rs as $w) {
            $komp->egyWidgetSor($w);
        }
        break;

}


function sorrend($pozicio)
{
    $sql = "SELECT * FROM bow_widgetxpozicio WHERE tema = '" . $_SESSION['_wg_tema'] .
        "' AND pozicio = '$pozicio' ORDER BY sorrend ASC";
    $rs = sqlAssocArr($sql);
    if (!empty($rs)) {
        $s = 10;
        foreach ($rs as $sor) {
            $sql = "UPDATE bow_widgetxpozicio SET sorrend = " . $s . " WHERE id = " . $sor['id'];

            sqluniv($sql);
            $s += 10;
        }
    }

}

?>