<?php

$html = Html::peldany();
$html->helyorzoHozzafuzes('adminUtvonal', '<a href="' . UNIC_URL . '">' . __f('Witgetek') .
    '</a>');
$html->helyorzo('lapCim', __f('Witgetek'));


if (!isset($_SESSION['_wg_tema'])) {
    $_SESSION['_wg_tema'] = STILUS;
}
if (isset($_POST['_wg_tema'])) {
    $_SESSION['_wg_tema'] = $_POST['_wg_tema'];
}

include ('_widgetKomp.php');

$komp = new _widgetKomp();
$widgetek = Beepulok::$widgetek;
 
?>
<form method="post">
    <label>Kérem, válaszd ki, melyik témához kapcsolódó widget-eket szeretnéd szerkeszteni! </label>
                    <select name="_wg_tema">
                        <?php

if ($handle = opendir(BASE . 'temak')) {
    $blacklist = array('.', '..');
    while (false !== ($file = readdir($handle))) {
        if (!in_array($file, $blacklist)) {
?>
                        <option value="<?= $file; ?>" <?= $_SESSION['_wg_tema'] ==
$file ? ' selected="selected" ' : '' ?> ><?= $file; ?></option>
                                    <?php
        }
    }
    closedir($handle);
}

$temaKonyvtar = BASE . 'temak/' . $_SESSION['_wg_tema'] . '/';
        // pozíciók begyüjtése
        @include ($temaKonyvtar . 'tulajdonsagok.php');
        if (!isset($poziciok)) $poziciok = false;
        
        
       
        
        


?>
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    <input type="submit" value="Rajta"/>    
</form>
<div class="row-fluid">
    <div class="span6">
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-file"></i>
								</span>
								<h5><?= __f('Elérhető widgetek') ?></h5>
							</div>
							<div class="widget-content nopadding kapcsoltTablak">
								<?php
                                $komp->widgetek($widgetek);
                                ?>
                                <br style="clear: both;"/>
							</div>
						</div>
    </div>
    
    <div class="span6" >
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-file"></i>
								</span>
								<h5><?= __f('Stílus pozíciók') ?></h5>
							</div>
							<div class="widget-content nopadding" >
								<?php
                                $komp->poziciok($poziciok);
                                ?>
                                <br style="clear: both;"/>
							</div>
						</div>
    </div>
    </div>
</div>
<div id="myEditor" class="modal hide" style="display: none; " aria-hidden="true">
										<div class="modal-header">
											<button data-dismiss="modal" class="close" type="button">×</button>
											<h3><?= __f('Widget szerkesztő')?></h3>
										</div>
                                        <form onsubmit="return false;" id="myModaForm">
										<div class="modal-body">
											
										</div>
										<div class="modal-footer">
											<a data-dismiss="modal" onclick="modalSave();" class="btn btn-primary" href="#"><?= __f('Mentés')?></a>
											<a data-dismiss="modal" class="btn" href="#"><?= __f('Mégsem')?></a>
										</div>
                                        </form>
</div>
<?php
$html->headerStart();
?>
<script type="text/javascript">
$().ready (function(){
    
    
});

function modalOpen(id) {
    $('#myEditor').modal();
    $('.modal-body').html('<center><?= __f('Betöltés folyamatban...'); ?></center>');
    $('.modal-body').load('<?= BASE_URL?>modulok/_widget/ajaxWidgetUnicorn.php?id='+id);    
}
function modalSave() {
    $.post('<?= BASE_URL?>modulok/_widget/ajaxWidgetUnicorn.php',$('#myModaForm').serialize());
}
function hivas(feladat, pozicio, elem, sorrend) {
    $('#'+pozicio).addClass('opa');
    $('#'+pozicio).load('<?= BASE_URL;?>modulok/_widget/ajaxWidgetUnicorn.php?f='+feladat+'&p='+pozicio+'&e='+elem+'&s='+sorrend, function(){
        $('#'+pozicio).removeClass('opa');
        $( '#'+pozicio ).sortable(
        {
            connectWith: ".kapcsoltTablak",
            helper: function() {
        //debugger;
                return "<div style=\"background:#fff;padding:3px;color:#888;position: static;\"><?= __f('Mozgass a megfelelő pozícióba');?></div>";
            },
            stop: function() {
                divek = $('#'+pozicio+' div.wgBox');
                idStr = '';
                for(i = 0; i < divek.length;i++) idStr = idStr + '_'+$(divek[i]).attr('data-wgid');
                //alert(idStr);
                hivas('sort',pozicio,idStr,0);
            },
            cursor: "crosshair", cursorAt: { top: 0, left: 0 },
            receive: function(e,ui) {
                copyHelper= null;
                uj = '';
                divek = $('#'+pozicio+' div.elemek');
                idStr = '';
                for(i = 0; i < divek.length;i++) {
                    idStr = idStr + '_'+$(divek[i]).attr('data-wgid');
                    if($(divek[i]).attr('id')) {
                        uj = $(divek[i]).attr('id');
                    }
                } 
                
                hivas('sort',pozicio,idStr,uj);
            }
        }
        );
    });
}


</script>
<style>
.tarolo {
    
    -webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;
    background: #400000;
    color: #fff;
    min-height: 30px;
}

.opa {
    opacity: 0.5;
}
.hovereffect:hover {
    margin-bottom: 30px;
}
.widgetKontener.helyorzo {
    display: none;
}
.tab-pane input, .tab-pane textarea {
    width: 81%;
}
.tab-pane textarea {
    height: 270px;
    
}
</style>
<?php
$html->headerStop();
?>



