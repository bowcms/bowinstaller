<?php

/**
 * Title: Unicorn admin system
 * Author: bowCms
 * Web: http://example.com
 * Description: Extend system with responsive administration tools
 * 
 **/
 
 
 Beepulok::regisztralKomponens('uniAdmin', 'Unicornis admin', 'uniadmin');
 
 
 function uniAdmin($feladat = '', $id = '') {
    require_once (dirname(__FILE__).'/osztaly_unicorn_futtato.php');
    $u = new osztaly_unicorn_futtato();
    require_once (dirname(__FILE__).'/osztaly_unicorn_futtatoKomp.php');
    $komp = new osztaly_unicorn_futtatoKomp();
    $u->komp = $komp;
    $u->oldalGeneralas();
    
 }
 