<?php

class UnicornIni
{

    public function __construct()
    {
        $url = Url::peldany();
        $tag = Tagok::peldany();
        $reszek = $url->reszek;


        if ($reszek[0] == 'ajax')
            array_shift($reszek);
        if ($reszek[0] == 'unicorn') {
            header('Location: ' . BASE_URL . 'uniadmin');
            exit;
        }

        if ($reszek[0] == 'uniadmin') {
            $url->stilus = 'unicorn';
            array_shift($reszek);
            $url->param = $reszek;
            define('UNICORN', 1);
            
        }

    }

    

}

?>