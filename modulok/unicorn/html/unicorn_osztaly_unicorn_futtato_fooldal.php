<h3><?= __f('Weboldal karbantartás'); ?></h3>
<div class="alert alert-info"><?= __f ('Amennyiben az oldal karbantartás alatt áll, kapcsold be a várakoztató üzemmódot. Ebben az esetben a témakönyvtárban található index[nyelvkod].html kerül betöltésre')?></div>
<div class="control-group">
    <form method="post">
        <label class="control-label"><?= __f('Weboldal karbantartás '); ?></label>
        <div class="controls">
            <select name="offline">
                <option value="0" <?= (($offLine==0)?' selected="selected" ':'')?> ><?= __f('Kikapcsolva');?></option>
                <option value="1" <?= (($offLine==1)?' selected="selected" ':'')?> ><?= __f('Bekapcsolva');?></option>
            </select>
            <button type="submit" class="btn btn-danger"><?= __f('Mentés')?></button>
        </div>
    </form>
</div>





