<?php

class osztaly_unicorn_futtatoKomp {
    
    private $h = Null;
    
    public function __construct() {
        $this->h = Html::peldany();
    }
    
    public function fomenu($lista) {
        
        $this->h->helyorzoFelvetelStart();
        foreach($lista as $csoport => $gombok) {
            if (count($gombok)==1) {
                $gomb = $gombok[0];
                $link = BASE_URL.'uniadmin/'.$gomb['modul'].':'.$gomb['fuggveny'].'.html';;

        ?>
        <button class="btn" onclick="window.location.href='<?= $link; ?>'"><?= (($gombok[0]['ikon']!='')?' <i class="'.$gombok[0]['ikon'].'" ></i> ':''). __f($gomb['cim']);;?></button>
        <?php    
            } else {
                ?>
        <button class="btn"><?= $gombok[0]['gombCsoport']; ?></button><button data-toggle="dropdown" id="dd_<?= $csoport; ?>" class="btn dropdown-toggle"><span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dd_<?= $csoport; ?>">
                <?php 
                foreach ($gombok as $gomb) {
                    $link = BASE_URL.'uniadmin/'.$gomb['modul'].':'.$gomb['fuggveny'].'.html';;

                    ?>
            <li role="presentation"><a href="<?= $link; ?>" role="menuitem" tabindex="-1"><i class="<?= $gomb['ikon']?>"></i> <?= __f($gomb['cim']);?></a></li>
                    <?php
                }
                ?>
        </ul>
                <?php
            }
        }
        
        
        $this->h->helyorzoFelvetelStop('unicornFomenu');
        
        
    }
    
    public function oldalMenu ($lista, $aktiv) {
        
        $this->h->helyorzoFelvetelStart();
        foreach($lista as $csoport => $gombok) {
            if (count($gombok)==1) {
                $gomb = $gombok[0];
                $hivott = $gomb['modul'].':'.$gomb['fuggveny'];
                $link = BASE_URL.'uniadmin/'.$gomb['modul'].':'.$gomb['fuggveny'].'.html';
        ?>
        <li <?= ($aktiv == $hivott)?' class="active" ':'' ?> ><a href="<?= $link; ?>"><?= (($gombok[0]['ikon']!='')?' <i class="icon '.$gombok[0]['ikon'].'" ></i> ':'').'<span>'. __f($gomb['cim']).'</span>';?></a></li>
        <?php    
            } else {
                ?>
                
        <li class="submenu">
					<a href="#"><i class="icon <?= $gombok[0]['ikon']; ?>"></i> <span><?= $gombok[0]['gombCsoport']; ?></span> <span class="label"><?= count($gombok); ?></span></a>
					<ul>
                    
                <?php 
                foreach ($gombok as $gomb) {
                    $link = BASE_URL.'uniadmin/'.$gomb['modul'].':'.$gomb['fuggveny'].'.html';
                    $hivott = $gomb['modul'].':'.$gomb['fuggveny'];
                    ?>
                    
            <li <?= ($aktiv == $hivott)?' class="active" ':'' ?> ><a href="<?= $link; ?>" ><?= __f($gomb['cim']);?></a></li>
            
                    <?php
                }
                ?>
						
					</ul>
        </li>
                
                <?php
            }
            ?>
        <script type="text/javascript">
            $().ready(function(){
                $('li.active').parents('li.submenu').addClass('active').addClass('open').children('ul').css('display','block');
            });
        </script>
            <?php
        }
        
        
        $this->h->helyorzoFelvetelStop('unicornOldalMenu');
        
        
    }
    
    public function uniTablaFej($tablaCim,  $opciok) {
        
        Beepulok::futtat('uniTablaFejButton_Pos',$opciok);
        
        
        $mezok = $opciok['mezok'];
        
        if(isset($opciok['filter'])):
        ?>
        
        <div class="row">
            
            <div class="span1"></div>
            <div class="span4">
            <?php
            Beepulok::futtat('uniTablaFejButton_Pos2',$opciok);
            
            if (isset($opciok['ujElemGomb'])):
            ?>
            <button class="btn btn-info" onclick="window.location.href='<?= UNIC_URL?>edit/0'"><?= $opciok['ujElemGomb']; ?></button>
            <?php
            endif;
            ?>
            </div>
            <div class="span7">
            
            <?php if (!isset($opciok['keresotiltas'])):?>
            <form class="form-horizontal" role="form" method="post">
            
                <input type="text" name="uniTablaKeresoStr" id="uniTablaKeresoStr" value="<?= $_SESSION['unicorn']['tablaszerkeszto']['kereses'];?>" placeholder="<?= __f('Keresés')?>" />
                <button class="btn btn-info" type="submit"><span class="icon  icon-search"></span></button>
                <button class="btn btn-warning" type="submit" onclick="$('#uniTablaKeresoStr').val('');"><span class="icon icon-remove"></span></button>
            </form>
            <?php
            endif;
            ?>
            </div>
            
        </div>
        <?php
        endif;
        ?>
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<input type="checkbox"  placeholder="<?= __f('Keresés')?>" id="title-checkbox" name="title-checkbox" />
								</span>
                                <h5><?= $tablaCim; ?></h5>
                                
								
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped with-check">
									<thead>
										<tr>
                                            <th><i class="icon-resize-vertical"></i></th>
											<?php
                                            foreach ($mezok as $mezo):
                                            ?>
                                            <th><?= $mezo; ?></th>
                                            <?php endforeach; 
                                            if (!empty($opciok['plusszmezok']))
                                            foreach ($opciok['plusszmezok'] as $kulcs =>  $plusszMezoFuggveny):
                                            ?>
                                            <th><?= $kulcs; ?></th>
                                            <?php
                                            endforeach;
                                            ?>
                                            <?php
                                            Beepulok::futtat('uniTablaFej_AddingTH', $opciok);
                                            ?>                                            
                                            <th> </th>
                                        </tr>
									</thead>
									<tbody>
                        
        <?php
    }
    
    public function uniTablaEgySor($sor, & $opciok) {
        $mezok = $opciok['mezok'];
        if (isset($opciok['uri'])) {
            $uriadd = $opciok['uri'];
        } else {
            $uriadd = '';
        }
        ?>
                                        <tr>
											<td><input type="checkbox" value="<?= $sor['id']; ?>" /></td>
        <?php
        
        foreach ($mezok as $mezo => $v):
            $ertek = $sor[$mezo];
            
            if (isset($opciok['callback'][$mezo])) {
                
                $ertek = call_user_func($opciok['callback'][$mezo], $ertek);
            }
        ?>
                                            <td><?= $ertek; ?></td>
                                            
        <?php
        endforeach;
        
        if (!empty($opciok['plusszmezok']))
        foreach ($opciok['plusszmezok'] as $plusszMezoFuggveny):
        ?>
                                            <td><?= call_user_func($plusszMezoFuggveny, $sor['id']); ?></td>
        <?php
        endforeach;
        
        Beepulok::futtat('uniTablaFej_AddingTD', $sor);
        ?>
        
                                            <td>
        <?php
        if (!empty($opciok['vezerlok'])) $this->vezerloGombok($sor['id'], $opciok['vezerlok']);
        ?>
        
                                            </td>
										</tr>
        <?php
    }
    
    public function uniTablaLab($opciok) {
        $link = UNIC_URL.'delete/';
            if (defined('UNIC_SUBURI')) {
                $link = UNIC_SUBURI.'delete/';
            }
        ?>
                                    </tbody>
								</table>							
							</div>
                            <br />
                            <div class="btn-group">
                                <?php
                                if (isset($opciok['plusszgomb'])) {
                                    foreach ($opciok['plusszgomb'] as $gomb) {
                                        
                                        call_user_func($gomb);
                                    }
                                }
                                Beepulok::futtat('uniTablaLabButton_Pos', $opciok);
                                ?>
                                <button class="btn-danger" onclick="if(confirm('<?= __f('Biztos vagy benne?'); ?>')==true)window.location.href='<?= $link;?>'+kotegeltTorlesUniBow(this);"><?= __f('Kijelöltek törlése');?></button>
                            </div>
                            <br />
                            <br />
						</div>
        <?php
    }
    
    public function vezerloGombok($id, $vezerlok) {
        
        foreach ($vezerlok as $uri => $vezer) {
            if (strpos($vezer,'callback')===0) {
                $cb = explode(':', $vezer);
                if (!call_user_func($cb[1], $id)) {
                    unset($vezerlok[$uri]);
                } else {
                    $vezerlok[$uri] = $cb[2];
                }
                
            }
            
        }
        
        ?>
        
        <?php
        foreach ($vezerlok as $uri => $vezer) {
            
            $link = UNIC_URL.$uri.'/'.$id;
            if (defined('UNIC_SUBURI')) {
                $link = UNIC_SUBURI.$uri.'/'.$id;
            }
            
            ?>
            
                <button class="btn btn-mini" onclick="<?= (($uri=='delete')?"if(confirm('".__f('Biztos vagy benne?')."')==false)return false;":'')?>window.location.href='<?= $link;?>'"><?= $vezer;?></button>
            
            <?php
        }
        ?>
        
        <?php
    }
    public function uzenetKiiras($cim, $szoveg, $stilus = 'success') {
        ?>
        <div class="alert alert-<?= $stilus?> alert-block">
            <a class="close" data-dismiss="<?= $stilus?>" href="javascript:void();" >×</a>
            <h4 class="alert-heading"><?= $cim; ?></h4>
            <?= $szoveg; ?>
        </div>
        <?php
    }
    public function formFej($cim, $tabla, $id) {
        ?>
        
        <div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5><?= $cim; ?></h5>
							</div>
							<div class="widget-content nopadding">
								<form action="<?= UNIC_URL; ?>" method="post" class="form-horizontal">
								    <input type="hidden" name="uniForm_<?= $tabla; ?>[id]" value="<?= $id; ?>"/>
        <?php
    }
    
    public function formInput($tabla, $kulcs, $ertek, & $opciok) {
        
        ?>
        <div class="control-group">
										<label class="control-label"><?= $opciok['mezok'][$kulcs]['label']; ?></label>
										<div class="controls">
											<input name="uniForm_<?= $tabla; ?>[<?= $kulcs;?>]" value="<?= $ertek; ?>" type="text" />
											<span class="help-block"><?= $opciok['mezok'][$kulcs]['help']; ?></span>
										</div>
        </div>
        <?php
    }
    public function formSelect($tabla, $kulcs, $ertek, & $opciok) {
        $lista = $opciok['mezok'][$kulcs]['ertekek'];
        ?>
        <div class="control-group">
										<label class="control-label"><?= $opciok['mezok'][$kulcs]['label'].':'; ?></label>
										
                                        
                                        <div class="controls">
											<select name="uniForm_<?= $tabla; ?>[<?= $kulcs;?>]">
                                                <?php
                                                foreach($lista as $e => $l):
                                                   
                                                   
                                                ?>
                                                <option value="<?= $e; ?>" <?= (($e == $ertek)?' selected="selected" ':'')?> ><?= $l; ?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											<span class="help-block"><?= $opciok['mezok'][$kulcs]['help']; ?></span>
										</div>
        </div>
        <?php
    }
    public function formSelectExtra($tabla, $kulcs, $ertek, & $opciok) {
        $lista = $opciok['extra_mezok'][$kulcs]['ertekek'];
        ?>
        <div class="control-group">
										<label class="control-label"><?= $opciok['extra_mezok'][$kulcs]['label'].':'; ?></label>
										
                                        
                                        <div class="controls">
											<select name="uniForm_<?= $tabla; ?>[<?= $kulcs;?>]">
                                                <?php
                                                foreach($lista as $e => $l):
                                                   
                                                   
                                                ?>
                                                <option value="<?= $e; ?>" <?= (($e == $ertek)?' selected="selected" ':'')?> ><?= $l; ?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											<span class="help-block"><?= $opciok['extra_mezok'][$kulcs]['help']; ?></span>
										</div>
        </div>
        <?php
    }
    
    public function formMultiSelectExtra($tabla, $kulcs, $ertek, & $opciok) {
        $lista = $opciok['extra_mezok'][$kulcs]['ertekek'];
        if (!is_array($ertek)) $ertek = array();
        ?>
        <div class="control-group">
										<label class="control-label"><?= $opciok['extra_mezok'][$kulcs]['label'].':'; ?></label>
										
                                        
                                        <div class="controls">
											<select name="uniForm_<?= $tabla; ?>[<?= $kulcs;?>][]" multiple="">
                                                <option value="" <?= ((empty($ertek))?' selected="selected" ':'')?> ><?= __f('Nincs kiválasztva semmi')?></option>
                                                <?php
                                                foreach($lista as $e => $l):
                                                   
                                                   
                                                ?>
                                                <option value="<?= $e; ?>" <?= (( array_search($e , $ertek) )?' selected="selected" ':'')?> ><?= $l; ?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											<span class="help-block"><?= $opciok['extra_mezok'][$kulcs]['help']; ?></span>
										</div>
        </div>
        <?php
    }
    
    public function formRadio($tabla, $kulcs, $ertek, & $opciok) {
        $lista = $opciok['mezok'][$kulcs]['ertekek'];
        ?>
        <div class="control-group">
										<label class="control-label"><?= $opciok['mezok'][$kulcs]['label']; ?></label>
										<div class="controls">
											
                                                <?php
                                                foreach($lista as $e => $l):
                                                    if($e == 'NULL') $e = '';
                                                ?>
                                                <label><input type="radio" name="uniForm_<?= $tabla; ?>[<?= $kulcs;?>]" <?= (($e == $ertek)?' checked="checked" ':'')?>/><?= $l; ?></label>
                                                <?php
                                                endforeach;
                                                ?>
                                            
											<span class="help-block"><?= $opciok['mezok'][$kulcs]['help']; ?></span>
										</div>
        </div>
        <?php
    }
    
    public function formText($tabla, $kulcs, $ertek, & $opciok) {
        
        ?>
        <div class="control-group">
										<label class="control-label"><?= $opciok['mezok'][$kulcs]['label']; ?></label>
										<div class="controls">
											<textarea name="uniForm_<?= $tabla; ?>[<?= $kulcs;?>]" ><?= stripslashes($ertek); ?></textarea>
										</div>
									</div>
        
        <?php
    }
    
    
    public function formLab() {
        ?>
                                            <div class="form-actions">
										<button type="submit" class="btn btn-primary"><?= __f('Mentés'); ?></button>
                                        <button class="btn btn-warning" onclick="window.location.href='<?= UNIC_URL; ?>';return false;"><?= __f('Mégsem'); ?></button>
									</div>
								</form>
							</div>
						</div>						
					</div>
				</div>
        <?php
    }   
    
    public function legorduloGomb($cim,$lista) {
        ?>
        <div class="btn-group dropup">
									  <button class="btn btn-success"><?= $cim; ?></button>
									  <button data-toggle="dropdown" class="btn btn-success dropdown-toggle"><span class="caret"></span></button>
									  <ul class="dropdown-menu">
								        <?php
                                        foreach($lista as $sor):
                                        ?>
                                        <li><a href="<?= $sor['link']?>"><?= $sor['cim']?></a></li>
                                        <?php
                                        endforeach;
                                        ?>
									  </ul>
									</div><!-- /btn-group -->
        <?php
        
    }
    
    public function adminFormFej($cim, $action = '', $formId = 'bowUnicornForm', $formclass = "form-horizontal") {
        
        
        
        
        ?>
        
        <div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-align-justify"></i>									
								</span>
								<h5><?= $cim; ?></h5>
							</div>
							<div class="widget-content nopadding">
								<form id="<?= $formId; ?>" action="<?= $action; ?>" method="post" class="<?= $formclass; ?>" enctype="multipart/form-data">
								    
        <?php
    }
    
    public function adminFormSelect($label,$name, $value, $lista, $help) {
        ?>
        <div class="">
        <div class="control-group">
										<label class="control-label"><?= $label; ?></label>
										
                                        
                                        <div class="controls">
											<select name="<?= $name; ?>">
                                                <?php
                                                foreach($lista as $e => $l):
                                                   
                                                   
                                                ?>
                                                <option value="<?= $e; ?>" <?= (($e == $value)?' selected="selected" ':'')?> ><?= $l; ?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											<span class="help-block"><?= $help; ?></span>
										</div>
        </div>
        <?php
    }
    
    public function adminFormSubmit($felirat) {
        
        ?>
                                    <div class="form-actions">
										<button type="submit" class="btn btn-primary"><?= $felirat; ?></button>
                                    </div>
        <?php
     
    }
    
    public function adminFormInput($label, $name, $value, $help = '') {
        
        ?>
        <div class="control-group">
										<label class="control-label"><?= $label; ?></label>
										<div class="controls">
											<input name="<?= $name; ?>" value="<?= htmlspecialchars($value); ?>" type="text" />
											<span class="help-block"><?= $help; ?></span>
										</div>
        </div>
        <?php
    }
    public function adminFormRadio($label, $name, $lista,$value, $help='') {
        
        ?>
        <div class="control-group">
										<label class="control-label"><?= $label; ?></label>
										<div class="controls">
											
                                                <?php
                                                foreach($lista as $e => $l):
                                                    if($e == 'NULL') $e = '';
                                                ?>
                                                <label><input type="radio" name="<?= $name; ?>" <?= (($e == $value)?' checked="checked" ':'')?> value="<?= $e; ?>"/><?= $l; ?></label>
                                                <?php
                                                endforeach;
                                                ?>
                                            
											<span class="help-block"><?= $help; ?></span>
										</div>
        </div>
        <?php
    }
    public function adminFormText($label, $name, $value, $help='') {
        
        ?>
        <div class="control-group">
										<label class="control-label"><?= $label; ?></label>
										<div class="controls">
											<textarea style="height: 100px;" name="<?= $name; ?>" ><?= stripslashes($value); ?></textarea>
										</div>
									</div>
        
        <?php
    }
    public function adminFormTextAdv($label, $name, $value, $help='', $id = 'tinyTartalom') {
        
        
        require_once (BASE . 'osztaly/osztaly_jsszerkeszto.php');
        jsSzerkeszto::szovegszerkeszto($id);
        ?>
        
        <div class="widget-box">
                            <div class="widget-title">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab1" onclick="if($(this).parent().hasClass('active'))return false;tinyMCE.execCommand('mceToggleEditor', false,'<?= $id; ?>');"><?= $label ?>(HTML)</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab1" onclick="if($(this).parent().hasClass('active'))return false;tinyMCE.execCommand('mceToggleEditor', false,'<?= $id; ?>');"><?= $label ?>(TEXT)</a></li>
                                </ul>
                            </div>
                            <div class="widget-content tab-content">
                                <div id="tab1" class="tab-pane active">
                                    <textarea style="height: 100px;" id="<?= $id;?>" name="<?= $name; ?>" ><?= stripslashes($value); ?></textarea>
                                </div>
                                
                                
                            </div>
                            <?= Beepulok::futtat('adminFormTextAdvBottom');?>                          
                        </div>
        
        <?php
    }
    
    public function adminFormTextTiny($label, $name, $value, $help='', $id = 'tinyTartalom') {
        
        
        require_once (BASE . 'osztaly/osztaly_jsszerkeszto.php');
        jsSzerkeszto::szovegszerkeszto($id);
        ?>
        
        <div class="widget-box">
                            <div class="widget-title">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#tab1" onclick="if($(this).parent().hasClass('active'))return false;tinyMCE.execCommand('mceToggleEditor', false,'<?= $id; ?>');"><?= $label ?>(HTML)</a></li>
                                    <li class=""><a data-toggle="tab" href="#tab1" onclick="if($(this).parent().hasClass('active'))return false;tinyMCE.execCommand('mceToggleEditor', false,'<?= $id; ?>');"><?= $label ?>(TEXT)</a></li>
                                </ul>
                            </div>
                            <div class="widget-content tab-content">
                                <div id="tab1" class="tab-pane active">
                                    <textarea style="height: 100px;" id="<?= $id;?>" name="<?= $name; ?>" ><?= stripslashes($value); ?></textarea>
                                </div>
                                
                                
                            </div>                 
                        </div>
        
        <?php
    }
    public function adminFormHidden($name, $value) {
        
        ?>
        <input type="hidden" value="<?= $value;?>" name="<?= $name; ?>" />
        <?php
    }
    
    
    public function adminFormMultiSelect( $label,$name,$value, $options , $help = '') {
        $lista = $options;
        
        if (!is_array($value)) $value = array();
        ?>
        <div class="control-group">
										<label class="control-label"><?= $label.':'; ?></label>
										
                                        
                                        <div class="controls">
											<select name="<?= $name; ?>" multiple="">
                                                <option value="" <?= ((empty($value))?' selected="selected" ':'')?> ><?= __f('Nincs kiválasztva semmi')?></option>
                                                <?php
                                                foreach($lista as $e => $l):
                                                   $selected = false;
                                                   
                                                   foreach($value as $val) {
                                                        if ($val == $e) $selected = true;
                                                        
                                                   }
                                                   
                                                   
                                                   
                                                ?>
                                                <option value="<?= $e; ?>" <?= (( $selected )?' selected="selected" ':'')?> ><?= $l; ?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											<span class="help-block"><?= $help; ?></span>
										</div>
        </div>
        <?php
    }
    public function adminFormLab($submitFgv = '', $return = '') {
        
        ?>
                                            <div class="form-actions">
										<button type="submit" onclick="<?= $submitFgv;?>" class="btn btn-primary"><?= __f('Mentés'); ?></button>
                                        <button class="btn btn-warning" onclick="window.location.href='<?= UNIC_URL.$return;?>';return false;"><?= __f('Mégsem'); ?></button>
									</div>
								</form>
							</div>
						</div>						
					</div>
				</div>
        <?php
     
    }
    
    function popUpNoti($cim , $leiras) {
        ?>
        <script type="text/javascript">
            //<!--
            gitterUzenet('<?= htmlspecialchars($cim);?>', '<?= htmlspecialchars($leiras);?>');
            //-->
        </script>
        <?php
    }
    
}


