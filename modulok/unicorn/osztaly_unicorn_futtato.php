<?php

class osztaly_unicorn_futtato
{

     public $komp;
    private static $peldany;

    public static function peldany()
    {
        return self::$peldany;
    }
    public function __construct()
    {
        self::$peldany = $this;

    }
    
    public function uresOldal() {
        
    }
    
    public function oldalGeneralas()
    {
        $url = Url::peldany();
        $param = $url->param;
        $tag = Tagok::peldany();
        
        
        $html = $this->h = Html::peldany();
        $this->h->helyorzo('lapCim', __f('Vezérlőpult'));
        
        $this->unicornisLogin();
        // belépés
        if (!$tag->admin) {
            
            $header = '';
            if (isset($url->param[0]))
                if ($url->param[0] == 'visszaallitas') {
                    $header = "<script>\$().ready(function(){\$('#newpass').show();$('#loginform').hide();});</script>";
                }

            $html = Html::peldany();
            $html->fejHTML = file_get_contents(BASE . '/temak/unicorn/login.php');
            $html->labHTML = '';
            $html->header = $header;
            return;
        
        }
        
        
        
        // widgetek
        $fomenuPozicio = 'unicornFomenu';
        $oldalmenuPozicio = 'unicornOldalMenu';

        $menuPontok = Memoria::olvas('adminMenu');

        foreach ($menuPontok as $sor) {
            $csoport = $sor['gombCsoport'];
            if ($csoport == '')
                $csoport = 'csop' . rand(1000, 9999);
            if ($sor['gombPozicio'] == $oldalmenuPozicio) {
                $oldalMenu[strToUrl($csoport)][] = $sor;
            }
            if ($sor['gombPozicio'] == $fomenuPozicio) {
                $foMenu[strToUrl($csoport)][] = $sor;
            }


        }
        // tartalom generálás
        $reszek = $url->param;
        
        if (empty($reszek)) {
            $aktivMenu = '';
        } else {
            $aktivMenu = $reszek[0];
        }

        $this->komp->fomenu($foMenu);
        $this->komp->oldalMenu($oldalMenu, $aktivMenu);


        if (empty($reszek)) {
            $this->fooldal();
            return;
        }


        $exp = explode(':', $reszek[0]);
        
        if (isset($exp[1])) {
            $modul = $exp[0];
            $fuggveny = $exp[1];
            array_shift($reszek);

            if (function_exists($fuggveny)) {

                define('UNIC_URL', BASE_URL . 'uniadmin/' . $modul . ':' . $fuggveny . '/');
                define('UNIC_AJAX_URL', BASE_URL . 'ajax/uniadmin/' . $modul . ':' . $fuggveny . '/');

                
                call_user_func_array($fuggveny, $reszek);
                return;
            } else {
                print '<p>' . __f('Modul beállító függvény nem található') . ': ' . $fuggveny .
                    '</p>';
            }

        } else {
            print '<p>' . __f('Modul beállító nem található') . '</p>';
        }


    }

    public function fooldal()
    {
        if (isset($_POST['offline'])) {
            bowMetaMentes('weboldal','offline',serialize((int)$_POST['offline']) );
            
        }
        
        $offLine = bowMeta('weboldal','offline');
        $offLine = $offLine[0]['ertek'];
        include(modulHtmlElem('unicorn', 'fooldal'));
    }

    /**
     * szerkesztoTabla
     * 
     * @param $opciok = array()
     * 
     * $opciok felépítése:
     * [mezok] = array('mezo1' => 'mezo1Név', 'mezo2' => 'mezo2Név', ...);
     * [tablacim] = string
     * [callback] = array(field1 => callbackFunkcióNév)
     * [vezerlok] = array (edit, delete, feladat1, feladat2)
     * [filter] = string (mezo1,mezo2,mezo3) - keresőt ad a táblázathoz, a felsorolt mezőkben keres LIKE sql paranccsal
     */
    public function szerkesztoTabla($tabla, $opciok = array())
    {
        $w = '';


        if (isset($opciok['filter'])) {

            if (!isset($_SESSION['unicorn']['tablaszerkeszto']['kereses'])) {
                $_SESSION['unicorn']['tablaszerkeszto']['kereses'] = '';
            }
            if (!isset($_POST['uniTablaKeresoStr'])) {
                $_POST['uniTablaKeresoStr'] = $_SESSION['unicorn']['tablaszerkeszto']['kereses'];
                ;
            }
            if (isset($_POST['uniTablaKeresoStr'])) {
                if ($_POST['uniTablaKeresoStr'] != '') {
                    $_SESSION['unicorn']['tablaszerkeszto']['kereses'] = $_POST['uniTablaKeresoStr'];
                    $w = 'WHERE ';
                    $miben = explode(',', $opciok['filter']);
                    foreach ($miben as $mezo) {
                        $kereses[] = ' `' . trim($mezo) . '` LIKE "%' . $_POST['uniTablaKeresoStr'] .
                            '%" ';
                    }
                    $w .= implode(' OR ', $kereses);
                } else {
                    $_SESSION['unicorn']['tablaszerkeszto']['kereses'] = '';
                }

            }

        }
        if (isset($opciok['where'])) {
            if ($w == '') $w .= " WHERE "; else $w .= ' AND ';
            $w .= ' '.$opciok['where'].' ';
        }
        $sorrend = (isset($opciok['rendezes']) ? $opciok['rendezes'] : '');
        if (is_string($tabla)) {
            $sql = "SELECT * FROM $tabla $w $sorrend";
            Memoria::ir('UnicornSzerkesztoTabla_sql', $sql);
            Beepulok::futtat('UnicornSzerkesztoTabla_sql');
            $sql = Memoria::olvas('UnicornSzerkesztoTabla_sql');
        
            $rs = sqluniv4($sql);
        } else {
            $rs = $tabla;
        }
        

        if (!isset($opciok['mezok'])) {
            $sql = "SHOW COLUMNS FROM `$tabla`";
            $tRs = sqluniv4($sql);
            foreach ($tRs as $sor)
                $mezok[$sor['Field']] = $sor['Field'];

        } else {
            foreach ($opciok['mezok'] as $mezoNev) {
                $mezok[] = $mezoNev;
            }
        }
        $tablaCim = ((isset($opciok['tablacim'])) ? $opciok['tablacim'] : $tabla);
        $this->komp->uniTablaFej($tablaCim, $opciok);

        foreach ($rs as $sor) {
            $this->komp->uniTablaEgySor($sor, $opciok);
        }


        $this->komp->uniTablaLab($opciok);
    }
    
    public function unicornisLogin()
    {
        
        $url = Url::peldany();
        $tag = Tagok::peldany();

        if (!defined('UNICORN'))
            return false;
        $session = Munkamenet::peldany();

        $belepve = $session->getVar('unicornis_login');
        if (isset($url->param[0]))
            if ($url->param[0] == 'kilep') {
                $tag->kileptet();
                $session->setVar('unicornis_login', false);
                
            }

        if ($tag->admin) {
            $session->setVar('unicornis_login', true);
            $belepve = true;
        } else {
            $session->setVar('unicornis_login', false);
            $belepve = false;
        }

        if ($belepve)
            return false;
        // belépés
        if (isset($_POST['unicorn'])) {
            $u = $_POST['unicorn'];
            if (isset($u['jelszo'])) {
                $sql = "SELECT id FROM bow_tagok WHERE email = '" . str_replace(' ', '', $u['email']) .
                    "' and jelszo = '" . base64_encode(md5($u['jelszo'])) . "' LIMIT 1";

                $rs = sqluniv3($sql);
                if (isset($rs['id'])) {
                    $tag->beleptet($rs['id']);
                    $session->setVar('unicornis_login', true);

                    header('Location: '.BASE_URL.'uniadmin');
                    return;
                } else {
                    $html->helyorzo('login_hiba', __f('Hibás e-mail vagy jelszó'));
                }

            }
            if (isset($u['elfelejtett_jelszo'])) {
                if (isEmail($u['elfelejtett_jelszo'])) {
                    $kod = $u['elfelejtett_jelszo'] . '/' . time() . rand(1000, 9999);
                    $kod = base64_encode(md5($kod));
                    $sql = "UPDATE bow_tagok SET visszaallito_kod = '$kod' WHERE email = '" .
                        urldecode($u['elfelejtett_jelszo']) . "' ";
                    sqluniv($sql);
                    if (mysql_affected_rows() > 0) {
                        $link = BASE_URL . 'unicorn/visszaallitas/' . $kod . '.html';

                        Levelezo::helyettesitoHozzaad('link', $link);
                        Levelezo::sablon('unicorn_jelzovisszaallitas');
                        Levelezo::kuldes($u['elfelejtett_jelszo']);
                        $html->helyorzo('login_hiba', __f('E-mail visszaállító linket elküldtük e-mail címére'));
                    } else {
                        $html->helyorzo('login_hiba', __f('Ismeretlen e-mail cím!'));
                    }
                }
            }
            if (isset($u['ujjelszo'])) {
                $kod = $url->param[1];
                $sql = "SELECT id FROM bow_tagok WHERE visszaallito_kod = '" . $kod .
                    "' LIMIT 1";
                $rs = sqluniv3($sql);
                if (isset($rs['id'])) {
                    $t['id'] = $rs['id'];
                    $t['jelszo'] = base64_encode(md5($u['ujjelszo']));
                    sqladat($t, 'bow_tagok');
                    $tag->beleptet($t['id']);
                    header('Location: ' . BASE . 'uniadmin');
                    exit;
                }
            }
        }
        
    }

    public function uzenetKiiras($cim, $szoveg, $stilus = 'success')
    {
        $this->komp->uzenetKiiras($cim, $szoveg, $stilus);
    }
    /**
     * adatSzerkeszto
     * 
     * @param $cim Táblázat címe
     * @param $tabla DB tábla neve
     * @param $id adott DB sor id-ja
     * @param $opciok = array()
     * 
     * $opciok felépítése:
     * [kihagyott_mezok] = array('mezo1', 'mezo2', ...); # ezeket a mezőket nem tölti szerkesztőbe
     * [mezok] => array(
     *       'kulcs' => array(
     *           'label' => 'label cím',
     *           'help' => 'segítség szöveg'
     *       )
     *       
     *   )
     * );
     * 
     */
    public function adatSzerkeszto($cim, $tabla, $id, &$opciok)
    {
        if ((int)$id > 0) {
            $sql = "SELECT * FROM $tabla WHERE id = " . (int)$id;
            $rs = sqluniv3($sql);
            $id = (int)$id;
            if (!isset($rs['id']))
                return false;
        } else {
            $rs = array();
            $id = 0;
        }

        $sql = "SHOW COLUMNS FROM `$tabla`";
        $mezok = sqluniv4($sql);

        $this->komp->formFej($cim, $tabla, $id);


        foreach ($mezok as $mezo) {
            if ($mezo['Field'] == 'id')
                continue;

            $kulcs = $mezo['Field'];
            if (isset($opciok['kihagyott_mezok'])) {
                if (array_search($kulcs, $opciok['kihagyott_mezok']) !== false) {
                    continue;
                }
            }
            $ertek = (isset($rs[$kulcs])) ? $rs[$kulcs] : '';

            $tipus = $mezo['Type'];
            preg_match('#^([a-zA-z]+)#', $tipus, $talalat);
            $tipus = $talalat[1];
            if (isset($opciok['mezok'][$kulcs]['specialis'])) {
                $tipus = $opciok['mezok'][$kulcs]['specialis'];
            }
            switch ($tipus) {
                case 'int':
                case 'varchar':
                case 'tinyint':
                    $this->komp->formInput($tabla, $kulcs, $ertek, $opciok);
                    break;
                case 'text':
                    $this->komp->formText($tabla, $kulcs, $ertek, $opciok);
                    break;
                case 'select':
                    $this->komp->formSelect($tabla, $kulcs, $ertek, $opciok);
                    break;
                case 'radio':
                    $this->komp->formRadio($tabla, $kulcs, $ertek, $opciok);
                    break;
                default:
                    print '<strong>' . __f('Típus nem támogatott') . ': ' . $tipus . '</strong>';
                    break;
            }
        }

        if (isset($opciok['extra_mezok'])) {
            foreach ($opciok['extra_mezok'] as $kulcs => $adatok) {
                $ertek = (isset($rs[$kulcs])) ? $rs[$kulcs] : '';

                $tipus = $adatok['specialis'];
                $ertek = $adatok['beallitas'];
                switch ($tipus) {

                    case 'select':
                        $this->komp->formSelectExtra($tabla . '_ext', $kulcs, $ertek, $opciok);
                        break;
                    case 'multiselect':
                        $this->komp->formMultiSelectExtra($tabla . '_ext', $kulcs, $ertek, $opciok);
                        break;

                    default:
                        print '<strong>' . __f('Típus nem támogatott') . ': ' . $tipus . '</strong>';
                        break;
                }
            }
        }

        $this->komp->formLab();
        return true;
    }

    public function legorduloGomb($cim, $lista)
    {
        $this->komp->legorduloGomb($cim, $lista);

    }
}
