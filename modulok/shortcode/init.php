<?php
/**
 * Title: Shortcodes
 * Author: bowCms
 * Web: http://example.com
 * Description: Common shortcode interface for admin post editor
 * 
 * 
 **/
Beepulok::regisztral('runShortcodes','runShortcodes');

function runShortcodes() {
    if (defined('UNIC_URL'))  return false;
    $shortcodeFunctions = Beepulok::$shortcode;
    $text = Memoria::olvas('postText');
    
    if(!empty($shortcodeFunctions)) {
        foreach ($shortcodeFunctions as $codeFunc) {
            
            $kod = $codeFunc['kod'];
            
            if (is_array($text)) {
                
                $kodok= shortcodeMintaAdatok($kod, $text['bevezeto']);
          
                $kodok= array_merge( shortcodeMintaAdatok($kod, $text['tartalom']));
                
            } else {
               
                $kodok = shortcodeMintaAdatok($kod, $text);
            }
            
            foreach ($kodok as $peldany) {
                
                ob_start();
                
                call_user_func($codeFunc['fuggveny'], $peldany['attr'], ((isset($peldany['inner']))?$peldany['inner']:false));
                $csere = ob_get_contents();
                ob_end_clean();
                if (is_array($text)) {
                    $text['bevezeto'] = str_replace($peldany['kod'], $csere, $text['bevezeto']);
                    $text['tartalom'] = str_replace($peldany['kod'], $csere, $text['tartalom']);    
                } else {
                    $text =  str_replace($peldany['kod'], $csere, $text);
                }
                
                
            }
    
            
        }
        Memoria::ir('postText',  $text);
    }
}

Beepulok::regisztral('adminFormTextAdvBottom', 'shortcodeList');

function shortcodeList($a) {
    $sz = Memoria::olvas('_szerkeszto_Szovegszerkesztes_formKirakasElott');
    ?>
    <div style="padding-top: 5px;text-align: left;">
    <a href="#shortCodeModal" data-toggle="modal" onclick="shortCodeListUpdate();"  class="btn btn-default"><?= __f('Shortcode beillesztés')?></a>
    </div>
    <div id="shortCodeModal" class="modal hide" >
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button">×</button>
            <h3><?= __f('Elérhető shortcode-ok')?> (<?= __f('A HTML szerkesztővel használható');?>)</h3>
    </div>
    <div class="modal-body" id="shortcodeModalBody">
    
    </div>
    </div>
    <?php
    
    $html = Html::peldany();
    $html->headerStart();
    ?>
    <script>
    $().ready(function(){
        shortCodeListUpdate();
    });
    function shortCodeListUpdate() {
        $('#shortcodeModalBody').html('<h4><?= __f('Betőltés folyamatban!')?></h4>')
        $('#shortcodeModalBody').load('<?= BASE_URL; ?>modulok/shortcode/shortcode_handler.php?cikkId=<?= $sz['id']; ?>');
        
    }
    function insertShortCode(scode,opc) {
        str = '['+scode+' ';
        for (var key in opc) { 
            str = str + key + '="'+opc[key] + '" ';
            
        }
        str = str + ']';
        tinymce.activeEditor.execCommand('mceInsertContent', false, str);
        
    }
    function insertShortCodeWithClose(scode,opc) {
        str = '['+scode+' ';
        for (var key in opc) { 
            str = str + key + '="'+opc[key] + '" ';
            
        }
        str = str + ']text[/'+scode+']';
        tinymce.activeEditor.execCommand('mceInsertContent', false, str);
        
    }
    function insertShortCodecloseTag(scode) {
        str = '[/'+scode+']';
        
        tinymce.activeEditor.execCommand('mceInsertContent', false, str);
        
    }
    </script>
    <?php
    $html->headerStop();
    
}