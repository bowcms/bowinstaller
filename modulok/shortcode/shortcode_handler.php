<?php
define('CSAK_KERET', 1);
$base = dirname(dirname(dirname(__FILE__))).'/';
include($base.'ajaxkeret.php');

$lista = Beepulok::shortCodeList();

if (isset($_GET['sc'])) {
    if (isset($lista[(int)$_GET['sc']])) {
        $fuggveny = $lista[(int)$_GET['sc']]['fuggveny'].'Insert';
        
        if (function_exists($fuggveny)) {
            call_user_func($fuggveny, (int)$_GET['cikkId']);
            ?>
            <div class="row-fluid" style="text-align: right;">
                <a class="btn btn-link" href="javascript:void(0);" onclick="shortCodeListUpdate();"><?= __f('Vissza'); ?></a>
            </div>
            <?php
            exit;
        }
        
    }
}

?>
<div class="row-fluid">
<?php 
$i = 0;
foreach($lista as $k => $sor):
?>
<div class="span6 alert alert-info">
    <a href="javascript:void(0);" onclick="$('#shortcodeModalBody').load('<?= BASE_URL; ?>modulok/shortcode/shortcode_handler.php?cikkId=<?= (int)$_GET['cikkId']?>&sc=<?= $k; ?>')"><?= $sor['fuggveny']; ?></a>
</div>
<?php
    $i++;
    if ($i == 2):
        $i = 0;
    ?>
    </div>
    <div class="row-fluid">
    <?php
    endif;
endforeach;
?>
</div>
<?php


