<?php

/**
 * Title: System letter templates
 * Author: bowCms
 * Web: http://example.com
 * Description: Editing letter templates
 * 
 **/            
 
 adminMenu(__f('Levélkészítő'), 'unicornOldalMenu', __f('Eszközök'), 'levelekUnicorn', 'icon-envelope');

 
 function levelekUnicorn($feladat = '', $id = '') {
    $o = modulOsztalyTolto('levelek');
    $u = osztaly_unicorn_futtato::peldany();
    $o->levelekUnicorn($u, $feladat, $id);
 }
 
?>