<?php

class levelek
{

    public function levelekUnicorn($u, $feladat, $id)
    {

        $html = Html::peldany();
        $html->helyorzo('lapCim', __f('Levélszerkesztő'));
        if ($feladat == 'ts') {
            $u->komp->popUpNoti(__f('Törlés sikeres'), __f('A kijelölt felhasználókat eltávolítottam!'));
        }

        if ($feladat == 'delete') {
            $ids = explode('_', $id);
            foreach ($ids as $id) {
                if (is_numeric($id)) {

                    $sql = "DELETE FROM bow_levelek WHERE id = $id ";
                    sqluniv($sql);

                }
            }
            $u->komp->popUpNoti(__f('Törlés sikeres'), __f('A kijelölt felhasználókat eltávolítottam!'));
            header('Location: ' . UNIC_URL . 'ts');
        }
        
        if (isset($_POST['a'])) {
            $a = $_POST['a'];
            sqladat($a,'bow_levelek');
            $u->komp->popUpNoti(__f('Frissítve'), 'A levél mentésre került');
        }
        
        if ($feladat == 'edit') {
            $id = (int)$id;
            $a = sqluniv3("select * from bow_levelek WHERE id = $id");
            if (!isset($a['id'])) {
                $a = array();
                $a['id'] = 0;
                $a['targy'] = '';
                $a['valaszcim'] = '';
                $a['valasznev'] = '';
                $a['szoveg'] = '';
                $a['kulcs'] = '';
            }
            $u->komp->adminFormFej(__f('Levél szerkesztése'), UNIC_URL);
            $u->komp->adminFormHidden('a[id]', $a['id']);

            $u->komp->adminFormInput(__f('Tárgy'), 'a[targy]', $a['targy']);
            $u->komp->adminFormInput(__f('Válasz cím'), 'a[valaszcim]', $a['valaszcim']);
            $u->komp->adminFormInput(__f('Válasz név'), 'a[valasznev]', $a['valasznev']);
            
            
            
            print '<hr />';

            $u->komp->adminFormTextTiny(__f('Szöveg'), 'a[szoveg]', $a['szoveg'], '');
            $u->komp->adminFormInput(__f('Egyedi kulcs'), 'a[kulcs]', $a['kulcs']);
            
            $u->komp->adminFormLab();

            return;
        }

        $tablaOpciok = array(
            'tablacim' => __f('Rendszerlevelek'),
            'mezok' => array(
                'id' => 'ID',
                'targy' => __f('Tárgy'),
                'valaszcim' => __f('Válaszcím'),
                'kulcs' => 'Kulcs'),
            'vezerlok' => array('edit' => __f('Szerkesztés'), 'delete' => __f('Törlés')),
            'filter' => 'targy, szoveg',
            'ujElemGomb' => __f('Új levél létrehozása'));


        $u->szerkesztoTabla('bow_levelek', $tablaOpciok);
    }
}
