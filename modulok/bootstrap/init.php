<?php
/**
 * Title: Bootstrap Columns Shortcodes
 * Author: bowCms
 * Web: http://example.com
 * Description: Create nice columns in posts
 * 
 * 
 **/
 
 Beepulok::regisztralShortcode('bscol', 'bootstrapColumns');
 
 function  bootstrapColumns($attr, $inner = false) {
    print '<div class="'.$attr['class'].'">'.$inner.'</div>';
 }
 
 
 
 function bootstrapColumnsInsert($cikkId) {
    ?>
    <h3><?=__f('Tartalom hasábok');?></h3>
    <table style="width: 100%;">
        <tr>
            <td style="width: 80%;">
                <div class="jeloloDiv" style="width: 49%;"></div>
                <div class="jeloloDiv" style="width: 49%;"></div>
            </td>
            <td>
                <a class="label" href="javascript:void(0)" onclick="bsColInsert('6_6')" ><?= __f('Beillesztés'); ?></a>
            </td>
        </tr>
        <tr>
            <td>
                <div class="jeloloDiv" style="width: 32%;"></div>
                <div class="jeloloDiv" style="width: 32%;"></div>
                <div class="jeloloDiv" style="width: 32%;"></div>
            </td>
            <td>
                <a class="label" href="javascript:void(0)" onclick="bsColInsert('4_4_4')" ><?= __f('Beillesztés'); ?></a>
            </td>
        </tr>
        <tr>
            <td>
                <div class="jeloloDiv" style="width: 24%;"></div>
                <div class="jeloloDiv" style="width: 24%;"></div>
                <div class="jeloloDiv" style="width: 24%;"></div>
                <div class="jeloloDiv" style="width: 24%;"></div>
                
            </td>
            <td>
                <a class="label" href="javascript:void(0)" onclick="bsColInsert('3_3_3_3')" ><?= __f('Beillesztés'); ?></a>
            </td>
        </tr>
        <tr>
            <td>
                <div class="jeloloDiv" style="width: 24%;"></div>
                <div class="jeloloDiv" style="width: 24%;"></div>
                <div class="jeloloDiv" style="width: 49%;"></div>
                
                
            </td>
            <td>
                <a class="label" href="javascript:void(0)" onclick="bsColInsert('3_3_6')" ><?= __f('Beillesztés'); ?></a>
            </td>
        </tr>
        <tr>
            <td>
                <div class="jeloloDiv" style="width: 49%;"></div>
                <div class="jeloloDiv" style="width: 24%;"></div>
                <div class="jeloloDiv" style="width: 24%;"></div>
                
                
                
            </td>
            <td>
                <a class="label" href="javascript:void(0)" onclick="bsColInsert('3_3_6')" ><?= __f('Beillesztés'); ?></a>
            </td>
        </tr>
        
    </table>
    <script type="text/javascript">
    function bsColInsert(str) {
        spans = str.split('_');
        
        for (i in spans) {
            insertShortCodeWithClose('bscol', {'class' : 'col-md-'+spans[i]});
        }
        
    }
    </script>
    <style>
        .jeloloDiv {
            height: 10px;
            background: #46D9EE;
            
            display: inline-block;
        }
    </style>
    <?php
 }