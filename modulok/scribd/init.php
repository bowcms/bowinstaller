<?php

/**
 * Title: SCRIBD Plugin
 * Author: bowCms
 * Web: http://example.com
 * Description: View uploaded PDF documents via Scribd
 * 
 **/

Beepulok::regisztralShortcode('scribd', 'scribdShortcode');

function scribdShortcode ($adat) {
    $out = '
    <iframe class="scribd_iframe_embed" src="//www.scribd.com/embeds/'.$adat['docid'].'/content?start_page=1&view_mode=scroll&show_recommendations=true" data-auto-height="false" data-aspect-ratio="undefined" scrolling="no" id="doc_9913" width="100%" height="600" frameborder="0"></iframe>
    ';
    print $out;
}
function scribdShortcodeInsert($cikkId) {
    ?>
        <div class="row-fluid">
            
            <div class="span12">
                <div class="control-group">
                    <label class="control-label"><?= __f('Scribd dokumentum ID')?></label>
                    <div class="controls">
                        <input id="srcurl" value="" />
                    </div>
                </div>
                <div class="control-group">
                    <button type="button" onclick="insertShortCode('scribd',{'docid':$('#srcurl').val()});" class="btn btn-primary"><?= __f('Beillesztés'); ?></button>
            
                </div>
            </div>
           
        </div>
    <?php
}
