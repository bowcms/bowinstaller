<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Kategória doboz',
        'fuggveny' => 'kategoriaWidget', // ebben a file-ban definiáljuk a megjelenítést
        'leiras' => 'Áruház kategóriák megjelenítése'
);
// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Minikosár',
        'fuggveny' => 'kosarWidget', // ebben a file-ban definiáljuk a megjelenítést
        'leiras' => 'Kosár widget'
);

function kategoriaWidget($a=array()) {
    if (defined('UNICORN')) return;
    require_once (BASE.'modulok/_aruhaz/osztaly/osztaly_aruhaz.php');
    
    $bos = Aruhaz::peldany();
    include(modulHtmlElem('_aruhaz', 'sidebar'));
    
}
function kosarWidget($a=array()) {
    global $bos;
    if (defined('UNICORN')) return;
    require_once (BASE.'modulok/_aruhaz/osztaly/osztaly_aruhaz.php');
    
    $bos = Aruhaz::peldany();
    if ($a['egyszeru']==1) {
        print '<div class="minicart">';
        include(modulHtmlElem('_aruhaz', 'minikosar'));
        print '</div>';
    } else {
        print '<div class="normalcart">';
        include(modulHtmlElem('_aruhaz', 'widgetkosar'));
        print '</div>';
    }
    
    
}
function kosarWidgetSet($a=array()) {
    if (empty($a)) $a = array('penznemek' => 1, 'egyszeru' => 1);
    ?>
    <div class="control-group">
        <label class="control-label"><?= __f('Pénznemválasztó'); ?></label>
        <select name="a[penznemek]">
            <option value="0" <?= (($a['penznemek']==0)?' selected="selected" ':'')?> ><?= __f('Nincs megjelenítve'); ?></option>
            <option value="1" <?= (($a['penznemek']==1)?' selected="selected" ':'')?> ><?= __f('Megjelenítve'); ?></option>
        </select>
    </div>
    <div class="control-group">
        <label class="control-label"><?= __f('Megjelenítés'); ?></label>
        <select name="a[egyszeru]">
            <option value="0" <?= (($a['egyszeru']==0)?' selected="selected" ':'')?> ><?= __f('Terméklista'); ?></option>
            <option value="1" <?= (($a['egyszeru']==1)?' selected="selected" ':'')?> ><?= __f('Egyszerű'); ?></option>
        </select>
    </div>
    
    <?php
}

function kategoriaWidgetSet($a) {
    ?>
    Nincs beállítási lehetőség
    <?php
}