<?php

function AH_LapCim($cim) {
    $html = Html::peldany();
    $html->helyorzo('lapCim','<a href="'.ARUADMIN.'">Áruház</a> &raquo; ' . $cim);
}
function AH_LapCimAd($cim) {
    $html = Html::peldany();
    $html->helyorzoHozzafuzes('lapCim',' &raquo; ' . $cim);
}
function AH_Gomb($felirat, $param,$szin='orange',  $onclick='') {
    $html = Html::peldany();
    $html->helyorzoHozzafuzes('kezelogombok','<a style="background:'.$szin.'" href="'.AM_URL.'?'.$param.'" onclick="'.$onclick.'" >'.$felirat.'</a>');
    
}

function AH_Html($html) {
    $file = AM_UT.'html/'.$html.'_'.$_SESSION['__nyelv'].'.php';
    if (!file_exists($file)) {
        file_put_contents($file, '<b>Html hiányzik: '.$file.'</b>');
    }
    return $file;
}
