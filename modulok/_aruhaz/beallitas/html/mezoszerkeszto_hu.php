<div class="figyelmezteto" style="text-align: center;">
    <strong>ADATBÁZIS SZINKRONIZÁLÁS</strong>
    <p>A szinkronizálásra kattintva a mezők az adatbázisszerkezetbe kerülnek</p>
    <form method="post" action="<?= AM_URL?>" ><input type="submit" value="INDULHAT A SZINKRONIZÁLÁS" name="szinkronizalas" /></form>
</div>

<table width="100%">
    <tr>
        <th>Név</th>
        <th>Kulcs</th>
        <th>Típus</th>
        <th>Műveletek</th>
    </tr>
    <?php
    if (!empty($mezok)) {
        foreach ($mezok as $sor) {
            ?>
    <tr>
        <td><?= $sor['nev']?></td>
        <td><?= $sor['kulcs']?></td>
        <td><?= $sor['tipus']?></td>
        
        <td class="action">
            <a href="<?= AM_URL?>?m_szerkeszt=<?= $sor['id']; ?>">Szerkeszt</a><br />
            <a href="<?= AM_URL?>?m_torol=<?= $sor['id']; ?>">Töröl</a>
            
        </td>
    </tr>     
            <?php
        }
    }
    ?>
</table>
<br /><br />
<strong>Új mező hozzáadása</strong>
<br /><br />
<form method="post" action="<?= AM_URL?>" class="jNice ahform">
    <input type="hidden" name="m[id]" value="<?= $m['id']; ?>" />
    <p>
        <label>Név</label>
        <input name="m[nev]" value="<?= $m['nev']; ?>" />
    </p>
    <p>
        <label>Kulcs (ascii név, pl leiras)</label>
        <input name="m[kulcs]" value="<?= $m['kulcs']; ?>" />
    </p>
    <p>
        <label>Típus</label>
        <select name="m[tipus]" >
            <option value="INT" <?= (($m['tipus']=='INT')?' selected="selected" ':''); ?>>Egész szám</option>
            <option value="VARCHAR(255)" <?= (($m['tipus']=='VARCHAR(255)')?' selected="selected" ':''); ?>>Rövid szöveg</option>
            <option value="TEXT" <?= (($m['tipus']=='TEXT')?' selected="selected" ':''); ?>>Hosszú leírás</option>
            <option value="FLOAT" <?= (($m['tipus']=='FLOAT')?' selected="selected" ':''); ?>>Lebegőpontos szám</option>
        </select>
    </p>
    <p>
        <label></label>
        <input type="submit" value="Mentés" />
    </p>
</form>