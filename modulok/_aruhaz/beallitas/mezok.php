<?php

// termk mezők kezelése

include AH_Html('keretfejlec');

osztalyTolto('adattablaszerkeszto');
$ak = new adattablaSzerkeszto;


$nyelvek = getRowsByField('bow_nyelvek', 'kulcs');

if (isset($_REQUEST['letrehoz'])) {
    $ak->termekLeiroLetrehoz($_REQUEST['letrehoz']);
    include AH_Html('tabla_letrehozas_sikeres');

}

foreach ($nyelvek as $kulcs => $ertek) {
    if (!$ak->letezik('bos_termekleiras_' . $kulcs)) {
        // tábla nem ltezik

        $hianyzo_tabla = 'bos_termekleiras_' . $kulcs;
        include AH_Html('hianyzotabla_leztrehozas');
    }
}

if (isset($_POST['szinkronizalas'])) {
    // szinkronizálás nyelvenként
    foreach ($nyelvek as $kulcs => $ertek) {
        $ak->szinkronizalas('bos_termekleiras_' . $kulcs);
    }

}

if (isset($_POST['m'])) {
    sqladat($_POST['m'], 'bos_termekmezok');
}
if (isset($_GET['m_szerkeszt'])) {
    $sql = "SELECT * FROM bos_termekmezok WHERE id = " . (int)$_GET['m_szerkeszt'];
    $m = sqluniv3($sql);
}
if (isset($_GET['m_torol'])) {
    sqltorles('bos_termekmezok', (int)$_GET['m_torol']);
    
}


if (!isset($m)) {
    $m = array(
        'id' => 0,
        'nev' => '',
        'kulcs' => '',
        'tipus' => 'TEXT');
}

$sql = "SELECT * FROM bos_termekmezok ORDER BY sorrend ASC";
$mezok = sqluniv4($sql);

include AH_Html('mezoszerkeszto');


include AH_Html('keretfejlab');
