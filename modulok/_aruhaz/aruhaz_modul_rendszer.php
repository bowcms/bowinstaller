<?php

$aruhaz_modulok = array(
    array(
        'nev' => 'CSV Import',
        'url' => 'csv',
        'ikon' => 'database.png'
    ),array(
        'nev' => 'Termékek',
        'url' => 'termekek',
        'ikon' => 'sale.png'
    ),array(
        'nev' => 'Kategóriák',
        'url' => 'kategoriak',
        'ikon' => 'attachment.png'
    ),array(
        'nev' => 'Gyártók',
        'url' => 'gyartok',
        'ikon' => 'edit_page.png'
    ),array(
        'nev' => 'Szállítás',
        'url' => 'szallitas',
        'ikon' => 'phone_book_edit.png'
    ),array(
        'nev' => 'Fizetés',
        'url' => 'fizetes',
        'ikon' => 'yen_currency_sign.png'
    ),array(
        'nev' => 'Rendelések',
        'url' => 'rendeles',
        'ikon' => 'shopping_cart.png'
    ),array(
        'nev' => 'Beállítások',
        'url' => 'beallitas',
        'ikon' => 'sterling_pound_currency_sign.png'
    )
);

Beepulok::futtat('Áruház főoldali menürendszer');
