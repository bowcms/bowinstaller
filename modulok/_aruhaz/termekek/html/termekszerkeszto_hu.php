
<form id="termekSzerkesztes" method="post" onsubmit="return false;" id="termek_adatlap_formok">
<div style="text-align: right;">
<input type="submit" value="Mentés" name="mentes" onclick="$('#termek_adatlap_formok').submit();" />
<input type="submit" value="Mentés és bezárás" name="mentes_bezaras"  onclick="$('#termek_adatlap_formok').submit();" />
<input type="submit" value="Mégsem" name="mentes_megsem"  onclick="$('#termek_adatlap_formok').submit();" />
</div>

<div id="tabs" style="margin-left: 20px;">
  <ul>
    <li><a href="#fragment-1"><span>Törzs adatok</span></a></li>
    <li><a href="#fragment-2"><span>Termék jellemzők</span></a></li>
    <li><a href="#fragment-3"><span>Képek</span></a></li>
    <li><a href="#fragment-4"><span>Kategóriák</span></a></li>
    <li><a href="#fragment-5"><span>Opciók</span></a></li>
    <li><a href="#fragment-6"><span>Űrlapok</span></a></li>
  </ul>
  <div id="fragment-1">
    <?php
    include(AH_Html('termek_torzsadatok'));
    ?>
  </div>
  <div id="fragment-2">
    <?php
    include(AH_Html('termek_jellemzok'));
    ?>
  </div>
  <div id="fragment-3">
    <?php
    include(AH_Html('termek_kepek'));
    ?>
  </div>
  <div id="fragment-4">
    <?php
    include(AH_Html('termek_kategoriak'));
    ?>
  </div>
  <div id="fragment-5">
    <?php
    include(AH_Html('termek_opciok'));
    ?>
  </div>
  <div id="fragment-6">
    <?php
    include(AH_Html('termek_urlapok'));
    ?>
  </div>
</div>
</form>
<script>
$( "#tabs" ).tabs();
</script>









<?php
$html = Html::peldany();
$html->headerStart();
?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<?php

$html->headerStop();