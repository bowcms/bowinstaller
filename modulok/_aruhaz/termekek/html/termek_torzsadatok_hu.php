<table style="width: 100%;">
<tr><td>
<p>
    <label>Cikkszám</label>
    <input name="tt[cikkszam]" id="cikkszam" value="<?= $tt['cikkszam']?>" />
    <input name="tt[id]" id="tid" value="<?= $tt['id']; ?>" type="hidden" />
</p>
<p>
    <label>Ár</label>
    <input name="tt[ar]" value="<?= $tt['ar']?>" />
</p>
<p>
    <label>Akciós ár</label>
    <input name="tt[akcios_ar]" value="<?= $tt['akcios_ar']?>" />
</p>
<p>
    <label>Beszerzési ár</label>
    <input name="tt[beszerzesi_ar]" value="<?= $tt['beszerzesi_ar']?>" />
</p>
<p>
    <label>Készlet</label>
    <input name="tt[keszlet]" value="<?= $tt['keszlet']?>" />
</p>
<p>
    <label>Lefoglalva</label>
    <input name="tt[lefoglalva]" value="<?= $tt['lefoglalva']?>" />
</p>

</td><td>

<p>
    <label>Kiemelt</label>
    <select name="tt[kiemelt]">
        <option <?= (($tt['kiemelt']==0)?' selected="selected" ':'')?> value="0" >Nem</option>
        <option <?= (($tt['kiemelt']==1)?' selected="selected" ':'')?> value="1" >Igen</option>
    </select>
    
</p>
<p>
    <label>Közzétéve</label>
    <select name="tt[publikus]">
        <option <?= (($tt['publikus']==0)?' selected="selected" ':'')?> value="0" >Nem</option>
        <option <?= (($tt['publikus']==1)?' selected="selected" ':'')?> value="1" >Igen</option>
    </select>
    
</p>
<p>
    <label>Adó osztály</label>
    <select name="tt[adoosztaly_id]">
    <?php
    $select = sqluniv4("SELECT * FROM bos_adoosztalyok ORDER by nev ASC");
    foreach ($select as $ops):
    ?>
    <option value="<?= $ops['id']?>" <?= (($ops['id']==$tt['adoosztaly_id'])?' selected="selected" ':'')?> ><?= $ops['nev']; ?></option>
    <?php
    endforeach;
    ?>
    </select>
</p>
<p>
    <label>Gyártó</label>
    <select name="tt[gyarto_id]">
    <?php
    $select = sqluniv4("SELECT * FROM bos_gyartok_".$_SESSION['aruhaz']['termek_nyelv']." ORDER by nev ASC");
    foreach ($select as $ops):
    ?>
    <option value="<?= $ops['id']?>" <?= (($ops['id']==$tt['gyarto_id'])?' selected="selected" ':'')?> ><?= $ops['nev']; ?></option>
    <?php
    endforeach;
    ?>
    </select>
</p>
<p>
    <label>Forgalmazó</label>
    <select name="tt[forgalmazo_id]">
    <?php
    $select = sqluniv4("SELECT * FROM bos_forgalmazok ORDER by nev ASC");
    foreach ($select as $ops):
    ?>
    <option value="<?= $ops['id']?>" <?= (($ops['id']==$tt['forgalmazo_id'])?' selected="selected" ':'')?> ><?= $ops['nev']; ?></option>
    <?php
    endforeach;
    ?>
    </select>
</p>

</td></tr>
</table>