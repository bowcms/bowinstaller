<table width="900" style="margin-left: 20px;width: 880px;">
    <tr>
        <th style="width: 100px;">ID</th>
        <th>Cikkszam</th>
        <th>Név</th>
        <th>Publikus</th>
        <th>Kiemelt</th>
        <th>Készlet</th>
        <th> </th><th> </th>
        
    </tr>
    <?php
    foreach ($lista as $sor):
    ?>
    <tr>
        <td><?= $sor['id'];?></td>
        <td><?= $sor['cikkszam'];?></td>
        <td><?= $sor['nev'];?></td>
        <td><div class="listaGomb kapcsolo_<?= $sor['publikus'];?>"></div></td>
        <td><div class="listaGomb kapcsolo_<?= $sor['kiemelt'];?>"></div></td>
        <td><?= $sor['keszlet'];?></td>
        <td class="action">
            <a href="<?= AM_URL?>?szerkeszt=<?= $sor['id'];?>" class="edit">Szerkeszt</a>
        </td>
        <td class="action">
            <a href="<?= AM_URL?>?torol=<?= $sor['id'];?>" <?= bowConfirm(); ?> class="delete">Töröl</a>
        </td>
    </tr>
    <?php
    endforeach;
    ?>
</table>