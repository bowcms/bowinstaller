<?php


$opcioTabla = 'bos_opciok_'.$_SESSION['aruhaz']['termek_nyelv'];

$sql = "SELECT * FROM ".$opcioTabla." WHERE termek_id = ".$tt['id']." ORDER BY sorrend ASC";
$oRs = sqluniv4($sql);
$nyelv = $_SESSION['aruhaz']['termek_nyelv'];
if(empty($oRs)) {
    print '<strong>'.__f('Nincs még opció hozzáadva a termékhez').'</strong>';
} else {
    foreach ($oRs as $sor) {
        ?>
    <div class="opciosor">
        <p>
            <label><?= __f('Opció neve:',$nyelv );?></label>
            <input name="opcio[<?= $sor['id']?>][nev]" value="<?= $sor['nev']; ?>" />
        </p>
        <p>
            <label><?= __f('Opció leírás:',$nyelv);?></label>
            <textarea name="opcio[<?= $sor['id']?>][leiras]"><?= $sor['leiras']; ?></textarea>
        </p>
        <p>
            <label><?= __f('Opciós ár:',$nyelv);?></label>
            <input name="opcio[<?= $sor['id']?>][ar]" value="<?= $sor['ar']; ?>" /> 
        </p>
        <p>
            <label><?= __f('Sorrend:',$nyelv);?></label>
            <input name="opcio[<?= $sor['id']?>][sorrend]" value="<?= $sor['sorrend']; ?>" />
             
            <select name="opciotorles[<?= $sor['id']?>]" onchange="alert('<?= __f('Mentés után kerül törlésre az opció',$nyelv);?>');$(this).parent().parent().fadeOut();">
                <option value=""><?= __f('-- OPCIÓ TÖRLÉS --',$nyelv);?></option>
                <option value="1"><?= __f('Töröljem ezt az opciót',$nyelv);?></option>
            </select>
        </p>
         
        
    </div>
        <?php
    }
}

?>
<a class="gomb" href="javascript:ujOpcio();">Új opció hozzáadása</a>
<div class="ujopcioBlokk"></div>

<?php

$html = HTML::peldany();
$html->headerStart();
?>
<script type="text/javascript">
    // <!--
    
    var ujOpcioId = 1;
    function ujOpcio() {
        ujOpcioId++;
        $('.ujopcioBlokk').append('<div class="opciosor ujopcio"><p><label><?= __f('Opció neve:',$nyelv );?></label><input name="ujopcio['+ujOpcioId+'][nev]" value="" /></p><p><label><?= __f('Opció leírás:',$nyelv);?></label><textarea name="ujopcio['+ujOpcioId+'][leiras]"></textarea></p><p><label><?= __f('Opciós ár:',$nyelv);?></label><input name="ujopcio['+ujOpcioId+'][ar]" value="" /></p><p><label><?= __f('Sorrend:',$nyelv);?></label><input name="ujopcio['+ujOpcioId+'][sorrend]" value="" /></p></div>');
    }
    // -->
</script>
<?php
$html->headerStop();
