<?php

/**
 * Bow Aruhaz modul
 * Termékek
 * 
 */

AH_LapCim('<a href="'.AM_URL.'">Termékek</a>');
$nyelvek = getRowsByField('bow_nyelvek', 'kulcs');

if (!isset($_SESSION['aruhaz']['termek_nyelv'])) {
    $_SESSION['aruhaz']['termek_nyelv'] = 'hu';
}
if (isset($_GET['term_nyelv'])) {
    $_SESSION['aruhaz']['termek_nyelv'] = $_GET['term_nyelv'];
}
include(AH_Html('nyelvvalaszto'));

$termekTabla = 'bos_termekleiras_'.$_SESSION['aruhaz']['termek_nyelv'];

if (isset($_GET['szerkeszt'])) {
    AH_LapCimAd('Szerkesztő');
    
    $tt = getRows('bos_termekek', (int)$_GET['szerkeszt']);
    $tj = getRows($termekTabla, (int)$_GET['szerkeszt'],' termek_id ');
    $tm = getAll('bos_termekmezok', ' sorrend ');
    include(AH_Html('termekszerkeszto'));
    
    return;
}

// termék lista
$sql = "SELECT * FROM bos_termekek t,$termekTabla l WHERE l.termek_id = t.id ORDER BY l.nev ASC ";
$lista = sqluniv4($sql);

include(AH_Html('termek_lista'));
