<?php

/**
 * Title: Bow Shop Component
 * Author: bowCms
 * Web: http://example.com
 * Description: Full features webshop component
 * Role: system
 * 
 **/

if (defined('SHOP_URI')) {
    $uri = SHOP_URI;
    define('SHOP_URL', BASE_URL.$uri.'/');
} else {
    $uri = 'shop';
    define('SHOP_URL', BASE_URL.$uri.'/');
}

Beepulok::regisztralKomponens('bowShop', 'Bow Shop', $uri);

function bowShop($p1 = '', $p2 = '', $p3 = '', $p4 = ''){
    require_once (BASE.'modulok/_aruhaz/osztaly/osztaly_aruhaz.php');
    global $bos;
    $bos = Aruhaz::peldany();
    $bos->run($p1, $p2, $p3, $p4);
}

adminMenu(__f('Webshop'), 'unicornFomenu', __f('Webshop'), 'shopEditorUnicorn', ' icon-cart');

function shopEditorUnicorn($feladat = '', $alfeladat = '', $id = 0) {
    $mod = modulOsztalyTolto('_aruhaz');
    $mod->kozpont($feladat, $alfeladat, $id);
}

Beepulok::regisztralAjaxHivas('unishopKategoriaMentes', 'uniKatMentes');

function unishopKategoriaMentes() {
    $mod = modulOsztalyTolto('_aruhaz');
    $mod->kozpont('kategoriak', 'ajax', 0);
}
