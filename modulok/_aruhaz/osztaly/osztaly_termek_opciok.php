<?php

class termekOpciok {
    
    
    public $lista = false;
    public $adat = array();
    public $opcio_id;
    public $db;
    
    public function __construct($id, $nyelv = '') {
        if ($nyelv=='') $nyelv = $_SESSION['__nyelv'];
        $sql = "SELECT * FROM bos_opciok_$nyelv WHERE termek_id = ".$id;
        $lista = sqlAssocArr($sql);
        
        if (empty ($lista)) {
            $lista = false;
            return;
        }
        
        foreach ($lista as $sor) {
            $this->lista[] = $sor['id'];
            $this->adat[$sor['id']] = $sor;
        }
        
    }
    public function set($id) {
        
        $this->opcio_id = $id;
        
    }
    public function setDb($db) {
        $this->db = $db;
    }
    public function nev() {
        return $this->adat[$this->opcio_id]['nev'];
    }
    public function ar() {
        return ($this->adat[$this->opcio_id]['ar']);
        
    }
    public function konvertaltAr ($mod = 'netto') {
        global $bos;
        
        $ar = $this->adat[$this->opcio_id]['ar'];
        $ar = $bos->ARAK->arKonverio($mod, $ar, $bos->KONF->beallitas('Áruház.AlapértelmezettPénznem'));
        return $ar;
    }
    public function opcioNev($id) {
         if (!isset($this->adat[$id])) return false;
         return $this->adat[$id]['nev'];
    }

}