<?php

class termekKepek {
    
    public $termek_id;
    public $kepek = false;
    
    public function __construct($termek_id = 0) {
        $this->termek_id = $termek_id;
        $sql = "SELECT * FROM bos_termekkepek WHERE termek_id = ".$termek_id." ORDER by sorrend ASC";
        $this->kepek = sqlAssocArr($sql);
        
    }
    
    public function foKep($x = 0, $y = 0) {
        if (isset($this->kepek[0])) {
            if (file_exists(BASE.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file'].'.jpg')) {
                $kep = BASE_URL.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file'].'.jpg';
                /*
                if ($x > 0 and $y > 0) {
                    $kep = BASE.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file']."_".$x."_$y".'.jpg';
                    if (file_exists($kep)) {
                        return BASE_URL.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file']."_".$x."_$y".'.jpg';
                    } else {
                        osztalyTolto('kepek');
                        $img = new Image(BASE.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file'].'.jpg');
                        $img->resize($x,$y, 'crop');
                        
                        $siker = $img->save($this->kepek[0]['file']."_".$x."_$y",BASE.'feltoltesek/term/'.$this->termek_id.'/');
                        
                        if($siker) return BASE_URL.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file']."_".$x."_$y".'.jpg';
                    }
                }
                */
                return BASE_URL.'feltoltesek/term/'.$this->termek_id.'/'.$this->kepek[0]['file'].'.jpg';
            }
            
        }
        return BASE_URL.'modulok/_aruhaz/images/noimg_f.jpg';
    }
}