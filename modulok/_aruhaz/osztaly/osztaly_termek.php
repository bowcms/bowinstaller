<?php

/**
 * osztaly_termek.php
 * Egy termék leíró osztály
 */

class Termek
{
    
    public $id = '';
    public $cikkszam = '';
    public $ar = '';
    public $akcios_ar = '';
    public $akcio = '';
    public $ar_penznem = '';
    public $beszerzesi_ar = '';
    public $beszerzesi_ar_penznem = '';
    public $kiemelt = '';
    public $publikus = '';
    public $adoosztaly_id = '';
    public $gyarto_id = '';
    public $forgalmazo_id = '';
    public $keszlet = '';
    public $lefoglalva = '';
    public $jellemzok;
    public $nyelv = '';
    public $termekKeszlet = 0;
    public $opciok, $adatlap;
    
    public function __construct($id = false, $nyelv = '')
    {
        
        if ($nyelv == '')
            $nyelv = $_SESSION['__nyelv'];
        $this->nyelv = $nyelv;
        
        if ($id) {

            
            $sql = "SELECT * FROM bos_termekek WHERE id = $id LIMIT 1";
            
            $rs = sqlAssocRow($sql);
            
            
            if (!empty($rs)) {

                // törzsadatok
                
                $this->id = $id;
                foreach ($rs as $kulcs => $ertek) {
                    $this->$kulcs = $ertek;
                }
                $this->termekKeszlet = $this->keszlet-$this->lefoglalva;
                // jellemzők
                
                require_once (dirname(__file__) . '/osztaly_termek_jellemzok.php');
                $this->jellemzok = new termekJellemzok($id, $nyelv);
                
                // képek
                require_once (dirname(__file__) . '/osztaly_termek_kepek.php');
                $this->kepek = new termekKepek($id, $nyelv);
                
                require_once (dirname(__file__) . '/osztaly_termek_opciok.php');
                $this->opciok = new termekOpciok($id, $nyelv);
                
                require_once (dirname(__file__) . '/osztaly_termek_form.php');
                $this->adatlap = new termekForm($id);
                
                
                
                 
                
                
            } else {
                return false;
            }
        }
    }
    
    public function kategoriaKereso() {
        if ($this->id == 0) return false;
        $nyelv = $_SESSION['__nyelv'];
        $sql = "SELECT kategoria_id FROM bos_termekxkategoria WHERE nyelv = '$nyelv' AND termek_id = ".$this->id;
        $rs = sqlAssocRow($sql);
        $katId = $rs['kategoria_id'];
        global $bos;
        
        $currentKat = $bos->KAT->kategoria($katId);
        $utvonal = $bos->KAT->utvonal($katId);
        
        $utvonal[] = array('nev' => $currentKat->getNev(), 'id' => $currentKat->getId(), 'link' => $currentKat->getUtvonal());
        
        return $utvonal;
        
    }
    public function pontosAr() {
        global $bos;
        
        return $bos->ARAK->arCalkulacio('netto',$this);
    }
    public function arDiv($class, $mod = 'netto')
    {
        global $bos;
        
        $ar = $bos->ARAK->arCalkulacio($mod,$this);
        $str = $bos->ARAK->formazas($ar);
        
        return "<div class=\"$class\">".$str."</div>";
    }
    
    public function kosarHozzaadasHtml() {
        global $bos;
        $bos->KOSAR->kosarbaRakHtml($this);    
    }
    
    public function vanOpcio() {
        if (!$this->opciok->lista) return false; else return true;
    }
    
    public function opcioSelectHtml() {
        $lista = $this->opciok->lista;
        $termek = $this;
        if ($lista) {
            include(modulHtmlElem('_aruhaz', 'opcio_select'));
        }
    }
    
    public function vanAdatlap() {
        if ($this->adatlap->adatlapId !=0) {
            return true;
        } else {
            return false;
        }
    }
    public function adatlapHtml() {
        if ($this->vanAdatlap()) {
            $inputok = $this->adatlap->urlapBetoltes();
            if (!empty($inputok)) {
                include(modulHtmlElem('_aruhaz', 'termek_urlap'));
            }
        }
    }
    public function keszlet() {
        return $this->keszlet - $this->lefoglalva;
    }
    
    public function getUtvonal() {
        
        return strToUrl($this->jellemzok->get('nev')).'_p'.$this->id.'.html';
    }
    public function setId($id)
    {
        $this->id = $id;
    }
    public function getId()
    {
        return $this->id;
    }
    public function setCikkszam($cikkszam)
    {
        $this->cikkszam = $cikkszam;
    }
    public function getCikkszam()
    {
        return $this->cikkszam;
    }
    public function setAr($ar)
    {
        $this->ar = $ar;
    }
    public function getAr()
    {
        return $this->ar;
    }
    public function setAkcios_ar($akcios_ar)
    {
        $this->akcios_ar = $akcios_ar;
    }
    public function getAkcios_ar()
    {
        return $this->akcios_ar;
    }
    public function setAkcio($akcio)
    {
        $this->akcio = $akcio;
    }
    public function getAkcio()
    {
        return $this->akcio;
    }
    public function setAr_penznem($ar_penznem)
    {
        $this->ar_penznem = $ar_penznem;
    }
    public function getAr_penznem()
    {
        return $this->ar_penznem;
    }
    public function setBeszerzesi_ar($beszerzesi_ar)
    {
        $this->beszerzesi_ar = $beszerzesi_ar;
    }
    public function getBeszerzesi_ar()
    {
        return $this->beszerzesi_ar;
    }
    public function setBeszerzesi_ar_penznem($beszerzesi_ar_penznem)
    {
        $this->beszerzesi_ar_penznem = $beszerzesi_ar_penznem;
    }
    public function getBeszerzesi_ar_penznem()
    {
        return $this->beszerzesi_ar_penznem;
    }
    public function setKiemelt($kiemelt)
    {
        $this->kiemelt = $kiemelt;
    }
    public function getKiemelt()
    {
        return $this->kiemelt;
    }
    public function setPublikus($publikus)
    {
        $this->publikus = $publikus;
    }
    public function getPublikus()
    {
        return $this->publikus;
    }
    public function setAdoosztaly_id($adoosztaly_id)
    {
        $this->adoosztaly_id = $adoosztaly_id;
    }
    public function getAdoosztaly_id()
    {
        return $this->adoosztaly_id;
    }
    public function setGyarto_id($gyarto_id)
    {
        $this->gyarto_id = $gyarto_id;
    }
    public function getGyarto_id()
    {
        return $this->gyarto_id;
    }
    public function setForgalmazo_id($forgalmazo_id)
    {
        $this->forgalmazo_id = $forgalmazo_id;
    }
    public function getForgalmazo_id()
    {
        return $this->forgalmazo_id;
    }
    public function setKeszlet($keszlet)
    {
        $this->keszlet = $keszlet;
    }
    public function getKeszlet()
    {
        return $this->keszlet;
    }
    public function setLefoglalva($lefoglalva)
    {
        $this->lefoglalva = $lefoglalva;
    }
    public function getLefoglalva()
    {
        return $this->lefoglalva;
    }

}
