<?php

class Termekek {
    
    private $termekLista;
    private $lapozoKulcs;
    
    public function __construct() {
        require_once(dirname(__FILE__).'/osztaly_termek.php');
    }
    
    // terméklisták készítése
    public function betoltes($id) {
        $sql = "SELECT id FROM bos_termekek WHERE id = ".(int)$id;
        $rs = sqlAssocRow($sql);
        if (!isset($rs['id'])) {
            return false;
        } else {
            return new Termek($id);
        }
    }
    // Kategória lista
    
    public function kategoriaTermekek($id, $nyelv = '') {
        if ($nyelv=='') $nyelv = $_SESSION['__nyelv'];
        $sql = "SELECT *, t.id as tid FROM bos_termekek t, bos_termekxkategoria x WHERE x.nyelv = '$nyelv' AND x.kategoria_id = $id 
        AND x.termek_id = t.id ORDER BY x.sorrend ASC";
        $this->termekLista = sqlAssocArr($sql);
        foreach ($this->termekLista as $k => $v) $this->termekLista[$k]['id'] = $v['tid'];
        return $this->termekLista;
    }
    
    
    // termélklista lapozó
    public function termekListaLapozoStart($kulcs) {
        global $bos;
        $this->lapozoKulcs = base64_encode(md5($kulcs));
        if (!isset($_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs])) {
            $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs] = array('start' => 0, 'limit' => 10);
        }
        $this->termekListaLapozoLapszam();
        $start = $bos->URL->termekLapozoAktivLap;
        
        if (!$start) {
            $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['start'] = 0;
        } else {
            $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['start'] = $start-1;
        }
    }
    
    public function termekListaAktualisLapszam() {
        return $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['start']+1;
    }
    public function termekListaLapozoLapszam() {
        if (empty($this->termekLista)) return 0;
        
        $osszTermek = count($this->termekLista);
        $limit = $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['limit'];
        $lapszam = floor($osszTermek/$limit)+1;
        if (($osszTermek % $limit)==0) {
            $lapszam--;
        }
        $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['lapszam'] = $lapszam;
        return $lapszam;
    }
    
    public function termekListaLapozoTermekszam($limit) {
        $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['limit'] = $limit;
    }
    public function termekListaLapozoElsolapUrl() {
        global $bos;
        $url = $bos->URL->aktivUrl;
        $jelzo = $bos->KONF->beallitas('Termékek.TerméklistaLapozóUrlTag', 'page-');
        return $url.$jelzo.'1.html';
    }
    public function termekListaLapozoUtolsoLapUrl() {
        global $bos;
        $url = $bos->URL->aktivUrl;
        $jelzo = $bos->KONF->beallitas('Termékek.TerméklistaLapozóUrlTag', 'page-');
        return $url.$jelzo.$this->termekListaLapozoLapszam().'.html';
    }
    public function termekListaLapozoLink($lap) {
        global $bos;
        $url = $bos->URL->aktivUrl;
        $jelzo = $bos->KONF->beallitas('Termékek.TerméklistaLapozóUrlTag', 'page-');
        return $url.$jelzo.$lap.'.html';
    }
    
    public function szurtTermekLista() {
        global $bos;
        
        $ret = array();
        $start = $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['start']*$_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['limit'];
        if (!isset($this->termekLista[$start])) {
            $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['start'] = 0;
            $start = 0;
        }
        
        
        
        
        for ($i = $start; $i < $start + $_SESSION['aruhaz']['termekListaLapozo'][$this->lapozoKulcs]['limit']; $i++ ) {
            if (isset($this->termekLista[$i])) {
                $ret[] = new Termek ($this->termekLista[$i]['id']);
            }
            
        }
        
        return $ret;
        
    }
    
    
    
    
    
}

