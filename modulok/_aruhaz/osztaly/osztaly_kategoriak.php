<?php

/**
 * osztaly_kategoriak.php
 * 
 * kategóriák leképezése, kezelése
 */

class Kategoriak
{
    
    public $kategoriaFa = array();
    public $kategoriaLista = array();
    
    public function __construct()
    {
        $this->kategoriaKibonto(0);
    }
    
    public function azUrl($id) {
        $ut = strToUrl($this->kategoriaLista[$id]['nev']).'_c'.$id.'.html';
        return $ut;
    }
    
    public function kategoriaKibonto($szulo, $szint = 0, $nyelv = '')
    {
        $szint++;
        $session = Munkamenet::peldany();
        if ($nyelv == '') {
            $nyelv = $session->getVar("__nyelv");
        }
        
        $sql = "SHOW TABLES LIKE 'bos_kategoriak_". $_SESSION['__nyelv']."'";
        $texists = sqluniv3($sql);
        if (empty($texists)) $this->katTablaLetrehozas($_SESSION['__nyelv']);
        
        $sql = "SELECT * FROM bos_kategoriak_" . $_SESSION['__nyelv'] .
            ' WHERE szulo = ' . $szulo . ' ORDER BY sorrend ASC';
        $rs = sqluniv4($sql);

        
        if (!empty($rs))
            foreach ($rs as $sor) {
                $this->kategoriaLista[$sor['id']] = $sor;
                $this->kategoriaFa[] = array(
                    'szint' => $szint,
                    'adat' => $sor
                );
                
                $this->kategoriaKibonto($sor['id'], $szint, $nyelv);
            }
    }
    
    
    public function katTablaLetrehozas($nyelv) {
        $sql = "CREATE TABLE bos_kategoriak_$nyelv LIKE bos_kategoriak_hu";
        sqluniv($sql);
        $sql = "INSERT bos_kategoriak_$nyelv SELECT * FROM bos_kategoriak_hu";
        sqluniv($sql);
    }
    
    public function subKategoriak($id) {
        $ret = array();
        foreach ($this->kategoriaFa as $sor) if ($sor['adat']['szulo']==$id) $ret[] = $sor;
        return $ret;
    }
    public function kategoria($id) {
        return new Kategoria($id);
    }
    public function utvonal($id, $ret = array()) {
        $ret = array();
        if (!isset($this->kategoriaLista[$id])) {
            return $ret;
        }
        while ($this->kategoriaLista[$id]['szulo']!=0) {
            $ret[] = array('nev' => $this->kategoriaLista[$id]['nev'], 'link' => $this->azUrl($id));
            $id = $this->kategoriaLista[$id]['szulo'];
        }
        $ret[] = array('nev' => $this->kategoriaLista[$id]['nev'], 'link' => $this->azUrl($id));
        $ret = array_reverse($ret);
        return $ret;
    }
}

class Kategoria {
    
    private $id;
    private $szulo;
    private $nev;
    private $leiras;
    private $kep;
    private $sorrend;
    private $termekSzam = NULL;
    private $sajatTermekSzam = NULL;
    
    public function __construct($id ,$nyelv = '') {
        if ($id > 0) $this->load($id,$nyelv);
    }
    public function load($id, $nyelv) {
        if ($nyelv=='') $nyelv = $_SESSION['__nyelv'];
        $sql = "SELECT * FROM bos_kategoriak_$nyelv WHERE id = $id LIMIT 1";
        $rs = sqlAssocRow($sql);
        foreach ($rs as $k => $v) $this->$k = $v;
    }
    
    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    
    public function getSzulo() {
        return $this->szulo;
    }
    public function setSzulo($szulo) {
        $this->szulo = $szulo;
    }
    public function getUtvonal() {
        return strToUrl($this->nev).'_c'.$this->id.'.html';
    }
    public function getNev() {
        return $this->nev;
    }
    public function setNev($nev) {
        $this->nev = $nev;
    }
    public function getLeiras() {
        return $this->leiras;
    }
    public function setLeiras($leiras) {
        $this->leiras = $leiras;
    }
    public function getKep() {
        if ($this->kep=='') {
            return BASE_URL.'modulok/_aruhaz/images/noimg_f.jpg';
        }
        return $this->$kep;
    }
    public function setKep($kep) {
        $this->kep = $kep;
    }
    public function getSorrend() {
        return $this->$sorrend;
    }
    public function setSorrend($sorrend) {
        $this->sorrend = $sorrend;
    }
    
    public function save($nyelv = '') {
        if ($nyelv=='') $nyelv = $_SESSION['__nyelv'];
        $a = array(
            'id' => $this->id,
            'szulo' => $this->szulo,
            'nev' => $this->nev,
            'leiras' => $this->leiras,
            'sorrend' => $this->sorrend,
            'kep' => $this->kep
            
        );
        sqladat($a, 'bos_kategoriak_'.$nyelv);
    }
    
    // computed values
    
    public function termekSzam($nyelv = '') {
        if ($nyelv == '') $nyelv = $_SESSION['__nyelv'];
        if ($this->termekSzam == NULL) {
            $this->termekSzam = 0;
            $this->termekSzamitas($this->id, $nyelv);
        }
        return $this->termekSzam;
    }
    public function sajatTermekSzam($nyelv = '') {
        if ($nyelv == '') $nyelv = $_SESSION['__nyelv'];
        if ($this->sajatTermekSzam == NULL) {
            $this->sajatTermekSzam = 0;
            $this->sajatTermekSzamitas($this->id, $nyelv);
        }
        return $this->sajatTermekSzam;
    }
    
    public function sajatTermekSzamitas($szulo, $nyelv) {
        if ($szulo == '') $szulo = 0;
        
        $sql = "SELECT COUNT(*) as ossz FROM bos_termekxkategoria x, bos_termekek t WHERE x.kategoria_id = $szulo AND x.termek_id = t.id AND t.publikus = 1";
        $rs = sqlAssocRow($sql);
        $this->sajatTermekSzam = $rs['ossz'];
    }
    
    public function termekSzamitas($szulo, $nyelv) {
        if ($szulo == '') $szulo = 0;
        $sql = "SELECT id FROM bos_kategoriak_$nyelv WHERE szulo = $szulo";
        $rs = sqlAssocArr($sql);
        if (!empty($rs))
        foreach($rs as $sor) {
            $this->termekSzamitas($sor['id'], $nyelv);
        }
        $sql = "SELECT COUNT(*) as ossz FROM bos_termekxkategoria x, bos_termekek t WHERE x.kategoria_id = $szulo AND x.termek_id = t.id AND t.publikus = 1";
        $rs = sqlAssocRow($sql);
        $this->termekSzam += $rs['ossz'];
    }
    
    
    
}

