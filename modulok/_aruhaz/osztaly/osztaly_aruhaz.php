<?php

/**
 * osztaly_aruhaz.php
 * 
 * Bow �ruh�z modul k�zponti oszt�ly
 * 
 */

class Aruhaz {
    
    public static $peldany = NULL;
    
    public $KAT = NULL; // kateg�ria oszt�ly
    public $ARAK = NULL;  // ad� oszt�ly
    public $GYARTO = NULL; // gy�rt�k oszt�ly
    public $TERM = NULL; // term�k oszt�ly
    public $KOSAR = NULL; // kos�r oszt�ly
    public $URL = NULL; // url feldolgoz� oszt�ly
    public $KONF = NULL; // konfigur�ci�s oszt�ly
    
    
    private function __construct($live = true) {
        // konstruktor ide
        define('SHOPBASE', dirname(dirname(__FILE__)).'/');
        define('ARUHAZ_FOOLDAL' , 'online-shop');
        global $shopKonf;
        //
        // konfigur�ci�s oszt�ly 
        include_once (SHOPBASE.'osztaly/osztaly_konfiguracio.php');
        $this->KONF = new Konfiguracio();
        $shopKonf = $this->KONF;
        
        // kateg�ria oszt�ly 
        include_once (SHOPBASE.'osztaly/osztaly_kategoriak.php');
        $this->KAT = new Kategoriak();
        
        if ($live) {
            // url feldolgoz�
            include_once (SHOPBASE.'osztaly/osztaly_bolturl.php');
            $this->URL = new BoltURL();
        }
        
        include_once (SHOPBASE.'osztaly/osztaly_termekek.php');
        $this->TERM = new Termekek();
        
        include_once (SHOPBASE.'osztaly/osztaly_arak_es_ado.php');
        $this->ARAK = new ArakEsAdo();
        
        include_once (SHOPBASE.'osztaly/osztaly_kosar.php');
        $this->KOSAR = new Kosar();
        
        
        // term�kek
        
        
    }
    public static function peldany() {
        if (self::$peldany == NULL) {
            self::$peldany = new aruhaz();
        }
        return self::$peldany;
    }
    
    public function run($p1 = '', $p2 = '', $p3 = '', $p4 = '') {
        $this->URL->urlFeldolgozas($p1, $p2, $p3, $p4);
        $parancs = $this->URL->parancs;
        if (empty($parancs)) {
            $metodus = 'aruhazKategoriaOldal';
            $id = 0;
        } else {
            $metodus = $this->URL->metodus;
            $id = $this->URL->id;
        }
        require_once(BASE.'modulok/_aruhaz/aruhazOldalak.php');
        
        $o = new aruhazOldalak();
        $o->$metodus($id);
    }
    
}