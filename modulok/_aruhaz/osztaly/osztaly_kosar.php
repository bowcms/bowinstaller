<?php

class Kosar {
    public static $peldany = NULL;
    public $tartalom = array();
    public $termekSzam = 0;
    public $osszAr = 0;
    public $arak = array();
    
    public $jelenElem = array();
    public $jelenKulcs = '';
    public $termek = null;
    public $kosarAjanlatos = false;
    public $kosarTermekArBrutto = false;
    public $kosarTermekArNetto = false;
    public $kosarTermekArLatszik = true;
    
    public $opcioIndex = 0;
    
    public static function peldany() {
        
        if (self::$peldany == NULL) {
            self::$peldany = new aruhaz();
        }
        return self::$peldany;
    }
    public function __construct(){
        $this->tartalom = ((isset($_SESSION['bos']['kosar']['tartalom']))?$_SESSION['bos']['kosar']['tartalom']:array());
        
        
    }
    
    public function kosarReset() {
        global $bos;
        reset($this->tartalom);
        $this->osszAr = 0;
        
        $this->kosarTermekArLatszik = $bos->KONF->beallitas('Termékek.termekArLatszik', 1);
        $termekArCsoportok = $bos->KONF->beallitas('Termékek.termekArCsoportok', '');
        if ($termekArCsoportok != '') {
            $tag = Tagok::peldany();

            $exp = explode(',', $termekArCsoportok);
            $vanJog = false;
            foreach ($exp as $csoportId) {
                if ($csoportId == $tag->adat['csoport_id'])
                    $vanJog = true;
            }
            $termekArLatszik = $vanJog;
        }
        $this->kosarTermekArNetto = $bos->KONF->beallitas('Kosár.termekArNetto', 1);
        $this->kosarTermekArBrutto = $bos->KONF->beallitas('Kosár.termekArBrutto', 0);
        
    }
    public function kosarVanElem() {
        $jelenElem = current($this->tartalom);
        
        if (isset($jelenElem['kulcs'])) {
            $this->jelenElem = $jelenElem;
            $this->termek = new Termek($this->jelenElem['id']);
            $this->jelenKulcs = $this->jelenElem['kulcs'];
            next($this->tartalom);
            return true;
        } else {
            return false;
        }
    }
    public function kosarTermekAr() {
        if (isset($this->arak[$this->jelenKulcs])) return $this->arak[$this->jelenKulcs];
        $this->arak[$this->jelenKulcs] = $this->jelenTermek->getPontosAr()*$this->kosarTermekDb();
        return $this->arak[$this->jelenKulcs];
    }
    public function kosarUres() {
        
        if (empty($this->tartalom)) {
            return true;
        }
        return false;
    }
    public function kosarVanOpcio() {
        
        if( isset($this->jelenElem['opciok']))
            if (!empty($this->jelenElem['opciok'])) {
                
                reset($this->jelenElem['opciok']);
                return true;
            }
        return false;
    }
    public function kosarTermekOpcio() {
        
        if ($opcio = each($this->jelenElem['opciok'])) {
            
            
            $opcio_id = $opcio['key'];
            
            $this->termek->opciok->set($opcio_id);
            $this->termek->opciok->setDb($opcio['value']);
            return $this->termek->opciok;
        } else {
            return false;
        }
    }
    public function kosarTermekDb() {
        return $this->jelenElem['db'];
    }
    public function kosarJelenTermekAr() {
        global $bos;
        
        $ar = $this->termek->pontosAr();
        
        if (!empty($this->jelenElem['opciok'])) {
            foreach ($this->jelenElem['opciok'] as  $opcio_id => $db) {
                $this->termek->opciok->set($opcio_id);
                $ar += $bos->ARAK->arKonverio('netto',$this->termek->opciok->ar())*$db;
            }
            
        }
        
        if ($this->kosarVanForm()) {
            $ar = false;
            $this->ajanlatos = true;
        } else {
            
        }
        
        return $ar;
    }
    public function kosarVanForm() {
        if (isset ($this->jelenElem['form'])) {
            if (!empty ($this->jelenElem['form'])) {
                $this->kosarAjanlatos = true;
                return true;
            }
        } else {
            return false;
        }
    }
    
    public function miniKosarWg() {
        global $bos, $arak;
        $lista = $this->kosarTartalom();
        if (empty($this->tartalom)) {
            echo __f('Az Ön kosara még üres');
            return;
        }
        echo __f('Termékek száma: ').'<span>'.$this->termekSzam().'</span><br />';
        $arak = new ArakEsAdo();
        echo __f('Összár: ').'<span>'.$arak->formazas($this->termekOsszar()).'</span><br />';
    }
    
    public function termekOsszar() {
        $ar = 0;
        $this->kosarReset();
        while ($this->kosarVanElem()) {
            
            $ar += $this->kosarJelenTermekAr()*$this->jelenElem['db'];
        }
        
        
        return $ar;
    }
    public function termekSzam() {
        $this->termekSzam = 0;
        foreach ($this->tartalom as $sor) $this->termekSzam += $sor['db'];
        return $this->termekSzam;
    }
    
    public function hozzaadas($termekId, $mennyiseg = 1, $opcioId = false, $termekForm = false) {
        if ($termekId=='') return false;
        // ha már benne van, akkor hozzáadunk, amennyiben nem termekFormos
        $kulcs = $termekId.(($opcioId!==false)?serialize($opcioId):'').((is_array($termekForm))?md5(serialize($termekForm)):'');
        $kulcs = md5($kulcs);
        // TODO: termék elérhetőségének vizsgálata
        if (isset($this->tartalom[$kulcs])) {
            $this->tartalom[$kulcs]['db'] += $mennyiseg;
        } else {
            $tartalom = array('id' => $termekId, 'db' => $mennyiseg);
            if ($opcioId) {
                // nulla darabszámos törlése
                foreach ($opcioId as $k => $v) if($v == 0) unset($opcioId[$k]);
                if (!empty($opcioId))
                    $tartalom['opciok'] = $opcioId;   
            }
            
            if ($termekForm) $tartalom['form'] = $termekForm;
            $tartalom['kulcs'] = $kulcs;
            $this->tartalom[$kulcs] = $tartalom;
            
        }
        
    }
    public function torles($kulcs) {
        
        if (isset($this->tartalom[$kulcs])) {
            unset($this->tartalom[$kulcs]);
        } 
        
    }
    public function __destruct(){
        $_SESSION['bos']['kosar']['tartalom'] = $this->tartalom;
    }
    public function kosarTartalom() {
        return $this->tartalom;
    }
    
    public function kosarbaRakHtml($termek) {
        global $bos;
        $termekKosar = $bos->KONF->beallitas('Termékek.terméklapKosarlatszik', 1);
        $tobbetIsHozzaadhat =  $bos->KONF->beallitas('Termékek.terméklapKosarTobbetishozzaadhat', 1);
        $keszletSzamit =  $bos->KONF->beallitas('Termékek.terméklapKosarKeszletszamit', 1);
        $nincsRaktaronErdeklodhet =  $bos->KONF->beallitas('Termékek.termeklapNincsRaktaronErdeklodhet', 1);
        
        if(!$termekKosar) return;
        $keszlet = $termek->keszlet - $termek->lefoglalva;
        if ($keszlet<1 and $keszletSzamit) {
            // érdeklődés form
            if (isset($_POST['termeklapForm'])) {
                if (isEmail($_POST['termeklapForm']['email'])) {
                    $level = Levelezo::peldany();
                    $level->sablon('aruhaz_erdeklodes');
                    $level->valaszEmail($_POST['termeklapForm']['email']);
                    $level->valaszNev($_POST['termeklapForm']['nev']);
                    $level->helyettesito(array('termek' => $termek->jellemzok->get('nev').' :: '.$termek->getCikkszam(), 'email' =>$_POST['termeklapForm']['email'],
                                                'nev' => $_POST['termeklapForm']['nev'], 'megjegyzes' => $_POST['termeklapForm']['uzenet'] ));
                    $level->kuldes($bos->KONF->beallitas('Aruhaz.EladóEmailCím', 'ivan@zente.org'));
                    
                    include(modulHtmlElem('_aruhaz', 'termeklap_erdeklodoform_sikeres_kuldes'));
                } else {
                    $erdeklodesHiba = __f('Helytelen E-mail cím');
                }
                
            }
            include(modulHtmlElem('_aruhaz', 'termeklap_erdeklodoform'));
        } else {
            if ($tobbetIsHozzaadhat) {
                include(modulHtmlElem('_aruhaz', 'termeklap_kosar_mennyiseg_hozzaad'));
            } else {
                include(modulHtmlElem('_aruhaz', 'termeklap_kosar_hozzaad'));
            }
        }
    }
}