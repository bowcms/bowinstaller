<?php

class ArakEsAdo
{


    public $ado;
    public $penznemAdat;
    public $arfolyamRss = 'http://themoneyconverter.com/rss-feed/EUR/rss.xml';
    public $arfolyamok = NULL;
    public $penzKod;

    public function __construct()
    {
        global $shopKonf;
        $sql = "SELECT * FROM bos_adoosztalyok ";
        $rs = sqlAssocArr($sql);
        foreach ($rs as $sor) {
            $this->ado[$sor['id']] = $sor;
        }
        $penzKod = $shopKonf->beallitas('Áruház.AlapértelmezettPénznem', 'HUF');
        if (isset($_SESSION['aruhaz']['penznem'])) {
            $penzKod = $_SESSION['aruhaz']['penznem'];
        }
        $this->penzKod = $penzKod;
        
    }
    public function arKonverio($mod, $ar, $ar_penznem = 'HUF') {
        global $shopKonf;
        // akciós
        
        
        //adó
        if ($mod == 'brutto') {
            $ado = $this->ado[$termek->adoosztaly_id];
            switch ($ado['szamitas']) {
                case 'plussz_szazalek':
                    
                    $ar = $ar + ($ar / 100) * $ado['modosito'];
                    
                    break;
            }
        }
        // pénznem
        $pn = $ar_penznem;
        $aruhazPn = $this->penzKod;
         
        if ($pn == $aruhazPn ) {
            
            return $ar;
        }
        
        // először EUR-ra váltunk
        if ($pn != 'EUR') {
            $rata = $this->arfolyamFrissites($pn);
            $ar = $ar/$rata;    
        }
        
        // majd váltjuk adott devizára
        if ($aruhazPn != 'EUR') {
            $rata = $this->arfolyamFrissites($aruhazPn);
            $ar = $ar * $rata;
        }
        
        return $ar;
        
    }
    public function arCalkulacio($mod, $termek)
    {
        
        global $shopKonf;
        // akciós
        
        $ar = $termek->ar;
        
        if ($termek->akcio == 1) {
            $ar = $termek->akcios_ar;
        }
        //adó
        if ($mod == 'brutto') {
            $ado = $this->ado[$termek->adoosztaly_id];
            switch ($ado['szamitas']) {
                case 'plussz_szazalek':
                    
                    $ar = $ar + ($ar / 100) * $ado['modosito'];
                    
                    break;
            }
        }
        
        // pénznem
        $pn = $termek->ar_penznem;
        $aruhazPn = $this->penzKod;
         
        if ($pn == $aruhazPn ) {
        
            return $ar;
        }
        
        // először EUR-ra váltunk
        if ($pn != 'EUR') {
            $rata = $this->arfolyamFrissites($pn);
            
            $ar = $ar/$rata;    
        }
        
        // majd váltjuk adott devizára
        if ($aruhazPn != 'EUR') {
            $rata = $this->arfolyamFrissites($aruhazPn);
            $ar = $ar * $rata;
        }
        
        return $ar;
        
    }
    
    public function formazas($ar) {
        $penzKod = $this->penzKod;
        $formatum = $this->formatumBetoltes($penzKod);
        $ar = number_format($ar,$formatum['decimal'],$formatum['decpont'],$formatum['ezrespont']);
        
        $str = '';
        if ($formatum['elotag'] == 0) {
            $str = $formatum['szimbolum'].' ';
        }
        $str .= $ar.' ';
        if ($formatum['elotag'] == 1) {
            $str .= $formatum['szimbolum'];
        }
        return $str;
    }
    
    public function arfolyamFrissites($kod) {
        $sql = "SELECT * FROM bos_arfolyamok WHERE penznem = '$kod' AND frissitve > ".(time()-24*90*90);
        $rs = sqlAssocRow($sql);
        if (isset($rs['rata'])) return $rs['rata'];
        
        if ($this->arfolyamok == NULL) {
            $this->arfolyamok = 1;
            $link = $this->arfolyamRss;
            
            $xmlstr =cUrl($link);
             
            $xml = new SimpleXMLElement($xmlstr);
            if(!$xml) return;
            foreach ($xml->channel[0]->item as $sor) {
                $kod = (string)$sor->title."\n";
                $exp = explode('/', $kod);
                $kod = $exp[0];
                
                $str = (string)$sor->description[0];
                preg_match('%1 Euro = ([0-9]+\.[0-9]+) (.*)$%', $str, $adat);
                if (!isset($adat[1]) or !isset($adat[2])) continue;
                if (trim($adat[1])=='' or trim($adat[2])=='') continue;
                $nev = $adat[2];
                $arfolyam = $adat[1];
                $sql = "SELECT id FROM bos_arfolyamok WHERE penznem = '$kod' LIMIT 1";
                $rs = sqlAssocRow($sql);
                $id = ((isset($rs['id']))?$rs['id']:0);
                $a = array(
                    'id' => $id,
                    'nev' => $nev,
                    'penznem' => $kod,
                    'rata' => $arfolyam,
                    'frissitve' => time(),
                    'bazis' => 'EUR'
                );
                sqladat($a, 'bos_arfolyamok');
                
            }
            
        }
    }
    

    public function formatumBetoltes($kod)
    {
        if (!empty($this->penznemAdat[$kod])) return $this->penznemAdat[$kod];
        $sql = "SELECT * FROM bos_penznemek WHERE kod = '$kod' LIMIT 1";
        $penznemRs = sqlAssocRow($sql);
        if (!empty($penznemRs)) {
            $this->penznemAdat[$kod] = $penznemRs;
            return $penznemRs;
        } else {
            return array(
                'elotag' => 0,
                'szimbolum' => $kod,
                'decimal' => 2,
                'decpont' => '.',
                'ezrespont' => ' ',
                'kod' => $kod,
                'nev' => $kod
            );
        }
        
         

    }

    function setTermek_id($termek_id)
    {
        $this->termek_id = $termek_id;
    }
    function getTermek_id()
    {
        return $this->termek_id;
    }
    function setAr($ar)
    {
        $this->ar = $ar;

    }
    function getAr()
    {
        return $this->ar;
    }
    function setAkcios_ar($akcios_ar)
    {
        $this->akcios_ar = $akcios_ar;
    }
    function getAkcios_ar()
    {
        return $this->akcios_ar;
    }
    function setAr_penznem($ar_penznem)
    {
        $this->ar_penznem = $ar_penznem;
    }
    function getAr_penznem()
    {
        return $this->ar_penznem;
    }


}
