<?php


class BoltURL {
    
    public $lap = '';
    public $aktivUrl = '';    
    public $oldalak = array(
        'aruhazFooldal' => 'aruhazFooldal',
        'cart' => 'aruhazKosar',
        'fabr' => 'aruhazGyartok'
    );
    public $parancsLista, $parancs = array();
    public $parancsKodok = array(
        'c' => 'aruhazKategoriaOldal',
        'p' => 'aruhazTermekOldal',
        'm' => 'aruhazGyartoOldal'
    );
    
    public $termekLapozoAktivLap = false;
    public $metodus = 'aruhazKategoriaOldal';
    public $id = 0;
    
    public function __construct() {
        
    }
    
    
    
    public function urlFeldolgozas() {
        // nézzük, hogy kategória van-e megadva az url-ben
        $url =  Url::peldany();
        global $shopKonf;
        $reszek = $url->param;
        
                        
        if (!isset($reszek[0])) {
            $this->lap = '';
            return;
        }
        $this->aktivUrl = BASE_URL;
        if ($reszek[0]=='ajax') {
            array_shift($reszek);
            $this->aktivUrl .= 'ajax/';
        }
        if ($reszek[0]==$shopKonf->beallitas('Áruház.AlapUrl', 'online-store')) {
            $this->lap = 'aruhazFooldal';
            array_shift($reszek);
            $this->aktivUrl .= $shopKonf->beallitas('Áruház.AlapUrl', 'online-store').'/';
        }
        
        $megfeleltetes = array();
        $tulajdonsagReszek = array();
        if (!empty($reszek)) {
            // áruház oldalak
            if ($reszek[0] == $shopKonf->beallitas('Kosár URI', 'cart')){
                $this->parancsLista = array();
                $this->parancs = 'cart';
                $this->metodus = $this->oldalak[$this->parancs];
                
                $this->id = $this->parancs['id'];
                return;
            }
            
            $talalat = false;
            foreach ($reszek as $resz) {
                
                
                $m = $this->ellemzo($resz);
                if (!empty($m)) {
                    $megfeleltetes[] = $m;
                    $talalat = true;
                    if (trim($resz)!= '') $this->aktivUrl .= $resz.'/';  
                }
            }
        }
        
        
        
        if (!empty($megfeleltetes)) {
            $this->parancsLista = $megfeleltetes;
            $this->parancs = end($megfeleltetes);
            $this->metodus = $this->parancsKodok[$this->parancs['parancs']];
            $this->id = $this->parancs['id'];
            
        }
        
    }
    
    public function ellemzo($str) {
        global $shopKonf;
        
        $minta = '#(.*)_(?<parancs>\w)(?<id>\d+)#';
        preg_match($minta,$str, $talalat);
        $r = array();
        if (isset($talalat['parancs'])) {
            $r = array(
                'parancs' => $talalat['parancs'],
                'id' => $talalat['id']
            );
        }
        // lapozo ellemzése
        $lapozo = $shopKonf->beallitas('Termékek.TerméklistaLapozóUrlTag', 'page-');
        if(strpos($str,$lapozo)!==false) {
            
            $this->termekLapozoAktivLap = str_replace('.html', '', str_replace($lapozo, '', $str));
            
        }
        
        return $r;
    }
}
