<?php

/**
 * Áruház konfigurációs osztály
 * 
 */
 
class Konfiguracio {
    
    public function beallitas($kulcs, $alapertek = '') {
        $sql = "SELECT * FROM bos_konf WHERE kulcs = '$kulcs'";
        $rs = sqlAssocRow($sql);
        if (!isset($rs['ertek'])) {
            $a = array('kulcs' => $kulcs, 'ertek' => $alapertek);
            sqlfelvitel($a, 'bos_konf');
            return $alapertek;
        }
        return $rs['ertek'];
    }
    
}