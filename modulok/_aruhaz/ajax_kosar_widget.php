<?php

define('CSAK_KERET', 1);
$base = (dirname(dirname(dirname(__file__)))) . '/';
include ($base . 'ajaxkeret.php');

if (defined('UNICORN')) return;
require_once (BASE.'modulok/_aruhaz/osztaly/osztaly_aruhaz.php');
global $bos;
$bos = Aruhaz::peldany();
if (!isset($_GET['mod'])) {
    $_GET['mod'] = 'mini';
}


if ($_GET['mod']=='mini') {
    include(BASE.'modulok/_aruhaz/html/_aruhaz_widgetek_minikosar.php');
} else {
    include(BASE.'modulok/_aruhaz/html/_aruhaz_widgetek_widgetkosar.php');
    
}