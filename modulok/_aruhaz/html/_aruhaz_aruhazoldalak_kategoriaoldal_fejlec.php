<div class="row">
    <div class="col-lg-12">
        <h3><?= $kategoria->getNev(); ?></h3>
    </div>
</div>
<div class="row">
    <div class="col-lg-3">
        <div class="box-style">
            <img src="<?= $kategoria->getKep(); ?>" alt="<?= $kategoria->getNev(); ?>" width="100%"/>
        </div>
    </div>
    <div class="col-lg-9">
        <?= $kategoria->getLeiras(); ?>
    </div>
</div>