

<h3><?= __f('Kosár'); ?></h3>
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Terméklista</div>
  <div class="panel-body">
    <button class="btn btn-warning pull-right kosarFrissit" onclick="bow_kosarFrissit('kosarOsszesito');"><?= __f('Kosár frissítése'); ?></button>
    <p><?= (($info)?$info['bevezeto']:''); ?></p>
  </div>


<form id="kosarOsszesito" onsubmit="return: false;">
<table class="table table-striped">
    <thead>
        <tr>
        <td><?= __f('Név'); ?></td>
        <td><?= __f('Változat neve'); ?></td>
        <td><?= __f('Darabszám'); ?></td>
        <td> </td>
        </tr>
    </thead>
    
    <tbody>
        <?php
        if ($kosar->kosarUres()) {
            echo '<tr><td colspan="4">'.__f('Még nincs termék a kosarában').'</td></tr>';
        } else {
            $kosar->kosarReset();
            while ($kosar->kosarVanElem() ):
                
                $termek = $kosar->termek;
                ?>
         <tr>
         <!-- Terméknév -->
         <td>
            <a href="<?= SHOP_URL.$termek->getUtvonal(); ?>"><?= $termek->jellemzok->nev; ?></a>
            
         </td>
         <!-- Változat -->
         <td>
            <!-- Opciók  -->
            <?php
            if ($kosar->kosarVanOpcio()):
                while($opcio = $kosar->kosarTermekOpcio()) {
                            
            ?>
                       <div class="kosarTablaSorOpcio">
                       <span class="glyphicon glyphicon-remove bg-danger " style="padding: 3px;"></span>
                       <?= $opcio->nev().' ('.$opcio->db.')';?> 
                       
                       </div>
            <?php
                            
                }
            endif;
            ?>
         </td>
          
         <td>
            <input type="button" style="width: 30px;"  id="termszam_<?= $termek->id; ?>" class="btn" value="<?= $kosar->kosarTermekDb(); ?>" />
            <button class="btn"  type="button" onclick="bos_mennyisegNovelId('termszam_<?= $termek->id; ?>')"><span class="glyphicon glyphicon glyphicon-circle-arrow-up"></span></button>
            <button class="btn"  type="button"  onclick="bos_mennyisegCsokkentId('termszam_<?= $termek->id; ?>')"><span class="glyphicon glyphicon glyphicon-circle-arrow-down"></span></button>
    
         </td> 
         
         <td>
            <button class="btn btn-danger"  type="button"  ><?= __f('Törlés'); ?></button>
         </td>
         
         </tr>       
                
                <?php
            endwhile;
        }
                ?>
    </tbody>
</table>
</form>
<button type="button" class="btn btn-primary ">Primary button</button>
<button type="button" class="btn btn-info ">Button</button>
<br />
<br />
</div>