<div class="row category-list">
    <div class="col-lg-12">
    <h4><?= __f('Kategóriák'); ?></h4>
    </div>
    <?php
    
    $elemPerSor = $bos->KONF->beallitas('Kategóriák.ElemPerSor', 4);
    $termekSzamLatszik = $bos->KONF->beallitas('Kategóriák.termekSzamLatszik', 1);
    
    $colMod = (12/$elemPerSor);
    
    foreach($subKategoriak as $sor) {
        $kategoria = new Kategoria($sor['adat']['id']);
        ?>
        <div class="col-lg-<?= $colMod?>">
            <div class="katBox">
                <a href="<?= SHOP_URL.$kategoria->getUtvonal(); ?>">
                <?php
                if ($termekSzamLatszik):
                ?>
                <span class="glyphicon glyphicon-bookmark"><strong><?= $kategoria->termekSzam();?></strong></span>
                <?php
                endif;
                ?>
                <img src="<?= $kategoria->getKep();?>" alt="<?= $kategoria->getNev();?>"/>
                <div class="kategoriaNev"><?= $kategoria->getNev();?></div>
                </a>
            </div>
        </div>
        <?php
    }
    ?>
</div>