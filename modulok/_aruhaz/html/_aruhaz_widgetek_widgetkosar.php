<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-shopping-cart"></span> <?= __f('Kosár'); ?></div>
  <div class="panel-body bos_cart_big" >
    <?php
        $kosar = $bos->KOSAR;
        
        if ($kosar->kosarUres()) {
            echo __f('Még nincs termék a kosarában');
        } else {
            $kosar->kosarReset();
            while ($kosar->kosarVanElem() ):
                
                $termek = $kosar->termek;
                ?>
                <div class="widget_cart_row">
                    <a href="javascript:void(0);" class="remove-product" onclick="bos_torles('<?= $kosar->jelenKulcs; ?>')">x</a>
                    <div class="img_box">
                       <img src="<?= $termek->kepek->foKep();?>" width="60" />
                       <span class="kosarSorNev"><a href="<?= SHOP_URL.$termek->getUtvonal(); ?>"><?= $termek->jellemzok->nev.' ˙('.$kosar->kosarTermekDb().')'; ?></a></span><br />
                       <?php
                       if ($kosar->kosarVanOpcio()):
                            while($opcio = $kosar->kosarTermekOpcio()) {
                            
                            
                            
                       ?>
                       <span class="kosarSorOpcio"><?= $opcio->nev().' ('.$opcio->db.')';?></span><br />
                       <?php
                            
                            }
                       endif;
                       ?>
                       <?php
                        if (false)
                        if ($kosar->kosarJelenTermekAr()!==false):
                          
                       ?>
                       <span class="kosarSorAr">
                            <?= $bos->ARAK->formazas($kosar->kosarJelenTermekAr());?>
                       </span>
                       <?php
                       endif;
                       ?>
                    </div>
                    <br style="clear: both;" />
                    
                </div>
                
                <?php
            endwhile;
            
        }
    $tag  = Tagok::peldany();
    if ($kosar->kosarTermekArLatszik and !$kosar->kosarUres() and $tag->id > 0 and false):
    ?>
    <div class="kosarOsszar">
        <?php if ($kosar->kosarAjanlatos):?>
            <?= __f('Kérje ajánlatunkat!');?>
        <?php else: ?>
            <?= __f('Össz ár: ');?>
            <?= $bos->ARAK->formazas($kosar->termekOsszar());?>            
        <?php endif; ?>
                                        
    </div>
    <?php
    endif;
    if ($tag->id > 0):
    ?>       
    <button class="btn btn-success" onclick="window.location.href='<?= SHOP_URL.$bos->KONF->beallitas('Kosár URI', 'cart'); ?>'"><?= __f('Kosár megtekintése'); ?></button>
    <?php
    else:
    ?>
    <strong><?= __f('Ajánlatkéréshez kérjük lépjen be vagy regisztráljon')?></strong>
    <?php
    endif;
    ?>
  </div>
</div>