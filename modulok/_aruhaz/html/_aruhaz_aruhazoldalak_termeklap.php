<h3 class="termekcim"><?= $termek->jellemzok->get('nev'); ?></h3>
<strong class="termekcim"><?= __f('Cikkszám'); ?>:</strong> <?= $termek->getCikkszam(); ?>
    <br />
        <strong class="termekcim"><?= __f('Típus'); ?>:</strong> <?= $termek->jellemzok->get('tipus'); ?>
    <br />
    <br />
<div class="row">
    <!-- TERMÉKKÉPEK-->
    <div class="col-md-6">
            <img src="<?= $termek->kepek->foKep();?>" alt="<?= $termek->jellemzok->get('nev');?>"/>
    </div><!-- /.col-md -->
    <!-- KIS LEÍRÁS ,ÁR, KOSÁR -->
    <div class="col-md-6 jellemzok">
            <!-- paraméterek -->
            <p><?= nl2br($termek->jellemzok->get('parameterek'));?></p>
    </div><!-- /.col-md -->
</div><!-- /.row-fluid -->

<div class="row">
	<div class="col-md-6 arak">
		<div class="arasresz">
        <!-- árak -->
            <?php
                if ($termekArLatszik and ($termek->ar!=0 or $termek->akcios_ar!=0)):
                    if ($termekArNetto):
                        print '<div class="col-md-6">'.'<span class="nettoar">'.__f('Nettó ár: ').'</span>'.$termek->arDiv('termekArNetto termeklap').'</div>';
                    endif;
                    if ($termekArBrutto):
                        print '<div class="col-md-6">'.'<span class="bruttoar">'.__f('Bruttó ár: ').'</span>'.$termek->arDiv('termekArBrutto termeklap', 'brutto').'</div>';
                    endif;
                endif;
            ?>
            </div>
            <!-- kosár gombok -->
            <?php
                
                if ($termekKosar):
                    $termek->kosarHozzaadasHtml();
                endif;
            ?>
    </div><!-- /.col-md -->
    <div class="col-md-6">
        <strong class="termekcim"><?= __f('További jellemzők'); ?>:</strong><br />
        <?= nl2br($termek->jellemzok->get('jellemzok'));?>
    </div><!-- /.col-md -->
</div><!-- /.row-fluid -->