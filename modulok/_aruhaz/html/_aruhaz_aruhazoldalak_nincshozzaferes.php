<h3><?= __f('Kosár')?></h3>
<p><?= __f('Ajánlatot csak belépett felhasználó kérhet.')?></p>
<p><?= __f('Kérjük, lépjen be, vagy regisztráljon oldalunkra!')?></p>
<a class="btn btn-primary" href="<?= bowUrl('fiokom'); ?>"><?= __f('Regisztráció'); ?></a>