<div class="col-lg-<?= $colMod?>">
            <div class="termBox">
                
                <?php
                global $bos;
                $meretX = $bos->KONF->beallitas('Termékkártya.Kép-X', 180);
                $meretY = $bos->KONF->beallitas('Termékkártya.Kép-Y', 180);
                
                if ($termek->kiemelt==1):
                ?>
                <span class="akcioIcon glyphicon glyphicon-bookmark"><strong></strong></span>
                <?php
                endif;
                ?>
                <a href="<?= SHOP_URL.$termek->getUtvonal();?>">
                <img style="height: 220px;" src="<?= $termek->kepek->foKep($meretX, $meretY);?>" alt="<?= $termek->jellemzok->get('nev');?>"/>
                </a>
                <div class="termekNev"><?= $termek->jellemzok->get('nev');?></div>
                <div class="termekLeiras"><?= $termek->jellemzok->get('kisleiras');?></div>
                <?php
                if ($termekArLatszik):
                    if ($termekArNetto):
                        print __f('Nettó ár').'<br />'.$termek->arDiv('termekArNetto');
                    endif;
                    if ($termekArBrutto):
                        print __f('Ár ÁFÁ-val').'<br />'.$termek->arDiv('termekArBrutto', 'brutto');
                    endif;
                endif;
                ?>
                <?php
                if ($termekKeszlet):
                ?>
                <div class="termekKeszletDiv <?= ($termek->termekKeszlet>0)?__f('termekVan'):__f('termekNincs');?>">
                    <strong><?= ($termek->termekKeszlet>0)?__f('Raktáron'):__f('Elfogyott');?></strong>
                </div>
                <?php
                endif;
                ?>
                <div class="termekTovabb">
                    <a href="<?= SHOP_URL.$termek->getUtvonal();?>"><?= __f('Bővebb információk'); ?></a>
                </div>
                <?php
                if ($gyorsNezet):
                ?>
                <div class="termekGyorsnezet">
                    <a href="javascript:void(0);" ><span class="glyphicon glyphicon-eye-open"></span> <?= __f('Gyorsnézet');?></a>
                </div>
                <?php
                endif;
                ?>
            </div>
</div>

