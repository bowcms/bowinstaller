<div class="katMenu">
<?php

foreach($bos->KAT->kategoriaFa as $k => $sor) {
    $szint = $sor['szint'];
    $adat = $sor['adat'];
    if ($szint > 1) {
        $style = ' style="display:none"; ';
    } else {
        $style = '';
    }
    ?>
    <div class="katSzint_<?= $szint; ?>" <?= $style; ?> >
        <a href="<?=SHOP_URL.$bos->KAT->azUrl($adat['id']) ?>"><?= $adat['nev']; ?></a>
        <?php
        if (isset($bos->KAT->kategoriaFa[$k+1])) $ujSzint = $bos->KAT->kategoriaFa[$k+1]['szint']; else $ujSzint = $szint;
        if ($ujSzint>$szint):
        ?>
        <span class=" glyphicon glyphicon-zoom-in katOpen"></span>
        <?php
        endif;
        ?>
    </div>
    <?php
}
?>

</div>
