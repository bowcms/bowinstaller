<?php


/**
 * CSV importáló
 * boolean import
 * 
 **/


if (!function_exists('igenNemImport')) {
    function igenNemImport($ertek)
    {
        $ertek = strtolower(trim($ertek));
        if ($ertek == 'i' or $ertek == 'igen' or ( is_numeric($ertek) and $ertek > 0 )) {
            return 1;
        } else {
            return 0;
        }
    }
}