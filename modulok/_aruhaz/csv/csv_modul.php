<?php

/**
 * Bow Aruhaz modul
 * CSV import
 * 
 */

AH_LapCim('<a href="'.AM_URL.'csv">CSV Import</a>');

if (isset($_FILES['csv'])) {
    $_SESSION['csv_import']['file'] = AM_UT.'tmp/'.'csv_import_'.date('Y_m_d_').rand(100,999).'.jpg';
    move_uploaded_file($_FILES['csv']['tmp_name'],$_SESSION['csv_import']['file']);
    $_SESSION['csv_import']['kod'] = $_POST['kod'];
    $_SESSION['csv_import']['nyelv'] = $_POST['nyelv'];
    $_SESSION['csv_import']['sablon'] = $_POST['sablon'];
    $_SESSION['csv_import']['hatarolo'] = $_POST['hatarolo'];
    $_SESSION['csv_import']['start'] = 1;
    
    AH_LapCimAd('Importálás');
    include AH_Html('import');
    AH_Gomb('Vissza', '');
    return;
    
}

if (isset($_POST['mezoSor'])) {
    $mezok = trim( $_POST['mezoSor'], '|' );
    $mezok = str_replace('|', ',', $mezok);
    
    
    file_put_contents(AM_UT.'sablonok/'.strToUrl($_POST['sablonFileNev']).'.csv', $mezok);
    $html = Html::peldany();
    $html->uzenetKiiratas('CSV importáló sablon mentése sikeres!');
}
if (isset($_GET['letolt'])) {
    AH_LapCimAd('Letöltsek');
    include AH_Html('letoltesek');
    AH_Gomb('Vissza', '');
    return;
}
if (isset($_GET['sablon'])) {
    AH_LapCimAd('Sablonszerkesztő');
    
    if (isset($_POST['sablonNyitas'])) {
        $termekTabla = 'bos_termekleiras_'.$_POST['sablonnyelv'];
        
        osztalyTolto('adattablaszerkeszto');
        $ak = new adattablaSzerkeszto;
        $ak->betolt('bos_termekleiras_'.$_POST['sablonnyelv']);
        
        
        $mezok = array(
            't_cikkszam' => 'Cikkszám',
            'c_kategoria' => 'Kategória',
            'c_gyarto' => 'Gyártó',
            't_ar' => 'Ár',
            't_akcios_ar' => 'Akciós ár',
            't_beszerzesi_ar_penznem' => 'Beszerzési ár pénznem',
            'ar_penznem' => 'Ár pénznem',
            'c_forgalmazo' => 'Beszállító',
            't_beszerzesi_ar' => 'Beszerzési ár',
            'c_kiemelt' => 'Kiemelt',
            'c_publikus' => 'Publikus',
            't_keszlet' => 'Készlet',
            'c_ado' => 'Adónem'
        );
        $mezok2 = $ak->adatMezok();
        
        $mezok = array_merge($mezok, $mezok2);
        if ($_POST['sablonNyitas']=='uj') $_POST['sablonNyitas'] = 'alap_sablon.csv';
        $sablonFile = file_get_contents(AM_UT.'sablonok/'.$_POST['sablonNyitas']);
        $sablonTorzs = explode(',',$sablonFile);
        include AH_Html('mezoszerkeszto');
        return;
    }
    
    include AH_Html('sablonszerkeszto');
    return;
    AH_Gomb('Vissza', '');
} else {
    AH_Gomb('Új CSV sablon', 'sablon=0');
    AH_Gomb('Sablon letöltse', 'letolt=1');
}

$nyelvek = getRowsByField('bow_nyelvek', 'kulcs');


include AH_Html('udvozlo');
