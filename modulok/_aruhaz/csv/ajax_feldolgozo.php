<?php

session_start();
define('BASE', dirname(dirname(dirname(dirname(__file__)))) . '/');

$szeparator = ',';

include (BASE . 'kozos_fuggvenyek.php');
include (BASE . 'osztaly/osztaly_beallitas.php');
Beallitas::beallitasBetolt('adatbazis');
include_once BASE . 'osztaly/sqlfuggvenyek.php';

// első futás, idézőjelek közül eltüntetjük a br-eket
if ($_SESSION['csv_import']['elsofutas'] == '') {
    $csvfile = file_get_contents($_SESSION['csv_import']['file']);
    $csvfile = preg_replace_callback('|"[^"]+"|', create_function( // single quotes are essential here,
        // or alternative escape all $ as \$
    '$matches', 'return str_replace("\\n",\'*TORES*\',$matches[0]);'), $csvfile);
    file_put_contents($_SESSION['csv_import']['file'], $csvfile);
    $_SESSION['csv_import']['elsofutas'] = '1';
    exit;
}

$lines = file($_SESSION['csv_import']['file']);


$sablon = file_get_contents(BASE . 'modulok/_aruhaz/csv/sablonok/' . $_SESSION['csv_import']['sablon']);
$sablon = explode(',', $sablon);
$ossz = count($lines);
$sorok = 20;
$start = $_SESSION['csv_import']['start'];


$_SESSION['csv_import']['start'] += $sorok;

$leiroTabla = "bos_termekleiras_".$_SESSION['csv_import']['nyelv'];

for ($i = $start; $i < $start + $sorok; $i++) {
    
    if (!isset($lines[$i])) {
        ?>
        <center>
            <strong>Az importálás befejeződött!</strong>
        </center>
        <?php
        exit;
    }
    
    $sor = trim($lines[$i], "\n");

    // kódolás
    if ($_SESSION['csv_import']['kod'] != 'UTF-8') {
        $sor = iconv($_SESSION['csv_import']['kod'], 'UTF-8', $sor);

    }
    // escape
    $sor = str_replace("\n", '*ujsor*', $sor);
    
    
    $sor = preg_replace_callback('|"[^"]+"|', create_function( // single quotes are essential here,
        // or alternative escape all $ as \$
    '$matches', 'return str_replace(\',\',\'*VESSZO*\',$matches[0]);'), $sor);
    
    
    $sor = str_replace('"', '', $sor);
    // felbontas
    //print htmlspecialchars($sor);
    $mezok = explode($_SESSION['csv_import']['hatarolo'], $sor);
    $ujMezok = array();
    foreach ($mezok as $mezo) {
        $ujMezok[] = base64Teszt($mezo);
    }
    $mezok = $ujMezok;
    
    $adat = array();
    
    
    // vesszők visszaírása
    foreach ($mezok as $k => $v) {
        $adat[$sablon[$k]] = trim(str_replace('*VESSZO*', ',', $v), ' "');
        $adat[$sablon[$k]] = str_replace('*TORES*', "<br />", $adat[$sablon[$k]]);
        
        
    }
    
    
    
    $termekTorzs = array();
    $termekLeiras = array();
    $kapcsoltTablak = array();
    
    foreach ($adat as $k => $v) {
        $parancs = explode('_', $k);
        if ($parancs[0] == 'c') {
            $k = str_replace('c_', '', $k);
            // kapcsolt tábla vezérlő
            $kapcsoltTablak[$k] = $v;
        } elseif ($parancs[0] == 't') {
            $k = str_replace('t_', '', $k);
            // terméktörzs
            $termekTorzs[$k] = trim($v);
        } else {
            // egyik sem leíró
            $termekLeiras[$k] = trim($v);
        }
    }
    
    
    //print '<br /><br />Kapcsolt:<br />';
    //print_r ($kapcsoltTablak);
    $kategoriak = array();
    
    if (!empty($kapcsoltTablak)) {
        foreach ($kapcsoltTablak as $kapcsolo => $ertek) {
            
            switch($kapcsolo) {
                case 'torles':
                    //print $ertek.'<br />';
                    $ertek = strtolower($ertek);
                    if ($ertek == 'i' or $ertek == 'igen' or $ertek == '1') {
                        $sql = "SELECT id FROM termekek WHERE cikkszam = '".$termekTorzs['cikkszam']."' ";
                        $tRs = sqluniv3($sql);
                        $tid = $tRs['id'];
                        
                        $sql = "DELETE FROM bos_termekxkategoria WHERE termek_id = $tid";
                        sqluniv($sql);
                        $sql = "DELETE FROM $leiroTabla WHERE termek_id = $tid";
                        sqluniv($sql);
                        $sql = "DELETE FROM bos_termekek WHERE id = $tid";
                        sqluniv($sql);
                        continue;
                    }
                    
                    
                break;
                case 'kategoria':
                    // kategóriák ellenőrzése
                    // kat1/kat1_1 & kat2 & kat3/kat1/kat2
                    include(dirname(__FILE__).'/import_kodok/kategoria_import.php');
                    // print_r ($kategorizalas);
                break;
                
                case 'gyarto':
                    include(dirname(__FILE__).'/import_kodok/gyarto_import.php');
                break;
                
                case 'forgalmazo':
                    include(dirname(__FILE__).'/import_kodok/forgalmazo_import.php');
                break;
                case 'kiemelt':
                    include_once(dirname(__FILE__).'/import_kodok/igen_nem_import.php');
                    $termekTorzs['kiemelt'] = igenNemImport($ertek);
                break;
                case 'publikus':
                    include_once(dirname(__FILE__).'/import_kodok/igen_nem_import.php');
                    $termekTorzs['publikus'] = igenNemImport($ertek);
                break;
                case 'ado':
                    //print $ertek.'<br />';
                    
                    include(dirname(__FILE__).'/import_kodok/ado_import.php');
                    
                break;
            }
            
            
        }
    }
    
    // termék felvitel
    $sql = "SELECT id FROM bos_termekek WHERE cikkszam = '".$termekTorzs['cikkszam']."' LIMIT 1";
    $termek = sqluniv3($sql);
    
    
    
    
   //print '<br /><br />Törzs:<br />';
   // print_r ($termekTorzs);
   // print '<br /><br />Leíró:<br />';
    
   // print_r ($termekLeiras);
   // print '<br /><br />';
    
    if (isset($termek['id'])) {
        $termekTorzs['id'] = $termek['id'];
        sqladat($termekTorzs, 'bos_termekek');
    } else {
        sqladat($termekTorzs, 'bos_termekek');
        $termekTorzs['id'] = mysql_insert_id();
    }
    
    // Termék leírók
    $termekLeiras['termek_id'] = $termekTorzs['id'];
    
    $sql = "SELECT * FROM $leiroTabla WHERE termek_id = ".$termekTorzs['id'];
    
    $leiroRs = sqluniv3($sql);
    if (isset($leiroRs['id'])) {
        $termekLeiras['id'] = $leiroRs['id'];
    } else {
        $termekLeiras['id'] = 0;
    }
    sqladat($termekLeiras, $leiroTabla);
    
    if (!empty($kategorizalas)) {
        $sql = "DELETE FROM bos_termekxkategoria WHERE termek_id = ".$termekTorzs['id'];
        sqluniv($sql);
        foreach ($kategorizalas as $kategoriaId) {
            $sql = "INSERT INTO bos_termekxkategoria SET termek_id = ".$termekTorzs['id'].", kategoria_id = ".$kategoriaId.", nyelv = '".$_SESSION['csv_import']['nyelv']."'";
            sqluniv($sql);
        }
    }
    
    //print_r($adat);
    
}

function base64Teszt($str) {
    if (strpos($str,'base64[')!==false) {
        $str = str_replace('base64[','',$str);
        $str = rtrim($str,']');
        $str = base64_decode($str);
        return $str;
    } else {
        return $str;
    }
}

?>
<center>
    Feldolgozott termékek: <b><?= $i?> db, <?= ceil( ($i/$ossz)*100) ;?> %</b>
</center>
<div class="process">
    <div class="bar" style="width:  <?= ceil( ($i/$ossz)*100) ;?>%;text-align: center;"> %</div>
</div>
