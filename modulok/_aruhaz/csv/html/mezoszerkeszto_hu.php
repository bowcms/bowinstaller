<form style="text-align: center;" method="post" onsubmit="return false;" id="csvImportForm">
    <input type="hidden" name="mezoSor" value="" id="mezoSor"/>
    <input name="sablonFileNev" value="<?= str_replace('.csv', '',$_POST['sablonNyitas']); ?>" />  <input type="button" onclick="CSV_SablonMentes();" value="Sablon Mentése"/>
</form>

<div class="csv_mezo_bal">
<?php
foreach($sablonTorzs as $k => $v):
    if (!isset($mezok[$v])) continue;
?>
<div class="sablonMezo mezoDiv" id="<?= $v; ?>"><?= $mezok[$v]; ?></div>
<?php
    unset($mezok[$v]);
endforeach;
?>

</div>

<div class="csv_mezo_jobb">
<?php
foreach($mezok as $k => $v):
?>
<div class="adottMezo mezoDiv" id="<?= $k; ?>"><?= $v; ?></div>
<?php
endforeach;
?>

</div>
<br /><br /><br />
<strong>&laquo; Importálás mezők</strong><br /><br />
<strong>Használható mezők &raquo;</strong><br /><br />


<br style="clear: both;"/><br />
<?php
$html= Html::peldany();
$html->headerStart();
?>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
function CSV_SablonMentes() {
    mezok = $('.csv_mezo_bal div');
    ret = '';
    for(i = 0; i < mezok.length; i++) {
        ret = ret + '|' + mezok[i].id;
    }
    alert(ret);
    $('#mezoSor').val(ret);
    document.getElementById('csvImportForm').submit();
}

  $(function() {
    
    $( ".csv_mezo_bal div" ).draggable({
      appendTo: "body",
      helper: "clone"
    });
    $( ".csv_mezo_jobb div" ).draggable({
      appendTo: "body",
      helper: "clone"
    });
    $( ".csv_mezo_jobb" ).droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      drop: function( event, ui ) {
        $( this ).find( ".placeholder" ).remove();
        ui.draggable.appendTo( this );
      }
    }).sortable({
      items: "div:not(.placeholder)",
      sort: function() {
        // gets added unintentionally by droppable interacting with sortable
        // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
        $('<div class="placeholder"></div>').appendTo(this);
        $( this ).removeClass( "ui-state-default" );
      }
    });
    $( ".csv_mezo_bal" ).droppable({
      activeClass: "ui-state-default",
      hoverClass: "ui-state-hover",
      accept: ":not(.ui-sortable-helper)",
      drop: function( event, ui ) {
        $( this ).find( ".placeholder" ).remove();
        ui.draggable.appendTo( this );
      }
    }).sortable({
      items: "div:not(.placeholder)",
      sort: function() {
        // gets added unintentionally by droppable interacting with sortable
        // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
        $( this ).removeClass( "ui-state-default" );
        $('<div class="placeholder"></div>').appendTo(this);
      }
    });
    
    
    
    
  });
  </script>
<?php
$html->headerStop();

?>