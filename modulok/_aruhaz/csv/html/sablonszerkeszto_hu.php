<div class="ah_help">
<b>CSV importálás testreszabása</b>
<p>
    A termékeket egyszerű csv file-lal imortálhatjuk a rendszerbe. Az importálóba beállíthatjuk, hogy mely értékek állnak rendelkezésre 
    táblázatunkban. Az egyetlen kötelező elem a cikkszám, ez alapján történik a már fent lévő termékek frissítése.
</p>
<form method="post">
    <p>Válassz sablont:</p>
    <select name="sablonNyitas">
        <option value="uj">Új sablon</option>
        <?php
        $sablonok = osszesFile(AM_UT.'sablonok/', 'csv');
        foreach ($sablonok as $sablon):
        ?>
        <option value="<?= $sablon; ?>"><?= $sablon; ?></option>
        <?php
        endforeach;
        ?>
    </select>
    Nyelv:
    <select name="sablonnyelv">
        <?php
        $nyelvek = getRowsByField('bow_nyelvek', 'kulcs');
        foreach ($nyelvek as $kulcs => $nyelv):
        ?>
        <option value="<?= $kulcs; ?>"><?= $nyelv['nev']; ?></option>
        <?php
        endforeach;
        ?>
        ?>
    </select>
    <input type="submit" value="kivalaszt" />
</form>
</div>