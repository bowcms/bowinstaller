<div class="ah_help">
<b>CSV importálás tulajdonságai</b>
<p>
    Az importálás csv sablonok segítségvel lehetséges. Ha még nincs sablon, kérem készíts egyet az Új sablon gomb megnyomásával.
</p>

<form enctype="multipart/form-data" method="post">
    <table align="center" style="width: 600px;">
        <tr>
        <td>
            <label>CSV file:</label>
            <input type="file" name="csv" />
        </td>
        <td>
            <label>Sablon:</label>
            <select name="sablon">
                
                <?php
                $sablonok = osszesFile(AM_UT.'sablonok/', 'csv');
                foreach ($sablonok as $sablon)  {
                    ?>
                    <option value="<?= $sablon?>"><?= $sablon?></option>
                    <?php
                }
                ?>
            </select>
        </td>
        <td>
            <label>Nyelv:</label>
            <select name="nyelv">
                
                <?php
                foreach ($nyelvek as $kulcs => $sor):
                ?>
                <option value="<?= $kulcs; ?>"><?= $sor['nev']; ?></option>
                <?php
                endforeach;
                ?>
            </select>
        </td>
        <td>
            <label>Kódolás:</label>
            <select name="kod">
                <option value="UTF-8">UTF-8</option>
                <option value="ISO-8859-2">ISO-8859-2</option>
            </select>
        </td>
        
        <td>
            <label>Mező határoló:</label>
            <input value="," name="hatarolo" style="width:20px" />
        </td>
        
        
        </tr>
        <tr>
            <td colspan="5" style="text-align: center;">
                <input type="submit" />
            </td>
        </tr>
    </table>
</form>


</div>