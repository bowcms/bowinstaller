<?php



class aruhazOldalak

{



    public function __construct($ajax = false)

    {

        if (!$ajax) {

            $html = Html::peldany();

            $html->bodyVegeStart();

            include (modulHtmlElem('_aruhaz', 'termeklap_javascript'));

            $html->bodyVegeStop();    

        }

        
        /*
        $sql = "SELECT termek_id, ant_id FROM bos_termekleiras_hu ";

        $rs = mysql_query($sql);

        while ($sor = mysql_fetch_assoc($rs)) {

            //print 'tid-'.$sor['termek_id'].' :: antid:'.$sor['ant_id'].'<br />';

            $sql = "UPDAtE bos_termekkepek SET termek_id = ".$sor['termek_id']." WHERE termek_id = ".$sor['ant_id'];

            //sqluniv($sql);

            //print 'Termekkep_update '.mysql_affected_rows();

            

            

            $sql = "SELECT file FROM bos_termekkepek WHERE termek_id = ".$sor['termek_id'];

            $tk = sqluniv4($sql);

            foreach ($tk as $kep){

                $dir = dirname(BASE).'/kepmappa/'.$sor['ant_id'].'/';

                $dir2 = BASE.'feltoltesek/term/'.$sor['termek_id'].'/';

                

                @mkdir($dir2);

                if (file_exists( $dir2.$kep['file'].'.jpg')) continue;
                
                $t = @copy($dir.'big_'.$kep['file'].'.jpg', $dir2.$kep['file'].'.jpg');
                if (!$t) print 'copy: '.$dir.'big_'.$kep['file'].'.jpg'.' >> '.$dir2.$kep['file'].'.jpg<br />';    

            }

            

            

        }
        */
        

    }

    public function aruhazKosar () {

        global $bos;

        $html = Html::peldany();

        $html->helyorzoHozzafuzes('navUtvonal', '<a href="' . SHOP_URL .

            '"><span class="glyphicon glyphicon-home"></span>' . __f('Áruház') .

            '</a> '.__f('Kosár'));

        

        $tag  = Tagok::peldany();

        if ($tag->id == 0) {

            include(modulHtmlElem('_aruhaz','nincshozzaferes'));

            return;

        }

        

        $termekArLatszik = $bos->KONF->beallitas('Termékek.termekArLatszik', 1);

        $termekArCsoportok = $bos->KONF->beallitas('Termékek.termekArCsoportok', '');

        if ($termekArCsoportok != '') {

            $tag = Tagok::peldany();



            $exp = explode(',', $termekArCsoportok);

            $vanJog = false;

            foreach ($exp as $csoportId) {

                if ($csoportId == $tag->adat['csoport_id'])

                    $vanJog = true;

            }

            $termekArLatszik = $vanJog;

        }

        $termekArNetto = $bos->KONF->beallitas('Termékek.termekArNetto', 1);

        $termekArBrutto = $bos->KONF->beallitas('Termékek.termekArBrutto', 1);

        

        $kosar = $bos->KOSAR;

        $info = szovegKulcsSzerint('kosar_info');

        

        

        include(modulHtmlElem('_aruhaz', 'kosaroldal'));

        

        

    }

    public function aruhazTermekOldal($id = 0)

    {

        global $bos;

        $html = Html::peldany();

        $html->helyorzoHozzafuzes('navUtvonal', '<a href="' . SHOP_URL .

            '"><span class="glyphicon glyphicon-home"></span>' . __f('Áruház főoldal') .

            '</a>');

        if (!($termek = $bos->TERM->betoltes($id))) {

            include (modulHtmlElem('_aruhaz', 'nincs_ilyen_termek'));

        }

        $utvonal = $termek->kategoriaKereso();

        foreach ($utvonal as $sor) {

            $html->helyorzoHozzafuzes('navUtvonal', '<a href="' . $sor['link'] . '">' . $sor['nev'] .

                '</a>');

        }

        $html->helyorzoHozzafuzes('navUtvonal', $termek->jellemzok->get('nev'));

        // termklap megjelenítés

        $termekArLatszik = $bos->KONF->beallitas('Termékek.termekArLatszik', 1);

        $termekArCsoportok = $bos->KONF->beallitas('Termékek.termekArCsoportok', '');

        if ($termekArCsoportok != '') {

            $tag = Tagok::peldany();



            $exp = explode(',', $termekArCsoportok);

            $vanJog = false;

            foreach ($exp as $csoportId) {

                if ($csoportId == $tag->adat['csoport_id'])

                    $vanJog = true;

            }

            $termekArLatszik = $vanJog;

        }

        $termekArNetto = $bos->KONF->beallitas('Termékek.termekArNetto', 1);

        $termekArBrutto = $bos->KONF->beallitas('Termékek.termekArBrutto', 1);

        $termekKeszlet = $bos->KONF->beallitas('Termékek.termekKészletLátszik', 1);

        $gyorsNezet = $bos->KONF->beallitas('Termékek.termékkártyaGyorsnézet', 0);

        $termekKosar = $bos->KONF->beallitas('Termékek.terméklapKosarlatszik', 1);



        include (modulHtmlElem('_aruhaz', 'termeklap'));

        

    }



    public function aruhazKategoriaOldal($id = 0)

    {

        global $bos;

        $html = Html::peldany();

        $html->helyorzoHozzafuzes('navUtvonal', '<a href="' . SHOP_URL .

            '"><span class="glyphicon glyphicon-home"></span>' . __f('Áruház főoldal') .

            '</a>');



        $subKategoriak = $bos->KAT->subKategoriak($id);

        $kategoria = $bos->KAT->kategoria($id);

        $utvonal = $bos->KAT->utvonal($id);



        foreach ($utvonal as $sor) {

            $html->helyorzoHozzafuzes('navUtvonal', '<a href="' . $sor['link'] . '">' . $sor['nev'] .

                '</a>');

        }

        if ($id != 0)

            include (modulHtmlElem('_aruhaz', 'kategoriaoldal_fejlec'));

        if (!empty($subKategoriak)) {

            include (modulHtmlElem('_aruhaz', 'kategoriaoldal_kategorialista'));

        }

        $termekSor = $bos->KONF->beallitas('Kategóriák.Terméksor', true);

        if ($termekSor) {

            $kategoria = $bos->KAT->kategoria($id);

            if ($kategoria->sajatTermekSzam() > 0) {

                $lista = $bos->TERM->kategoriaTermekek($id);

                include (modulHtmlElem('_aruhaz', 'termeklista_fejlec'));



                $lapozo = $bos->KONF->beallitas('Termékek.TerméklistaLapozó', true);

                

                // lapozó és szűró

                if ($lapozo) {

                    $bos->TERM->termekListaLapozoStart('kategoriaOldal_' . $id);

                    $bos->TERM->termekListaLapozoTermekszam($bos->KONF->beallitas('Termékek.TerméklistaLapozóTermékszám',

                        9));

                    include (modulHtmlElem('_aruhaz', 'termeklista_lapozo'));

                }

                // terméklista

                $colMod = 12 / $bos->KONF->beallitas('Termékek.TermékPerSor', 3);





                $szurtLista = $bos->TERM->szurtTermekLista();

                if (count($szurtLista) > 0) {





                    $termekArLatszik = $bos->KONF->beallitas('Termékek.termekArLatszik', 1);

                    $termekArCsoportok = $bos->KONF->beallitas('Termékek.termekArCsoportok', '');

                    if ($termekArCsoportok != '') {

                        $tag = Tagok::peldany();



                        $exp = explode(',', $termekArCsoportok);

                        $vanJog = false;

                        foreach ($exp as $csoportId) {

                            if ($csoportId == $tag->adat['csoport_id'])

                                $vanJog = true;

                        }

                        $termekArLatszik = $vanJog;

                    }

                    $termekArNetto = $bos->KONF->beallitas('Termékek.termekArNetto', 1);

                    $termekArBrutto = $bos->KONF->beallitas('Termékek.termekArBrutto', 1);

                    $termekKeszlet = $bos->KONF->beallitas('Termékek.termekKészletLátszik', 1);

                    $gyorsNezet = $bos->KONF->beallitas('Termékek.termékkártyaGyorsnézet', 0);



                    foreach ($szurtLista as $termek) {

                        include (modulHtmlElem('_aruhaz', 'termeklista_termekkartya'));

                    }

                    // lapozó és szűró

                    if ($lapozo) {

                        $bos->TERM->termekListaLapozoStart('kategoriaOldal_' . $id);

                        $bos->TERM->termekListaLapozoTermekszam($bos->KONF->beallitas('Termékek.TerméklistaLapozóTermékszám',

                            9));

                        include (modulHtmlElem('_aruhaz', 'termeklista_lapozo'));

                    }



                }

                // div zárás

                include (modulHtmlElem('_aruhaz', 'termeklista_lablec'));

            } else {

                include (modulHtmlElem('_aruhaz', 'kategoriaoldal_nicstermek'));

            }

        }

    }

}

