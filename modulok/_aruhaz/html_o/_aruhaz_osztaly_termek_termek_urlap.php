<i><?= __f('Adja meg a kívánt jellemzőket')?></i><br /><br />
<?php foreach ($inputok as $input):?>
<label class=""><?= $input['label']; ?></label>
<div class="input-group">
    <input class="form-control" name="termekform[<?= $input['id']?>]" value="<?= $input['alap']?>" <?= (($input['szerkesztheto']!=1)?' disabled="disabled" ':'')?>/>
    <?php if ($input['egyseg']!=''):?>
    <span class="input-group-addon"><?= $input['egyseg']; ?></span>
    <?php endif; ?>    
    
</div>
<?php endforeach; ?>