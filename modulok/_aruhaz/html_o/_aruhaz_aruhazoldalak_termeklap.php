<h3><?= $termek->jellemzok->get('nev'); ?></h3>


<div class="row-fluid">
    <!-- TERMÉKKÉPEK-->
    <div class="col-lg-6">
        
            <img src="<?= $termek->kepek->foKep();?>" alt="<?= $termek->jellemzok->get('nev');?>"/>
            
        
    </div>
    <!-- KIS LEÍRÁS ,ÁR, KOSÁR -->
    <div class="col-lg-6">
            <!-- paraméterek -->
            <p><?= nl2br($termek->jellemzok->get('parameterek'));?></p>
            
            <!-- árak -->
            <?php
                if ($termekArLatszik and ($termek->ar!=0 or $termek->akcios_ar!=0)):
                    if ($termekArNetto):
                        print __f('Nettó ár').'<br />'.$termek->arDiv('termekArNetto termeklap');
                    endif;
                    if ($termekArBrutto):
                        print __f('Ár ÁFÁ-val').'<br />'.$termek->arDiv('termekArBrutto termeklap', 'brutto');
                    endif;
                endif;
            ?>
            <!-- kosár gombok -->
            
            <?php
                
                if ($termekKosar):
                    $termek->kosarHozzaadasHtml();
                endif;
            ?>
            
            
    </div>
</div>

<div class="row-fluid">
    <div class="col-lg-12">
        <strong><?= __f('Cikkszám'); ?>: <?= $termek->getCikkszam(); ?></strong>
        <br />
        <strong><?= __f('Típus'); ?>: <?= $termek->jellemzok->get('tipus'); ?></strong>
        <br />
        <br />
        <strong><?= __f('További jellemzők'); ?>:</strong><br />
        <?= nl2br($termek->jellemzok->get('jellemzok'));?>
        
    </div>
</div>