<div class="row-fluid clear-both roundedbox">
    <div class="col-lg-3">
     
    </div>
    <div class="col-lg-3">
     
    </div>
    
     

    <div class="col-lg-6">
        <?= __f('Lapok');?>: <?= $bos->TERM->termekListaLapozoLapszam().' / '.$bos->TERM->termekListaAktualisLapszam();?><br />
        <ul class="pagination">
            <li><a href="<?= $bos->TERM->termekListaLapozoElsolapUrl(); ?>">&laquo;</a></li>
            <?php
            $osszLap = $bos->TERM->termekListaLapozoLapszam();
            for ($i = 1; $i <= $osszLap; $i++):
            ?>
            <li><a href="<?= $bos->TERM->termekListaLapozoLink($i); ?>"><?= $i; ?></a></li>
            <?php
            endfor;
            ?>
            <li><a href="<?= $bos->TERM->termekListaLapozoUtolsoLapUrl(); ?>">&raquo;</a></li>
        </ul>
        
    </div>
    <br class="clear-both"/>
</div>