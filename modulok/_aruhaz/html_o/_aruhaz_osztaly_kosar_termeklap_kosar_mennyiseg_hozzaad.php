<?php global $shopKonf; ?>
<form method="post" id="cartForm">
<div class="row-fluid clear-both roundedbox">
    <?php if($termek->vanOpcio())  $termek->opcioSelectHtml();?>
    <?php if($termek->vanAdatlap())  $termek->adatlapHtml();?>
    <div class="btn-group">
        <input class="btn" value="1" id="mennyiseg" name="kosar.mennyiseg" />
        <input  value="<?= $termek->id; ?>" name="termek_id" type="hidden"  />
        <input type="hidden" name="kosarUrl" value="<?= SHOP_URL.$shopKonf->beallitas('Áruház.KosárUri', 'cart');?>"/>
        <button class="btn" type="button" onclick="bos_mennyisegNovel(<?= $termek->keszlet; ?>)"><span class="glyphicon glyphicon glyphicon-circle-arrow-up"></span></button>
        <button class="btn" type="button"  onclick="bos_mennyisegCsokkent()"><span class="glyphicon glyphicon glyphicon-circle-arrow-down"></span></button>
        <button class="btn" type="button" onclick="bos_hozzaad(this);"><span class="glyphicon glyphicon glyphicon-shopping-cart"></span></button>
    </div>
    
</div>
</form>
<div id="cartModal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <h4><?= __f('Termék hozzáadása a kosárhoz')?></h4>
      <p id="modalCartmmSG"></p>
    </div>
  </div>
</div>
<style>
.modal-dialog {
    z-index: 2000;
}
.modal-content {
    padding: 10px;
}
</style>