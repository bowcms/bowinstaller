<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-shopping-cart"></span> <?= __f('Kosár'); ?></div>
  <div class="panel-body bos_cart_big" >
    <?php
        $lista = $bos->KOSAR->kosarTartalom();
        if (empty($lista)) {
            echo __f('Még nincs termék a kosarában');
        } else {
            print_r ($lista);
            foreach ($lista as $k => $sor ):
                $termek = new Termek($sor['id']);
                
                ?>
                <div class="widget_cart_row">
                    <a href="javascript:void(0);" class="remove-product" onclick="termekLevetel(<?= $k; ?>)">x</a>
                    <div class="img_box">
                       <img src="<?= $termek->kepek->foKep();?>" width="60" />
                       <span><?= $termek->jellemzok->nev; ?></span>
                    </div>
                    <br style="clear: both;" />
                    
                </div>
                
                <?php
            endforeach;
        }
    ?>
  </div>
</div>