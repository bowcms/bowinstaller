<h5><?= __f('A termék sajnos kifogyott, kérem érdeklődjön irodánkban az elérhetőségét illetően:');?></h5>
<form method="post">
    <?= ((isset($erdeklodesHiba))?'<div class="alert alert-warning">'.$erdeklodesHiba.'</div>':'')?>
  <div class="input-group" style="width: 100%;">
    
    <input type="text" name="termeklapForm[nev]" class="form-control" placeholder="<?= __f('Név');?>">
  </div>
  <div class="input-group" style="width: 100%;">
    <span class="input-group-addon">@</span>
    <input type="text" name="termeklapForm[email]" class="form-control" placeholder="<?= __f('E-mail cím');?>">
  </div>
  <div class="input-group" style="width: 100%;">
    <textarea name="termeklapForm[uzenet]" class="form-control" placeholder="<?= __f('Üzenet');?>"></textarea>
  </div>
  <br>
  <div class="input-group" style="width: 100%;text-align: center;">
    <input type="submit" class="btn" value="<?= __f('Küldés');?>">
  </div>
  
  </form>