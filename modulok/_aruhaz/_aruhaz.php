<?php

class _aruhaz {
    
    public function kozpont($feladat = '', $alfeladat = '', $id = '') {
        define('ARUADMIN', BASE_URL.'uniadmin/_aruhaz:shopEditorUnicorn/');
        define('ARUMODUL_URL', BASE_URL.'modulok/_aruhaz/');
        define('ARUMODUL_UT', BASE.'modulok/_aruhaz/');
        include (ARUMODUL_UT.'ah_kozos_fuggvenyek.php');
        $mem = Memoria::peldany();
        $html = Html::peldany();
        
        
        
        $html->helyorzo('lapCim', 'Áruház');
        $this->komp->header();
        
        if($feladat!=''){
            if(file_exists(ARUMODUL_UT.$feladat.'/'.$feladat.'_modul.php')) {
                define('AM_UT',ARUMODUL_UT.$feladat.'/' );
                define('AM_URL',ARUADMIN.$feladat.'/' );
                
                include (ARUMODUL_UT.$feladat.'/'.$feladat.'_modul.php');
                return;
            } else {
                $html->hibaKiiratas('Modul nem elérhető: '.$feladat.'/'.$feladat.'_modul.php');
            }
        }
        
        
        $this->komp->kozpont();
        
    }
}