<?php

define('CSAK_KERET', 1);
$base = (dirname(dirname(dirname(__file__)))) . '/';
include ($base . 'ajaxkeret.php');

include (dirname(__FILE__).'/osztaly/osztaly_kosar.php');

$kosar = new Kosar();
if (isset($_POST['tid'])) {
    if (isset($kosar->tartalom[$_POST['tid']])) {
        $kosar->torles($_POST['tid']);
        $uzenet = __f('Törlés sikeresen megtörtént');
    } else {
        $uzenet = __f('Nem sikerült törölnöm az elemet');
    }
    ?>
<div style="text-align: center;">
    <?= $uzenet; ?><br /><br />
    <button class="btn btn-info" onclick="$('#cartModal_del').modal('hide');"><?= __f('Vissza'); ?></button>
    <button class="btn btn-success" onclick="window.location.href='<?= $_POST['kosarUrl']; ?>'"><?= __f('Kosár megtekintése'); ?></button>
</div>
<?php
    exit;
}


$mennyiseg = ((isset($_POST['kosar_mennyiseg']))?$_POST['kosar_mennyiseg']:1);
$termekId = $_POST['termek_id'];
$opciok = ((isset($_POST['termek_opcio']))?$_POST['termek_opcio']:false);
$termekForm = ((isset($_POST['termekform']))?$_POST['termekform']:false);

if ((int)$termekId==0) exit;

$kosar->hozzaadas($termekId, $mennyiseg, $opciok, $termekForm);
ob_start();
?>
<div style="text-align: center;">
    <?= __f('Termék a kosárba került!'); ?><br /><br />
    <button class="btn btn-info" onclick="$('#cartModal').modal('hide');"><?= __f('Vissza'); ?></button>
    <button class="btn btn-success" onclick="window.location.href='<?= $_POST['kosarUrl']; ?>'"><?= __f('Kosár megtekintése'); ?></button>
</div>
<?php
$modal = ob_get_contents();
ob_end_clean();
print $modal;


?>

