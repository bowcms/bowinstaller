<?php

class _aruhazKomp {
    
    public function kozpont() {
        include(ARUMODUL_UT.'aruhaz_modul_rendszer.php');
        foreach ($aruhaz_modulok as $sor):
        ?>
        <a class="kozpontdiv"  href="<?= ARUADMIN.$sor['url']; ?>">
            <img src="<?= ARUMODUL_URL ; ?>images/<?= $sor['ikon']?>" />
            <span><?= $sor['nev']?></span>
        </a>
        <?php
        endforeach;
        
    }
    
    public function header() {
        $html = Html::peldany();
        $html->headerStart();
        ?>
        <style type="text/css">
        a.kozpontdiv {
            width: 128px;
            padding: 2px;
            border: 2px solid #eee;
            display: inline-block;
            margin: 5px;
            text-align: center;
            -webkit-box-shadow: 7px 7px 5px rgba(50, 50, 50, 0.75);
-moz-box-shadow:    7px 7px 5px rgba(50, 50, 50, 0.75);
box-shadow:         7px 7px 5px rgba(50, 50, 50, 0.75);
-webkit-border-radius: 5px;
-webkit-border-bottom-right-radius: 0;
-moz-border-radius: 5px;
-moz-border-radius-bottomright: 0;
border-radius: 5px;
border-bottom-right-radius: 0;
            
        }
        a.kozpontdiv span {
            display: block;
            padding: 3px;
            color: #008040;
            text-align: center;
            text-decoration: underline;
            text-decoration-style: dotted;
        }
        a.kozpontdiv:hover {
            background: #FFFADF;
            -webkit-box-shadow: 4px 4px 5px rgba(50, 50, 50, 0.75);
-moz-box-shadow:    4px 4px 5px rgba(50, 50, 50, 0.75);
box-shadow:         4px 4px 5px rgba(50, 50, 50, 0.75);
        
        }
        .kezelok {
            float: right;
        }
        .kezelok a {
            -webkit-box-shadow: 4px 4px 5px rgba(50, 50, 50, 0.75);
-moz-box-shadow:    4px 4px 5px rgba(50, 50, 50, 0.75);
box-shadow:         4px 4px 5px rgba(50, 50, 50, 0.75);
            background: #FF8000;
            padding: 5px 7px 5px 7px;
            color: #fff;
            font-size: 14px;
            margin: 3px;
            margin-top: 7px;
            display: inline-block;
            text-decoration: none;
            -webkit-border-radius: 4px;
-moz-border-radius: 4px;
border-radius: 4px;
        }
        .ah_help {
            margin: 5px;
            padding: 10px;
            font-size: 12px;
        }
        .ah_tab a{
            
            padding: 10px 5px 10px 5px;
            text-decoration: none;
            color: #000;
            float: left;
            -webkit-border-top-left-radius: 10px;
-webkit-border-top-right-radius: 10px;
-moz-border-radius-topleft: 10px;
-moz-border-radius-topright: 10px;
border-top-left-radius: 10px;
border-top-right-radius: 10px;
            border: 1px solid #aaa;
            background: #eee;
            
        }
        .ah_tab a.aktiv {
            background: #fff;
            border-bottom: none;
        }
        .ah_szerkeszto_keret {
            margin: 2px;
            border: 1px solid #ddd;
            padding: 10px;
            clear: both;
        }
        .figyelmezteto {
            margin: 5px;
            padding: 5px;
            border: 1px solid #FF8080;
            background: #FED3DB;
        }
        .ahform label {
            display: inline-block;
            width: 200px;
            float: left;
        }
        .csv_mezo_bal {
            margin: 5px; border: 1px solid #aaa;width:40%;float: left;
            padding: 5px;   
            min-height: 200px;
        }
        .csv_mezo_jobb {
            margin: 5px; border: 1px solid #eee;width:40%;float: right;
            padding: 5px;   
            min-height: 200px;
        }
        .csv_mezo_bal div{
            background: #9FFFCF;
            border: 1px solid #00D96C;
            padding: 3px;
            font-weight: bold;
            
        }
        .csv_mezo_jobb div{
            background: #0080C0;
            border: 1px solid #004F75;
            padding: 3px;
            font-weight: bold;
            color: #fff;
            
        }
        .mezoDiv {
            margin: 5px;
            background: #F3F3F3;
            border: 1px solid #00D96C;
            padding: 3px;
            font-weight: bold;
        }
        
        .process {
            margin: 20px;
            height: 30px;
            border: 1px solid #aaa;
        }
        .process .bar {
            height: 100%;
            width: 10%;
            background: green;
        }
        .listaGomb {
            width: 15px;
            height: 15px;
            background: yellow;
        }
        .kapcsolo_0 {
            background: red;
        }
        .kapcsolo_1 {
            background: green;
        }
        table label {
            width: 160px;
            display: block;
            float: left;
        }
        table p {
            clear: both;
        }
        
        </style>
        <?php
        $html->headerStop();
    }
}