<?php

/**
 * 
 * _menu.php
 * 
 * Menükezelés
 */

class _menu {
    
    public function menucsoportok($feladat='', $id=0) {
        $html = Html::peldany();
        $html->helyorzo('lapCim', 'Menüpontok szerkesztése');
        $tag = Tagok::peldany();
        
        if (isset($_POST['m'])) {
            $m = $_POST['m'];
            if ($m['cim']=='' or $m['menucsoport']=='') {
                $html->hibaKiiratas('Cím és menücsoport megadása kötelező!');
                
                $this->komp->menupontSzerkeszto($m);
                return;
            }
            sqladat($m, 'bow_menuk');
            $html->uzenetKiiratas('Menüpont mentve!');
        }
        
        $id = (int)$id;
        if ($feladat=='szerkeszt') {
            $html->helyorzo('lapCim', '<a href="'.ADMIN_URL.'_menu/menucsoportok">Menüpontok</a> | Menüpont szerkesztése');
            if ($id > 0) {
                $sql = "SELECT * FROM bow_menuk WHERE id = $id LIMIT 1";
                $menupont = sqluniv3($sql);
                
            } else {
                $menupont = sqluresmezok('bow_menuk');
            }
            $this->komp->menupontSzerkeszto($menupont);
            return;
            
        }
        if ($feladat=='torol') {
            if ($id > 0) {
                $sql = "DELETE FROM bow_menuk WHERE id = $id LIMIT 1";
                sqluniv($sql);
                $sql = "DELETE FROM bow_menuxcsoport WHERE menu_id = $id LIMIT 1";
                sqluniv($sql);
            } 
        }
        if (isset($_POST['csxmo'])) {
            $id = $_POST['menu_id'];
            $sql = "DELETE FROM bow_menuxcsoport WHERE menu_id = $id";
            sqluniv($sql);
            foreach ($_POST['csxmo'] as $csoportId) {
                $sql = "INSERT INTO bow_menuxcsoport SET menu_id = $id, csoport_id = $csoportId ";
                sqluniv($sql);
            }
            $html->uzenetKiiratas('Jogosultságok módosítása megtörtént!');
        }
        if ($feladat == 'jogok') {
            $html->helyorzo('lapCim', '<a href="'.ADMIN_URL.'_oldalak/lista">Menü szerkesztő</a> | Menüpont megjelenítésnek korlátozása');
        
            $sql = "SELECT * FROM bow_csoportok WHERE rang <= (SELECT rang FROM bow_csoportok WHERE id = ".$tag->adat['csoport_id']." ) ORDER BY rang ASC";
            $csoportok = sqluniv4($sql);
            $this->komp->menuCsoportBeallito($id, $csoportok);
            return;
        }
        $menuk = sqluniv4("SELECT distinct(menucsoport) FROM bow_menuk ORDER BY menucsoport ASC");
        $this->komp->menucsoportok($menuk);
        
    }
    
    
    public function menucsoportokUnicorn($feladat='', $id=0) {
        $tag = Tagok::peldany();
        global $u;
        if (isset($_POST['m'])) {
            $m = $_POST['m'];
            if ($m['cim']=='' or $m['menucsoport']=='') {
                $html->hibaKiiratas('Cím és menücsoport megadása kötelező!');
                
                $this->komp->menupontSzerkeszto($m);
                return;
            }
            sqladat($m, 'bow_menuk');
            $html->uzenetKiiratas('Menüpont mentve!');
        }
        
        $id = (int)$id;
        if ($feladat=='szerkeszt') {
            $html->helyorzo('lapCim', '<a href="'.ADMIN_URL.'_menu/menucsoportok">Menüpontok</a> | Menüpont szerkesztése');
            if ($id > 0) {
                $sql = "SELECT * FROM bow_menuk WHERE id = $id LIMIT 1";
                $menupont = sqluniv3($sql);
                
            } else {
                $menupont = sqluresmezok('bow_menuk');
            }
            $this->komp->menupontSzerkeszto($menupont);
            return;
            
        }
        if ($feladat=='torol') {
            if ($id > 0) {
                $sql = "DELETE FROM bow_menuk WHERE id = $id LIMIT 1";
                sqluniv($sql);
                $sql = "DELETE FROM bow_menuxcsoport WHERE menu_id = $id LIMIT 1";
                sqluniv($sql);
            } 
        }
        if (isset($_POST['csxmo'])) {
            $id = $_POST['menu_id'];
            $sql = "DELETE FROM bow_menuxcsoport WHERE menu_id = $id";
            sqluniv($sql);
            foreach ($_POST['csxmo'] as $csoportId) {
                $sql = "INSERT INTO bow_menuxcsoport SET menu_id = $id, csoport_id = $csoportId ";
                sqluniv($sql);
            }
            $html->uzenetKiiratas('Jogosultságok módosítása megtörtént!');
        }
        if ($feladat == 'jogok') {
            $html->helyorzo('lapCim', '<a href="'.ADMIN_URL.'_oldalak/lista">Menü szerkesztő</a> | Menüpont megjelenítésnek korlátozása');
        
            $sql = "SELECT * FROM bow_csoportok WHERE rang <= (SELECT rang FROM bow_csoportok WHERE id = ".$tag->adat['csoport_id']." ) ORDER BY rang ASC";
            $csoportok = sqluniv4($sql);
            $this->komp->menuCsoportBeallito($id, $csoportok);
            return;
        }
        
        
        $menuk = sqluniv4("SELECT distinct(menucsoport) FROM bow_menuk ORDER BY menucsoport ASC");
        ?>
        <div class="row" style="text-align: right;">
            <div class="btn-group">
                                      <button class="btn " onclick="window.location.href='<?= UNIC_URL; ?>groups/0'"><?= __f('Új menüpont')?></button>

									  <button data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><?= __f('Menücsoport kiválaszta')?> <span class="caret"></span></button>
									  
									  <ul class="dropdown-menu">
										<?php
                                        foreach ($menuk as $k => $menu):
                                            if (trim($menu['menucsoport'])=='') continue;
                                            
                                        ?>
                                        <li><a href="<?= UNIC_URL; ?>groups/<?= urlencode($menu['menucsoport'])?>"><?= $menu['menucsoport']; ?></a></li>
                                        <?php
                                        endforeach;
                                        ?>
									  </ul>
            </div>
            
        </div>
        <?php
        print $feladat.' '.$id;
        
        if ($id != '') {
            $sql = "SELECT * FROM bow_menuk WHERE menucsoport = '".$id."' ORDER BY sorrend ASC";
            print $sql;
            $rs = sqluniv4($sql);
            print_r ($rs);
            ?><?PHP
            
        }
        
    }
}
if (defined('UNICORN')) {
    $menu = new _menu();
    $menu->menucsoportokUnicorn($feladat, $id);
    
}

