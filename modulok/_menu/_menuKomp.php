<?php

/**
 * 
 * _menuKomp.php
 * 
 * Menükezelés
 */

class _menuKomp {
    
    public function menucsoportok($csoportok = '') {
        ?>
        <br />
        <div class="infoDiv">
            A menüszerkesztő segítségével menücsoportokat hozhatsz létre.
        </div>
        <br />
        <form action="<?= ADMIN_URL?>_menu/menucsoportok/szerkeszt/0" class="jNice">
            <input type="submit" value="Új Menüpont létrehozása"/>
        </form>
        <br style="clear: both;"/>
        <br /><br />
        
        <?php
        if (!empty($csoportok))
        foreach ($csoportok as $csoport):
        ?>
        
        <fieldset>
            <legend>"<?= $csoport['menucsoport']?>" csoport szerkesztése</legend>
            <br style="clear: both;"/>
            <?php 
            $sql = "SELECT * FROM bow_menuk WHERE menucsoport = '".$csoport['menucsoport']."' ORDER BY sorrend ASC";
            $rs = sqluniv4($sql);
            if (!empty($rs))
            foreach ($rs as $menu):
            ?>
            <div class="menusor szint<?= $menu['szint']?>">
                <div class="menu_cim"><?= $menu['cim']?></div>
                <div class="menu_gombok">
                    <a class="edit" href="<?= ADMIN_URL?>_menu/menucsoportok/szerkeszt/<?= $menu['id']; ?>">Szerkesztés</a>
                    <a class="view" href="<?= ADMIN_URL?>_menu/menucsoportok/jogok/<?= $menu['id']; ?>">Korlátozások</a>
                    
                    <a class="delete" href="<?= ADMIN_URL?>_menu/menucsoportok/torol/<?= $menu['id']; ?>">Töröl</a>
                    
                    
                </div>
            </div>
            <br />
            
            <?php
            endforeach;
            ?>
        </fieldset>
        
        <?php
        endforeach;
        
    }
    
    public function menupontSzerkeszto($m){
        ?>
        <br />
        <div class="infoDiv">
            A menüpontokat a <strong>Menücsoport név</strong>vel rendezheted csoportokba (pl. Főmenü, Oldalmenü stb.). A <strong>Sorrend</strong> értéke határozza meg, 
            hogy milyen sorrendben jelenítjük meg a menüpontokat, a <strong>Szint</strong> pedig határozza meg az almenü szinteket.
        </div>
        <br />
        <fieldset>
            <form class="jNice" action="<?= ADMIN_URL?>_menu/menucsoportok" method="post">
                <input name="m[id]" type="hidden" value="<?= $m['id']?>" />
                <p>
                    <label>Menücsoport:</label>
                    <input name="m[menucsoport]" value="<?= $m['menucsoport']?>" />
                </p>
                <p>
                    <label>Cím:</label>
                    <input name="m[cim]" value="<?= $m['cim']?>" />
                </p>
                
                <p>
                    <label>Link:</label>
                    <input name="m[link]" id="link" value="<?= $m['link']?>" /><br />
                    
                    <select name="insert" onchange="$('#link').val($(this).val());">
                        <option>Már létrehozott oldal kiválasztása</option>
                        <?php
                        $sql = "SELECT cim, url FROM bow_oldalak ORDER BY cim ASC";
                        $urlek = sqluniv4($sql);
                        
                        if (!empty($urlek)) {
                            foreach ($urlek as $sor):
                            ?>
                            <option value="<?= $sor['url']?>.html"><?= $sor['cim']?></option>
                            <?php
                            endforeach;
                        }
                        
                        ?>
                    </select>
                    
                </p>
                
                <p>
                    <label>Menüszint:</label>
                    <select name="m[szint]">
                        <option value="0" <?= ($m['szint']==0)?' selected="selected" ':''?> >Főmenü</option>
                        <option value="1" <?= ($m['szint']==1)?' selected="selected" ':''?> >Al-menü</option>
                        <option value="2" <?= ($m['szint']==2)?' selected="selected" ':''?> >Al-al-menü</option>
                        
                    </select>
                </p>
                
                <p>
                    <label>Sorrend:</label>
                    <input name="m[sorrend]" value="<?= $m['sorrend']?>" />
                </p>
                
                <p>
                    <input type="submit" value="Kész"/>
                </p>
                
            </form>
        </fieldset>
        <br />
        <br />
        
        
        
        
        <?php
    }
    
    public function menuCsoportBeallito($menuId, $csoportok) {
        ?>
        <form class="jNice" method="post" action="<?= ADMIN_URL?>_menu/menucsoportok">
            <br />
            <div class="infoDiv">
                Menüpontok megjelenítését korlátozhatod ezzel a beállítással
                Ezen beállítások használata <strong>nem kötelező</strong>, Ha nem jelölsz be semmit, akkor a menüpont minden esetben megjelenik.
            </div>
            <p>
                    
                    <br /><br />
                    
                    <?php
                    
                    foreach ($csoportok as $csoport):
                        $sql = "SELECT * FROM bow_menuxcsoport WHERE menu_id = ".$menuId." AND csoport_id = ".$csoport['id'];;
                        $vanCsoport = sqluniv3($sql);
                    ?>
                        <input value="<?= $csoport['id']?>" type="checkbox" name="csxmo[]" <?= isset($vanCsoport['id'])?' checked="checked" ':''; ?> /> - <?= $csoport['nev']; ?><br style="clear: both;" />
                    <?php
                    endforeach;
                    ?>
                </p>
                <p>
                    <label> </label>
                    <input name="menu_id" value="<?= $menuId; ?>" type="hidden" />
                    <input type="submit" value="Kész" />
                </p>
                 <br /><br /><br /><br />
        </form>
        <?php
    }
}