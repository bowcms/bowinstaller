<?php

define('CSAK_KERET', 1);
$base = (dirname(dirname(dirname( __FILE__ )))) .
    '/';
include ($base . 'ajaxkeret.php');
if (isset($_GET['mcs'])) {
    $_SESSION['menuEditor']['menucsoport'] = base64_decode($_GET['mcs']);
}
if (!isset($_SESSION['menuEditor']['menucsoport']))$_SESSION['menuEditor']['menucsoport'] = '***';
$sql = "SELECT id FROM bow_menuk WHERE menucsoport = '".$_SESSION['menuEditor']['menucsoport']."'";
$rs = sqluniv3($sql);
if (!isset($rs['id'])) {
    unset ($_SESSION['menuEditor']['menucsoport']);
}

if (isset($_GET['eid'])) {
    modalSzerkeszto('Menüpont szerkesztése', (int)$_GET['eid'],(($_GET['csoportB64']!='')?base64_decode($_GET['csoportB64']):''));
    exit;
}

if (isset($_GET['tid'])) {
    sqltorles('bow_menuk',(int)$_GET['tid']);
}
if (isset($_GET['s'])) {
    $sorrendStr = trim($_GET['s'],'_');
    $sorrendArr = explode ('_',$sorrendStr);
    $sorrend = 0;
    if (!empty($sorrendArr))
    foreach($sorrendArr as $id) {
        $sql = "UPDATE bow_menuk SET sorrend = ".$sorrend." WHERE id = ".(int)$id;
        sqluniv($sql);
        $sorrend += 10;
    }
}

if (isset($_GET['szint'])) {
    $szintArr = explode('_',$_GET['szint'] );
    if (isset($szintArr[1])) {
        $sql = "UPDATE bow_menuk SET szint = ".(int)$szintArr[1]." WHERE id = ".$szintArr[0];
        sqluniv($sql);
    }
}
if (isset($_POST['cs'])) {
    $cs = $_POST['cs'];
    if ($cs['id']==0) {
        $sql = "SELECT MAX(sorrend) as maxsorrend FROM bow_menuk WHERE menucsoport = '".$cs['menucsoport']."'";
        $rs = sqluniv3($sql);
        $cs['sorrend'] = $rs['maxsorrend'] + 10;
        
    }
    $link = $_POST['tipus'.$_POST['tipus']];
    $cs['tipus'] = $_POST['tipus'];
    $cs['link'] = $link;
    
    sqladat ($cs, 'bow_menuk');
    if ($cs['id']==0) $cs['id'] = mysql_insert_id();
    if (isset($_POST['csoportxmenu'])) {
        $fcsoportok = $_POST['csoportxmenu'];    
    } else {
        $fcsoportok = array();
    }
    
    
    $sql = "SELECT id FROM bow_menuxcsoport WHERE menu_id = ".$cs['id']." ";
    $vanCoportokRs = sqluniv4($sql);
    foreach ($vanCoportokRs as $vanCoportokSor) $vanCoportok[$vanCoportokSor['id']] = $vanCoportokSor['id'];
    
    foreach ($fcsoportok as $fcsId => $fcsoport) {
        $sql = "SELECT * FROM bow_menuxcsoport WHERE menu_id = ".$cs['id']." AND csoport_id = ".$fcsoport;
        
        $vanCsop = sqluniv3($sql);
        if (!empty($vanCsop)) {
            unset($vanCoportok[$vanCsop['id']]);
            
        } else {
            $sql = "INSERT INTO bow_menuxcsoport SET menu_id = ".$cs['id']." , csoport_id = ".$fcsoport;
            
            sqluniv($sql);
        }
    }
    if (!empty ($vanCoportok)) {
        $sql = "DELETE FROM bow_menuxcsoport WHERE id IN (".implode(', ', $vanCoportok).")";
        
        sqluniv($sql);
    }
    
    
    
}

$menuk = sqluniv4("SELECT distinct(menucsoport) FROM bow_menuk ORDER BY menucsoport ASC");

foreach ($menuk as $menucsoport):
    if (!isset($_SESSION['menuEditor']['menucsoport'])) {
        $_SESSION['menuEditor']['menucsoport'] = $menucsoport['menucsoport'];
    }
    if ($_SESSION['menuEditor']['menucsoport']!=$menucsoport['menucsoport']) continue;
?>
<div class="row-fluid">
    <div class="span8" style="overflow: hidden;">
<h3><?= $menucsoport['menucsoport']?></h3>
<div class="menuBox_<?= strToUrl($menucsoport['menucsoport']); ?> menuEditorMenuBox" style="padding: 20px;">
<?php 
$sql = "SELECT * FROM bow_menuk WHERE menucsoport = '".$menucsoport['menucsoport']."' ORDER BY sorrend ASC";
$rs = sqluniv4($sql);
foreach ($rs as $k => $sor):
?>
<div class="menuRowGrid " id="menuRowGrid_<?= strToUrl($menucsoport['menucsoport']).'_'.$k; ?>" style="height: 1px;"></div>
<div class="alert alert-success menuEditorMenuRow" style="width:2000px;position: relative; left: <?= $sor['szint']*60?>px;text-align:left" data-id="<?= $sor['id']; ?>" >
    <a class=" btn"  href="javascript:void(0);" onclick="newMenuGroup(<?= $sor['id']; ?>,'')"><span class="icon icon-zoom-in"></span></a>
    <a class="btn-warning btn" href="javascript:void(0);" onclick="if(confirm('<?= __f('Biztos vagy benne?'); ?>')==true)frissitMenuLista('?tid=<?= $sor['id']?>')"><span class="icon icon-remove"></span></a>
    
    <?= $sor['cim'];?>
    </div>
<?php
endforeach;
?>
    <div class="menuRowGrid " id="menuRowGrid_<?= strToUrl($menucsoport['menucsoport']).'_'.$k+1; ?>" style="height: 1px;"></div>
</div>

<a href="javascript:void(0);" onclick="newMenuGroup(0,'<?= base64_encode($menucsoport['menucsoport']);?>')" class="btn btn-large tip-bottom"><?= __f('Új menüpont hozzáadása')?></a>
<button class="btn btn-large tip-bottom" onclick="frissitMenuLista('');" data-original-title="<?= __f('Frissítés')?>"><span class="icon-refresh"></span></button>

    </div>
    <div class="span4" >
        <?= __f('Menücsoport kiválasztása')?>:<br /><br />
        <select id="menuGroupSelect" onchange="if($(this).val()=='ujMenucsoport') newMenuGroup(); else frissitMenuLista('?mcs='+$(this).val()); ">
            <?php
            foreach ($menuk as $menucsop):
            ?>
            <option value="<?= base64_encode($menucsop['menucsoport']);?>" <?= (($menucsop['menucsoport'] == $menucsoport['menucsoport'])?' selected="selected" ':'')?> ><?= $menucsop['menucsoport'];?></option>
            <?php
            endforeach;
            ?>
            <option value="ujMenucsoport"><?= __f('Új menücsoport'); ?></option>
        </select>
    </div>
</div>
<script type="text/javascript">
    $('.menuEditorMenuRow').draggable({ helper: function( event ) {
        
        return $( "<div class='alert alert-info lebegoDiv'><?= __f('Mozgass a helyemre...')?></div>" );
      },snap: ".menuRowGrid",grid: [ 60, 10 ],stop: function() {
        xpos = ($('.lebegoDiv').css('left'));
        xpos = parseInt(xpos.replace('px',''));
        xpos = xpos-20;
        if (xpos > 90) xpos = 120;
        if (xpos < 0) xpos = 0;
        
        szint = xpos/60;
        
        $('.helyorzo').remove();
        var esemeny = false;
        var snapped = $(this).data('draggable').snapElements;

        /* Pull out only the snap targets that are "snapping": */
        var snappedTo = $.map(snapped, function(element) {
            return element.snapping ? element.item : null;
        });
        var result= '';
        $.each(snappedTo, function(idx, item) {
            result = $(item).attr('id');
        });
        if (result!='') {
            cid = $(this).attr('data-id');
            clone = $(this).clone();
            $(clone).removeClass('alert-success');
            $(clone).addClass('alert-info');
            
            $(this).remove();
            $('#'+result).after(clone);
            esemeny = true;
        }
        if (esemeny) {
            sorok = $('.menuEditorMenuRow');
            ids = '';
            for(i = 0; i < sorok.length; i++) {
                ids += $(sorok[i]).attr('data-id')+'_';
            }
            
            $('.dimLayer').show();
            $(menuScriptDiv).load(menuScriptUrl+'?s='+ids+"&szint="+cid+"_"+szint, function(){ $('.dimLayer').hide()});
        }
      },
      drag: function() {
        $(this).hide();
        var snapped = $(this).data('draggable').snapElements;

        /* Pull out only the snap targets that are "snapping": */
        var snappedTo = $.map(snapped, function(element) {
            return element.snapping ? element.item : null;
        });
        var result= '';
        $.each(snappedTo, function(idx, item) {
            result = $(item).attr('id');
        });
        if (result!='') {
            $('.helyorzo').remove();
            $('#'+result).after('<div class="alert helyorzo">&nbsp;</div>');
            
        }
        xpos = ($('.lebegoDiv').css('left'));
        xpos = parseInt(xpos.replace('px',''));
        xpos = xpos-20;
        if (xpos > 120) xpos = 120;
        if (xpos < 0) xpos = 0;
        
        $('.helyorzo').css('margin-left',xpos+"px");
        
      }
      
      });
</script>
<div class="dimLayer hide"><h3><?= __f('Betöltés')?></h3></div>
<style>
.dimLayer
{
			height:100%;
			width:100%;
			position:fixed;
			left:0;
			top:0;
			z-index:100 !important;
			background-color:white;
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
            filter: alpha(opacity=50);-moz-opacity:0.5;-khtml-opacity: 0.5;opacity: 0.5;
}
.dimLayer h3 {
    position: fixed;
    width: 100%;
    top: 50%;
    text-align: center;
}
</style>
<hr />
<?php
endforeach;


?>
<script>
    function newMenuGroup(id, csoport) {
        if (!csoport) csoport = '';
        $('#otherMenuEditModal').remove();
        $.get(menuScriptUrl+'?eid='+id + '&csoportB64=' + csoport, function(e){
            
            $('body').append(e);
            $('#otherMenuEditModal').modal('show');
            $('#otherMenuEditModal select').select2();
            
        })
    }
</script>

<?php

function modalSzerkeszto($cim = 'Új menüpont', $id = 0, $csoport = '') {
    if ($id != 0) {
        $cs = sqluniv3("SELECT * FROM bow_menuk WHERE id = ".(int)$id);
        
    }
    if (!empty ($cs)) {
        $csoport = $cs['menucsoport'];
    } else {
        $cs = array(
        'id' => 0,
        'cim' => '',
        'nyelv' => $_SESSION['__nyelv'],
        'tipus' => 1,
        'link' => ''
        );
    }
    
    if ($cs['tipus']==0) $cs['tipus'] = 1;
    ?>
<div id="otherMenuEditModal" class="modal ">
   
										<div class="modal-header">
											<button data-dismiss="modal" class="close" type="button">×</button>
											<h3><span class="icon icon-pencil"></span> <?= __f($cim); ?></h3>
                                            
										</div>
										<div class="modal-body">
											<form method="post" onsubmit="return false;" class="form-horizontal" role="form">
                                            <?php 
                                            if ($csoport != ''):
                                            ?>
                                                <input type="hidden" name="cs[menucsoport]" value="<?= $csoport; ?>" />
                                            <?php
                                            endif;
                                            ?>
                                                <input type="hidden" name="cs[id]" value="<?= $cs['id']?>" />
                                        <?php 
                                            if ($csoport == ''):
                                            ?>
                                                
                                        <div class="control-group">
										  <label class="control-label"><?= __f("Menü csoport"); ?></label>
										  <div class="controls">
											<input name="cs[menucsoport]" value="<?= $csoport; ?>" type="text" />
											
										  </div>
                                        </div>        
                                               
                                                
                                        <?php
                                            endif;
                                        ?>
                                        <div class="control-group">
										  <label class="control-label"><?= __f("Menüpont felirat"); ?></label>
										  <div class="controls">
											<input name="cs[cim]" value="<?= $cs['cim']?>" type="text" />
											
										  </div>
                                        </div>
                                               
                                        <div class="control-group">
										  <label class="control-label"><?= __f("Nyelv"); ?></label>
										  <div class="controls">
											<select name="cs[nyelv]" >
                                            <?php
                                            $nyelvek = valasztoLista('bow_nyelvek', 'kulcs', 'nev',' nev ASC ');
                                            
                                            foreach ($nyelvek as $kulcs =>  $nyelv):
                                            ?>
                                            <option value="<?= $kulcs; ?>" <?= (($kulcs==$cs['nyelv'])?' selected="selected" ':'')?>><?= $nyelv;?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                            </select>
											
										  </div>
                                        </div>
                                        
                                        <div class="control-group">
										  <label class="control-label"><?= __f("Típus és link"); ?></label>
										  <div class="controls">
											<select name="tipus" onchange="$('.menuEditFormHiddenSections').hide();$('.menuEditFormHiddenSection_'+$(this).val()).show();" >
                                                <option value="1" <?= ($cs['tipus']==1)?' selected="selected" ':''?> ><?= __f('Oldal link'); ?></option>
                                                <option value="2" <?= ($cs['tipus']==2)?' selected="selected" ':''?> ><?= __f('Külső link'); ?></option>
                                                <option value="3" <?= ($cs['tipus']==3)?' selected="selected" ':''?> ><?= __f('Komponens oldal'); ?></option>
                                                
                                            </select>
											
										  </div>
                                        </div> 
                                                
                                         <div class="control-group menuEditFormHiddenSections menuEditFormHiddenSection_1 <?= ($cs['tipus']!=1)?' hide ':''?>">
										  <label class="control-label"><?= __f("Válassz az elkészült oldalak közül"); ?></label>
										  <div class="controls">
											<select name="tipus1">
                                                <?php
                                            $lista = valasztoLista('bow_oldalak', 'url', 'cim',' cim ASC ');
                                            
                                            foreach ($lista as $kulcs =>  $nev):
                                                
                                            ?>
                                            <option  value="<?= $kulcs; ?>" <?= ($cs['link']==$kulcs)?' selected="selected" ':''; ?> ><?= $nev;?></option>
                                            <?php
                                            endforeach;
                                            ?>
                                            </select>
											
										  </div>
                                        </div>
                                        <div class="control-group menuEditFormHiddenSections menuEditFormHiddenSection_2 <?= ($cs['tipus']!=2)?' hide ':''?>">
										  <label class="control-label"><?= __f("Add meg a linket (http://example.com/site)"); ?></label>
										  <div class="controls">
											<input name="tipus2" value="<?= $cs['link']; ?>" type="text" />
											
											
										  </div>
                                        </div>
                                        <div class="control-group menuEditFormHiddenSections menuEditFormHiddenSection_3 <?= ($cs['tipus']!=3)?' hide ':''?>">
										  <label class="control-label"><?= __f("Rendelkezésre álló komponensek"); ?></label>
										  <div class="controls">
											<select name="tipus3">
                                                <?php
                                                $lista = Beepulok::$komponens;
                                                foreach ($lista as $k):
                                                ?>
                                                <option value="<?= $k['uri']; ?>" <?= ($cs['link']==$k['uri'])?' selected="selected" ':''; ?> ><?= $k['cim'];?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											
										  </div>
                                        </div>
                                        
                                        <div class="control-group">
										  <label class="control-label"><?= __f("Kik láthatják? (hagyd üresen, ha mindenki láthatja)"); ?></label>
										  <div class="controls">
											<select name="csoportxmenu[]" multiple="">
                                                <?php
                                                $felhCsoportok = valasztoLista('bow_csoportok','id','nev', ' nev ASC ');
                                                
                                                foreach($felhCsoportok as $csopId => $felhCsop):
                                                    $selected = false;
                                                    $sql = "SELECT id FROM bow_menuxcsoport WHERE menu_id = ".$cs['id']." AND csoport_id = ".$csopId;
                                                    $selected = sqluniv3($sql);
                                                    if (!empty($selected)) $selected = true;
                                                ?>
                                                <option <?= (($selected)?' selected="selected" ':'')?> value="<?= $csopId; ?>"><?= $felhCsop; ?></option>
                                                <?php
                                                endforeach;
                                                ?>
                                            </select>
											
										  </div>
                                        </div>
                                        
                                        <a class="btn btn-info " data-dismiss="modal"  href="javascript:void(0);" onclick="$('#otherMenuEditModal').hide();$(menuScriptDiv).html('<?= __f('..::betöltés::..');?>');$.post(menuScriptUrl,$(this).parents('form').serialize(), function(e){ $(menuScriptDiv).html(e); });"><?= __f('Mentés'); ?></a>
                                         
                                            </form>
										</div>

    <?php
}

