<?php
/**
 * Title: Menu Editor
 * Author: bowCms
 * Web: http://example.com
 * Description: Editing menu groups and items
 * 
 **/
 
adminMenu(__f('Menüszerkesztő'), 'unicornFomenu', __f('Menüszerkesztő'), 'menuEditorUnicorn', ' icon-list');
adminMenu(__f('Menüszerkesztő'), 'unicornOldalMenu',__f('Eszközök'), 'menuEditorUnicorn', ' icon-list');


function menuEditorUnicorn($feladat = '', $id = '') {
    global $u;
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Menüszerkesztő'));
    $html->helyorzoHozzafuzes('adminUtvonal', '<a href="'.UNIC_URL.'">'.__f('Menüszerkesztő').'</a>');
    ?>
    <div class="widget-box">
        <div class="widget-title">
								<span class="icon">
									<i class="icon-list"></i>
								</span>
								<h5><?= __f('Menüszerkesztő'); ?></h5>
							</div>
                            <div id="menuEditorUnic" style=""></div>
    </div>
    <?php
    $html->headerStart();
    ?>
    <script>
        var menuScriptDiv = '#menuEditorUnic';
        var menuScriptUrl = '<?= BASE_URL; ?>modulok/_menu/ajaxMenuEditor.php';
        $().ready(function(){
            $(menuScriptDiv).load(menuScriptUrl);
        });
        function frissitMenuLista(p) {
            $('.dimLayer').show();
            $(menuScriptDiv).load(menuScriptUrl+p, function(){
                $('.dimLayer').hide();
            });
        }
    </script>
    <?php
    $html->headerStop();
    
}