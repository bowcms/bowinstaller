<?php
/**
 * Blogmotor modul beállító
 * 
 * bow_oldalxmodul paraméter: a bow_szovegek tábla id, mást nem mentünk.
 * 
 * amit kapunk:
 * 
 * $oldalxmodulId: a modul és az oldalt össze kapcsoló tábla sorának id-je (ha nem újat viszünk fel, értéke > 0)
 * $oldalId: annak az oldalnak az id-je ahova a beállításokat és a modult mentjük
 * $modulId: ennek a modulnak az ID-je a bow_modulok táblában
 * $adat: a kapcsolt táblába mentett sor tartalma associatív tömbben
 * 
 * 
 * amit beállíthatunk: 
 * 
 * $sikeresBeallitas true ra ha megvagyunk a mentéssel, így visszakapjuk az adminban a modullistát
 * 
 * amit használhatunk:
 * 
 * _oldalak::parameterFelvitel($oldalId, $modulId, 'Bejegyzéscím', 'mentett adatok (szám szöveg, serializált tömb is lehet)', $oldalxmodulId);
 * 
 * 
 */
if (isset($adat['adat'])) if ($adat['adat'] != '') $adat = unserialize($adat['adat']); else $adat = '';

if (isset($_POST['a'])) {
    $a = $_POST['a'];
    _oldalak::parameterFelvitel($oldalId, $modulId, 'Egyedi HTML - '.$a['cim'], serialize($a), $_POST['oldalxmodul_id']);
    
    $sikeresBeallitas = true;
    $html = Html::peldany();
    $html->uzenetKiiratas('Sikeresen mentettem a szöveget!');
}



if (!isset($adat['html'])) {
    $adat['html'] = '';
    $adat['cim'] = 'Egyedi HTML';
    
}
?>
<form method="post" action="<?= EPITO_URL; ?>" class="jNice">
    
    <p>
        <label>Cím</label>
        <input type="text" name="a[cim]" value="<?= $adat['cim'];?>" />
    </p>
    <p>
        <label>HTML</label>
        <textarea  name="a[html]"  ><?= htmlspecialchars($adat['html']);?></textarea>
    </p>
    
    
    
    
    <input type="hidden" name="hozzaad" value="<?= $_POST['hozzaad'] ?>" />
    <input type="hidden" name="oldalxmodul_id" value="<?= $oldalxmodulId; ?>" />
    <p>
        <label></label>
        <input type="submit" value="Mentés" />
    </p>
</form>