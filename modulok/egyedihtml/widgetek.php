<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'HTML doboz',
        'fuggveny' => 'wgHtml',
        'leiras' => 'Egyedi html'
);
function wgHtml($a) {
    require_once(dirname(__FILE__).'/egyedihtml.php');
    $f = new Egyedihtml();
    $f->wgHtml($a['html']);
}
function wgHtmlSet($a) {
    
    if ($a=='') {
        $a = array(
            
            'html' => '<b>Kód ide...</b>'
        );
    } else {
        $a = unserialize($a);
    }
    
    
    ?>
    
    <p>
        <label>HTML:</label>
        <textarea name="a[html]"  ><?= htmlspecialchars(stripslashes($a['html']));?></textarea>
    </p>
    
    <?php
}