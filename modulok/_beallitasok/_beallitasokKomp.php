<?php

/**
 * _beallitasokKomp.php
 * 
 * konfigurációs konstansokszerkesztése
 * 
 */

class _beallitasokKomp {
    
    public function lista($csoport, $lista) {
       
        ?>
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5><?= __f($csoport['csoport']); ?></h5>
							</div>
							<div class="widget-content nopadding">
        
            
            <form method="post" action="<?= UNIC_URL; ?>" class="form-horizontal" >
                <?php
                foreach ($lista as $sor):
                ?>
                <div class="control-group">
                    <label class="control-label"><?= $sor['kulcs']?></label>
                    <div class="controls">
                    <input name="a[<?= $sor['kulcs']; ?>]" value="<?= $sor['ertek']?>" style="width: 95%;"/>
                    </div>
                </div>
                <?php
                endforeach;
                ?>
                <div class="control-group">
                    <input class=" btn btn-primary" type="submit" value="<?= __f('Kész!'); ?>"/>
                </div>
                
            </form>
        
        </div>
</div>
       
        <?php
    }
    
    public function beallitoFej() {
        ?>
        <div class="alert alert-error">
							<button class="close" data-dismiss="alert">×</button>
							<?= __f('A konfigurációs adatok szerkesztése befolyásolja az oldal működését, és el is ronthatja azt! Csak akkor szerkeszd, ha tudod mit csinálsz!')?>
        </div>
        
        <?php
    }
}