<?php

/**
 * _beallitasok.php
 * 
 * konfigurációs konstansokszerkesztése
 * 
 */

class _beallitasok {
    
    public function lista() {
        $html = Html::peldany();
        $u = osztaly_unicorn_futtato::peldany();
        if (isset($_POST['a'])) {
            foreach ($_POST['a'] as $kulcs => $ertek) {
                $sql = "UPDATE bow_config SET ertek = '".$ertek."' WHERE kulcs = '$kulcs'";
                sqluniv($sql);
            }
            $u->komp->popUpNoti(__f('A konfigurációs értékek megváltoztak!'), '');
        }
        
        $html->helyorzo('lapCim', __f('Általános beállítások'));
        $html->helyorzoHozzafuzes('adminUt', __f('Általános beállítások'));
        $sql = "SELECT DISTINCT(csoport) FROM bow_config ORDER BY csoport ASC";
        $rs = sqluniv4($sql);
        
        $this->komp->beallitoFej();
        
        foreach ($rs as $csoport) {
            $sql = "SELECT * FROM bow_config WHERE csoport = '$csoport[csoport]'";
            $csRs = sqluniv4($sql);
            $this->komp->lista($csoport, $csRs);
            
        }
    }
}