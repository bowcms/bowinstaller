<?php
/**
 * Title: General settings
 * Author: bowCms
 * Web: http://example.com
 * Description: Editing general configuration
 * 
 **/
 
adminMenu(__f('Általános beállítások'), 'unicornOldalMenu', 'beallitasokUnicorn', 'beallitasokUnicorn', 'icon-cog');

function beallitasokUnicorn($feladat = '', $id = '') {
    
    
    require_once(dirname(__FILE__).'/_beallitasok.php');
    require_once(dirname(__FILE__).'/_beallitasokKomp.php');
    $o = new _beallitasok();
    $o->komp = new _beallitasokKomp();
    $o->lista();
}