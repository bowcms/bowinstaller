<?php


class blogLinkek {
    
    function wgBloglink($cim, $oldalKulcs, $limit, $rendezesSzam = 0) {
        $kulcsok = explode ('_',$oldalKulcs);
        $oldalxmodul = trim($kulcsok[0]);
        $oldalUrl = trim($kulcsok[1]);
        $cimkeId = trim($kulcsok[2]);
        $limit = (int)$limit;
        if ($limit == 0) $limit = 3;
        $sql = "SELECT cimke FROM bow_cimkek WHERE id = $cimkeId";
        $cimRs = sqluniv3($sql);
        
        $cimke = trim($cimRs['cimke']);
        
        if ($rendezesSzam == 0)
                $rendezes = " letrehozva DESC ";
            elseif ($rendezesSzam == 1)
                $rendezes = " letrehozva ASC ";
            else
                $rendezes = " x.sorrend ASC ";

        
        $url = Url::peldany();
        $stilus = $url->stilus;
        
        $sql = "SELECT sz.cim, sz.id as id, c.id as cimke_id FROM bow_cimkek c, bow_cimkexszoveg x, bow_szovegek sz Where c.cimke = '$cimke' AND c.id = x.cimke_id AND x.szoveg_id = sz.id ORDER BY $rendezes LIMIT $limit";
       
        $rs = sqluniv4($sql);
        
        
         $linksor = '';
         $sajatLink = $url->eredetiUt;
         $sajatLink = str_replace('.html', '', $sajatLink);
                    
        foreach ($rs as $sor) {
            $sajatLink2 = BASE_URL.$oldalUrl.'/'.'bovebben/'.$oldalxmodul.'/'.$sor['id'].'/'.strToUrl($cim).'.html';
                    
            $linksor .= '<li><a href="'.$sajatLink2.'">'.$sor['cim'].'</a></li>';
        }
        
        include(htmlElem('footer_linklista'));
        
    }
    
    public function linksor($cim,$cimke,$blogoldal, $sablonFile, $limit = 3 ) {
        $cimke = trim($cimke);
        $sablonFile = trim($sablonFile);
        $blogoldal = trim($blogoldal);
        
        
        $url = Url::peldany();
        $stilus = $url->stilus;
        
        $sql = "SELECT sz.cim, sz.id as id, c.id as cimke_id FROM bow_cimkek c, bow_cimkexszoveg x, bow_szovegek sz Where c.cimke = '$cimke' AND c.id = x.cimke_id AND x.szoveg_id = sz.id ORDER BY sz.letrehozva DESC LIMIT $limit";
        
        $rs = sqluniv4($sql);
        
        
         $linksor = '';
         $sajatLink = $url->eredetiUt;
         $sajatLink = str_replace('.html', '', $sajatLink);
                    
        foreach ($rs as $sor) {
            $sajatLink2 = $sajatLink.'/bovebben/'.Bow::$aktivModulId.'/'.$sor['id'].'/'.strToUrl($cim).'.html';
                    
            $linksor .= '<li><a href="'.$sajatLink2.'">'.$sor['cim'].'</a></li>';
        }
        
        if(!file_exists(BASE.'temak/'.$stilus.'/html/'.$sablonFile)) {
                $sablon = '<strong>Blog sablon nem található: '.BASE.'temak/'.$stilus.'/html/'.$sablonFile.'</strong><br />'."<h3>\$cim</h3><ul>\$linksor</ul>";    
                file_put_contents(BASE.'temak/'.$stilus.'/html/'.$sablonFile, $sablon);
        }
        include BASE.'temak/'.$stilus.'/html/'.$sablonFile;
        
    }
}