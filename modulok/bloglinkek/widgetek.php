<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Bloglinkek',
        'fuggveny' => 'wgBloglink',
        'leiras' => 'Blog linkek'
);

function wgBloglinkSet($a) {
    
    if ($a=='') {
        $a = array(
            'cim' => 'Cím'
        );
        $a['blogoldal'] = '';
        
        $a['cimke'] = '';
        
        $a['limit'] = '';
        
        $a['rendezes'] = 0;
        
        
    } else {
        $a = unserialize($a);
    }
    $sql ="SELECT id FROM bow_modulok WHERE modul = 'blogmotor'";
    $blogId = sqluniv3($sql);
    $blogId = $blogId['id'];
    
    $sql = "SELECT x.id, x.adat, o.url, o.cim FROM bow_oldalxmodul x , bow_oldalak o WHERE x.modul_id = $blogId AND x.oldal_id = o.id";
    $oldalak = sqluniv4($sql);
    
    $sql = "SELECT cimke FROM bow_cimkek ORDER BY cimke ASC";
    $cimkeRs = sqluniv4($sql);
    ?>
    <p>
        <label>Dobozcím:</label>
        <input name="a[cim]" value="<?= $a['cim'];?>"/>
    </p>
    
    <p>
        <label>Cél bloglista oldal:</label>
        <select name="a[blogoldal]">
            <?php
            foreach ($oldalak as $sor):
                $adat = unserialize($sor['adat']);
                $kulcs = $sor['id'].'_'.$sor['url'].'_'.$adat['cimke_id'];
            ?>
            <option value="<?= $kulcs; ?>" <?= (($kulcs == $a['blogoldal'])?' selected="selected" ':'')?> ><?= $sor['cim']; ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    
    <p>
        <label>Linkek száma:</label>
        <input name="a[limit]" value="<?= $a['limit'];?>"/>
    </p>
    
    <p>
        <label>Lista rendezés:</label>
        <select name="a[rendezes]">
            <option value="0" <?= (($a['rendezes']==0)?' selected ':'')?> >Blog</option>
            <option value="1" <?= (($a['rendezes']==1)?' selected ':'')?> >Időrend</option>
            <option value="2" <?= (($a['rendezes']==2)?' selected ':'')?> >Sorrend</option>
            
        </select>
    </p>
    <?php
}