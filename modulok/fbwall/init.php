<?php
/**
 * Title: Facebook Wall
 * Author: bowCms
 * Web: http://example.com
 * Description: Displaying facebook post on my website
 * 
 **/
modulBejegyzes('Facebook wall posts', 'fbWall', false);

function fbWall($a)
{
    $base = dirname(__file__) . '/';
    include (dirname(__file__) . '/config.php');

    if (file_exists($base . 'data/' . (strtotime(date('Y-m-d 00:00:00'))))) {
        $feed = file_get_contents($base . 'data/' . (strtotime(date('Y-m-d 00:00:00'))));
    } else {
        $acces_token = cUrl("https://graph.facebook.com/oauth/access_token?client_id=$clientId&client_secret=$appSecret&grant_type=client_credentials");
        if (empty($acces_token)) {
            print '<strong>'.__f('Nem sikerült elérni a Facebook szervert').'</strong><br />';
            return;
        }
        print '**'.$acces_token.'**';
        $exp = explode('=', $acces_token);
        $acces_token = $exp[1];
        $feedLink = 'https://graph.facebook.com/135815283149646/feed?access_token=' . $acces_token;
        $feed = cUrl($feedLink);
        if (trim($feed) !='')file_put_contents($base . 'data/' . (strtotime(date('Y-m-d 00:00:00'))), $feed);

    }

    $feed = json_decode($feed, true);
    
    foreach ($feed['data'] as $sor) {
        
        if (!isset($sor['name'])) $sor['name'] = '';
        if (!isset($sor['picture'])) $sor['picture'] = '';
        if (!isset($sor['description'])) $sor['description'] = '';
        if (!isset($sor['link'])) $sor['link'] = '';
        if (!isset($sor['message'])) $sor['message'] = '';
        if (!isset($sor['story'])) $sor['story'] = '';
        
        
         
        
        include(modulHtmlElem('fbwall','egypost'));
    }
    $html = HTML::peldany();
    $html->headerStart();
    ?>
    <style>
        .feedEgypost img{
            width:20%;border:2px solid #aaa;margin: 7px;float: left;
        }
        .feedEgypost {
            clear: both;
            padding: 10px;
            margin-bottom: 10px;
            border: 1px solid #eee;
            /* IE9 SVG, needs conditional override of 'filter' to 'none' */
background: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIxJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIwLjc0Ii8+CiAgICA8c3RvcCBvZmZzZXQ9IjUzJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIwLjM1Ii8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZmZmZmYiIHN0b3Atb3BhY2l0eT0iMCIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);
background: -moz-linear-gradient(top,  rgba(255,255,255,0.74) 1%, rgba(255,255,255,0.35) 53%, rgba(255,255,255,0) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(1%,rgba(255,255,255,0.74)), color-stop(53%,rgba(255,255,255,0.35)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(255,255,255,0.74) 1%,rgba(255,255,255,0.35) 53%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(255,255,255,0.74) 1%,rgba(255,255,255,0.35) 53%,rgba(255,255,255,0) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(255,255,255,0.74) 1%,rgba(255,255,255,0.35) 53%,rgba(255,255,255,0) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(255,255,255,0.74) 1%,rgba(255,255,255,0.35) 53%,rgba(255,255,255,0) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bdffffff', endColorstr='#00ffffff',GradientType=0 ); /* IE6-8 */
            -webkit-border-radius: 10px;
-moz-border-radius: 10px;
border-radius: 10px;
        }
        .feedSzerzo {
            font-size: 12px;
            display: inline-block;
            float: right;
        }
        .feedEgypost p{
            color: #888;
        }
        
    </style>
    <?php
    $html->headerStop();
    
}
?>