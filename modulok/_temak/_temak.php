<?php

/**
 * 
 * _temak.php
 * 
 * téma könyvtárak kezelése
 */
 
class _temak{
    
    public function lista($feladat= '', $ertek = '') {
        
        $u = osztaly_unicorn_futtato::peldany();
        
        $html = Html::peldany();
        if (isset($_POST['temadir'])) {
            if ($_POST['temadir']!='' and $_POST['minta']!='') {
                
                konyvtarMasolo(BASE.'temak/'.$_POST['minta'],BASE.'temak/'.$_POST['temadir'] );
                $u->uzenetKiiras(__f('Téma másolása megtörtént!'),'');
                
            }
        }
        
        
        if ($feladat == 'bekapcsol') {
            $sql = "UPDATE bow_config SET ertek = '$ertek' WHERE kulcs = 'STILUS'";
            sqluniv($sql);
            $alapTema = $ertek;
            $u->uzenetKiiras(__f('Sikeres téma beállítás!'),'');
            
        } else {
            $alapTema = STILUS;
        }
        
        $html->helyorzo('lapCim', __f('Témakezelő'));
        $this->komp->lista($alapTema);
        
    }
    
}