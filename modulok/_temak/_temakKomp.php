<?php

/**
 * 
 * _temakKomp.php
 * 
 * témák modul html komponensei
 * 
 */

class _temakKomp
{
    public function lista($alapTema)
    {
        ?>
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5><?= __f('Táma könyvtár klónozó'); ?></h5>
							</div>
							<div class="widget-content nopadding">
        
        <br />
        <form method="post" >
            
            <select name="minta">
                <option><?= __f('Téma könyvtár másolása ez alapján:')?></option>
                <?php
        if ($handle = opendir(BASE.'temak/')) {
            $blacklist = array('adminisztracio');
            
            while (false !== ($file = readdir($handle))) {
               
                
                if (!in_array($file, $blacklist)) {
                    if (is_file(BASE.'temak/'.$file.'/tulajdonsagok.php')){
                        include(BASE.'temak/'.$file.'/tulajdonsagok.php');
                        ?>
                        <option value="<?=$file?>"><?= $t['nev'];?></option>
                        <?php
                        
                        }
                    }
                }
            }
            ?>
            </select>
            <input value="<?= __f('Témakönyvtár');?>" onclick="this.value=''" name="temadir" />
            <input type="submit" class="btn btn-danger"  value="<?= __f('Másolás');?>" />
        </form>
        <br /><br />
        <table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th><?= __f('Kép');?></th>
											<th><?= __f('Név');?></th>
											<th><?= __f('Szerző');?></th>
											<th><?= __f('Kiválasztás');?></th>
										</tr>
									</thead>
									<tbody>
        <?php
        if ($handle = opendir(BASE.'temak/')) {
            $blacklist = array('adminisztracio');
            
            while (false !== ($file = readdir($handle))) {
               
                
                if (!in_array($file, $blacklist)) {
                    if (is_file(BASE.'temak/'.$file.'/tulajdonsagok.php')):
                        $t = array();
                        include(BASE.'temak/'.$file.'/tulajdonsagok.php');
                    ?>
        <tr>
            <td style="text-align: center;"><?php if($t['kiskep'] != '') if (is_file(BASE.'temak/'.$file.'/'.$t['kiskep'])): ?><img style="width: 100;height: 100;margin:5px" src="<?= BASE_URL.'temak/'.$file.'/'.$t['kiskep']; ?>"/><?php  else: print '<img src="http://placehold.it/100x100" />';endif; ?></td>
            <td style="vertical-align: middle;"><strong><?= $t['nev']; ?></strong></td>
            <td style="vertical-align: middle;"><a href="<?= $t['szerzoLink']; ?>" target="_blank"><?= $t['szerzo']; ?></a></td>
            <td style="vertical-align: middle;">
            <?php if ($file == $alapTema):?>
            <strong>Kiválasztott alap stílus</strong>
            <?php else: ?>
            <a class="edit" href="<?= UNIC_URL; ?>bekapcsol/<?= $file; ?>" >Bekapcsolás</a>
            <?php endif; ?>
            
            </td>
            
        </tr>
            
                    <?php
                    endif;
                }
            }
            closedir($handle);
        }
        ?>
            </tbody>
        </table>
        </div>
    </div>
        <?php
    }
}
