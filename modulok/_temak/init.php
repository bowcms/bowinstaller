<?php
/**
 * Title: Themes
 * Author: bowCms
 * Web: http://example.com
 * Description: Theme settings
 * 
 **/
 
adminMenu(__f('Témakezelő'), 'unicornOldalMenu', __f('Eszközök'), 'temakUnicorn', 'icon-camera');

function temakUnicorn($feladat = '', $id = '') {
    
    
    require_once(dirname(__FILE__).'/_temak.php');
    require_once(dirname(__FILE__).'/_temakKomp.php');
    $o = new _temak();
    $o->komp = new _temakKomp();
    $o->lista($feladat, $id);
}