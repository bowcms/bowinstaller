<?php

/**
 * _adminMenu.php
 * 
 * Adminisztrációs felület menüpontjai
 * 
 */
 
class _adminMenu {
    
    public function modulkezeloLinkek() {
        
        
        ?>
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/22" >Szövegcimkék</a>
            </li>
        </ul>
        
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/9" >Szövegsablonok</a>
            </li>
        </ul>
        
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/21" >Felhasználó kezelés</a>
            </li>
        </ul>
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/26" >Bemutatkozó kártyák</a>
            </li>
        </ul>
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/17" >Levélszerkesztő</a>
            </li>
        </ul>
        <?php
        $sql = "SELECT id FROM bow_kapcsolat_uzenetek WHERE megtekintve = 0";
        $rs = sqluniv3($sql);
        if (isset($rs['id'])) {
            $stilus = "color:red;";
        } else {
            $stilus = '';
        }
        ?>
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/24" style="<?= $stilus; ?> " >Kapcsolat üzenetek</a>
            </li>
        </ul>
        
        
        
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista/beallit/15" >Weblap építőkockák</a>
            </li>
        </ul>
        <br />
         
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_modulok/telepito" >Modul telepítés</a>
            </li>
            <li>
                <a href="<?= ADMIN_URL?>_modulok/lista" >Modullista</a>
            </li>
        </ul>
        <br />
       
        <ul class="sideNav">
            <!--
            <li>
                <a href="<?= ADMIN_URL?>_temak/telepito" >Téma telepítése</a>
            </li>
            -->
            <li>
                <a href="<?= ADMIN_URL?>_temak/lista" >Témák kiválasztása</a>
            </li>
        </ul>
        <br />
        
        <ul class="sideNav">
            <li>
                <a href="<?= ADMIN_URL?>_beallitasok/lista" >Weblap konfiguráció</a>
            </li>
            <li>
                <a href="<?= ADMIN_URL?>_beepulok/lista" >Beépülő modulok</a>
            </li>
            <li>
                <a href="<?= ADMIN_URL?>_migracio/start" >Migráció</a>
            </li>
            
        </ul>
        <br />
        
        
        <?php
    }
}