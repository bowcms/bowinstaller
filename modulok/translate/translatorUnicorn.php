<?php

// vizsgáljuk a táblát megvannak-e a mezők
$sql = "SELECT * FROM bow_fordito WHERE nyelv = 'hu' ORDER BY hivo";
$rs = sqluniv4($sql);

?>
<div class="row-fluid">
					<div class="span12">
						<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-flag"></i>
								</span>
                                <h5><?= __f('Fordító tábla')?></h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th style="width: 50%;">HU</th>
											<th>EN</th>
										</tr>
									</thead>
									<tbody>
										<?php
                                        $group = '';
                                        foreach ($rs as $sor):
                                            if($sor['hivo']!=$group) {
                                                $group = $sor['hivo'];
                                                ?>
                                        <tr>
                                            <td colspan="2" style="text-align: right;background: #D9EDF7;">
                                                <strong><?= $group; ?></strong>
                                            </td>
                                        </tr>
                                                <?php
                                            }
                                            
                                        ?>
                                        <tr>
                                        <td>
                                            <input style="width: 80%;" onclick="this.select();" data-hash="<?= $sor['hash'];?>" data-lang="hu" value="<?= $sor['szoveg'];?>"  />
                                            
                                        </td>
                                        <td>
                                            <input style="width: 80%;" onclick="this.select();" data-hash="<?= $sor['hash'];?>" data-lang="en" value="<?php
                                            $sql = "SELECT szoveg FROM bow_fordito WHERE hash = '".$sor['hash']."' and nyelv = 'en' ";
                                            $eRs = sqluniv3($sql);
                                            
                                            if (isset($eRs['szoveg'])) {
                                                print $eRs['szoveg'];
                                            } else {
                                                print 'NONE';
                                            }
                                            ?>" />
                                        </td>
                                        </tr>
                                        <?php
                                        endforeach;
                                        ?>
									</tbody>
								</table>							
							</div>
						</div>
						
				</div>
</div>
<?php

$html->headerStart();

?>
<script>
    $().ready(function(){
        $('.widget-box input').keyup(function (e) {
            if (e.keyCode == 13) {
                // Do something
                $(this).blur();
            }
        });
        $('.widget-box input').blur(function () {
            
            transInputSave(this);
            
        });
    })
function transInputSave(o) {
    $(o).addClass('savingTime');
    var json = {
        'a[hash]' : $(o).attr('data-hash'),
        'a[nyelv]' : $(o).attr('data-lang'),
        'a[szoveg]' : $(o).val()
    };
    
    $.post('<?= BASE_URL; ?>modulok/translate/saveItem.php', json, function(){
        $(o).removeClass('savingTime');
    });
}
</script>
<style>
    .savingTime {
        background: purple;
        color: #fff;
    }
    
</style>
<?php

$html->headerStop();



