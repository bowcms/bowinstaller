<?php

/**
 * Title: Translator
 * Author: bowCms
 * Web: http://example.com
 * Description: Site translator
 * 
 **/


adminMenu(__f('Fordító'), 'unicornOldalMenu',__f('Eszközök'), 'fordirtoUnicorn', 'icon-flag');

function fordirtoUnicorn($feladat = '', $id = 0) {
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Fordító'));
    $html->helyorzoHozzafuzes('adminUtvonal', '<a href="' . UNIC_URL . '">' . __f('Fordító') .
        '</a>');
    include(dirname(__FILE__).'/translatorUnicorn.php');
    
}