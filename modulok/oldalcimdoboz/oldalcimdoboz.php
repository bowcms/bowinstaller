<?php

class oldalcimdoboz {
    
    public function wgoldalcim() {
        $url = URL::peldany();
        $oldalId = $url->oldal_id;
        if((int)$oldalId == 0) return;
        $stilus = $url->stilus;
        $sql = "SELECT * FROM bow_oldalak WHERE id = $oldalId";
        $adat = sqluniv3($sql);
        
        $sablonFile = 'ep_oldalcim_doboz.php';
        if(!file_exists(BASE.'temak/'.$stilus.'/html/'.$sablonFile)) {
                $sablon = '<strong><?= $adat["cim"]; ?></strong>';    
                file_put_contents(BASE.'temak/'.$stilus.'/html/'.$sablonFile, $sablon);
        }
        include(BASE.'temak/'.$stilus.'/html/'.$sablonFile);
    }
    
}