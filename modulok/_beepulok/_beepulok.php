<?php

/**
 * _beepulok.php
 * 
 * beépülő scriptek szerkesztések
 * 
 */
 
class _beepulok {
    
    public function lista($feladat = '', $id = 0) {
        
        $html = Html::peldany();
        $html->helyorzo('lapCim', 'Beépülők szerkesztése');
        
        if ($feladat == 'torol') {
            $id = (int)$id;
            $beepulo = getRows('bow_beepulok', $id);
            @unlink (BASE.'beepulok/'.$beepulo['file']);
            sqltorles('bow_beepulok', $id);
            $html->uzenetKiiratas('Törlés elvégezve!');
            
        }
        
        if (isset($_POST['b'])) {
            $b = $_POST['b'];
            
            if (!isset($b['kikapcsolva'])) $b['kikapcsolva'] = 0;
            $sikerult = false;
            $file = trim($b['file']);
            if ($file=='') {
                $html->hibaKiiratas('Nem adtál meg filenevet!');
            } else {
                $kit = explode('.', $file);
                if (strtolower(end($kit))!='php') {
                    $html->hibaKiiratas('PHP kiterjesztés szükséges!');
                } else {
                    if (trim($b['nev'])=='') {
                        $html->hibaKiiratas('Nem adtad meg a beépülő nevét!');
                    } else {
                        
                        sqladat($b, 'bow_beepulok');
                        
                        $s = file_put_contents(BASE.'beepulok/'.$file, $_POST['kod']);
                        $html->uzenetKiiratas('Beépülő mentése sikeres!');
                        $sikerult = true;
                    }
                }
            }
            if ($sikerult == false) {
                // visszadobjuk a szerkesztőbe
                $html->helyorzo('lapCim', '<a href="'.ADMIN_URL.'_beepelok/lista">Beépülők listája</a> | Szerkesztés');
                $this->komp->szerkeszto($b);
                return;
            }
        }
        
        if ($feladat == 'szerkeszt') {
            $html->helyorzo('lapCim', '<a href="'.ADMIN_URL.'_beepelok/lista">Beépülők listája</a> | Szerkesztés');
            $id = (int)$id;
            $b = array('nev' => '', 'file' => '', 'belepes' => '', 'kikapcsolva' => 1,'sorrend' => 0);
            
            if ($id>0) {
                $sql = "SELECT * FROM bow_beepulok WHERE id = $id";
                $rs = sqluniv3($sql);
                if (!empty($rs)) {
                    $b = $rs;
                }
            }
            $this->komp->szerkeszto($b);
            return;
        }
        
        $sql = "SELECT * FROM bow_beepulok ORDER BY nev ASC";
        $rs = sqluniv4($sql);
        $this->komp->fej();
        
        $this->komp->lista($rs);
        
        
    }
    
}