<?php

/**
 * _beepulokKomp.php
 * 
 * beépülő scriptek szerkesztések
 * 
 */

class _beepulokKomp
{

    public function fej()
    {
?>
        <br />
        <div class="infoDiv">
            A beépülő programok segítségével a programfutás különböző pontjain tudunk scripteket futtatni. 
            Ezzel könnyen testreszabható a tartalomkezelő működése.<br />
            <strong>Figyelem! </strong> A beépülő programok szerkesztése befolyásolja az oldal működését, és el is ronthatja azt! 
            Csak akkor szerkeszd, ha tudod mit csinálsz!
        </div>
        <br />
        <form action="<?= ADMIN_URL ?>_beepulok/lista/szerkeszt/0" class="jNice">
            <input type="submit" value="Új beépülő létrehozása"/>
        </form>
        
        <br style="clear: both;"/><br />
        <?php
    }
    
    public function lista($lista) {
        ?>
        <table cellpadding="0" cellspacing="0" >
            <?php
            foreach ($lista as $sor):
            ?>
            <tr>
                <td><?= $sor['nev']?></td>
                <td><?= $sor['file']?></td>
                <td><?= $sor['file']?></td>
                <td><?= (($sor['kikapcsolva']==0)?'<b style="color:green">BE</b>':'<b style="color:red">KI</b>');?></td>
                <td class="action">
                    <a class="edit" href="<?= ADMIN_URL.'_beepulok/lista/szerkeszt/'.$sor['id']?>">Szerkesztése</a>
                    <a class="delete" href="<?= ADMIN_URL.'_beepulok/lista/torol/'.$sor['id']?>" onclick="if(confirm('Biztos')==false) return false;">Törlés</a>
                    
                    
                </td>
                
                
            </tr>
            <?php
            endforeach;
            ?>
            
        </table>
        <br /><br />
        <?php
    }

    public function szerkeszto($b, $hiba = array())
    {
        require_once (BASE . 'osztaly/osztaly_jsszerkeszto.php');
        jsSzerkeszto::kodSzerkeszto('kod', 'php');
        
?>
        <br />
        <fieldset>
        <form action="<?= ADMIN_URL; ?>_beepulok/lista" method="post" class="jNice">
            <input type="hidden" name="b[id]" value="<?= $b['id']; ?>"/>
            <p>
                <label>A beépülő funkciója</label>
                <input name="b[nev]" value="<?= $b['nev'] ?>" />
            </p>
            <p>
                <label>Sorrend</label>
                <input name="b[sorrend]" value="<?= $b['sorrend'] ?>" />
            </p>
            
            <p>
                <label>File-név (.php kiterjesztéssel együtt)</label>
                <input name="b[file]" value="<?= $b['file'] ?>" />
            </p>
            <p>
                <label>Belépési pont</label>
                <select  name="b[belepes]" >
                    <?php
        $belepesek = getAll('bow_beepulo_belepesek', 'sorrend');
        foreach ($belepesek as $belepes):
?>
                    <option value="<?= $belepes['nev'] ?>" <?= (($b['belepes'] ==$belepes['nev']) ? ' selected="selected" ' : ''); ?>><?= $belepes['nev'] ?></option>
                    <?php
        endforeach;
?>
                </select>
                
            </p>
            
            <p>
                <label>Letiltás bekapcsolása (Ki: fut a beépülő, Be: nem fut a beépülő)</label>
                <input name="b[kikapcsolva]" value="1" <?= (($b['kikapcsolva']==1)?' checked="checked" ':''); ?> type="checkbox" />
            </p>
            
            <p>
                <label>PHP kód</label>
                <textarea id="kod" name="kod"><?php
        if (is_file(BASE . 'beepulok/' . $b['file'])) {
            print htmlspecialchars(file_get_contents(BASE . 'beepulok/' . $b['file']));
        } else {
            print htmlspecialchars("<?php\n // Beépülő kódja ide\n\n");
        }
            
        
        ?></textarea>
            </p>
            <p>
                <input value="Kész" type="submit" />
            </p>
        </form>
        </fieldset>
        <br />
        <?php
    }

}
