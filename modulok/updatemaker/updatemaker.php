<?php

class updatemaker {
    
    public function admin($feladat, $id) {
        $u = osztaly_unicorn_futtato::peldany();
        
        if (isset($_FILES['zip'])) {

            $oName = $_FILES['zip']['name'];
            $kit = explode('.', $oName);
            $modulnev = $kit[0];
            $kit = end($kit);

            if ($kit != 'zip') {
                $u->komp->uzenetKiiras(__f('ZIP file feltöltését várjuk!') ,'', 'warning');

            }
            $name = $oName;
            $ut = BASE . 'tmp/';
            if (!move_uploaded_file($_FILES['zip']['tmp_name'], $ut . $name)) {
                $u->komp->uzenetKiiras(__f('ZIP file feltöltése sikertelen!'), '', 'warning');
            } else {
                // kicsomagolás

                $zip = new ZipArchive;
                $res = $zip->open($ut . $name);
                if ($res === true) {
                    $zip->extractTo(BASE );
                    $zip->close();

                    // tmp törlse
                    unlink($ut . $name);
                    $u->uzenetKiiras(__f('Zip file kicsomagolásra került'), __f('A kitömörítés helye:').' '.BASE);
                } else {
                    $u->uzenetKiiras(__f('Hiba!'),__f('ZIP kicsomagolása sikertelen!'));
                }
            }
        }
        
        if (isset($_POST['kiaddatum'])) {
            $datum = $_POST['kiaddatum'];
            $filename = 'update_'.date('YmdHi').'.zip';
            $cel = BASE.'modulok/updatemaker/UPDATE/'.date('YmdHi').'/';
            $url = BASE_URL.'modulok/updatemaker/UPDATE/'.date('YmdHi').'/';
            
            $mappa = BASE;
            frisebbMasolasa($datum, $mappa, $cel);
            Zip($cel, $cel.$filename);
            
            $u->uzenetKiiras(__f('Frissitési példány létrejött itt: '),'<a href="'.$url.$filename.'" target="_blank">'.$url.$filename.'</a>');
        }
        
        $u->komp->adminFormFej(__f('Frissítés készítése'));
        $u->komp->adminFormInput(__f('Kiadás dátumtól: (YYYY-mm-dd HH:ii)'),'kiaddatum',date('Y-m-d').' 00:00');
        ?>
        <div class="control-group">
            <label class="control-label"><?= __f ('Téma könyvtár másolása?')?></label>
            <div class="controls">
                <input type="checkbox" value="1" name="temakonyvtar" />
            </div>
        </div>
        <?php
        $u->komp->adminFormLab();
        
        $u->komp->adminFormFej(__f('Frissítés feltöltése (ZIP)'));
        ?>
        <div class="control-group">
            <label class="control-label"><?= __f('Zip file');?></label>
            <input type="file" name="zip" />
        </div>
        <?php
        $u->komp->adminFormLab();
        
        
    }
    
}

function frisebbMasolasa($datum, $mappa, $cel) {
    $ido = strtotime($datum);
    $dir =  $mappa;
    if (isset($_POST['temakonyvtar'])) $konyvtar = 'temak'; else $konyvtar = '';
    if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            if ($file == '.' or $file == $konyvtar or $file == '..' or $file == 'UPDATE' or $file == 'feltoltesek' or $file == '.git' or $file == 'beallitas' or $file == 'tmp') continue;
            
            $fido = filemtime($dir.$file);
             
            if ($fido) {
                if (true) {
                    //print 'updated: '.$dir.$file.'<br />';
                    if (is_dir($dir.$file)) {
                        chmod($dir,0755);
                        //mkdir($cel.$file,0777,true);
                        //print 'mkdir: '.$cel.$file.'<br />';
                        frisebbMasolasa($datum, $mappa.$file.'/', $cel.$file.'/');
                    }
                    if (is_file($dir.$file)) {
                        //print 'Ez file...';
                        if ($fido > $ido) {
                            print 'copy: '.$cel.$file.'<br />';
                            if(!is_dir($cel)) mkdir($cel,0777,true);
                            copy($dir.$file, $cel.$file);
                        }
                    }
                }
            }
        }
        closedir($dh);
    }
}
    
}

function Zip($source, $destination)
{
    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }

    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
        
        foreach ($files as $file)
        {
            
            $file = str_replace('\\', '/', $file);
            $base = str_replace('\\', '/', $source);
            $base = trim($base, '/');
            $base .= '/';
           
            
            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            $file = str_replace('\\', '/',realpath($file));
            $dirNev = str_replace($base, '', $file);
            
            if (is_dir($file) === true)
            {
                
                
                $zip->addEmptyDir($dirNev.'/');
            }
            else if (is_file($file) === true)
            {
                $fileNev = str_replace($base, '', $file);
                $zip->addFromString($fileNev, file_get_contents($file));
            }
        }
    }
    
    return $zip->close();
}

?>