<?php
/**
 * Feliratkozó modul beállító
 * 
 * bow_oldalxmodul paraméter: a bow_szovegek tábla id, mást nem mentünk.
 * 
 * amit kapunk:
 * 
 * $oldalxmodulId: a modul és az oldalt össze kapcsoló tábla sorának id-je (ha nem újat viszünk fel, értéke > 0)
 * $oldalId: annak az oldalnak az id-je ahova a beállításokat és a modult mentjük
 * $modulId: ennek a modulnak az ID-je a bow_modulok táblában
 * $adat: a kapcsolt táblába mentett sor tartalma associatív tömbben
 * 
 * 
 * amit beállíthatunk: 
 * 
 * $sikeresBeallitas true ra ha megvagyunk a mentéssel, így visszakapjuk az adminban a modullistát
 * 
 * amit használhatunk:
 * 
 * _oldalak::parameterFelvitel($oldalId, $modulId, 'Bejegyzéscím', 'mentett adatok (szám szöveg, serializált tömb is lehet)', $oldalxmodulId);
 * 
 * 
 */
if (isset($_POST['feliratkozo_email'])) {
    $adat = array(
        'cim' => $_POST['feliratkozo_cim'],
        'leiras' => $_POST['feliratkozo_leiras'],
        'epito_id' => $_POST['epito_id']
        
    );
    $adat = serialize($adat);
    _oldalak::parameterFelvitel($oldalId, $modulId, 'Feliratkozó - '.$_POST['cimke'], $adat, $_POST['oldalxmodul_id']);
    // ezzel jelezzük, hogy kész vagyunk
    $sikeresBeallitas = true;
    $html = Html::peldany();
    $html->uzenetKiiratas('Sikeresen mentettem a szöveget!');
} else {

    $sql = "SELECT * FROM bow_epitokocka WHERE modulsablon = 1 ORDER BY nev ASC";
    $epito = sqluniv4($sql);
    
    if (isset($adat['adat'])) {
        $adat = unserialize( stripslashes($adat['adat']));        
        $id = $adat['epito_id'];
        $cim = $adat['feliratkozo_leiras'];
        $leiras = $adat['feliratkozo_leiras'];s
    } else {
        $id = 0;
        $cimke = '';
        $link = '';
    }

    
?>
<form method="post" action="<?= EPITO_URL; ?>" class="jNice">
    
    
    <p>
        <label>Válassz a korábban létrehozott pítőkövekből:<br /><i>Építőkövek létrehozhatóak a modulbeállításoknál</i></label>
        <select name="epito_id">
            <?php
            foreach ($epito as $sor):
            ?>
            <option value="<?= $sor['id']?>" <?= (($id==$sor['id'])?' selected="selected" ':'')?> ><?= $sor['nev']?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    <p>
        <label>Szövegcimke:</label>
        <input name="cimke" value="<?= $cimke; ?>" />
    </p>
    <p>
        <label>Link:</label>
        <input name="link" value="<?= $link; ?>" />
    </p>
    <p>
        <label> </label>
        <input type="submit" value="Kész vagyok" />
    </p>
    <!-- a köv 2 paramáter megadása fontos, ez vezérli a tartalom hozzáfűzést -->
    <input type="hidden" name="hozzaad" value="<?= $_POST['hozzaad'] ?>" />
    <input type="hidden" name="oldalxmodul_id" value="<?= $oldalxmodulId; ?>" />
    
    
    
</form>

<?php
}
