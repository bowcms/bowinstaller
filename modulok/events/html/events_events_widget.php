<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><?= $cim; ?></div>
  

  <!-- List group -->
  <ul class="list-group">

    <?php
    foreach($rs as $sor):
    ?>
    
    <li class="list-group-item">
        
        
        
        <div class="datum">
            <strong><?= date('d',strtotime($sor['datum']));?></strong>
            <span><?= $honapok[(int)date('m')] ;?></span>
        </div>
        
        <strong><?= $sor['cim']?></strong>
        <div class="clearfix"></div>
    </li>
    
    <?php
    endforeach;
    ?>
    </ul>
</div>