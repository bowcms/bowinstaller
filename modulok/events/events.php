<?php

class events {
    
    public function megjelenites($a) {
        $cim = $a['cim'];
        $sql = "SELECT * FROM bow_esemenyek WHERE datum >= '".date('Y-m-d', time()-24*60*60)."' ORDER BY datum ASC LIMIT ".$a['mennyiseg'];
        $rs = sqlAssocArr($sql);
        $honapok = array(
            1 => 'január',
            2 => 'február',
            3 => 'március',
            4 => 'április',
            5 => 'május',
            6 => 'június',
            7 => 'július',
            8 => 'augusztus',
            9 => 'szeptember',
            10 => 'október',
            11 => 'november',
            12 => 'deccember'
            
        );
        include(modulHtmlElem('events', 'widget'));
        $html = Html::peldany();
        $html->headerStart();
        include(modulHtmlElem('events', 'css'));
        $html->headerStop();
        
        
    }
    
    public function beallitas($a) {
        if ($a=='') {
        $a = array(
            'cim' => 'Cím',
            'mennyiseg' => 5
        );
    } else {
        $a = unserialize($a);
    }
    
    
        ?>
        <div class="control-group">
            <label><?= __f('Doboz felirat:')?>:</label>
            <div class="controls">
                <input name="a[cim]" value="<?= $a['cim'];?>"/>
            </div>
        </div>
        <div class="control-group">
            <label><?= __f('Megjelenített sorok:')?>:</label>
            <div class="controls">
                <input name="a[mennyiseg]" value="<?= $a['mennyiseg'];?>"/>
            </div>
        </div>
        
        <div class="esemenyLista"></div>
        <script type="text/javascript">
            $('.esemenyLista').load('<?= BASE_URL?>modulok/events/eventsAjaxUnicorn.php');
            
        </script>
        <?php
        
        
    }
    
    public function install() {
        $sql = "
        CREATE TABLE IF NOT EXISTS `bow_esemenyek` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `datum` date NOT NULL,
  `cim` varchar(255) NOT NULL,
  `leiras` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1
    ";
        sqluniv($sql);
        return __f('Adatbázis tábla létrejött.');
    }
}