<?php

define('CSAK_KERET', 1);
$base = (dirname(dirname(dirname( __FILE__ )))) .
    '/';
include ($base . 'ajaxkeret.php');

$tag = Tagok::peldany();
if (!$tag->admin) exit;

if (isset($_GET['tid'])) {
    sqltorles('bow_esemenyek', (int)$_GET['tid']);
    print '<div class="alert alert-success">'.__f('Esemény törlése megtörtént!').'</div>';
}

if (isset($_POST['e'])) {
    if ($_POST['e']['cim'] != '' and $_POST['e']['datum']!='') {
        $e = $_POST['e'];
        $e['datum'] = date('Y-m-d',strtotime($e['datum']));    
        sqladat($e, 'bow_esemenyek');
    }
    else 
        print '<div class="alert alert-info">'.__f('Cím és dátum megadása kötelező!').'</div>';
}

$sql = "SELECT * FROM bow_esemenyek ORDER BY datum DESC ";
$rs = sqlAssocArr($sql);
?>
<div class="control-group">
    <div class="controls">
        <input name="e[cim]" placeholder="<?= __f('Új esemény címe');?>"/> <input placeholder="<?= __f('Dátum'); ?>" name="e[datum]" value="" class="datumMezo" /><br />
        <br />
        <textarea placeholder="<?= __f('Leírás')?>" style="height: 60px;"></textarea>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="button" class="btn btn-info" onclick="$.post('<?= BASE_URL?>modulok/events/eventsAjaxUnicorn.php', $('#myModaForm').serialize(), function (e){$('.esemenyLista').html(e);});"><?= __f('Új esemény mentése');?></button>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    
    $('.datumMezo').datepicker( { dateFormat : "yy-mm-dd" });
    
</script>

<div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-refresh"></i>
								</span>	
								<h5><?= __f('Események');?></h5>
							</div>
							
							<div class="widget-content nopadding updates">
								
<?php

foreach ($rs as $sor) {
    ?>
    <div class="new-update clearfix">
									<i class="icon-remove" style="color: red;cursor:pointer" title="<?= __f('Törlés')?>" onclick="$('.esemenyLista').load('<?= BASE_URL?>modulok/events/eventsAjaxUnicorn.php?tid=<?= $sor['id']; ?>');"></i>
									<div class="update-done">										
										<strong><?= $sor['cim']?></strong>
										
									</div>
									<div class="update-date"><span class="update-day"><?= date('d', strtotime($sor['datum']))?></span><?= date('M', strtotime($sor['datum']))?></div>
									
    </div>
    
    <?php
}		
?>					
							</div>
						</div>



