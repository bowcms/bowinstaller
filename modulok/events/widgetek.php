<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Events',
        'fuggveny' => 'wgEvents',
        'leiras' => 'Events listing'
);

function wgEventsSet($a){
    $e = modulOsztalyTolto('events');
    $e->beallitas($a);
}

function wgEvents($a) {
    $e = modulOsztalyTolto('events');
    $e->megjelenites($a);
}