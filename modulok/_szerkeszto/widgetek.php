<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Post Widget',
        'fuggveny' => 'wgPost',
        'leiras' => 'Post kirakása widgetbe'
);

function wgPost($a) {
    
    $sql = "SELECT * FROM bow_szovegek WHERE id = ".$a['szoveg_id'];
    $rs = sqlAssocRow($sql);
    print $a['elo_html'];
    print $rs['tartalom'];
    print $a['uto_html'];
    
    
}

function wgPostSet($a) {
    
    if ($a=='') {
        $a = array(
            'szoveg_id' => 0,
            'elo_html' => '',
            'uto_html' => ''
            
        );
        
        
    } else {
        $a = unserialize($a);
    }
    $sql = "SELECT * FROM bow_szovegek ORDER BY cim ASC ";
    $rs = sqlAssocArr($sql);
    ?>
    <p>
        <label>Post:</label>
        <select name="a[szoveg_id]">
            <?php
            foreach($rs as $sor):
            ?>
            <option value="<?= $sor['id']?>" <?= (($a['szoveg_id']==$sor['id'])?' selected="selected" ':'')?>><?= $sor['cim']?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    <p>
        <label><?= __f('HTML a szöveg előtt'); ?></label>
        <textarea name="a[elo_html]"><?= htmlspecialchars($a['elo_html'])?></textarea>
    </p>
    <p>
        <label><?= __f('HTML a szöveg után'); ?></label>
        <textarea name="a[uto_html]"><?= htmlspecialchars($a['uto_html'])?></textarea>
    </p>
    
  
    <?php
}