<?php
/**
 * Title: Post Editor
 * Author: bowCms
 * Web: http://example.com
 * Description: Editing posts
 * Role: system
 * 
 **/


adminMenu('Post Editor', 'unicornFomenu', 'Post Editor', 'postEditorUnicorn', 'icon-pencil');
adminMenu('Post Editor', 'unicornOldalMenu',__f('Weboldal'), 'postEditorUnicorn', 'icon-pencil');


function postEditorUnicorn($feladat = '', $id = '') {
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Post Editor'));
    include (dirname(__FILE__).'/unicornPostEditor.php');
}

            
?>