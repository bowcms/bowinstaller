<?php
$szerkestesKesz = true;
$html = Html::peldany();

if(isset($_POST['sorrendment'])) {
    $sorrend = $_POST['sorrend'];
    foreach($sorrend as $k => $sor) {
        if ($sor == 0) continue;
        $kulcs = str_replace('c', '', $k);
        $kulcs = explode('_', $kulcs);
        $szovegId = $kulcs[1];
        $cimkeId = $kulcs[0];
        $sql = "UPDATE bow_cimkexszoveg SET sorrend = $sor WHERE szoveg_id = $szovegId AND cimke_id = $cimkeId ";
        sqluniv($sql);
        
    }
}

if (isset($_POST['sz'])) {
    $sz = $_POST['sz'];
    
    sqladat($sz, 'bow_szovegek');
    if ($sz['id'] == 0) {
        $sz['id'] = mysql_insert_id();
        
    }
    
    //főkép
    if ($_FILES['fokep']['name']!='') {
        $nev = $_FILES['fokep']['name'];
        $nevArr = explode('.',$nev);
        $kiterjesztes = end($nevArr);
        $ujNev = 'cikkkep'.time().'_'.rand(100,999).'.'.$kiterjesztes;
        if (!move_uploaded_file($_FILES['fokep']['tmp_name'], BASE.'feltoltesek/cikkek/'.$ujNev)){
            $html->hibaKiiratas('Kép feltöltése sikertelen!');
            
            
        } else {
            $a['fokep']=$ujNev;
            $a['id'] = $sz['id'];
            sqladat($a, 'bow_szovegek');
        }
    }
    if (isset($_POST['fokeptorles'])) {
        $adat = getRows('bow_szovegek', $sz['id']);
        unlink(BASE.'feltoltesek/cikkek/'.$adat['fokep']);  
        $a['fokep']='';
        $a['id'] = $sz['id'];
        sqladat($a, 'bow_szovegek');
        
    }
    if ($_POST['fokepfeluliras']!='') {
        @copy(BASE_URL.'feltoltesek/cikkgaleria/'.$sz['id'].'/'.$_POST['fokepfeluliras'],BASE.'feltoltesek/cikkek/'. $_POST['fokepfeluliras']);
        
        
        $a['fokep']=$_POST['fokepfeluliras'];
        $a['id'] = $sz['id'];
        sqladat($a, 'bow_szovegek');
        
    }
    
    
    if (isset($_POST['cimkek'])) {
        $cimkekSor = array();
        foreach ($_POST['cimkek'] as $cimke) {
            $cimke = explode('_', $cimke);
            $a = array();
            $a['cimke_id'] = $cimke[1];
            $cimkekSor[] = $a['cimke_id'];
            $a['szoveg_id'] = $sz['id'];
            
            $sql = "SELECT * FROM bow_cimkexszoveg WHERE cimke_id = ".$a['cimke_id']." AND szoveg_id = ".$a['szoveg_id'];
            $vanCimke = sqluniv3($sql);
            $vanCimke = (isset($vanCimke['cimke_id'])?true:false);
            if ($vanCimke) continue;
            
            sqlfelvitel($a, "bow_cimkexszoveg");

        }
        
    }
    if (empty($cimkekSor))$cimkekSor = array(-1);
    $sql = "DELETE FROM bow_cimkexszoveg WHERE szoveg_id = ".$sz['id']." AND cimke_id NOT IN (".implode(', ', $cimkekSor).")";
    
    sqluniv($sql);
    
    
    $html->uzenetKiiratas('Szöveg mentse sikeres');
    $szerkestesKesz = true;
}

if (isset($_GET['szerkeszt'])) {
    $szerkestesKesz = false;
    include ('szovegszerkeszto.php');
}
if (isset($_GET['torles'])) {
    $id = (int)$_GET['torles'];
    sqltorles('bow_szovegek', $id);
    sqltorles('bow_cimkexszoveg', $id, 'szoveg_id');
    $sql = "DELETE FROM bow_oldalxmodul WHERE adat = $id and modul_id = 9";
    sqluniv($sql);
    $html = Html::peldany();
    $html->uzenetKiiratas('Törlés megtörtént. Figyelem: az oldal építőben kirakott szöveg nyoma megmarad az oldalépítőben, ezeket helyben mg törölni kell!');
    
}
if ($szerkestesKesz == false) return;
$talalatszam = 10;

if (!isset($_SESSION['start'])) {
    $_SESSION['start'] = 0;
}
if (!isset($_SESSION['cimke'])) {
    $_SESSION['cimke'] = '';
}
if (!isset($_SESSION['kereses'])) {
    $_SESSION['kereses'] = '';
}

if (isset($_GET['kerstr'])) {
    $_SESSION['kereses'] = $_GET['kerstr'];
}
if (isset($_GET['cimkeszuro'])) {
    $_SESSION['cimke'] = $_GET['cimkeszuro'];
}



$where = array();
if ($_SESSION['cimke'] != '') {
    $sql = "SELECT szoveg_id FROM bow_cimkek c, bow_cimkexszoveg x WHERE x.cimke_id = c.id AND cimke LIKE '".$_SESSION['cimke']."' ";
    $rs = sqluniv4($sql);
    if (!empty ($rs)) {
        foreach ($rs as $cid) {
            $idArr[] = $cid['szoveg_id'];
        }
        $where[] = ' id IN ('.implode(' , ', $idArr).' ) ';
    }
}

if ($_SESSION['kereses']!= '') {
    $str = $_SESSION['kereses'];
    $where[] = " ( cim LIKE '%$str%' OR bevezeto LIKE '%$str%' OR tartalom LIKE '%$str%' ) ";
}
if (!empty($where)) {
    $where = implode(' and ', $where);
} else {
    $where = ' 1 = 1 ';
}

$talalat = sqltalalat("SELECT * FROM bow_szovegek WHERE $where ");

if (isset($_GET['elore'])) {
    if ($talalat > $_SESSION['start']+$talalatszam) {
        $_SESSION['start']+=$talalatszam;
        if ($_SESSION['start']> $talalat) $_SESSION['start'] = $talalat-$talalatszam;
    }
}
if (isset($_GET['vissza'])) {
    if ($_SESSION['start']>0) {
        $_SESSION['start']-=$talalatszam;
        if ($_SESSION['start']<0) $_SESSION['start'] = 0;
    }
}
if ($_SESSION['start'] > $talalat) {
    $_SESSION['start'] = 0;
}

$limit = " LIMIT ".$_SESSION['start'].", $talalatszam ";

$sql = "SELECT * FROM bow_szovegek WHERE $where ORDER BY cim ASC $limit";

$szovegek = sqluniv4($sql);

?>
<form method="post" action="<?= $sajatUrl; ?>?szerkeszt=0" class="jNice" enctype="multipart/form-data" style="display: inline-block;float: right;">
    <input type="submit" value="Új bejegyzés"/>
</form>
<h3>Cimkék beállítsa</h3>
<br class="clear"/>
<form action="<?= $sajatUrl; ?>" class="jNice" enctype="multipart/form-data" >
    <select name="cimkeszuro">
        <option value="">Nincs cimke szerinti szűrés</option>
        <?php
            $sql = "SELECT * FROM bow_cimkek ORDER BY cimke ASC";
            $cimkek = sqluniv4($sql);
            foreach ($cimkek as $cimke):
            ?>
            <option value="<?= $cimke['cimke']; ?>" <?= (($_SESSION['cimke']==$cimke['cimke'])?' selected="selected" ':'') ?> ><?= $cimke['cimke']; ?></option>
            <?php
            endforeach;
        ?>
    </select>&nbsp;&nbsp;
    Keresés: <input name="kerstr" value="<?= $_SESSION['kereses']?>" />
    
    <input type="submit" value="OK" style="float: right;;"/>
</form>
<br class="clear" /><br />
<p>Megjelenítve <?= $_SESSION['start']; ?> - <?= $_SESSION['start']+$talalatszam; ?> / <?= $talalat; ?>
<?php
if ($talalat > $talalatszam) {
    if ($_SESSION['start']> 0) {
        ?>
        <a href="<?= $sajatUrl; ?>?vissza=1" class="adminLapozo">Vissza</a>
        <?php
        
    }
    if ($_SESSION['start']+$talalatszam<  $talalat) {
        ?>
        <a href="<?= $sajatUrl; ?>?elore=1" class="adminLapozo">Előre</a>
        <?php
        
    }
    
}
?>
</p>
<form method="post">
<table class="jNice">
<?php
if (!empty($szovegek))
    foreach ($szovegek as $sor):
        $bevezeto = wordwrap(htmlspecialchars($sor['bevezeto']),100,"[brk]");
        $bevezeto = explode("[brk]",$bevezeto);
        $bevezeto = $bevezeto[0];
?>
    <tr>
        <td><?= $sor['cim'];?></td>
        <td><?= $bevezeto;?></td>
        <td style="color: orange;"><?php 
        $sql = "SELECT cimke FROM bow_cimkexszoveg cx, bow_cimkek c WHERE cx.cimke_id = c.id AND cx.szoveg_id = ".$sor['id'];
        $cimkek = sqluniv4($sql, 'cimke');
        print implode(', ', $cimkek);
        
        ?></td>
        <td>
            <?php
            if ($_SESSION['cimke'] != '') {
                $sql = "SELECT x.sorrend, x.cimke_id, x.szoveg_id FROM bow_cimkek c, bow_cimkexszoveg x WHERE x.cimke_id = c.id AND cimke LIKE '".$_SESSION['cimke']."' AND szoveg_id = ".$sor['id'];
                $sorrendRs = sqluniv3($sql);
            ?>
            Sorrend: <input onclick="this.focus();" style="width: 50px;" value="<?= $sorrendRs['sorrend']; ?>" name="sorrend[c<?= $sorrendRs['cimke_id'].'_'.$sorrendRs['szoveg_id'];?>]" />
            <?php   
            }
            ?>
        </td>
    
    <td class="action">
            <a class="delete" href="<?= $sajatUrl; ?>?torles=<?= $sor['id']; ?>" <?= bowConfirm(); ?> >Törlés</a>
            <a class="delete" href="<?= $sajatUrl; ?>?szerkeszt=<?= $sor['id']; ?>"  >Szerkesztés</a>
    </td>
    </tr>
<?php
    endforeach;
?>
</table>
<?php
            if ($_SESSION['cimke'] != '') {
                
            ?>
<input type="submit" class="jNiceInputInner" name="sorrendment" value="Sorrend mentése" />
<?php
            }
?>
</form>
