<?php



    $tag = Tagok::peldany();
    if (isset($_GET['szerkeszt'])) {
        $szovegId = (int)$_GET['szerkeszt'];
    } else {
        $szovegId = 0;
    }

    if ($szovegId > 0) {
        $sql = "SELECT * FROM bow_szovegek WHERE id = " . $szovegId;
        $sz = sqluniv3($sql);
    } else {
        $sz = array(
            'cim' => '',
            'bevezeto' => '',
            'tartalom' => '',
            'csakbevezeto' => 0,
            'szerzo' => $tag->id,
            'id' => 0,
            'cimke' => '',
            'sablon' => 0,
            'letrehozva' => time(),
            'fokep' => '');
    }
    require_once (BASE . 'osztaly/osztaly_jsszerkeszto.php');
    jsSzerkeszto::szovegszerkeszto('tartalom');

?>
<form method="post" action="<?= $sajatUrl; ?>" class="jNice" enctype="multipart/form-data">
    <div class="cimkeHozzaadoDoboz">
        Cimke: (a cimkék segítségével rendezheted szövegeidet különböző kategóriákba.
        <div class="cimkebox"></div>
        <?php
    $sql = "SELECT *, c.id as cimkeid FROM bow_cimkek c, bow_cimkexszoveg x WHERE x.cimke_id = c.id AND x.szoveg_id = " .
        $sz['id'];
    $sajatcimkek = sqluniv4($sql);

    $sql = "SELECT * FROM bow_cimkek ORDER BY cimke ASC";
    $cimkek = sqluniv4($sql);
    $i = 0;
    foreach ($sajatcimkek as $s):
?>
        <div class="kipipalo round3 cimke<?= $s['cimkeid']; ?> cimkedoboz<?= $i; ?>">
            <?= $s['cimke'] ?>
            <input type="hidden" value="<?= $sz['id'] . '_' . $s['cimkeid'] ?>" name="cimkek[]" />
            <span class="round3" onclick="$('.cimkedoboz<?= $i; ?>').remove();">X</span>
        </div>
        <?php

    endforeach;
?>
        
        <br /><br />
        Cimke hozzáadása.
        <div class="round3 orange" >
            <?php
    foreach ($cimkek as $s) {
?>
                <a href="javascript:void(0);" onclick="ujCimke('<?= $s['id']; ?>','<?= $sz['id'] .
'_' . $s['id'] ?>', '<?= $s['cimke'] ?>')"><?= $s['cimke'] ?></a>
                <?php
    }
?>
        </div>
        <br class="clear"/>
         <a href="<?= ADMIN_URL ?>_modulok/lista/beallit/22" onclick="if(confirm('Biztos elhagyod az oldalt?')==false)return false;">Új cimke létrehozása</a>
        <?php
        $html = Html::peldany();
        $html->headerStart();
        ?>
        <script>
        var i = 1000;
        function ujCimke(cimkeId, id, cimke) {
            cimkeDiv = $('.cimke'+cimkeId);
            if (cimkeDiv.length>0)return;
            i++;
            $('.cimkebox').append('<div class="kipipalo round3 cimke'+cimkeId+' cimkedoboz'+i+'">'+cimke+'<input type="hidden" value="'+id+'" name="cimkek[]" /><span class="round3" onclick="$(\'.cimkedoboz'+i+'\').remove();">X</span></div>');
        }
        </script>
        <style>
        .cimkeHozzaadoDoboz {
            float: right;
            width: 200px;
            display: block;
            margin: 0 10px 10px 10px;
            background: #fff;
            border: 1px solid #eee;
            padding: 10px;
        }
        </style>
        <?php
        $html->headerStop();
        ?>
    </div>
    
     <?php 
        if ($sz['fokep']!='') {
            print kiskepKocka(BASE_URL.'feltoltesek/cikkek/'.$sz['fokep'], 60, ' style="float:right;border:1px solid #bbb;" ');
        }
        ?>
    <p style="float: none;">
        <label>Cím:</label>
        <input name="sz[cim]" value="<?= $sz['cim'] ?>" />
    </p>
    
    <p style="float: none;">
        <label>Bevezető:</label>
        <textarea class="editText" name="sz[bevezeto]" ><?= $sz['bevezeto'] ?></textarea>
    </p>
    <p>
        <label>Tartalom:</label>
        <textarea class="editText" id="tartalom" name="sz[tartalom]" ><?= $sz['tartalom'] ?></textarea>
    </p>
    <p>
        <label>Csak a bevezető megjelenítése:</label>
        <select name="sz[csakbevezeto]">
            <option value="0" <?= (($sz['csakbevezeto'] == 0) ?
    ' selected="selected" ' : '') ?> >Nem</option>
            <option value="1" <?= (($sz['csakbevezeto'] == 1) ?
        ' selected="selected" ' : '') ?> >Igen</option>
            
        </select>
    </p>
   
    <p>
        <label>Sablon: (ezzel beállíthatod, hogy, hogyan nézzen ki a cikk. Sablonokat a Modullista menüpontban a Szövegszerkesztő modul Beállításainál hozjatsz létre)</label>
        <select name="sz[sablon]">
            <option value="0" <?= (($sz['sablon'] == 0) ?
' selected="selected" ' : '') ?>>Alapértelmezett</option>
            <?php
    
    $rs = szovegSablonok();
    if (!empty($rs))
        foreach ($rs as $sor):
?>
            <option value="<?= $sor ?>" <?= (($sz['sablon'] == $sor) ?
' selected="selected" ' : '') ?>><?= $sor ?></option>
            <?php
        endforeach;
?>
        
        </select>
        
    </p>
    <p>
         <?php 
        if ($sz['fokep']!='') {
            print kiskepKocka(BASE_URL.'feltoltesek/cikkek/'.$sz['fokep'], 150, ' style="float:right;border:1px solid #bbb;" ');
        }
        ?>
        <label>Cikk főkép (autómatikusan megjelenik listázáskor)</label>
        <input type="file" name="fokep" /> 
       <?php 
        if ($sz['fokep']!='') {
            ?>
            <br />
            <br />
            VAGY<br /><br />
            <input name="fokeptorles" value="1" type="checkbox" /> - töröld a meglévő képet
            <?php
        }
        ?>
        <input name="fokepfeluliras" id="fokepfeluliras" value="" type="hidden"/>
    </p>
   
    <br class="clear"/>
    <p>
        <label> </label>
        <input type="submit" value="Kész vagyok" />
        <button type="button" onclick="location.href='<?= $sajatUrl ?>'" style="float: right;">Vissza a szövegek listájához</button>
    </p>
    <input type="hidden" name="sz[id]" value="<?= $sz['id'] ?>" />
    <input type="hidden" name="sz[szerzo]" value="<?= $sz['szerzo'] ?>" />
    <input type="hidden" name="sz[letrehozva]" value="<?= $sz['letrehozva'] ?>" />
    
    
    
</form>
 <?php
    Beepulok::futtat('_szerkeszto_Szövegszerkesztés', $sz);


