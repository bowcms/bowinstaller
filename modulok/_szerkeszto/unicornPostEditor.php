<?php
$html = Html::peldany();
$html->helyorzoHozzafuzes('adminUtvonal', '<a href="'.UNIC_URL.'">Post Editor</a>');

if ($feladat == 'sorrend') {
    trim($id, '|');
    $ids = explode('|', $id);
    if (is_array($ids))
    foreach ($ids as $sor) {
        $exp = explode('_',$sor);
        if (isset($exp[3])) {
            $sql = "UPDATE bow_cimkexszoveg SET sorrend = ".(int)$exp[3]." WHERE szoveg_id = ".(int)$exp[1]." AND cimke_id = ".(int)$exp[2];
            sqluniv($sql);    
        }
        
    }
}

if ($feladat == 'delete') {
    $ids = explode('_', $id);
    foreach ($ids as $id) {
        if (is_numeric($id)) {

            $sql = "DELETE FROM bow_cimkexszoveg WHERE szoveg_id = $id ";
            sqluniv($sql);
            $sql = "DELETE FROM bow_szovegek WHERE id = $id ";
            
            sqluniv($sql);
            


        }
    }
    $u->uzenetKiiras(__f('Törlés sikeres'), __f('A kijelölt elemeket eltávolítottam!'));
    header('Location: '.UNIC_URL);
}
$_POST = Memoria::olvas('xss_orig_post');
if (isset($_POST['a'])) {
        $a = $_POST['a'];
        
        if ($_POST['fokepfeluliras']!='') {
            $a['fokep'] = 'feltoltesek/cikkgaleria/'.$a['id'].'/'.$_POST['fokepfeluliras'];
        }
        // [9:07:14] Wagner László: innorail997
        $a['tartalom'] = base64_decode($a['tartalom']);
        
        if ($a['id']==0) {
            unset($a['id']);
            sqlfelvitel($a, 'bow_szovegek');
            $szid = mysql_insert_id();
            
        } else {
            sqlmodositas($a, 'bow_szovegek');
            $szid = $a['id'];
        }
        
        $hozzarendeltCimkek = getColArrayAsId('cimke_id', 'bow_cimkexszoveg', "szoveg_id = $szid");
        
        if (isset($_POST['cimkek'])) {
            
            
            foreach ($_POST['cimkek'] as $k => $cimke) {
                unset($hozzarendeltCimkek[$cimke]);
                if ((int)$cimke == 0) continue;
                
                $sql = "SELECT id FROM bow_cimkexszoveg WHERE cimke_id = $cimke  AND szoveg_id = $szid";
                $vanX = sqluniv3($sql);
                if (!isset($vanX['id'])) {
                    $sql = "INSERT INTO bow_cimkexszoveg SET cimke_id = $cimke , szoveg_id = $szid"; 
                    sqluniv($sql); 
                }
                
               
               
               
            }
            
            
            
        }
        if (!empty($hozzarendeltCimkek)) {
                $tIds = implode(', ',$hozzarendeltCimkek);
                $sql = "dELETE FROM bow_cimkexszoveg WHERE szoveg_id = $szid AND cimke_id IN ($tIds) ";
                sqluniv($sql);
        }
        $u->komp->popUpNoti(__f('Szöveg mentése sikeres'),'');
        
}

if ($feladat == 'edit') {
    
    if ((int)$id > 0) {
        $sql = "SELECT * FROM bow_szovegek WHERE id = ".(int)$id;
        $a = sqluniv3($sql);
    }
    $tag = Tagok::peldany();
    if (!isset($a)) {
        $a = array(
            'cim' => '',
            'bevezeto' => '',
            'tartalom' => '',
            'csakbevezeto' => 0,
            'id' => 0,
            'szerzo' => $tag->id,
            'letrehozva' => time());
    }
    $admin = $u;
    
    Memoria::ir('_szerkeszto_Szovegszerkesztes_formKirakasElott', $a);
    
    
    $admin->komp->adminFormFej(__f('Szöveges tartalom szerkesztése'),UNIC_URL,'bowform');
    $admin->komp->adminFormInput(__f('Cím'),'a[cim]', $a['cim']);
    
    $osszesCimke = valasztoLista('bow_cimkek','id', 'cimke',' cimke ASC');
    $cimkek = getColArray('cimke_id', 'bow_cimkexszoveg', "szoveg_id = ".$a['id'] );
    
    
    $admin->komp->adminFormMultiSelect(__f('Cimkék'),'cimkek[]',$cimkek, $osszesCimke);
    $admin->komp->adminFormText(__f('Bevezető'), 'a[bevezeto]', $a['bevezeto']);
    $admin->komp->adminFormTextAdv(__f('Tartalom'), 'a[tartalom]', $a['tartalom']);
    $admin->komp->adminFormHidden('a[szerzo]', $a['szerzo']);
    $admin->komp->adminFormHidden('a[letrehozva]', $a['letrehozva']);
    $admin->komp->adminFormHidden('a[id]', $a['id']);
    $admin->komp->adminFormHidden('fokepfeluliras', '');
    
    
    Beepulok::futtat('postEditorFormBottom', $a);
    
    $a = Memoria::olvas('_szerkeszto_Szovegszerkesztes_formKirakasElott');
    
    
    $admin->komp->adminFormLab();
    $html->bodyVegeStart();
    ?>
    <script>
        $('#bowform').submit(function(){
            tinyMCE.remove();
            $('#tinyTartalom').val( Base64.encode($('#tinyTartalom').val()) );
            
        });
        var Base64 = {
// private property
_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

// public method for encoding
encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {

        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
        Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) +
        Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);

    }

    return output;
},

// public method for decoding
decode : function (input) {
    var output = "";
    var chr1, chr2, chr3;
    var enc1, enc2, enc3, enc4;
    var i = 0;

    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

    while (i < input.length) {

        enc1 = Base64._keyStr.indexOf(input.charAt(i++));
        enc2 = Base64._keyStr.indexOf(input.charAt(i++));
        enc3 = Base64._keyStr.indexOf(input.charAt(i++));
        enc4 = Base64._keyStr.indexOf(input.charAt(i++));

        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;

        output = output + String.fromCharCode(chr1);

        if (enc3 != 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 != 64) {
            output = output + String.fromCharCode(chr3);
        }

    }

    output = Base64._utf8_decode(output);

    return output;

},

// private method for UTF-8 encoding
_utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

        var c = string.charCodeAt(n);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }

    }

    return utftext;
},

// private method for UTF-8 decoding
_utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

        c = utftext.charCodeAt(i);

        if (c < 128) {
            string += String.fromCharCode(c);
            i++;
        }
        else if((c > 191) && (c < 224)) {
            c2 = utftext.charCodeAt(i+1);
            string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
            i += 2;
        }
        else {
            c2 = utftext.charCodeAt(i+1);
            c3 = utftext.charCodeAt(i+2);
            string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
            i += 3;
        }

    }
    return string;
}
}
    </script>
    <?php
    $html->bodyVegeStop();
    // még nem vagyunk készen
    return false;
    
    
}
$tablaOpciok = array(
        'tablacim' => __f('Szövegek'),
        'mezok' => array('id' => 'ID', 'cim' => __f('Tartalom neve')),
        'vezerlok' => array(
            'edit' => __f('Szerkesztés'),
            'delete' => __f('Törlés')),
        'filter' => 'cim, bevezeto, tartalom', 
        'posteditor_ujPostGomb' => 1);
        
Beepulok::regisztral('uniTablaFejButton_Pos2', 'PostEditorUjPostGomb' );
Beepulok::regisztral('uniTablaFejButton_Pos', 'PostEditorTagSelect' );
Beepulok::regisztral('UnicornSzerkesztoTabla_sql', 'PostEditorTagSzuro');
Beepulok::regisztral('uniTablaFej_AddingTH', 'CimkeSorrendAllitoTH');
Beepulok::regisztral('uniTablaFej_AddingTD', 'CimkeSorrendAllitoTD');
Beepulok::regisztral('uniTablaLabButton_Pos', 'CimkeSorrendAllitoBTN');

    
    
    
$u->szerkesztoTabla('bow_szovegek', $tablaOpciok);


function PostEditorUjPostGomb(& $a) {
    
    if (!isset($a['posteditor_ujPostGomb']))return ;
    ?>
    <button onclick="window.location.href='<?= UNIC_URL?>edit/0'" class="btn"><?= __f ('Új bejegyzés felvitele'); ?></button>
    <?php
}
function PostEditorTagSelect(& $a) {
    
    if (!isset($a['posteditor_ujPostGomb'])) return ;
    if (!isset($_SESSION['UnicornSzerkesztoTabla_tag'])) $_SESSION['UnicornSzerkesztoTabla_tag'] = '';
    $kiv = $_SESSION['UnicornSzerkesztoTabla_tag'];
    $sql = "SELECT * FROM bow_cimkek ORDER BY cimke ASC";
    $rs = sqluniv4($sql);
    ?>
    <div class="row">
    <div class="span12" class="info">
        <?= __f('Szűrés szövegcimkére: ')?>
        <select name="postEditorTagFilter" onchange="window.location.href='?tag='+$(this).val();">
            <option value=""><?= __f('Nincs kiválasztva cimke')?></option>
            <?php
            foreach ($rs as $cimke):
            ?>
            <option <?= (($kiv == $cimke['id'])?' selected="selected" ':'')?> value="<?= $cimke['id']; ?>" ><?= $cimke['cimke']; ?></option>
            <?php 
            endforeach;
            ?>
        </select>
    </div>
    </div>
    <?php
}

function PostEditorTagSzuro() {
    
    $sql = Memoria::olvas('UnicornSzerkesztoTabla_sql');
    if (!isset($_SESSION['UnicornSzerkesztoTabla_tag'])) $_SESSION['UnicornSzerkesztoTabla_tag'] = '';
    if (isset($_GET['tag'])) {
        $_SESSION['UnicornSzerkesztoTabla_tag'] = str_replace(' ', '', $_GET['tag']);
    }
    $w = '';
    if ($_SESSION['UnicornSzerkesztoTabla_tag']!='') {
        
        if (isset($_SESSION['unicorn']['tablaszerkeszto']['kereses'])) {
            if ($_SESSION['unicorn']['tablaszerkeszto']['kereses'] != '') {
                $str = $_SESSION['unicorn']['tablaszerkeszto']['kereses'];
                $w = " (sz.cim LIKE '%$str%' or sz.bevezeto LIKE '%$str%' or sz.tartalom LIKE '%$str%' ) AND ";
            }
        }
        
        $sql = "SELECT sz.id, sz.cim FROM bow_szovegek sz, bow_cimkexszoveg x WHERE $w x.cimke_id = ".(int)$_SESSION['UnicornSzerkesztoTabla_tag']." 
        AND x.szoveg_id = sz.id ORDER BY x.sorrend
        ";
        
    }
    Memoria::ir('UnicornSzerkesztoTabla_sql', $sql);
    
    
}
function CimkeSorrendAllitoTH($a) {
    if (isset($_SESSION['UnicornSzerkesztoTabla_tag'])) {
        if ($_SESSION['UnicornSzerkesztoTabla_tag']!="") {
            print '<th>'.__f('Sorrend').'</th>';
        }
    }
}

function CimkeSorrendAllitoTD($sor) {
    
    if (isset($_SESSION['UnicornSzerkesztoTabla_tag'])) {
        if ($_SESSION['UnicornSzerkesztoTabla_tag']!="") {
            $sql = "SELECT sorrend FROM bow_cimkexszoveg WHERE szoveg_id = ".$sor['id'];
            $rs = sqluniv3($sql);
            if (isset($rs['sorrend'])) {
                $s = $rs['sorrend'];
            } else {
                $s = 0;
            }
            ?>
            <td>
                <input class="uniPostEditorSorrendallitoTxt" rel="sz_<?= $sor['id'].'_'.$_SESSION['UnicornSzerkesztoTabla_tag'];?>" value="<?= $s; ?>" />
            </td>
            <?php
        }
    }
}

function CimkeSorrendAllitoBTN($a) {
    if (isset($_SESSION['UnicornSzerkesztoTabla_tag'])) {
        if ($_SESSION['UnicornSzerkesztoTabla_tag']!="") {
            ?>
            <button class="btn-inverse" onclick="unicornPostEditorSaveOrder();"><?= __f('Sorrend mentése'); ?></button>
            <?php
            
            $html = Html::peldany();
            $html->headerStart();
            ?>
            <script type="text/javascript">
                function unicornPostEditorSaveOrder() {
                    imp = $('.uniPostEditorSorrendallitoTxt');
                    ret = '';
                    if (imp.length>0) {
                        for(i = 0; i < imp.length; i++) {
                            ret = ret + $(imp[i]).attr('rel')+ '_' + $(imp[i]).val()+'|';
                        }
                    }
                    $('#tartalomDiv').load('<?= UNIC_AJAX_URL;?>sorrend/'+ret);
                }
            </script>
            <?php
            $html->headerStop();
        }
    }
}

