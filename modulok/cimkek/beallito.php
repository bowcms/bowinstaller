<?php
if (isset($_GET['torles'])) {
    $id = (int)$_GET['torles'];
    sqltorles('bow_cimkek', $id);
    sqltorles('bow_cimkexszoveg', $id, 'cimke_id');
    $html = Html::peldany();
    $html->uzenetKiiratas('Cimke törlése sikeres!');
}
if (isset($_POST['a'])) {
    $a = $_POST['a'];
    if (!empty( $_POST['cs'] ))
        $a['feliratkozhat'] = implode(',', $_POST['cs']);
    else
        $a['feliratkozhat'] = '';
    sqladat($a, 'bow_cimkek');
    $html = Html::peldany();
    $html->uzenetKiiratas('Cimke módosítása sikeres!');
}       
if (isset($_GET['szerkeszt'])) {
    $a = getRows("bow_cimkek", (int)$_GET['szerkeszt']);
    ?>

    <form action="<?= $sajatUrl; ?>" method="post" class="jNice">
            <input type="hidden" name="a[id]" value="<?= $a['id']; ?>"/>
            <p>
                <label>Cimke</label>
                <input name="a[cimke]" value="<?= $a['cimke'] ?>" />
            </p>
            <p>
                <label>Leírás</label>
                <textarea name="a[leiras]"><?= $a['leiras'] ?></textarea>
            </p>
            <p>
                <label>Kik iratkozhatnak fel ennek a cimknek a cikkeire?</label>
                <?php
                    $tag = Tagok::peldany();
                    $tagCsoport = $tag->adat['csoport_id'];
                    
                    
                    $sql = "SELECT * FROM bow_csoportok WHERE rang <= (SELECT rang FROM bow_csoportok WHERE id = $tagCsoport ) ORDER BY rang ASC";
                    $csoportok = sqluniv4($sql);
                    
                    foreach ($csoportok as $csoport):
                        $vanCsoport = false;
                        $mehet = $a['feliratkozhat'];
                        
                        if ($mehet !='') {
                            $mehet = explode(',', $mehet);
                            foreach($mehet as $csoportId) {
                                
                                if ($csoport['id'] == $csoportId) {
                                    $vanCsoport = true;
                                    
                                }
                            }
                        }
                    ?>
                        <input value="<?= $csoport['id']?>" type="checkbox" name="cs[]" <?= ($vanCsoport)?' checked="checked" ':''; ?> /> - <?= $csoport['nev']; ?><br style="clear: both;" />
                    <?php
                    endforeach;
                    ?>
            </p>
            
            <p>
                <input type="submit" value="Mentés"/>
            </p>
    </form>
    <?php
} else {

$sql = "SeLECT * FROM bow_cimkek ORDER BY cimke ASC";
$cimkek = sqluniv4($sql);

?>
<h3>Cimkék beállítsa</h3>
<form method="post" action="<?= $sajatUrl; ?>?szerkeszt=0" class="jNice" enctype="multipart/form-data">
    <input type="submit" value="Új cimke"/>
</form>
<br /><br />
<table class="jNice">
    
    <?php
    foreach($cimkek as $cimke):
    ?>
    <tr>
        <td style="vertical-align: top;"><strong><?= $cimke['cimke']?></strong></td>
        <td style="vertical-align: top;"><?= $cimke['leiras']?></td>
        <td style="vertical-align: top;">Feliratkozhat:<br /><div style="color: green;">
        <?php
        if (trim($cimke['feliratkozhat'])=='') {
            $feliratkozok = array();
        } else {
            $feliratkozok = explode(',', $cimke['feliratkozhat']);    
        }
        
        
        if (empty($feliratkozok)) {
            print 'Senki';
        } else {
            foreach($feliratkozok as $iratkozo) {
                print '<em>'.getCol('nev', 'bow_csoportok', " id = ".$iratkozo).'</em><br />';
            }
        }
        ?></div></td>
        <td class="action">
            <a class="delete" href="<?= $sajatUrl; ?>?torles=<?= $cimke['id']; ?>" <?= bowConfirm(); ?> >Törlés</a>
            <a class="delete" href="<?= $sajatUrl; ?>?szerkeszt=<?= $cimke['id']; ?>"  >Szerkesztés</a>
        </td>
    </tr>
    <?php
    endforeach;
    ?>
</table>





<?php
}
/*
$sql = "SeLECT * FROM bow_szovegek ";
$sz = sqluniv4($sql);

foreach ($sz as $szoveg) {
    $cimke = explode(',',$szoveg['cimke']);
    if (!empty ($cimke)) {
        foreach ($cimke as $cmk) {
            $cim = trim($cmk);
            if ($cim != '') {
                $id = sqlLetezik('bow_cimkek', $cim, 'cimke');
                print 'Létezik: '.$cim.'::'.$id."<br />";
                if (!$id) {
                    $a['cimke'] = $cim;
                    sqladat($a, 'bow_cimkek');
                    $id = mysql_insert_id();
                }
                $x = array('cimke_id' => $id, 'szoveg_id' => $szoveg['id']);
                sqladat($x,'bow_cimkexszoveg');
            }
        }
    }
}
*/