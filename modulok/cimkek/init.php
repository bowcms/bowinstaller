<?php
/**
 * Title: Text tags editing
 * Author: bowCms
 * Web: http://example.com
 * Description: Editing text tags in the admin area
 * 
 **/

adminMenu(__f('Szövegcimkék'), 'unicornOldalMenu', __f('Weboldal'), 'unicornCimkeAdmin',
    'icon-tags');

function unicornCimkeAdmin($feladat = '', $id = '')
{
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Szövegcímkék'));
    $html->helyorzoHozzafuzes('adminUtvonal', '<a href="' . UNIC_URL . '">' . __f('Szövegcímkék') .
        '</a>');
    
    if (isset($_POST['a'])) {
        $a = $_POST['a'];
        $a['feliratkozhat'] = ((isset($_POST['feliratkozok']))?implode(',',$_POST['feliratkozok']):'');
        sqladat($a, 'bow_cimkek');
        $u->komp->popUpNoti(__f('Mentés sikeres'),'');
    }

    if ($feladat == 'delete') {
        $ids = explode('_', $id);
        foreach ($ids as $id) {
            if (is_numeric($id)) {

                $sql = "DELETE FROM bow_cimkexszoveg WHERE cimke_id = $id ";
                sqluniv($sql);
                $sql = "DELETE FROM bow_cimkek WHERE id = $id ";

                sqluniv($sql);


            }
        }
        $u->uzenetKiiras(__f('Törlés sikeres'), __f('A kijelölt elemeket eltávolítottam!'));
        header('Location: ' . UNIC_URL);
    }
    if ($feladat == 'edit') {
        if ((int)$id>0) {
            $sql = "SELECT * FROM bow_cimkek WHERE id = ".(int)$id;
            $a = sqluniv3($sql);
        }else {
            $a = array(
                'id' => 0,
                'cimke' => '',
                'leiras' => '',
                'feliratkozhat' => ''
                
            );
        }
        
        $u->komp->adminFormFej(__f('Cimke szerkesztése'), UNIC_URL);
        $u->komp->adminFormHidden('a[id]', $a['id']);
        $u->komp->adminFormInput(__f('Cimke'), 'a[cimke]',$a['cimke']);
        $u->komp->adminFormTextAdv(__f('Leírás'), 'a[leiras]',$a['leiras']);
        
        $lista = valasztoLista('bow_csoportok', 'id', 'nev', ' nev asc');
        $ertek = explode(',', $a['feliratkozhat']);
        
        $u->komp->adminFormMultiSelect(__f('Feliratkozás (pl. rendszeres hírlevélhez)'),'feliratkozok[]', $ertek,$lista);
        
        $u->komp->adminFormLab();
        return;
        
    }

    $tablaOpciok = array(
        'tablacim' => __f('Szövegcímkék'),
        'mezok' => array('id' => 'ID', 'cimke' => __f('Cimke')),
        'vezerlok' => array('edit' => __f('Szerkesztés'), 'delete' => __f('Törlés')),
        'filter' => 'cimke',
        'ujElemGomb' => __f('Új cimke'));
    $u->szerkesztoTabla('bow_cimkek', $tablaOpciok);


}
?>