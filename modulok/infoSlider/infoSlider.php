<?php

class infoSlider
{
    public function wgSlider($a) {
       
        $this->megjelenit(true);
    }


    public function megjelenit($widget = false)
    {
        $html = Html::peldany();
        if(!$widget) $html->helyorzoFelvetelStart();
        
        
        $sql = "SELECT * FROM bow_infoslider_info where datum >= '".date('Y-m-d')."' ORDER BY datum ASC";
        $szovegek = sqluniv4($sql);
        
        $kepek = sqluniv4("SELECT * FROM bow_infoslider ORDER BY sorrend ASC");
        $this->komp->kirak($kepek, $szovegek);
        if(!$widget) $html->helyorzoFelvetelStop('infoSliderTartalom');
    }
}
