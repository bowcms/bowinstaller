<?php
$html = Html::peldany();
if (isset($_POST['sorrend'])) {
    foreach($_POST['sorrend'] as $id => $ertek) {
        sqluniv("UPDATE bow_infoslider SET sorrend = ".(int)$ertek." WHERE id = $id");
    }
}
if (isset($_POST['e'])) {
    
    
    foreach($_POST['e'] as $sor) {
        if($sor['datum']!=''){
            sqladat($sor,'bow_infoslider_info');
        }
    }
    
    
    
}

if (isset($_POST['f'])) {
    
    
    foreach($_POST['f'] as $id => $sor) {
        if(isset($sor['torles'])){
            sqltorles('bow_infoslider_info', $id);
            continue;
        }
        $sor['id'] = $id;
        sqladat($sor, 'bow_infoslider_info');
        
    }
    
    
    
}

if (isset($_GET['torol'])) {
    $sql = "SELECT * FROM bow_infoslider WHERE id = ".(int)$_GET['torol'];
    $kep = sqluniv3($sql);
    $kepLink = BASE.'feltoltesek/infoSlider/'.$kep['kep'];
    unlink($kepLink);
    $sql = "DELETE FROM  bow_infoslider WHERE id = ".(int)$_GET['torol'];
    sqluniv($sql);
    $html->uzenetKiiratas('Kép törlése megtörtént.');
}
if (isset($_FILES['kep'])) {
    if(!is_dir(BASE.'feltoltesek/infoSlider')) {
        mkdir(BASE.'feltoltesek/infoSlider');
    }
    $nev = explode('.', $_FILES['kep']['name']);
    $kit = end($nev);
    $ujNev = time().rand(100,999).'.'.$kit;
    if(!move_uploaded_file($_FILES['kep']['tmp_name'],BASE.'feltoltesek/infoSlider/'.$ujNev ) ) {
        $html->hibaKiiratas('Feltöltés sikertelen, ellenőrizd az írási jogokat!');
    } else {
        $sql = "INSERT INTO bow_infoslider SET kep = '$ujNev'";
        sqluniv($sql);
        $html->uzenetKiiratas('Kép mentése megtörtént.');
    }
}
$html->headerStart();
?>
<script src="<?= BASE_URL; ?>js/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript">
$(function() {
    $( ".datepicker" ).datepicker( { "dateFormat" : 'yy-mm-dd' } );
  });
</script>
<?php
$html->headerStop();
?>
<h3>Képek feltöltése, méret beállítása</h3>
<form method="post" action="<?= $sajatUrl; ?>" class="jNice" enctype="multipart/form-data">
    
    <input type="file" name="kep" /> 
    
    <input type="submit" value="Feltöltés"/>
</form>
<br />
<form method="post" action="<?= $sajatUrl; ?>" class="jNice" >
<table cellpadding="0" cellspacing="0">
<?php
$sql = "SELECT * FROM bow_infoslider ORDER BY sorrend ASC";
$rs = sqluniv4($sql);
if(!empty($rs))
foreach($rs as $sor):

?>
<tr>
    <td>
        <img height="100" src="<?= BASE_URL.'feltoltesek/infoSlider/'.$sor['kep']; ?>" />
    </td>
    <td style="vertical-align: middle;">Sorrend:<br /><input name="sorrend[<?= $sor['id']?>]" value="<?= $sor['sorrend']?>"/></td>
    <td class="action" style="vertical-align: middle;">
        <a class="delete" href="<?= $sajatUrl; ?>?torol=<?= $sor['id']?>">Törlés</a>
    </td>
</tr>
<?php
endforeach;
?>
</table>
    <input type="submit" value="Feltöltés"/>
</form>

<br style="clear: both;"/>
<form method="post" action="<?= $sajatUrl; ?>#esemeny" name="esemeny" id="esemeny" class="jNice" >
    <h3>Szövegdoboz paraméterek</h3>
    <?php
    for ($i = 0; $i < 5; $i++):
    ?>
    <p>
        <label><strong>Esemény <?= $i + 1; ?></strong></label>
       Időpont: <input class="datepicker" type="text" value="" name="e[<?= $i; ?>][datum]" /> Cím:<input type="text" value="" name="e[<?= $i; ?>][cim]" />
        <br />Kisleírás: <input type="text" value="" name="e[<?= $i; ?>][leiras]" /> 
    </p>
    <?php
    endfor;
    ?>
    <p>
        <input type="submit" value="Mentés"/>
    </p>
</form>
<form method="post" action="<?= $sajatUrl; ?>" class="jNice" >

<table>

<?php
    $sql = "SELECT * FROM bow_infoslider_info ORDER BY datum ASC";
    $rs = sqluniv4($sql);
    foreach($rs as $sor) {
        ?>
        <tr>
            <td>
                Időpont: <br /><input class="datepicker" type="text" value="<?= $sor['datum']; ?>" name="f[<?= $sor['id']; ?>][datum]" /> 
                
            </td><td>
            
                Cím:<br /><input type="text" value="<?= $sor['cim']; ?>" name="f[<?= $sor['id']; ?>][cim]" />
                
            </td><td>
            
                Kisleírás: <br /><input type="text" value="<?= $sor['leiras']; ?>" name="f[<?= $sor['id']; ?>][leiras]" /> 
                
            </td><td>
                
                Törlés:  <input type="checkbox" value="1" name="f[<?= $sor['id']; ?>][torles]" />
            </td>
        </tr>
        <?php
    }
?> 
</table>
<p>
        <input type="submit" value="Mentés"/>
    </p>
</form>

