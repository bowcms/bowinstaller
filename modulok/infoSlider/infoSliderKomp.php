<?php

class infoSliderKomp {
    
    public function kirak($kepek, $szovegek) {
        $html = Html::peldany();
        
        $honapNevek = array(
        
            1 => 'január',
            2 => 'február',
            3 => 'március',
            4 => 'április',
            5 => 'május',
            6 => 'június',
            7 => 'július',
            8 => 'augusztus',
            9 => 'szeptember',
            10 => 'október',
            11 => 'november',
            12 => 'december'
            
            );
        
        ?>
        <div class="sliderDoboz shadow">
    
    <div id="slider-code">
    <div class="viewport">
        <ul class="overview">
            <?php
            foreach ($kepek as $sor):
            ?>
            <li><img src="<?= BASE_URL; ?>feltoltesek/infoSlider/<?= $sor['kep']; ?>" width="684"/></li>
            <?php
            endforeach;
            ?>
        </ul>
    </div>
    
    </div>
    
    <div id="slider-code-right">
    <h3>Közelgő események</h3>
    <div class="viewport" style="height: 146px;overflow-y: hidden;">
        <ul class="overview" style="height: 490px; top: -210px;">
                        <?php
                        if(!empty($szovegek))
                        foreach($szovegek as $sor):
                            $datum = $sor['datum'];
                            $d = explode('-', $datum);
                            $datum = $d[0].' '.$honapNevek[(int)$d[1]].' '.$d[2].'.';
                            
                        ?>
                        <li><a href="javascript:void(0)" <?php if($sor['leiras'] !=''):?> onclick="$('.isc_<?= $sor['id'];?>').fadeIn();" <?php endif; ?> class="menu-box round_4"> <span class="title"><?= $sor['cim']?></span><br>
							<?= $datum; ?><?php if($sor['leiras'] !=''):?><span class="arrow"></span><?php endif; ?></a></li>
						
                        <?php
                        endforeach;
                        ?>
                        
        </ul>
    </div>
    <a class="buttons prev" href="#"> </a>
    <a class="buttons next" href="#"> </a>
    </div>
    
    </div>
    <?php
    foreach($szovegek as $sor):
    ?>
    <div class="infoSliderContent round_8 transparent isc_<?= $sor['id'];?>" >
        <a class="isc_close">Bezár</a>
        <h3><?= $sor['cim'];?></h3>
        <em><?= $sor['datum'];?></em><br /><br />
        <?= $sor['leiras'];?>
    </div>
    <?php
    endforeach;
    ?>
        <?php
        
        // header scriptek
        
        $html->headerStart();
        ?>
        <script type="text/javascript" src="<?= BASE_URL?>modulok/infoSlider/jquery.tinycarousel.min.js"></script>
        <link href='<?= BASE_URL?>modulok/infoSlider/style.css' rel='stylesheet' type='text/css'>

<script>
$().ready(function(){
    $('.isc_close').click(function() {
        $('.infoSliderContent').fadeOut();
    });
    $('#slider-code').tinycarousel({ interval: true,animation: false  });
    $('#slider-code-right').tinycarousel({ interval: true,axis: 'y'});
})
</script>
<style>
.infoSliderContent {
    position: absolute;
    top: 141px;
left: 165px;
    width: 600px;
    background: #000;
    padding: 10px;
    color: #ddd;
    font-size: 11px;
    display: none;
}
.infoSliderContent h3{
    font-size: 15px;
    color: #aaa;
}
.infoSliderContent em{
    color: #fff;
}
.infoSliderContent a{
    float: right;
    cursor: pointer;
}
.transparent {
	/* Required for IE 5, 6, 7 */
	/* ...or something to trigger hasLayout, like zoom: 1; */
	
		
	/* Theoretically for IE 8 & 9 (more valid) */	
	/* ...but not required as filter works too */
	/* should come BEFORE filter */
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
	
	/* This works in IE 8 & 9 too */
	/* ... but also 5, 6, 7 */
	filter: alpha(opacity=50);
	
	/* Older than Firefox 0.9 */
	-moz-opacity:0.5;
	
	/* Safari 1.x (pre WebKit!) */
	-khtml-opacity: 0.5;
    
	/* Modern!
	/* Firefox 0.9+, Safari 2?, Chrome any?
	/* Opera 9+, IE 9+ */
	opacity: 0.5;
}
</style>
        <?php
        $html->headerStop();
    }
}