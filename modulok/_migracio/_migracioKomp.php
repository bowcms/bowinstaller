<?php

class _migracioKomp {
    
    public function start() {
        $html = Html::peldany();
        $html->helyorzo('lapCim', __f('Migrációs eszköz'));
        
        $_POST['localpath'] = dirname(BASE);
        $_POST['konyvtar'] = CMS_KONYVTAR;
        ?>
        <div class="alert alert-info">Ez az eszköz egy másolatot készít a rendszerből a megadott mappába. A mappa tartalmát fel lehet másolni a rendszer új helyére, s ott elindítani a telepítést.
            A telepítő futtatása után a rendszer másolata jön létre az új helyen.</div>
        
        <div class="widget-box">
            <div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5><?= __f('Migrációs eszköz'); ?></h5>
							</div>
                            <div class="widget-content nopadding">
                            
                            
        <form method="post" class="form-vertical">
            
            
            
                <div class="control-group">
                
                    <label class="control-label" >Másolás célja:</label>
                    <div class="controls">
                        <input name="localpath" value="<?=  $_POST['localpath'].'/'; ?>" style="width: 94%;"/>
                    </div>
                </div>
                <div class="control-group">
                
                    <label class="control-label">A telepítés helyén milyen könyvtárba kerül a rendszer ('/' ha a gyökérkönyvtárba, '/dir1/dir2/' ha almappába):</label>
                    <div class="controls">
                        <input name="konyvtar" value="<?=  $_POST['konyvtar']; ?>"  style="width: 94%;"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label">Telepítő létrehozása</label>
                    <div class="controls">
                        <input type="submit" class="btn btn-primary" value="Start" />
                    </div>
                </div>
            
        </form>
        <br />
        <form method="post" class="jNice">
            <input type="submit" class="btn btn-info"  value="Helyi sql file felülírása" name="sqlfeluliras" />
        </form>
        </div>
        </div>
        <br /><br />
        <?php
    }
}