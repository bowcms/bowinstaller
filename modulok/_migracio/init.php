<?php
/**
 * Title: Migration
 * Author: bowCms
 * Web: http://example.com
 * Description: Migration and backup tool
 * 
 **/
 
adminMenu(__f('Migráció'), 'unicornOldalMenu', 'migracioUnicorn', 'migracioUnicorn', 'icon-plus-sign');

function migracioUnicorn($feladat = '', $id = '') {
    
    
    require_once(dirname(__FILE__).'/_migracio.php');
    require_once(dirname(__FILE__).'/_migracioKomp.php');
    $o = new _migracio();
    $o->komp = new _migracioKomp();
    $o->start();
}