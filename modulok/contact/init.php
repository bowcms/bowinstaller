<?php
/**
 * Title: Contact form
 * Author: bowCms
 * Web: http://example.com
 * Description: Contact form for webpages
 * 
 **/


Beepulok::regisztralShortcode('contact', 'ContactForm');

function contactInstall() {
    
    // bowmeta telepítése
    $sql = "
CREATE TABLE IF NOT EXISTS `bow_meta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kategoria` varchar(255) NOT NULL,
  `kulcs` varchar(255) NOT NULL,
  `meta_id` int(11) NOT NULL,
  `nev` varchar(100) NOT NULL,
  `ertek` text NOT NULL,
  `sorrend` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kategoria` (`kategoria`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

    ";
    sqluniv($sql);
    //levélsablon
    $sql = "SELECT id FROM bow_levelek WHERE kulcs = 'bowContact_admin' ";
    $rs = sqlAssocRow($sql);
    if (!isset($rs['id'])) {
        $sql = "
INSERT INTO `bow_levelek` (`id`, `targy`, `valaszcim`, `valasznev`, `szoveg`, `kulcs`) VALUES
(8, 'Érdeklődés BowCms', 'info@thesupreme.me.uk', 'Bow Cms', '<p><strong>Tisztelt Adminisztr&aacute;tor,</strong></p>\r\n<p>az oldalr&oacute;l &eacute;rdeklőd&eacute;s &eacute;rkezett. Kit&ouml;lt&ouml;tt adatok:</p>\r\n<p>|adat|</p>\r\n<p>Sz&eacute;p napot:</p>\r\n<p><strong>bowCMS</strong></p>', 'bowContact_admin');

        ";
        sqluniv($sql);    
    }
    
    
}

function ContactForm($attr, $inner = false) {
    $o = modulOsztalyTolto('contact');
    $o->megjelenit($attr['id']);
}

function ContactFormInsert($cikkId)
{

    $lista = bowMeta('contact_form', 'urlappeldany');
    ?>
    <div class="control-group">
        <label class="control-label"><?= __f('Űrlap kiválasztása'); ?></label>
        <select id="contactFormList">
            <?php
            foreach($lista as $sor):
            ?>
            <option value="<?= $sor['id']?>"><?= $sor['ertek']['cim']?></option>
            <?php
            endforeach;
            ?>
        </select>
        <button type="button" onclick="contactColInsert($('#contactFormList').val());"><?= __f('Beilleszt'); ?></button>
    </div>
    <script type="text/javascript">
    function contactColInsert(str) {
        insertShortCode('contact', {'id' : str});
    }
    </script>
    <?php
}

adminMenu(__f('Contact Form beállítás'), 'unicornOldalMenu',__f('Weboldal'), 'contactFormUnicorn', 'icon-pencil');


function contactFormUnicorn($feladat = '', $id = '') {
    $u = osztaly_unicorn_futtato::peldany();
    $html = Html::peldany();
    $html->helyorzo('lapCim', __f('Kapcsolat űrlap szerkesztése'));
    include (dirname(__FILE__).'/contactFormUnicorn.php');
}