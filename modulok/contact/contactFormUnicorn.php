

<?php
$tablazat = true;
if ($feladat == 'delete') {
        $ids = explode('_', $id);
        foreach ($ids as $id) {
            if (is_numeric($id)) {

                $sql = "DELETE FROM bow_meta WHERE meta_id = $id ";
                sqluniv($sql);
                $sql = "DELETE FROM bow_meta WHERE id = $id ";

                sqluniv($sql);


            }
        }
        $u->uzenetKiiras(__f('Törlés sikeres'), __f('A kijelölt elemeket eltávolítottam!'));
        header('Location: ' . UNIC_URL);
    }
if (isset($_POST['a'])) {
    $a = $_POST['a'];
    $mid = $a['id'];
    $a = serialize($a);
    
    $b = array(
        'kategoria' => 'contact_form',
        'kulcs' => 'urlappeldany',
        'ertek' => $a
    );
    if ($mid> 0) {
        $b['id'] = $mid;
    }
    sqladat($b, 'bow_meta');
    $u->komp->popUpNoti(__f('Mentés sikeres'), __f('Hozzáadhatsz mezőket az űrlaphoz.'));
    
}
if ($feladat == 'fields') {
    
    // szerkesztett mező jön
    if (isset($_POST['ue'])) {
        $ue = $_POST['ue'];
        foreach ($ue as $ueId => $sor) {
            $b = array(
                'id' => $ueId,
                'ertek' => serialize($sor),
                'sorrend' => (int)$_POST['s'][$ueId]
            );
            if (!isset($_POST['t'][$ueId])) {
                sqlmodositas($b, 'bow_meta');
            } else {
                sqltorles('bow_meta', $ueId);
            }
            
        }
    }
    
    //újmezőérkezik,vigyükfel
    if(isset($_POST['hozzaadas'])){
        $adat = array();
        switch ($_POST['hozzaadas']){
            case 'input':
                $adat = array(
                    'tipus'=>'input',
                    'label' => __f('Felirat'),
                    'ellenorzes' => 'minchar(6), maxchar(10),password',
                    'attr' =>'',
                    'help' => ''
                );
            break;
            case 'text':
                $adat = array(
                    'tipus'=>'text',
                    'label' => __f('Felirat'),
                    'ellenorzes' => 'minchar(20), maxchar(100)',
                    'attr' =>'',
                    'help' => ''
                );
            break;
            case 'select':
                $adat = array(
                    'tipus'=>'select',
                    'label' => __f('Felirat'),
                    'lista' => 'egy:Egy,!ketto:Kettő',
                    'attr' =>'',
                    'help' => ''
                );
            break;
            case 'radio':
                $adat = array(
                    'tipus'=>'radio',
                    'label' => __f('Felirat'),
                    'lista' => 'egy:Egy,!ketto:Kettő',
                    'attr' =>'',
                    'help' => ''
                );
            break;
            case 'checkbox':
                $adat = array(
                    'tipus'=>'checkbox',
                    'label' => __f('Felirat'),
                    'attr' =>'',
                    'help' => ''
                );
            break;
        }
        
        $adat = serialize($adat);
        $sql= "SELECT MAX(sorrend) as ossz FROM bow_meta WHERE kategoria = 'contact_form' and kulcs = 'urlapelem' and meta_id = ".$id;
        $rs = sqlAssocRow($sql);
        if (isset($rs['ossz'])) {
            $sorrend = $rs['ossz']+10;
        } else {
            $sorrend = 10;
        }
        $b = array(
            'kategoria' => 'contact_form',
            'kulcs' => 'urlapelem',
            'meta_id' => $id,
            'ertek' => $adat
        );
        sqladat($b, 'bow_meta');
        $u->komp->popUpNoti(__f('Mező felvitel sikeres'), __f('Az új mező a mezőlista végén található'));
        
    }
    
    
    $u->komp->adminFormFej(__f('Mező felvitel'));
    $ajaxUrl = dirname(__FILE__).'/ajaxContactFormUniform.php?id='.$id;
    ?>
    <div class="control-group">
        <label class="control-label"><?= __f('Mező hozzáadása:');?></label>
        <div class="controls">
            <select id="tipus" name="hozzaadas">
                <option value="input">Input</option>
                <option value="text">Textarea</option>
                <option value="select">Select</option>
                <option value="radio">Radio</option>
                <option value="checkbox">Checkbox</option>
                
            </select>
            <button type="submit" class="btn btn-inverse"  ><?= __f('Hozzáadom')?></button>
        </div>
    </div>
    
    
    </form>
</div>
</div>

    <?php
    
    
    
    // mezőlista
    $u->komp->adminFormFej(__f('Mezők szerkesztése'));
    $lista = bowMeta('contact_form','urlapelem', $id);
    if (!empty($lista))
    foreach ($lista as $sor):
        switch($sor['ertek']['tipus']) {
            case 'input':
                print '<h5>Input</h5>';
                 $u->komp->adminFormHidden('ue['.$sor['id'].'][tipus]', 'input');
                 $u->komp->adminFormInput('Label','ue['.$sor['id'].'][label]',$sor['ertek']['label'],__f('Mező melletti felirat'));
                 $u->komp->adminFormInput(__f('Ellenőrzés'),'ue['.$sor['id'].'][ellenorzes]',$sor['ertek']['ellenorzes'],__f('Lehetőségek, vesszővel elválasztva: minchar(katarkerszám),maxchar(karakterszám), mask(Jquery.inputmask), date(dateformat)'));
                 $u->komp->adminFormInput(__f('Extra attributum'),'ue['.$sor['id'].'][attr]',$sor['ertek']['attr'],__f('Hozzáadható bármilyen attributum (stílus, id stb)'));
                 $u->komp->adminFormInput(__f('Segítség'),'ue['.$sor['id'].'][help]',$sor['ertek']['help'],__f('Segítség szöveg'));
                 ?>
                 
                 <div class="control-group">
                    <label class="control-label"><?= __f('sorrend')?></label>
                    <div class="controls">
                        <input  name="s[<?= $sor['id']?>]" value="<?= $sor['sorrend']?>" />
                    </div>
                 </div>
                 <div class="control-group">
                    <label class="control-label" style="color: red;"><?= __f('Töröld ezt az űrlapelemet!')?></label>
                    <div class="controls">
                        <input type="checkbox" name="t[<?= $sor['id']?>]" value="1" />
                    </div>
                 </div>
                 <div style="height: 10px; color: #555;"></div>
                 <?php
            break;
            
            case 'text':
                print '<h5>Textbox</h5>';
                 $u->komp->adminFormHidden('ue['.$sor['id'].'][tipus]', 'text');
                 $u->komp->adminFormInput('Label','ue['.$sor['id'].'][label]',$sor['ertek']['label'],__f('Mező melletti felirat'));
                 $u->komp->adminFormInput(__f('Ellenőrzés'),'ue['.$sor['id'].'][ellenorzes]',$sor['ertek']['ellenorzes'],__f('Lehetőségek, vesszővel elválasztva: minchar(katarkerszám),maxchar(karakterszám)'));
                 $u->komp->adminFormInput(__f('Extra attributum'),'ue['.$sor['id'].'][attr]',$sor['ertek']['attr'],__f('Hozzáadható bármilyen attributum (stílus, id stb)'));
                 $u->komp->adminFormInput(__f('Segítség'),'ue['.$sor['id'].'][help]',$sor['ertek']['help'],__f('Segítség szöveg'));
                 ?>
                 
                 <div class="control-group">
                    <label class="control-label"><?= __f('sorrend')?></label>
                    <div class="controls">
                        <input  name="s[<?= $sor['id']?>]" value="<?= $sor['sorrend']?>" />
                    </div>
                 </div>
                 <div class="control-group">
                    <label class="control-label" style="color: red;"><?= __f('Töröld ezt az űrlapelemet!')?></label>
                    <div class="controls">
                        <input type="checkbox" name="t[<?= $sor['id']?>]" value="1" />
                    </div>
                 </div>
                 <div style="height: 10px; color: #555;"></div>
                 <?php
            break;
            case 'select':
                print '<h5>Select</h5>';
                 $u->komp->adminFormHidden('ue['.$sor['id'].'][tipus]', 'select');
                 $u->komp->adminFormInput('Label','ue['.$sor['id'].'][label]',$sor['ertek']['label'],__f('Mező melletti felirat'));
                 $u->komp->adminFormInput(__f('Lista'),'ue['.$sor['id'].'][lista]',$sor['ertek']['lista'],__f('Az opciók érték:felirat párokban, vesszővel elválasztva, ! jellel jelőlve a selected elemet: alma:Alma,korte:Körte, !eger:Egér'));
                 $u->komp->adminFormInput(__f('Extra attributum'),'ue['.$sor['id'].'][attr]',$sor['ertek']['attr'],__f('Hozzáadható bármilyen attributum (stílus, id stb)'));
                 $u->komp->adminFormInput(__f('Segítség'),'ue['.$sor['id'].'][help]',$sor['ertek']['help'],__f('Segítség szöveg'));
                 ?>
                 
                 <div class="control-group">
                    <label class="control-label"><?= __f('sorrend')?></label>
                    <div class="controls">
                        <input  name="s[<?= $sor['id']?>]" value="<?= $sor['sorrend']?>" />
                    </div>
                 </div>
                 <div class="control-group">
                    <label class="control-label" style="color: red;"><?= __f('Töröld ezt az űrlapelemet!')?></label>
                    <div class="controls">
                        <input type="checkbox" name="t[<?= $sor['id']?>]" value="1" />
                    </div>
                 </div>
                 <div style="height: 10px; color: #555;"></div>
                 <?php
            break;
            
            case 'radio':
                print '<h5>Radio</h5>';
                 $u->komp->adminFormHidden('ue['.$sor['id'].'][tipus]', 'radio');
                 $u->komp->adminFormInput('Label','ue['.$sor['id'].'][label]',$sor['ertek']['label'],__f('Radio gombok melletti felirat'));
                 $u->komp->adminFormInput(__f('Lista'),'ue['.$sor['id'].'][lista]',$sor['ertek']['lista'],__f('Az opciók érték:felirat párokban, vesszővel elválasztva, ! jellel jelőlve a selected elemet: alma:Alma,korte:Körte, !eger:Egér'));
                 $u->komp->adminFormInput(__f('Extra attributum'),'ue['.$sor['id'].'][attr]',$sor['ertek']['attr'],__f('Hozzáadható bármilyen attributum (stílus, id stb)'));
                 $u->komp->adminFormInput(__f('Segítség'),'ue['.$sor['id'].'][help]',$sor['ertek']['help'],__f('Segítség szöveg'));
                 ?>
                 
                 <div class="control-group">
                    <label class="control-label"><?= __f('sorrend')?></label>
                    <div class="controls">
                        <input  name="s[<?= $sor['id']?>]" value="<?= $sor['sorrend']?>" />
                    </div>
                 </div>
                 <div class="control-group">
                    <label class="control-label" style="color: red;"><?= __f('Töröld ezt az űrlapelemet!')?></label>
                    <div class="controls">
                        <input type="checkbox" name="t[<?= $sor['id']?>]" value="1" />
                    </div>
                 </div>
                 <div style="height: 10px; color: #555;"></div>
                 <?php
            break;
            case 'checkbox':
                print '<h5>Checkbox</h5>';
                 $u->komp->adminFormHidden('ue['.$sor['id'].'][tipus]', 'checkbox');
                 $u->komp->adminFormInput('Label','ue['.$sor['id'].'][label]',$sor['ertek']['label'],__f('Checkbox gombok melletti felirat'));
                 $u->komp->adminFormInput(__f('Extra attributum'),'ue['.$sor['id'].'][attr]',$sor['ertek']['attr'],__f('Hozzáadható bármilyen attributum (stílus, id stb)'));
                 $u->komp->adminFormInput(__f('Segítség'),'ue['.$sor['id'].'][help]',$sor['ertek']['help'],__f('Segítség szöveg'));
                 ?>
                 
                 <div class="control-group">
                    <label class="control-label"><?= __f('sorrend')?></label>
                    <div class="controls">
                        <input  name="s[<?= $sor['id']?>]" value="<?= $sor['sorrend']?>" />
                    </div>
                 </div>
                 <div class="control-group">
                    <label class="control-label" style="color: red;"><?= __f('Töröld ezt az űrlapelemet!')?></label>
                    <div class="controls">
                        <input type="checkbox" name="t[<?= $sor['id']?>]" value="1" />
                    </div>
                 </div>
                 <div style="height: 10px; color: #555;"></div>
                 <?php
            break;
        }
    endforeach; else print __f('Nincs még mező ebben az űrlapban!');
    
    $u->komp->adminFormLab();
    
    $tablazat = false;
}
if ($feladat == 'edit'):
    $tablazat = false;
    $sql = "SELECT * FROM bow_meta WHERE kategoria = 'contact_form' and kulcs = 'urlappeldany' AND id = ".$id;
    
    $rs = sqlAssocRow($sql);
    if (!empty($rs)) {
        $a = unserialize($rs['ertek']);
       
    } else {
        $a = array(
            'id' => 0,
            'cim' => __f('Új űrlap'),
            'email' => '',
            'koszonet' => __f('Köszönjük érdeklődését, hamarosan keresni fogjuk.'),
            'redirect' => ''
        );
        
    }
    
    $u->komp->adminFormFej($a['cim'],UNIC_URL);
    $u->komp->adminFormHidden('a[id]', $id);
?>
    
    <div class="control-group">
        <label class="control-label"><?= __f('Cím'); ?></label>
        <div class="controls">
            <input name="a[cim]" value="<?= $a['cim']; ?>" />
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label"><?= __f('Címzett e-mail'); ?></label>
        <div class="controls">
            <input name="a[email]" value="<?= $a['email']; ?>" />
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label"><?= __f('Mentés utáni köszönet'); ?></label>
        <div class="controls">
            <input name="a[koszonet]" value="<?= $a['koszonet']; ?>" style="width: 50%;" />
        </div>
    </div>
    <div class="control-group">
        <label class="control-label"><?= __f('Mentés után átirányítás (hagyd üresen ha nem szüksges);'); ?></label>
        <div class="controls">
            <input name="a[redirect]" value="<?= $a['redirect']; ?>"  style="width: 50%;"/>
        </div>
    </div>

    
    
<?php
    $u->komp->adminFormLab();
endif;

if ($tablazat) {
    
    $sql = "SELECT * FROM bow_meta WHERE kategoria = 'contact_form' and kulcs = 'urlappeldany' ";
    $rs = sqlAssocArr($sql);
    if (!empty ($rs)) {
        $adat = array();
        foreach ($rs as $sor) {
            $a = unserialize($sor['ertek']);
            $a['id'] = $sor['id'];
            $adat[] = $a;
        }
        
        
        
    } else {
        $adat = ARRAY();
    }
    $tablaOpciok = array(
            'tablacim' => __f('Űrlapok'),
            'filter' => '',
            'ujElemGomb' => __f('Új űrlap'),
            'mezok' => array('id' => 'ID', 'cim' => __f('Cim')),
            'vezerlok' => array('fields' => __f('Mezők'),'edit' => __f('Szerkesztés'), 'delete' => __f('Törlés')),
            'keresotiltas' => true,
            'ujElemGomb' => __f('Űrlap léltrehozása'));
    $u->szerkesztoTabla($adat, $tablaOpciok);
    
}
    
?>


</div>
