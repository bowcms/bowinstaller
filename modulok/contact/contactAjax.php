<?php
define ('CSAK_KERET', 1);
include(dirname(dirname(dirname(__FILE__))).'/ajaxkeret.php');

$ue = $_REQUEST['ue'];
$id = $_GET['id'];
$hiba = array();
$kitoltes = array();

foreach($ue as $ueId => $ertek) {
    $meta = bowMetaId($ueId);
    $kitoltes[] = array('kerdes' => $meta['ertek']['label'], 'valasz' => htmlspecialchars($ertek));
    if(isset($meta['ertek']['ellenorzes'])) {
        if (trim($meta['ertek']['ellenorzes'])!='') {
            
            $ellenorzes = $meta['ertek']['ellenorzes'];
            $ell = explode(',', $ellenorzes);
            
            foreach($ell as $sor) {
                $sor = trim($sor);
                $sor = str_replace(' ','',$sor);
                
                switch ($sor) {
                    case 'email':
                        
                        if (!isEmail($ertek)) {
                            $hiba['hiba'][] = array('id' => $ueId,'error' => __f('Hibás E-mail cím'));
                            
                        }
                    break;
                    default:
                        preg_match('#minchar\((.*)\)#', $sor, $talalat);
                        if (!empty($talalat)) {
                            $min = $talalat[1];
                            if (strlen($ertek)<$min) {
                                $hiba['hiba'][] = array('id' => $ueId,'error' => __f('Kérjük, legalább '.$min.' karakter hosszú legyen'));
                                
                            }
                        }
                        preg_match('#maxchar\((.*)\)#', $sor, $talalat);
                        if (!empty($talalat)) {
                            $max = $talalat[1];
                            if (strlen($ertek)>$max) {
                                $hiba['hiba'][] = array('id' => $ueId,'error' =>  __f('Kérjük, maximum '.$max.' karakter hosszú legyen'));
                            }
                        }
                        
                    break;
                }
            }
        }
    }
    
}
if (!empty($hiba)) {
    print 'var data = '. json_encode($hiba);
    exit;
}

// MInden oké, mehet a ments-küldés
$meta = bowMetaId($id);

$level = Levelezo::peldany();
$level->sablon('bowContact_admin');
$adat = '';
foreach($kitoltes as $sor) {
    $adat .= '<strong>'.$sor['kerdes'].'</strong>: '.$sor['valasz']."<br />";
}
$level->helyettesitoHozzaad('adat', $adat);
$level->kuldes($meta['ertek']['email']);
$v = array('ok'=>1, 'valasz' => __f($meta['ertek']['koszonet']));
if ($meta['ertek']['redirect']!='') $v['redir'] = $meta['ertek']['redirect'];

print 'var data = '.json_encode($v);

$sql = "INSERT INTO bow_meta SET kategoria = 'contact_form', kulcs = 'kitoltes', ertek = '".serialize($kitoltes)."' ";
sqluniv($sql);
$sql = "INSERT INTO bow_meta SET kategoria = 'noti', kulcs = '', nev = '".__f('Kapcsolat üzenet érkezett')."', ertek = '".serialize(array('modul'=>'contact:contactListUnicorn', 'id' => mysql_insert_id()))."' ";
sqluniv($sql);

