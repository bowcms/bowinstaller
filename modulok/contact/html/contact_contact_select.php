<div class="hiba_<?= $sor['id']?>" style="display: none;"></div>
<div class="input-group" style="width: 90%;">
    <span class="input-group-addon">
        <?= $adat['label']; ?>
      </span>
      <select class="form-control" name="ue[<?= $sor['id']?>]" <?= (($selected)?' checked="checked" ':'')?> >
<?php
$lista = explode(',', $adat['lista']);
foreach($lista as $sor):
    $r = explode (':',trim($sor));
    $kulcs = trim($r[0]);
    $label = trim($r[1]);
    $selected = false;
    
    if (strpos($kulcs, '!')!==false) $selected = true;
    $kulcs = trim($kulcs,'!');
?>
            <option <?= (($selected)?' selected="selected" ':'')?> value="<?= $kulcs?>" ><?= $label?></option>
<?php 
endforeach;
?>
        </select>
       <?php
    if (trim($adat['help']!='')) {
        ?>
        <span class="input-group-addon">
        <button type="button" class="" data-toggle="popover" data-placement="top" title="<?= $adat['help']; ?>">?</button>
        </span>
        <?php
    }
    ?>
</div>

<br />