<div class="hiba_<?= $sor['id']?>" style="display: none;"></div>
<div class="input-group">
    <?= $adat['label']; ?>
     <?php
    if (trim($adat['help']!='')) {
        ?>
        
        <button type="button" class="" data-toggle="popover" data-placement="top" title="<?= $adat['help']; ?>">?</button>
        
        <?php
    }
    ?>
</div>
<?php
$lista = explode(',', $adat['lista']);

foreach($lista as $sor2):
    $r = explode (':',trim($sor2));
    $kulcs = trim($r[0]);
    $label = trim($r[1]);
    $selected = false;
    
    if (strpos($kulcs, '!')!==false) $selected = true;
    $kulcs = trim($kulcs,'!');
?>
<div class="input-group" style="width: 90%;margin-bottom: 3px;">
    
    <span class="input-group-addon">
        <input type="radio" value="<?= $kulcs; ?>" name="ue[<?= $sor['id']?>]" <?= (($selected)?' checked="checked" ':'')?> />
      </span>
      <input type="text" class="form-control"   placeholder="<?= $label; ?>" disabled="disabled" />
</div>
<?php 
endforeach;
?>
<br />