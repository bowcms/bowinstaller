<?php

class Contact {
    
    public function megjelenit($id) {
        
        $html = Html::peldany();
        $html->bodyVegeStart();
        ?>
        <script src="<?= BASE_URL; ?>modulok/contact/js/jqmask.js"></script>
        <?php
        $html->bodyVegeStop();
        
        $formId = uniqid(md5($id.rand(999,9999).time()));
                
        $lista = bowMeta('','',$id);
        include (modulHtmlElem('contact','formfej'));
        foreach ($lista as $sor) {
            $adat = $sor['ertek'];
            include (modulHtmlElem('contact',$sor['ertek']['tipus']));
        }
        include (modulHtmlElem('contact','submit'));
        
        
        $html->bodyVegeStart();
        ?>
        <script type="text/javascript">
            function bowContactValidal(id) {
                $('.keret_'+id + ' .fatyol').show();
                $.post('<?= BASE_URL ?>modulok/contact/contactAjax.php?id=<?= $id; ?>',$('#'+id).serialize(),function (ret){
                    $('.keret_'+id + ' .fatyol').fadeOut(1000);
                    eval( ret );
                    $('.bowContactError').hide();
                    
                    if (data.hiba) {
                        
                        for(i = 0; i < data.hiba.length; i++) {
                            
                            $('.hiba_'+data.hiba[i].id).html('<div class="alert alert-warning" >'+data.hiba[i].error+'</div>').show();
                        }
                    }
                    if (data.ok) {
                        $('.keret_'+id + ' .fatyol').hide();
                        $('.keret_'+id ).slideUp(1000, function(){
                            $('.keret_'+id ).html('<div class="alert alert-success" >'+data.valasz+'</div>');
                            $('.keret_'+id ).slideDown();
                            if(data.redir) {
                                window.location.href=data.redirect;
                            }
                        });
                    }
                });
            }
            $().ready(function(){
                $('.input-group-addon button').popover();
                
            });
        </script>
        <?php
        $html->bodyVegeStop();
        
        
    }
    

}