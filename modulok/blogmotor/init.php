<?php
/**
 * Title: Creating, and editind tags
 * Author: bowCms
 * Web: http://example.com
 * Description: You can add text tags for blogengine, and for other use
 * 
 **/

modulBejegyzes('Blog megjelenítése cimke alapján', 'blogmotor', true );

function blogmotor($a) {
    
    $o = modulOsztalyTolto('blogmotor');
    
    $o->megjelenit($a);
    
}
function blogmotorBeallito($admin, $adat) {
    if (isset($_POST['a'])) {
        
        $sql = "SELECT * FROM bow_cimkek WHERE id = ".(int)$_POST['a']['cimke_id'];
        $rs = sqlAssocRow($sql);
        if (!empty($rs)) {
            $cim = $rs['cimke'];
            $ret = array(
            'adat' => $_POST['a'],
            'cim' => $cim
            );
            return $ret;    
        } else {
            $admin->uzenetKiiras(__f('Hiba'), __f('Nem választottál ki cimkét!'));
        }
        
    }
    if (empty($adat)) {
        $adat = array(
            'cim' => __f('Blogoldal címe'),
            'cimke_id' => 0,
            'sablon' => '',
            'csakbevezeto' => 0,
            'mennyiseg' => 10,
            'rendezes' => 0,
            
            );
    }
    
    $cimkek = valasztoLista('bow_cimkek','id','cimke',' cimke ASc');
    $sablonok = array('' => __f('Alapértelmezett'));
    $admin->komp->adminFormFej(__f('Blogoldal beállítása'));
    $admin->komp->adminFormSelect(__f('Cimke kiválasztása'), 'a[cimke_id]',$adat['cimke_id'],$cimkek,__f('Cikmék létrehozása itt:').' '.BASE_URL.'unicorn/cimkek:unicornCimkeAdmin.html');
    $admin->komp->adminFormInput(__f('Sablon módósító'), 'a[sablon]',$adat['sablon'],__f('Alapértelmezett sablon a blogmotor modul html mappájában van, ha nem adsz meg semmit, az lesz a megjelenítő, ha megadsz akkor létrehoz egy módosítható másolatot a template könyvtárában'));
    $admin->komp->adminFormSelect(__f('Csak bevezető megjelenítése'),'a[csakbevezeto]', $adat['csakbevezeto'], array(0 => __f('Nem'), 1 => __f('Igen')),'');
    $admin->komp->adminFormInput(__f('Megjelenített cikkek száma'),'a[mennyiseg]' ,$adat['mennyiseg'],'');
    $admin->komp->adminFormSelect(__f('Rendezés'),'a[rendezes]',$adat['rendezes'],array(0=>'Blog',1=>'Legrégebbi legelől',3=>'Beállított sorrend szerint'),'');
    
    $admin->komp->adminFormLab();
    
    return false;
}

?>