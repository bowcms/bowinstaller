<?php

class blogMotor
{

    public function bejegyzesoldal($adat)
    {
        $adat = unserialize($adat);
        $url = Url::peldany();
        $parameterek = $url->param;
        if (!empty($parameterek)) {
            $cimkeId = current($parameterek);

            if ($cimkeId == '')
                return 0;
            $szoveg_id = next($parameterek);

            if (!is_numeric($cimkeId))
                return;


        } else {
            return;
        }

        $adat['mennyiseg'] = 10000;
        $adat['cimke_id'] = $cimkeId;
        $adat['szoveg_id'] = (is_numeric($szoveg_id)) ? $szoveg_id : false;
        $adat['bejegyzesoldal'] = 1;
        
        $this->megjelenit(serialize($adat));

    }

    public function megjelenit($adat)
    {
        
        //print_r ($adat);
        $bovebb = false;
        $bow = Bow::peldany();
        $bovebben = false;
        $url = Url::$peldany;
        $stilus = $url->stilus;


        if (!isset($adat['szoveg_id']))
            $adat['szoveg_id'] = false;
        if (!isset($adat['bejegyzesoldal']))
            $adat['bejegyzesoldal'] = false;
        if (!isset($adat['rendezes']))
            $adat['rendezes'] = 0;

        if ($bow->bovebben > 0 or $adat['szoveg_id'] > 0) {
            $bovebben = true;

            if ($adat['szoveg_id'] > 0) {
                // bejegyzés oldalon nyomat bővebbent
                $szId = $adat['szoveg_id'];
            } else {
                // cimke oldalon nyom bővebbent
                $szId = (int)$bow->bovebbParam[0];
            }


            $sql = "SELECT * FROM bow_szovegek WHERE id = " . $szId;

        } else {
            if ($adat['rendezes'] == 0)
                $rendezes = " letrehozva DESC ";
            if ($adat['rendezes'] == 1)
                $rendezes = " letrehozva ASC ";
            if ($adat['rendezes'] == 2)
                $rendezes = " x.sorrend ASC ";

            $sql = "SELECT COUNT(t.id) as ossz FROM bow_szovegek t, bow_cimkexszoveg x WHERE t.id = x.szoveg_id AND x.cimke_id = " .
                $adat['cimke_id'] . "
            ORDER BY $rendezes  ";
            $osszes = sqluniv3($sql);
            $osszesCikk = $osszes['ossz'];
            $start = 0;
            if (isset($_GET['lap'])) {
                $start = (int)$_GET['lap'];
            }
            $cikkszam = $adat['mennyiseg'];
            $sql = "SELECT t.* FROM bow_szovegek t, bow_cimkexszoveg x WHERE t.id = x.szoveg_id AND x.cimke_id = " .
                $adat['cimke_id'] . "
            ORDER BY $rendezes LIMIT $start,$cikkszam";
        }

        $textRs = sqlAssocArr($sql);
        if (!empty($textRs)) {
            

            $limit = 0;
            foreach ($textRs as $szoveg) {
                
                Memoria::ir('postText',$szoveg );
                Beepulok::futtat("runShortcodes");
                $szoveg = Memoria::olvas('postText');
                
                $sablon = "";
                $datum = date('Y-m-d', $szoveg['letrehozva']);
                $sql = "SELECT cimke FROM bow_cimkexszoveg, bow_cimkek WHERE bow_cimkexszoveg.cimke_id = bow_cimkek.id AND  bow_cimkexszoveg.szoveg_id = " .
                    $szoveg['id'] . " ORDER BY cimke ASC";
                $cimkek = sqluniv4($sql);
                $cimkeArr = array();
                foreach ($cimkek as $cimke) {
                    $cimkeArr[] = $cimke['cimke'];
                }
                $cimkek = implode(', ', $cimkeArr);

                $cikk = $szoveg;
                if ($adat['sablon']!='') {
                    if (!file_exists(BASE.'/temak/'.$url->stilus.'/html/blogmotor_blogmotor_alap_lista_nezet_'.$adat['sablon'].'.php'))
                        file_put_contents(BASE.'/temak/'.$url->stilus.'/html/blogmotor_blogmotor_alap_lista_nezet_'.$adat['sablon'].'.php', file_get_contents(BASE.'modulok/blogmotor/html/blogmotor_blogmotor_alap_lista_nezet.php'));
                    $sablon = '_'.$adat['sablon'];
                } else {
                    $sablon = '';
                }
                include (modulHtmlElem('blogmotor','alap_lista_nezet'.$sablon));


                //print $kimenet;
            }

            
        }
    }
}
