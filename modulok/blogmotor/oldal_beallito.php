<?php

/**

 * Blogmotor modul beállító

 * 

 * bow_oldalxmodul paraméter: a bow_szovegek tábla id, mást nem mentünk.

 * 

 * amit kapunk:

 * 

 * $oldalxmodulId: a modul és az oldalt össze kapcsoló tábla sorának id-je (ha nem újat viszünk fel, értéke > 0)

 * $oldalId: annak az oldalnak az id-je ahova a beállításokat és a modult mentjük

 * $modulId: ennek a modulnak az ID-je a bow_modulok táblában

 * $adat: a kapcsolt táblába mentett sor tartalma associatív tömbben

 * 

 * 

 * amit beállíthatunk: 

 * 

 * $sikeresBeallitas true ra ha megvagyunk a mentéssel, így visszakapjuk az adminban a modullistát

 * 

 * amit használhatunk:

 * 

 * _oldalak::parameterFelvitel($oldalId, $modulId, 'Bejegyzéscím', 'mentett adatok (szám szöveg, serializált tömb is lehet)', $oldalxmodulId);

 * 

 * 

 */

if (isset($adat['adat'])) if ($adat['adat'] != '') $adat = unserialize($adat['adat']); else $adat = '';



if (isset($_POST['a'])) {

    $a = $_POST['a'];

    $sql = "SELECT * FROM bow_cimkek WHERE id = ".(int)$a['cimke_id'];

    $cimkeRs = sqluniv3($sql);

    _oldalak::parameterFelvitel($oldalId, $modulId, 'Blogmotor - '.$cimkeRs['cimke'], serialize($a), $_POST['oldalxmodul_id']);

    

    $sikeresBeallitas = true;

    $html = Html::peldany();

    $html->uzenetKiiratas('Sikeresen mentettem a szöveget!');

}



$sql = "SELECT * FROM bow_cimkek ORDER BY cimke DESC";

$cimkeRs = sqluniv4($sql);




$sablonRs = szovegSablonok();



if (!isset($adat['cimke_id'])) {

    $adat['cimke_id'] = 0;

    $adat['mennyiseg'] = 3;

    $adat['lista_sablon'] = '';

    $adat['egyedul_sablon'] = '';

    $adat['blog_url'] = '';

    $adat['akcio'] = 0; 

    $adat['fokep_meret_x'] = 100;

    $adat['fokep_meret_y'] = 100;

    $adat['fokep_stilus'] = '';

    $adat['fokep_attr'] = '';

    $adat['tovabb_sablon'] = '';

    $adat['vissza_sablon'] = '';  
    
    $adat['lapozo'] = 1;  
    
    $adat['rendezes'] = 0;  

    //<p> <a href="#" class="cta_2">Bővebben »</a></p>

}
?>
<form method="post" action="<?= EPITO_URL; ?>" class="jNice">
    <p>

        <label>Cimke:</label>

        <select name="a[cimke_id]">

            <?php

            foreach ($cimkeRs as $cimke):

            ?>

            <option value="<?= $cimke['id']?>" <?= (($cimke['id']==$adat['cimke_id'])?' selected="selected" ':'')?>><?= $cimke['cimke']?></option>

            <?php

            endforeach;

            ?>

        </select>

    </p>
    <p>

        <label>Megjelenített mennyiség?</label>

        <input type="text" name="a[mennyiseg]" value="<?= $adat['mennyiseg'];?>" />

    </p>
    
    <p>

        <label>Lapozás korábbi bejegyzésekhez?</label>

        <select name="a[lapozo]" >

            <option value="0" <?= (($adat['lapozo']==0)?' selected="selected" ':'')?>  >Nem</option>

            <option value="1" <?= (($adat['lapozo']==1)?' selected="selected" ':'')?> >Igen</option>

            
        

        </select>
    </p>
    <p>

        <label>Rendezés?</label>

        <select name="a[rendezes]" >

            <option value="0" <?= (($adat['rendezes']==0)?' selected="selected" ':'')?>  >Blog (időben visszafele)</option>

            <option value="1" <?= (($adat['rendezes']==1)?' selected="selected" ':'')?> >Időrendben</option>

            <option value="2" <?= (($adat['rendezes']==2)?' selected="selected" ':'')?> >Beállított sorrend szerint</option>

            
        

        </select>
    </p>

    <p>
        <label>Cikk sablon amikor listában jelenik meg: (<a href="<?= ADMIN_URL; ?>_modulok/lista/beallit/9">Szövegsablonok létrehozása itt!)</a></label>
        <select name="a[lista_sablon]">
            <option value="cikk_blog_lista.php">Alapértelmezett lista sablon</option>
            <?php
            foreach ($sablonRs as $sablon):
            ?>
            <option value="<?= $sablon?>" <?= (($sablon==$adat['lista_sablon'])?' selected="selected" ':'')?>><?= $sablon?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    
    <p>
        <label>Cikk sablon amikor csak az adott jelenik meg:</label>
        <select name="a[egyedul_sablon]">
            <option value="cikk_blog_egycikk.php">Alapértelmezett egy cikk sablon</option>
            
            <?php
            foreach ($sablonRs as $sablon):
            ?>
            <option value="<?= $sablon?>" <?= (($sablon==$adat['egyedul_sablon'])?' selected="selected" ':'')?>><?= $sablon?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    
    <p>
        <label>Cikkre kattintva ez törtnik:</label>
        <select name="a[akcio]" onchange="if(this.selectedIndex==2)$('.blog_url').show(); else $('.blog_url').hide();">
            <option value="0" <?= (($adat['akcio']==0)?' selected="selected" ':'')?>  >Semmi</option>
            <option value="1" <?= (($adat['akcio']==1)?' selected="selected" ':'')?> >Adott cikket megjelenítjük</option>
            <option value="2" <?= (($adat['akcio']==2)?' selected="selected" ':'')?> >Adott URL-re ugrunk</option>
        
        </select>
    </p>
    
    <p class="blog_url" style="display: <?= (($adat['akcio']==2)?'inline':'none')?>;">
        <label>Url</label>
        <input type="text" name="a[blog_url]" value="<?= $adat['blog_url'];?>" />
    </p>
    <input type="hidden" name="hozzaad" value="<?= $_POST['hozzaad'] ?>" />
    <input type="hidden" name="oldalxmodul_id" value="<?= $oldalxmodulId; ?>" />
    <p>
        <label></label>
        <input type="submit" value="Mentés" />
    </p>
</form>