<?php
$html = Html::peldany();
if (isset($_GET['torol'])) {
    unlink(BASE.'temak/'.STILUS.'/html/'.$_GET['torol']);
    $html->uzenetKiiratas('Sikeres törlés.');
}
if (isset($_POST['b'])) {
    $nev = $_POST['b']['nev'];
    $kod = $_POST['b']['kod'];
    file_put_contents(BASE.'temak/'.STILUS.'/html/cikk_'.$nev.'.php', $kod);
    $html->uzenetKiiratas('Sikeres mentés');
}

if (isset($_GET['szerkeszt'])) {
    require_once (BASE . 'osztaly/osztaly_jsszerkeszto.php');
    jsSzerkeszto::kodSzerkeszto('kod', 'php');
    $fileNev = $_GET['szerkeszt'];
    $c = 1;
    $nev = substr($fileNev,5);
    $nev = str_replace('.php', '' ,$nev);
    $b = array();
    $b['nev'] = $nev;
    if (is_file(BASE.'temak/'.STILUS.'/html/'.$fileNev))  {
        $b['kod'] = file_get_contents(BASE.'temak/'.STILUS.'/html/'.$fileNev);
    } else {
        $b['kod'] = '';
    }
    ?>
    <fieldset>
        <form action="<?= $sajatUrl; ?>" method="post" class="jNice">
            
            <p>
                <label>Fájlnév</label>
                temak/<?= STILUS; ?>/html/cikk_<input name="b[nev]" value="<?= $b['nev'] ?>" />.php
            </p>
            <p>
                <label>HTML/PHP kód</label>
                <textarea name="b[kod]" id="kod"><?= htmlspecialchars( stripslashes($b['kod']));?></textarea>
            </p>
            
            <p>
                <input type="submit" value="Mentés"/>
            </p>
        </form>
    </fieldset>
    <?php
} else {
?>
<form method="post" action="<?= $sajatUrl; ?>?szerkeszt=uj" class="jNice" enctype="multipart/form-data">
    <input type="submit" value="Új szövegsablon készítése"/>
</form>
<table cellpadding="0" cellspacing="0">
<?php
$dir = BASE.'temak/'.STILUS.'/html/';
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            
            if ($file == '.' or $file == '..') continue;
            
            if (strpos($file, 'cikk_' )!== 0) continue;
            
        

?>
<tr>
    <td>
        <?= $file; ?>
    </td>
    <td class="action" style="vertical-align: middle;">
        <a class="delete" href="<?= $sajatUrl; ?>?torol=<?= $file;?>" onclick="if(confirm('Biztosan?')==false) return false;">Törlés</a>
        <a class="edit" href="<?= $sajatUrl; ?>?szerkeszt=<?= $file;?>">Szerkeszt</a>
    </td>
</tr>
<?php
            }
        closedir($dh);
    }
}

?>
</table>
<?php
}
?>