<?php

class _szovegekKomp {
    public function cikkMegjeleito($cikk, $bovebben = 0) {
        $url = Url::$peldany;
        $stilus = $url->stilus;
        
        if ($cikk["sablon"]!="") {
            if (file_exists(BASE.'temak/'.$stilus.'/html/'.$cikk["sablon"])) {
                
                include(BASE.'temak/'.$stilus.'/html/'.$cikk["sablon"]);
                
                return false;
            }
        }
        
        
        if(!file_exists(BASE.'temak/'.$stilus.'/html/cikk_egycikk_rendszer.php')) {
            if (!is_dir(BASE.'temak/'.$stilus.'/html')) mkdir(BASE.'temak/'.$stilus.'/html');
            
            $alapCikkMegjelenes = "
<h3><?= \$cikk['cim']; ?></h3>
        <?php
        if (\$cikk['bevezeto']!=''):
        ?>
        <p>
        <strong><?= \$cikk['bevezeto'];?></strong>
        </p>
        <?php
        endif;
        ?>
        <?php
        if (\$cikk['csakbevezeto']==0 or (\$cikk['csakbevezeto']==0 and \$bovebben != 0)):
        ?>
        <p>
        <?= \$cikk['tartalom'];?>
        </p>
        <?php
        endif;
        ?>
        <?php
        if (\$cikk['csakbevezeto']==1 and \$cikk['tartalom'] !='' and \$bovebben == false):
        ?>
        <br />
        <a class=\"textMore\" href=\"<?= rtrim(\$url->eredetiUt, '/'); ?>/bovebben/<?= Bow::\$aktivModulId.'/'.\$cikk['cim'].'.html'; ?>\">Bővebben</a>
        <?php
        endif;
        ?>
        <?php
        if (\$bovebben == 1):
        ?>
        <br />
        <a class=\"textMore\" href=\"javascript:window.history.back();\">Vissza</a>
        <?php
        endif;
        ?>
            ";
            file_put_contents(BASE.'temak/'.$stilus.'/html/cikk_egycikk_rendszer.php', $alapCikkMegjelenes);
            print "Mentés";
        } 
        
        include(BASE.'temak/'.$stilus.'/html/cikk_egycikk_rendszer.php');
        
       
    }
}