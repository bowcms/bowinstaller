<?php
/**
 * Title: Text for pages
 * Author: bowCms
 * Web: http://example.com
 * Description: Text element for pages
 * Role: system
 * 
 **/

modulBejegyzes('One text for pages', 'szovegMegjelenites', true );

function szovegMegjelenites($adat) {
    osztalyIndit('_szovegek', 'cikkMegjeleito',$adat['szoveg_id']);
}

function szovegMegjelenitesBeallito($admin, $adat) {
    // feldolgozás
    
    $tag = Tagok::peldany();
    $_POST = Memoria::olvas('xss_orig_post');
    if (isset($_POST['lezeto_szoveg_id'])) {
        $sql  = "SELECT cim FROM bow_szovegek WHERE id = ".(int)$_POST['lezeto_szoveg_id'];
        $rs = sqluniv3($sql);
        if (isset($rs['cim']))
        return array(
            'adat' => array('szoveg_id' => (int)$_POST['lezeto_szoveg_id']),
            'cim' => $rs['cim']
        );
    }
    
    if (isset($_POST['a'])) {
        $a = $_POST['a'];
        if ($_POST['fokepfeluliras']!='') {
            $a['fokep'] = 'feltoltesek/cikkgaleria/'.$a['id'].'/'.$_POST['fokepfeluliras'];
        }
        if ($a['id']==0) {
            unset($a['id']);
            sqlfelvitel($a, 'bow_szovegek');
            $id = mysql_insert_id();
        } else {
            sqlmodositas($a, 'bow_szovegek');
            $id = $a['id'];
        }
        
        $ret = array(
            'adat' => array('szoveg_id' => $id),
            'cim' => $a['cim']
        );
        return $ret;
    }
    
    if (empty($adat)) {
        $a = array(
            'cim' => '',
            'bevezeto' => '',
            'tartalom' => '',
            'csakbevezeto' => 0,
            'id' => 0,
            'szerzo' => $tag->id,
            'letrehozva' => time());
    } else {
        $sql = "SELECT * FROM bow_szovegek WHERE id = ".(int)$adat['szoveg_id'];
        $a = sqluniv3($sql);
    }
    Memoria::ir('_szerkeszto_Szovegszerkesztes_formKirakasElott', $a);
    
    $admin->komp->adminFormFej(__f('Korábban elkészített szöveg hozzáadása'));
    
    // új vagy használt szöveg
    $szovegek = valasztoLista('bow_szovegek','id', 'cim', 'cim ASC');
    $admin->komp->adminFormSelect(__f('Létező szöveg kiválasztása'), 'lezeto_szoveg_id',0,$szovegek,__f('Korábban létrehozott szöveg hozzáadásához válassz ki egyet, és kattints a ments gombra!'));
    $admin->komp->adminFormLab();
    
    $admin->komp->adminFormFej(__f('Szöveges tartalom szerkesztése'));
    $admin->komp->adminFormInput(__f('Cím'),'a[cim]', $a['cim']);
    $admin->komp->adminFormText(__f('Bevezető'), 'a[bevezeto]', $a['bevezeto']);
    $admin->komp->adminFormTextAdv(__f('Tartalom'), 'a[tartalom]', $a['tartalom']);
    $admin->komp->adminFormHidden('a[szerzo]', $a['szerzo']);
    $admin->komp->adminFormHidden('a[letrehozva]', $a['letrehozva']);
    $admin->komp->adminFormHidden('a[id]', $a['id']);
    $admin->komp->adminFormHidden('fokepfeluliras', '');
    
    
    
    
    Beepulok::futtat('postEditorFormBottom', $a);
    
    $a = Memoria::olvas('_szerkeszto_Szovegszerkesztes_formKirakasElott');
    
    
    $admin->komp->adminFormLab();
    // még nem vagyunk készen
    return false;
}

?>