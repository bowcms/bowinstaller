<?php
/**
 * Szöveg modul beállító
 * 
 * bow_oldalxmodul paraméter: a bow_szovegek tábla id, mást nem mentünk.
 * 
 * amit kapunk:
 * 
 * $oldalxmodulId: a modul és az oldalt össze kapcsoló tábla sorának id-je (ha nem újat viszünk fel, értéke > 0)
 * $oldalId: annak az oldalnak az id-je ahova a beállításokat és a modult mentjük
 * $modulId: ennek a modulnak az ID-je a bow_modulok táblában
 * $adat: a kapcsolt táblába mentett sor tartalma associatív tömbben
 * 
 * 
 * amit beállíthatunk: 
 * 
 * $sikeresBeallitas true ra ha megvagyunk a mentéssel, így visszakapjuk az adminban a modullistát
 * 
 * amit használhatunk:
 * 
 * _oldalak::parameterFelvitel($oldalId, $modulId, 'Bejegyzéscím', 'mentett adatok (szám szöveg, serializált tömb is lehet)', $oldalxmodulId);
 * 
 * 
 */

if (isset($_POST['sz'])) {


    if ($_POST['letezoSzoveg'] != '') {
        $szovegId = $_POST['letezoSzoveg'];
        $sql = "SELECT cim FROM bow_szovegek WHERE id = $szovegId LIMIT 1";
        $rs = sqluniv3($sql);
        
        $sz['cim'] = $rs['cim'];
        print_r ($sz['cim']);
    } else {
        $sz = $_POST['sz'];
        if ($sz['id'] > 0) {
            sqlmodositas($sz, 'bow_szovegek');
            $szovegId = $sz['id'];
        } else {
            unset($sz['id']);
            sqlfelvitel($_POST['sz'], 'bow_szovegek');
            $szovegId = mysql_insert_id();
        }
    }


    _oldalak::parameterFelvitel($oldalId, $modulId, 'Egy szöveg: ' . $sz['cim'], $szovegId,
        $_POST['oldalxmodul_id']);
    // ezzel jelezzük, hogy kész vagyunk
    $sikeresBeallitas = true;
    $html = Html::peldany();
    $html->uzenetKiiratas('Sikeresen mentettem a szöveget!');
} else {


    $tag = Tagok::peldany();
    if (isset($adat['adat'])) {
        $szovegId = (int)$adat['adat'];
    } else {
        $szovegId = 0;
    }

    if ($szovegId > 0) {
        $sql = "SELECT * FROM bow_szovegek WHERE id = " . $szovegId;
        $sz = sqluniv3($sql);
    } else {
        $sz = array(
            'cim' => '',
            'bevezeto' => '',
            'tartalom' => '',
            'csakbevezeto' => 0,
            'szerzo' => $tag->id,
            'id' => 0,
            'cimke' => '',
            'sablon' => 0,
            'letrehozva' => time());
    }
    require_once (BASE . 'osztaly/osztaly_jsszerkeszto.php');
    jsSzerkeszto::szovegszerkeszto('tartalom');

    $sql = "SELECT id, cim FROM bow_szovegek ORDER BY cim ASC";
    $szovegek = sqluniv4($sql);
?>
<form method="post" action="<?= EPITO_URL; ?>" class="jNice">
    <p>
        <label>Már létező szöveget jelenítek meg itt:</label>
        <select name="letezoSzoveg">
            <option value="">Nem, újat készítek vagy ezt módosítom</option>
            <?php
    foreach ($szovegek as $szoveg) {

?>
                <option value="<?= $szoveg['id'] ?>"><?= $szoveg['cim'] ?></option>
                <?php
    }
?>
        </select>
    
    </p>
    <p>
        <label>Cím:</label>
        <input name="sz[cim]" value="<?= $sz['cim'] ?>" />
    </p>
    <p>
        <label>Bevezető:</label>
        <textarea class="editText" name="sz[bevezeto]" ><?= $sz['bevezeto'] ?></textarea>
    </p>
    <p>
        <label>Tartalom:</label>
        <textarea class="editText" id="tartalom" name="sz[tartalom]" ><?= $sz['tartalom'] ?></textarea>
    </p>
    <p>
        <label>Csak a bevezető megjelenítése:</label>
        <select name="sz[csakbevezeto]">
            <option value="0" <?= (($sz['csakbevezeto'] == 0) ?
    ' selected="selected" ' : '') ?> >Nem</option>
            <option value="1" <?= (($sz['csakbevezeto'] == 1) ?
        ' selected="selected" ' : '') ?> >Igen</option>
            
        </select>
    </p>
    
    <p>
        <label>Sablon: (ezzel beállíthatod, hogy, hogyan nézzen ki a cikk. Sablonokat a Modullista menüpontban a Szövegszerkesztő modul Beállításainál hozhatsz létre)</label>
        <select name="sz[sablon]">
            <option value="0" <?= (($sz['sablon'] == '0') ?
' selected="selected" ' : '') ?>>Alapértelmezett</option>
            <?php
    $rs = szovegSablonok();
    if (!empty($rs))
        foreach ($rs as $sor):
?>
            <option value="<?= $sor ?>" <?= (($sz['sablon'] == $sor) ?
' selected="selected" ' : '') ?>><?= $sor ?></option>
            <?php
        endforeach;
?>
        
        </select>
        
    </p>
    
    <p>
        <label> </label>
        <input type="submit" value="Kész vagyok" />
    </p>
    <input type="hidden" name="sz[id]" value="<?= $sz['id'] ?>" />
    <input type="hidden" name="sz[szerzo]" value="<?= $sz['szerzo'] ?>" />
    <input type="hidden" name="sz[letrehozva]" value="<?= $sz['letrehozva'] ?>" />
    <!-- a köv 2 paramáter megadása fontos, ez vezérli a tartalom hozzáfűzést -->
    <input type="hidden" name="hozzaad" value="<?= $_POST['hozzaad'] ?>" />
    <input type="hidden" name="oldalxmodul_id" value="<?= $oldalxmodulId; ?>" />
    
    
    
</form>
<?php
    Beepulok::futtat('_szerkeszto_Szövegszerkesztés', $sz);
    ?>
<?php

}

