<?php

/**
 * _modulok.php
 * 
 * modulok kezelse, telepítése, eltávolítása
 */

class _modulok
{

    public $komp;

    

    public function lista($feladat = '', $id = 0)
    {
        $id = (int)$id;
        $html = Html::peldany();
        $html->helyorzo('lapCim', __f('Modulok listája'));

        $u = osztaly_unicorn_futtato::peldany();
        
        if (isset($_POST['mappa'])) {
            if (file_exists(BASE . '/modulok/' . $_POST['mappa'] . '/init.php')) {
                $iniFile = file_get_contents(BASE . '/modulok/' . $_POST['mappa'] . '/init.php');
                preg_match_all('#/*\s+(\w+):(.*)#', $iniFile, $talalat);
                if (isset($talalat[1][0])) {
                    foreach ($talalat[1] as $k => $v) {
                        $ujModul[$v] = $talalat[2][$k];
                    }
                }
                $rendszer = 0;
                if (isset($ujModul['Role'])) {
                    if ($ujModul['Role']=='system') {
                        $rendszer = 1;
                    }
                }
                $a = array(
                    'mappa' => $_POST['mappa'],
                    'nev' => ((isset($ujModul['Title']))?$ujModul['Title']:$_POST['mappa']),
                    'leiras' => ((isset($ujModul['Description']))?$ujModul['Description']:''),
                    'statusz' => 1,
                    'rendszer' => $rendszer
                );
                
                include_once(BASE . '/modulok/' . $_POST['mappa'] . '/init.php');
                $install = $_POST['mappa'].'Install';
                if (function_exists($install)) {
                    $eredmeny = call_user_func($install);
                    $u->uzenetKiiras(__f('Modul telepítés sikeres'), $eredmeny);
                }
                
                sqladat($a , 'bow_rendszer');
            }
        }

        switch ($feladat) {
            case 'kapcsol':
                $sql = "SELECT statusz FROM bow_rendszer WHERE id = $id";
                $rs = sqluniv3($sql);
                if ($rs['statusz'] == 0) {
                    $statusz = 1;
                    $u->komp->popUpNoti(__f('Modul bekapcsolva'), 'Modul státusz beállítás sikeres');
                } else {
                    $statusz = 0;
                    $u->komp->popUpNoti(__f('Modul kikapcsolva'), 'Modul státusz beállítás sikeres');

                }
                $sql = "UPDATE bow_rendszer SET statusz =  $statusz WHERE id = $id LIMIT 1";

                sqluniv($sql);
                if (mysql_affected_rows() > 0) {
                    
                } else {
                    
                }
                break;
            case 'beallit':
                $html->helyorzo('lapCim', 'Modul beállítása');
                $sql = "SELECT * FROM bow_modulok WHERE id = $id";
                $rs = sqluniv3($sql);

                $beallito = $rs['beallito'];
                $utvonal = BASE . 'modulok/' . $rs['modul'] . '/' . $beallito;

                $visszaUrl = ADMIN_URL . '_modulok/lista';
                $sajatUrl = ADMIN_URL . '_modulok/lista/beallit/' . $id . '/';
                if (file_exists($utvonal)) {
                    if ($this->komp->beallitoKeret($utvonal, $sajatUrl, $visszaUrl, $rs) == false) {
                        return;
                    }

                }
                break;
        }
        // kikapcsolt modulok listája

        $osszModul = array();

        $sql = "SELECT  * FROM bow_rendszer WHERE statusz = 1 ORDER BY nev ASC";
        $rs = sqluniv4($sql);

        if (!empty($rs))
            $this->komp->modullista(__f('Bekapcsolt modulok'), $rs);

        foreach ($rs as $sor) {
            $osszModul[$sor['mappa']] = 1;
        }

        $sql = "SELECT  * FROM bow_rendszer WHERE statusz = 0 ORDER BY nev ASC";
        $rs = sqluniv4($sql);

        foreach ($rs as $sor) {
            $osszModul[$sor['mappa']] = 1;
        }

        if (!empty($rs))
            $this->komp->modullista(__f('Kikapcsolt modulok'), $rs);

        $dir = BASE . 'modulok/';

        $ujModulok = array();
        $i = 0;
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {

                    if ($file == '.' or $file == '..')
                        continue;
                    if (array_key_exists($file, $osszModul))
                        continue;
                    if (is_file($dir . $file . '/init.php')) {

                        $iniFile = file_get_contents($dir . $file . '/init.php');

                        preg_match_all('#/*\s+(\w+):(.*)#', $iniFile, $talalat);

                        if (isset($talalat[1][0])) {
                            foreach ($talalat[1] as $k => $v) {
                                $ujModulok[$i][$v] = $talalat[2][$k];
                            }
                        }
                        if (!isset($ujModulok[$i]['Title']))
                            $ujModulok[$i]['Title'] = $file;
                        $ujModulok[$i]['mappa'] = $file;
                        $i++;


                    }
                }

                if (!empty($ujModulok))
                    $this->komp->ujModul(__f('Telepíthető modulok'), $ujModulok);

            }
        }

    }
}


?>