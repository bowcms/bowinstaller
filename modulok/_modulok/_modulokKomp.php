<?php

/**
 * _modulokKomp.php
 * 
 * modulok admin modul html elemei
 * 
 */
 
class _modulokKomp {
    
    public function feltolto() {
        ?>
        <fieldset>
            <legend>Modulok telepítése/újratelepítése</legend>
            <p>Itt telepítheted moduljaidat. Töltsd fel a installációs zip file-t. Telepítés után a Modulkezelő menüpontben beállíthatod annak működését.</p>
            
            <form method="post" action="<?= UNICORN; ?>_modulok/telepito" enctype="multipart/form-data">
                <input name="zip" type="file" />
                <br />
                <input type="submit"/>
            </form>
        </fieldset>
        <?php
    }
    
    public function modullista($cim, $lista) {
        ?>
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5><?= $cim; ?></h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th><?= __f('Modulnév');?></th>
											<th><?= __f('Leírás');?></th>
											<th><?= __f('Műveletek');?></th>
											
										</tr>
									</thead>
									<tbody>
									
									
            				
            <?php
            foreach ($lista as $elem):
            ?>
            <tr>
                <td><?= $elem['nev'];?></td>
                <td><?= $elem['leiras']; ?></td>
                
                <td class="action">
                    
                    
                    <a class="delete" href="<?= UNIC_URL.'kapcsol/'.$elem['id']; ?>">
                        <?php
                        if ($elem['rendszer']==0):
                        ?>
                        <?= (($elem['statusz']==1)?__f('Kikapcsol'):__f('Bekapcsolás')); ?>
                        <?php
                        endif;
                        ?>
                    </a>
                </td>
                
            </tr> 
            
            <?php
            endforeach;
            ?>
            </tbody>
        </table>							
    </div>
</div>
        <?php
    }
    
    public function beallitoKeret($utvonal,$sajatUrl,  $visszaUrl, $modul) {
        
        ?>
        
            <h3><?= $modul['modulnev']?> általános beállításai</h3>
            <fieldset><?php include($utvonal);?> </fieldset>
            <div>
                <form action="<?= $visszaUrl ?>" method="post" class="jNice">
                    <input type="submit" value="Vissza"/>
                </form>
            </div>
            
       
        <?php
        
    }
    
    public function ujModul($cim, $lista) {
        ?>
        <div class="widget-box">
							<div class="widget-title">
								<span class="icon">
									<i class="icon-th"></i>
								</span>
								<h5><?= $cim; ?></h5>
							</div>
							<div class="widget-content nopadding">
								<table class="table table-bordered table-striped">
									<thead>
										<tr>
											<th><?= __f('Modulnév');?></th>
											<th><?= __f('Leírás');?></th>
											<th><?= __f('Műveletek');?></th>
											
										</tr>
									</thead>
									<tbody>
									
									
        <?php
        
        foreach ($lista as $sor) {
            $cim = $sor['Title'];
            if (isset($sor['Description'])) $leiras = $sor['Description']; else $leiras = '';
            if (isset($sor['Author'])) $szerzo = $sor['Author']; else $szerzo = '';
            if (isset($sor['Web'])) $web = $sor['Web']; else $web = '#';
            ?>
            <tr>
                <td><?= $cim?></td>
                <td><a href="<?= $web?>" target="_blank"><?= $szerzo?></a></td>
                <td><?= $leiras?></td>
                <td><form method="post" style="display: inline;"><input type="submit" class="btn btn-inverse"  value="<?= __f('Telepítés'); ?>"/><input type="hidden" name="mappa" value="<?= $sor['mappa']?>"/></form></td>
            </tr>
            
            <?php
            
            
        }
        ?>
        </tbody>
        </table>							
    </div>
</div>
        <?php
    }
}