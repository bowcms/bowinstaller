<?php
/**
 * Title: Add or remove modules
 * Author: bowCms
 * Web: http://example.com
 * Description: Modul installation tools
 * 
 **/
 
adminMenu(__f('Modulok'), 'unicornOldalMenu', __f('Eszközök'), 'modulokUnicorn', 'icon-retweet');

function modulokUnicorn($feladat = '', $id = '') {
    
    
    require_once(dirname(__FILE__).'/_modulok.php');
    require_once(dirname(__FILE__).'/_modulokKomp.php');
    $o = new _modulok();
    $o->komp = new _modulokKomp();
    $o->lista($feladat, $id);
}