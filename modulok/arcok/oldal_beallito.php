<?php
/**
 * Arcok modul beállító
 * 
 * bow_oldalxmodul paraméter: a bow_szovegek tábla id, mást nem mentünk.
 * 
 * amit kapunk:
 * 
 * $oldalxmodulId: a modul és az oldalt össze kapcsoló tábla sorának id-je (ha nem újat viszünk fel, értéke > 0)
 * $oldalId: annak az oldalnak az id-je ahova a beállításokat és a modult mentjük
 * $modulId: ennek a modulnak az ID-je a bow_modulok táblában
 * $adat: a kapcsolt táblába mentett sor tartalma associatív tömbben
 * 
 * 
 * amit beállíthatunk: 
 * 
 * $sikeresBeallitas true ra ha megvagyunk a mentéssel, így visszakapjuk az adminban a modullistát
 * 
 * amit használhatunk:
 * 
 * _oldalak::parameterFelvitel($oldalId, $modulId, 'Bejegyzéscím', 'mentett adatok (szám szöveg, serializált tömb is lehet)', $oldalxmodulId);
 * 
 * 
 */
if (isset($adat['adat'])) if ($adat['adat'] != '') $adat = unserialize($adat['adat']); else $adat = '';


if (isset($_POST['a'])) {
    $a = $_POST['a'];
    
    _oldalak::parameterFelvitel($oldalId, $modulId, 'Munkatársak megjelenítse ', serialize($a), $_POST['oldalxmodul_id']);
    
    $sikeresBeallitas = true;
    $html = Html::peldany();
    $html->uzenetKiiratas('Sikeres beállítás!');
}



if (!isset($adat['mennyiseg'])) {
    $adat['mennyiseg'] = 3;
    $adat['lista_sablon'] = 0;
    $adat['fokep_meret_x'] = 100;
    $adat['fokep_meret_y'] = 100;
    $adat['fokep_stilus'] = '';
    $adat['fokep_attr'] = '';
    $adat['rendezes'] = '';
    
   
}
?>
<form method="post" action="<?= EPITO_URL; ?>" class="jNice">
    
    <p>
        <label>Megjelenített munkatársak száma?</label>
        <input type="text" name="a[mennyiseg]" value="<?= $adat['mennyiseg'];?>" />
    </p>
    
    <p>
        <label>Megjelenítés sablonja: (<a href="<?= ADMIN_URL; ?>_modulok/lista/beallit/15">Sablonok létrehozása itt!)</a></label>
        <select name="a[lista_sablon]">
            <?php
            $sablonRs = epitoElemek();
            foreach ($sablonRs as $sablon):
            ?>
            <option value="<?= $sablon?>" <?= (($sablon==$adat['lista_sablon'])?' selected="selected" ':'')?>><?= $sablon; ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    
    
    <p>
        <label>Rendezés:</label>
        <select name="a[rendezes]" >
            <option value="0" <?= (($adat['rendezes']==0)?' selected="selected" ':'')?>  >ABC</option>
            <option value="1" <?= (($adat['rendezes']==1)?' selected="selected" ':'')?> >Véletlenszerű</option>
            
        </select>
    </p>
    
    <input type="hidden" name="hozzaad" value="<?= $_POST['hozzaad'] ?>" />
    <input type="hidden" name="oldalxmodul_id" value="<?= $oldalxmodulId; ?>" />
    <p>
        <label></label>
        <input type="submit" value="Mentés" />
    </p>
</form>