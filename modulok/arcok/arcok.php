<?php

class Arcok {
    
    public function lista($adat){
        $adat = unserialize($adat);
        $sablonFile = $adat['lista_sablon'];
        $url = Url::$peldany;
        $stilus = $url->stilus;
        
        if(!file_exists(BASE.'temak/'.$stilus.'/html/'.$sablonFile)) {
                $sablon = '<strong>Munkatársak sablon nem található: '.BASE.'temak/'.$stilus.'/html/'.$sablonFile.'</strong>';    
                file_put_contents(BASE.'temak/'.$stilus.'/html/'.$sablonFile, $sablon);
        }
        
        $kimenet = '';
        
        $rendezes = ' ORDER BY '.(($adat['rendezes'] == 0)?' nev ASC ':' RAND() ');
        $sql = "SELECT * FROM palanta_arcok $rendezes LIMIT ".(int)$adat['mennyiseg'];
        $lista = sqluniv4($sql);
        
        foreach ($lista as $sor) {
            
            //print $kimenet;
            
            include(BASE.'temak/'.$stilus.'/html/'.$sablonFile);
            
        }
        
    }
}
