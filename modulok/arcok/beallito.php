<?php
$html = Html::peldany();

if (isset($_GET['torol'])) {
    
    $sql = "DELETE FROM  palanta_arcok WHERE id = ".(int)$_GET['torol'];
    sqluniv($sql);
    $html->uzenetKiiratas('Törlés megtörtént.');
}
if (isset($_POST['b'])) {
    $html = Html::peldany();
    if (isset($_POST['keptorles'])) {
        $_POST['b']['kep'] = '';
        $kepRs = getCol('kep','palanta_arcok',' id = '.$_POST['b']['id']);
        unlink(BASE.'feltoltesek/arcok/'.$kepRs);
        $html->uzenetKiiratas('Kép törlése sikerült!');
    }
    
    if ($_FILES['kep']['name']!='') {
        $ext = kiterjesztes($_FILES['kep']['name']);
        
        $ujNev = 'arcok_'.time().rand(11,99).'.'.$ext;
        if(!move_uploaded_file($_FILES['kep']['tmp_name'],BASE.'feltoltesek/arcok/'.$ujNev )) {
            $html->hibaKiiratas('File mentése nem sikerült!');
        } else {
            $_POST['b']['kep'] = $ujNev;
        }
    }
    
    sqladat($_POST['b'], 'palanta_arcok');
    $html->uzenetKiiratas('Sikeres mentés');
}


if (isset($_GET['szerkeszt'])) {
    $sql = "SELECT * FROM palanta_arcok WHERE id = ".(int)$_GET['szerkeszt'];
    $b = sqluniv3($sql);
    
    ?>
    <fieldset>
        <form action="<?= $sajatUrl; ?>" method="post" class="jNice" enctype="multipart/form-data">
            <input type="hidden" name="b[id]" value="<?= $b['id']; ?>"/>
            <p>
                <label>Név</label>
                <input name="b[nev]" value="<?= $b['nev'] ?>" />
            </p>
            <p>
                <label>Bemutatás (1-2 mondatos)</label>
                <textarea name="b[bemutato]" ><?= htmlspecialchars($b['bemutato']); ?></textarea>
            </p>
            <p>
                <label>E-mail</label>
                <input name="b[email]" value="<?= $b['email'] ?>" />
            </p>
            <p>
                <label>Egyéb elérhetőség</label>
                <textarea name="b[elerhetoseg]" ><?= htmlspecialchars($b['elerhetoseg']);?></textarea>
            </p>
            <p>
                <label>Publikus?</label>
                <select name="b[publikus]">
                    <option value="0" <?= $b['publikus']==0?' selected="selected" ':'' ?>>Igen</option>
                    <option value="1" <?= $b['publikus']==1?' selected="selected" ':'' ?>>Nem</option>
                </select>
            </p>
            <p>
                <?php
                    $vanKep = false;
                    if ($b['kep']!='')
                        if (is_file(BASE.'feltoltesek/arcok/'.$b['kep'])) {
                            $vanKep = true;
                            kiskepKocka(BASE_URL.'feltoltesek/arcok/'.$b['kep'],100,' style="float:right; margin:5px; border:3px solid #aaa;" ');
                        }
                ?>
                <label>Arckép</label>
                <input type="file" name="kep" />
                <?php
                if ($vanKep):
                ?>
                <br /><br />
                <input name="keptorles" value="1" type="checkbox"/> - kép törlése
                <?php endif; ?>
            </p>
            <p>
                <input type="submit" value="Mentés"/>
            </p>
        </form>
    </fieldset>
    <?php
} else {
?>
<h3>Munkatársak listája</h3>
<form method="post" action="<?= $sajatUrl; ?>?szerkeszt=0" class="jNice" >
    
    
    <input type="submit" value="Új munkatárs felvitele"/>
</form>
<br />
<table cellpadding="0" cellspacing="0">
<?php
$sql = "SELECT * FROM palanta_arcok ORDER BY nev ASC";
$rs = sqluniv4($sql);
if(!empty($rs))
foreach($rs as $sor):

?>
<tr>
    <td>
        <?= $sor['nev']?>
    </td>
    <td>
        <?= $sor['elerhetoseg']?>
    </td>
    <td class="action" style="vertical-align: middle;">
        <a class="delete" href="<?= $sajatUrl; ?>?torol=<?= $sor['id']?>" onclick="if(confirm('Biztosan?')==false) return false;">Törlés</a>
        <a class="delete" href="<?= $sajatUrl; ?>?szerkeszt=<?= $sor['id']?>">Szerkesztés</a>
    </td>
</tr>
<?php
endforeach;
?>
</table>
    


<br style="clear: both;"/>

<?php
}