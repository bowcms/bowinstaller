<?php



class hirlevel
{

    public function cron()
    {
        $kuldesSzam = 0;
        $sql = "SELECT * FROM bow_hirlevel_feliratkozas WHERE kikuldve < '" . (time() -
            24 * 60 * 60) . "'";
        $cimzettek = sqluniv4($sql);
        if (!empty($cimzettek)) {
            foreach ($cimzettek as $sor) {
                $cimke_id = $sor['cimke'];
                $szoveg_id = $sor['szoveg_id'];
                
                
                $sql = "SELECT *, sz.id as szid FROM bow_szovegek sz, bow_cimkexszoveg x WHERE 
                x.cimke_id = $cimke_id AND x.szoveg_id = sz.id AND sz.id > $szoveg_id ORDER BY x.id ASC LIMIT 1";
                    $sz = sqluniv3($sql);
                    
                if (!isset($sz['szid'])) continue; // nincs kiküldhető levél
                $levelKod['kod'] = $cimke_id . '_sz' . $sz['szid'];
                $sql = "SELECT * FROM bow_hirlevel_keszlevelek WHERE kod = '" . $levelKod['kod'] .
                    "' LIMIT 1";
                $levelRs = sqluniv3($sql);
                print $sql;
                if (!isset($levelRs['id'])) {
                    $sql = "SELECT max(id) as mid FROM bow_hirlevel_keszlevelek";
                    $mRs = sqluniv3($sql);
                    $onlId = $mRs['mid'] + 1;

                    

                    // ha van új hírlevél
                    $email = $sor['email'];

                    ob_start();
                    include (modulHtmlElem('hirlevel'));

                    $level = ob_get_contents();
                    ob_end_clean();


                    //print $level;
                    $levelKod['kod'] = $cimke_id . '_sz' . $sz['szid'];
                    $levelKod['tartalom'] = $level;
                    $levelKod['szoveg_id'] = $sz['szid'];
                    sqladat($levelKod, 'bow_hirlevel_keszlevelek');

                    $levelRs = $levelKod;


                } else {
                    $level = $levelRs['tartalom'];
                }

                preg_match_all('/< *img[^>]*>/i', $level, $kepek);

                $csatolmanyok = array();
                $k = 0;
                foreach ($kepek[0] as $kep) {
                    $parser = xml_parser_create();
                    xml_parse_into_struct($parser, $kep, $values);

                    $src = $values[0]['attributes']['SRC'];

                    $helyiKep = sajatMasolatWebrol($src);


                    $csatolmanyok[$k] = $helyiKep['path'];
                    $level = str_replace($src, 'cid:kep_' . $k, $level);


                    $k++;

                }

                Levelezo::kuldesCsatolmannyal($sor['email'], 'Gyógyító Kód hírlevél', $level, $csatolmanyok);
                //print $level;
                $sql = "UPDATE bow_hirlevel_feliratkozas SET kikuldve = '" . (time()) .
                    "', szoveg_id = " . $levelRs['szoveg_id'] . " WHERE email = '" . $sor['email'] .
                    "'";
                sqluniv($sql);
                
                $kuldesSzam++;
                if ($kuldesSzam==5) exit;


            }
        }

        $szovegek = szovegCimkeSzerint('Hírlevél');
        $szovegSzam = count($szovegek);

    }

    public function feliratkozasok()
    {
        if (isset($_GET['onl'])) {
            $sql = "SELECT * FROM bow_hirlevel_keszlevelek WHERE id = ".(int)$_GET['onl'];
            $rs = sqluniv3($sql);
            if (isset($rs['id'])) {
                $sql = "SELECT * FROM bow_szovegek WHERE id = ".$rs['szoveg_id'];
                $sz = sqluniv3($sql);
                include (modulHtmlElem('hirlevel', 'aktkoszonet'));
                return;
            }
        }

        if (isset($_GET['kod'])) {
            $email = base64_decode($_GET['kod']);
            $sql = "SELECT * FROM bow_hirlevel_feliratkozas WHERE megerosito_kod = '" .
                htmlspecialchars($_GET['kod']) . "' ORDER BY id DESC LIMIT 1";
            $rs = sqluniv3($sql);
            if (isset($rs[id]))
                if ($rs['megerositve'] == 0) {
                    $a['id'] = $rs['id'];
                    $a['megerositve'] = 1;
                    sqladat($a, 'bow_hirlevel_feliratkozas');
                    $sz = szovegKulcsSzerint('hirl_akt_kosz');
                    include (modulHtmlElem('hirlevel', 'aktkoszonet'));
                    return;
                }
        }
        if (isset($_GET['leir'])) {
            $email = base64_decode($_GET['kod']);
            $sql = "SELECT * FROM bow_hirlevel_feliratkozas WHERE megerosito_kod = '" .
                htmlspecialchars($_GET['leir']) . "' ORDER BY id DESC LIMIT 1";
            $rs = sqluniv3($sql);
            if (isset($rs[id])) {
                $a['id'] = $rs['id'];

                sqltorles($a, 'bow_hirlevel_feliratkozas');
                $sz = szovegKulcsSzerint('hirl_leiratkozas');
                include (modulHtmlElem('hirlevel', 'aktkoszonet'));
                return;
            }

        }

        $sz = szovegKulcsSzerint('hirl_hiba');
        include (modulHtmlElem('hirlevel', 'aktkoszonet'));
        return;
    }


    public function wgFeliratkozas($cim, $leiras, $sikeresFeliratkozasSzoveg, $cimke)
    {

        if (isset($_POST['hirlev']['nev'])) {

            $hiba = false;

            $a = $_POST['hirlev'];

            if ($a['nev'] == '') {

                $hiba = 'Hiányos név';

            }

            if (!isEmail($a['email'])) {

                $hiba = 'Hibás E-mail';

            }

            if (!$hiba) {

                $a['cimke'] = $cimke;


                include (htmlElem('sikeres'));


                // megerősítő

                $level = Levelezo::peldany();

                $level->sablon('hirlevel_megerosito');


                $a['megerosito_kod'] = base64_encode($a['email']);

                $link = BASE_URL . "hirlevel_feliratkozas?kod=" . $a['megerosito_kod'];

                $level->helyettesitoHozzaad('LINK', '<a href="' . $link . '"  >' . $link .
                    '</a>');

                $level->kuldes($a['email']);


                sqladat($a, 'bow_hirlevel_feliratkozas');


                return;

            }

        }

        include (htmlElem());

    }

}


?>