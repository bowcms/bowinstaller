<?php

// widget beállítások
$widgetPeldanyok = array(
        'nev' => 'Hírlevél feliratkozó',
        'fuggneny' => 'wgFeliratkozas',
        'leiras' => 'Hírlevl feliratkozó'
);

function wgFeliratkozasSet($a) {
    
    if ($a=='') {
        $a = array(
            'cim' => 'Cím',
            'leiras' => '',
            'sikerszoveg' => '',
            'cimke' => ''
        );
    }else {
        $a = unserialize($a);
    }
    $sql = "SELECT * FROM bow_cimkek ORDER BY cimke ASC";
    $rs = sqluniv4($sql);
    
    
    ?>
    <p>
        <label>Dobozcím:</label>
        <input name="a[cim]" value="<?= $a['cim'];?>"/>
    </p>
    <p>
        <label>Leírás:</label>
        <textarea name="a[leiras]"><?= $a['leiras'];?></textarea>
    </p>
    <p>
        <label>Sikeres feliratkozás üzenet:</label>
        <textarea name="a[sikerszoveg]"><?= $a['sikerszoveg'];?></textarea>
    </p>
    <p>
        <label>Cimke:</label>
        <select name="a[cimke]">
            <?php
            foreach ($rs as $sor):
            ?>
            <option value="<?= $sor['id']; ?>" <?= (($a['cimke']==$sor['id'])?' selected="selected" ':'');?> ><?= $sor['cimke']; ?></option>
            <?php
            endforeach;
            ?>
        </select>
    </p>
    
    <?php
}