<?php
/**
 * unicornOldalepito
 * 
 * oldalépítő adminisztrációs eszköz:
 * Oldalak létrehozása, szerkesztése, törlése
 * Oldalakhoz tartalmak hozzáfűzése
 */

$u = osztaly_unicorn_futtato::peldany();
$url = Url::peldany();
$html = Html::peldany();
$html->helyorzo('lapCim', __f('Weboldalak szerkesztése'));
$html->helyorzoHozzafuzes('adminUtvonal', '<a href="' . UNIC_URL . '" >' . __f('Oldalak listája') .
    '</a>');
// alapadat mentés
if (isset($_POST['uniForm_bow_oldalak'])) {
    $a = $_POST['uniForm_bow_oldalak'];
    sqladat($a, 'bow_oldalak');
    $id = $a['id'];
    if ($id == 0)
        $id = mysql_insert_id();
    $u->uzenetKiiras(__f('Mentés sikeres'), '');

    if (!isset($_POST['uniForm_bow_oldalak_ext']['korlatozas']))
        $_POST['uniForm_bow_oldalak_ext']['korlatozas'] = false;

    if (is_array($_POST['uniForm_bow_oldalak_ext']['korlatozas'])) {

        foreach ($_POST['uniForm_bow_oldalak_ext']['korlatozas'] as $k => $csoportId) {
            if ($csoportId == '') {
                unset($_POST['uniForm_bow_oldalak_ext']['korlatozas'][$k]);
                continue;
            }
            $sql = "SELECT id FROM bow_oldalxcsoport WHERE oldal_id = $id AND csoport_id = $csoportId";
            $rs = sqluniv3($sql);
            if (!isset($rs['id'])) {
                $a = array('oldal_id' => $id, 'csoport_id' => $csoportId);
                sqladat($a, 'bow_oldalxcsoport');

            }
        }
        if (!empty($_POST['uniForm_bow_oldalak_ext']['korlatozas'])) {
            $sql = "DELETE FROM bow_oldalxcsoport WHERE oldal_id = $id AND csoport_id NOT IN (" .
                implode(', ', $_POST['uniForm_bow_oldalak_ext']['korlatozas']) . ")";

        } else {
            $sql = "DELETE FROM bow_oldalxcsoport WHERE oldal_id = $id ";
        }
        sqluniv($sql);
    } else {
        $sql = "DELETE FROM bow_oldalxcsoport WHERE oldal_id = $id ";
        sqluniv($sql);
    }

}

if ($feladat == 'delete') {
    $ids = explode('_', $id);
    foreach ($ids as $id) {
        if (is_numeric($id)) {

            $sql = "DELETE FROM bow_oldalxmodul WHERE oldal_id = $id ";
            sqluniv($sql);
            $sql = "DELETE FROM bow_oldalxcsoport WHERE oldal_id = $id ";
            sqluniv($sql);
            $sql = "DELETE FROM bow_oldalxurl WHERE oldal_id = $id ";
            sqluniv($sql);
            $sql = "DELETE FROM bow_oldalak WHERE id = $id ";
            sqluniv($sql);


        }
    }
    $u->uzenetKiiras(__f('Törlés sikeres'), __f('A kijelölt elemeket eltávolítottam!'));
}

if ($feladat == 'edit') {

    $csoportok = valasztoLista('bow_csoportok', 'id', 'nev', 'nev');
    $oldalxcsoport = getColArray('csoport_id', 'bow_oldalxcsoport', ' oldal_id = ' .
        $id);


    $opciok = array(
        'kihagyott_mezok' => array(
            'lertehozva',
            'letrehozo',
            'egyedi_fej',
            'egyedi_lab',
            'tema'),
        'mezok' => array(
            'cim' => array('label' => __f('Oldal címe'), 'help' => __f('Adminisztrációs felületen ezen a néven találod meg')),
            'keywords' => array('label' => __f('SEO kulcsszavak'), 'help' => __f('Kulcsszavag a keresők számára')),
            'description' => array('label' => __f('SEO leírás'), 'help' => __f('Rövid leírás keresők számára')),
            'meta_felulir' => array(
                'label' => __f('Meta adatok működése'),
                'help' => __f('Hogyan működjenek a meta adatok'),
                'specialis' => 'select',
                'ertekek' => array('0' => __f('Hozzáillesztjük az alap meta tagokhoz'), '1' =>
                        __f('Felülírjuk az általános beállítást'))),
            'nyelv' => array(
                'label' => __f('Nyelv'),
                'help' => __f('Ennek az oldalnak a nyelve'),
                'specialis' => 'select',
                'ertekek' => valasztoLista('bow_nyelvek', 'kulcs', 'nev', 'nev')),
            'url' => array('label' => __f('Az oldal URL-je'), 'help' => __f('Csak ékezet nélküli ASCII karakterek használhatóak')),
            'specialis' => array(
                'label' => __f('Speciális működésű oldal'),
                'help' => __f('Itt lehet beállítani speciális rendeltetésű oldalakat'),
                'specialis' => 'select',
                'ertekek' => array(
                    'NULL_STRING' => __f('Nem speciális oldal'),
                    'fooldal' => __f('Főoldal'),
                    '404' => __f('404-es hibaoldal'),
                    'nincshozzaferes' => __f('Nincs hozzáférés hibaoldal')))),
        'extra_mezok' => array('korlatozas' => array(
                'label' => __f('Korlátozás'),
                'help' => __f('Oldal megjelenítésének szabályozása a felhasználói szintek alapján'),
                'specialis' => 'multiselect',
                'ertekek' => $csoportok,
                'beallitas' => $oldalxcsoport)));
    if ($u->adatSzerkeszto(__f('Oldal szerkesztése'), 'bow_oldalak', $id, $opciok))
        return;
    else {
        $u->uzenetKiiras(__f('Hiba'), __f('Az adatsor nem található!'), 'warning');
    }

}

if ($feladat == 'kibont') {
     $oldalId = (int)$url->paremeterOlvas(2);
    $sql = "SELECT cim FROM bow_oldalak WHERE id = ".$oldalId;
    $oldalRs = sqluniv3($sql);
    $cim = $oldalRs['cim'];

    $html->helyorzoHozzafuzes('adminUtvonal', '<a href="' . UNIC_URL . 'kibont/' . $id .
        '" >' . __f('Megjelenített tartalmak listája') . ': '.$cim.'</a>');
   
    $modulok = Memoria::olvas('modulBejegyzes');
    
    if (isset($_POST['rulez_beallitas'])) {
        $oxt = $_POST['oxt'];
        $sql = "DELETE FROM bow_modulxcsoport WHERE oldalxtartalom_id = ".(int)$oxt;
        sqluniv($sql);
        
        
        if (isset($_POST['csoportxtartalom'] ))
            if ($_POST['csoportxtartalom']!='')
        foreach ($_POST['csoportxtartalom'] as $csoportId) {
            $a = array('oldalxtartalom_id' => $oxt, 'csoport_id' => $csoportId);
            if ($csoportId!=0) sqlfelvitel($a,'bow_modulxcsoport');
        }
        $u->komp->popUpNoti(__f('Jogosultság mentés kész'),__f('A tartalom csoporthoz rendelése sikeres'));
    }
    
    
    if ($url->paremeterOlvas(3)=='sorrend') {
        $sorrend = $url->paremeterOlvas(4);
        $sorrend = explode('|', $sorrend);
        foreach($sorrend as $sorElem) {
            $sorElem = explode('_',$sorElem);
            if (!isset($sorElem[1])) continue;
            
            $sql = "UPDATE bow_oldalxtartalom SET sorrend = ".(int)$sorElem[1]." WHERE id = ".(int)$sorElem[0];
            //print $sql;
            sqluniv($sql);
        }
        $u->komp->popUpNoti(__f('Sorrend mentés'), __f('Sorrend mentése sikeres'));
    }
    
    if ($url->paremeterOlvas(3)=='rulez') {
        $tId = (int)$url->paremeterOlvas(4);
        $u->komp->adminFormFej(__f('Csoportok beállítása'), UNIC_URL.'kibont/'.$id);
        $u->komp->adminFormHidden('oxt',$tId );
        $sql = "SELECT x.csoport_id FROM bow_modulxcsoport x, bow_csoportok cs WHERE x.csoport_id = cs.id AND x.oldalxtartalom_id = $tId ORDER BY cs.nev ASC";
        $beallitottakRs = sqluniv4($sql);
        $beallitottak = array();
        foreach ($beallitottakRs as $sor) {
            $beallitottak[] = $sor['csoport_id'];
        }
        
        $sql = "SELECT id, nev FROM bow_csoportok ORDER BY rang ASC";
        $csoportok = sqluniv4($sql);
        $csoportLista = array();
        foreach ($csoportok as $csoport) {
            $csoportLista[$csoport['id']] = $csoport['nev'];
        }
        $u->komp->adminFormMultiSelect(__f('Válaszd ki, kik láthatják a tartalmakat, vagy ne válassz ki semmit, így mindenki láthatja'),'csoportxtartalom[]',$beallitottak,$csoportLista,'');
        $u->komp->adminFormHidden('rulez_beallitas',1);
        $u->komp->adminFormLab();
        
        $u->komp->popUpNoti(__f('Csoportok beállítása'), __f('Az itt beállítt csoportok fogják látni a tartalmat. Ha nincs senki beállítva, akkor mindenki láthatja.'));
        return;
    }
    
    
    if ($url->paremeterOlvas(3) == 'delete') {
        $mit = $url->paremeterOlvas(4);
        $mit = trim($mit,'_');
        
        if (strpos($mit, '_')!==false) {
            $ids = explode('_', $mit);
            
        } else {
            $ids = array((int)$mit);
        }
        if (!empty($ids)) {
            foreach($ids as $tid) {
                
                sqltorles('bow_oldalxtartalom',$tid);
            }
            $u->uzenetKiiras(__f('Törlés sikeres'),'');
        } else {
            $u->uzenetKiiras(__f('Törlés hiba'), '', 'warning');
        }
    }
    
    if ($url->paremeterOlvas(3) == 'edit') {
        $tartalomRendszerId = (int)$url->paremeterOlvas(4);
        
        $sql = "SELECT * FROM bow_oldalxtartalom WHERE id = " . $tartalomRendszerId;
        $tartalomAdat = sqluniv3($sql);
        $adat = $tartalomAdat['adat'];

        $fuggveny = $tartalomAdat['fuggveny'];
        
        $installAdat = modulKereso($fuggveny);
        
        if (function_exists($fuggveny . 'Beallito')) {


            $sikeresBeallitas = call_user_func($fuggveny . 'Beallito', $u, @unserialize($tartalomAdat['adat']));
            if (is_array($sikeresBeallitas)) {
                $a = array(
                    'adat' => serialize($sikeresBeallitas['adat']),
                    'cim' => $installAdat['funkcio'] . ' - ' . $sikeresBeallitas['cim']);
                if ($tartalomRendszerId == 0) {
                    sqlfelvitel($a, 'bow_oldalxtartalom');
                    $u->komp->popUpNoti(__f('Sikeres tartalomfelvitel'), __f('Új tartalom hozzáadásra került'));
                } else {
                    $a['id'] = $tartalomRendszerId;
                    sqlmodositas($a, 'bow_oldalxtartalom');
                    $u->komp->popUpNoti(__f('Sikeres tartalom módosítás'), __f('Tartalom módosítása sikerült'));
                }

            } else {
                return;
            }
        } else {
            $u->komp->popUpNoti(__f('Nincs beállítási lehetőség'), __f('Ennél a tartalomnál nincs beállítási lehetőség'));
        }

    }
    if ($url->paremeterOlvas(3) == 'tartalom') {

        $hozzaadasHiba = false;

        $mudulAdat = $url->paremeterOlvas(4);
        $tartalomRendszerId = (int)$url->paremeterOlvas(5);

        $modulAdat = explode(':', $mudulAdat);
        $modul = $modulAdat[0];
        if (isset($modulAdat[1])) {
            $tartalomId = modulBekapcsolva($modul);
            if ($tartalomId) {
                $fuggveny = $modulAdat[1];
                $installAdat = false;
                foreach ($modulok as $k => $sor) {
                    if ($sor['modul'] == $modul and $sor['fuggveny'] == $fuggveny) {
                        $installAdat = $sor;
                    }
                }
                if (is_array($installAdat)) {
                    if ($installAdat['dinamikusBeallito']) {
                        if (function_exists($fuggveny . 'Beallito')) {
                            if ($tartalomRendszerId != 0) {
                                $sql = "SELECT * FROM bow_oldalxtartalom WHERE id = " . $tartalomRendszerId;
                                $tartalomAdat = sqluniv3($sql);
                                $adat = $tartalomAdat['adat'];
                            } else {
                                $adat = array();
                            }

                            $sikeresBeallitas = call_user_func($fuggveny . 'Beallito', $u, @unserialize($tartalomAdat['adat']));
                            if (is_array($sikeresBeallitas)) {
                                $a = array(
                                    'oldal_id' => $id,
                                    'tartalom_id' => $tartalomId,
                                    'fuggveny' => $fuggveny,
                                    'adat' => serialize($sikeresBeallitas['adat']),
                                    'cim' => $installAdat['funkcio'] . ' - ' . $sikeresBeallitas['cim']);
                                if ($tartalomRendszerId == 0) {
                                    sqlfelvitel($a, 'bow_oldalxtartalom');
                                    $u->uzenetKiiras(__f('Sikeres tartalomfelvitel'), __f('Új tartalom hozzáadásra került'));
                                } else {
                                    $a['id'] = $tartalomRendszerId;
                                    sqlmodositas($a, 'bow_oldalxtartalom');
                                    $u->uzenetKiiras(__f('Sikeres tartalom módosítás'), __f('Tartalom módosítása sikerült'));
                                }

                            } else {
                                return;
                            }
                        } else {
                            $hozzaadasHiba = __f('Beállító függvény hiányzik') . ': ' . $fuggveny .
                                'Beallito';
                        }
                    } else {
                        // nincs beállítás, azonnal mentjük
                        $a = array(
                            'oldal_id' => $id,
                            'tartalom_id' => $tartalomId,
                            'fuggveny' => $fuggveny,
                            'cim' => $installAdat['funkcio']);
                        sqlfelvitel($a, 'bow_oldalxtartalom');
                        $u->uzenetKiiras(__f('Sikeres tartalomfelvitel'), __f('Új tartalom hozzáadásra került'));
                    }
                } else {
                    $hozzaadasHiba = __f('Modul futtató függvény nem található');
                }

            } else {
                $hozzaadasHiba = __f('Modul nincs telepítve') . ': ' . $modul;
            }
        } else {
            $hozzaadasHiba = __f('Hibás paramterek');
        }

        if ($hozzaadasHiba)
            $u->uzenetKiiras('Hiba', $hozzaadasHiba, 'warning');
    }

    $gombok = array();
    foreach ($modulok as $k => $sor):
        $gombok[] = array('cim' => $sor['funkcio'], 'link' => UNIC_URL . 'kibont/' . $id .
                '/tartalom/' . $sor['modul'] . ':' . $sor['fuggveny']);
    endforeach;
    $u->komp->legorduloGomb(__f('Tartalom hozzáadása az oldalhoz'), $gombok);
    print '<br /><br />';
    $tablaOpciok = array(
        'tablacim' => __f('Tartalmak'),
        'rendezes' => ' ORDER BY sorrend ASC ',
        'mezok' => array('id' => 'ID', 'cim' => __f('Tartalom neve')),
        'plusszmezok' => array(__f('Sorrend') => 'sorrendInput'),
        'vezerlok' => array(
            'edit' => __f('Szerkesztés'),
            'rulez' => __f('Korlátozások'),
            'delete' => __f('Törlés')),
        'filter' => 'cim',
        'where' => ' oldal_id = '.$oldalId,
        'plusszgomb' => array('sorrendMentesGomb'));
    define('UNIC_SUBURI', UNIC_URL . 'kibont/' . $id . '/');
    $u->szerkesztoTabla('bow_oldalxtartalom', $tablaOpciok);


    return;
}

$tablaOpciok = array(
    'tablacim' => __f('Létrehozott oldalak listája'),
    'mezok' => array(
        'id' => 'ID',
        'cim' => __f('Oldal neve'),
        'keywords' => __f('Kulcsszavak'),
        'url' => 'Url'),
    'callback' => array('url' => 'oldalSzerkesztoUrlLink'),
    'vezerlok' => array(
        'kibont' => 'Oldalépítő',
        'edit' => 'Alapadatok',
        'delete' => 'Törlés'),
    'filter' => 'url,keywords,description',
    'ujElemGomb' => __f('Új oldal hozzáadása'));

$u->szerkesztoTabla('bow_oldalak', $tablaOpciok);


function oldalSzerkesztoUrlLink($url)
{
    $link = BASE_URL . $url;
    return '<a href="' . $link . '" target="_blank">' . $url . '</a>';
}
function sorrendInput($id) {
    $sql = "SELECT sorrend FROM bow_oldalxtartalom WHERE id = ".(int)$id;
    $sorrend = sqluniv3($sql);
    
    ?>
    <input style="width: 60px;margin: auto;text-align: center;" name="sorrend[<?= $id?>]" rel="<?= $id?>" class="sorrendInp" value="<?= $sorrend['sorrend']; ?>" />
    <?php
}

function sorrendMentesGomb() {
    $ajaxHivas = str_replace(BASE_URL,'',UNIC_SUBURI);
    $ajaxHivas = BASE_URL.'ajax/'.$ajaxHivas.'sorrend/';
    ?>
    <button class="btn-success" onclick="$('#tartalomDiv').load('<?= $ajaxHivas; ?>'+kotegeltSorrendMentes(this));this.disabled=true;"><?= __f('Sorrend mentése');?></button>
    <?php    
}

