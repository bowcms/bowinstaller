<?php


class _oldalak
{

    public static $felvitelElkeszult = false;
    public function oldalEpito($id, $feladat = '', $oldalxmodulSorId = 0)
    {
        $tag = Tagok::peldany();
        $html = Html::peldany();
        define('EPITO_URL', ADMIN_URL . '_oldalak/lista/kibont/' . $id . '/');

        if ($feladat == 'torol') {

            // oldalhoz kapcsolt modul bejegyzés törlése a bow_oldalxmodul táblából

            $sql = "DELETE FROM  bow_oldalxmodul WHERE id = " . $oldalxmodulSorId;
            sqluniv($sql);
            $html->uzenetKiiratas('A tartalmat eltávolítottuk az oldalról.');
        }
        if ($feladat == 'csoportok') {
            $html->helyorzo('lapCim', '<a href="' . ADMIN_URL .
                '_oldalak/lista">Oldalak kezelése</a> | <a href="' . EPITO_URL .
                '">Oldalépítő</a> | Tartalmak megtekintésének korlátozása');

            $sql = "SELECT * FROM bow_csoportok WHERE rang <= (SELECT rang FROM bow_csoportok WHERE id = " .
                $tag->adat['csoport_id'] . " ) ORDER BY rang ASC";
            $csoportok = sqluniv4($sql);
            $this->komp->modulCsoportBeallito($csoportok, $oldalxmodulSorId);
            return;
        }
        if (isset($_POST['oldalxmodulSorId'])) {
            $sql = "DELETE FROM bow_modulxcsoport WHERE oldalxmodul_id = " . (int)$_POST['oldalxmodulSorId'];
            sqluniv($sql);
            if (!empty($_POST['csxmo']))
                foreach ($_POST['csxmo'] as $csoportId) {
                    $sql = "INSERT INTO bow_modulxcsoport SET csoport_id = $csoportId , oldalxmodul_id = " . (int)
                        $_POST['oldalxmodulSorId'];
                    sqluniv($sql);
                }
            $html->uzenetKiiratas('Jogosultságok beállítása sikeres.');
        }
        if (isset($_POST['sorrend'])) {
            foreach ($_POST['sorrend'] as $oldalxmodulId => $ertek)
                ;
            $sql = "UPDATE bow_oldalxmodul SET sorrend = $ertek WHERE id = " . $oldalxmodulId;

            sqluniv($sql);
            $html->uzenetKiiratas('Tartalmak sorrendje megváltozott.');

        }


        // a szerkesztett oldal adatai
        $sql = "SELECT * FROM bow_oldalak WHERE id = $id LIMIT 1";
        $oRs = sqluniv3($sql);
        $html->helyorzo('lapCim', '<a href="' . ADMIN_URL .
            '_oldalak/lista">Oldalak kezelése</a> | Oldalépítő - ' . $oRs['cim']);

        // modul hozzadása
        if (isset($_POST['hozzaad']))
            if ($_POST['hozzaad'] == '')
                unset($_POST['hozzaad']);
        if (isset($_POST['hozzaad']) or $feladat == 'beallit') {
            // ha van oxm, akkor szerkesztés, betöltjük az oldalhoz kapcsolt modul dinamikus beállításait

            if ($oldalxmodulSorId > 0) {
                $oldalxmodulId = $oldalxmodulSorId;
                $sql = "SELECT * FROM bow_oldalxmodul WHERE id = $oldalxmodulId LIMIT 1";
                $modulAdat = sqluniv3($sql);
                $_POST['hozzaad'] = $modulAdat['modul_id'];
            } else {
                $oldalxmodulId = 0;
                $modulAdat = array();
            }

            // a kiválasztott modul adatai

            $sql = "SELECT * FROM bow_modulok WHERE id = " . (int)$_POST['hozzaad'];
            $modulRs = sqluniv3($sql);

            // nézzük, van-e beállító
            $beallito = $modulRs['dinamikus_beallito'];
            $beallitoUt = BASE . 'modulok/' . $modulRs['modul'] . '/' . $modulRs['dinamikus_beallito'];

            if (is_file($beallitoUt)) {

                $sikeresBeallitas = $this->komp->beallitoKeret($beallitoUt, EPITO_URL, $modulRs,
                    $modulAdat, $id);
            } else {
                self::parameterFelvitel($id, (int)$_POST['hozzaad'], $modulRs['modulnev'] .
                    ' példány', '', $oldalxmodulId);
                $sikeresBeallitas = true;
                $html->uzenetKiiratas('Sikeresen hozzáadtad a szöveget az oldalhoz!');
                // nincs beállító, tehát akkor paraméterek nélkül visszük fel

            }

            if ($sikeresBeallitas == false)
                return;
        }

        $sql = "SELECT * FROM bow_oldalxmodul WHERE oldal_id = $id ORDER BY sorrend ASC";
        $rs = sqluniv4($sql);
        $sql = "SELECT * FROM bow_modulok WHERE rendszermodul = 0 ORDER BY modulnev ASC";
        $modulokRs = sqluniv4($sql);
        $this->komp->oldalEpito($rs, $modulokRs);
    }

    public function lista($feladat = '', $id = '', $alfeladat = '', $oldalxmodulSorId =
        0)
    {
        if ($feladat == 'kibont') {
            $this->oldalEpito($id, $alfeladat, $oldalxmodulSorId);
            return;
        }

        $id = (int)$id;
        $html = Html::peldany();
        $tag = Tagok::peldany();


        if ($feladat == 'torol') {
            $sql = "DELETE FROM bow_oldalxcsoport WHERE oldal_id = $id";
            sqluniv($sql);
            $sql = "DELETE FROM bow_oldalxmodul WHERE oldal_id = $id";
            sqluniv($sql);
            $sql = "DELETE FROM bow_oldalak WHERE id = $id";
            sqluniv($sql);
            $html->uzenetKiiratas('A törlés sikeres, kapcsolódó adatok törölve.');
        }

        if (isset($_POST['o'])) {

            $o = $_POST['o'];

            if ($o['cim'] == '') {
                $html->hibaKiiratas('A cím megadása kötelező;');
                $feladat = 'szerkeszt';
                $id = $o['id'];

            } else {
                $url = $_POST['url'];
                if (trim($url) == '') {
                    // képezzünk url-t
                    $cim = strtolower($o['cim']);

                    $csere = explode(' ', 'Í É Á Ű Ő Ú Ö Ü Ó í á ű ő ú ü ó ö é');
                    $mire = explode(' ', 'i e a u o u o u o i a u o u u o o e');
                    $cim = str_replace($csere, $mire, $cim);
                    $cim = str_replace(' ', '-', $cim);

                    $cim = preg_replace('/[^(\x20-\x7F)]*/', '', $cim);
                    $cim = preg_replace("/[^A-Za-z0-9 -]/", "", $cim);
                } else {
                    $cim = $url;
                }
                $ujra = true;
                $cim0 = $cim;
                $i = 1;

                while ($ujra == true) {
                    $sql = "SELECT id FROM bow_oldalak WHERE url LIKE '$cim' AND id != " . (int)$o['id'] .
                        " LIMIT 1";
                    $cRs = sqluniv3($sql);
                    if (!isset($cRs['id'])) {
                        $ujra = false;
                    } else {
                        $cim = $cim0 . '_' . $i;
                        $i++;
                    }
                }

                $o['url'] = $cim;


                $o['lertehozva'] = time();
                $o['letrehozo'] = $tag->id;

                if ($o['id'] == 0) {
                    // új oldal
                    unset($o['id']);
                    sqlfelvitel($o, 'bow_oldalak');
                    $id = mysql_insert_id();
                } else {
                    sqlmodositas($o, 'bow_oldalak');
                    $id = $o['id'];
                }

                // jogosultságok beállítása
                $sql = "DELETE FROM bow_oldalxcsoport WHERE oldal_id = $id";
                sqluniv($sql);
                if (!empty($_POST['cs'])) {
                    foreach ($_POST['cs'] as $csoport_id) {
                        $cs = array('oldal_id' => $id, 'csoport_id' => $csoport_id);
                        sqlfelvitel($cs, 'bow_oldalxcsoport');
                    }
                }


                $html->uzenetKiiratas('A mentés sikeres, a módosításokat rögzítettem!');
            }


        }

        if ($feladat == 'szerkeszt') {


            if ($id != '') {
                $sql = "SELECT * FROM bow_oldalak WHERE id = $id LIMIT 1";
                $oldal = sqluniv3($sql);
            } else {
                $oldal = array(
                    'id' => 0,
                    'cim' => '',
                    'description' => '',
                    'keywords' => '',
                    'meta_felulir' => 0,
                    'nyelv' => 'hu',
                    'tema' => '',
                    'specialis' => '',
                    'url' => '',
                    'nyelv' => 'hu',
                    'egyedi_fej' => '',
                    'egyedi_lab' => '');

            }

            if (isset($o['id'])) {
                $oldal = $o;
            }

            $this->komp->oldalSzerkeszto($oldal);
            return;
        }

        $html = Html::peldany();
        $html->helyorzo('lapCim', 'Oldalak kezelése');

        $sql = "SELECT * FROM bow_oldalak ORDER BY cim ASC";
        $rs = sqluniv4($sql);
        $this->komp->lista($rs);
    }

    public static function parameterFelvitel($oldalId, $modulId, $cim, $adat, $oldalxmodulId =
        0)
    {
        if ($oldalxmodulId == 0) {
            $sql = "SELECT MAX(sorrend) as max FROM bow_oldalxmodul WHERE oldal_id = $oldalId";
            $sorRs = sqluniv3($sql);
            if (isset($sorRs['max'])) {
                $sorrend = $sorRs['max'] + 10;
            } else {
                $sorrend = 0;
            }

        }
        $adat = addslashes($adat);
        if ($oldalxmodulId > 0) {
            $sql = "UPDATE bow_oldalxmodul SET adat = '$adat', nev = '$cim' WHERE id = $oldalxmodulId";
            sqluniv($sql);
        } else {
            $sql = "INSERT INTO bow_oldalxmodul SET sorrend = $sorrend, oldal_id = $oldalId,nev = '$cim' , modul_id = $modulId, adat = '$adat' ";
            sqluniv($sql);
        }
    }

}
