<?php

/**
 * _oldalakKomp.php
 * 
 * oldalak tartalmainak kezelése
 * 
 */
 
class _oldalakKomp {
    
    public function oldalEpito($rs, $modRs) {
        ?>
        <div class="infoDiv">
            Itt pakolhatsz oldaladra különböző tartalmakat, illetve beállíthatod azokat.
        </div>
        <br />
        <form method="post" action="<?= EPITO_URL; ?>" class="jNice">
        
            <select name="hozzaad">
                <option value="">Tartalom hozzáadása az oldalhoz</option>
                <?php
                foreach ($modRs as $modul):
                ?>
                <option value="<?= $modul['id']?>"><?= $modul['funkcio']?></option>
                <?php
                endforeach;
                ?>
            </select>
        
            <button type="submit" style="float: right;">Hozzáadom</button>
        
        </form>
        <br style="clear: both;"/>
        <?php
        if (empty($rs)) {
            ?>
            <h3>Ez az oldal még üres, addj hozzá tartalmakat a fenti legördülővel!</h3>
            
            <?php
            return;
        }
        ?>
        <h3>Az oldal tartalma:</h3>
        <table cellpadding="0" cellspacing="0">
        <?php
        foreach ($rs as $sor):
        ?>
        <tr>
            <td><?= $sor['nev']?></td>
            <td><form id="f<?= $sor['id']?>" style="display: inline-block;" action="<?= EPITO_URL; ?>" method="post">
                <input style="border: none; background: none;color: geen" onfocus="this.style.background='black';this.style.color='white';" onblur="this.style.background='none';this.style.color='green';" name="sorrend[<?= $sor['id']?>]" value="<?= $sor['sorrend']; ?>" onchange="$('#f<?= $sor['id']?>').submit();" />
            </form></td>
            
            <td class="action">
                <?php
                $sql = "SELECT dinamikus_beallito FROM bow_modulok WHERE id = ".$sor['modul_id'];
                $beallitoRs = sqluniv3($sql);
                if ($beallitoRs['dinamikus_beallito']!=''):
                ?>
                <a class="edit" href="<?= EPITO_URL?>beallit/<?= $sor['id']?>">Szerkesztés</a>
                <?php
                endif;
                ?>
                <br />
                <a class="view" href="<?= EPITO_URL; ?>csoportok/<?= $sor['id']; ?>" >
                    Korlátozások
                </a>
                <br />
                <a class="delete" href="<?= EPITO_URL; ?>torol/<?= $sor['id']; ?>" onclick="if(confirm('Biztos benne?')==false) return false;">
                    Törlés
                </a>
            </td>
        </tr>
        
        <?php
        endforeach;
        ?>
        </table>
        <br /><br />
        <?php
    }
    
    public function beallitoKeret($utvonal,$visszaUrl, $modul, $adat, $oldalId) {
        $modulId = $_POST['hozzaad'];
        if (isset($adat['id'])) {
            $oldalxmodulId = (int)$adat['id'];
        } else {
            $oldalxmodulId = 0;
        }
        $sikeresBeallitas = false;
        ob_start();
        include($utvonal);
        $beallitasok = ob_get_contents();
        ob_end_clean();
        
        if ($sikeresBeallitas == true) return true;
        
        ?>
        
            <h3><?= $modul['modulnev']?> bellítása</h3>
            <fieldset><?= $beallitasok; ?> </fieldset>
            <div>
                <form action="<?= EPITO_URL ?>" method="post" class="jNice">
                    <input type="submit" value="Vissza"/>
                </form>
            </div>
            
       
        <?php
        return false;
    }
    
    public function oldalSzerkeszto($o) {
        $id = $o['id'];
        ?>
        <h3>Oldal alapadatainak szerkesztése</h3>
        <div class="infoDiv">Minden oldalhoz tartozik néhány tulajdonság, melyeket itt beállíthatsz. Ilyenek a meta tag-ek, amelyeket a keresők számára llíthatunk be, illetve az oldalhoz tartozó téma, nyelv.</div>
        <form class="jNice" method="post" action="<?= ADMIN_URL; ?>_oldalak/lista">
            <fieldset>
                <input type="hidden" name="o[id]" value="<?= $id ?>" />
                <p>
                    <label>Oldal címe:</label>
                    <input name="o[cim]" value="<?= $o['cim']; ?>" />
                </p>
                <?php
                $url = $o['url'];
                ?>
                <p>
                    <label>Az oldal link:</label>
                    <i>
                        Az oldal link határozza meg, hogy milyen az oldal elérési útvonala. Pl. ha itt megadjuk, hogy "valami", akkor
                        ezt az oldalt a "<?= BASE_URL; ?>valami" linken lehet majd elérni.
                    </i>
                    <br />
                    <input name="url" value="<?= $url; ?>" />
                </p>
                <p>
                    <label>Kulcsszavak:</label>
                    <textarea name="o[keywords]"><?= $o['keywords']; ?></textarea>
                </p>
                <p>
                    <label>Az oldal rövid tartalma:</label>
                    <textarea name="o[description]"><?= $o['description']; ?></textarea>
                </p>
                <p>
                    <label>Felülírjuk ezekkel az értékekkel a meta tagokat, vagy hozzáfűzzük?</label>
                    <select name="o[meta_felulir]">
                        <option value="0" <?= $o['meta_felulir']==0?' selected="selected" ':''?> >Nem írjuk felül, csak hozzáfűzzük</option>
                        <option value="1" <?= $o['meta_felulir']==1?' selected="selected" ':''?> >Felülírjuk</option>
                    
                    </select>
                </p>
                <p>
                    <label>Egy-egy oldalhoz egyedi kinézet választható. Hagyd üresen, ha az alapértelmezett megjelenést szeretnéd.</label>
                    <select name="o[tema]">
                        <option value="" <?= $o['tema']==''?' selected="selected" ':''?> >Alapértelmezett</option>
                        <?php
                        
                        if ($handle = opendir(BASE.'temak')) {
                                $blacklist = array('.', '..');
                                while (false !== ($file = readdir($handle))) {
                                if (!in_array($file, $blacklist)) {
                                    ?>
                        <option value="<?= $file; ?>" <?= $o['tema']==$file?' selected="selected" ':''?> ><?= $file; ?></option>
                                    <?php
                                }
                            }
                            closedir($handle);
                        }
                        ?>
                    </select>
                </p>
                
                <p>
                    <label>Oldalaid megtekintését korlátozhatod felhasználókra. Ha nem jelölsz ki semmit, akkor bárki megtekintheti a tartalmat.</label>
                    
                    
                    <?php
                    $tag = Tagok::peldany();
                    $tagCsoport = $tag->adat['csoport_id'];
                    
                    
                    $sql = "SELECT * FROM bow_csoportok WHERE rang <= (SELECT rang FROM bow_csoportok WHERE id = $tagCsoport ) ORDER BY rang ASC";
                    $csoportok = sqluniv4($sql);
                    
                    foreach ($csoportok as $csoport):
                        $sql = "SELECT * FROM bow_oldalxcsoport WHERE oldal_id = ".(int)$o['id']." AND csoport_id = ".$csoport['id'];
                        $vanCsoport = sqluniv3($sql);
                    ?>
                        <input value="<?= $csoport['id']?>" type="checkbox" name="cs[]" <?= isset($vanCsoport['id'])?' checked="checked" ':''; ?> /> - <?= $csoport['nev']; ?><br style="clear: both;" />
                    <?php
                    endforeach;
                    ?>
                </p>
                <p>
                    <label>Nyelv</label>
                    <select name="o[nyelv]">
                        <?php
                        $sql = "SELECT * FROM bow_nyelvek ORDER BY nev ASC";
                        $nyRs = sqluniv4($sql);
                        foreach($nyRs as $sor):
                        ?>
                        <option value="<?= $sor['kulcs']?>" <?= $o['nyelv']==$sor['kulcs']?' selected="selected" ':''?> ><?= $sor['nev']?></option>
                        <?php
                        endforeach;
                        ?>
                    </select>
                </p>
                <p>
                    <label>Speciális célú oldal, főoldal, hibaoldalak itt állíthatóak be</label>
                    <select name="o[specialis]">
                        <option value="" <?= $o['specialis']==''?' selected="selected" ':''?> ></option>
                        <option value="fooldal" <?= $o['specialis']=='fooldal'?' selected="selected" ':''?> >Ez a főoldal</option>
                        <option value="nincshozzaferes" <?= $o['specialis']=='nincshozzaferes'?' selected="selected" ':''?> >Nincs hozzáférés hibaoldal</option>
                        <option value="404" <?= $o['specialis']=='404'?' selected="selected" ':''?> >Oldal nem található hibaoldal</option>
                    
                    </select>
                </p>
                
                <p>
                    <label>Egyedi fej php file:</label>
                    <i>
                       Adott oldalhoz egyedi file is választható az adott template-on belül, ezt itt adhatod meg.
                    </i>
                    <br />
                    <input name="o[egyedi_fej]" value="<?= $o['egyedi_fej']; ?>" />
                </p>
                
                <p>
                    <label>Egyedi láb php file:</label>
                    <i>
                       Adott oldalhoz egyedi file is választható az adott template-on belül, ezt itt adhatod meg.
                    </i>
                    <br />
                    <input name="o[egyedi_lab]" value="<?= $o['egyedi_lab']; ?>" />
                </p>
                <input type="submit" value="Kész" />
                &nbsp;&nbsp;&nbsp;
                <button type="button" onclick="location.href='<?= ADMIN_URL; ?>_oldalak/lista'" style="float: right;" >Vissza</button>
                
            </fieldset>
        </form>
        <?php
    }
    
    public function lista($lista) {
        if (!empty($lista)):
        ?>
        <div class="infoDiv">
            Itt találod a rendszerben megjelenített lapokat/oldalakat. 
            A szerkesztés gombra kattintva beállíthatod az oldalhoz tartozó meta tagokat, kinézetet, jogosultságot,
            míg az Oldalépítő gombbal a tényleges tartalmakat, szövegeket, képeket, különböző modulokat adhatod hozzá.
        </div>
        
        <form style="text-align: right;" class="jNice" action="<?= ADMIN_URL?>_oldalak/lista/szerkeszt/0">
            <br />
            <input type="submit" value="Új oldal felvitele" style="float: right;"/>
            <br style="clear: both;"/><br />
        </form>
        <table cellpadding="0" cellspacing="0">
        <?php
        foreach ($lista as $sor):
        ?>
        <tr>
            <td><strong><?= $sor['cim']?></strong></td>
            <td><a style="color: green;text-decoration: none;" href="<?= BASE_URL.$sor['url']?>" target="_blank"><?= $sor['url']?></a></td>
            <td><?= $sor['description']?></td>
            <td class="action">
                
                
                
                <a class="view" href="<?= ADMIN_URL; ?>_oldalak/lista/kibont/<?= $sor['id']; ?>" >
                    Oldalépítő
                </a>
                <br />
                <a class="edit" href="<?= ADMIN_URL; ?>_oldalak/lista/szerkeszt/<?= $sor['id']; ?>" >
                    Alapadatok
                </a>
                <br />
                <a class="delete" href="<?= ADMIN_URL; ?>_oldalak/lista/torol/<?= $sor['id']; ?>" onclick="if(confirm('Biztos benne?')==false) return false;">
                    Törlés
                </a>
            </td>
        </tr>
        <?php
        endforeach;
        
        ?>
        </table>
        <?php
        endif;
    }
    
    public function modulCsoportBeallito($csoportok, $oldalxmodulSorId) {
        ?>
        <form class="jNice" method="post" action="<?= EPITO_URL?>">
            <br />
            <div class="infoDiv">
                Egyes modulokat elrejthetsz bizonyos felhasználói csoportoktól. Ennek segítségével például akár egész más felépítést adhatsz 
                    a regisztrált felhasználóknak a látogatókhoz képest.<br /><br />
                Ezen beállítások használata <strong>nem kötelező</strong>, Ha nem jelölsz be semmit, akkor a tartalmakat bárki megtekintheti.
            </div>
            <p>
                    
                    <br /><br />
                    
                    <?php
                    
                    foreach ($csoportok as $csoport):
                        $sql = "SELECT * FROM bow_modulxcsoport WHERE oldalxmodul_id = ".$oldalxmodulSorId." AND csoport_id = ".$csoport['id'];;
                        $vanCsoport = sqluniv3($sql);
                    ?>
                        <input value="<?= $csoport['id']?>" type="checkbox" name="csxmo[]" <?= isset($vanCsoport['id'])?' checked="checked" ':''; ?> /> - <?= $csoport['nev']; ?><br style="clear: both;" />
                    <?php
                    endforeach;
                    ?>
                </p>
                <p>
                    <label> </label>
                    <input name="oldalxmodulSorId" value="<?= $oldalxmodulSorId; ?>" type="hidden" />
                    <input type="submit" value="Kész" />
                </p>
                 <br /><br /><br /><br />
        </form>
        <?php
    }
}
