<?php

/**
 * osztaly_html.php
 * 
 * a témák kezelését lehetővé tevő osztály
 * 
 * @author Cworrek
 * @copyright 2013
 */
 
/**
 * Téma változók és függvények
 * 
 * a témán velül változó elhelyezése:
 * {* Változónév *}
 * 
 * Stílus függvény elhelyése ( a témakönyvtárban található temaFuggvenyek.php-ból hívjuk)
 * 
 * {* függvénynév ( param1, param2, ...) *}
 * 
 * bow_modulokban regisztrált osztály-metódus hívás
 * 
 * {* Modulosztály.Metodus (param1, param2, ...) *}
 * 
 */


class Html
{

    public static $peldany;
    public $stilus;
    public $title;
    public $header;
    public $bodyVege = '';
    public $fejHTML;
    public $tartalomHTML;
    public $labHTML;

    public $kimenetHTML;
    public $ajaxKeretBetoltve = false;

    /*
    * html helyőrzők: változók és függvényhívások
    */

    public $stilusfuggvenyek = array();
    public $stilusvaltozok = array();

    public $jqBehivva = 0;

    public function __construct()
    {

        self::$peldany = $this;
        $this->stilusBetoltes();

    }

    public function stilusBetoltes()
    {
        $url = Url::peldany();
        // ha admin stílus, akkor nincs template
        if ($url->ajax)
            return;

        $stilus = $url->stilus;
        $this->stilus = BASE . 'temak/' . $stilus . '/';
        define('STILUS_URL', BASE_URL.'temak/' . $stilus . '/');
        $fej = 'fej.php';
        $lab = 'lab.php';

        if ($url->egyedi_fej != '')
            $fej = $url->egyedi_fej;
        if ($url->egyedi_lab != '')
            $lab = $url->egyedi_lab;

        $lapBetoltve = false;
        if (is_file($this->stilus . 'lap.php')) {
            // egybe lapot töltünk
            $lap = @file_get_contents($this->stilus . 'lap.php');
            $lap = explode('[TARTALOM]', $lap);
            if (isset($lap[1])) {
                $this->fejHTML = $lap[0];
                $this->labHTML = $lap[1];
                $lapBetoltve = true;
            }
        }
        if (!$lapBetoltve) {
            $this->fejHTML = @file_get_contents($this->stilus . $fej);
            if ($this->fejHTML == '')
                die('Fej téma nem elérhető: ' . $this->stilus . $fej);

            $this->labHTML = @file_get_contents($this->stilus . $lab);
            if ($this->labHTML == '')
                die('Láb téma nem elérhető: ' . $this->stilus . $lab);

        }

        if (file_exists($this->stilus . 'temaFuggvenyek.php')) {
            include_once ($this->stilus . 'temaFuggvenyek.php');
        }


        $this->helyorzoFeldolgozo($this->fejHTML);
        $this->helyorzoFeldolgozo($this->labHTML);

        $this->stilusvaltozok['meta_title'] = BOW_META_TITLE;
        $this->stilusvaltozok['meta_keywords'] = BOW_META_KEYWORDS;
        $this->stilusvaltozok['meta_description'] = BOW_META_DESC;


        $this->helyorzo('stilusutvonal', BASE_URL . 'temak/' . $stilus . '/');


    }
    public function kimenetKeszites()
    {
        Beepulok::futtat('HTML kimenet készítés');
        $this->tartalomOsszefuzes();
        $this->helyettesites();
        $this->kimenet();
    }

    public static function peldany()
    {
        if (self::$peldany == null) {
            self::$peldany = new Html();
        }
        return self::$peldany;
    }

    public function helyorzoFeldolgozo($html)
    {
        $minta = '%{\*(.*)\*}%';

        preg_match_all($minta, $html, $talalat);

        foreach ($talalat[1] as $orzo) {
            // függvényhívás
            if (strpos($orzo, '(') !== false) {

                // függvény(param1,param2,param3)

                preg_match('#(.*)\((.*)\)#', $orzo, $fgm);

                if ($fgm) {

                    $this->stilusfuggvenyek[] = array(
                        'fuggveny' => trim($fgm[1]),
                        'param' => explode(',', trim($fgm[2])),
                        'helyorzo' => trim($fgm[0]));
                    
                }


            } else {
                // változó helyőrző

                $this->stilusvaltozok[trim($orzo)] = '';
            }
        }


        return $html;
    }

    public function helyorzo($helyorzoNev, $ertek)
    {
        $this->stilusvaltozok[$helyorzoNev] = $ertek;
    }
    public function helyorzoFelvetelStart()
    {
        ob_start();
    }

    public function helyorzoFelvetelStop($helyorzoNev)
    {
        if (!isset($this->stilusvaltozok[$helyorzoNev]))
            $this->stilusvaltozok[$helyorzoNev] = '';
        $this->stilusvaltozok[$helyorzoNev] .= ob_get_contents();
        ob_end_clean();
    }
    public function helyorzoFelvetelStopFelulir($helyorzoNev)
    {
        if (!isset($this->stilusvaltozok[$helyorzoNev]))
            $this->stilusvaltozok[$helyorzoNev] = '';
        $this->stilusvaltozok[$helyorzoNev] = ob_get_contents();
        ob_end_clean();
    }

    public function helyorzoHozzafuzes($helyorzoNev, $ertek, $space = ' ')
    {
        if (!isset($this->stilusvaltozok[$helyorzoNev]))
            $this->stilusvaltozok[$helyorzoNev] = '';
        $this->stilusvaltozok[$helyorzoNev] .= $space . $ertek;
    }
    public function forditottHelyorzoHozzafuzes($helyorzoNev, $ertek, $space = ' ')
    {
        if (!isset($this->stilusvaltozok[$helyorzoNev]))
            $this->stilusvaltozok[$helyorzoNev] = '';
        $this->stilusvaltozok[$helyorzoNev] = $ertek . $space . $this->stilusvaltozok[$helyorzoNev];
    }


    public function tartalomFelvetelStart()
    {
        ob_start();
    }
    public function tartalomFelvetelStop()
    {
        global $tartalom; // hogy a beépülők elrhessék
        $tartalom = ob_get_contents();
        ob_end_clean();

        Beepulok::futtat('Tartalom hozzáfűzés');

        $this->tartalomHTML .= $tartalom;
    }

    public function tartalomOsszefuzes()
    {
        $this->kimenetHTML = $this->fejHTML . $this->tartalomHTML . $this->labHTML;
    }


    public function helyettesites()
    {
        $tag = Tagok::peldany();
        foreach ($this->stilusvaltozok as $valtozo => $ertek) {
            //$this->kimenetHTML = str_replace($valtozo, $ertek, $this->kimenetHTML);
            $this->kimenetHTML = preg_replace('%{\*( *)' . $valtozo . '( *)\*}%', $ertek, $this->
                kimenetHTML);


        }
        if ($this->stilusfuggvenyek)
            foreach ($this->stilusfuggvenyek as $valtozo => $ertek) {

                if (function_exists($ertek['fuggveny'])) {
                    $fgv = $ertek['fuggveny'];
                    ob_start();
                    call_user_func_array($fgv, $ertek['param']);

                    $fgvOutput = ob_get_contents();
                    ob_end_clean();
                    $this->kimenetHTML = str_replace($ertek['helyorzo'], $fgvOutput, $this->
                        kimenetHTML);
                } else {
                    // osztály/metódis hívás is lehet
                    $m = explode('.', $ertek['fuggveny']);
                    if (isset($m[1])) {
                        $osztaly = trim($m[0]);
                        $metodus = trim($m[1]);
                        $param = $ertek['param'];

                        // léteuzik-e és van-e jogunk?
                        $sql = "SELECT * FROM bow_modulok WHERE osztaly = '$osztaly' AND metodus = '$metodus' AND (csoport = 0 OR csoport = " .
                            $tag->adat['csoport_id'] . ")";

                        $rs = sqluniv3($sql);
                        if (isset($rs['id'])) {
                            ob_start();
                            Bow::statikusModulFuttato($rs['modul'], $osztaly, $metodus, $param, 0);
                            $fgvOutput = ob_get_contents();
                            ob_end_clean();
                            $this->kimenetHTML = str_replace($ertek['helyorzo'], $fgvOutput, $this->
                                kimenetHTML);
                        }
                    }
                    $this->kimenetHTML = str_replace($ertek['helyorzo'], '', $this->kimenetHTML);
                }


            }
        $this->kimenetHTML = str_replace('{*', '', $this->kimenetHTML);
        $this->kimenetHTML = str_replace('*}', '', $this->kimenetHTML);
        $url = Url::peldany();
        if (!$url->ajax) {
            $out = $this->kimenetHTML;
            $pos = strpos($out, '</head>');
        
            $out = substr($out, 0, $pos) . $this->header . substr($out, $pos);
        
            $pos = strpos($out, '</body>');

            $this->kimenetHTML = substr($out, 0, $pos) . $this->bodyVege . substr($out, $pos);

        }
        
    }

    public function hibaKiiratas($str, $class = '')
    {
        $this->tartalomFelvetelStart();
        echo '<br style="clear:both"/><div class="hibaDiv ' . $class . '">' . $str .
            '</div>';
        $this->tartalomFelvetelStop();
    }
    public function uzenetKiiratas($str, $class = '')
    {
        $this->tartalomFelvetelStart();
        echo '<div class="alert alert-success ' . $class . '">' . $str .
            '</div>';
        $this->tartalomFelvetelStop();
    }

    public function headerStart()
    {
        ob_start();
    }
    public function headerStop()
    {
        $this->header .= ob_get_contents();
        ob_end_clean();
    }
    public function bodyVegeStart()
    {
        ob_start();
    }
    public function bodyVegeStop()
    {
        $this->bodyVege .= ob_get_contents();
        ob_end_clean();
    }
    public function ajaxKeret()
    {
        if ($this->ajaxKeretBetoltve == true)
            return;
        $this->ajaxKeretBetoltve = true;
        $this->headerStart();
        $tag = Tagok::peldany();
?>
        <script type="text/javascript">
        // <!--
        function ajaxHivas(modul, file, param, cel ) {
            $.post('<?= BASE_URL; ?>ajaxkeret.php', 'ajax_token=<?= $_SESSION['ajax_token']; ?>&tagid=<?= $tag->
id; ?>&modul='+modul+'&file='+file+'&'+param, function(e){
                $(cel).html(e);
            })
        }
        // -->
        </script>
        <?php
        $this->headerStop();
    }
    public function kimenet()
    {
        //print htmlspecialchars($this->kimenetHTML);
        
        Memoria::ir('postText',$this->kimenetHTML );
        Beepulok::futtat("runShortcodes");
        $this->kimenetHTML = Memoria::olvas('postText');
        
        eval('?>
        ' . $this->kimenetHTML . '
        <?
        ');

    }


}


