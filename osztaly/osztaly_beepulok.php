<?php

/**
 * Description of osztaly_beepulok
 * Plugin futtató osztály
 * @author Ceglédi Iván
 * 
 * két módon lehetséges beépülőt futtatni: adminon létrehozott file segítségével illetve a modul könyvtárban
 * elhelyezett bow_beepulok file által regisztrált módon
 * 
 * 
 * 
 */
class Beepulok
{
    public static $adat;
    public static $regiszter;
    public static $shortcode;
    public static $komponens;
    public static $widgetek;
    public static $ajaxHivas;
    
    public static function shortCodeList() {
        return self::$shortcode;
    }
    
    public static function regisztralKomponens($fuggveny, $cim, $uri)
    {
        
        self::$komponens[] = array(
            
            'fuggveny' => $fuggveny,
            'cim' => $cim,
            'uri' => $uri);

    }
    public static function regisztralAjaxHivas($fuggveny, $uri)
    {
        
        self::$ajaxHivas[] = array(
            
            'fuggveny' => $fuggveny,
            'uri' => $uri);

    }
    public static function ajaxHivasKereso($uri) {
        if (!empty(self::$ajaxHivas)) {
            
            foreach (self::$ajaxHivas as $sor) {
                if (trim($uri,'/')==$sor['uri']) {
                    if (function_exists($sor['fuggveny'])) {
                        
                        call_user_func($sor['fuggveny']);
                        exit;
                    }
                }
            }
        }
    }
    
    public static function regisztralShortcode($kod, $fuggveny)
    {
        self::$shortcode[] = array(
            
            'fuggveny' => $fuggveny,
            'kod' => $kod);

    }
    public static function regisztral($belepesiPont, $fuggveny, $parameter = array())
    {
        self::$regiszter[] = array(
            'belepesiPont' => $belepesiPont,
            'fuggveny' => $fuggveny,
            'parameter' => $parameter);

    }

    public static function futtat($belepes,& $adat = array())
    {

        $fileok = getRowsArr('bow_beepulok', "'$belepes' AND kikapcsolva = 0 ORDER BY sorrend ASC ",
            'belepes');
        if (!empty($fileok)) {
            foreach ($fileok as $file) {
                
                if (file_exists(BASE . 'beepulok/' . $file['file'])) {
                    include (BASE . 'beepulok/' . $file['file']);
                    //return $adat;
                }
            }

        }
        if (!empty(self::$regiszter))
        foreach (self::$regiszter as $fuggvenyek) {
            
            if ($fuggvenyek['belepesiPont'] == $belepes) {
                
                if (function_exists($fuggvenyek['fuggveny'])) {
                    call_user_func($fuggvenyek['fuggveny'], $adat);
                }
            }
        }
    }

    public static function parameter($p)
    {
        self::$adat = $p;
    }

    public static function autoLoad()
    {
        $dir = BASE . 'modulok/';
        if (is_dir($dir)) {
            if ($dh = opendir($dir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file != '.' and $file != '..') {
                        if (is_dir($dir . $file)) {
                            if (file_exists($dir . $file . '/bow_beepulok.php')) {
                                if (modulBekapcsolva($file)) {
                                    include_once ($dir . $file . '/bow_beepulok.php');
                                }
                            }
                            if (file_exists($dir . $file . '/widgetek.php')) {
                                if (modulBekapcsolva($file)) {
                                    $widgetPeldanyok = false;
                                    include_once ($dir . $file . '/widgetek.php');
                                    if(!empty($widgetPeldanyok)) {
                                        if (isset($widgetPeldanyok[0])) {
                                            foreach ($widgetPeldanyok as $peldany) {
                                                $peldany['path'] = $dir . $file . '/widgetek.php';
                                                self::$widgetek[] = $peldany;
                                            }
                                        } else {
                                            $widgetPeldanyok['path'] = $dir . $file . '/widgetek.php';
                                            self::$widgetek[] = $widgetPeldanyok;
                                        }
                                        
                                        
                                        
                                    }
                                }
                            }
                        }
                    }
                }
                closedir($dh);
            }
        }

    }
    
    public static function vanWidget($fuggveny) {
        if (empty(self::$widgetek)) return false;
        foreach (self::$widgetek as $sor) {
            if ($sor['fuggveny']==$fuggveny) return $sor;
        }
    }
}




