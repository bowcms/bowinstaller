<?php

/*
 * osztaly_beallitas
 *
 * A beállító file-okat dolgozza fel
 *
 *
 */



class Beallitas {
    
    public static function beallitasSqlBetolt($csoport) {
        $sql = "SELECT * FROM bow_config WHERE csoport LIKE '$csoport'";
        $rs = sqluniv4($sql);
        
        foreach ($rs as $sor) {
            if (!defined($sor['kulcs'])) {
                
                define ($sor['kulcs'], $sor['ertek']);
            }
        }
    }
    
    public static function beallitasBetolt($nev) {
        $beallitasDir = BASE.'/beallitas/';
        if (!is_file($beallitasDir.$nev.'.set.php')) {
            HibaKezelo::hibaJel(HIBA_KRITIKUS, 'Beállítás nem tölthető be: '.$nev);
            return FALSE;
        }
        $host = $_SERVER['HTTP_HOST'];
        if ((strpos($host, 'localhost') !== false) or (strpos($host, '127.0') !== false)) {
            $modulus = '.local';
        } else {
            $modulus = '';
        }
        $txt = file_get_contents($beallitasDir.$nev.".set$modulus.php");

        $file = explode("\n", $txt);
        foreach ($file as $sor) {
            if (strpos($sor, '#')===0) {
                $beallitasSor = trim($sor, '#;');

                $par = explode(':=', $beallitasSor);
                $kulcs = trim($par[0]);
                if (!isset($par[1])) {
                    $ertek = '';
                } else {
                    $ertek = trim($par[1]);
                }

                if (!defined($kulcs)) {
                    define($kulcs, $ertek);
                }
            }
        }
    }
}
