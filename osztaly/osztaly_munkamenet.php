<?php
/**
 * EyeSecureSession
 * Handles session encryption and decryption using CAST128
 *
 * LICENSE: This source file is subject to the BSD license
 * that is available through the world-wide-web at the following URI:
 * http://www.eyesis.ca/license.txt.  If you did not receive a copy of
 * the BSD License and are unable to obtain it through the web, please
 * send a note to mike@eyesis.ca so I can send you a copy immediately.
 *
 * @author     Micheal Frank <mike@eyesis.ca>
 * @copyright  2008 Eyesis
 * @license    http://www.eyesis.ca/license.txt  BSD License
 * @version    v1.0.1 11/12/2008 5:36:43 PM
 * @link       http://www.eyesis.ca/projects/securesession.html
 */


class Munkamenet
{
    const SESSION_PREFIX = 'EYESESS';
    const REGEN_COUNT = 5;
    const COUNT_NAME = '_eyesession_count';

    private $cast;
    private $secret;
    private static $peldany = NULL;

    /**
    * Constructor
    *
    * @param string $secret Private encryption key, set this to whatever just don't leave it blank
    * @return void
    */
    private function __construct($secret = '')
    {
        
        @session_start();
    }
    public static function getIstance() {
        if (self::$peldany == NULL) {
            self::$peldany = new Munkamenet;
        }
        return self::$peldany;
    }
    public static function peldany() {
        if (self::$peldany == NULL) {
            self::$peldany = new Munkamenet;
        }
        return self::$peldany;
    }
    /**
    * Retrieve a session variable
    *
    * @param string $name Name of the variable you are looking for
    * @return mixed
    */
    public function getVar($name)
    {
        if (!isset($_SESSION[$name])) return false;
        $value = $_SESSION[$name];

        return $value;
    }
public function getArr($name)
    {
        if (!isset($_SESSION[$name])) return false;
        $value = unserialize($_SESSION[$name]);

        return $value;
    }

    public function save($name,$value) {
        
        $_SESSION[$name] = $value;

        return true;
    }
    /**
    * Store a session variable
    *
    * @param string $name Name of the variable
    * @param mixed $value Value of the variable; can be string, array, object, etc
    * @return boolean
    */
    public function setVar($name, $value)
    {
        
        $value = serialize($value);
        $_SESSION[$name] = $value;

        return true;
    }
}