<?php

/**
 * 
 * osztaly_szerkeszto.php
 * 
 * Különböző javascript html szerkesztők betöltését végző osztály
 * 
 */
 
class jsSzerkeszto {
    private static $kodSzerkesztoBetoltve = false;
    private static $szerkesztoBetoltve = false;
    
    public static function szovegszerkeszto($id) {
        $html = Html::peldany();
        $html->headerStart();
        
        if (self::$szerkesztoBetoltve == false) {
            $base = array($_SERVER['SERVER_NAME']);
            if ($_SERVER['SERVER_PORT']!='') $base[] = $_SERVER['SERVER_PORT'];
            $base = 'http://'.implode(':', $base).'/';
            ?>
<script type="text/javascript" src="<?= BASE_URL ?>js/tiny_mce/tinymce.min.js"></script>
            <?php
        }
        ?>
        
        
<script type="text/javascript">
 tinyMCE.EditorManager.init({
    selector: "#<?= $id; ?>",
    theme: "modern",
    height: 300,
    plugins: [
         "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
   style_formats: [
        {title: 'Bold text', inline: 'b'},
        {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
        {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
        {title: 'Example 1', inline: 'span', classes: 'example1'},
        {title: 'Example 2', inline: 'span', classes: 'example2'},
        {title: 'Table styles'},
        {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ],
    relative_url : false,
    convert_urls: false,
    file_browser_callback: bowBrowser,
    document_base_url: "<?= $base; ?>"
 }); 
 

function bowBrowser (field_name, url, type, win) {

    //alert("Field_Name: " + field_name + "nURL: " + url + "nType: " + type + "nWin: " + win); // debug/testing

    /* If you work with sessions in PHP and your client doesn't accept cookies you might need to carry
       the session name and session ID in the request string (can look like this: "?PHPSESSID=88p0n70s9dsknra96qhuk6etm5").
       These lines of code extract the necessary parameters and add them back to the filebrowser URL again. */

    

    var frame = tinyMCE.activeEditor.windowManager.open({
        file : '<?= BASE_URL?>komponensek/bowBrowser/bowBrowser.php',
        title : 'Feltöltés',
        width : 700,  // Your dimensions may differ - toy around with them!
        height : 400,
        resizable : "no",
        inline : "no",  // This parameter only has an effect if you use the inlinepopups plugin!
        close_previous : "no"
    }, {
        window : win,
        input : field_name
    });
    window.bowBrowser_activeEditor = tinyMCE.activeEditor;
    window.bowBrowser_activeFrame = frame;
    window.bowBrowser_activeInput = field_name;

    return false;
  }
</script>
        <script>
function mySubmit(URL) {
  //call this function only after page has loaded
  //otherwise tinyMCEPopup.close will close the
  //"Insert/Edit Image" or "Insert/Edit Link" window instead

  
  var win = window.bowBrowser_activeEditor;

  // insert information now
  document.getElementById(window.bowBrowser_activeInput).value = URL;

  // for image browsers: update image dimensions
  
  // close popup window
  window.bowBrowser_activeFrame.close();
  }
</script>
        
        <?php
        $html->headerStop();
    }
    
    public static function kodSzerkeszto($id, $nyelv) {
        $html = Html::peldany();
        $html->headerStart();
        if (self::$kodSzerkesztoBetoltve == false) {
            self::$kodSzerkesztoBetoltve = true;
            ?>
            <script language="javascript" type="text/javascript" src="<?= BASE_URL?>js/edit_area/langs/hu.js"></script>
            <script language="javascript" type="text/javascript" src="<?= BASE_URL?>js/edit_area/edit_area_full.js"></script>
            <?php
        }
        ?>
        <script language="javascript" type="text/javascript">
        editAreaLoader.init({
	       id : "<?= $id; ?>"		// textarea id
	       ,syntax: "<?= $nyelv; ?>"			// syntax to be uses for highgliting
	       ,start_highlight: true,
           language: 'hu'		// to display with highlight mode on start-up
        });
        </script>
        <?php
        $html->headerStop();
    }
}