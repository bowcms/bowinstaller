<?php

/**
 * osztaly_hitelesito.php
 * 
 * alapvető form hitelesítés mezőnevek alapján
 * @author Cworrek
 * @copyright 2013
 */

class Hitelesito
{
    
    private static $peldany = NULL;
    private $mezoLista;
    private $vanHiba;

    private $hibaUzenet = array(
        'szukseges' => 'Kérem töltse ki!',
        'email' => 'Kérem, valós E-mail címet adj meg!',
        'jelszo' => 'Legalább 6 karakter hosszú jelszó szüksges, kis s nagybetűk, valamint számok is legyenek benne!');
    private $feltetelek = array(
        'szükséges' => 'szukseges',
        'jelszó' => 'jelszo',
        'email' => 'email');
        
    public function __construct() {
        self::$peldany = $this;
    }
    public function mezoEllenorzes($mezoNev, $feltetel)
    {
        $this->mezoLista[$mezoNev] = explode(',', $feltetel);
        
    }
    public function feltetelTorles()
    {
        $this->mezoLista = array();
    }
    public function hibaUzenetBeallito($uzenetek)
    {
        $this->hibaUzenet = $uzenetek;
    }
    public function hibaUzenetHozzaadas($kulcs,$uzenet)
    {
        $this->hibaUzenet[$kulcs] = $uzenet;
    }
    public function ellenorzes($post)
    {
       
        foreach ($post as $mezoNev => $mezo) {
            
            if (isset($this->mezoLista[$mezoNev])) {
                
                $feltetelek = $this->mezoLista[$mezoNev];
                if (!empty($feltetelek)) {
                    foreach ($feltetelek as $feltetel) {
                        $metodus =  $this->feltetelek[trim($feltetel)];
                        if ($metodus=='') {
                            die ('Hitelesítés hiba: '.$mezoNev.' mezőnél');
                        }
                        $this->$metodus($mezoNev, $mezo);
                    }
                }

            }
        }
        
    }
    public static function peldany() {
        return self::$peldany;
    }
    public static function hibaKiiratas($mezoNev) {
        $hibakezelo = self::$peldany;
        if ($hibakezelo == NULL) return;
        if (isset($hibakezelo->vanHiba[$mezoNev])) {
            $hibak = $hibakezelo->vanHiba[$mezoNev];
        } else {
            return;
        }
        
        if (empty($hibak)) return;
        foreach ($hibak as $hiba) {
            print $hiba.'<br />';
        }
        return;
    }
    
    private function szukseges($mezoNev, $mezo) {
        if ($mezo == '') {
            $this->vanHiba[$mezoNev][] = $this->hibaUzenet['szukseges'];
            
        }
        return;
    }
    private function email ($mezoNev, $mezo) {
        $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/'; 
        if (!preg_match($regex, $mezo)) {
            $this->vanHiba[$mezoNev][] = $this->hibaUzenet['email'];
        }
    }
    private function jelszo ($mezoNev, $mezo) {
        if (strlen($mezo) < 6) {
            $this->vanHiba[$mezoNev][] = $this->hibaUzenet['jelszo'];
        }
        return;
    }
    public function hibaTalalat() {
        if (empty($this->vanHiba)) {
            return false;
        } else {
            return true;
        }
    }
    public function hibaBeallitas($kulcs, $ertek) {
        $this->vanHiba[$kulcs][] = $ertek;
    }
}
