<?php

/*
 * Hibakezelo
 *
 * Fut�sidej� hib�kat,figyelmeztet�seket kezel
 */

define ('HIBA_FIGYELMEZTETES', 0);
define ('HIBA_KRITIKUS', 10);
define ('SIKERESEG_UZENET', 100);

class HibaKezelo {

    public static function hibaJel($szint, $hibaStr) {
        if (class_exists('Html')) {
            $html = Html::peldany();
        
            $html->hibaKiiratas($hibaStr);    
        } else {
            print '<b>'.$hibaStr.'</b>';
        }
        
    }
}