<?php

/**
 * osztaly_bow.php
 * 
 * A tartalomkezelő központi oszálya:
 * menük, tartalmek lekérdezése, modulok futtatása
 * 
 * @author Cworrek
 * @copyright 2013
 */


class Bow
{

    public static $peldany = 0;
    public static $aktivModulId = null;
    public $bovebben = 0;
    public $bovebbParam = null;
    public $bovebbVisszaUrl = null;
    private function __construct()
    {
        

    }

    public static function peldany()
    {
        
        if (self::$peldany === 0) {
            
            self::$peldany = new Bow();
        }
        return self::$peldany;
    }

    public function start()
    {
        $url = Url::peldany();
        $html = Html::peldany();
        $session = Munkamenet::peldany();
        $tag = Tagok::peldany();
        // regisztrált ajax hívás
        if ($url->reszek[0]!='') {
            Beepulok::ajaxHivasKereso($url->reszek[0]);
        }
        // widget-ek futtatása
        $sql = "SELECT * FROM bow_widgetxpozicio WHERE tema = '".STILUS."' ORDER BY sorrend ASC";
        $wgrs = sqluniv4($sql);
        if (!empty($wgrs)) {
            foreach($wgrs as $wg) {
                
                // oldalra korlátozás
                $sql = "SELECT * FROM bow_widgetxoldal WHERE widget_id = ".$wg['id']." ";
                $wgxoldalRs = sqluniv4($sql);
                if (!empty($wgxoldalRs)) {
                    // van korlátozás, mehet-e ki erre az oldalra?
                    $sql = "SELECT * FROM bow_widgetxoldal WHERE widget_id = ".$wg['id']." AND oldal_id = ".(int)$url->oldal_id;
                    $wgxoldalRs = sqluniv3($sql);
                    if (!isset($wgxoldalRs['id'])) {
                        
                        continue;
                    } 
                    
                }
                
                // csoportra korlátozás
                $sql = "SELECT * FROM bow_widgetxcsoport WHERE widget_id = ".$wg['id']." ";
                $wgxcsoportRs = sqluniv4($sql);
                if (!empty($wgxcsoportRs)) {
                    // van korlátozás, mehet-e ki erre az oldalra?
                    $sql = "SELECT * FROM bow_widgetxcsoport WHERE widget_id = ".$wg['id']." AND csoport_id = ".(int)$tag->adat['csoport_id'];
                    
                    $wgxcsoportRs = sqluniv3($sql);
                    if (!isset($wgxcsoportRs['id'])) {
                        
                        continue;
                    }
                    
                }
                
                // bent maradtunk, mehet a widget
                $html->helyorzoFelvetelStart();
               
                call_user_func($wg['fuggveny'],unserialize($wg['adat']));
                $html->helyorzoFelvetelStop($wg['pozicio']);
                // van-e adott widgetkonténer nyitó?
                
                
            }
        }
        

        // ha találtunk oldalt akkor futtatjuk az oldal modulokat
        
        if ($url->hivasTipus == 'oldal') {
            $sql = "SELECT * FROM bow_oldalak WHERE id = ".$url->oldal_id;
            $oRs = sqluniv3($sql);
            
            if ($oRs['meta_felulir'] == 1) {
                $html->helyorzo('meta_title', $oRs['cim']);
                $html->helyorzo('meta_keywords', $oRs['keywords']);
                $html->helyorzo('meta_description', $oRs['description']);
                
            } else {
                $html->forditottHelyorzoHozzafuzes('meta_title', $oRs['cim'],' - ');
                $html->forditottHelyorzoHozzafuzes('meta_keywords', $oRs['keywords'],' - ');
                $html->forditottHelyorzoHozzafuzes('meta_description', $oRs['description'],' - ');
            }
            
            
            
            $sql = "SELECT * FROM bow_oldalxtartalom WHERE oldal_id = ".$url->oldal_id." ORDER BY sorrend ASC";
            
            $tartalmak = sqluniv4($sql);
            if (empty($tartalmak)) {
                // nincs még tartalom ezen a lapon
                print 'Nincs tartalom az oldalon';
            } else {
                foreach ($tartalmak as $tartalom) {
                    
                    $sql = "SELECT csoport_id FROM bow_modulxcsoport WHERE oldalxtartalom_id = ".$tartalom['id'];
                    
                    $jogRs = sqluniv4($sql);
                    
                    if (empty($jogRs)) {
                        $jogosult = true;
                    } else {
                        $jogosult = false;
                        foreach($jogRs as $sor) {
                            if ($tag->adat['csoport_id']== $sor['csoport_id']) {
                                $jogosult = true;
                            }
                        }
                    }
                    
                    if (!$jogosult) continue;
                    if (modulKereso($tartalom['fuggveny']) ) {
                        $html->tartalomFelvetelStart();
                        if (function_exists($tartalom['fuggveny'])) 
                            call_user_func($tartalom['fuggveny'], @unserialize($tartalom['adat']));
                        else {
                            print __f('Modul indító függvény nem található!').'<br /><code>';
                            print_r (modulKereso($tartalom['fuggveny']));
                            print '</code>';
                        }
                        $html->tartalomFelvetelStop();
                        
                        
                    } else {
                        $html->tartalomFelvetelStart();
                        print '<strong>Modul futtató hiányzik</strong><br />';var_dump(modulKereso($tartalom['fuggveny']));print '<br />';
                        $html->tartalomFelvetelStop();
                    }
                    
                }
            }
            
            
        } elseif ($url->hivasTipus == 'komponens') {
            // metódus hívás?
            $komp = $url->hivottKomponens;
            $html->tartalomFelvetelStart();
            call_user_func_array($komp['fuggveny'], $url->param);
            $html->tartalomFelvetelStop();
            
            // beépülő futtatása
            Beepulok::futtat('Komponens futtatás', $sor);
        }

        // feldolgozás kész, kimenetet készítünk
        $html->kimenetKeszites();
    }

    public function modulFuttato($ut, $osztaly, $metodus, $adat = "", $modultablaId =
        0)
    {
        // letiltás ellenőrzése
        $sql = "SELECT letiltva FROM bow_modulok WHERE osztaly = '$osztaly' AND metodus = '$metodus'";
        $rs = sqluniv3($sql);
        
        if ($rs['letiltva'] == 1) return;
        if (!class_exists($osztaly)) {
            if (file_exists(BASE . 'modulok/' . $ut . '/' . $osztaly . '.php')) {
                require_once (BASE . 'modulok/' . $ut . '/' . $osztaly . '.php');
            } else {
                print 'Osztály nem létezik: ' . $osztaly . '::' . $metodus;
                return;
            }
            if (file_exists(BASE . 'modulok/' . $ut . '/' . $osztaly . 'Komp.php')) {
                require_once (BASE . 'modulok/' . $ut . '/' . $osztaly . 'Komp.php');
            }

            $o = new $osztaly();
        } else {
            $o = new $osztaly();
        }
        $o->id = $modultablaId;
        
        $komp = $osztaly . 'Komp';
        if (class_exists($komp)) {
            $komponens = new $komp();

        } else {
            $komponens = false;
        }
        $o->komp = $komponens;
        $o->bovebben = $this->bovebben;
        
        
        if (method_exists($o, $metodus)) {
            
            $param = $adat;
            
            if (is_array($param)) {
                call_user_func_array(array($o, $metodus), $param);

            } else {
                call_user_func(array($o, $metodus), $adat);

            }

        } else {
            print 'Metódus nem létezik: ' . $osztaly . '::' . $metodus;
        }
        
    }
    public static function statikusModulFuttato($ut, $osztaly, $metodus, $adat = "",
        $modultablaId = 0)
    {
        $sql = "SELECT letiltva, csoport FROM bow_modulok WHERE osztaly = '$osztaly' AND metodus = '$metodus'";
        $rs = sqluniv3($sql);
        if ($rs['letiltva'] == 1) return;
        
        if (!class_exists($osztaly)) {
            if (file_exists(BASE . 'modulok/' . $ut . '/' . $osztaly . '.php')) {
                require_once (BASE . 'modulok/' . $ut . '/' . $osztaly . '.php');
            } else {
                print 'Osztály nem létezik: ' . $osztaly . '::' . $metodus;
                return;
            }
            if (file_exists(BASE . 'modulok/' . $ut . '/' . $osztaly . 'Komp.php')) {
                require_once (BASE . 'modulok/' . $ut . '/' . $osztaly . 'Komp.php');
            }
            $komp = $osztaly . 'Komp';
            
            $o = new $osztaly();
        } else {
            $o = new $osztaly();
        }
        $o->id = $modultablaId;
        
        if (file_exists(BASE . 'modulok/' . $ut . '/' . $osztaly . 'Komp.php')) {
            require_once (BASE . 'modulok/' . $ut . '/' . $osztaly . 'Komp.php');
        }
        $komp = $osztaly . 'Komp';
        if (class_exists($komp)) {
            $komponens = new $komp();

        } else {
            $komponens = false;
        }
        $o->komp = $komponens;
        
        if (method_exists($o, $metodus)) {
            if (is_array($adat)) {
                $param = $adat;
            } else {
                $param = @unserialize($adat);
            }
            if (is_array($param)) {
                call_user_func_array(array($o, $metodus), $param);

            } else {
                call_user_func(array($o, $metodus), $adat);

            }

        } else {
            print 'Metódus nem létezik: ' . $osztaly . '::' . $metodus;
        }
    }
}
