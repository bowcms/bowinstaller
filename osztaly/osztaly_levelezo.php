<?php

/**
 * 
 * osztaly_levelezo.php
 * 
 * levél küldése 
 * 
 */

class Levelezo
{

    private static $peldany = null;
    private static $adat = array();

    private function __construct()
    {

    }
    public static function peldany()
    {
        if (self::$peldany == null) {
            self::$peldany = new Levelezo();
        }
        return self::$peldany;
    }

    public static function sablon($ertek)
    {
        self::$adat['kulcs'] = $ertek;
        $sql = "SELECT * FROM bow_levelek WHERE kulcs = '$ertek' LIMIT 1";
        $rs = sqluniv3($sql);
        
        if (!empty($rs['valaszcim'])) {
            self::valaszEmail($rs['valaszcim']);
        }
        if (!empty($rs['valasznev'])) {
            self::valaszNev($rs['valasznev']);
        }
        if (!empty($rs['targy'])) {
            self::targy($rs['targy']);
        }
        if (!empty($rs['szoveg'])) {
            self::tartalom($rs['szoveg']);
        }
        
        
    }
    public static function valaszNev($ertek)
    {
        self::$adat['valasznev'] = $ertek;
    }
    public static function valaszEmail($ertek)
    {
        self::$adat['valaszcim'] = $ertek;
    }
    public static function targy($ertek)
    {
        self::$adat['targy'] = $ertek;
    }
    public static function tartalom($ertek)
    {
        self::$adat['tartalom'] = $ertek;
    }
    
    public static function helyettesito($ertek)
    {
        self::$adat['helyettesito'] = $ertek;
    }
    public static function helyettesitoHozzaad($kulcs , $ertek)
    {
        self::$adat['helyettesito'][$kulcs] = $ertek;
    }
    public static function kuldes($email)
    {
        include_once (BASE."osztaly/class.phpmailer5.php");

        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        
        if (EMAIL_SMTP_HOST != '') {
            $mail->IsSMTP();
            //$mail->SMTPDebug = true;
            $mail->Host = EMAIL_SMTP_HOST; // SMTP server

            if (EMAIL_SMTP_USER != '') {

                $mail->SMTPAuth = true; // enable SMTP authentication
                $mail->Username = EMAIL_SMTP_USER; // SMTP account username
                $mail->Password = EMAIL_SMTP_PASS; // SMTP account password

            }
            $mail->Port = EMAIL_SMTP_PORT;

        }
        if (!isset(self::$adat['valaszcim'])) self::$adat['valaszcim'] = '';
        if (self::$adat['valaszcim'] != '') {
            $mail->From = self::$adat['valaszcim'];
        } else {
            $mail->From = EMAIL_FELADO_CIM;
        }
        
        if (!isset(self::$adat['valasznev'])) self::$adat['valasznev'] = '';
        if (self::$adat['valasznev'] != '') {
            $mail->FromName = self::$adat['valasznev'];
        } else {
            $mail->FromName = EMAIL_FELADO_NEV;
        }
         // Feladó e-mail címe
        $mail->AddAddress($email, $email); // Címzett és neve
        $mail->AddBCC("pehapesenior@gmail.com", "Ivan Cegledi"); // Címzett és neve
        
        $mail->IsHTML(true); // Küldés HTML-ként
        
        if (!isset(self::$adat['targy'])) self::$adat['targy'] = '';
        $mail->Subject = self::$adat['targy'] ; // A levél tárgya
        
        
        $level = self::$adat['tartalom'];
        
        // helyettesíts
        if(!empty(self::$adat['helyettesito']))
            foreach (self::$adat['helyettesito'] as $kulcs => $ertek) {
                $level = str_replace('|'.$kulcs.'|', $ertek, $level);
            }
        
        //print $level;
        $mail->Body = '' . $level . ''; // A levél tartalma
       
        $mail->AltBody = strip_tags($level); // A levél szöveges tartalma
        //print_r ($mail);
        $err = $mail->Send();
        
        return true;
    }
    
    public static function kuldesCsatolmannyal($email,$targy, $level,  $csatolmanyok = array())
    {
        include_once ("osztaly/class.phpmailer5.php");

        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        
        if (EMAIL_SMTP_HOST != '') {
            $mail->IsSMTP();
            //$mail->SMTPDebug = true;
            $mail->Host = EMAIL_SMTP_HOST; // SMTP server

            if (EMAIL_SMTP_USER != '') {

                $mail->SMTPAuth = true; // enable SMTP authentication
                $mail->Username = EMAIL_SMTP_USER; // SMTP account username
                $mail->Password = EMAIL_SMTP_PASS; // SMTP account password

            }
            $mail->Port = EMAIL_SMTP_PORT;

        }
        if (!isset(self::$adat['valaszcim'])) self::$adat['valaszcim'] = '';
        if (self::$adat['valaszcim'] != '') {
            $mail->From = self::$adat['valaszcim'];
        } else {
            $mail->From = EMAIL_FELADO_CIM;
        }
        
        if (!isset(self::$adat['valasznev'])) self::$adat['valasznev'] = '';
        if (self::$adat['valasznev'] != '') {
            $mail->FromName = self::$adat['valasznev'];
        } else {
            $mail->FromName = EMAIL_FELADO_NEV;
        }
         // Feladó e-mail címe
        $mail->AddAddress($email, $email); // Címzett és neve
        $mail->AddBCC("pehapesenior@gmail.com", "Ivan Cegledi"); // Címzett és neve
        
        $mail->IsHTML(true); // Küldés HTML-ként
        
        
        $mail->Subject = $targy; // A levél tárgya
        
        
        $level = $level;
        
        // helyettesíts
        
        //print $level;
        $mail->Body = '' . $level . ''; // A levél tartalma
       
        $mail->AltBody = strip_tags($level); // A levél szöveges tartalma
        //print_r ($mail);
        $err = $mail->Send();
        
        return true;
    }

}
