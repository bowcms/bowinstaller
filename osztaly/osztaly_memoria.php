<?php

/**
 * 
 * osztaly_memoria.php
 * 
 * Adat tárolásra használható singleton
 * 
 */

class Memoria {
    
    private static $peldany = null;
    private static $adat = array();
    
    private function __construct() {
        
    }
    public static function peldany() {
        if (self::$peldany == null) {
            self::$peldany = new Memoria();
        }
        return self::$peldany;
    }
    
    public static function tombHozzafuz($kulcs, $tomb) {
        self::$adat[$kulcs][] = $tomb;
    }
    
    public static function ir($kulcs, $ertek) {
        self::$adat[$kulcs] = $ertek;
    }
    public static function olvas($kulcs) {
        if(isset(self::$adat[$kulcs])){
            return self::$adat[$kulcs];
        } else {
            return false;
        }
    }
    
}

