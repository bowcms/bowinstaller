<?php

/**
 * osztaly_telepito.php
 * 
 * @author Cworrek
 * @copyright 2013
 */


class Telepito
{

    public static function vizsgalat()
    {
        if (DBHOST == '' or DBUSER == '') {
            // installálunk
            $telepito = new Telepito();
            return;
        }
        define('TELEPITES', false);
    }

    public function __construct()
    {
        require_once (BASE . 'osztaly/osztaly_hitelesito.php');
        $hitelesito = new Hitelesito();

        include (BASE . 'komponensek/komp_telepito.php');
        $komp = new TelepitoKomp();
        define('TELEPITES', true);
        set_time_limit(0);
        if (isset($_POST['a'])) {


            $hitelesito->feltetelTorles();
            $hitelesito->mezoEllenorzes('DBHOST', 'szükséges');
            $hitelesito->mezoEllenorzes('DBUSER', 'szükséges');
            $hitelesito->mezoEllenorzes('DBNAME', 'szükséges');

            $hitelesito->ellenorzes($_POST['a']);

            $hitelesito->feltetelTorles();
            $hitelesito->mezoEllenorzes('vezeteknev', 'szükséges');
            $hitelesito->mezoEllenorzes('keresztnev', 'szükséges');
            $hitelesito->mezoEllenorzes('email', 'email');
            $hitelesito->mezoEllenorzes('pwd', 'jelszó');

            $hitelesito->ellenorzes($_POST['u']);

            $hitelesito->feltetelTorles();
            $hitelesito->mezoEllenorzes('BASE_URL', 'szükséges');
            $hitelesito->mezoEllenorzes('BOW_META_TITLE', 'szükséges');
            $hitelesito->mezoEllenorzes('BOW_META_DESC', 'szükséges');
            $hitelesito->mezoEllenorzes('BOW_META_KEYWORDS', 'szükséges');

            $hitelesito->ellenorzes($_POST['w']);

            $hitelesito->feltetelTorles();
            $hitelesito->mezoEllenorzes('EMAIL_FELADO_NEV', 'szükséges');
            $hitelesito->mezoEllenorzes('EMAIL_FELADO_CIM', 'email');

            $hitelesito->ellenorzes($_POST['e']);

            if (!$hitelesito->hibaTalalat()) {

                if ($this->install()) {
                    $komp->siker();
                    return;
                }

            }

        } else {
            $_POST['a']['DBHOST'] = 'localhost';
            $_POST['a']['DBNAME'] = '';
            $_POST['a']['DBUSER'] = '';
            $_POST['a']['DBPASS'] = '';

            $_POST['u']['vezeteknev'] = '';
            $_POST['u']['keresztnev'] = '';
            $_POST['u']['email'] = '';
            $_POST['u']['pwd'] = '';

            $_POST['w']['BASE_URL'] = '';
            $_POST['w']['CMS_KONYVTAR'] = '';
            $_POST['w']['BOW_META_TITLE'] = '';
            $_POST['w']['BOW_META_DESC'] = '';
            $_POST['w']['BOW_META_KEYWORDS'] = '';

            $_POST['e']['EMAIL_FELADO_NEV'] = '';
            $_POST['e']['EMAIL_FELADO_CIM'] = '';
            $_POST['e']['EMAIL_SMTP_HOST'] = '';
            $_POST['e']['EMAIL_SMTP_PORT'] = '';
            $_POST['e']['EMAIL_SMTP_USER'] = '';
            $_POST['e']['EMAIL_SMTP_PASS'] = '';

        }

        $komp->regisztracio();
    }

    public function install()
    {

        // adatbázis beállító
        //print_r ($_POST['a']);
        $adatbazisConf = '<?php

/*
 *    Bow CMS adatbázis beállító file
 */

';
        foreach ($_POST['a'] as $kulcs => $ertek) {
            $hitelesito = Hitelesito::peldany();


            $adatbazisConf .= "
#$kulcs:=               $ertek
";

        }
        print $adatbazisConf;
        // teszteljül a DB kapcsolatot

        $res = @mysql_connect($_POST['a']['DBHOST'], $_POST['a']['DBUSER'], $_POST['a']['DBPASS']);
        if (!is_resource($res)) {

            $hitelesito->hibaBeallitas('dbhosthiba',
                'Nem sikerült csatlakozni az adatbázishoz!');

            return false;
        }

        if (!mysql_select_db($_POST['a']['DBNAME'])) {

            $hitelesito->hibaBeallitas('dbhosthiba', 'Adatbázis kivlasztása sikertelen!');

            return false;
        }
        mysql_set_charset('UTF-8');
        mysql_query("SET NAMES utf8 ");

        $templine = '';
        // Read in entire file
        $lines = file(BASE . 'beallitas/install.sql');
        // Loop through each line
        foreach ($lines as $line) {
            // Skip it if it's a comment
            if (substr($line, 0, 2) == '--' || $line == '')
                continue;

            // Add this line to the current segment
            $templine .= $line;
            // If it has a semicolon at the end, it's the end of the query
            if (substr(trim($line), -1, 1) == ';') {
                // Perform the query
                mysql_query($templine) or print ('Installációs hiba: \'<strong>' . $templine . '\': ' .
                    mysql_error() . '<br /><br />');
                // Reset temp variable to empty
                $templine = '';
            }
        }
        
        // Minden oké, mehetnek a config adatok az adatbzisba

        foreach ($_POST['e'] as $kulcs => $ertek) {

            $this->beallitasIras($kulcs, $ertek, 'E-mail bellítások');

        }
        foreach ($_POST['w'] as $kulcs => $ertek) {

            $this->beallitasIras($kulcs, $ertek, 'Weboldal általános beállítások');


        }


        $user = $_POST['u'];
        $sql = "DELETE * FROM bow_tagok WHERE 1 = 1";
        mysql_query($sql);
        $sql = "SELECT id FROM bow_csoportok ORDER BY rang DESC LIMIT 1";
        $rs = mysql_query($sql);
        $csoportId = mysql_fetch_assoc($rs);
        $csoportId = $csoportId['id'];

        $sql = "INSERT INTO bow_tagok SET 
            nev = '" . $user['vezeteknev'] . ' ' . $user['keresztnev'] . "',
            email = '" . $user['email'] . "',
            jelszo = '" . base64_encode( md5($user['pwd']) ). "',
            csoport_id = '" .$rs['id']. "',
            regido = '" . time() . "'
        ";

        mysql_query($sql);

        include_once ("osztaly/class.phpmailer5.php");

        $mail = new PHPMailer();
        $mail->CharSet = 'UTF-8';
        $email = $user['email'];
        if ($_POST['e']['EMAIL_SMTP_HOST'] != '') {
            $mail->IsSMTP();
            $mail->Host = $_POST['e']['EMAIL_SMTP_HOST']; // SMTP server

            if ($_POST['e']['EMAIL_SMTP_USER'] != '') {
                
                $mail->SMTPAuth = true; // enable SMTP authentication
                $mail->Username = $_POST['e']['EMAIL_SMTP_USER']; // SMTP account username
                $mail->Password = $_POST['e']['EMAIL_SMTP_PASS']; // SMTP account password

            }
            $mail->Port = $_POST['e']['EMAIL_SMTP_PORT'];

        }

        $mail->From = $_POST['e']['EMAIL_FELADO_CIM']; // Feladó e-mail címe
        $mail->FromName = $_POST['e']['EMAIL_FELADO_NEV']; // Feladó neve
        $mail->AddAddress($user['email'] , $user['vezeteknev'] . ' ' . $user['keresztnev']); // Címzett és neve
        //$mail->AddBCC("ivan@web2marketing.hu", "Ivan."); // Címzett és neve
        //$mail->AddBCC("web@web2marketing.hu","Négypólus Kft.");   // Címzett és neve

        //$mail->AddReplyTo("info@site.com","Information");  // Válaszlevél ide
        $mail->WordWrap = 50; // Sortörés állítása
        $mail->IsHTML(true); // Küldés HTML-ként

        $mail->Subject = "BoW Tartalomkezelő telepítése sikeres!"; // A levél tárgya
        
        $level = '<h1>Gratulálunk, sikeresen telepítetted új tartalomkezelő rendszeredet!</h1>';
        $level .= '<p>Frissen elkészült oldaladat itt érheted el: <a href="'.$_POST['w']['BASE_URL'].'">'.$_POST['w']['BASE_URL'].'</a>';
        $level .= '<p>Belépési adataid:</p>';
        $level .= "<pre>
        Belépési e-mail cím: <b>$user[email]</b>
        Jelszavad: <b>$user[pwd]</b>
        </pre>";
        $level .= '<p>Üdvözlettel: a boldog Bow CMS</p>';
        //print $level;
        $mail->Body = '' . $level . ''; // A levél tartalma
        $mail->AltBody = "Megrendelése a Printerszalon oldalán sikeres! Kollégáink hamarosan felveszik Önnel a kapcsolatot!"; // A levél szöveges tartalma
        //print_r ($mail);
        $err = $mail->Send();
        //var_dump($err);

        file_put_contents(BASE.'beallitas/adatbazis.set.php', $adatbazisConf);
        return true;
    }

    public function beallitasIras($kulcs, $ertek, $csoport)
    {
        if ($this->beallitasOlvasas($kulcs) == 0) {
            $sql = "INSERT INTO bow_config SET kulcs = '$kulcs', ertek = '$ertek', csoport = '$csoport'";
            mysql_query($sql);
        } else {
            $sql = "UPDATE bow_config SET ertek = '$ertek', csoport = '$csoport' WHERE  kulcs = '$kulcs' LIMIT 1";
            mysql_query($sql);
        }
    }

    public function beallitasOlvasas($kulcs)
    {
        $sql = "SELECT * FROM bow_config WHERE kulcs = '$kulcs' ";
        $res = mysql_query($sql);
        return @mysql_num_rows($res);
    }
}
