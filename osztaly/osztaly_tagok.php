<?php

/*
 *  osztaly_tagok
 *
 * felhasználói feladatok:
 *
 * beléptetés, kiléptetés,
 * felvitel, módosíás
 * csoport jogok
 *
 */

class Tagok {

    private static $peldany = NULL;

    public $adat = array();

    public $csoportNev;

    public $id;
    
    public $rang;
    
    public $csomagLejart = 0;
    
    public $admin = false;

    private function __construct() {
        $session = Munkamenet::getIstance();
        $tagId = $session->getArr('tagId');
        
        if (!$tagId) {
            $this->id = 0;
            $csoportId = getCol('id', 'bow_csoportok', ' alapertelmezett = 1 ');
            $this->csoportNev = getCol('nev', 'bow_csoportok', ' alapertelmezett = 1 ');
			$sql = "SELECT accesstoken FROM bow_tagok ORDER BY aktivitas DESC LIMIT 1";
			$rs = sqluniv3($sql);
            $this->adat = array(
                'nev' => 'Látogató',
                'csoport_id' => $csoportId,
                'accesstoken' => $rs['accesstoken']
            );
        } else {
            $this->id = $tagId;
            $letezik = getRows('bow_tagok', $tagId);
            if (!isset($letezik['id'])) {
                $this->kileptet($tagId);
                return;
            }
            $this->aktivitas($tagId);
            $this->beleptet ($tagId);
            $sql = "UPDATE bow_tagok SET aktivitas = ".time()." WHERE id = $tagId";
            sqluniv($sql);
            $this->adat = sqluniv3("SELECT * FROM bow_tagok WHERE id = $tagId LIMIT 1");
            
            
        }

    }

    public static function peldany() {
        if (self::$peldany == NULL) {
            self::$peldany = new Tagok();
        }
        return self::$peldany;
    }

    public function beleptet($tagId) {
        $session = Munkamenet::getIstance();
        $this->adat = sqluniv3("SELECT * FROM bow_tagok WHERE id = $tagId LIMIT 1");
        $this->id = $tagId;
        $this->rang = getCol('rang', 'bow_csoportok', " id = ".$this->adat['csoport_id']);
        $this->csoportNev = getCol('nev', 'bow_csoportok', " id = ".$this->adat['csoport_id']);
        $admin = getCol('admin', 'bow_csoportok', " id = ".$this->adat['csoport_id']);
        if($admin == 1) $this->admin = true;
        if ($this->adat) {

            $session->setVar('tagId', $tagId);
            $this->id = $tagId;
        }
        
    }
    public function kileptet() {
        $session = Munkamenet::getIstance();
        $session->setVar('tagId', false);
        $this->id = 0;
        $csoportId = getCol('id', 'bow_csoportok', ' alapertelmezett = 1 ');
        $this->csoportNev = getCol('nev', 'bow_csoportok', ' alapertelmezett = 1 ');

        $this->adat = array(
                'nev' => 'Látogató',
                'csoport_id' => $csoportId
        );

    }

    public function jogosult($osztaly, $metodus, $lehetoseg) {
        $jogokId = getCol('id', 'bow_csoportjogok', " osztaly = '$osztaly' AND metodus = '$metodus' ");
        if (!$jogokId) {
            HibaKezelo::hibaJel(HIBA_FIGYELMEZTETES, 'Nincs jogosultsági beállítás ehhez: '.$osztaly.'::'.$metodus.'!');
            return;

        }
        $csoportId = $this->adat['csoport_id'];
        $sql = "SELECT * FROM bow_csoportxjogok WHERE
            csoport_id = $csoportId AND
            csoportjog_id = $jogokId AND
            lehetoseg = $lehetoseg
            LIMIT 1
        ";
        if (sqltalalat($sql)) {
            return true;
        } else {
            return false;
        }
    }

    public static function vanEJog($cim, $lehetoseg) {
        $tag = self::$peldany;
        $jogId = getCol('id', 'bow_csoportjogok', " cim = '$cim' LIMIT 1");
        if (!$jogId) return false;
        $csopJog = getCol("id", 'bow_csoportxjogok', " csoport_id = ".$tag->adat['csoport_id']." AND csoportjog_id = $jogId AND lehetoseg = $lehetoseg");
        if ($csopJog) return true;
        return false;
    }
    public static function aktivitas($tagId) {
        return;
        if (!isset($_SESSION['aktivitasId'])) {
            $sql = "UPDATE mod_felhasznalo_aktivitas SET lezarva = 1 WHERE tagid = $tagId";
            sqluniv($sql);
            $azonosito = $_SESSION['aktivitasId'] = md5($tagId.rand(10000,99999).time());
            $sql = "INSERT INTO mod_felhasznalo_aktivitas SET 
                tagid = $tagId,
                belepes = ".time()." , 
                aktivitas = ".time().",
                azonosito = '$azonosito'
            ";
            sqluniv($sql);
        } else {
            $azonosito = $_SESSION['aktivitasId'];
            $sql = "UPDATE mod_felhasznalo_aktivitas SET 
                aktivitas = ".time()."
                WHERE
                azonosito = '$azonosito'
            ";
            sqluniv($sql);
        }
        
    }
}
