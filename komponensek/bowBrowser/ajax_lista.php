<?php
session_start();

// UTF-8 feljéc elküldése

header("Content-type: text/html; charset=utf-8");

$base = dirname(dirname(dirname(__FILE__))).'/';
define('BASE', $base);
include($base.'osztaly/osztaly_beallitas.php');
Beallitas::beallitasBetolt('adatbazis');
include_once $base.'osztaly/sqlfuggvenyek.php';


include(BASE.'kozos_fuggvenyek.php');

Beallitas::beallitasSqlBetolt('Weboldal általános beállítások');
$alapKonyvtar = FELTOLTES_KEPKONYVTAR;
//$feltoltoUrl = CIKKEK_KEPKONYVTAR;
$feltoltoURL = BASE_URL. $alapKonyvtar;
$feltoltoUt = BASE. $alapKonyvtar;
$almappa = '';
if (isset($_GET['almappa'])) {
    $almappa = $_GET['almappa'];
}

$almappa = explode('/', $almappa);
if (isset($almappa[0])) {
    $m = count($almappa)-1;
    unset($almappa[$m]);
    $utolso = end($almappa);
    
    if ($utolso=='..') {
        $m = count($almappa)-1;
        unset($almappa[$m]);
        if (isset($almappa[$m-1])) unset($almappa[$m-1]);
        
        
    }
}
$almappa = implode('/', $almappa).'/';
if ($almappa == '/') $almappa = '';

if (isset($_POST['mappa'])) {
    $mappa = strToUrl($_POST['mappa']);
    
    $hiba = '';
    if (file_exists($feltoltoUt.$almappa.$mappa)) {
        print '<div class="hiba">File vagy mappa már létezik!</div>';
    } else {
        mkdir($feltoltoUt.$almappa.$mappa);    
    }
    
}


?>
<div class="almappaUtvonal">Képek mappa /<?= $almappa; ?></div>
<div class="feltolto lenyilo">
    <form enctype="multipart/form-data" method="post" id="kepek" action="bowBrowser.php?almappa=<?= $almappa; ?>" >
        <div class="filefeltolto">File 1 <input type="file" name="kep[]" /></div>
        <div class="filefeltolto">File 2 <input type="file" name="kep[]" /></div>
        <div class="filefeltolto">File 3 <input type="file" name="kep[]" /></div>
        <div class="filefeltolto">File 4 <input type="file" name="kep[]" /></div>
        <div class="filefeltolto">File 5 <input type="file" name="kep[]" /></div>
        <div class="filefeltolto">File 6 <input type="file" name="kep[]" /></div>
        <input  type="submit" name="submitFile" value="Feltöltés (összesen max <?= ini_get("upload_max_filesize");?>)" /> <input type="button" value="Mégsem" onclick="$('.feltolto').slideUp();"/>
    </form>
</div>
<div class="ujmappa lenyilo">
    <form enctype="multipart/form-data" method="post" id="ujmappa" onsubmit="return false;" action="ajax_lista.php?almappa=<?= $almappa; ?>">
        <input type="hidden" name="almappa" value="<?= $almappa; ?>" />
        <div class="filefeltolto">Új mappa neve: <input type="text" name="mappa" /></div>
        
        <input type="submit" onclick="$.post('ajax_lista.php?almappa=<?= $almappa; ?>' ,$('#ujmappa').serialize(), function(e){$('.mezo').html(e);});return false;" value="Létrehozás" /> <input type="button" value="Mégsem" onclick="$('.lenyilo').slideUp();"/>
    </form>
</div>
<?php

$dir = $feltoltoUt.$almappa;

// Open a known directory, and proceed to read its contents
if (is_dir($dir)) {
    if ($dh = opendir($dir)) {
        while (($file = readdir($dh)) !== false) {
            $fileNev = wordwrap($file,10,'<br />',true);
            if (is_dir($dir.$file)):
                //
                //rejtett könyvtár
                if (strpos($file, '.')===0 and $file != '..') continue;
                // gyökérben vagyunk, nincs vissza
                if ($file == '..' and $almappa == '') continue;
                if ($file == '..') {
                    $kep = 'vissza.png';
                    $fileNev = '<b style="color: #999;">VISSZA</b>';  
                } else $kep = 'mappa.png';
            ?>
            <div class="mappa" onclick="$('.mezo').load('ajax_lista.php?almappa=<?= $almappa.$file?>/');">
                <img src="<?= $kep;?>" width="100" height="100"/>
                <br />
                <span class="cim"><?= $fileNev; ?></span>
            </div>
            <?php
            endif;
            // rejtett file
            if (strpos($file, '.')===0)continue;
            $ext = @end(explode('.', $file));
            $ext = strtolower($ext);
           
            if (is_file($dir.$file)):
                
                
                if ($ext=='jpg' or $ext=='jpeg' or $ext=='png' or $ext=='gif' or $ext=='ico') {
                    $kep = kiskepKockaStr($feltoltoURL.$almappa.$file, 100, ' style="border:1px solid #aaa;" ');
                } else {
                    $kep = '<img src="file.png" width="100" height="100" />';
                }
            ?>
            <div class="mappa" onclick="parent.mySubmit('<?= CMS_KONYVTAR.FELTOLTES_KEPKONYVTAR.$almappa.$file; ?>');">
                <?= $kep; ?>
                <br />
                <span class="cim"><?= $fileNev; ?></span>
            </div>
            <?php
            endif;
            
            
        }
        closedir($dh);
    }
}
