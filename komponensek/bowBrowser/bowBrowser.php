<?php
session_start();

// UTF-8 feljéc elküldése

header("Content-type: text/html; charset=utf-8");

$base = dirname(dirname(dirname(__FILE__))).'/';
define('BASE', $base);
include($base.'osztaly/osztaly_beallitas.php');
Beallitas::beallitasBetolt('adatbazis');
include_once $base.'osztaly/sqlfuggvenyek.php';
include(BASE.'kozos_fuggvenyek.php');
Beallitas::beallitasSqlBetolt('Weboldal általános beállítások');

$feltoltoUrl = FELTOLTES_KEPKONYVTAR;
if (!isset($_GET['almappa']))$_GET['almappa'] = '';
$almappa = $_GET['almappa'];


if (isset($_FILES['kep'])) {
    
    foreach ($_FILES['kep']['name'] as $k => $fileNev) {
        if ($fileNev != '') {
            $ext = @end(explode('.', $fileNev));
            $nev = str_replace('.'.$ext, '', $fileNev);
            $nev = strToUrl($nev);
            $ujNev = $nev.'.'.$ext;
            $i = 0;
            while (file_exists(BASE.$feltoltoUrl.$almappa.$ujNev)) {
                $ujNev = $ujNev = $nev.'_'.$i.'.'.$ext;
            }
            if (!move_uploaded_file($_FILES['kep']['tmp_name'][$k],BASE.$feltoltoUrl.$almappa.$ujNev )) {
                print '<div class="hiba">File feltöltése sikertelen: <strong>'.BASE.$feltoltoUrl.$almappa.$ujNev.'</strong></div>';
            }
        }
        
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Filefeltöltő</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="hu" />

	
    
<link href="<?= BASE_URL ?>komponensek/bowBrowser/style.css?r=<?= rand(100,999);?>" rel="stylesheet" type="text/css" media="screen" />
<script type="text/javascript" src="jquery.js"></script>
<script>
    $().ready(function() {
       $('.mezo').load('ajax_lista.php?almappa=<?=$almappa; ?>'); 
    });
</script>
<script type="text/javascript" src="../../js/tiny_mce/tiny_mce_popup.js"></script>
<script>
var FileBrowserDialogue = {
    init : function () {
        // Here goes your code for setting your custom things onLoad.
    },
    mySubmit : function (URL) {
        var win = tinyMCEPopup.getWindowArg("window");

        // insert information now
        win.document.getElementById(tinyMCEPopup.getWindowArg("input")).value = URL;

        // are we an image browser
        if (typeof(win.ImageDialog) != "undefined") {
            // we are, so update image dimensions...
            if (win.ImageDialog.getImageData)
                win.ImageDialog.getImageData();

            // ... and preview if necessary
            if (win.ImageDialog.showPreviewImage)
                win.ImageDialog.showPreviewImage(URL);
        }

        // close popup window
        tinyMCEPopup.close();
    },
    closeBrowser : function() {
        tinyMCEPopup.close();
    }
}

tinyMCEPopup.onInit.add(FileBrowserDialogue.init, FileBrowserDialogue);
</script>
</head>
<body>
<div class="panel">
    <a href="javascript:void(0);" onclick="$('.feltolto').slideDown();">Feltöltés</a>
    <a href="javascript:void(0);" onclick="$('.ujmappa').slideDown();">Új mappa</a>
    <a href="javascript:void(0);" class="utolso" onclick="FileBrowserDialogue.closeBrowser();">Bezárás</a>
    <div class="cimke">bowBrowser plugin</div> 
</div>

<div class="mezo"></div>
</body>
</html>