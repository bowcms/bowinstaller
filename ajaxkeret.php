<?php

/**
 * 
 * BowCMS 2.0 ajax keret
 * adott modul adott file-ját betölti
 * Request adatok:
 * 
 * ajax_token
 * tagid
 * modul
 * file
 * 
 * 
 * 
 * Ceglédi Iván
 * 2013
 */
 
session_start();

// UTF-8 feljéc elküldése

header("Content-type: text/html; charset=utf-8");

// Alap elérsi útvonal

define ('BASE', dirname(__FILE__).'/');

include(BASE.'kozos_fuggvenyek.php');

// Globális datastore osztály betöltése

include(BASE.'osztaly/osztaly_memoria.php');

// Alap beállítások betöltése

include(BASE.'osztaly/osztaly_beallitas.php');
Beallitas::beallitasBetolt('adatbazis');
// Tovbbi konstansok betöltése
include_once BASE.'osztaly/sqlfuggvenyek.php';

// modulok és beépülők registrálása

Beallitas::beallitasSqlBetolt('Rendszer');
Beallitas::beallitasSqlBetolt('Weboldal általános beállítások');
Beallitas::beallitasSqlBetolt('E-mail bellítások');
include(BASE.'osztaly/osztaly_hibakezelo.php');
include BASE.'osztaly/osztaly_munkamenet.php';
include BASE.'osztaly/osztaly_tagok.php';
include BASE.'osztaly/osztaly_beepulok.php';
include BASE.'osztaly/osztaly_levelezo.php';

$session = Munkamenet::getIstance();

$tag = Tagok::peldany();

modulElotolto();
Beepulok::autoLoad();



ini_set('DISPLAY_ERRORS', PHP_HIBA);

// Hibajelzés kapcsolása

if (defined('CSAK_KERET')) return;
// megvizsgáljuk, hogy telepítve van-e a rendszer
include(BASE.'osztaly/osztaly_telepito.php');

if ($_REQUEST['ajax_token']!=$_SESSION['ajax_token']) die('Nincs hozzaferes 1!');




if ($_REQUEST['tagid']!=$tag->id) die('Nincs hozzaferes! 2');

$include = $_REQUEST['modul'];
$file = $_REQUEST['file'];
include_once(BASE.'modulok/'.$include.'/'.$include.'.php');
$osztaly = new $include;

if (file_exists(BASE.'modulok/'.$include.'/'.$include.'Komp.php')) {
    include_once (BASE.'modulok/'.$include.'/'.$include.'Komp.php');
    $kompNev = $include.'Komp';
    $osztaly->komp = new $kompNev;
    
}


include(BASE.'modulok/'.$include.'/'.$file.'.php');